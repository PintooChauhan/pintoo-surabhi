<!-- Header Layout start -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->

      <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/css/jquery.dataTables.min.css">
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/css/select.dataTables.min.css"> -->
  
      <!-- <script src="app-assets/js/scripts/data-tables.js"></script> -->
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.0/css/fixedColumns.dataTables.min.css"> -->
      <!-- END PAGE LEVEL JS-->


<!-- Header Layout start -->




      <style>


div.dataTables_wrapper {
    width: 100% !important;
    margin: 0 auto;
}
select {
    display: initial !important; ;
}

/*::-webkit-scrollbar {
  display: none;
}*/
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.dataTables_filter input {
    border: 1px solid #aaa;
    border-radius: 3px;
    padding: 0px !important;
}
 


.dataTables_scrollBody{
    height: auto !important;
}




/* .tableFixHead {
  overflow: auto;
 
 
}*/

.tableFixHead table {
  border-collapse: collapse;
  width: 100%;
}

.tableFixHead th,
.tableFixHead td {
  padding: 8px 16px;
}


td:first-child, th:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:white;
}
td:nth-child(2),th:nth-child(2)  { 
position:sticky;
  left:102px;
  z-index:1;
  background-color:white;
  }
.tableFixHead th {
  position: sticky;
  top: 0;
  background: #eee;
  z-index:2
}
th:first-child , th:nth-child(2) {
  z-index:3
  }

</style>

    <script src="app-assets/js/custom/buyer.js"></script>


      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
      <div id="main" class="main-full" style="min-height: auto">
         <div class="row">
           
         <input type="hidden" value="@if(isset($buyerId) && !empty($buyerId)) {{$buyerId['buyer_tenant_id']}} @endif" id="buyer_tenant_id">
            <!-- <div class="col s12"> -->
               <!-- <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">BUYER INPUT FORM</span><hr>  </div> -->
                <x-leadinfor1-layout :buyerId="$buyerId"  :getBuyerData="$getBuyerData" :getCustomerData="$getCustomerData"></x-leadinfor1-layout>
                <x-requirementr1-layout :getBuyerData="$getBuyerData" ></x-requirementr1-layout>
                <x-requirementr2-layout :getBuyerData="$getBuyerData" ></x-requirementr2-layout>
                <x-requirementr3-layout :getBuyerData="$getBuyerData"></x-requirementr3-layout>
                <x-requirementr4-layout :getCustomerData="$getCustomerData"  :getBuyerData="$getBuyerData"></x-requirementr4-layout>
                <x-requirementr5-layout ></x-requirementr5-layout>
            <hr>                   
            <x-requirementr6-layout :getBuyerData="$getBuyerData" :getmemberData="$getmemberData"></x-requirementr6-layout>
            <a href="#" onClick="saveBuyer()">Save</a>
            <x-all_modals-layout></x-all_modals-layout>
             <hr>
             <!-- Code by avinash -->
 </div>
            
  
 
             <div class="row">
                            <div class="col s12">
                                <div class="card">
                                    <div class="card-content">
                                       
                                        <div class="row">
                                            <div class="col s12">
                                                <div class="tableFixHead">
                                                    <table id="scroll-vert-hor" class="display nowrap">
                                                    <thead>
                                                        <tr class="pin">
                                                            <th>First name</th>
                                                            <th>Last name</th>
                                                            <th>Position</th>
                                                            <th>Office</th>
                                                            <th>Age</th>
                                                            <th>Start date</th>
                                                            <th>Salary</th>
                                                            <th>Extn.</th>
                                                            <th>E-mail</th>
                                                            <th>First name</th>
                                                            <th>Last name</th>
                                                            <th>Position</th>
                                                            <th>Office</th>
                                                            <th>Age</th>
                                                            <th>Start date</th>
                                                            <th>Salary</th>
                                                            <th>Extn.</th>
                                                            <th>E-mail</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                            <td>Tiger</td>
                                                            <td>Nixon</td>
                                                            <td>System Architect</td>
                                                            <td>Edinburgh</td>
                                                            <td>61</td>
                                                            <td>2011/04/25</td>
                                                            <td>$320,800</td>
                                                            <td>5421</td>
                                                            <td>t.nixon@datatables.net</td>
                                                            <td>Tiger</td>
                                                            <td>Nixon</td>
                                                            <td>System Architect</td>
                                                            <td>Edinburgh</td>
                                                            <td>61</td>
                                                            <td>2011/04/25</td>
                                                            <td>$320,800</td>
                                                            <td>5421</td>
                                                            <td>t.nixon@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Garrett</td>
                                                            <td>Winters</td>
                                                            <td>Accountant</td>
                                                            <td>Tokyo</td>
                                                            <td>63</td>
                                                            <td>2011/07/25</td>
                                                            <td>$170,750</td>
                                                            <td>8422</td>
                                                            <td>g.winters@datatables.net</td>
                                                            <td>Garrett</td>
                                                            <td>Winters</td>
                                                            <td>Accountant</td>
                                                            <td>Tokyo</td>
                                                            <td>63</td>
                                                            <td>2011/07/25</td>
                                                            <td>$170,750</td>
                                                            <td>8422</td>
                                                            <td>g.winters@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ashton</td>
                                                            <td>Cox</td>
                                                            <td>Junior Technical Author</td>
                                                            <td>San Francisco</td>
                                                            <td>66</td>
                                                            <td>2009/01/12</td>
                                                            <td>$86,000</td>
                                                            <td>1562</td>
                                                            <td>a.cox@datatables.net</td>
                                                            <td>Ashton</td>
                                                            <td>Cox</td>
                                                            <td>Junior Technical Author</td>
                                                            <td>San Francisco</td>
                                                            <td>66</td>
                                                            <td>2009/01/12</td>
                                                            <td>$86,000</td>
                                                            <td>1562</td>
                                                            <td>a.cox@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Cedric</td>
                                                            <td>Kelly</td>
                                                            <td>Senior Javascript Developer</td>
                                                            <td>Edinburgh</td>
                                                            <td>22</td>
                                                            <td>2012/03/29</td>
                                                            <td>$433,060</td>
                                                            <td>6224</td>
                                                            <td>c.kelly@datatables.net</td>
                                                            <td>Cedric</td>
                                                            <td>Kelly</td>
                                                            <td>Senior Javascript Developer</td>
                                                            <td>Edinburgh</td>
                                                            <td>22</td>
                                                            <td>2012/03/29</td>
                                                            <td>$433,060</td>
                                                            <td>6224</td>
                                                            <td>c.kelly@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Airi</td>
                                                            <td>Satou</td>
                                                            <td>Accountant</td>
                                                            <td>Tokyo</td>
                                                            <td>33</td>
                                                            <td>2008/11/28</td>
                                                            <td>$162,700</td>
                                                            <td>5407</td>
                                                            <td>a.satou@datatables.net</td>
                                                            <td>Airi</td>
                                                            <td>Satou</td>
                                                            <td>Accountant</td>
                                                            <td>Tokyo</td>
                                                            <td>33</td>
                                                            <td>2008/11/28</td>
                                                            <td>$162,700</td>
                                                            <td>5407</td>
                                                            <td>a.satou@datatables.net</td>
                                                        </tr>
                                                         <tr>
                                                            <td>Jena</td>
                                                            <td>Gaines</td>
                                                            <td>Office Manager</td>
                                                            <td>London</td>
                                                            <td>30</td>
                                                            <td>2008/12/19</td>
                                                            <td>$90,560</td>
                                                            <td>3814</td>
                                                            <td>j.gaines@datatables.net</td>
                                                            <td>Jena</td>
                                                            <td>Gaines</td>
                                                            <td>Office Manager</td>
                                                            <td>London</td>
                                                            <td>30</td>
                                                            <td>2008/12/19</td>
                                                            <td>$90,560</td>
                                                            <td>3814</td>
                                                            <td>j.gaines@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Quinn</td>
                                                            <td>Flynn</td>
                                                            <td>Support Lead</td>
                                                            <td>Edinburgh</td>
                                                            <td>22</td>
                                                            <td>2013/03/03</td>
                                                            <td>$342,000</td>
                                                            <td>9497</td>
                                                            <td>q.flynn@datatables.net</td>
                                                            <td>Quinn</td>
                                                            <td>Flynn</td>
                                                            <td>Support Lead</td>
                                                            <td>Edinburgh</td>
                                                            <td>22</td>
                                                            <td>2013/03/03</td>
                                                            <td>$342,000</td>
                                                            <td>9497</td>
                                                            <td>q.flynn@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Charde</td>
                                                            <td>Marshall</td>
                                                            <td>Regional Director</td>
                                                            <td>San Francisco</td>
                                                            <td>36</td>
                                                            <td>2008/10/16</td>
                                                            <td>$470,600</td>
                                                            <td>6741</td>
                                                            <td>c.marshall@datatables.net</td>
                                                            <td>Charde</td>
                                                            <td>Marshall</td>
                                                            <td>Regional Director</td>
                                                            <td>San Francisco</td>
                                                            <td>36</td>
                                                            <td>2008/10/16</td>
                                                            <td>$470,600</td>
                                                            <td>6741</td>
                                                            <td>c.marshall@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Haley</td>
                                                            <td>Kennedy</td>
                                                            <td>Senior Marketing Designer</td>
                                                            <td>London</td>
                                                            <td>43</td>
                                                            <td>2012/12/18</td>
                                                            <td>$313,500</td>
                                                            <td>3597</td>
                                                            <td>h.kennedy@datatables.net</td>
                                                            <td>Haley</td>
                                                            <td>Kennedy</td>
                                                            <td>Senior Marketing Designer</td>
                                                            <td>London</td>
                                                            <td>43</td>
                                                            <td>2012/12/18</td>
                                                            <td>$313,500</td>
                                                            <td>3597</td>
                                                            <td>h.kennedy@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tatyana</td>
                                                            <td>Fitzpatrick</td>
                                                            <td>Regional Director</td>
                                                            <td>London</td>
                                                            <td>19</td>
                                                            <td>2010/03/17</td>
                                                            <td>$385,750</td>
                                                            <td>1965</td>
                                                            <td>t.fitzpatrick@datatables.net</td>
                                                            <td>Tatyana</td>
                                                            <td>Fitzpatrick</td>
                                                            <td>Regional Director</td>
                                                            <td>London</td>
                                                            <td>19</td>
                                                            <td>2010/03/17</td>
                                                            <td>$385,750</td>
                                                            <td>1965</td>
                                                            <td>t.fitzpatrick@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Michael</td>
                                                            <td>Silva</td>
                                                            <td>Marketing Designer</td>
                                                            <td>London</td>
                                                            <td>66</td>
                                                            <td>2012/11/27</td>
                                                            <td>$198,500</td>
                                                            <td>1581</td>
                                                            <td>m.silva@datatables.net</td>
                                                            <td>Michael</td>
                                                            <td>Silva</td>
                                                            <td>Marketing Designer</td>
                                                            <td>London</td>
                                                            <td>66</td>
                                                            <td>2012/11/27</td>
                                                            <td>$198,500</td>
                                                            <td>1581</td>
                                                            <td>m.silva@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Paul</td>
                                                            <td>Byrd</td>
                                                            <td>Chief Financial Officer (CFO)</td>
                                                            <td>New York</td>
                                                            <td>64</td>
                                                            <td>2010/06/09</td>
                                                            <td>$725,000</td>
                                                            <td>3059</td>
                                                            <td>p.byrd@datatables.net</td>
                                                            <td>Paul</td>
                                                            <td>Byrd</td>
                                                            <td>Chief Financial Officer (CFO)</td>
                                                            <td>New York</td>
                                                            <td>64</td>
                                                            <td>2010/06/09</td>
                                                            <td>$725,000</td>
                                                            <td>3059</td>
                                                            <td>p.byrd@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Gloria</td>
                                                            <td>Little</td>
                                                            <td>Systems Administrator</td>
                                                            <td>New York</td>
                                                            <td>59</td>
                                                            <td>2009/04/10</td>
                                                            <td>$237,500</td>
                                                            <td>1721</td>
                                                            <td>g.little@datatables.net</td>
                                                            <td>Gloria</td>
                                                            <td>Little</td>
                                                            <td>Systems Administrator</td>
                                                            <td>New York</td>
                                                            <td>59</td>
                                                            <td>2009/04/10</td>
                                                            <td>$237,500</td>
                                                            <td>1721</td>
                                                            <td>g.little@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bradley</td>
                                                            <td>Greer</td>
                                                            <td>Software Engineer</td>
                                                            <td>London</td>
                                                            <td>41</td>
                                                            <td>2012/10/13</td>
                                                            <td>$132,000</td>
                                                            <td>2558</td>
                                                            <td>b.greer@datatables.net</td>
                                                            <td>Bradley</td>
                                                            <td>Greer</td>
                                                            <td>Software Engineer</td>
                                                            <td>London</td>
                                                            <td>41</td>
                                                            <td>2012/10/13</td>
                                                            <td>$132,000</td>
                                                            <td>2558</td>
                                                            <td>b.greer@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Dai</td>
                                                            <td>Rios</td>
                                                            <td>Personnel Lead</td>
                                                            <td>Edinburgh</td>
                                                            <td>35</td>
                                                            <td>2012/09/26</td>
                                                            <td>$217,500</td>
                                                            <td>2290</td>
                                                            <td>d.rios@datatables.net</td>
                                                            <td>Dai</td>
                                                            <td>Rios</td>
                                                            <td>Personnel Lead</td>
                                                            <td>Edinburgh</td>
                                                            <td>35</td>
                                                            <td>2012/09/26</td>
                                                            <td>$217,500</td>
                                                            <td>2290</td>
                                                            <td>d.rios@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jenette</td>
                                                            <td>Caldwell</td>
                                                            <td>Development Lead</td>
                                                            <td>New York</td>
                                                            <td>30</td>
                                                            <td>2011/09/03</td>
                                                            <td>$345,000</td>
                                                            <td>1937</td>
                                                            <td>j.caldwell@datatables.net</td>
                                                            <td>Jenette</td>
                                                            <td>Caldwell</td>
                                                            <td>Development Lead</td>
                                                            <td>New York</td>
                                                            <td>30</td>
                                                            <td>2011/09/03</td>
                                                            <td>$345,000</td>
                                                            <td>1937</td>
                                                            <td>j.caldwell@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Yuri</td>
                                                            <td>Berry</td>
                                                            <td>Chief Marketing Officer (CMO)</td>
                                                            <td>New York</td>
                                                            <td>40</td>
                                                            <td>2009/06/25</td>
                                                            <td>$675,000</td>
                                                            <td>6154</td>
                                                            <td>y.berry@datatables.net</td>
                                                            <td>Yuri</td>
                                                            <td>Berry</td>
                                                            <td>Chief Marketing Officer (CMO)</td>
                                                            <td>New York</td>
                                                            <td>40</td>
                                                            <td>2009/06/25</td>
                                                            <td>$675,000</td>
                                                            <td>6154</td>
                                                            <td>y.berry@datatables.net</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Caesar</td>
                                                            <td>Vance</td>
                                                            <td>Pre-Sales Support</td>
                                                            <td>New York</td>
                                                            <td>21</td>
                                                            <td>2011/12/12</td>
                                                            <td>$106,450</td>
                                                            <td>8330</td>
                                                            <td>c.vance@datatables.net</td>
                                                            <td>Caesar</td>
                                                            <td>Vance</td>
                                                            <td>Pre-Sales Support</td>
                                                            <td>New York</td>
                                                            <td>21</td>
                                                            <td>2011/12/12</td>
                                                            <td>$106,450</td>
                                                            <td>8330</td>
                                                            <td>c.vance@datatables.net</td>
                                                        </tr>

                                                    </tbody>
                                                    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



     

             </div>


                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>
    <!-- <script src="app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> -->
    <!-- <script src="app-assets/vendors/data-tables/js/dataTables.select.min.js"></script> -->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <!-- <script src="app-assets/js/plugins.js"></script> -->
    <!-- <script src="app-assets/js/search.js"></script> -->
    <!-- <script src="app-assets/js/custom/custom-script.js"></script> -->
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/data-tables.js"></script>
    <!-- END PAGE LEVEL JS-->