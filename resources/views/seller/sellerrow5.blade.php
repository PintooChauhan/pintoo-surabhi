<div class="row">     
    
        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="key_member_seller()">
                <div class="collapsible-header" id="col_key_member_seller">Key members name </div>                
                </li>                                        
            </ul>
        </div>  

                        
        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="challenges_seller()">
                <div class="collapsible-header" id="col_challenges_seller">Challenges & solutions </div>                
                </li>                                        
            </ul>
        </div>   

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="lead_analysis_seller()">
                <div class="collapsible-header" id="col_lead_analysis_seller">Lead Analysis & Intensity </div>                
                </li>                                        
            </ul>
        </div> 
        
        

        
</div>


<div class="collapsible-body"  id='challenges_seller' style="display:none">

    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Challenges : <span class="red-text">*</span></label>
            <input type="text"  name="challenges"  Placeholder="Enter" >
            <!-- <div  class="addbtn" >
                    <a href="#" id="add_location13" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  -->
        </div>

        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Solutions : <span class="red-text">*</span></label>
            <input type="text"  name="solutions"  Placeholder="Enter" >
            <!-- <div  class="addbtn" >
                    <a href="#" id="add_location32" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  -->
        </div>
        
        <div class="input-field col l3 m4 s12" id="InputsWrapper2">

            <div class="row" >
                <div class="input-field col l2 m2 s6 display_search">
                <div class="preloader-wrapper small active" id="loader3" style="display:none">
                    <div class="spinner-layer spinner-green-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                    </div>
                </div>
                </div>
            </div>

                <div class="row" id="submitLooking">
                    <div class="col l4 m4">
                        <a href="javascript:void(0)" onClick="lookingSince()" class=" save_lookingSince btn-small  waves-effect waves-light green darken-1 " id="save_lookingSince">Save</a>
                    </div>
                    <div class="col l3 m3">
                            <a href="javascript:void(0)" onClick="clear_lookingSince()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_lookingSince"><i class="material-icons dp48">clear</i></a>
                    </div>
                </div>
            </div>
    </div>
   
    <br>
    <div class="row">
                <table class="bordered" id="looking_since_table" style="width: 100% !important" >
                    <thead>
                    <tr >
                    <th  style="color: white;background-color: #ffa500d4;">Sr No. </th>
                    <th  style="color: white;background-color: #ffa500d4;">Challenges</th>
                    <th  style="color: white;background-color: #ffa500d4;">Solutions</th>                    
                    <th  style="color: white;background-color: #ffa500d4;">User</th>                 
                    <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                    <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                    </tr>
                    </thead>
                    <tbody>
                  
                    </tbody>
                </table>
                <input type="hidden" id="incID1" value="">
          
    </div>

</div>



<div class="collapsible-body"  id='lead_analysis_seller' style="display:none">
    <div class="row">
            <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="lead_analysis">
                    <option value="" disabled selected>Select</option>
                    <option value="option 1">Option 1</option>
                </select>
                <label for="possession_year" class="active">Lead Stage </label>              
            </div>

            <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="lead_intensity">
                    <option value="" disabled selected>Select</option>
                    <option value="option 1">Cold</option>
                    <option value="option 1">Warm</option>
                </select>
                <label for="possession_year" class="active">Lead Intensity </label>              
            </div>


            <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="lead_analysis">
                    <option value="" disabled selected>Select</option>
                    <option value="option 1">Option 1</option>
                </select>
                <label for="possession_year" class="active">Lead Status </label>              
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">                            
                        <label for="wap_number" >Expected closure Date: </label>
                    <input type="text" class="datepicker" name="available_from" id="available_from" required placeholder="Enter" >                                
            </div>   
           

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Closure Description : <span class="red-text">*</span></label>
                <input type="text"  name="describe_in_5wh"  Placeholder="Enter" >               
            </div>

        
    </div>
</div>

<div class="collapsible-body"  id='key_member_seller' style="display:none">
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="suffix_decision" name="suffix_decision"  class="select2 browser-default">
                        <option value="mr" selected>Mr</option>
                        <option value="2000000">Option1</option>                      
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Decisoin Maker Name: <span class="red-text">*</span></label>
                <input type="text" class="validate mobile_number" name="decision_maker_name" id="decision_maker_name" required placeholder="Enter" >      
            
        </div>    
            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Relation with Seller : <span class="red-text">*</span></label>
                <input type="text"  name="decision_name_relation"  Placeholder="Enter" >               
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Remark : <span class="red-text">*</span></label>
                <input type="text"  name="decision_in_5wh"  Placeholder="Enter" >               
            </div>
    </div>

    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="suffix_influencer" name="suffix_influencer"  class="select2 browser-default">
                        <option value="mr" selected>Mr</option>
                        <option value="2000000">Option1</option>                      
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Influencer Name: <span class="red-text">*</span></label>
                <input type="text" class="validate mobile_number" name="influencer_name" id="influencer_name" required placeholder="Enter" >      
            
        </div>
            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Relation with Seller : <span class="red-text">*</span></label>
                <input type="text"  name="influencer_name_relation"  Placeholder="Enter" >               
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Remark : <span class="red-text">*</span></label>
                <input type="text"  name="influencer_in_5wh"  Placeholder="Enter" >               
            </div>
    </div>
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="suffix_financer" name="suffix_financer"  class="select2 browser-default">
                        <option value="mr" selected>Mr</option>
                        <option value="2000000">Option1</option>                      
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Financer Name: <span class="red-text">*</span></label>
                <input type="text" class="validate mobile_number" name="financer_name" id="financer_name" required placeholder="Enter" >      
            
        </div>
            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Relation with Seller : <span class="red-text">*</span></label>
                <input type="text"  name="financer_name_relation"  Placeholder="Enter" >               
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Remark : <span class="red-text">*</span></label>
                <input type="text"  name="financer_in_5wh"  Placeholder="Enter" >               
            </div>
    </div>

    
</div>

