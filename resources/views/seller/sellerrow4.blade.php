<div class="row">      
    
<div class="col l3 s12"  >
    <ul class="collapsible" >
        <li onClick="washroom_and_client()" >
        <div class="collapsible-header" id='col_washroom_and_client'>Washroom & Ideal Client</div>
        
        </li>                                        
    </ul>
</div>
    
    <div class="col l3 s12"  >
            <ul class="collapsible" >
                <li onClick="images_and_video()" >
                <div class="collapsible-header" id='col_images_and_video'>Images & Video</div>
                
                </li>                                        
            </ul>
    </div>    

    <div class="col l3 s12"  >
            <ul class="collapsible" >
                <li onClick="marketing()" >
                <div class="collapsible-header" id='col_marketing'>Marketing</div>
                
                </li>                                        
            </ul>
    </div>   

      
</div>



<div class="collapsible-body"  id='images_and_video' style="display:none">
    <div class="row">      

        <div class="input-field col l3 m4 s12 display_search">
            <label  class="active"> Unit Image : </label>
            <input type="file" name="images[]" multiple  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
        </div>

        <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">Unit Videos : </label>
            <input type="file" name="videos[]" multiple  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
        </div>

        <div class="input-field col l3 m4 s12 display_search">
        <label  class="active"> Unit Videos link : </label>
        <input type="text" name="video_links" multiple  id="input-file-now" placeholder="Enter">
        </div>
    </div>
</div>

<div class="collapsible-body"  id='washroom_and_client' style="display:none">
    <div class="row">
       

        <div class="input-field col l3 m4 s12 display_search">                
            <select class="select2  browser-default" multiple="multiple" id="washroom" data-placeholder="Select" name="washroom">
                <option value="option 1">Attached </option>
                <option value="option 1">Common</option>
            </select>
            <label for="building_type" class="active">Washroom </label>
        </div>
        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Ideal Client/Business  : </label>
            <input type="text"  name="nature_of_bussiness"  Placeholder="Enter" >
        </div>
    </div>
</div>

<div class="collapsible-body"  id='marketing' style="display:none">
    <div class="row">
       

        <div class="input-field col l3 m4 s12 display_search">                
            <select class="select2  browser-default" multiple="multiple" id="portal_name" data-placeholder="Select" name="portal_name">
                <option value="option 1">Option 1</option>                
            </select>
            <label for="building_type" class="active">Portal Name </label>
        </div>

        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Portal URL  : </label>
            <input type="text"  name="portal_url" id="portal_url"  Placeholder="Enter" >
        </div>

        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Comment : </label>
            <input type="text"  name="portal_comment" id="portal_comment"  Placeholder="Enter" >
        </div>

        <div class="input-field col l3 m4 s12 display_search">                
            <select class="select2  browser-default" multiple="multiple" id="portal_user_name" data-placeholder="Select" name="portal_user_name">
                <option value="option 1">Option 1</option>                
            </select>
            <label for="building_type" class="active">User name </label>
        </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">

                <div class="row" >
                    <div class="input-field col l2 m2 s6 display_search">
                    <div class="preloader-wrapper small active" id="loader3" style="display:none">
                        <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                    <div class="row" id="submitLooking">
                        <div class="col l4 m4">
                            <a href="javascript:void(0)" onClick="lookingSince()" class=" save_lookingSince btn-small  waves-effect waves-light green darken-1 " id="save_lookingSince">Save</a>
                        </div>
                        <div class="col l3 m3">
                                <a href="javascript:void(0)" onClick="clear_lookingSince()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_lookingSince"><i class="material-icons dp48">clear</i></a>
                        </div>
                    </div>
            </div> 
    </div>
    <br>
    <div class="row">
                <table class="bordered" id="looking_since_table" style="width: 100% !important" >
                    <thead>
                    <tr >
                    <th  style="color: white;background-color: #ffa500d4;">Sr No. </th>
                    <th  style="color: white;background-color: #ffa500d4;">Portal Name</th>
                    <th  style="color: white;background-color: #ffa500d4;">Portal URL</th>
                    <th  style="color: white;background-color: #ffa500d4;">Comment</th>
                    <th  style="color: white;background-color: #ffa500d4;">User name</th>                 
                    <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                    <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                    </tr>
                    </thead>
                    <tbody>
                  
                    </tbody>
                </table>
                <input type="hidden" id="incID1" value="">
          
            </div>
</div>


