<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
                label{ color: #908a8a;      }
                .input_select_size{padding: 0px 12px !important;}
            </style>
<style>label{ color: #908a8a;      }</style>
            <div class="row">                                        
                    
<!-- 
                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="building_name_and_structure()">
                            <div class="collapsible-header" id="col_building_name_and_structure">Building Info</div>
                            
                            </li>                                        
                        </ul>
                    </div> -->

                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="unit_info()">
                            <div class="collapsible-header" id='col_unit_info'>Unit Info</div>
                            
                            </li>                                        
                        </ul>
                    </div>

                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="price_seller()">
                            <div class="collapsible-header" id='col_price_seller'>Unit Price </div>
                            
                            </li>                                        
                        </ul>
                    </div> 

                                      
                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="finalization_seller()">
                            <div class="collapsible-header" id="col_finalization_seller">Reason to Sell</div>
                            
                            </li>                                        
                        </ul>
                    </div>

                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="unit_ame()">
                            <div class="collapsible-header" id="col_unit_ame">Unit Amenities</div>
                            
                            </li>                                        
                        </ul>
                    </div>
                    
                    

                    
                

                 

               

                    
            </div>

                                                    
                                                

        <div class="row">        
            <!-- Unit Info -->
            <div class="collapsible-body"  id='unit_info' style="display:none">
                                                    
                <div class="row" >
                        <div class="input-field col l3 m4 s12 display_search">
                            
                            <select class="validate select2 browser-default" id="building_name" name="building_name" onChange="getWing()">
                                    <option value="option 1" >Search or Add</option>
                                    <option value=""  selected>Select</option>
                                    @foreach($society as $sVal)
                                     @if( !empty($sVal->society_id) )
                                    <?php 
                                        $ids =$sVal->company_id.','.$sVal->project_id.','.$sVal->society_id;
                                        $bn ='';
                                        if($sVal->building_name != ''){
                                            $bn = ' ,'.ucwords($sVal->building_name);
                                        }

                                        $sub ='';
                                        if($sVal->sub_location_name != ''){
                                            $sub = ' ,'.ucwords($sVal->sub_location_name);
                                        }

                                        $loc ='';
                                        if($sVal->location_name != ''){
                                            $loc = ' ,'.ucwords($sVal->location_name);
                                        }
                                    ?>
                                      <option value="{{$ids}}"  ><?php echo ucwords($sVal->group_name).', '.ucwords($sVal->project_complex_name).$bn.$sub.$loc ?></option>
                                      @endif
                                  @endforeach 
                                </select>
                                <label for="building_name  " class="active">Building Name
                                </label>

                                <div id="AddMoreFileId3" class="addbtn"  style="top: 11px !important;right: -6px !important;">
                                    <a href="#modal14" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                                </div>  
                        </div>   

                        <div class="input-field col l3 m4 s12 display_search">
                            <select  id="seller_wing" name="seller_wing" class="validate select2 browser-default">
                            <option value=""  selected>Search or Add</option>

                                @foreach($wing as $wing)
                                    <option value="{{$wing->wing_id}}">{{ucwords($wing->wing_name)}}</option>
                                @endforeach
                            </select>
                            <label class="active">Wing Name </label>

                            <div id="AddMoreFileId3" class="addbtn"  style="top: 11px !important;right: -6px !important;">
                                    <a href="#modal15" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                                </div>  
                        </div>  


                        
                        <div class="input-field col l3 m4 s12 display_search">
                            
                                <select class="select2  browser-default" id="unit_status"   name="seller_unit_status">
                                    <option value=""  selected>Select</option>
                                    @foreach($unit_status as $unts)
                                        <option value="{{$unts->unit_status_id}}"> {{ucwords($unts->unit_status)}}</option>
                                    @endforeach
                                </select>
                                <label for="seller_unit_type  " class="active">Unit Status
                                </label>                            
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            
                            <select class="select2  browser-default" id="seller_unit_type"   name="seller_unit_type">
                              <option value=""  selected>Select</option>
                                    
                                    @foreach($unit_type as $unt)
                                        <option value="{{$unt->unit_type_id}}"> {{ucwords($unt->unit_type)}}</option>
                                    @endforeach
                            </select>
                            <label for="seller_unit_type  " class="active">Unit Type
                            </label>                            
                    </div>

                        
                    
                    </div>
                    <div class="row" >
                        
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                            
                                <label for="wap_number" >Unit No: </label>
                                <input type="text" class="validate" name="seller_unit_no" id="seller_unit_no" required placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12 ">                        
                            <select  id="seller_floor" name="seller_floor" placeholder="Select" class="select2  browser-default" >
                                <option>1</option>
                                <option>2</option>
                            </select>
                            <label class="active">Floor</label>
                        
                        </div>

                        <div class="input-field col l3 m4 s12 ">                        
                            <select  id="seller_configuration" name="seller_configuration" placeholder="Select" class="select2  browser-default"  >
                                @foreach($configuration as $config)
                                    <option value="{{$config->configuration_id}}">{{ ucwords($config->configuration_name)}}</option>
                                @endforeach
                            </select>
                            <label class="active">Configuration</label>

                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                                <ul class="collapsible" style="margin-top: -2px">
                                    <li onClick="panel_opening()">
                                    <div class="collapsible-header"  id="col_panel_opening">Area </div>
                                    
                                    </li>                                        
                                </ul>
                            </div> 

                    </div>

                    <div class='row' id='panel_opening' style="display:none" >


                             <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                    <div class="row">
                                        <div class="input-field col m8 s8" style="padding: 0 10px;">
                                        <input type="text" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area" required placeholder="Enter"  >
                                            <label>Carpet Area
                                        </div>
                                        <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                            <select  id="carpet_area_unit" name="carpet_area_unit" class="select2 browser-default ">
                                                @foreach($unit as $un1)
                                                    <option value="{{ $un1->unit_id }}">{{ ucwords($un1->unit_name) }}</option>                                     
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>  

                                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                    <div class="row">
                                        <div class="input-field col m8 s8" style="padding: 0 10px;">
                                        <input type="text" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area" required placeholder="Enter"  >
                                            <label>Built up Area
                                        </div>
                                        <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                            <select  id="build_up_area_unit" name="build_up_area_unit" class="select2 browser-default ">
                                                @foreach($unit as $un2)
                                                    <option value="{{ $un2->unit_id }}">{{ ucwords($un2->unit_name) }}</option>                                    
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                            

                                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                    <div class="row">
                                        <div class="input-field col m8 s8" style="padding: 0 10px;">
                                        <input type="text" class="input_select_size" name="rera_carpet_area" id="rera_carpet_area" required placeholder="Enter" >
                                            <label>RERA Carpet Area
                                        </div>
                                        <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                            <select  id="rera_carpet_area_unit" name="rera_carpet_area_unit" class="select2 browser-default ">
                                            @foreach($unit as $un3)
                                                <option value="{{ $un3->unit_id }}">{{ ucwords($un3->unit_name) }}</option>                                    
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                    <!-- <div class="row"> -->
                                        <!-- <div class="input-field col m8 s8" style="padding: 0 10px;"> -->
                                        <input type="text" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area" required placeholder="Enter"  >
                                            <label> Area
                                       
                                </div>

                                  

                            

                    </div>
                        

                        
                    <div class="row" >
                
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                            
                                <label for="wap_number" >Configuration Size: </label>
                                <input type="text" class="validate" name="configuration_size" id="configuration_size" required placeholder="Enter" >
                        </div>
                        
                    

                        <div class="input-field col l3 m4 s12 ">                        
                            <select  id="seller_dfd1" name="seller_dfd" placeholder="Select" class="select2  browser-default" >
                                <option>West</option>
                                <option>North East</option>
                            </select>
                            <label class="active">DFD</label>
                        
                        </div>

                        <div class="input-field col l3 m4 s12 ">                        
                            <select  id="seller_balcony" name="seller_balcony" placeholder="Select" class="select2  browser-default" >
                                <option>Select</option>   
                                <option>Yes</option>
                                <option>No</option>
                            </select>
                            <label class="active">Balcony</label>                       
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                            
                                <label for="wap_number">Unit View: </label>
                                <input type="text" class="validate" name="seller_ unit_code" id="seller_unit_code" placeholder="Enter">                                
                            </div>
                        
                            
                    </div>
                </div>

                       

            </div>

            <!-- Unit Price -->
            <div class="collapsible-body"  id='price_seller' style="display:none">
                <div class="row">
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            
                                <select  id="expected_price_amt" name="expected_price_amt" onChange="convert_expected_price()"  class="select2 browser-default">
                                    <option value="" disabled selected>Select</option>
                                        @foreach($expected_price as $val1)
                                        <option value="{{$val1->expected_price}}">{{$val1->expected_price_in_short}}</option>
                                        @endforeach
                                </select>
                                
                            <label for="wap_number" class="dopr_down_holder_label2 active">Expected Price: </label>
                            <span id="expected_price_words"><br></span>                                
                            
                        </div>
                        

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <select  id="final_price_amt1" name="final_price_amt1" onChange="convert_final_price()"  class="select2 browser-default">
                                    <option value="" disabled selected>Select</option>
                                      @foreach($final_price as $val2)
                                        <option value="{{$val2->final_price}}">{{$val2->final_price_in_short}}</option>
                                      @endforeach                                       
                                </select>
                            <label for="wap_number" class="dopr_down_holder_label1 active">Final Price: </label>
                            <span id="final_price_words"></span>                                                                               
                            
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                            
                                <label for="wap_number" >Unit Available for Inspection: </label>
                                <input type="text" class="validate" name="unit_of_inspection" id="seller_unit_no"  placeholder="Enter" >
                        </div>


                        <div class="col m3 l3 s12">
                            <div class="row">                                           

                                    <div class="input-field col l6 m6 s8" style="padding: 0 10px;">
                                
                                        <select class="select2 browser-default loan_amount" name="loan_amount" id="minimum_carpet_area" >
                                            <option>Select</option>
                                            @foreach($loan_amount as $val3)
                                                <option value="{{$val3->loan_amount}}">{{$val3->loan_amount_in_short}}</option>
                                            @endforeach   
                                        </select>
                                        <label for="minimum_carper_area" class="active">Loan Amount</label>   
                                    </div>
                                    <div class="input-field col l6 m6 s4 mobile_view" style="padding: 0 10px;">
                                        <select class="select2 browser-default" name="minimum_carpet_area_unit" id="minimum_carpet_area_unit" >
                                           <option>Select</option>
                                              @foreach($bank as $val4)
                                                <option value="{{$val4->dd_bank_id}}">{{ucwords($val4->bank)}}</option>
                                            @endforeach  
                                        </select>
                                        <label for="minimum_carper_area" class="active">Bank Name                              </label>   
                                    </div>
                            </div>
                        </div>
                                                
                </div>
                <div class="row">
                     

                        <div class="input-field col l3 m4 s12"  >                    
                            <label for="wap_number" class="active">Date of Registration: </label>
                            <input type="text" class="datepicker" name="date_of_registration"  id="date_of_registration"  placeholder="Calender" style="border: 1px solid #c3bf20 !important;height: 41px;" onChange="countDays()">
                            <span id="calender_span" style="display:none;color: white; border: solid 1px green; background-color: green;padding: 1px 10px 2px 10px;"></span>
                            <input type="hidden" id="calender_span_val">
                        </div> 


                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <select  id="final_price_amt" name="final_price_amt" onChange="convert_final_price()"  class="select2 browser-default">
                                    <option value="" disabled selected>Select</option>
                                    <option value="ri">Resident Indian</option>
                                    <option value="nri">Non resident Indian</option>                                                                
                                </select>
                            <label for="wap_number" class=" active">Seller's Nationality: </label>
                            
                        </div>

                </div>
                            

                            
                            
                         

            </div>
            
            <!-- Reason To Sell -->

            <div class="collapsible-body"  id='finalization_seller' style="display:none">
                <div class="row">
                                
                                <div class="input-field col l3 m4 s12 display_search">                
                                    <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="finalization_month_seller" id="finalization_month_seller">          

                                        @for($i=0; $i<=5;$i++)
                                            <option value="<?php echo date('M Y',strtotime('first day of +'.$i.' month')); ?>"><?php echo date('M Y',strtotime('first day of +'.$i.' month')); ?> </option> 
                                        @endfor                     
                                        
                                    </select>
                                    <label for="purpose" class="active"> Finalization Month  </label>
                                </div>
            
                                

                                <div class="input-field col l3 m4 s12" >  
                                    <label for="lead_assign" class="active">Reason to Sell: </label>                                    
                                    <input type="text" class="validate" name="reason_to_sell" id="reason_to_sell" placeholder="Enter" >
                                </div>

                                <div class="input-field col l3 m4 s12" id="InputsWrapper2">

                                    <div class="row" >
                                        <div class="input-field col l2 m2 s6 display_search">
                                        <div class="preloader-wrapper small active" id="loader3" style="display:none">
                                            <div class="spinner-layer spinner-green-only">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div><div class="gap-patch">
                                                <div class="circle"></div>
                                            </div><div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                        <div class="row" id="submitLooking">
                                            <div class="col l4 m4">
                                                <a href="javascript:void(0)" onClick="reasontoSell()" class=" save_reasontoSell btn-small  waves-effect waves-light green darken-1 " id="save_reasontoSell">Save</a>
                                            </div>
                                            <div class="col l3 m3">
                                                    <a href="javascript:void(0)" onClick="clear_reasontoSell()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_reasontoSell"><i class="material-icons dp48">clear</i></a>
                                            </div>
                                        </div>
                                </div> 
                                
                </div>
                <br>

                <div class="row">
                    <table class="bordered" id="looking_since_table" style="width: 100% !important" >
                        <thead>
                        <tr >
                        <th  style="color: white;background-color: #ffa500d4;">Sr No. </th>
                        <th  style="color: white;background-color: #ffa500d4;">Finalization Month</th>
                        <th  style="color: white;background-color: #ffa500d4;">Reason To Sell</th>
                          
                        <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                        <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                        </tr>
                        </thead>
                        <tbody>
                    
                        </tbody>
                    </table>
                    <input type="hidden" id="incID1" value="">
            
                </div>
            </div>      
        

        <div class="collapsible-body"  id='unit_ame' style="display:none">
            <div class='row'>
                <div class='col l3 m3 s12'>
                    <div class="input-field col l12 m12 s12 display_search">                    
                                <select class="select2  browser-default"  data-placeholder="Select" name="furnishing_status">
                                    <option value="" disabled selected>Select</option>
                                    <option value="option 1">Furnished</option>
                                    <option value="option 1">Unfurnished</option>
                                </select>
                                <label for="possession_year" class="active">Furnishing Status </label>
                                <div id="AddMoreFileId3" class="addbtn" style="position: unset !important">   
                        <a href="#modal13" id="unit_am" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                    </div>  
                    </div>
                    <!-- <div class="input-field col l12 m12 s12" >  
                        <label for="lead_assign" class="active">Comment: </label>
                        <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="unit_amenities_comment" rows='2' col='4'></textarea>
                    </div> -->
                </div>
                <div class='col l8 m8 s12'>
                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>
                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>
                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>
                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>
                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>
                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>

                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>

                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>
                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>
                    <div id="ck1-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                        </label>
                    </div>
                </div>  
               
            </div>

            <div class="row">
                <div class="col l3 s12">
                   <ul class="collapsible">
                       <li onClick="parking_seller()">
                       <div class="collapsible-header" id='col_parking_seller'>Parking</div>
                       
                       </li>                                        
                   </ul>
                </div>
            

        </div>
            
        </div>

      

        <div class="collapsible-body"  id='parking_seller' style="display:none">
           <div class='row'>
               <div class="input-field col l3 m4 s12 display_search">                    
                   <select class="select2  browser-default"  multiple="multiple"  data-placeholder="Select" name="parking_type">
                       <option value="allotted" >Allotted  </option>
                       <option value="basement">Basement</option>
                       <option value="podium">Podium</option>
                   </select>
                   <label for="possession_year" class="active">Parking Type </label>
               </div>

               <div class=" input-field  col l3 m4 s12">
                           <label for="loan_amt" id="loan_amt" class="active"> Parking Number: </label>                           
                           <input type="number" class="validate" name="parking_number"  id='no_of_fw_parking'  min="0" max="10"  placeholder="Enter" >
                        
                           
               </div>

              

               <div class=" input-field  col l3 m4 s12" id='no_of_tw_parking_show' >
                       <label for="loan_amt" id="loan_amt" class="active">Allotted Parking Number</label>                           
                       <input type="number" class="validate" name="no_of_tw_parking"  id='no_of_tw_parking'   min="0" max="10" placeholder="Enter" >
                       
               </div>

               <div class=" input-field  col l3 m4 s12">
                           <label for="loan_amt" id="loan_amt" class="active">No of FW Parking: </label>                           
                           <input type="number" class="validate" name="no_of_fw_parking"  id='no_of_fw_parking'  min="0" max="10"  placeholder="Enter" >
                        
                           
               </div>
           </div>


       </div>   


          
        
        </div>

                                            
    
        <!-- Modal Add Building Name -->
<div id="modal14" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Building</h5>
        <hr>
        
        <form method="post" id="add_building_name_form">
        <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                <select class="select2  browser-default"    name="company_id" id="company_id">
                        <option value="" selected>Select</option>
                        @foreach($company as $com)
                        <option value="{{$com->company_id}}" >{{ucwords($com->group_name)}}</option>
                        @endforeach
                    </select>
                    <label for="lead_assign" class="active">Group Name: </label>

                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <select class="select2  browser-default"    name="project_id" id="project_id">
                        <option value="" selected>Select</option>
                        @foreach($complex as $comp)
                        <option value="{{$comp->project_id}}" >{{ucwords($comp->project_complex_name)}}</option>
                        @endforeach
                    </select>
                    <label for="lead_assign" class="active">Complex Name: </label>
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Building Name: </label>
                    <input type="text" class="validate" name="building_name" id="building_name"   placeholder="Enter">                                       
                </div>
            </div>
            <div class="row">
                <div class="input-field col l4 m4 s12 display_search">
                    <select class="select2  browser-default"    name="sub_location" id="sub_location" onChange="getsublocation(this)">
                        <option value="" selected>Select</option>
                        @foreach($sub_location as $sub_loc)
                        <option value="{{$sub_loc->sub_location_id}}" >{{ucwords($sub_loc->sub_location_name)}}</option>
                        @endforeach
                    </select>
                    <label for="lead_assign" class="active">Sub Location: </label>
                   
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <select class="select2  browser-default"    name="location" id="location">
                        <option value="" selected>Select</option>
                        @foreach($location as $loc)
                        <option value="{{$loc->location_id}}" >{{ucwords($loc->location_name)}}</option>
                        @endforeach
                    </select>
                    <label for="lead_assign" class="active">Location: </label>
                    
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <select class="select2  browser-default"    name="city" id="city">
                            <option value="" selected>Select</option>
                            @foreach($city as $cty)
                            <option value="{{$cty->city_id}}" >{{ucwords($cty->city_name)}}</option>
                            @endforeach
                        </select>                                        
                    <label for="lead_assign" class="active">City: </label>
                   
                </div>

            </div>

            <div class="row">
                
                <div class="input-field col l4 m4 s12 display_search">
                        <select class="select2  browser-default"    name="state" id="state">
                            <option value="" selected>Select</option>
                            @foreach($state as $st)
                            <option value="{{$st->dd_state_id}}" >{{ucwords($st->state_name)}}</option>
                            @endforeach
                        </select>    
                    <label for="lead_assign" class="active">State: </label>
                    
                </div>
            </div>     
            
           
            <div class="alert alert-danger print-error-msg_add_building_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_building_name_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>

  <!-- Modal Add Wing -->
  <div id="modal15" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Wing</h5>
        <hr>
        
        <form method="post" id="add_wing_name_form">
        <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                <select class="select2  browser-default"    name="building" id="building">
                            <option value="" selected>Select</option>
                            @foreach($society1 as $building)
                            <option value="{{$building->society_id}}" >{{ucwords($building->building_name)}}</option>
                            @endforeach
                        </select> 
                    <label for="lead_assign" class="active">Building name: </label>
                  
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Wing: </label>
                    <input type="text" class="validate" name="add_wing_name" id="add_wing_name"   placeholder="Enter">                                       
                </div>

                
            </div>     
            
           
            <div class="alert alert-danger print-error-msg_add_wing_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_wing_name_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div> 



<script>
        function getCity(argument) {
            var city_id = argument;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(city_id) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {city_id:city_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        // $('#city').empty();
                        // $('#city').focus;
                        // $('#city').append('<option value=""> Select City</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="city"]').append('<option value="'+ value.city_id +'">' + value.city_name.charAt(0).toUpperCase()+value.city_name.slice(1) +'</option>');
                        $('#state').val(data[0].dd_state_id).trigger('change'); 
                    // });
                  }else{
                    $('#city').empty();
                  }
                  }
                });
            }else{
              $('#city').empty();
            }
        }

        function getlocation(argument) {
            var location_id = argument;
           // alert(location_id); return;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(location_id) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {location_id:location_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data);// return;
                      if(data){
                        // $('#location').empty();
                        // $('#location').focus;
                        // $('#location').append('<option value=""> Select location</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="location"]').append('<option value="'+ value.location_id +'">' + value.location_name.charAt(0).toUpperCase()+ value.location_name.slice(1)+ '</option>');
                        $('#city').val(data[0].city_id).trigger('change'); 
                        getCity(data[0].city_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }
        
        function getsublocation(argument) {
            var SublocationID = argument.value;
            // alert(SublocationID);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        // $('#location').empty();
                        // $('#location').focus;
                        // $('#location').append('<option value=""> Select location</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="location"]').append('<option value="'+ value.location_id +'">' + value.location_name.charAt(0).toUpperCase()+ value.location_name.slice(1)+ '</option>');

                        $('#location').val(data[0].location_id).trigger('change');  
                        getlocation(data[0].location_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }

  </script>


<script>

function apply_css1(skip,attr, val=''){   
                var id =returnColArray1();           
                id.forEach(function(entry) {
                    
                    if(entry==skip){
                        // alert(11)
                        $('#'+skip).css(attr,'orange','!important');
                        // $('#'+skip).attr('style', 'background-color: orange !important');
                    }else{            
                        if($('#'+entry).css('pointer-events') == 'none'){

                        }else{
                            $('#'+entry).css(attr,val);
                        }
                        
                        //action_to_hide();            
                        
                    }        
                });
                }

        function hide_n_show1(skip){
        var id = collapsible_body_ids1();
        collapsible_body_ids1().forEach(function(entry) {
            // console.log(id);
            if(entry==skip){
                // alert(skip);
                var x = document.getElementById(skip);  
                $('#'+skip).css('background-color','rgb(234 233 230)');
                alert(x)          
                if (x.style.display == "none") {
                    // alert(1)
                    
                    x.style.display = "block";
                } else {
                    // alert(2)
                    // alert(skip);
                    // $('#col_lead_info').removeAttr("style");
                    // $('#col_lead_info').css('background-color','white',"!important");
                    x.style.display = "none";
                    $('#col_'+skip).css('background-color','white');

                }
            }else{          
                $('#'+entry).hide();
            }
            
        });
        }


        function returnColArray1(){
        var a = Array('col_parking_seller');
        return a;
        }

        function collapsible_body_ids1(){
        var b = Array('parking_seller');
        return b;
        }

        function parking_seller(){    
        apply_css1('col_parking_seller','background-color','');
        hide_n_show1("parking_seller");     
        }


        function apply_css2(skip,attr, val=''){   
                var id =returnColArray2();           
                id.forEach(function(entry) {
                    
                    if(entry==skip){
                        // alert(11)
                        $('#'+skip).css(attr,'orange','!important');
                        // $('#'+skip).attr('style', 'background-color: orange !important');
                    }else{            
                        if($('#'+entry).css('pointer-events') == 'none'){

                        }else{
                            $('#'+entry).css(attr,val);
                        }
                        
                        //action_to_hide();            
                        
                    }        
                });
                }

        function hide_n_show2(skip){
        var id = collapsible_body_ids2();
        collapsible_body_ids2().forEach(function(entry) {
            
            // alert(entry)          
            if(entry==skip){
                // alert(skip);
                var x = document.getElementById(skip);  
                $('#'+skip).css('background-color','rgb(234 233 230)');
                // alert(x)          
                if (x.style.display == "none") {
                    // alert(1)
                    
                    x.style.display = "block";
                } else {
                    // alert(2)
                    // alert(skip);
                    // $('#col_lead_info').removeAttr("style");
                    // $('#col_lead_info').css('background-color','white',"!important");
                    x.style.display = "none";
                    $('#col_'+skip).css('background-color','white');

                }
            }else{          
                $('#'+entry).hide();
            }
            
        });
        }


        function returnColArray2(){
        var a = Array('col_panel_opening');
        return a;
        }

        function collapsible_body_ids2(){
        var b = Array('panel_opening');
        return b;
        }

        function panel_opening(){    
        apply_css2('col_panel_opening','background-color','');
        hide_n_show2("panel_opening");     
        }

</script>