@if(isset($getSpecificChoiceData))  
    @if(isset($getSpecificChoiceData[0]->building_amenities ) && !empty($getSpecificChoiceData[0]->building_amenities )) 
        <?php
                $decodeEle4 = json_decode($getSpecificChoiceData[0]->building_amenities) ;
                if(!empty($decodeEle4)){
                    if(!empty($decodeEle4[0])){
                    $exp4 = explode(',',$decodeEle4[0]);
                    foreach($exp4 as $val4){ ?>
                        <script>
                            $(document).ready(function(){
                                blankArry3.push(<?= $val4 ?>);
                                // alert(blankArry1);
                            });

                        </script>
                    <?php }
                        }
                    }
                ?>
    @endif
@endif

@if(isset($getSpecificChoiceData))  
    @if(isset($getSpecificChoiceData[0]->unit_amenities ) && !empty($getSpecificChoiceData[0]->unit_amenities )) 
        <?php
                $decodeEle5 = json_decode($getSpecificChoiceData[0]->unit_amenities) ;
                if(!empty($decodeEle5)){
                    if(!empty($decodeEle5[0])){
                    $exp5 = explode(',',$decodeEle5[0]);
                    foreach($exp5 as $val5){ ?>
                        <script>
                            $(document).ready(function(){
                                blankArry4.push(<?= $val5 ?>);
                                // alert(blankArry1);
                            });

                        </script>
                    <?php }
                        }
                    }
                ?>
    @endif
@endif
        <div class="row">                                        
                
                 <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="unit_amenities()">
                        <div class="collapsible-header" id='col_unit_amenities'>   Unit Amenities  </div>
                        
                        </li>                                        
                    </ul>
                </div> 
                                
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="society_amenities()">
                        <div class="collapsible-header" id="col_society_amenities">Building Amenities  </div>
                        
                        </li>                                        
                    </ul>
                </div>
                
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="parking()">
                        <div class="collapsible-header" id='col_parking'>Parking</div>
                        
                        </li>                                        
                    </ul>
                </div>

                                     
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="specific_choice()">
                        <div class="collapsible-header" id="col_specific_choice">Specific Choice </div>
                        
                        </li>                                        
                    </ul>
                </div>

             
                

        </div> 
      

        

        <div class="collapsible-body"  id='unit_amenities' style="display:none">
            <div class='row'>
            <!-- <div class='col l1 m1 s2'>
                    
                </div> -->
                <div class='col l3 m3 s12'>
                    <div class="input-field col l12 m12 s12 display_search">                    
                                <select class="select2  browser-default"  data-placeholder="Select" name="furnishing_status" id="furnishing_status">
                                    <option value="" disabled selected>Select</option>
                                    <option value="furnished"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->furnishing_status) && !empty($getBuyerData[0]->furnishing_status) ) @if($getBuyerData[0]->furnishing_status == 'furnished' ) selected  @endif @endif @endif>Furnished</option>
                                    <option value="unfurnished"   @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->furnishing_status) && !empty($getBuyerData[0]->furnishing_status) ) @if($getBuyerData[0]->furnishing_status == 'unfurnished' ) selected  @endif @endif @endif >Unfurnished</option>
                                    
                                    <!-- <option value="fully_furnished"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->furnishing_status) && !empty($getBuyerData[0]->furnishing_status) ) @if($getBuyerData[0]->furnishing_status == 'fully_furnished' ) selected  @endif @endif @endif>Fully-furnished</option> -->
                                </select>
                                <label for="possession_year" class="active">Furnishing Status </label>
                                <div id="AddMoreFileId3" class="addbtn" style="position: unset !important">   
                        <a href="#modal11" id="unit_am" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                    </div>  
                    </div>
                    <!-- <div class="input-field col l12 m12 s12" >  
                        <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                        <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="unit_amenities_comment" rows='2' col='4'></textarea>
                    </div> -->
                </div>
                <div class='col l8 m8 s12' id="ume">
                    
                    @foreach($unit_amenities as $unit_amenities1)

                    @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) )

                        <?php 
                                $decodeEle5 = json_decode($getSpecificChoiceData[0]->unit_amenities) ;
                                // print_r($decodeEle5);
                                if(isset($decodeEle5[0])){
                                $exp5 = explode(',',$decodeEle5[0]);
                                if(in_array($unit_amenities1->unit_amenities_id,$exp5)){?>
                                     <div id="ck1-button">
                                        <label>
                                            <input type="checkbox" checked onClick='addUnitAmenity({{$unit_amenities1->unit_amenities_id}})' value="{{$unit_amenities1->unit_amenities_id}}" id='unit_amenities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($unit_amenities1->amenities)}}</span>
                                        </label>
                                    </div>   
                                <?php }else{ ?>
                                     <div id="ck1-button">
                                        <label>
                                            <input type="checkbox" onClick='addUnitAmenity({{$unit_amenities1->unit_amenities_id}})' value="{{$unit_amenities1->unit_amenities_id}}" id='unit_amenities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($unit_amenities1->amenities)}}</span>
                                        </label>
                                    </div>
                                <?php } }else{ ?>
                                     <div id="ck1-button">
                            <label>
                                <input type="checkbox" onClick='addUnitAmenity({{$unit_amenities1->unit_amenities_id}})' value="{{$unit_amenities1->unit_amenities_id}}" id='unit_amenities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($unit_amenities1->amenities)}}</span>
                            </label>
                        </div>
                                <?php }
                            ?>
                        @else
                         <div id="ck1-button">
                            <label>
                                <input type="checkbox" onClick='addUnitAmenity({{$unit_amenities1->unit_amenities_id}})' value="{{$unit_amenities1->unit_amenities_id}}" id='unit_amenities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($unit_amenities1->amenities)}}</span>
                            </label>
                        </div>
                        @endif

                   
                    @endforeach

               
                </div>  
                
            </div>
            
            
        </div>


        
        
        <div class="collapsible-body"  id='society_amenities' style="display:none">
            
        <div class='row'>
                <!-- <div class='col l3 m3 s12'>                    
                    <div class="input-field col l12 m12 s12" >  
                        <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                        <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="unit_amenities_comment" rows='2' col='4'></textarea>
                    </div>
                </div> -->
                <div class='col l1 m1 s2'>
                    <div id="AddMoreFileId3" class="addbtn" style="position: unset !important">   
                        <a href="#modal12" id="unit_am" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                    </div>   
                </div> 
                <div class='col l10 m10 s12' id="bame">
                    @foreach($society_amnities as $society_amnities1)

                    @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) )

                        <?php 
                                $decodeEle4 = json_decode($getSpecificChoiceData[0]->building_amenities) ;
                                // print_r($decodeEle4);
                                if(isset($decodeEle4[0])){
                                $exp4 = explode(',',$decodeEle4[0]);
                                if(in_array($society_amnities1->project_amenities_id,$exp4)){?>
                                    <div id="ck-button">
                                        <label>
                                            <input type="checkbox" checked onClick='addBuildingAmenity({{$society_amnities1->project_amenities_id}})' value="{{$society_amnities1->project_amenities_id}}" id='society_amnities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($society_amnities1->amenities)}}</span>
                                        </label>
                                    </div>   
                                <?php }else{ ?>
                                    <div id="ck-button">
                                        <label>
                                            <input type="checkbox" onClick='addBuildingAmenity({{$society_amnities1->project_amenities_id}})' value="{{$society_amnities1->project_amenities_id}}" id='society_amnities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($society_amnities1->amenities)}}</span>
                                        </label>
                                    </div>
                                <?php } }else{ ?>
                                    <div id="ck-button">
                            <label>
                                <input type="checkbox" onClick='addBuildingAmenity({{$society_amnities1->project_amenities_id}})' value="{{$society_amnities1->project_amenities_id}}" id='society_amnities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($society_amnities1->amenities)}}</span>
                            </label>
                        </div>
                                <?php }
                            ?>
                        @else
                        <div id="ck-button">
                            <label>
                                <input type="checkbox" onClick='addBuildingAmenity({{$society_amnities1->project_amenities_id}})' value="{{$society_amnities1->project_amenities_id}}" id='society_amnities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($society_amnities1->amenities)}}</span>
                            </label>
                        </div>
                    @endif
                      
                   
                    @endforeach
                   
                </div>
                 
            </div>
        </div>
           

        <div class="collapsible-body"  id='parking' style="display:none">
            <div class='row'>
                <div class="input-field col l3 m4 s12 display_search">         
                               
                    <select class="select2  browser-default"  multiple="multiple"  data-placeholder="Select" name="parking_type" id="parking_type">
                        @if(isset($getBuyerData))
                            @if(isset($getBuyerData[0]->parking_type_id ) && !empty($getBuyerData[0]->parking_type_id ))
                                @php $parking_type_id2 = json_decode($getBuyerData[0]->parking_type_id ); 
                                    $parking_type_id2 = explode(",",$parking_type_id2[0]);     
                                @endphp
                                @foreach($parking as $parking)
                                <option value="{{$parking->parking_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $parking->parking_id,$parking_type_id2)) selected  @endif  @endif >{{ucwords($parking->parking_name)}}</option>
                                @endforeach
                            @else
                                @foreach($parking as $parking)
                                    <option value="{{$parking->parking_id}}" >{{ucwords($parking->parking_name)}}</option>
                                @endforeach
                            @endif
                        @else
                            @foreach($parking as $parking)
                                <option value="{{$parking->parking_id}}" >{{ucwords($parking->parking_name)}}</option>
                            @endforeach
                        @endif     
                        
                    
                    </select>
                    <label for="possession_year" class="active">Parking Type </label>
                </div>

                <div class=" input-field  col l3 m4 s12">
                            <label for="loan_amt" id="loan_amt" class="active">No of FW Parking: </label>                           
                            <input type="text" class="validate" name="no_of_fw_parking"  id='no_of_fw_parking'  placeholder="Enter"  value="@if(isset($getBuyerData)) @if( isset($getBuyerData[0]->no_of_four_wheeler_parking) ) {{$getBuyerData[0]->no_of_four_wheeler_parking}} @endif @endif ">
                         
                            
                </div>
                                    
                <div class=" input-field  col l3 m4 s12" id='no_of_tw_parking_show' > <!-- style="display:none" -->
                        <label for="loan_amt" id="loan_amt" class="active">No of TW Parking</label>                           
                        <input type="text"  class="validate" name="no_of_tw_parking"  id='no_of_tw_parking1' value="@if(isset($getBuyerData)) @if( isset($getBuyerData[0]->no_of_two_wheeler_parking) ) {{$getBuyerData[0]->no_of_two_wheeler_parking}} @endif @endif ">
                        
                </div>
            </div>


        </div>

        <div class="collapsible-body"  id='specific_choice' style="display:none">
            <div class="row">
                <!--  here get data form op tables project and society.  -->
                    <!-- <div class="input-field col l3 m4 s12 display_search">                
                            <label for="loan_amt" id="loan_amt" class="active">: </label>                           
                            <input type="text" class="validate" name="project_name"  id='project_name' placeholder="Enter" >
                    </div> -->

                    <div class="input-field col l3 m4 s12 display_search">         
                      
                        <select class="select2  browser-default"  multiple="multiple"  data-placeholder="Select" name="project_name" id="project_name">

                        @if(isset($getSpecificChoiceData))
                            @if(isset($getSpecificChoiceData[0]->complex_name ) && !empty($getSpecificChoiceData[0]->complex_name ))
                                @php $complex_name2 = json_decode($getSpecificChoiceData[0]->complex_name ); 
                                    $complex_name2 = explode(",",$complex_name2[0]);     
                                @endphp

                                @foreach($project as $project)
                                <option value="{{$project->project_id}}" @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) ) @if(in_array( $project->project_id,$complex_name2)) selected  @endif  @endif >{{ucwords($project->project_complex_name)}}</option>
                                @endforeach
                            @else
                                @foreach($project as $project)
                                    <option value="{{$project->project_id}}"  >{{ucwords($project->project_complex_name)}}</option>
                                @endforeach
                            @endif
                        @else
                            @foreach($project as $project)
                                <option value="{{$project->project_id}}"  >{{ucwords($project->project_complex_name)}}</option>
                            @endforeach
                            
                        @endif  


                     
                        </select>
                        <label for="possession_year" class="active">Complex Name </label>
                    </div>

                 

                    <div class="input-field col l3 m4 s12 display_search">         
                      
                      <select class="select2  browser-default"  multiple="multiple"  data-placeholder="Select" name="building_name" id="building_name">

                      @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData))
                            @if(isset($getSpecificChoiceData[0]->building_name ) && !empty($getSpecificChoiceData[0]->building_name ))
                                @php $building_name2 = json_decode($getSpecificChoiceData[0]->building_name ); 
                                    $building_name2 = explode(",",$building_name2[0]);     
                                @endphp

                                @foreach($society as $society)
                                <option value="{{$society->society_id}}" @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) ) @if(in_array( $society->society_id,$building_name2)) selected  @endif  @endif >{{ucwords($society->building_name)}}</option>
                                @endforeach
                            @else
                                @foreach($society as $society)
                                    <option value="{{$society->society_id}}"  >{{ucwords($society->building_name)}}</option>
                                @endforeach   

                            @endif
                        @else
                            @foreach($society as $society)
                                <option value="{{$society->society_id}}"  >{{ucwords($society->building_name)}}</option>
                            @endforeach
                            
                        @endif  

                     
                      </select>
                      <label for="possession_year" class="active">Building Name </label>
                  </div>


                    <div class="input-field col l3 m4 s12 display_search">         
                      
                      <select class="select2  browser-default"  multiple="multiple"  data-placeholder="Select" name="sub_location" id="sub_location">

                      @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData))
                            @if(isset($getSpecificChoiceData[0]->sub_location ) && !empty($getSpecificChoiceData[0]->sub_location ))
                                @php $sub_location2 = json_decode($getSpecificChoiceData[0]->sub_location ); 
                                    $sub_location2 = explode(",",$sub_location2[0]);     
                                @endphp

                                @foreach($sub_location as $sub_location)
                                <option value="{{$sub_location->sub_location_id}}" @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) ) @if(in_array( $sub_location->sub_location_id,$sub_location2)) selected  @endif  @endif >{{ucwords($sub_location->sub_location_name)}}</option>
                                @endforeach
                            @else
                                @foreach($sub_location as $sub_location)
                                 <option value="{{$sub_location->sub_location_id}}"  >{{ucwords($sub_location->sub_location_name)}}</option>
                                 @endforeach 

                            @endif
                        @else
                            @foreach($sub_location as $sub_location)
                                <option value="{{$sub_location->sub_location_id}}"  >{{ucwords($sub_location->sub_location_name)}}</option>
                            @endforeach
                            
                        @endif  

                       
                      </select>
                      <label for="possession_year" class="active">Sub Location </label>
                  </div>


                    
                    <div class="input-field col l3 m4 s12 display_search">         
                      
                      <select class="select2  browser-default"  multiple="multiple"  data-placeholder="Select" name="location" id="location">
                        @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData))
                            @if(isset($getSpecificChoiceData[0]->location ) && !empty($getSpecificChoiceData[0]->location ))
                                @php $location2 = json_decode($getSpecificChoiceData[0]->location ); 
                                    $location2 = explode(",",$location2[0]);     
                                @endphp

                                @foreach($location as $location)
                                <option value="{{$location->location_id}}" @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) ) @if(in_array( $location->location_id,$location2)) selected  @endif  @endif >{{ucwords($location->location_name)}}</option>
                                @endforeach
                            @else
                                @foreach($location as $location)
                                     <option value="{{$location->location_id}}"  >{{ucwords($location->location_name)}}</option>
                                @endforeach

                            @endif
                        @else
                            @foreach($location as $location)
                            <option value="{{$location->location_id}}"  >{{ucwords($location->location_name)}}</option>
                            @endforeach                            
                        @endif 


                   
                      </select>
                      <label for="possession_year" class="active">Location </label>
                  </div>

                   
            </div>

            <div class="row">
                      
                

  
                  

                      <div class="input-field col l3 m4 s12" >  
                        <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                        <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="specific_choice_comment" rows='2' col='4'></textarea> -->
                        <input type="text" class="validate" name="specific_choice_comment" id="specific_choice_comment" placeholder="Enter" >
                    </div>

                  
                     
            </div>
        </div>


    <!-- Modal Unit amenities -->
    <div id="modal12" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Building Amenities</h5>
        <hr>
        
        <form method="post" id="add_building_amenity_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Amenities: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_building_amenity" id="add_building_amenity"   placeholder="Enter">
                    <span class="add_building_amenity_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_building_amenity" style="display:none">
            <ul style="color:red"></ul>
            </div>
            <span id="buildingerr" style="color:red"></span>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_building_amenity_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>

     <!-- Modal Building  amenities -->
     <div id="modal11" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Unit Amenities</h5>
        <hr>
        
        <form method="post" id="add_unit_amenity_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Amenities: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                    <span class="add_unit_amenity_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_unit_amenity" style="display:none">
                 <ul style="color:red"></ul>
            </div>
            <span id="uniterr" style="color:red"></span>
            </div>
           
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_unit_amenity1()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>
<script>

function execute1(url,form,err){
    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(url);
            console.log(data);// return;
            if($.isEmptyObject(data.error)){
                $('#uniterr').html('');
                alert(data.success);
                var html= data.resp;
                        $('#ume').append( html);
                        $('#modal11').modal('close');
                        $('#unit_amenity').val('');
                // location.reload();
            }else{
                if(data.error=='0'){
                    $('#uniterr').html('Already Exist');
                    return false;
                }
                $('#uniterr').html('');
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
}

    function add_unit_amenity1(){
    var url = 'addUnitAmenity';
    var form = 'add_unit_amenity_form';
    var err = 'print-error-msg_add_unit_amenity';
    execute1(url,form,err);
}

function execute2(url,form,err){
    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(url);
            console.log(data);// return;
            if($.isEmptyObject(data.error)){
                $('#buildingerr').html('');
                alert(data.success);
                var html= data.resp;
                        $('#bame').append( html);
                        $('#modal12').modal('close');
                        $('#add_building_amenity').val('');
                // location.reload();
            }else{
                if(data.error=='0'){
                    $('#buildingerr').html('Already Exist');
                    return false;
                }
                $('#buildingerr').html('');
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
}

function add_building_amenity_form(){
    var url = 'addBuildingAmenity';
    var form = 'add_building_amenity_form';
    var err = 'print-error-msg_add_building_amenity';
    execute2(url,form,err);
}
</script>