<style>
    .select2-container--default .select2-selection--multiple:before{
        display: none !important;
    }
</style>
          
        <!-- });
</script> -->
        <div class="row">                                        
                
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="members()">
                            <div class="collapsible-header" id='col_members'> Family Members</div>                        
                        </li>                                        
                    </ul>
                </div>            
                
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="nature_of_business()" id="nature_of_business_li">
                        <div class="collapsible-header" id="col_nature_of_business">Nature of Business & Washroom</div>
                        
                        </li>                                        
                    </ul>
                </div>   

                
                <div class="col l3 s12" style="display:none !important">
                    <ul class="collapsible">
                        <li onClick="parking()">
                            <div class="collapsible-header" id='col_parking'>Parking</div>
                        
                        </li>                                        
                    </ul>
                </div>

                                     
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="specific_choice()">
                          <div class="collapsible-header" id="col_specific_choice">Specific Choice </div>
                        
                        </li>                                        
                    </ul>
                </div>

             
                

        </div> 
      

      
       


        <div class="collapsible-body"  id='parking' style="display:none">
            <div class='row'>
              

                <div class=" input-field  col l3 m4 s12">
                            <label for="loan_amt" id="loan_amt" class="active">No of FW Parking: </label>  
                            @php $no_of_four_wheeler_parking=''; @endphp
                            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->no_of_four_wheeler_parking) && !empty($getBuyerData[0]->no_of_four_wheeler_parking)) @php $no_of_four_wheeler_parking = $getBuyerData[0]->no_of_four_wheeler_parking; @endphp @endif @endif                         
                            <input type="text" onkeypress="return onlyNumberKey(event)" name="no_of_fw_parking"  id='no_of_fw_parking'  placeholder="Enter"  value="{{$no_of_four_wheeler_parking}}">
                         
                            
                </div>
                                    
                <div class=" input-field  col l3 m4 s12" id='no_of_tw_parking_show' style="display:none !important"> <!--  -->
                        <label for="loan_amt" id="loan_amt" class="active">No of TW Parking</label>  
                        @php $no_of_two_wheeler_parking=''; @endphp
                        @if(isset($getBuyerData))@if(isset($getBuyerData[0]->no_of_two_wheeler_parking) && !empty($getBuyerData[0]->no_of_two_wheeler_parking)) @php $no_of_two_wheeler_parking = $getBuyerData[0]->no_of_two_wheeler_parking; @endphp @endif @endif                                                  
                        <input type="text" onkeypress="return onlyNumberKey(event)" name="no_of_tw_parking"  id='no_of_tw_parking1'  placeholder="Enter"  value="{{$no_of_two_wheeler_parking}}">
                        
                </div>
            </div>


        </div>

        <div class="collapsible-body"  id='specific_choice' style="display:none">
            <div class="row">
                <!--  here get data form op tables project and society.  -->
                    <!-- <div class="input-field col l3 m4 s12 display_search">                
                            <label for="loan_amt" id="loan_amt" class="active">: </label>                           
                            <input type="text" class="validate" name="project_name"  id='project_name' placeholder="Enter" >
                    </div> -->

                   

                 

             

                    <div class="input-field col l6 m6 s12 display_search">                
                        <label for="spaecific_cmt" id="spaecific_cmt" class="active">Remark: </label>
                        @php $specific_choice_comment=''; @endphp
                        @if(isset($getSpecificChoiceData))@if(isset($getSpecificChoiceData[0]->comment) && !empty($getSpecificChoiceData[0]->comment)) @php $specific_choice_comment = $getSpecificChoiceData[0]->comment; @endphp @endif @endif                           
                        <input type="text" class="validate" name="specific_choice_comment"  id='specific_choice_comment' placeholder="Enter" value="{{$specific_choice_comment}}" title="{{$specific_choice_comment}}" >
                        
                      
                    </div>
                    <div class="input-field col l6 m6 s12 display_search">        
                    <span>What is that one thing which you are looking in this property?,</span> <br><span>What is your expectations from us as a property consultant?</span> 
                    </div>


                   
            </div>

            
        </div>


        <div class="collapsible-body"  id='members' style="display:none">
            <div class="row">
                <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Family Size : </label>
                    @php $familly_size=''; @endphp
                    @if(isset($getBuyerData))@if(isset($getBuyerData[0]->familly_size) && !empty($getBuyerData[0]->familly_size)) @php $familly_size = $getBuyerData[0]->familly_size; @endphp @endif @endif
                    <input type="text" onkeypress="return onlyNumberKey(event)"  name="family_size" id="family_size"  Placeholder="Enter" value="{{$familly_size}}">
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Working Members: </label>
                    @php $working_members=''; @endphp
                    @if(isset($getBuyerData))@if(isset($getBuyerData[0]->working_members) && !empty($getBuyerData[0]->working_members)) @php $working_members = $getBuyerData[0]->working_members; @endphp @endif @endif
                    <input type="text" onkeypress="return onlyNumberKey(event)"  name="working_members" id="working_members" Placeholder="Enter" value="{{$working_members}}">
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Members details: </label>
                    @php $member_details=''; @endphp
                    @if(isset($getBuyerData))@if(isset($getBuyerData[0]->member_details) && !empty($getBuyerData[0]->member_details)) @php $member_details = $getBuyerData[0]->member_details; @endphp @endif @endif
                    <input type="text"   name="members_details" id="members_details"  Placeholder="Enter" value="{{$member_details}}" title="{{$member_details}}">
                </div>  
                <div class="input-field col l3 m4 s12 display_search">
                    In case childrens ask bout their schools & college, If working which location , how they travel ?
                </div>
            </div>
        </div>

        <div class="collapsible-body"  id='nature_of_business' style="display:none">
                <div class="row">
                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Nature of Business : </label>
                        @php $nature_of_business_id=''; @endphp
                        @if(isset($getBuyerData))@if(isset($getBuyerData[0]->nature_of_business_id) && !empty($getBuyerData[0]->nature_of_business_id)) @php $nature_of_business_id = $getBuyerData[0]->nature_of_business_id; @endphp @endif @endif
                        <input type="text"  name="nature_of_bussiness" id="nature_of_bussiness"  Placeholder="Enter" value="{{$nature_of_business_id}}" title="{{$nature_of_business_id}}">
                    </div>

                    <div class="input-field col l3 m4 s12 display_search"> 
                        
                    
                        <select class="select2  browser-default" multiple="multiple" id="washroom" name="washroom">
                        @if(isset($getBuyerData))
                            @if(isset($getBuyerData[0]->washroom_type_id ) && !empty($getBuyerData[0]->washroom_type_id ))
                                @php $washroom_type_id2 = json_decode($getBuyerData[0]->washroom_type_id ); 
                                    $washroom_type_id2 = explode(",",$washroom_type_id2[0]);   
                                @endphp
                                @foreach($washroom_type as $washroom_type)
                                <option value="{{$washroom_type->dd_washroom_type_id}}"  @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $washroom_type->dd_washroom_type_id,$washroom_type_id2)) selected  @endif  @endif >{{ucwords($washroom_type->washroom_type)}}</option>
                                @endforeach
                            @else
                            @foreach($washroom_type as $washroom_type)
                                <option value="{{$washroom_type->dd_washroom_type_id}}"   >{{ucwords($washroom_type->washroom_type)}}</option>
                            @endforeach
                                @endif
                        @else
                            @foreach($washroom_type as $washroom_type)
                                <option value="{{$washroom_type->dd_washroom_type_id}}"   >{{ucwords($washroom_type->washroom_type)}}</option>
                            @endforeach
                        @endif

                
                        
                    
                    
                        </select>
                        <label for="building_type" class="active">Washroom </label>
                    </div>
                </div>
        </div>

