<meta name="csrf-token" content="{{ csrf_token() }}">
        <style>label{ color: #908a8a;      }</style>
       
        <script>
            $(document).ready(function(){


                    var unit_type = $('#unit_type').val();
                    if(unit_type != '' && unit_type != 'null' && unit_type != null ){
                        hideShowOnunitStatus();

                      
                    }

                    var unit_status = $('#unit_status').val();                  
                    if(unit_status != '' && unit_status != 'null' && unit_status != null ){
                        onunitStatusChange();      
                    }

                 
                      });
        </script>
            @if(isset($getBuyerData))  
           
            @if(isset($getBuyerData[0]->max_budget ) && isset($getBuyerData[0]->own_contribution_amount )) 
            @if($getBuyerData[0]->max_budget == $getBuyerData[0]->own_contribution_amount )
            <script>
                $(document).ready(function(){
                 $('#home_loan_li').css('pointer-events','none');
                 $("#col_home_loan").css('background-color','#f3c5c5'); 
                });
            </script>
            @endif 
            @endif 

 


            @if(isset($getBuyerData[0]->location_id ) && !empty($getBuyerData[0]->location_id )) 
                <?php
                    $decodeEle1 = json_decode($getBuyerData[0]->location_id) ;
                    if(!empty($decodeEle1)){
                        if(!empty($decodeEle1[0])){
                        $exp = explode(',',$decodeEle1[0]);
                        foreach($exp as $val){ ?>
                            <script>
                                $(document).ready(function(){
                                    blankArry1.push(<?= $val ?>);
                                    // alert(blankArry1);
                                });

                            </script>
                        <?php }
                            }
                        }
                 ?>
             @endif 

             @if(isset($getBuyerData[0]->sublocation_id ) && !empty($getBuyerData[0]->sublocation_id )) 
                <?php
                    $decodeEle2 = json_decode($getBuyerData[0]->sublocation_id) ;
                    if(!empty($decodeEle2)){
                        if(!empty($decodeEle2[0])){
                        $exp1 = explode(',',$decodeEle2[0]);
                        foreach($exp1 as $val1){ ?>
                            <script>
                                $(document).ready(function(){
                                    blankArry.push(<?= $val1 ?>);
                                    // alert(blankArry1);
                                });

                            </script>
                        <?php }
                            }
                        }
                 ?>
             @endif 


        @endif


            <div class="row">                                        
                    

                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="unit()">
                            <div class="collapsible-header" id="col_unit">Unit</div>
                            
                            </li>                                        
                        </ul>
                    </div>

                    <div class="col l3 s12">
                            <ul class="collapsible">
                                <li onClick="budget_loan()">
                                <div class="collapsible-header" id='col_budget_loan'>Budget</div>
                                
                                </li>                                        
                            </ul>
                    </div>

                    
                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="location1()">
                            <div class="collapsible-header" id='col_location1'>Location</div>
                            
                            </li>                                        
                        </ul>
                    </div>


                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="building_age()" id="building_age_li">
                                <div class="collapsible-header" id='col_building_age'>  Building & Unit Preference</div>                            
                            </li>                                        
                        </ul>
                    </div>

                    
                    
                    
                

                    <!-- <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="purpose()">
                            <div class="collapsible-header" id="col_purpose">Purpose</div>
                            
                            </li>                                        
                        </ul>
                    </div> -->

                    
            </div>

                                                    
                                                

        <div class="row">        
         

            <div class="collapsible-body"  id='location1' style="display:none">
                                                
                <div class='row'>
                    <div class="input-field col l3 m4 s12 display_search">
                        
                        <select disabled  id="location11" name="location1" class="validate select2 browser-default" multiple="true">
                         

                            @if(isset($getBuyerData))
                                @if(isset($getBuyerData[0]->location_id ) && !empty($getBuyerData[0]->location_id ))
                                    @php $location_id2 = json_decode($getBuyerData[0]->location_id ); 
                                        $location_id2 = explode(",",$location_id2[0]);   
                                    @endphp
                                    @foreach($location as $location1)
                                        <option value="{{$location1->location_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $location1->location_id,$location_id2)) selected  @endif  @endif>{{ucwords($location1->location_name )}}</option>
                                    @endforeach
                                @else
                                    @foreach($location as $location1)
                                        <option value="{{$location1->location_id}}" >{{ucwords($location1->location_name )}}</option>
                                    @endforeach
                                @endif
                            @else
                                @foreach($location as $location1)
                                    <option value="{{$location1->location_id}}" >{{ucwords($location1->location_name )}}</option>
                                @endforeach
                            @endif
                            
                        </select>
                        <label class="active">Location</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        
                        <select disabled  id="sub_location1" name="sub_location1" class="validate select2 browser-default"  multiple="true">                          
                            @if(isset($getBuyerData))
                                @if(isset($getBuyerData[0]->sublocation_id ) && !empty($getBuyerData[0]->sublocation_id ))
                                    @php $sub_location_id2 = json_decode($getBuyerData[0]->sublocation_id ); 
                                        $sub_location_id2 = explode(",",$sub_location_id2[0]);   
                                    @endphp
                                      @foreach($sub_location as $sub_location11)
                                        <option value="{{$sub_location11->sub_location_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $sub_location11->sub_location_id,$sub_location_id2)) selected  @endif  @endif>{{ucwords($sub_location11->sub_location_name )}}</option>
                                    @endforeach
                                @else
                                    @foreach($sub_location as $sub_location11)
                                        <option value="{{$sub_location11->sub_location_id}}" >{{ucwords($sub_location11->sub_location_name)}}</option>
                                    @endforeach
                                @endif
                            @else
                                  @foreach($sub_location as $sub_location11)
                                    <option value="{{$sub_location11->sub_location_id}}" >{{ucwords($sub_location11->sub_location_name)}}</option>
                                  @endforeach
                            @endif
                        </select>
                        <label class="active">Sub Location</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">                    
                        <select class="select2  browser-default" multiple="multiple" id="city1" data-placeholder="Company name, Branch name" name="city1">
                                <option value="option 1" selected>Thane</option>
                                <option value="option 1">Mulund</option>
                                <option value="option 1">Kalyan</option>
                            </select>
                            <label for="leadtype37  " class="active">City </label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    
                        <select  id="state1" name="state1" class="validate select2 browser-default">
                        <option value="" disabled selected>Select</option>
                            <option value="1" selected>Maharashtra</option>
                            
                        </select>
                        <label class="active">State</label>
                    </div>
                        
                    <div class="input-field col l1 m4 s12 display_search">
                    
                        <div  class="addbtn" >
                                <a href="#modal3" id="add_location" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                        </div> 
                </div>
                </div>
                <br>
                <div class="row">
                    <table>
                        <thead>
                            <tr>
                                <th width="15%">Locations</th>
                                <th>Sub Locations</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($location as $locations)
                          
                            <tr>
                                <td>
                            @if(isset($getBuyerData[0]->location_id ) && !empty($getBuyerData[0]->location_id )) 
                               <?php 
                                  $decodeEle1 = json_decode($getBuyerData[0]->location_id) ;
                                  $exp = explode(',',$decodeEle1[0]);
                                  if(in_array($locations->location_id,$exp)){?>
                                    <div id="ck-button">
                                        <label>
                                            <input type="checkbox" checked  id="ckkl_{{$locations->location_id}}" onClick='addLocationInDD({{$locations->location_id}})'><span style="padding: 0px 10px 0px 10px;" > {{ ucwords($locations->location_name) }}</span>
                                        </label>
                                    </div>   
                                  <?php }else{ ?>
                                    <div id="ck-button">
                                        <label>
                                            <input type="checkbox"    id="ckkl_{{$locations->location_id}}" onClick='addLocationInDD({{$locations->location_id}})'><span style="padding: 0px 10px 0px 10px;" > {{ ucwords($locations->location_name) }}</span>
                                        </label>
                                    </div>   
                                  <?php }
                               ?>
                               @else<div id="ck-button">
                                        <label>
                                            <input type="checkbox"    id="ckkl_{{$locations->location_id}}" onClick='addLocationInDD({{$locations->location_id}})'><span style="padding: 0px 10px 0px 10px;" > {{ ucwords($locations->location_name) }}</span>
                                        </label>
                                    </div>   
                            @endif
                                   
                                </td>
                                <td>
                                    @foreach($sub_location as $sub_locations)
                                        @if($sub_locations->location_id ==  $locations->location_id)

                                            @if(isset($getBuyerData[0]->sublocation_id ) && !empty($getBuyerData[0]->sublocation_id )) 
                      
                                                <?php 
                                                    $decodeEle3 = json_decode($getBuyerData[0]->sublocation_id) ;
                                                    $exp3 = explode(',',$decodeEle3[0]);
                                                    if(in_array($sub_locations->sub_location_id,$exp3)){?>
                                                        <div id="ck-button">
                                                            <label>
                                                                <input type="checkbox" checked  onClick='addSubLocationInDD({{$sub_locations->sub_location_id}},{{$locations->location_id}})'><span style="padding: 0px 10px 0px 10px;">{{ ucwords($sub_locations->sub_location_name) }}</span>
                                                            </label>
                                                        </div>   
                                                    <?php }else{ ?>
                                                        <div id="ck-button">
                                                            <label>
                                                                <input type="checkbox"  onClick='addSubLocationInDD({{$sub_locations->sub_location_id}},{{$locations->location_id}})'><span style="padding: 0px 10px 0px 10px;">{{ ucwords($sub_locations->sub_location_name) }}</span>
                                                            </label>
                                                        </div>   
                                                    <?php } 
                                                ?>
                                            @else
                                            <div id="ck-button">
                                                <label>
                                                    <input type="checkbox"  onClick='addSubLocationInDD({{$sub_locations->sub_location_id}},{{$locations->location_id}})'><span style="padding: 0px 10px 0px 10px;">{{ ucwords($sub_locations->sub_location_name) }}</span>
                                                </label>
                                            </div> 
                                            @endif
                                        @endif
                                    @endforeach     
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="collapsible-body"  id='unit' style="display:none">
                                                
                    <div class="row" >

                    <div class="input-field col l3 m4 s12 ">
                        <select class="select2  browser-default"  id="unit_type"   name="unit_type" onChange="getUnitType()"  >
                            <option value="residential"  @if(isset($getBuyerData))  @if(isset($getBuyerData[0]->unit_type_id ) && !empty($getBuyerData[0]->unit_type_id )) @if($getBuyerData[0]->unit_type_id == 'residential') selected @endif @endif @else selected @endif>Residential</option>
                            <option value="commercial" @if(isset($getBuyerData))  @if(isset($getBuyerData[0]->unit_type_id ) && !empty($getBuyerData[0]->unit_type_id )) @if($getBuyerData[0]->unit_type_id == 'commercial') selected @endif @endif @endif>Commercial</option>
                        </select>
                        <label for="purpose" class="active"> Unit type</label>
                    </div>

                        <div class="input-field col l3 m4 s12 ">
                                <!-- onChange="hideShowUnitType()" -->
                                @if($getBuyerData)                               
                                    @if(isset($getBuyerData[0]->units ) && !empty($getBuyerData[0]->units ))                                    
                                            @php $units2 = json_decode($getBuyerData[0]->units);
                                                $units22  = $units2[0];                                                  
                                            @endphp                
                                            <input type="text" id="unitsSel" value="<?php print_r($units22);?>" style="display:none !important">
                                    @endif   
                                @else
                                 
                                @endif   
                                <select class="select2  browser-default units" multiple="multiple"  id="units"   name="units"  >
                                    @if($getBuyerData)
                                        @if(isset($getBuyerData[0]->units ) && !empty($getBuyerData[0]->units ))
                                        @if($getBuyerData[0]->unit_type_id == 'residential') 
                                            @foreach($unit_type_residential as $unit_type_residential1)         
                                                    <option value="{{$unit_type_residential1->unit_type_residential_id}}" >{{ucfirst($unit_type_residential1->unit_type_residential_name)}}</option>
                                            @endforeach  
                                        @else
                                            @foreach($unit_type_commercial as $dd_unit_type_commercial1)         
                                                    <option value="{{$dd_unit_type_commercial1->unit_type_commercial_id}}"  >{{ucfirst($dd_unit_type_commercial1->unit_type_commercial_name)}}</option>
                                            @endforeach
                                        @endif
                                        @endif   
                                    @else
                                        @foreach($unit_type_residential as $unit_type_residential1)         
                                                <option value="{{$unit_type_residential1->unit_type_residential_id}}"  @if($unit_type_residential1->unit_type_residential_id == 1) selected  @endif>{{ucfirst($unit_type_residential1->unit_type_residential_name)}}</option>
                                        @endforeach
                                    @endif 
                                </select>
                                <label class="active">Unit</label>
                        </div>


                        <div class="input-field col l3 m4 s12 ">   
                            @if($getBuyerData)
                                @if(isset($getBuyerData[0]->configuration_id ) && !empty($getBuyerData[0]->configuration_id ))
                                         @php $config2 = json_decode($getBuyerData[0]->configuration_id);
                                            $config22  = $config2[0];        
                                         @endphp                
                                        <input type="hidden" id="ConfigSel" value="<?php print_r($config22);?>" style="display:none !important">
                                @endif            
                            @endif   
                            <select  id="configuration" name="configuration" placeholder="Select" class="select2  browser-default configuration" multiple="multiple" onChange="hideShowMinCarpet()" >
                                @foreach($configuration as $config)
                                    <option value="{{$config->configuration_id}}"    >{{ strtoupper($config->configuration_name)}}</option>
                                @endforeach
                            </select>
                            <label class="active">Configuration</label>
                        </div>


                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select  id="unit_status" name="unit_status" onChange="onunitStatusChange(this)" class="validate select2 browser-default">
                            <option value=""  selected>Select</option>                                
                                <option value="under_construction"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->unit_status) ) @if( $getBuyerData[0]->unit_status == 'under_construction' ) selected @endif @endif @endif >Under Construction</option>
                                <option value="rtmi" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->unit_status) ) @if( $getBuyerData[0]->unit_status == 'rtmi' ) selected @endif @endif @endif >RTMI</option>            
                                <option value="under_construction_and_rtmi" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->unit_status) ) @if( $getBuyerData[0]->unit_status == 'under_construction_and_rtmi' ) selected @endif @endif @endif >Under Construction & RTMI</option>            
                            </select>
                            <label class="active">Unit Status</label>
                        </div>

                    </div>              

                    <div class="row" >

                      <!-- Start work from here Prasanna -->
                 		
                      <div class="col m3 l3 s12">
                            <div class="row">
                                    @if($getBuyerData)
                                        @if(isset($getBuyerData[0]->min_carpet_area ) && !empty($getBuyerData[0]->min_carpet_area ))
                                                <input type="hidden" id="minCarpetSel" value="{{$getBuyerData[0]->min_carpet_area}}">
                                                <input type="hidden" id="minimum_carpet_area" class="minimum_carpet_area_sel" value="{{$getBuyerData[0]->min_carpet_area}}">
                                        @endif            
                                    @endif            

                                    <div class="input-field col l8 m6 s8" style="padding: 0 10px;">
                                
                                        <select class="select2 browser-default minimum_carpet_area" name="minimum_carpet_area" id="minimum_carpet_area" onChange="getMaxCarpetArea()">
                                            
                                        </select>
                                        <label for="minimum_carper_area" class="active">Minimum carpet area                              </label>   
                                    </div>
                                    <div class="input-field col l4 m6 s4 mobile_view" style="padding: 0 10px;">
                                        <select class="select2 browser-default" name="minimum_carpet_area_unit" id="minimum_carpet_area_unit" >
                                            @foreach($unit as $unit1)
                                                <option value="{{$unit1->unit_id}}" >{{ ucwords($unit1->unit_name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>
                        </div>

                        <!-- End work from here Prasanna -->
                            <div class="col m3 l3 s12">
                                <div class="row">
                                        @if($getBuyerData)
                                            @if(isset($getBuyerData[0]->max_carpet_area ) && !empty($getBuyerData[0]->max_carpet_area ))
                                                    <input type="hidden" id="maxCarpetSel" value="{{$getBuyerData[0]->max_carpet_area}}">
                                                    <input type="hidden" id="maximum_carpet_area" class="maximum_carpet_area_sel"  value="{{$getBuyerData[0]->max_carpet_area}}">
                                            @endif            
                                        @endif  
                                        <div class="input-field col l8 m6 s8" style="padding: 0 10px;">
                                                                                                        <!-- onChange="ChekwithMinCarpet()" -->
                                            <select class="select2 browser-default maximum_carpet_area" name="maximum_carpet_area" id="maximum_carpet_area" >
                                            
                                            </select>
                                            <label for="maximum_carper_area" class="active">Maximum carpet area</label>   
                                        </div>
                                        <div class="input-field col l4 m6 s4 mobile_view" style="padding: 0 10px;">
                                                <select class="select2 browser-default"  name="maximum_carpet_area_unit" id="maximum_carpet_area_unit">
                                                @foreach($unit as $unit2)
                                                <option value="{{$unit2->unit_id}}">{{ ucwords($unit2->unit_name)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                </div>
                                <span id="max_carpet_error" style="color:red"></span>
                            </div>
                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select  id="possession_year11" name="possession_year"  class="validate select2 browser-default">
                                <option value=""  selected>Select</option>                                
                                @foreach($possession_year as $possession_year1)
                                    <option value="{{$possession_year1->dd_possession_year_id}}" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->possision_year) ) @if( $possession_year1->dd_possession_year_id ==  $getBuyerData[0]->possision_year ) selected @endif @endif @endif >{{ ucwords($possession_year1->possession_year)}}</option>
                                @endforeach
                            </select>
                            <label class="active">Possesion Year</label>
                        </div>

               
                   
                        <div class="input-field col l3 m4 s12 display_search" id="payment_pln">
       
                            <select class="select2  browser-default" multiple="multiple"  name="payment_plan" id="payment_plan11">
                                  
                                
                                  @if(isset($getBuyerData))
                                    @if(isset($getBuyerData[0]->payment_plan_id ) && !empty($getBuyerData[0]->payment_plan_id ))
                                    
                                        @php $payment_plan_id2 = json_decode($getBuyerData[0]->payment_plan_id ); 
                                            $payment_plan_id2 = explode(",",$payment_plan_id2[0]);   
                                        @endphp
                                        @foreach($payment_plan as $payment_plan)
                                            <option value="{{$payment_plan->dd_payment_plan_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $payment_plan->dd_payment_plan_id,$payment_plan_id2)) selected  @endif  @endif >{{  ucwords($payment_plan->payment_plan)}}</option>
                                        @endforeach
                                    @else
                                    
                                        @foreach($payment_plan as $payment_plan)
                                            <option value="{{$payment_plan->dd_payment_plan_id}}" >{{ ucwords($payment_plan->payment_plan)}}</option>
                                        @endforeach
                                    @endif
                                @else
                                
                                 @foreach($payment_plan as $payment_plan)
                                      <option value="{{$payment_plan->dd_payment_plan_id}}" >{{ ucwords($payment_plan->payment_plan)}}</option>
                                  @endforeach
                                @endif
                                
                            </select>
                            <label for="purpose" class="active">Payment Plan
                            </label>
                        </div> 
                   
                    
                        </div>
            </div>

            <div class="collapsible-body"  id='budget_loan' style="display:none">
                    <div class="row">
                       

                        <div class="input-field col l3 m4 s12 display_search">   
                        @if(isset($getBuyerData) && !empty($getBuyerData)) @if(isset($getBuyerData[0]->min_budget) && !empty($getBuyerData[0]->min_budget) && $getBuyerData[0]->min_budget != null) 
                            <input type="hidden" value="{{$getBuyerData[0]->min_budget}}" id="minBudSel"> 
                            <input type="hidden" value="{{$getBuyerData[0]->min_budget}}" id="minimum_budget_amt"> 
                        @endif @endif                 
                             <select  id="minimum_budget_amt" name="minimum_budget_amt" onChange="convert_min_budget()"  class="select2 browser-default minimum_budget_amt">
                                <option value="" disabled selected>Select</option>
                                @foreach($minimum_budget as $minimum_budget2)
                                    <option value="{{ $minimum_budget2->minimum_budget }}" @if(isset($getBuyerData)) @if(isset($getBuyerData[0]->min_budget) && !empty($getBuyerData[0]->min_budget)) @if( $minimum_budget2->minimum_budget ==  $getBuyerData[0]->min_budget) selected @endif @endif @endif>{{ ucwords($minimum_budget2->minimum_budget_in_short)}}</option>
                                @endforeach
                                
                            </select>
                            <label class="active">Minimum Budget</label>
                            <span id="minimum_budget_amt_words"><br></span>   
                        </div>

                        
                        <div class="input-field col l3 m4 s12 display_search">                    
                        @if(isset($getBuyerData) && !empty($getBuyerData)) @if(isset($getBuyerData[0]->max_budget) && !empty($getBuyerData[0]->max_budget) && $getBuyerData[0]->max_budget != null) 
                            <input type="hidden" value="{{$getBuyerData[0]->max_budget}}" id="maxBudSel"> 
                            <input type="hidden" value="{{$getBuyerData[0]->max_budget}}" id="maximum_budget_amt"> 
                        @endif @endif
                            <select  id="maximum_budget_amt" name="maximum_budget_amt" onChange="convert_max_budget()"  class="select2 browser-default maximum_budget_amt"> </select>
                            <label class="active">Maximum Budget</label>
                            <span id="maximum_budget_words"><br></span>  
                            <span id="maximum_budget_error" style="color:red"></span>     
                        </div>
                       
                        <div class="input-field col l3 m4 s12 display_search">   
                        @if(isset($getBuyerData) && !empty($getBuyerData)) @if(isset($getBuyerData[0]->own_contribution_amount) && !empty($getBuyerData[0]->own_contribution_amount) && $getBuyerData[0]->own_contribution_amount != null) 
                            <input type="hidden" value="{{$getBuyerData[0]->own_contribution_amount}}" id="OwnCoBudSel"> 
                            <input type="hidden" value="{{$getBuyerData[0]->own_contribution_amount}}" id="own_contribution_amt" class="own_contribution_amt_sel"> 
                        @endif @endif                 
                                <select   id="own_contribution_amt" name="own_contribution_amt" onChange="convert_own_contribution_amt()"   class="select2 browser-default own_contribution_amt"> </select>
                            <label class="active">Own Contribution Amount</label>
                            <span id="own_contribution_amount_words"></span> 
                            <span id="own_contribution_error" style="color:red"></span>       
                        </div>                                      
                        
                        <div class="input-field col l3 m4 s12 display_search">                                    
                                    <select  id="oc_time_period" name="oc_time_period" class="validate select2 browser-default">
                                    <option value=""  selected>Select</option>
                                          @foreach($oc_time_period as $oc_time_period2)
                                            <option value="{{$oc_time_period2->dd_oc_time_period_id}}" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->oc_time_period) ) @if( $oc_time_period2->dd_oc_time_period_id == $getBuyerData[0]->oc_time_period ) selected @endif @endif @endif >{{ ucwords($oc_time_period2->oc_time_period)}}</option>
                                            @endforeach
                                    </select>
                                    <label class="active">Own Contribution Time Period</label>
                        </div>
                        
                           
                        
                    </div>
                    

                    <div class="row">                    

                    <div class="input-field col l3 m4 s12 display_search">
                                 
                                 <select class="select2  browser-default" multiple="multiple" id="own_contribution_source"  name="own_contribution_source">
                                     <option value="" disabled >select</option>
                                     @if(isset($getBuyerData))
                                         @if(isset($getBuyerData[0]->oc_source_id ) && !empty($getBuyerData[0]->oc_source_id ))
                                             @php $oc_source_id2 = json_decode($getBuyerData[0]->oc_source_id ); 
                                                 $oc_source_id2 = explode(",",$oc_source_id2[0]);     
                                             @endphp
                                             @foreach($own_contribution_source as $own_contribution_source2)
                                             <option value="{{$own_contribution_source2->dd_own_contribution_source_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $own_contribution_source2->dd_own_contribution_source_id,$oc_source_id2)) selected  @endif  @endif >{{ ucwords($own_contribution_source2->own_contribution_source)}}</option>
                                             @endforeach
                                         @else
                                               @foreach($own_contribution_source as $own_contribution_source2)
                                             <option value="{{$own_contribution_source2->dd_own_contribution_source_id}}">{{ ucwords($own_contribution_source2->own_contribution_source)}}</option>
                                             @endforeach
                                         @endif
                                     @else   
                                         @foreach($own_contribution_source as $own_contribution_source2)
                                             <option value="{{$own_contribution_source2->dd_own_contribution_source_id}}">{{ ucwords($own_contribution_source2->own_contribution_source)}}</option>
                                         @endforeach

                                     @endif

                                
                                     
                                 </select>
                                 <label for="own_contri_source  " class="active">Own Contribution Source
                                 </label>
                         </div>
                        
                         
                        <!-- <div class=" input-field  col l3 m4 s12" style="padding-top: 10px">
                               
                        </div> -->
                        <div class="input-field col l3 m4 s12 display_search">

               

<select class="select2  browser-default" multiple="multiple"   name="preferred_bank" id="preferred_bank">
@if(isset($getBuyerData))
        @if(isset($getBuyerData[0]->preferred_bank_id ) && !empty($getBuyerData[0]->preferred_bank_id ))
            @php $preferred_bank_id2 = json_decode($getBuyerData[0]->preferred_bank_id ); 
                $preferred_bank_id2 = explode(",",$preferred_bank_id2[0]);   
            @endphp
            @foreach($bank as $bank2)
                <option value="{{$bank2->dd_bank_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $bank2->dd_bank_id,$preferred_bank_id2)) selected  @endif  @endif >{{ ucwords($bank2->bank)}}</option>
            @endforeach
        @else
        @foreach($bank as $bank2)
            <option value="{{$bank2->dd_bank_id}}"  >{{ ucwords($bank2->bank)}}</option>
        @endforeach
        @endif
@else
    @foreach($bank as $bank2)
        <option value="{{$bank2->dd_bank_id}}"  >{{ ucwords($bank2->bank)}}</option>
    @endforeach
@endif

</select>
<label for="purpose" class="active">Preferred Bank
</label>
<!-- <span>Given a choice which bank would you prefer to go ?</span> -->
<?php $m=$o=0; ?>
                                @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->max_budget) ) @php $m = $getBuyerData[0]->max_budget ; @endphp   @endif @else @php $m = 0; @endphp  @endif
                                @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->max_budget) ) @php $o = $getBuyerData[0]->own_contribution_amount ; @endphp  @endif @else @php $o = 0; @endphp @endif
                                <span style="font-weight: 600" >Loan Amount : <span id="loan_amt" style="display:none"> @if(isset($getBuyerData) && !empty($getBuyerData)) @php print_r((int)$m -(int)$o);  @endphp @endif</span>   <span id="loan_amt_in_words"></span> </span> 
                                <br><span>Given a choice which bank would you prefer to go ?</span>
</div>

<div class="input-field col l3 m4 s12 display_search">
<!-- onChange="change_loan_status_window()" -->
<select class="select2  browser-default"    dname="loan_status" id="loan_status">
    <option value="0"  selected>Select</option>
    <option value="sanctioned"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->loan_status_id) ) @if( $getBuyerData[0]->loan_status_id == 'sanctioned' ) selected @endif @endif @endif >Sanctioned</option>
    <option value="in_process"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->loan_status_id) ) @if( $getBuyerData[0]->loan_status_id == 'in_process' ) selected @endif @endif @endif >In Process</option>
    <option value="need_assiatance"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->loan_status_id) ) @if( $getBuyerData[0]->loan_status_id == 'need_assiatance' ) selected @endif @endif @endif >Need Assistance</option>
</select>
<label for="purpose" class="active">Loan Status
</label>
</div>



                           

                            <div class=" input-field  col l3 m4 s12">
                                <label for="lead_assign" class="active">Remark: <span class="red-text">*</span></label>
                                @php $Other_financial_details=''; @endphp
                                @if(isset($getBuyerData))@if(isset($getBuyerData[0]->Other_financial_details) && !empty($getBuyerData[0]->Other_financial_details)) @php $Other_financial_details = $getBuyerData[0]->Other_financial_details; @endphp @endif @endif
                                <input type="text" name="other_financial_details" id="other_financial_details" placeholder="Enter" value="{{$Other_financial_details}}" title="{{$Other_financial_details}}">
                                
                            </div>

                           

                           

                    </div>
                    
                  

                </div>

            </div>

            <div class="collapsible-body"  id='building_age' style="display:none">
                <div class="row">

                <div class="input-field col l3 m4 s12 display_search">  
                        @php $complex_name=''; @endphp
                        @if(isset($getSpecificChoiceData))@if(isset($getSpecificChoiceData[0]->complex_name) && !empty($getSpecificChoiceData[0]->complex_name)) @php $complex_name = $getSpecificChoiceData[0]->complex_name;
                        $complex_name2 = json_decode($complex_name); 
                        $complex_name2 = explode(",",$complex_name2[0]);       
                         @endphp @endif @endif          


                      
                        <select class="select2  browser-default"  multiple="multiple"  name="project_name" id="project_name">

                             @foreach($society as $sVal)
                                  <?php 
                                    $ids = $sVal->project_id.'-'.$sVal->society_id;
                                    $bn ='';
                                    if($sVal->building_name != ''){
                                        $bn = ' - '.ucwords($sVal->building_name);
                                    }
                                    //print_r($getSpecificChoiceData->complex_name);
                                  ?>
                                      <option value="{{$ids}}" @if(isset($complex_name2) && !empty($complex_name2) ) @if(in_array($ids,$complex_name2)) selected  @endif  @endif ><?php echo ucwords($sVal->project_complex_name).$bn ?></option>
                                  @endforeach 
                           


                     
                        </select>
                        <label for="possession_year" class="active">Complex/Building Name </label>
                        <span>Why in this complex?</span>  <span>Why in this building?</span>
                    </div>
                    
                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"   name="building_age" id="building_age1" >
                        <option value=""  selected>Select</option>
                        @foreach($building_age as $building_age1)
                            <option value="{{$building_age1->dd_building_age_id}}" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->building_age) ) @if( $building_age1->dd_building_age_id ==  $getBuyerData[0]->building_age ) selected @endif @endif @endif >{{ucwords($building_age1->building_age)}}</option>
                        @endforeach 

                        </select>
                        <label for="building_age" class="active">Building Age  </label>
                    </div>   
                   

                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"   name="floor_choice" id="floor_choice"  multiple="multiple">
                            <!-- <option value=""  selected>Select</option> -->
             

                                 @if(isset($getBuyerData))
                                       @if(isset($getBuyerData[0]->floor_choice ) && !empty($getBuyerData[0]->floor_choice ))
                                           @php $floor_choice2 = json_decode($getBuyerData[0]->floor_choice ); 
                                               $floor_choice2 = explode(",",$floor_choice2[0]);     
                                           @endphp
           
                                           @foreach($floor_choice as $floor_choice)
                                           <option value="{{$floor_choice->dd_floor_choice_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $floor_choice->dd_floor_choice_id,$floor_choice2)) selected  @endif  @endif >{{ucwords($floor_choice->floor_choice)}}</option>
                                           @endforeach
                                       @else
                                           @foreach($floor_choice as $floor_choice1)
                                               <option value="{{$floor_choice1->dd_floor_choice_id}}" >{{ucwords($floor_choice1->floor_choice)}}</option>
                                           @endforeach
           
                                       @endif
                                   @else
                                       @foreach($floor_choice as $floor_choice1)
                                           <option value="{{$floor_choice1->dd_floor_choice_id}}" >{{ucwords($floor_choice1->floor_choice)}}</option>
                                       @endforeach
                                       
                                   @endif  

                        </select>
                        <label for="floor_choice" class="active">Floor Choice    </label>
                    </div>   

                    <div class="input-field col l3 m4 s12 display_search">   
                                   
                        <select class="select2  browser-default" multiple="multiple"   name="door_facing_direction" id="door_facing_direction">
                        @if(isset($getBuyerData))
                            @if(isset($getBuyerData[0]->door_direction_id ) && !empty($getBuyerData[0]->door_direction_id ))
                                @php $door_direction_id2 = json_decode($getBuyerData[0]->door_direction_id ); 
                                    $door_direction_id2 = explode(",",$door_direction_id2[0]);     
                                @endphp

                                @foreach($direction as $direction)
                                <option value="{{$direction->direction_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $direction->direction_id,$door_direction_id2)) selected  @endif  @endif >{{ucwords($direction->direction)}}</option>
                                @endforeach
                            @else
                                @foreach($direction as $direction1)
                                    <option value="{{$direction1->direction_id}}" >{{ucwords($direction1->direction)}}</option>
                                @endforeach

                            @endif
                        @else
                            @foreach($direction as $direction1)
                                <option value="{{$direction1->direction_id}}" >{{ucwords($direction1->direction)}}</option>
                            @endforeach
                            
                        @endif  

                        
                        </select>
                        <label for="purpose" class="active">Door Direction</label>
                    </div>


                
                 </div>
                 

                 <div class='row'>

                 <div class="input-field col l3 m4 s12 display_search">   
                                   
                                   <select class="select2  browser-default" multiple="multiple"   name="balcony" id="balcony">
                                   @if(isset($getBuyerData))
                                       @if(isset($getBuyerData[0]->balcony_id ) && !empty($getBuyerData[0]->balcony_id ))
                                           @php $balcony_id2 = json_decode($getBuyerData[0]->balcony_id ); 
                                               $balcony_id2 = explode(",",$balcony_id2[0]);     
                                           @endphp
           
                                           @foreach($balcony as $balcony)
                                           <option value="{{$balcony->balcony_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $balcony->balcony_id,$balcony_id2)) selected  @endif  @endif >{{ucwords($balcony->balcony)}}</option>
                                           @endforeach
                                       @else
                                           @foreach($balcony as $balcony1)
                                               <option value="{{$balcony1->balcony_id}}" >{{ucwords($balcony1->balcony)}}</option>
                                           @endforeach
           
                                       @endif
                                   @else
                                       @foreach($balcony as $balcony1)
                                           <option value="{{$balcony1->balcony_id}}" >{{ucwords($balcony1->balcony)}}</option>
                                       @endforeach
                                       
                                   @endif  
           
                                   
                                   </select>
                                   <label for="purpose" class="active">Balcony</label>
                    </div>

                 <div class="input-field col l3 m4 s12 display_search">   
                                   
                                   <select class="select2  browser-default" multiple="multiple"   name="bathroom" id="bathroom">
                                   @if(isset($getBuyerData))
                                       @if(isset($getBuyerData[0]->bathroom_id ) && !empty($getBuyerData[0]->bathroom_id ))
                                           @php $bathroom_id2 = json_decode($getBuyerData[0]->bathroom_id ); 
                                               $bathroom_id2 = explode(",",$bathroom_id2[0]);     
                                           @endphp
           
                                           @foreach($bathroom as $bathroom)
                                           <option value="{{$bathroom->bathroom_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $bathroom->bathroom_id,$bathroom_id2)) selected  @endif  @endif >{{ucwords($bathroom->bathroom)}}</option>
                                           @endforeach
                                       @else
                                           @foreach($bathroom as $bathroom1)
                                               <option value="{{$bathroom1->bathroom_id}}" >{{ucwords($bathroom1->bathroom)}}</option>
                                           @endforeach
           
                                       @endif
                                   @else
                                       @foreach($bathroom as $bathroom1)
                                           <option value="{{$bathroom1->bathroom_id}}" >{{ucwords($bathroom1->bathroom)}}</option>
                                       @endforeach
                                       
                                   @endif  
           
                                   
                                   </select>
                                   <label for="purpose" class="active">Bathrooom</label>
                    </div>
                     
                    
                    <div class="input-field col l3 m4 s12 display_search">         
                               
                               <select class="select2  browser-default"  multiple="multiple"  name="parking_type" id="parking_type">
                                   @if(isset($getBuyerData))
                                       @if(isset($getBuyerData[0]->parking_type_id ) && !empty($getBuyerData[0]->parking_type_id ))
                                           @php $parking_type_id2 = json_decode($getBuyerData[0]->parking_type_id ); 
                                               $parking_type_id2 = explode(",",$parking_type_id2[0]);     
                                           @endphp
                                           @foreach($parking as $parking)
                                           <option value="{{$parking->parking_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $parking->parking_id,$parking_type_id2)) selected  @endif  @endif >{{ucwords($parking->parking_name)}}</option>
                                           @endforeach
                                       @else
                                           @foreach($parking as $parking)
                                               <option value="{{$parking->parking_id}}" >{{ucwords($parking->parking_name)}}</option>
                                           @endforeach
                                       @endif
                                   @else
                                       @foreach($parking as $parking)
                                           <option value="{{$parking->parking_id}}" >{{ucwords($parking->parking_name)}}</option>
                                       @endforeach
                                   @endif     
                                   
                               
                               </select>
                               <label for="possession_year" class="active">Parking Type </label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <select class="select2  browser-default" multiple="multiple"   name="purpose" id="purpose1">
                        @if(isset($getBuyerData))
                            @if(isset($getBuyerData[0]->purpose_id ) && !empty($getBuyerData[0]->purpose_id ))
                                @php $purpose_id2 = json_decode($getBuyerData[0]->purpose_id ); 
                                    $purpose_id2 = explode(",",$purpose_id2[0]);     
                                @endphp
                                @foreach($purpose as $purpose2)
                                    <option value="{{$purpose2->dd_purpose_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $purpose2->dd_purpose_id,$purpose_id2)) selected  @endif  @endif  >{{ ucwords($purpose2->purpose)}}</option>
                                @endforeach
                                @else
                                @foreach($purpose as $purpose2)
                                    <option value="{{$purpose2->dd_purpose_id}}" >{{ ucwords($purpose2->purpose)}}</option>
                                @endforeach
                            @endif
                        @else      
                            @foreach($purpose as $purpose2)
                                <option value="{{$purpose2->dd_purpose_id}}" >{{ ucwords($purpose2->purpose)}}</option>
                            @endforeach
                        @endif   
                                
                            </select>
                            <label for="purpose" class="active">Purpose
                            </label>
                        </div>  

                   
                </div>

            </div>

            <!-- <div class="collapsible-body"  id='purpose' style="display:none">
                                                
              
            </div> -->

        
        </div>

                                            

     <!-- Modal Location Structure -->
    <div id="modal3" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Location </h5>
        <hr>
        
        <form method="post" id="add_location_form">
                <div class="row" style="margin-right: 0rem !important">
                    <div class="input-field col l3 m3 s12 display_search">
                        <label  class="active">State.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="state_name" placeholder="Enter"  >
                    </div>

                    
                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">City: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_city"   placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Location: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_location"  placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Sub Location: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_sub_location"   placeholder="Enter" >
                    </div>
                </div> 
                
                
                <div class="alert alert-danger print-error-msg_add_location" style="display:none">
                <ul style="color:red"></ul>
                </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="add_location_btn()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
                
                
            </div>
        </form>
    </div>

<script>
    function ChekwithMinCarpet() {
     
        var max = $('#maximum_carpet_area').val();
        alert(max)
        var min = $('#minimum_carpet_area').val();
        if(min = max){
           
            // alert('Maximum carpet area should be greater than or equal to minimum carpet area');
                $('#max_carpet_error').html('Maximum carpet area should be greater than or equal to minimum carpet area');
                $('#maximum_carpet_area').val('').trigger('change');
           
        }else{
            $('#max_carpet_error').html('');
        }
        
    }
</script>
<!-- <input type="text" id="mbi" > -->

<script>
    $(document).ready(function(){

        
    // onleadbyChange();
    // hideShowOnunitStatus();
    // onunitStatusChange();
    var unitsSel = $('#unitsSel').val();
    if(unitsSel !='' || typeof unitsSel != 'undefined' || unitsSel != null){   
        // alert(1)
        var numbersArray = unitsSel.split(',');           
        $('#units').val(numbersArray).trigger('change');
    }  


    var ConfigSel = $('#ConfigSel').val();
    var minCarpetSel = $('#minCarpetSel').val();
    var maxCarpetSel = $('#maxCarpetSel').val(); 
    
    if( ConfigSel != null || ConfigSel !='' || typeof ConfigSel != 'undefined' ){      
        if(typeof ConfigSel != 'undefined'){
            console.log(ConfigSel);
            var numbersArray = ConfigSel.split(',');   
            $('.configuration').val(numbersArray).trigger("change");
            hideShowMinCarpet(minCarpetSel,maxCarpetSel); 
        }
        
            
    }

    
                      

   

    });


</script>