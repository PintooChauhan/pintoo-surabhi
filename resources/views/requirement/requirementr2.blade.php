<meta name="csrf-token" content="{{ csrf_token() }}">

@if(isset($getSpecificChoiceData))  
                @if(isset($getSpecificChoiceData[0]->building_amenities ) && !empty($getSpecificChoiceData[0]->building_amenities )) 

                    <?php  
                            $decodeEle4 = json_decode($getSpecificChoiceData[0]->building_amenities) ;
                            if(!empty($decodeEle4)){

                                
                                if(!empty($decodeEle4[0])){
                                $exp4 = explode(',',$decodeEle4[0]);
                              
                                foreach($exp4 as $val4){ ?>
                                    <script>
                                        // $(document).ready(function(){
                                            blankArry3.push(<?= $val4 ?>);
                                            // alert(blankArry1);
                                        // });
                                       // alert(blankArry3);
                                    </script>
                                <?php }
                                    }
                                }
                            ?>
                @endif
            @endif

            @if(isset($getSpecificChoiceData))  
                @if(isset($getSpecificChoiceData[0]->unit_amenities ) && !empty($getSpecificChoiceData[0]->unit_amenities )) 
                    <?php
                            $decodeEle5 = json_decode($getSpecificChoiceData[0]->unit_amenities) ;
                            if(!empty($decodeEle5)){
                                if(!empty($decodeEle5[0])){
                                $exp5 = explode(',',$decodeEle5[0]);
                                foreach($exp5 as $val5){ ?>
                                    <script>
                                        // $(document).ready(function(){
                                            blankArry4.push(<?= $val5 ?>);
                                            // alert(blankArry4);
                                        // });

                                    </script>
                                <?php }
                                    }
                                }
                            ?>
                @endif
            @endif

        <div class="row">                                        
                           
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="looking_since()">
                        <div class="collapsible-header" id="col_looking_since"> Looking Since & Property Seen</div>
                        
                        </li>                                        
                    </ul>
                </div>

                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="finalization()">
                        <div class="collapsible-header" id='col_finalization'>Finalization</div>
                        
                        </li>                                        
                    </ul>
                </div>

                <div class="col l3 s12"  >
                        <ul class="collapsible" >
                            <li onClick="address()" >
                            <div class="collapsible-header" id='col_address'>F2F & Address</div>
                            
                            </li>                                        
                        </ul>
                </div>

                
                <div class="col l3 s12" style="display:none !important">
                    <ul class="collapsible">
                        <li onClick="home_loan()" id="home_loan_li">
                        <div class="collapsible-header" id='col_home_loan'>Loan Status</div>
                        
                        </li>                                        
                    </ul>
                </div>

                
                           
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="society_amenities_unit()">
                        <div class="collapsible-header" id="col_society_amenities_unit">Unit & Building Amenities  </div>
                        
                        </li>                                        
                    </ul>
                </div>     
                
        </div>


        <div class="collapsible-body"  id='home_loan' style="display:none">
        <?php $m=$min=$o=0; ?>
        @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->max_budget) ) @php $m = $getBuyerData[0]->max_budget ; @endphp   @endif @else @php $m = 0; @endphp  @endif
        @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->min_budget) ) @php $min = $getBuyerData[0]->min_budget ; @endphp   @endif @else @php $min = 0; @endphp  @endif
        @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->own_contribution_amount) ) @php $o = $getBuyerData[0]->own_contribution_amount ; @endphp  @endif @else @php $o = 0; @endphp @endif
        <div class="row">

            <div class=" col l3 m4 s12 ">
                <span  style="font-weight: 600">Loan Amount: <span id="loan_amount"  style="display:none">
                    @if(isset($getBuyerData) && !empty($getBuyerData)) @php print_r((int)$m -(int)$o);  @endphp @endif</span>  
                        <!-- ( <span id="loan_amt_in_words1"></span> )  -->
                </span>
                <span  style="font-weight: 600" id="loan_amount_total_amount_words"></span> 
            </div>

            <div class=" col l3 m4 s12 ">
            <span  style="font-weight: 600">Minimum Budget:<span id="loan_amount_min" style="display:none">
                 @if(isset($getBuyerData) && !empty($getBuyerData)) @php print_r((int)$min );  @endphp @endif</span>  
                   
            </span>
             <span  style="font-weight: 600" id="loan_amount_min_amount_words"></span> 
            </div>
            <div class=" col l3 m4 s12 ">
            <span  style="font-weight: 600">Maximum Budget: <span id="loan_amount_max"  style="display:none">
                 @if(isset($getBuyerData) && !empty($getBuyerData)) @php print_r((int)$m );  @endphp @endif</span>  
                    <!-- ( <span id="loan_amt_in_words1"></span> )  -->
            </span> 

             <span  style="font-weight: 600" id="loan_amount_max_amount_words"></span> 
            </div>

              <div class=" col l3 m4 s12 ">

                <span  style="font-weight: 600">Own Contribution Amount: <span id="loan_amount_own"  style="display:none">
                    @if(isset($getBuyerData) && !empty($getBuyerData)) @php print_r((int)$o);  @endphp @endif</span>  
                        <!-- ( <span id="loan_amt_in_words1"></span> )  -->
                </span> 
                 <span  style="font-weight: 600" id="loan_amount_own_amount_words"></span> 
            </div>
            
            
        </div>   
        
       
           
          

         </div>

        
       

        
        <div class="collapsible-body"  id='finalization' style="display:none">
             <div class="row">
                      
                      <div class="input-field col l3 m4 s12 display_search">
                                       

                          <select class="select2  browser-default" multiple="multiple"   name="finalization_month" id="finalization_month" onChange="finalization_month_ar()">          
                           
                          @if(isset($getBuyerData))
                                @if(isset($getBuyerData[0]->finalization_month  ) && !empty($getBuyerData[0]->finalization_month  ))
                                    @php $finalization_month2 = json_decode($getBuyerData[0]->finalization_month  ); 
                                        $finalization_month2 = explode(",",$finalization_month2[0]);            
                                    @endphp
                                    @for($i=0; $i<=5;$i++)
                                    @php $date =date('M Y',strtotime('first day of +'.$i.' month'));   @endphp
                                        <option value="<?php echo $date;?>"  @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array($date,$finalization_month2)) selected  @endif  @endif ><?php echo date('M Y',strtotime('first day of +'.$i.' month')); ?> </option> 
                                    @endfor 
                                    @else
                                    @for($i=0; $i<=5;$i++)
                                        @php $date =date('M Y',strtotime('first day of +'.$i.' month'));   @endphp
                                            <option value="<?php echo $date;?>"   ><?php echo date('M Y',strtotime('first day of +'.$i.' month')); ?> </option> 
                                        @endfor  
                                @endif
                            @else
                                @for($i=0; $i<=5;$i++)
                                @php $date =date('M Y',strtotime('first day of +'.$i.' month'));   @endphp
                                    <option value="<?php echo $date;?>"   ><?php echo date('M Y',strtotime('first day of +'.$i.' month')); ?> </option> 
                                @endfor 
                            @endif   

                            
                                               
                              
                          </select>
                          <label for="purpose" class="active"> Finalization Month  </label>
                          <span> Why do you want to finalize by this month?</span>
                      </div>
  
                     

                      <div class="input-field col l3 m4 s12" >  
                        <label for="lead_assign" class="active">Reason for Finalization : <span class="red-text">*</span></label>
                            @php $reason_for_finalization=''; @endphp
                            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->reason_for_finalization) && !empty($getBuyerData[0]->reason_for_finalization)) @php $reason_for_finalization = $getBuyerData[0]->reason_for_finalization; @endphp @endif @endif                            
                        <input type="text"  name="reason_for_finalization" id="reason_for_finalization" placeholder="Enter" value="{{$reason_for_finalization}}" title="{{$reason_for_finalization}}">
                    </div>
                    <!-- <div class="input-field col l6 m6 s12" >
                        <span>
                        <b> W1</b> What is that they are looking in property? ,<b> W4</b>  Where will they buy ?  <br>
                        <b> W2</b> Why they will finalize by ___ (Display Month)? , <b>W5</b>  When they will buy ?  <br>                                                            
                        <b> W3</b> Who is the final decision maker(Name anyone)? ,<b>H1</b> How this will be done ?
                        </span>    
                    </div> -->
                     
            </div>
        </div>



        <div class="collapsible-body"  id='address' style="display:none">
                    <div class="row">

                         <div class="input-field col l3 m4 s12 display_search">                
                            <select class="select2  browser-default"  onChange="show_rent_amt()" name="current_residence_status" id="current_residence_status">
                                <option value="" selected >Select </option>
                                <option value="owned" @if( isset($getBuyerData[0]->current_residential_status) ) @if( $getBuyerData[0]->current_residential_status == "owned"  ) selected @endif @endif>Owned</option>
                                <option value="rented"  @if( isset($getBuyerData[0]->current_residential_status) ) @if( $getBuyerData[0]->current_residential_status == "rented"  ) selected @endif @endif>Rented</option>
                            </select>
                            <label for="purpose" class="active"> Current Residence Status  </label>
                        </div>  

                        @if(isset($getBuyerData) && !empty($getBuyerData))  
                            @if( isset($getBuyerData[0]->current_residential_status) && !empty($getBuyerData[0]->current_residential_status) ) 
                                @if($getBuyerData[0]->current_residential_status == 'rented')  
                                    <div class="input-field col l3 m4 s12 rntamt">                    
                                       
                                        <select  id="rent_amount" name="rent_amount"  class="select2 browser-default">
                                            <option value=""  selected>Select</option>
                                            @foreach($minimum_rent as $minimum_rent1)
                                            <option value="{{$minimum_rent1->minimum_rent}}"  @if(isset($getCustomerData) && !empty($getCustomerData)) @if( isset($getCustomerData[0]->rent_amount) ) @if(  $minimum_rent1->minimum_rent == $getCustomerData[0]->rent_amount  ) selected @endif @endif @endif >{{$minimum_rent1->minimum_rent_in_words}}</option>
                                            @endforeach
                                            
                                        </select>
                                        <label for="wap_number" class="active">Rent Amount : </label>
                                    </div>
                                @endif 

                                @else
                                    <div class="input-field col l3 m4 s12 rntamt"  style="display:none"  >                    
                                     
                                        <select  id="rent_amount" name="rent_amount"  class="select2 browser-default">
                                            <option value=""  selected>Select</option>
                                            @foreach($minimum_rent as $minimum_rent1)
                                            <option value="{{$minimum_rent1->minimum_rent}}"  >{{$minimum_rent1->minimum_rent_in_words}}</option>
                                            @endforeach
                                            
                                        </select>
                                        <label  class="active">Rent Amount : </label>
                                        
                                    </div>
                            @endif 
                        @else
                        <div class="input-field col l3 m4 s12 display_search"> </div>
                                    <div class="input-field col l3 m4 s12 rntamt"  style="display:none"  >                    
                                       
                                        <select  id="rent_amount" name="rent_amount"  class="select2 browser-default">
                                            <option value=""  selected>Select</option>
                                            @foreach($minimum_rent as $minimum_rent1)
                                            <option value="{{$minimum_rent1->minimum_rent}}"  >{{$minimum_rent1->minimum_rent_in_words}}</option>
                                            @endforeach
                                            
                                        </select>
                                        <label  class="active">Rent Amount : </label>
                                        
                                    </div>
                            
                        @endif 
                       

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"   name="f2f_done" id="f2f_done">
                                <option value=""  selected>Select</option>
                                <option value="yes"  @if(isset($getBuyerData))@if(isset($getBuyerData[0]->f2f_done) && !empty($getBuyerData[0]->f2f_done)) @if($getBuyerData[0]->f2f_done == 'yes') selected @endif @endif  @endif >Yes</option>
                                <option value="no" @if(isset($getBuyerData))@if(isset($getBuyerData[0]->f2f_done) && !empty($getBuyerData[0]->f2f_done)) @if($getBuyerData[0]->f2f_done == 'no') selected @endif @endif  @endif >No</option>
                               
                            </select>
                            <label for="possession_year" class="active"> F2F Done </label>
                           
                        </div>

                    </div>

                    <div class="row">
                        <div class="input-field col l3 m4 s12" >  
                            <label for="lead_assign" class="active">Current Residence Address: </label>
                            @php $customer_res_address=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->customer_res_address) && !empty($getCustomerData[0]->customer_res_address)) @php $customer_res_address = $getCustomerData[0]->customer_res_address; @endphp @endif @endif                         
                            <input type="text" name="current_residence_address" id="current_residence_address" placeholder="Enter" value="{{$customer_res_address}}" title="{{$customer_res_address}}">
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  name="current_residence_city" id="current_residence_city">
                                <option value=""  selected>Select</option>
                                @foreach($city as $city2)
                                <option value="{{$city2->city_id}}"  @if( isset($getCustomerData[0]->customer_city) ) @if( $city2->city_id == $getCustomerData[0]->customer_city  ) selected @endif @endif >{{ucwords($city2->city_name)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">City </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                            <a href="#modal5" id="add_cra_city" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  name="current_residence_state" id="current_residence_state">
                                @foreach($state as $state2)
                                  <option value="{{$state2->dd_state_id}}" @if( isset($getCustomerData[0]->customer_state) ) @if( $state2->dd_state_id == $getCustomerData[0]->customer_state  ) selected @endif @endif >{{ucwords($state2->state_name)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">State </label>
                        </div>

                        <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">Pincode : </label>
                            @php $customer_pin_code=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->customer_pin_code) && !empty($getCustomerData[0]->customer_pin_code)) @php $customer_pin_code = $getCustomerData[0]->customer_pin_code; @endphp @endif @endif
                            <input type="text" onkeypress="return onlyNumberKey(event)" name="current_residence_pincode" id="current_residence_pincode"  Placeholder="Enter" value="{{$customer_pin_code}}">
                        </div>
                    </div>
                  <div class="row">                

                        <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">Company Name  : </label>
                            @php $customer_company_name_address=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->customer_company_name_address) && !empty($getCustomerData[0]->customer_company_name_address)) @php $customer_company_name_address = $getCustomerData[0]->customer_company_name_address; @endphp @endif @endif
                            <input type="text"  name="company_name" id="company_name"  Placeholder="Enter" value="{{$customer_company_name_address}}" title="{{$customer_company_name_address}}">
                        </div>


                        <div class="input-field col l3 m4 s12 display_search" style="display:none">                    
                            <select class="select2  browser-default"  name="company_city" id="company_city">
                                <option value=""  selected>Select</option>
                                @foreach($city as $city1)
                                <option value="{{$city1->city_id}}"  @if( isset($getCustomerData[0]->company_city) ) @if( $city1->city_id == $getCustomerData[0]->company_city  ) selected @endif @endif >{{ucwords($city1->city_name)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">City </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                                <a href="#modal6" id="add_com_city" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search" style="display:none">                    
                            <select class="select2  browser-default"  name="company_state" id="company_state">
                                @foreach($state as $state1)
                                  <option value="{{$state1->dd_state_id}}"  @if( isset($getCustomerData[0]->company_state) ) @if($state1->dd_state_id == $getCustomerData[0]->company_state  ) selected @endif @endif >{{ucwords($state1->state_name)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">State </label>                         
                        </div>

                        <div class="input-field col l3 m4 s12" style="display:none">                    
                            <label for="wap_number" class="active">Pincode : </label>
                            @php $company_pincode=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->company_pincode) && !empty($getCustomerData[0]->company_pincode)) @php $company_pincode = $getCustomerData[0]->company_pincode; @endphp @endif @endif
                            <input type="text" onkeypress="return onlyNumberKey(event)"   name="company_pincode" id="company_pincode"   Placeholder="Enter"  value="{{$company_pincode}}">
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  name="designation" id="designation">
                                <option value=""  selected>Select</option>
                                @foreach($designation as $designation)
                                <option value="{{$designation->dd_designation1_id}}"  @if( isset($getCustomerData[0]->customer_designation_id) ) @if( $designation->dd_designation1_id == $getCustomerData[0]->customer_designation_id  ) selected @endif @endif >{{ucwords($designation->designation)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">Designation </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                            <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  name="industry_name" id="industry_name">
                                <option value=""  selected>Select</option>
                                @foreach($industry as $industry)
                                <option value="{{$industry->dd_industry_id}}"   @if( isset($getCustomerData[0]->industry) ) @if( $industry->dd_industry_id == $getCustomerData[0]->industry  ) selected @endif @endif>{{ucwords($industry->industry)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">Industry </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                            <a href="#modal10" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div>
                        </div>
                    

                        

                        
                        <div class="input-field col l3 m4 s12" >  
                            <label for="lead_assign" class="active">Information: </label>
                            @php $information=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->information) && !empty($getCustomerData[0]->information)) @php $information = $getCustomerData[0]->information; @endphp @endif @endif
                            <input type="text"  name="information" id="information" placeholder="Enter" value="{{$information}}" title="{{$information}}">
                        </div>


            </div>
            
           
        </div>

        <div class="collapsible-body"  id='society_amenities_unit' style="display:none">
            <div class="row">
            <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="unit_amenities1()">
                        <div class="collapsible-header" id='col_unit_amenities1'>   Unit Amenities  </div>
                        
                        </li>                                        
                    </ul>
                </div> 

                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="society_amenities1()">
                        <div class="collapsible-header" id="col_society_amenities1">Building Amenities  </div>
                        
                        </li>                                        
                    </ul>
                </div>
                       
            </div> 


            
            <div class="collapsible-body"  id='unit_amenities1' style="display:none">
                <div class='row'>
                <!-- <div class='col l1 m1 s2'>
                        
                    </div> -->
                    <div class='col l3 m3 s12'>
                        <div class="input-field col l12 m12 s12 display_search">                    
                                    <select class="select2  browser-default"  name="furnishing_status" id="furnishing_status">
                                        <option value=""  selected>Select</option>
                                        <option value="furnished"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->furnishing_status) && !empty($getBuyerData[0]->furnishing_status) ) @if($getBuyerData[0]->furnishing_status == 'furnished' ) selected  @endif @endif @endif>Furnished</option>
                                        <option value="unfurnished"   @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->furnishing_status) && !empty($getBuyerData[0]->furnishing_status) ) @if($getBuyerData[0]->furnishing_status == 'unfurnished' ) selected  @endif @endif @endif >Unfurnished</option>
                                        
                                        <!-- <option value="fully_furnished"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->furnishing_status) && !empty($getBuyerData[0]->furnishing_status) ) @if($getBuyerData[0]->furnishing_status == 'fully_furnished' ) selected  @endif @endif @endif>Fully-furnished</option> -->
                                    </select>
                                    <label for="possession_year" class="active">Furnishing Status </label>
                                    <div id="AddMoreFileId3" class="addbtn" style="position: unset !important">   
                            <a href="#modal11" id="unit_am" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                        </div>  
                        </div>
                        <!-- <div class="input-field col l12 m12 s12" >  
                            <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                            <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="unit_amenities_comment" rows='2' col='4'></textarea>
                        </div> -->
                    </div>
                    <div class='col l8 m8 s12' id="ume">
                        
                        @foreach($unit_amenities as $unit_amenities1)

                        @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) )

                            <?php 
                                    $decodeEle5 = json_decode($getSpecificChoiceData[0]->unit_amenities) ;
                                    // print_r($decodeEle5);
                                    if(isset($decodeEle5[0])){
                                    $exp5 = explode(',',$decodeEle5[0]);
                                    if(in_array($unit_amenities1->unit_amenities_id,$exp5)){?>
                                        <div id="ck1-button">
                                            <label>
                                                <input type="checkbox" checked onClick='addUnitAmenity({{$unit_amenities1->unit_amenities_id}})' value="{{$unit_amenities1->unit_amenities_id}}" id='unit_amenities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($unit_amenities1->amenities)}}</span>
                                            </label>
                                        </div>   
                                    <?php }else{ ?>
                                        <div id="ck1-button">
                                            <label>
                                                <input type="checkbox" onClick='addUnitAmenity({{$unit_amenities1->unit_amenities_id}})' value="{{$unit_amenities1->unit_amenities_id}}" id='unit_amenities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($unit_amenities1->amenities)}}</span>
                                            </label>
                                        </div>
                                    <?php } }else{ ?>
                                        <div id="ck1-button">
                                <label>
                                    <input type="checkbox" onClick='addUnitAmenity({{$unit_amenities1->unit_amenities_id}})' value="{{$unit_amenities1->unit_amenities_id}}" id='unit_amenities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($unit_amenities1->amenities)}}</span>
                                </label>
                            </div>
                                    <?php }
                                ?>
                            @else
                            <div id="ck1-button">
                                <label>
                                    <input type="checkbox" onClick='addUnitAmenity({{$unit_amenities1->unit_amenities_id}})' value="{{$unit_amenities1->unit_amenities_id}}" id='unit_amenities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($unit_amenities1->amenities)}}</span>
                                </label>
                            </div>
                            @endif

                    
                        @endforeach

                
                    </div>  
                    
                </div>
                
                
            </div>


            
            
            <div class="collapsible-body"  id='society_amenities1' style="display:none">
                
            <div class='row'>
                    <!-- <div class='col l3 m3 s12'>                    
                        <div class="input-field col l12 m12 s12" >  
                            <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                            <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="unit_amenities_comment" rows='2' col='4'></textarea>
                        </div>
                    </div> -->
                    <div class='col l1 m1 s2'>
                        <div id="AddMoreFileId3" class="addbtn" style="position: unset !important">   
                            <a href="#modal12" id="unit_am" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                        </div>   
                    </div> 
                    <div class='col l10 m10 s12' id="bame">
                        @foreach($society_amnities as $society_amnities1)

                        @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) )

                            <?php 
                                    $decodeEle4 = json_decode($getSpecificChoiceData[0]->building_amenities) ;
                                    // print_r($decodeEle4);
                                    if(isset($decodeEle4[0])){
                                    $exp4 = explode(',',$decodeEle4[0]);
                                    if(in_array($society_amnities1->project_amenities_id,$exp4)){?>
                                        <div id="ck-button">
                                            <label>
                                                <input type="checkbox" checked onClick='addBuildingAmenity({{$society_amnities1->project_amenities_id}})' value="{{$society_amnities1->project_amenities_id}}" id='society_amnities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($society_amnities1->amenities)}}</span>
                                            </label>
                                        </div>   
                                    <?php }else{ ?>
                                        <div id="ck-button">
                                            <label>
                                                <input type="checkbox" onClick='addBuildingAmenity({{$society_amnities1->project_amenities_id}})' value="{{$society_amnities1->project_amenities_id}}" id='society_amnities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($society_amnities1->amenities)}}</span>
                                            </label>
                                        </div>
                                    <?php } }else{ ?>
                                        <div id="ck-button">
                                <label>
                                    <input type="checkbox" onClick='addBuildingAmenity({{$society_amnities1->project_amenities_id}})' value="{{$society_amnities1->project_amenities_id}}" id='society_amnities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($society_amnities1->amenities)}}</span>
                                </label>
                            </div>
                                    <?php }
                                ?>
                            @else
                            <div id="ck-button">
                                <label>
                                    <input type="checkbox" onClick='addBuildingAmenity({{$society_amnities1->project_amenities_id}})' value="{{$society_amnities1->project_amenities_id}}" id='society_amnities1' ><span style="padding: 0px 10px 0px 10px;">{{ucwords($society_amnities1->amenities)}}</span>
                                </label>
                            </div>
                        @endif
                        
                    
                        @endforeach
                    
                    </div>
                    
                </div>
            </div>
           
        </div>

        <div class="collapsible-body"  id='looking_since' style="display:none">
            <input type="hidden" id="buyer_tenant_id1">                    
            <input type="hidden" id="property_seen_id">       
            <div class='row'>
                <div class="input-field col l3 m4 s12 display_search">
                
                     <select class="select2  browser-default"    name="looking_since" id="looking_since1">
                            <option selected  value="">Select</option>
                            @foreach($looking_since as $looking_since1)
                                 <option value="{{$looking_since1->looking_since_id}}" @if( isset($getBuyerData )) @if( isset($getBuyerData[0]->looking_since) ) @if( $looking_since1->looking_since_id == $getBuyerData[0]->looking_since  ) selected @endif @endif @endif>{{ ucwords($looking_since1->looking_since) }} </option> 
                             @endforeach 
                        </select>
                        <label for="looking_since" class="active">Looking Since
                        </label>
                </div>
                
                <div class="input-field col l3 m4 s12 " style="display:none !important">
                                <select class="select2  browser-default" onChange="property_seen_hide_show()" id="property_seen1"   name="property_seen">
                                    <option value=""  selected>Select</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                                <label for="property_seen" class="active">Property Seen
                                </label>
                </div>

                <div class="input-field col l3 m4 s12" id="InputsWrapper2">

                    <div class="row" >
                        <div class="input-field col l2 m2 s6 display_search">
                        <div class="preloader-wrapper small active" id="loader3" style="display:none">
                            <div class="spinner-layer spinner-green-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>

                        <div class="row" id="submitLooking">
                            <div class="col l4 m4">
                                <a href="javascript:void(0)" onClick="lookingSince()" class=" save_lookingSince btn-small  waves-effect waves-light green darken-1 " id="save_lookingSince">Save</a>
                            </div>
                            <div class="col l3 m3">
                                    <a href="javascript:void(0)" onClick="clear_lookingSince()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_lookingSince"><i class="material-icons dp48">clear</i></a>
                            </div>
                        </div>
                </div> 
                
            </div>
           
                <div class='row'>      
                        <!--  style="display:none" -->
                    <div class="input-field col l3 m4 s12 display_search" >
                         <select class="select2  browser-default"   name="unit_information" id="unit_information">
                                <option value=""  selected>Select</option>
                                    @foreach($society as $sVal)
                                  <?php 
                                    $ids = $sVal->project_id.'-'.$sVal->society_id;
                                    $bn ='';
                                    if($sVal->building_name != ''){
                                        $bn = ' - '.ucwords($sVal->building_name);
                                    }
                                  ?>
                                      <option value="{{$ids}}"  ><?php echo ucwords($sVal->project_complex_name).$bn ?></option>
                                  @endforeach 
                            </select>
                            <label for="visited_by" class="active">Complex/Building Name
                    </div>  

                    <div class="input-field col l3 m4 s12 display_search" >                                    
                        <div class="input-field col l6 m6 s12 display_search" >
                            <label for="loan_amt" id="loan_amt" class="active">Liked: </label>                           
                            <input type="text" class="validate" name="unit_information_like" Placeholder="Enter"  id='unit_information_like'  > 
                        </div>  


                        <div class="input-field col l6 m6 s12 display_search" >
                            <label for="loan_amt" id="loan_amt" class="active">Disliked: </label>                           
                            <input type="text" class="validate" name="unit_information_dislike" Placeholder="Enter"  id='unit_information_dislike'  > 
                        </div>  
                    </div>  

                    
                    <div class="input-field col l3 m4 s12 display_search"  >
                                <!-- onChange="property_seen_details_hide_show()"  -->
                            <select class="select2  browser-default"   name="visited_by" id="visited_by">
                                <option value=""  selected>Select</option>
                                <option value="property consultant">Property Consultant</option>
                                <option value="direct">Direct</option>
                              <option value="surabhi realtor">Surabhi realtor</option>
                              <!--     <option value="uc_direct">UC- Direct</option> -->
                            </select>
                            <label for="visited_by" class="active">Property seen through
                            </label>
                    </div> 

                    <div class="input-field col l3 m4 s12"  >                    
                        <label for="wap_number" class="active">Date of Visit: <span class="red-text">*</span></label>
                        <input type="text" class="datepicker" name="date_of_visit"  id="date_of_visit"  placeholder="Calender" style="border: 1px solid #c3bf20 !important;height: 41px;" onChange="countDays()">
                        <span id="calender_span" style="display:none;color: white; border: solid 1px green; background-color: green;padding: 1px 10px 2px 10px;"></span>
                        <input type="hidden" id="calender_span_val">
                        <!-- <span id="tat_within" style="display:none;border-radius: 3px;color: white; border: solid 1px red; background-color: red; padding: 1px 15px 2px 10px;"></span>
                        <span id="tat_after" style="display:none;border-radius: 3px;color: white; border: solid 1px green; background-color: green;padding: 1px 15px 2px 10px;">No of days left : 10</span> -->
                    </div> 
                </div>
                <div class='row'> 
                    
                    

                    <div class="input-field col l3 m4 s12" style="display:none !important" >  
                        <label for="lead_assign" class="active">Remark: <span class="red-text">*</span></label>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="property_seen_comment" rows='2' col='4'></textarea> -->
                        <input type="text" class="validate" name="physical_virtual" placeholder="Enter" id="physical_virtual">
                    </div>                    
                    
                </div>
             
            <br>
            <div class="row">
                <table class="bordered" id="looking_since_table" style="width: 100% !important" >
                    <thead>
                    <tr >
                    <th  style="color: white;background-color: #ffa500d4;">Sr No. </th>
                    <th  style="color: white;background-color: #ffa500d4;">Complex/Building Name</th>
                    <th  style="color: white;background-color: #ffa500d4;">Liked</th>
                    <th  style="color: white;background-color: #ffa500d4;">Disliked</th>
                    <th  style="color: white;background-color: #ffa500d4;">Property seen through</th>
                    <th  style="color: white;background-color: #ffa500d4;">Date of Visit</th>
                    <th  style="color: white;background-color: #ffa500d4;">No of Days</th>

                    <!-- <th  style="color: white;background-color: #ffa500d4;">Comment</th> -->
                    <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                    <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                    </tr>
                    </thead>
                    <tbody>
                    @php $incID1 = count($getPropertySeenData); @endphp
                    @php $i3=count($getPropertySeenData); @endphp
                             @foreach($getPropertySeenData as $val)
                            
                                 <tr id="psid_{{$val->property_seen_id}}">
                                 <td id="sno_{{$val->property_seen_id}}">@php $incID= $i3--; @endphp {{$incID}}</td> 
                                     <td id="ui_{{$val->property_seen_id}}">
                                        <?php
                                        if(isset($val->unit_information) && !empty($val->unit_information)) {
                                            $ex = explode("-", $val->unit_information);
                                             $pr  = $ex[0];

                                             $prn = DB::select("select project_complex_name from op_project where project_id=".$pr." ");
                                            $fprn = $prn[0]->project_complex_name;
                                            $fbun = '';
                                             if(isset( $ex[1]) && !empty( $ex[1])){
                                                $bu  = $ex[1];
                                                $bun = DB::select("select building_name from op_society where society_id=".$bu." ");
                                                  $fbun = '-'. $bun[0]->building_name;
                                             }
                                           echo  $fprn.$fbun;
                                        }
                                        ?> 

                                     </td>
                                     <td id="li_{{$val->property_seen_id}}">{{$val->likes}}</td>
                                     <td id="dli_{{$val->property_seen_id}}">{{$val->dislike}}</td>
                                     <td id="pvb_{{$val->property_seen_id}}">{{$val->property_visited_by}}</td>
                                     <td id="dov_{{$val->property_seen_id}}">{{$val->date_of_visit}}</td>
                                     <td id="nod_{{$val->property_seen_id}}">{{$val->no_of_days}}</td>
                                     
                                     <td id="lud_{{$val->property_seen_id}}">{{$val->created_date}}</td>
                                     <td>
                                        <a href="javascript:void(0)"  class="waves-effect waves-light " onClick="updateLookingSince({{$val->property_seen_id}})" >Edit</a> |
                                        <a href='javascript:void(0)'  onClick="confirmDelLookingSince({{$val->property_seen_id}})"   >Delete</a>
                                     </td>
                                 </tr>   
                             @endforeach
                    </tbody>
                </table>
                <input type="hidden" id="incID1" value="{{$incID1}}">
          
            </div>
</div>

        

     
        

    <!-- Modal Unit Information Structure -->
    <div id="modal4" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Unit Information </h5>
        <hr>
        
        <form method="post" id="add_unit_information">
                <div class="row" style="margin-right: 0rem !important">
                    <div class="input-field col l3 m3 s12 display_search">
                        <label  class="active">New Building.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="building" placeholder="Enter"  >
                    </div>

                    <div class="input-field col l3 m3 s12 display_search">
                        <label  class="active">Complex.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="complex" placeholder="Enter"  >
                    </div>   

                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Sub Location: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_sub_location"   placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Location: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_location"  placeholder="Enter" >
                    </div>
                </div> 
                <div class="row" style="margin-right: 0rem !important">
                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">City: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_city"   placeholder="Enter" >
                    </div>
                    <div class="input-field col l3 m3 s12 display_search">
                        <label  class="active">State.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="state_name" placeholder="Enter"  >
                    </div>                
                </div>
                    
                
                
                
                <div class="alert alert-danger print-error-msg_add_unit_information" style="display:none">
                <ul style="color:red"></ul>
                </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="add_unit_information_btn()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
                
                
            </div>
        </form>
    </div>


    
    <!-- Modal Unit amenities -->
    <div id="modal12" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Building Amenities</h5>
        <hr>
        
        <form method="post" id="add_building_amenity_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Amenities: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_building_amenity" id="add_building_amenity"   placeholder="Enter">
                    <span class="add_building_amenity_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_building_amenity" style="display:none">
            <ul style="color:red"></ul>
            </div>
            <span id="buildingerr" style="color:red"></span>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_building_amenity_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>

     <!-- Modal Building  amenities -->
     <div id="modal11" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Unit Amenities</h5>
        <hr>
        
        <form method="post" id="add_unit_amenity_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Amenities: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                    <span class="add_unit_amenity_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_unit_amenity" style="display:none">
                 <ul style="color:red"></ul>
            </div>
            <span id="uniterr" style="color:red"></span>
            </div>
           
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_unit_amenity1()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>
    <script>
                function apply_css1(skip,attr, val=''){   
                var id =returnColArray1();           
                id.forEach(function(entry) {
                    
                    if(entry==skip){
                        // alert(11)
                        $('#'+skip).css(attr,'orange','!important');
                        // $('#'+skip).attr('style', 'background-color: orange !important');
                    }else{            
                        if($('#'+entry).css('pointer-events') == 'none'){

                        }else{
                            $('#'+entry).css(attr,val);
                        }
                        
                        //action_to_hide();            
                        
                    }        
                });
                }

        function hide_n_show1(skip){
        var id = collapsible_body_ids1();
        collapsible_body_ids1().forEach(function(entry) {
            // console.log(id);
            if(entry==skip){
                // alert(skip);
                var x = document.getElementById(skip);  
                $('#'+skip).css('background-color','rgb(234 233 230)');
                // alert(x)          
                if (x.style.display === "none") {
                    // alert(1)
                    
                    x.style.display = "block";
                } else {
                    // alert(2)
                    // alert(skip);
                    // $('#col_lead_info').removeAttr("style");
                    // $('#col_lead_info').css('background-color','white',"!important");
                    x.style.display = "none";
                    $('#col_'+skip).css('background-color','white');

                }
            }else{          
                $('#'+entry).hide();
            }
            
        });
        }

        function returnColArray1(){
        var a = Array('col_unit_amenities1','col_society_amenities1');
        return a;
        }

        function collapsible_body_ids1(){
        var b = Array('unit_amenities1','society_amenities1');
        return b;
        }

        function unit_amenities1(){    
        apply_css1('col_unit_amenities1','background-color','');
        hide_n_show1("unit_amenities1");     
        }

        function society_amenities1(){    
            apply_css1('col_society_amenities1','background-color','');
            hide_n_show1("society_amenities1");     
        }


        </script>
<script>

function execute1(url,form,err){
    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(url);
            console.log(data);// return;
            if($.isEmptyObject(data.error)){
                $('#uniterr').html('');
                alert(data.success);
                var html= data.resp;
                        $('#ume').append( html);
                        $('#modal11').modal('close');
                        $('#unit_amenity').val('');
                // location.reload();
            }else{
                if(data.error=='0'){
                    $('#uniterr').html('Already Exist');
                    return false;
                }
                $('#uniterr').html('');
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
}

    function add_unit_amenity1(){
    var url = 'addUnitAmenity';
    var form = 'add_unit_amenity_form';
    var err = 'print-error-msg_add_unit_amenity';
    execute1(url,form,err);
}

function execute2(url,form,err){
    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(url);
            console.log(data);// return;
            if($.isEmptyObject(data.error)){
                $('#buildingerr').html('');
                alert(data.success);
                var html= data.resp;
                        $('#bame').append( html);
                        $('#modal12').modal('close');
                        $('#add_building_amenity').val('');
                // location.reload();
            }else{
                if(data.error=='0'){
                    $('#buildingerr').html('Already Exist');
                    return false;
                }
                $('#buildingerr').html('');
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
}

function add_building_amenity_form(){
    var url = 'addBuildingAmenity';
    var form = 'add_building_amenity_form';
    var err = 'print-error-msg_add_building_amenity';
    execute2(url,form,err);
}
</script>

    

    <div class="modal" id="deleteLookingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-content">
               <h5 style="text-align: center">Delete record</h5>
               <hr>        
               <form method="post" id="delete_looking" >
                  <input type="hidden" class="property_seen_id2" id="property_seen_id2" name="property_seen_id">
                  <input type="hidden" class="form_type" id="form_type" name="form_type" value="property_seen">
                  <div class="modal-body">
                     
                     <h5>Are you sure want to delete this record?</h5>
                  
                  </div>
            
                   </div>
            
               <div class="modal-footer" style="text-align: left;">
                     <div class="row">
                     <div class="input-field col l2 m2 s6 display_search">
                           <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                           <a href="javascript:void(0)" onClick="deleteLookingSince()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                     </div>    
                     
                     <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                           <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                     </div>    
                  </div> 
               </div>
           </form>
    </div>