
    <div class="row">                 
            
      

       

     

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="brokerage_fee()" id="brokerage_fee_li">
                <div class="collapsible-header" id='col_brokerage_fee'>  Brokerage fees</div>
                
                </li>                                        
            </ul>
        </div>
    </div> 
       

       






<div class="collapsible-body"  id='brokerage_fee' style="display:none">
    <div class="row">

           

            <div class="input-field col l3 m4 s12" >  
                <label for="lead_assign" class="active">Brokerage fees: </label>
                @php $brokerage_fees=''; @endphp
                @if(isset($getBuyerData))@if(isset($getBuyerData[0]->brokerage_fees) && !empty($getBuyerData[0]->brokerage_fees)) @php $brokerage_fees = $getBuyerData[0]->brokerage_fees; @endphp @endif @endif
                <input type="text"  name="brokerage_fees" id="brokerage_fees" placeholder="Enter"  value="{{$brokerage_fees}}">
                <!-- <span id="brokerage_fees_words"></span>                                 -->

            </div> 

            <div class="input-field col l3 m4 s12" >  
                <label for="lead_assign" class="active">Comment: </label>
                @php $brokerage_fees_comment=''; @endphp
                @if(isset($getBuyerData))@if(isset($getBuyerData[0]->brokerage_fees_comment) && !empty($getBuyerData[0]->brokerage_fees_comment)) @php $brokerage_fees_comment = $getBuyerData[0]->brokerage_fees_comment; @endphp @endif @endif
                <input type="text"  name="brokerage_fees_comment"  id="brokerage_fees_comment" placeholder="Enter" value="{{$brokerage_fees_comment}}" title="{{$brokerage_fees_comment}}">
            </div> 

            <div class="input-field col l6 m6 s12" style="margin-top: 10px;">  
                <span style="margin-left: 20px;">Are you aware what is the standard industry norms for the brokerage fees?</span>
            </div>

    </div>
</div>




     <!-- Modal Company address - city Structure -->
     <div id="modal5" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add City</h5>
        <hr>
        
        <form method="post" id="add_res_city_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">City: </label>
                    <input type="text" class="validate" name="add_res_city" id="add_res_city"   placeholder="Enter">
                    <span class="add_res_city_err"></span>
                    
                </div>
                <div class="input-field col l3 m4 s12 ">                        
                    <select  id="add_res_state" name="add_res_state" placeholder="Select" class="select2  browser-default" >

                        <option selected disabled>Select State</option>
                         @foreach($state as $st)   
                        <option value={{$st->dd_state_id}}>{{ucfirst($st->state_name)}}</option>
                        @endforeach
                    </select>
                    <label class="active">State</label>                
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_res_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_res_city_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div>    

            </div>
        </form>
    </div>

    <!-- Modal Company address - city Structure -->
    <div id="modal6" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add City</h5>
        <hr>
        
        <form method="post" id="add_Company_city_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">City: </label>
                    <input type="text" class="validate" name="add_company_city" id="add_company_city"   placeholder="Enter">
                    <span class="add_company_city_err"></span>
                    
                </div>
                <div class="input-field col l3 m4 s12 ">                        
                    <select  id="add_company_state" name="add_company_state" placeholder="Select" class="select2  browser-default" >
                    <option selected disabled>Select State</option>
                         @foreach($state as $st)   
                        <option value={{$st->dd_state_id}}>{{ucfirst($st->state_name)}}</option>
                        @endforeach
                    </select>
                    <label class="active">State</label>                
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_company_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_Company_city_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div>    
            </div>
        </form>
    </div>

      <!-- Modal Designation Structure -->
      <div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation: </label>
                    <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>


      <!-- Modal Industry Name Structure -->
      <div id="modal10" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Industry Name</h5>
        <hr>
        
        <form method="post" id="add_industry_form1">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Industry Type: </label>
                    <input type="text" class="validate" name="add_industry_type" id="add_industry_type"   placeholder="Enter">
                    <span class="add_industry_name_err"></span>                    
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Industry Name: </label>
                    <input type="text" class="validate" required name="add_industry_name" id="add_industry_name"   placeholder="Enter">
                    <span class="add_industry_name_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_industry_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_industry_form1()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>

<script>

function add_industry_form1(){

    if($('#add_industry_name').val() == ''){
        alert('Enter Industry name');
        return false;
    }
    var url = 'addIndustry';
    var form = 'add_industry_form1';
    var err = 'print-error-msg_add_industry_name';
    execute1(url,form,err);
}



function execute1(url,form,err){
    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(url);
            
            var d = data.data;
            console.log(d.dd_industry_id);// return;
            if(d.industry_type != 'null' && d.industry_type != '' ){
                var l = d.industry_type +'-'+d.industry
            }else{
                var l = d.industry
            }
            $('#industry_name').append('<option value="'+d.dd_industry_id+'" selected> '+l+'</option>'); 
            $('#add_industry_name').val('');
            $('#add_industry_name').val('');
            $('#modal10').modal('close');
        }
    });    
    
    
}

function add_Company_city_form(){
    // alert($('#add_company_state').val());
    if($('#add_company_state').val() == null ){
        alert('Select state');
        return false;
    }
    var url = 'addCompanyCity';
    var form = 'add_Company_city_form';
    var err = 'print-error-msg_add_company_city';
    execute2(url,form,err);
}


function execute2(url,form,err){
    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
          
            var d = data.data;
            console.log(d);
            $('#company_city').append('<option value="'+d.city_id+'" selected> '+d.city_name+'</option>'); 
            $('#add_company_city').val('');
            $('#add_company_state').val('').trigger('change');
            $('#modal6').modal('close');
        }
    });    
    

}


function add_res_city_form(){

    if( $('#add_res_state').val() == null){
        alert('Select state');
        return false;
    }

    var url = 'addResCity';
    var form = 'add_res_city_form';
    var err = 'print-error-msg_add_res_city';
    execute3(url,form,err);
}

function execute3(url,form,err){

    

    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
          
            var d = data.data;
            console.log(d);
            $('#current_residence_city').append('<option value="'+d.city_id+'" selected> '+d.city_name+'</option>'); 
            $('#add_res_city').val('');
            $('#add_res_state').val('').trigger('change');
            $('#modal5').modal('close');
        }
    });    
    

}

function add_designation_form() {

    if( $('#add_designation').val() == null){
        alert('Enter designation');
        return false;
    }

    var url = 'addDesignation';
    var form = 'add_designation_form';
    var err = 'print-error-msg_add_res_city';
    execute4(url,form,err);
}

function execute4(url,form,err){

    

    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
            $('#'+form).serialize() ,
        
        success: function(data) {
        
            var d = data.data;
            console.log(d);
            $('#designation').append('<option value="'+d.dd_designation+'" selected> '+d.designation_name+'</option>'); 
            $('#add_designation').val('');      
            $('#modal9').modal('close');
        }
    });    


}

</script>