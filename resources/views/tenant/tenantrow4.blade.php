<div class="row">                                        
    
        <div class="col l3 s12"  >
                <ul class="collapsible" >
                    <li onClick="tenant_type_and_details()" id="tenant_type_and_details_li">
                    <div class="collapsible-header" id='col_tenant_type_and_details'> Tenant Type & Details</div>
                    
                    </li>                                        
                </ul>
        </div>

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="parking()">
                <div class="collapsible-header" id='col_parking'>Parking</div>
                
                </li>                                        
            </ul>
        </div>

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="license_period()">
                <div class="collapsible-header" id="col_license_period">License Period & Lock in Period</div>
                
                </li>                                        
            </ul>
        </div>
        
        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="specific_choice()">
                <div class="collapsible-header" id="col_specific_choice">Specific Choice </div>
                
                </li>                                        
            </ul>
        </div>

        <!--  -->
</div> 
    
        <div class="collapsible-body"  id='tenant_type_and_details' style="display:none">
            <div class="row">
                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default" onChange="hideShowTenantType()"   name="tenant_type" id="tenant_type">                            
                            <option value="" selected>Select</option>
                            <option value="family"  @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->tenant_type) ) @if($getBuyerData[0]->tenant_type == 'family' ) selected @endif @endif @endif>Family </option>
                            <option value="bachelor" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->tenant_type) ) @if($getBuyerData[0]->tenant_type == 'bachelor' ) selected @endif @endif @endif>Bachelor</option>
                        </select>
                        <label class="active">Tenant Type  </label>
                    </div>                    


                    <div class="input-field col l3 m4 s12" id="family_size_div">                    
                            <label for="wap_number" class="active">Family Size : <span class="red-text">*</span></label>
                            @php $familly_size=''; @endphp
                            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->familly_size) && !empty($getBuyerData[0]->familly_size)) @php $familly_size = $getBuyerData[0]->familly_size; @endphp @endif @endif
                            <input type="text" onkeypress="return onlyNumberKey(event)"  name="family_size" id="family_size"  Placeholder="Enter" value="{{$familly_size}}">
                
                    </div>   

                    <div class="input-field col l3 m4 s12" id="working_members_div" >                    
                            <label for="wap_number" class="active">Working Members : <span class="red-text">*</span></label>
                            @php $working_members=''; @endphp
                            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->working_members) && !empty($getBuyerData[0]->working_members)) @php $working_members = $getBuyerData[0]->working_members; @endphp @endif @endif
                            <input type="text" onkeypress="return onlyNumberKey(event)"  name="working_members" id="working_members" Placeholder="Enter" value="{{$working_members}}">
                    </div>

                    <div class="input-field col l3 m4 s12" id="no_of_members_div" style="display:none" >                    
                            <label for="wap_number" class="active">No of Members : <span class="red-text">*</span></label>
                            @php $no_of_members=''; @endphp
                             @if(isset($getBuyerData))@if(isset($getBuyerData[0]->no_of_members) && !empty($getBuyerData[0]->no_of_members)) @php $no_of_members = $getBuyerData[0]->no_of_members; @endphp @endif @endif
                            <input type="numeric"  name="no_of_members" id="no_of_members" onkeypress="return onlyNumberKey(event)"  value="{{$no_of_members}}"  Placeholder="Enter" >                                                                                             
                    </div>

                    <div class="input-field col l3 m4 s12">  
                        <label for="lead_assign" class="active">Member Details: <span class="red-text">*</span></label>
                        @php $member_details=''; @endphp
            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->member_details) && !empty($getBuyerData[0]->member_details)) @php $member_details = $getBuyerData[0]->member_details; @endphp @endif @endif
            <input type="text"   name="members_details" id="members_details"  Placeholder="Enter" value="{{$member_details}}" title="{{$member_details}}">
            <span>In case childrens ask bout their schools & college, If working which location , how they travel ?</span>
                     </div> 
            </div>

        </div>

       <div class="collapsible-body"  id='parking' style="display:none">
           <div class='row'>
               <div class="input-field col l3 m4 s12 display_search">                    
                   <select class="select2  browser-default"  multiple="multiple"  name="parking_type" id="parking_type1">
                   @if(isset($getBuyerData))
                            @if(isset($getBuyerData[0]->parking_type_id ) && !empty($getBuyerData[0]->parking_type_id ))
                                @php $parking_type_id2 = json_decode($getBuyerData[0]->parking_type_id ); 
                                    $parking_type_id2 = explode(",",$parking_type_id2[0]);     
                                @endphp
                                @foreach($parking as $parking)
                                <option value="{{$parking->parking_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $parking->parking_id,$parking_type_id2)) selected  @endif  @endif >{{ucwords($parking->parking_name)}}</option>
                                @endforeach
                            @else
                                @foreach($parking as $parking)
                                    <option value="{{$parking->parking_id}}" >{{ucwords($parking->parking_name)}}</option>
                                @endforeach
                            @endif
                        @else
                            @foreach($parking as $parking)
                                <option value="{{$parking->parking_id}}" >{{ucwords($parking->parking_name)}}</option>
                            @endforeach
                        @endif     
                   </select>
                   <label for="possession_year" class="active">Parking Type </label>
               </div>

               <div class=" input-field  col l3 m4 s12">
                           <label for="loan_amt" id="loan_amt" class="active">No of FW Parking: </label>                           
                           @php $no_of_four_wheeler_parking=''; @endphp
                            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->no_of_four_wheeler_parking) && !empty($getBuyerData[0]->no_of_four_wheeler_parking)) @php $no_of_four_wheeler_parking = $getBuyerData[0]->no_of_four_wheeler_parking; @endphp @endif @endif                         
                            <input type="text" onkeypress="return onlyNumberKey(event)" name="no_of_fw_parking"  id='no_of_fw_parking'  placeholder="Enter"  value="{{$no_of_four_wheeler_parking}}">
                        
                           
               </div>

               <div class=" input-field  col l3 m4 s12" id='no_of_tw_parking_show' style="display:none !important">
                       <label for="loan_amt" id="loan_amt" class="active">No of TW Parking</label>                           
                       @php $no_of_two_wheeler_parking=''; @endphp
                        @if(isset($getBuyerData))@if(isset($getBuyerData[0]->no_of_two_wheeler_parking) && !empty($getBuyerData[0]->no_of_two_wheeler_parking)) @php $no_of_two_wheeler_parking = $getBuyerData[0]->no_of_two_wheeler_parking; @endphp @endif @endif                                                  
                        <input type="text" onkeypress="return onlyNumberKey(event)" name="no_of_tw_parking"  id='no_of_tw_parking1'  placeholder="Enter"  value="{{$no_of_two_wheeler_parking}}">
                       
               </div>
           </div>


       </div>
     

        <div class="collapsible-body"  id='license_period' style="display:none">
            <div class="row">
                      
                      <div class="input-field col l3 m4 s12 display_search">                
                          <select class="select2  browser-default"  name="license_period" id="license_period1">  
                            <option value="" selected>Select</option>        
                            @foreach($licence_period as $licence_period)
                            <option value="{{ $licence_period->dd_licence_period_id }}" @if( isset($getBuyerData[0]->license_period) ) @if( $licence_period->dd_licence_period_id == $getBuyerData[0]->license_period  ) selected @endif @endif >{{ucwords($licence_period->licence_period)}}</option>
                            @endforeach
                          </select>
                          <label for="purpose" class="active">License Period </label>
                      </div>                  

                      <div class="input-field col l3 m4 s12 display_search">                
                          <select class="select2  browser-default"  name="lock_in_period" id="lock_in_period1">  
                                <option value="" selected>Select</option>                
                                @foreach($lock_in_period as $lock_in_period)
                                   <option value="{{ $lock_in_period->dd_lock_in_period_id }}" @if( isset($getBuyerData[0]->lock_in_period) ) @if( $lock_in_period->dd_lock_in_period_id == $getBuyerData[0]->lock_in_period  ) selected @endif @endif>{{ucwords($lock_in_period->lock_in_period)}}</option>
                                @endforeach
                          </select>
                          <label for="purpose" class="active">Lock In Period </label>
                      </div>
  
                     
                     
            </div>
        </div>  

     


       
       
        
        

        <div class="collapsible-body"  id='specific_choice' style="display:none">
            <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
				<div class="clonable" data-ss="1">
            <div class="row">
                      
                   
            <div class="input-field col l6 m6 s12 display_search">         
                      
                      @php $complex_name=''; @endphp
                        @if(isset($getSpecificChoiceData))@if(isset($getSpecificChoiceData[0]->complex_name) && !empty($getSpecificChoiceData[0]->complex_name)) @php $complex_name = $getSpecificChoiceData[0]->complex_name;
                        $complex_name2 = json_decode($complex_name); 
                        $complex_name2 = explode(",",$complex_name2[0]);       
                         @endphp @endif @endif          


                      <select class="select2  browser-default"  multiple="multiple"  name="project_name" id="project_name">

                         
                             @foreach($society as $sVal)
                                  <?php 
                                    $ids = $sVal->project_id.'-'.$sVal->society_id;
                                    $bn ='';
                                    if($sVal->building_name != ''){
                                        $bn = ' - '.ucwords($sVal->building_name);
                                    }
                                  ?>
                                      <option value="{{$ids}}" @if(isset($complex_name) && !empty($complex_name) ) @if(in_array($ids,$complex_name2)) selected  @endif  @endif ><?php echo ucwords($sVal->project_complex_name).$bn ?></option>
                                  @endforeach 

                   
                      </select>
                      <label for="possession_year" class="active">Complex/Building Name </label>
                        <span>Why in this complex?</span> <br> <span>Why in this building?</span>
                    </div>
               

               <!--    <div class="input-field col l3 m4 s12 display_search">         
                    
                      <select class="select2  browser-default"  multiple="multiple"  name="building_name" id="building_name">

                      @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData))
                              @if(isset($getSpecificChoiceData[0]->building_name ) && !empty($getSpecificChoiceData[0]->building_name ))
                                  @php $building_name2 = json_decode($getSpecificChoiceData[0]->building_name ); 
                                      $building_name2 = explode(",",$building_name2[0]);     
                                  @endphp

                                  @foreach($society as $society)
                                  <option value="{{$society->society_id}}" @if(isset($getSpecificChoiceData) && !empty($getSpecificChoiceData) ) @if(in_array( $society->society_id,$building_name2)) selected  @endif  @endif >{{ucwords($society->building_name)}}</option>
                                  @endforeach
                              @else
                                  @foreach($society as $society)
                                      <option value="{{$society->society_id}}"  >{{ucwords($society->building_name)}}</option>
                                  @endforeach   

                              @endif
                          @else
                              @foreach($society as $society)
                                  <option value="{{$society->society_id}}"  >{{ucwords($society->building_name)}}</option>
                              @endforeach
                              
                          @endif  

                      
                      </select>
                      <label for="possession_year" class="active">Building Name </label>
                      <span>Why in this building?</span>
                  </div>

 -->
                   
                  <div class="input-field col l6 m6 s12" >  
                    <label for="lead_assign" class="active">Remark: <span class="red-text">*</span></label>
                        @php $comment=''; @endphp
                        @if(isset($getSpecificChoiceData))@if(isset($getSpecificChoiceData[0]->comment) && !empty($getSpecificChoiceData[0]->comment)) @php $comment = $getSpecificChoiceData[0]->comment; @endphp @endif @endif                                                  
                    <input type="text" class="validate" name="specific_choice_comment" id="specific_choice_comment" placeholder="Enter"  value="{{$comment}}">
                    
                </div>
                <div class="input-field col l6 m6 s12" >  
                <span>What is that one thing which you are looking in this property?,</span> <br><span>What is your expectations from us as a property consultant?</span> 
                </div>
            </div>

              
                  
                </div>
              </div>  
        </div>
       
 

       


    <!-- Modal Building amenity -->
    <div id="modal12" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Building Amenities</h5>
        <hr>
        
        <form method="post" id="add_building_amenity_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Amenities: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_building_amenity" id="add_building_amenity"   placeholder="Enter">
                    <span class="add_building_amenity_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_building_amenity" style="display:none">
            <ul style="color:red"></ul>
            </div>
            <span id="buildingerr" style="color:red"></span>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_building_amenity_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>

<script>

function execute2(url,form,err){
    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(url);
            console.log(data);// return;
            if($.isEmptyObject(data.error)){
                $('#buildingerr').html('');
                alert(data.success);
                var html= data.resp;
                        $('#bame').append( html);
                        $('#modal12').modal('close');
                        $('#add_building_amenity').val('');
                // location.reload();
            }else{
                if(data.error=='0'){
                    $('#buildingerr').html('Already Exist');
                    return false;
                }
                $('#buildingerr').html('');
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
}

function add_building_amenity_form(){
    var url = 'addBuildingAmenity';
    var form = 'add_building_amenity_form';
    var err = 'print-error-msg_add_building_amenity';
    execute2(url,form,err);
}

</script>