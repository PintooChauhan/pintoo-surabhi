<div class="row">        

     
        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="key_member()">
                <div class="collapsible-header" id="col_key_member">Key members </div>                
                </li>                                        
            </ul>
        </div>   

     
        <!-- <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="expected_closure()" id="expected_closure_li">
                <div class="collapsible-header" id='col_expected_closure'>Expected closure</div>
                
                </li>                                        
            </ul>
        </div> -->
                        
        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="challenges()">
                <div class="collapsible-header" id="col_challenges">Challenges & solutions </div>                
                </li>                                        
            </ul>
        </div>   


        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="lead_analysis()">
                <div class="collapsible-header" id="col_lead_analysis">Lead Stage ,Intensity & Status </div>                
                </li>                                        
            </ul>
        </div>   

        
        

        
</div>

<div class="collapsible-body"  id='challenges' style="display:none">
    <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
    <div class="clonable" data-ss="1">
        <div class="row">
            <input type="hidden" id="challanges_id">
            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Challenges : </label>
                <input type="text"  name="challenges"  Placeholder="Enter" id="challenges1"  >
                <!-- <div  class="addbtn" >
                        <a href="#" id="add_location13" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                </div>  -->
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Solutions : </label>
                <input type="text"  name="solutions"  Placeholder="Enter"  id="solutions1"> 
                <!-- <div  class="addbtn" >
                        <a href="#" id="add_location32" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                </div>  -->
            </div>

        
            
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">

                <div class="row" >
                    <div class="input-field col l2 m2 s6 display_search">
                    <div class="preloader-wrapper small active" id="loader4" style="display:none">
                        <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                    <div class="row" id="submitChallenges">
                        <div class="col l4 m4">
                            <a href="javascript:void(0)" onClick="challeges_solution()" class=" save_challeges_solution btn-small  waves-effect waves-light green darken-1 " id="save_challeges_solution">Save</a>
                        </div>
                        <div class="col l3 m3">
                                <a href="javascript:void(0)" onClick="clear_challeges_solution()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_challeges_solution"><i class="material-icons dp48">clear</i></a>
                        </div>
                    </div>
            </div> 

        </div>
        </div>
    </div>
    <br>
   
           <div class="row">
                <table class="bordered" id="challeges_solution_table" style="width: 100% !important" >
                    <thead>
                    <tr >
                    <th  style="color: white;background-color: #ffa500d4;">Sr No. </th>
                    <th  style="color: white;background-color: #ffa500d4;">Challenges</th>
                    <th  style="color: white;background-color: #ffa500d4;">Solutions</th>
                    <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                    <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                    </tr>
                    </thead>
                    <tbody>
                  
                    @php $incIDC1 = count($getChallengesData); @endphp
                    @php $ii3=count($getChallengesData); @endphp
                             @foreach($getChallengesData as $val1)
                            
                                 <tr id="cid_{{$val1->challanges_id}}">
                                 <td>@php $incIDC= $ii3--; @endphp {{$incIDC}}</td> 
                                     <td id="c_{{$val1->challanges_id}}">{{$val1->challenges}}</td>
                                     <td id="s_{{$val1->challanges_id}}">{{$val1->solution}}</td>
                                     <td id="clud_{{$val1->challanges_id}}">{{$val1->created_date}}</td>   
                                     <td>
                                        <a href="javascript:void(0)"  class="waves-effect waves-light " onClick="updateChallenges({{$val1->challanges_id}})" >Edit</a> |
                                        <a href='javascript:void(0)'  onClick="confirmDelChallenges({{$val1->challanges_id}})"   >Delete</a>
                                     </td>
                                 </tr>   
                             @endforeach
             
                    </tbody>
                </table>
                <input type="hidden" id="incIDC" value="{{$incIDC1}}">
          
            </div>
</div>


<div class="collapsible-body"  id='lead_analysis' style="display:none">
<div class='row'>
                <div class="input-field col l3 m4 s12 display_search">         
                Shifting Date:  <span id="sht1"></span>
                </div>
            </div>
    <div class="row">

            <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="lead_stage" id="lead_stage1">
                    @foreach($lead_stage as $lead_stage)
                        <option value="{{ $lead_stage->lead_stage_id }}" @if( isset($getBuyerData[0]->lead_stage) ) @if( $lead_stage->lead_stage_id == $getBuyerData[0]->lead_stage  ) selected @endif @endif >{{ucwords($lead_stage->lead_stage_name)}}</option>
                    @endforeach
                </select>
                <label for="possession_year" class="active">Lead Stage </label>              
            </div>
            

            <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="lead_intensity" id="lead_intensity1">
                    <option value=""  selected>Select</option>
                    <option value="hot" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->lead_intesity_id ) ) @if(  $getBuyerData[0]->lead_intesity_id  == 'hot' ) selected @endif @endif @endif >Hot</option>
                    <option value="medium" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->lead_intesity_id ) ) @if(  $getBuyerData[0]->lead_intesity_id  == 'medium' ) selected @endif @endif @endif >Medium</option>
                    <option value="cold" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->lead_intesity_id ) ) @if(  $getBuyerData[0]->lead_intesity_id  == 'cold' ) selected @endif @endif @endif >Cold</option>
                </select>
                <label for="possession_year" class="active">Lead Intensity </label>              
            </div>

            <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="lead_analysis" id="lead_analysis1">
                    <option value=""  selected>Select</option>
                    <option value="need" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->lead_analysis) ) @if(  $getBuyerData[0]->lead_analysis == 'need' ) selected @endif @endif @endif >Need</option>
                    <option value="desire" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->lead_analysis) ) @if(  $getBuyerData[0]->lead_analysis == 'desire' ) selected @endif @endif @endif >Desire</option>
                    <option value="want" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->lead_analysis) ) @if(  $getBuyerData[0]->lead_analysis == 'want' ) selected @endif @endif @endif >Want</option>
                </select>
                <label for="possession_year" class="active">Lead Status </label>              
            </div>
        
    </div>

    <div class="row">
           
           <div class="input-field col l3 m4 s12 display_search">
               <label for="lead_assign" class="active">Expected closure Date : </label>
               @php $expected_closure_date=''; @endphp
               @if(isset($getBuyerData))@if(isset($getBuyerData[0]->expected_closure_date) && !empty($getBuyerData[0]->expected_closure_date)) @php $expected_closure_date = $getBuyerData[0]->expected_closure_date; @endphp @endif @endif
               <input type="text"  class="datepicker" name="expected_closure_date"  Placeholder="Enter" id="expected_closure_date" value="{{$expected_closure_date}}">
           </div>

           <div class="input-field col l3 m4 s12 display_search">
               <label for="lead_assign" class="active">Closure Description : </label>
               @php $describe_in_method=''; @endphp
               @if(isset($getBuyerData))@if(isset($getBuyerData[0]->describe_in_method) && !empty($getBuyerData[0]->describe_in_method)) @php $describe_in_method = $getBuyerData[0]->describe_in_method; @endphp @endif @endif
               <input type="text"  name="describe_in_5wh" id="describe_in_5wh"  Placeholder="Enter" value="{{$describe_in_method}}" title="{{$describe_in_method}}">
           </div>
       </div>
</div>

<div class="collapsible-body"  id='key_member' style="display:none">
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="decision_initial" name="decision_initial"  class="select2 browser-default">
                   
                    @foreach($initials as $initials1)
                        <option value="{{$initials1->initials_id}}"  @if(isset($getmemberData)) @if( isset( $getmemberData[0]->decision_initial) ) @if( $initials1->initials_id == $getmemberData[0]->decision_initial ) selected @endif @endif @endif >{{ucwords($initials1->initials_name)}}</option>
                    @endforeach                   
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Decisoin Maker Name: </label>
                @php $decision_maker_name=''; @endphp
                @if(isset($getmemberData))@if(isset($getmemberData[0]->decision_maker_name) && !empty($getmemberData[0]->decision_maker_name)) @php $decision_maker_name = $getmemberData[0]->decision_maker_name; @endphp @endif @endif
                <input type="text" class="mobile_number" name="decision_maker_name" id="decision_maker_name"  placeholder="Enter" value="{{$decision_maker_name}}">      
            
        </div>    
            <div class="input-field col l3 m4 s12 display_search">
            <select class="select2  browser-default"   name="relation_with_buyer" id="relation_with_buyer" multiple="true">
                   
                    @if(isset($getmemberData))
                            @if(isset($getmemberData[0]->relation_with_buyer ) && !empty($getmemberData[0]->relation_with_buyer ))
                                @php $relation_with_buyer2 = json_decode($getmemberData[0]->relation_with_buyer ); 
                                $relation_with_buyer2 = explode(",",$relation_with_buyer2[0]);  
                                @endphp
                             
                                @foreach($relation as $relation2)
                                    <option value="{{$relation2->dd_relation_id}}" @if(isset($getmemberData) && !empty($getmemberData) ) @if(in_array( $relation2->dd_relation_id,$relation_with_buyer2)) selected  @endif  @endif>{{ucwords($relation2->relation)}}</option>
                                @endforeach
                            @else
                                @foreach($relation as $relation2)
                                    <option value="{{$relation2->dd_relation_id}}" >{{ucwords($relation2->relation)}}</option>
                                @endforeach
                            @endif
                        @else
                            @foreach($relation as $relation2)
                                <option value="{{$relation2->dd_relation_id}}" >{{ucwords($relation2->relation)}}</option>
                            @endforeach
                        @endif 
                </select>
                <label for="lead_assign" class="active">Relation with Tenant : </label>
                            
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Remark : </label>
                @php $decision_in_5wh=''; @endphp
                @if(isset($getmemberData))@if(isset($getmemberData[0]->decision_in_5wh) && !empty($getmemberData[0]->decision_in_5wh)) @php $decision_in_5wh = $getmemberData[0]->decision_in_5wh; @endphp @endif @endif
                <input type="text"  name="decision_in_5wh"  id="decision_in_5wh" Placeholder="Enter" value="{{$decision_in_5wh}}" title="{{$decision_in_5wh}}">
            </div>
    </div>
  

    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="influencer_initial" name="influencer_initial"  class="select2 browser-default">
                   
                    @foreach($initials as $initials3)
                        <option value="{{$initials3->initials_id}}"  @if(isset($getmemberData)) @if( isset( $getmemberData[0]->influencer_initial) ) @if( $initials3->initials_id == $getmemberData[0]->influencer_initial ) selected @endif @endif @endif >{{ucwords($initials3->initials_name)}}</option>
                    @endforeach                     
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Influencer Name: </label>
                @php $influencer_name=''; @endphp
                @if(isset($getmemberData))@if(isset($getmemberData[0]->influencer_name) && !empty($getmemberData[0]->influencer_name)) @php $influencer_name = $getmemberData[0]->influencer_name; @endphp @endif @endif
                <input type="text" class="mobile_number" name="influencer_name" id="influencer_name"  placeholder="Enter" value="{{$influencer_name}}">      
            
        </div>
            <div class="input-field col l3 m4 s12 display_search">
                
                 <select class="select2  browser-default"   name="relation_with_buyer_i" id="relation_with_buyer_i" multiple="true">
                  
                    @if(isset($getmemberData))
                            @if(isset($getmemberData[0]->relation_with_buyer_f  ) && !empty($getmemberData[0]->relation_with_buyer_f  ))
                                @php $relation_with_buyer3 = json_decode($getmemberData[0]->relation_with_buyer_f ); 
                                $relation_with_buyer3 = explode(",",$relation_with_buyer3[0]);  
                                @endphp
                             
                                @foreach($relationn as $relation3)
                                    <option value="{{$relation3->dd_relation_id}}" @if(isset($getmemberData) && !empty($getmemberData) ) @if(in_array( $relation3->dd_relation_id,$relation_with_buyer3)) selected  @endif  @endif>{{ucwords($relation3->relation)}}</option>
                                @endforeach
                            @else
                                @foreach($relationn as $relation3)
                                   <option value="{{$relation3->dd_relation_id}}"  >{{ucwords($relation3->relation)}}</option>
                                @endforeach
                            @endif
                        @else
                            @foreach($relationn as $relation3)
                                <option value="{{$relation3->dd_relation_id}}"  >{{ucwords($relation3->relation)}}</option>
                            @endforeach
                        @endif 
                </select>
                <label for="lead_assign" class="active">Relation with Tenant : </label>
                           
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Remark : </label>
                @php $influencer_in_5wh=''; @endphp
                @if(isset($getmemberData))@if(isset($getmemberData[0]->influencer_in_5wh) && !empty($getmemberData[0]->influencer_in_5wh)) @php $influencer_in_5wh = $getmemberData[0]->influencer_in_5wh; @endphp @endif @endif
                <input type="text"  name="influencer_in_5wh" id="influencer_in_5wh"  Placeholder="Enter" value="{{$influencer_in_5wh}}" title="{{$influencer_in_5wh}}">               
            </div>
    </div>

    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="financer_initial" name="financer_initial"  class="select2 browser-default">
                   
                    @foreach($initials as $initials2)
                        <option value="{{$initials2->initials_id}}" @if(isset($getmemberData)) @if( isset( $getmemberData[0]->financer_initial) ) @if( $initials2->initials_id == $getmemberData[0]->financer_initial ) selected @endif @endif @endif >{{ucwords($initials2->initials_name)}}</option>
                    @endforeach               
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Financer Name: </label>
                @php $financer_name=''; @endphp
                @if(isset($getmemberData))@if(isset($getmemberData[0]->financer_name) && !empty($getmemberData[0]->financer_name)) @php $financer_name = $getmemberData[0]->financer_name; @endphp @endif @endif
                <input type="text" class="mobile_number" name="financer_name" id="financer_name"  placeholder="Enter"  value="{{$financer_name}}">
            
        </div>
            <div class="input-field col l3 m4 s12 display_search">

                <select class="select2  browser-default"   name="relation_with_buyer_f" id="relation_with_buyer_f" multiple="true">
                   
                    @if(isset($getmemberData))
                            @if(isset($getmemberData[0]->relation_with_buyer_i  ) && !empty($getmemberData[0]->relation_with_buyer_i  ))
                                @php $relation_with_buyer4 = json_decode($getmemberData[0]->relation_with_buyer_i ); 
                                $relation_with_buyer4 = explode(",",$relation_with_buyer4[0]);  
                                @endphp
                             
                                @foreach($relation as $relation)
                                    <option value="{{$relation->dd_relation_id}}" @if(isset($getmemberData) && !empty($getmemberData) ) @if(in_array( $relation->dd_relation_id,$relation_with_buyer4)) selected  @endif  @endif>{{ucwords($relation->relation)}}</option>
                                @endforeach
                            @else
                                @foreach($relation as $relation)
                                  <option value="{{$relation->dd_relation_id}}"   >{{ucwords($relation->relation)}}</option>
                                @endforeach
                            @endif
                        @else
                            @foreach($relation as $relation)
                                 <option value="{{$relation->dd_relation_id}}"   >{{ucwords($relation->relation)}}</option>
                            @endforeach
                        @endif
                </select>
                <label for="lead_assign" class="active">Relation with Tenant : </label>
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Remark : </label>
                @php $financer_in_5wh=''; @endphp
                @if(isset($getmemberData))@if(isset($getmemberData[0]->financer_in_5wh) && !empty($getmemberData[0]->financer_in_5wh)) @php $financer_in_5wh = $getmemberData[0]->financer_in_5wh; @endphp @endif @endif
                <input type="text"  name="financer_in_5wh" id="financer_in_5wh"  Placeholder="Enter" value="{{$financer_in_5wh}}" title="{{$financer_in_5wh}}">
            </div>
    </div>
</div>


<div class="modal" id="deleteChallengesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-content">
               <h5 style="text-align: center">Delete record</h5>
               <hr>        
               <form method="post" id="delete_challenges" >
                  <input type="hidden" class="challanges_id2" id="challanges_id2" name="challanges_id">
                  <input type="hidden" class="form_type" id="form_type" name="form_type" value="challenges">
                  <div class="modal-body">
                     
                     <h5>Are you sure want to delete this record?</h5>
                  
                  </div>
            
                   </div>
            
               <div class="modal-footer" style="text-align: left;">
                     <div class="row">
                     <div class="input-field col l2 m2 s6 display_search">
                           <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                           <a href="javascript:void(0)" onClick="deleteChallenges()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                     </div>    
                     
                     <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                           <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                     </div>    
                  </div> 
               </div>
           </form>
    </div>
