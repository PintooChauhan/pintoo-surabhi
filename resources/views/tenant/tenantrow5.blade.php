<div class="row">                                        
                


  

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="nature_of_business()" id="nature_of_business_li">
            <div class="collapsible-header" id="col_nature_of_business">Nature of Business & Washroom</div>
            
            </li>                                        
        </ul>
    </div>   

    <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="brokerage_fee()" id="brokerage_fee_li">
                <div class="collapsible-header" id='col_brokerage_fee'> Brokerage fees</div>
                
                </li>                                        
            </ul>
        </div>   
               

       </div>
      
 

       

        
<div class="collapsible-body"  id='nature_of_business' style="display:none">
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Nature of Business : <span class="red-text">*</span></label>
            @php $nature_of_business_id=''; @endphp
            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->nature_of_business_id) && !empty($getBuyerData[0]->nature_of_business_id)) @php $nature_of_business_id = $getBuyerData[0]->nature_of_business_id; @endphp @endif @endif
            <input type="text"  name="nature_of_bussiness" id="nature_of_bussiness1"  Placeholder="Enter" value="{{$nature_of_business_id}}" title="{{$nature_of_business_id}}">
        </div>

        <div class="input-field col l3 m4 s12 display_search">                
            <select class="select2  browser-default" multiple="multiple" id="washroom"  name="washroom">
                @if(isset($getBuyerData))
                    @if(isset($getBuyerData[0]->washroom_type_id ) && !empty($getBuyerData[0]->washroom_type_id ))
                        @php $washroom_type_id2 = json_decode($getBuyerData[0]->washroom_type_id ); 
                            $washroom_type_id2 = explode(",",$washroom_type_id2[0]);   
                        @endphp
                        @foreach($washroom_type as $washroom_type)
                        <option value="{{$washroom_type->dd_washroom_type_id}}"  @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $washroom_type->dd_washroom_type_id,$washroom_type_id2)) selected  @endif  @endif >{{ucwords($washroom_type->washroom_type)}}</option>
                        @endforeach
                    @else
                    @foreach($washroom_type as $washroom_type)
                        <option value="{{$washroom_type->dd_washroom_type_id}}"   >{{ucwords($washroom_type->washroom_type)}}</option>
                    @endforeach
                        @endif
                @else
                    @foreach($washroom_type as $washroom_type)
                        <option value="{{$washroom_type->dd_washroom_type_id}}"   >{{ucwords($washroom_type->washroom_type)}}</option>
                    @endforeach
                @endif

            </select>
            <label for="building_type" class="active">Washroom </label>
        </div>
    </div>
</div>

<div class="collapsible-body"  id='brokerage_fee' style="display:none">
    <div class="row">

    <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Brokerage fees: <span class="red-text">*</span></label>
            @php $brokerage_fees=''; @endphp
                @if(isset($getBuyerData))@if(isset($getBuyerData[0]->brokerage_fees) && !empty($getBuyerData[0]->brokerage_fees)) @php $brokerage_fees = $getBuyerData[0]->brokerage_fees; @endphp @endif @endif
                <input type="text"  name="brokerage_fees" id="brokerage_fees" placeholder="Enter"  value="{{$brokerage_fees}}">
            <!-- <div  class="addbtn" >
                    <a href="#" id="add_location13" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  -->
        </div>

            
            <div class="input-field col l3 m4 s12" >  
                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                @php $brokerage_fees_comment=''; @endphp
                @if(isset($getBuyerData))@if(isset($getBuyerData[0]->brokerage_fees_comment) && !empty($getBuyerData[0]->brokerage_fees_comment)) @php $brokerage_fees_comment = $getBuyerData[0]->brokerage_fees_comment; @endphp @endif @endif
                <input type="text" name="brokerage_fees_comment" id="brokerage_fees_comment" placeholder="Enter" value="{{$brokerage_fees_comment}}" >
            </div> 

            <div class="input-field col l6 m6 s12" style="margin-top: 10px;">  
                <span style="margin-left: 20px;">Are you aware what is the standard industry norms for the brokerage fees?</span>
            </div>


            
    </div>
</div>
