@if(isset($getSpecificChoiceData))  
                @if(isset($getSpecificChoiceData[0]->building_amenities ) && !empty($getSpecificChoiceData[0]->building_amenities )) 

                    <?php  
                            $decodeEle4 = json_decode($getSpecificChoiceData[0]->building_amenities) ;
                            if(!empty($decodeEle4)){

                                
                                if(!empty($decodeEle4[0])){
                                $exp4 = explode(',',$decodeEle4[0]);
                                foreach($exp4 as $val4){ ?>
                                    <script>
                                        // $(document).ready(function(){
                                            blankArry3.push(<?= $val4 ?>);
                                            // alert(blankArry1);
                                        // });

                                    </script>
                                <?php }
                                    }
                                }
                            ?>
                @endif
            @endif

<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row">

<div class="col l3 s12"  >
            <ul class="collapsible" >
                <li onClick="address()" >
                <div class="collapsible-header" id='col_address'>F2F & Address</div>
                
                </li>                                        
            </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="shifting_date()">
            <div class="collapsible-header" id="col_shifting_date">Shifting Date</div>
            
            </li>                                        
        </ul>
    </div>
   
   

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="notice_period()">
            <div class="collapsible-header" id='col_notice_period'>Notice Period</div>
            
            </li>                                        
        </ul>
    </div>

   
    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="building_age()" id="building_age_li">
            <div class="collapsible-header" id='col_building_age'> Building & Unit Preference</div>
            
            </li>                                        
        </ul>
    </div>
   

    
                           
</div>
                   
                             
           
        <div class="collapsible-body"  id='building_age' style="display:none">
            <div class="row">
                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"   name="building_age" id="building_age1" >
                         <!-- <option value=""  selected>Select</option> -->
                         <option value=""  selected>Select</option>
                        @foreach($building_age as $building_age1)
                            <option value="{{$building_age1->dd_building_age_id}}" @if(isset($getBuyerData)) @if( isset($getBuyerData[0]->building_age) ) @if( $building_age1->dd_building_age_id ==  $getBuyerData[0]->building_age ) selected @endif @endif @endif >{{ucwords($building_age1->building_age)}}</option>
                        @endforeach 
                        </select>
                        <label for="building_age" class="active">Building Age  </label>
                    </div>   
                    
                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"   name="floor_choice" id="floor_choice1"  multiple="multiple">
                            <!-- <option value=""  selected>Select</option> -->
                            @if(isset($getBuyerData))
                                       @if(isset($getBuyerData[0]->floor_choice ) && !empty($getBuyerData[0]->floor_choice ))
                                           @php $floor_choice2 = json_decode($getBuyerData[0]->floor_choice ); 
                                               $floor_choice2 = explode(",",$floor_choice2[0]);     
                                           @endphp
           
                                           @foreach($floor_choice as $floor_choice)
                                           <option value="{{$floor_choice->dd_floor_choice_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $floor_choice->dd_floor_choice_id,$floor_choice2)) selected  @endif  @endif >{{ucwords($floor_choice->floor_choice)}}</option>
                                           @endforeach
                                       @else
                                           @foreach($floor_choice as $floor_choice1)
                                               <option value="{{$floor_choice1->dd_floor_choice_id}}" >{{ucwords($floor_choice1->floor_choice)}}</option>
                                           @endforeach
           
                                       @endif
                                   @else
                                       @foreach($floor_choice as $floor_choice1)
                                           <option value="{{$floor_choice1->dd_floor_choice_id}}" >{{ucwords($floor_choice1->floor_choice)}}</option>
                                       @endforeach
                                       
                                   @endif  
                        </select>
                        <label for="floor_choice" class="active">Floor Choice    </label>
                    </div>   

                    <div class="input-field col l3 m4 s12 display_search">                
                        <select class="select2  browser-default" multiple="multiple"  name="door_facing_direction" id="door_facing_direction1">
                         
                                
                        @if(isset($getBuyerData))
                                @if(isset($getBuyerData[0]->door_direction_id ) && !empty($getBuyerData[0]->door_direction_id ))
                                    @php $door_direction_id2 = json_decode($getBuyerData[0]->door_direction_id ); 
                                        $door_direction_id2 = explode(",",$door_direction_id2[0]);   
                                    @endphp
                                    @foreach($direction as $direction)
                                        <option value="{{ $direction->direction_id }}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $direction->direction_id,$door_direction_id2)) selected  @endif  @endif >{{ucwords($direction->direction)}}</option>
                                    @endforeach
                                @else
                                @foreach($direction as $direction)
                                    <option value="{{ $direction->direction_id }}"  >{{ ucwords($direction->direction )}}</option>
                                @endforeach
                                @endif
                        @else
                            @foreach($direction as $direction)
                                <option value="{{ $direction->direction_id }}"  >{{ ucwords($direction->direction )}}</option>
                            @endforeach
                        @endif

                        </select>
                        <label for="purpose" class="active">Door Direction</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">   
                                   
                                   <select class="select2  browser-default" multiple="multiple"   name="bathroom" id="bathroom">
                                   @if(isset($getBuyerData))
                                       @if(isset($getBuyerData[0]->bathroom_id ) && !empty($getBuyerData[0]->bathroom_id ))
                                           @php $bathroom_id2 = json_decode($getBuyerData[0]->bathroom_id ); 
                                               $bathroom_id2 = explode(",",$bathroom_id2[0]);     
                                           @endphp
           
                                           @foreach($bathroom as $bathroom)
                                           <option value="{{$bathroom->bathroom_id}}" @if(isset($getBuyerData) && !empty($getBuyerData) ) @if(in_array( $bathroom->bathroom_id,$bathroom_id2)) selected  @endif  @endif >{{ucwords($bathroom->bathroom)}}</option>
                                           @endforeach
                                       @else
                                           @foreach($bathroom as $bathroom1)
                                               <option value="{{$bathroom1->bathroom_id}}" >{{ucwords($bathroom1->bathroom)}}</option>
                                           @endforeach
           
                                       @endif
                                   @else
                                       @foreach($bathroom as $bathroom1)
                                           <option value="{{$bathroom1->bathroom_id}}" >{{ucwords($bathroom1->bathroom)}}</option>
                                       @endforeach
                                       
                                   @endif  
           
                                   
                                   </select>
                                   <label for="purpose" class="active">Bathrooom</label>
                    </div>

                    
            </div>

            


        </div>         

        
                
          
        <div class="collapsible-body"  id='notice_period' style="display:none">
            <div class='row'>
                <div class="input-field col l3 m4 s12 display_search">         
                Current Residence Status:   <span id="crs">@if( isset($getBuyerData[0]->current_residential_status) ) {{ucfirst($getBuyerData[0]->current_residential_status)}} @endif</span>
                </div>
            </div>
           <div class='row'>
               <div class="input-field col l3 m4 s12 display_search">                    
                   <select class="select2  browser-default" onChange="hideShowNoticePeriod()"  name="notice_period" id="notice_period_served">
                       <option value="" >Select  </option>
                       <option value="yes" @if( isset($getBuyerData[0]->notice_served) ) @if( $getBuyerData[0]->notice_served == 'yes'  ) selected @endif @endif >Yes</option>
                       <option value="no" @if( isset($getBuyerData[0]->notice_served) ) @if( $getBuyerData[0]->notice_served == 'no'  ) selected @endif @endif>No</option>
                   </select>
                   <label  class="active">Notice Served </label>
               </div>

              <div class=" input-field  col l3 m4 s12" id="notice_period_date_div" style="display:none">
                <label for="wap_number" class="active">Notice Period Date <span class="red-text">*</span></label>
                @php $notice_period_date=''; @endphp
                @if(isset($getBuyerData))@if(isset($getBuyerData[0]->notice_period_date) && !empty($getBuyerData[0]->notice_period_date)) @php $notice_period_date = $getBuyerData[0]->notice_period_date; @endphp @endif @endif
                <input type="text" class="datepicker" name="notice_period_date" id="notice_period_date"   placeholder="Calender" style="height: 41px;" value="{{$notice_period_date}}">
                
              </div>

              <div class="input-field col l3 m4 s12" id="notice_period_reason_div" style="display:none" >  
                    <label for="lead_assign" class="active"> Reason for not serving notice period: <span class="red-text">*</span></label>
                    @php $notice_period_reason=''; @endphp
                @if(isset($getBuyerData))@if(isset($getBuyerData[0]->notice_period_reason) && !empty($getBuyerData[0]->notice_period_reason)) @php $notice_period_reason = $getBuyerData[0]->notice_period_reason; @endphp @endif @endif
                    <input type="text" class="validate" name="notice_period_reason" id="notice_period_reason"   placeholder="Enter" style="height: 41px;" value="{{$notice_period_reason}}">
                </div> 
           </div>


    </div>
   
           
    
        

        


        <div class="collapsible-body"  id='shifting_date' style="display:none">
            <div class='row'>
                <div class="input-field col l3 m4 s12 display_search">         
                Current Residence Status:  <span id="crs1">@if( isset($getBuyerData[0]->current_residential_status) ) {{ucfirst($getBuyerData[0]->current_residential_status)}} @endif</span>
                </div>
            </div>
            <div class="row">
                        
            <div class="input-field col l3 m4 s12" >                    
                    <label  class="active">Shifting Date: <span class="red-text">*</span></label>
                    @php $shifting_date=''; @endphp
                            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->shifting_date) && !empty($getBuyerData[0]->shifting_date)) @php $shifting_date = $getBuyerData[0]->shifting_date; @endphp @endif @endif
                    <input type="text" class="datepicker" name="shifing_date"   id="shifing_date1" placeholder="Calender" value="{{$shifting_date}}" style="height: 41px;">                               
                    <span> Why do you want to shift by this Date?</span><br>
                    <span>What if you are not able to Shift by this Date?</span>
                </div> 
                
                <div class="input-field col l3 m4 s12 " >
                    <label  class="active">Reason for Shifting : </label>       
                    @php $reason_of_shifting=''; @endphp
                            @if(isset($getBuyerData))@if(isset($getBuyerData[0]->reason_of_shifting) && !empty($getBuyerData[0]->reason_of_shifting)) @php $reason_of_shifting = $getBuyerData[0]->reason_of_shifting; @endphp @endif @endif                    
                    <input type="text" class="active" name="reason_for_shifting" Placeholder="Enter"  id='reason_for_shifting' value="{{$reason_of_shifting}}" > 
                </div> 
                
                <!-- <div class="input-field col l6 m6 s12" >
                    <span>
                    <b> W1</b> What is that they are looking in property? ,<b> W4</b>  Where will they buy ?  <br>
                    <b> W2</b> Why they will finalize by ___ (Display Month)? , <b>W5</b>  When they will buy ?  <br>                                                            
                    <b> W3</b> Who is the final decision maker(Name anyone)? ,<b>H1</b> How this will be done ?
                    </span>    
                </div> -->
                
                    
            </div>

                                    
            

  
        </div>
           
        <div class="collapsible-body"  id='address' style="display:none">
            <div class="row">
                
                        <!-- <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">F2F Time & Date : <span class="red-text">*</span></label>
                            <input type="datetime-local"  name="f2f_time_and_date"   placeholder="Calender" >                                                                                             
                        </div> -->
                        <div class="input-field col l3 m4 s12 display_search">                
                            <select class="select2  browser-default"  onChange="show_rent_amt()" name="current_residence_status" id="current_residence_status">
                                <option value="" selected >Select </option>
                                <option value="owned" @if( isset($getBuyerData[0]->current_residential_status) ) @if( $getBuyerData[0]->current_residential_status == "owned"  ) selected @endif @endif>Owned</option>
                                <option value="rented"  @if( isset($getBuyerData[0]->current_residential_status) ) @if( $getBuyerData[0]->current_residential_status == "rented"  ) selected @endif @endif>Rented</option>
                            </select>
                            <label for="purpose" class="active"> Current Residence Status  </label>
                        </div>  

                        @if(isset($getBuyerData) && !empty($getBuyerData))  
                            @if( isset($getBuyerData[0]->current_residential_status) && !empty($getBuyerData[0]->current_residential_status) ) 
                                @if($getBuyerData[0]->current_residential_status == 'rented')  
                                    <div class="input-field col l3 m4 s12 rntamt">                    
                                      
                                        <select  id="rent_amount" name="rent_amount"  class="select2 browser-default">
                                            <option value=""  selected>Select</option>
                                            @foreach($minimum_rent as $minimum_rent1)
                                            <option value="{{$minimum_rent1->minimum_rent}}"  @if(isset($getCustomerData) && !empty($getCustomerData)) @if( isset($getCustomerData[0]->rent_amount) ) @if(  $minimum_rent1->minimum_rent == $getCustomerData[0]->rent_amount  ) selected @endif @endif @endif >{{$minimum_rent1->minimum_rent_in_words}}</option>
                                            @endforeach
                                            
                                        </select>
                                        <label for="wap_number" class="active">Rent Amount : </label>
                                    </div>
                                @endif 
                            @else
                            <div class="input-field col l3 m4 s12 rntamt"  style="display:none"  >                    
                                      <select  id="rent_amount" name="rent_amount"  class="select2 browser-default">
                                            <option value=""  selected>Select</option>
                                            @foreach($minimum_rent as $minimum_rent1)
                                            <option value="{{$minimum_rent1->minimum_rent}}"  @if(isset($getCustomerData) && !empty($getCustomerData)) @if( isset($getCustomerData[0]->rent_amount) ) @if(  $minimum_rent1->minimum_rent == $getCustomerData[0]->rent_amount  ) selected @endif @endif @endif >{{$minimum_rent1->minimum_rent_in_words}}</option>
                                            @endforeach                                            
                                        </select>
                            <label for="wap_number" class="active">Rent Amount : </label>

                        </div>
                            @endif 
                     @else
                     <div class="input-field col l3 m4 s12 rntamt"  style="display:none"  >                    
                       
                            <select  id="rent_amount" name="rent_amount"  class="select2 browser-default">
                                <option value=""  selected>Select</option>
                                @foreach($minimum_rent as $minimum_rent1)
                                <option value="{{$minimum_rent1->minimum_rent}}"  @if(isset($getCustomerData) && !empty($getCustomerData)) @if( isset($getCustomerData[0]->rent_amount) ) @if(  $minimum_rent1->minimum_rent == $getCustomerData[0]->rent_amount  ) selected @endif @endif @endif >{{$minimum_rent1->minimum_rent_in_words}}</option>
                                @endforeach                                
                            </select>
                            <label for="wap_number" class="active">Rent Amount : </label>
                        
                    </div>
    
                        @endif 


                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"   name="f2f_done" id="f2f_done">
                                <option value=""  selected>Select</option>
                                <option value="yes"  @if(isset($getBuyerData))@if(isset($getBuyerData[0]->f2f_done) && !empty($getBuyerData[0]->f2f_done)) @if($getBuyerData[0]->f2f_done == 'yes') selected @endif @endif  @endif >Yes</option>
                                <option value="no" @if(isset($getBuyerData))@if(isset($getBuyerData[0]->f2f_done) && !empty($getBuyerData[0]->f2f_done)) @if($getBuyerData[0]->f2f_done == 'no') selected @endif @endif  @endif >No</option>
                               
                            </select>
                            <label for="possession_year" class="active"> F2F Done </label>
                           
                            </div>

                     

                    </div>

                    <div class="row">
                        <div class="input-field col l3 m4 s12" >  
                            <label for="lead_assign" class="active">Current Residence Address: <span class="red-text">*</span></label>
                           @php $customer_res_address=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->customer_res_address) && !empty($getCustomerData[0]->customer_res_address)) @php $customer_res_address = $getCustomerData[0]->customer_res_address; @endphp @endif @endif                         
                            <input type="text" name="current_residence_address" id="current_residence_address" placeholder="Enter" value="{{$customer_res_address}}" title="{{$customer_res_address}}">
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"   name="current_residence_city" id="current_residence_city">
                                <option value=""  selected>Select</option>
                                @foreach($city as $city2)
                                <option value="{{$city2->city_id}}"  @if( isset($getCustomerData[0]->customer_city) ) @if( $city2->city_id == $getCustomerData[0]->customer_city  ) selected @endif @endif >{{ucwords($city2->city_name)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">City </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                            <a href="#modal5" id="add_cra_city" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                            </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"   name="current_residence_state" id="current_residence_state">
                                <option value=""  selected>Select</option>
                                @foreach($state as $state2)
                                  <option value="{{$state2->dd_state_id}}" @if( isset($getCustomerData[0]->customer_state) ) @if( $state2->dd_state_id == $getCustomerData[0]->customer_state  ) selected @endif @endif >{{ucwords($state2->state_name)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">State </label>
                            <!-- <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                            <a href="#" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                            </div> -->
                        </div>

                        <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">Pincode : <span class="red-text">*</span></label>
                            @php $customer_pin_code=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->customer_pin_code) && !empty($getCustomerData[0]->customer_pin_code)) @php $customer_pin_code = $getCustomerData[0]->customer_pin_code; @endphp @endif @endif
                            <input type="text" onkeypress="return onlyNumberKey(event)" name="current_residence_pincode" id="current_residence_pincode"  Placeholder="Enter" value="{{$customer_pin_code}}">
                        </div>
            </div>
            <div class="row">                

                        <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">Company Name : <span class="red-text">*</span></label>
                            @php $customer_company_name_address=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->customer_company_name_address) && !empty($getCustomerData[0]->customer_company_name_address)) @php $customer_company_name_address = $getCustomerData[0]->customer_company_name_address; @endphp @endif @endif
                            <input type="text"  name="company_name" id="company_name"  Placeholder="Enter" value="{{$customer_company_name_address}}" title="{{$customer_company_name_address}}">
                        </div>


                        <div class="input-field col l3 m4 s12 display_search" style="display:none !important">                    
                        <select class="select2  browser-default"  name="company_city" id="company_city1">
                                <option value=""  selected>Select</option>
                                    @foreach($city as $city1)
                                    <option value="{{$city1->city_id}}"  @if( isset($getCustomerData[0]->company_city) ) @if( $city1->city_id == $getCustomerData[0]->company_city  ) selected @endif @endif >{{ucwords($city1->city_name)}}</option>
                                    @endforeach
                            </select>
                            <label for="possession_year" class="active">City </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                                <a href="#modal6" id="add_com_city" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                            </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search" style="display:none !important">                    
                            <select class="select2  browser-default"  name="company_state" id="company_state1">
                                <option value=""  selected>Select</option>
                                    @foreach($state as $state1)
                                    <option value="{{$state1->dd_state_id}}"  @if( isset($getCustomerData[0]->company_state) ) @if($state1->dd_state_id == $getCustomerData[0]->company_state  ) selected @endif @endif >{{ucwords($state1->state_name)}}</option>
                                    @endforeach
                            </select>
                            <label for="possession_year" class="active">State </label>
                           
                        </div>

                        <div class="input-field col l3 m4 s12" style="display:none !important">                    
                            <label for="wap_number" class="active">Pincode : <span class="red-text">*</span></label>
                             @php $company_pincode=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->company_pincode) && !empty($getCustomerData[0]->company_pincode)) @php $company_pincode = $getCustomerData[0]->company_pincode; @endphp @endif @endif
                            <input type="text" onkeypress="return onlyNumberKey(event)"   name="company_pincode" id="company_pincode"   Placeholder="Enter"  value="{{$company_pincode}}">
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"   name="designation" id="designation">
                                <option value=""  selected>Select</option>
                                @foreach($designation as $designation)
                                <option value="{{$designation->dd_designation1_id}}"  @if( isset($getCustomerData[0]->customer_designation_id) ) @if( $designation->dd_designation1_id == $getCustomerData[0]->customer_designation_id  ) selected @endif @endif >{{ucwords($designation->designation)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">Designation </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                            <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                            </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"   name="industry_name" id="industry_name">
                                <option value=""  selected>Select</option>
                                @foreach($industry as $industry)
                                <option value="{{$industry->dd_industry_id}}"   @if( isset($getCustomerData[0]->industry) ) @if( $industry->dd_industry_id == $getCustomerData[0]->industry  ) selected @endif @endif>{{ucwords($industry->industry)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">Industry Name </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                            <a href="#modal10" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                            </div>
                        </div>

                        

                        
                        <div class="input-field col l3 m4 s12" >  
                            <label for="lead_assign" class="active">Information: <span class="red-text">*</span></label>
                            @php $information=''; @endphp
                            @if(isset($getCustomerData))@if(isset($getCustomerData[0]->information) && !empty($getCustomerData[0]->information)) @php $information = $getCustomerData[0]->information; @endphp @endif @endif
                            <input type="text"  name="information" id="information" placeholder="Enter" value="{{$information}}" title="{{$information}}">
                        </div>


            </div>
            
    
        </div>
                   
        
        

     <!-- Modal Company address - city Structure -->
     <div id="modal5" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add City</h5>
        <hr>
        
        <form method="post" id="add_res_city_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">City: </label>
                    <input type="text" class="validate" name="add_res_city" id="add_res_city"   placeholder="Enter">
                    <span class="add_res_city_err"></span>
                    
                </div>
                <div class="input-field col l3 m4 s12 ">                        
                    <select  id="add_res_state" name="add_res_state" placeholder="Select" class="select2  browser-default" >

                        <option selected disabled>Select State</option>
                         @foreach($state as $st)   
                        <option value={{$st->dd_state_id}}>{{ucfirst($st->state_name)}}</option>
                        @endforeach
                    </select>
                    <label class="active">State</label>                
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_res_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_res_city_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div>    

            </div>
        </form>
    </div>

    <!-- Modal Company address - city Structure -->
    <div id="modal6" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add City</h5>
        <hr>
        
        <form method="post" id="add_Company_city_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">City: </label>
                    <input type="text" class="validate" name="add_company_city" id="add_company_city"   placeholder="Enter">
                    <span class="add_company_city_err"></span>
                    
                </div>
                <div class="input-field col l3 m4 s12 ">                        
                    <select  id="add_company_state" name="add_company_state" placeholder="Select" class="select2  browser-default" >
                    <option selected disabled>Select State</option>
                         @foreach($state as $st)   
                        <option value={{$st->dd_state_id}}>{{ucfirst($st->state_name)}}</option>
                        @endforeach
                    </select>
                    <label class="active">State</label>                
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_company_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_Company_city_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div>    
            </div>
        </form>
    </div>

      <!-- Modal Designation Structure -->
      <div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation: </label>
                    <input type="text" class="validate" name="add_designation1" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>


      <!-- Modal Industry Name Structure -->
      <div id="modal10" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Industry Name</h5>
        <hr>
        
        <form method="post" id="add_industry_form1">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Industry Type: </label>
                    <input type="text" class="validate" name="add_industry_type" id="add_industry_type"   placeholder="Enter">
                    <span class="add_industry_name_err"></span>                    
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Industry Name: </label>
                    <input type="text" class="validate" required name="add_industry_name" id="add_industry_name"   placeholder="Enter">
                    <span class="add_industry_name_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_industry_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_industry_form1()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>


<script>
function apply_css11(skip,attr, val=''){   
    var id =returnColArray11();           
    id.forEach(function(entry) {
        
        if(entry==skip){
            // alert(11)
            $('#'+skip).css(attr,'orange','!important');
            // $('#'+skip).attr('style', 'background-color: orange !important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){

            }else{
                $('#'+entry).css(attr,val);
            }
            
            //action_to_hide();            
            
        }        
    });
}

function hide_n_show11(skip){
   var id = collapsible_body_ids11();
   collapsible_body_ids11().forEach(function(entry) {
       // console.log(id);
       if(entry==skip){
           // alert(skip);
           var x = document.getElementById(skip);  
           $('#'+skip).css('background-color','rgb(234 233 230)');
           // alert(x)          
           if (x.style.display === "none") {
               // alert(1)
               
               x.style.display = "block";
           } else {
               // alert(2)
               // alert(skip);
               // $('#col_lead_info').removeAttr("style");
               // $('#col_lead_info').css('background-color','white',"!important");
               x.style.display = "none";
               $('#col_'+skip).css('background-color','white');

           }
       }else{          
           $('#'+entry).hide();
       }
       
   });
}


function add_industry_form1(){

if($('#add_industry_name').val() == ''){
    alert('Enter Industry name');
    return false;
}
var url = 'addIndustry';
var form = 'add_industry_form1';
var err = 'print-error-msg_add_industry_name';
execute1(url,form,err);
}



function execute1(url,form,err){
var url = url;
var form = form;
var err = err;

$.ajax(  {
    url:"/"+url,
    type:"GET",
    data:                     
         $('#'+form).serialize() ,
    
    success: function(data) {
        // alert(url);
        
        var d = data.data;
        console.log(d.dd_industry_id);// return;
        if(d.industry_type != 'null' && d.industry_type != '' ){
            var l = d.industry_type +'-'+d.industry
        }else{
            var l = d.industry
        }
        $('#industry_name').append('<option value="'+d.dd_industry_id+'" selected> '+l+'</option>'); 
        $('#add_industry_name').val('');
        $('#add_industry_name').val('');
        $('#modal10').modal('close');
    }
});    


}

function add_Company_city_form(){
// alert($('#add_company_state').val());
if($('#add_company_state').val() == null ){
    alert('Select state');
    return false;
}
var url = 'addCompanyCity';
var form = 'add_Company_city_form';
var err = 'print-error-msg_add_company_city';
execute2(url,form,err);
}


function execute2(url,form,err){
var url = url;
var form = form;
var err = err;

$.ajax(  {
    url:"/"+url,
    type:"GET",
    data:                     
         $('#'+form).serialize() ,
    
    success: function(data) {
      
        var d = data.data;
        console.log(d);
        $('#company_city').append('<option value="'+d.city_id+'" selected> '+d.city_name+'</option>'); 
        $('#add_company_city').val('');
        $('#add_company_state').val('').trigger('change');
        $('#modal6').modal('close');
    }
});    


}


function add_res_city_form(){

if( $('#add_res_state').val() == null){
    alert('Select state');
    return false;
}

var url = 'addResCity';
var form = 'add_res_city_form';
var err = 'print-error-msg_add_res_city';
execute3(url,form,err);
}

function execute3(url,form,err){



var url = url;
var form = form;
var err = err;

$.ajax(  {
    url:"/"+url,
    type:"GET",
    data:                     
         $('#'+form).serialize() ,
    
    success: function(data) {
      
        var d = data.data;
        console.log(d);
        $('#current_residence_city').append('<option value="'+d.city_id+'" selected> '+d.city_name+'</option>'); 
        $('#add_res_city').val('');
        $('#add_res_state').val('').trigger('change');
        $('#modal5').modal('close');
    }
});    


}

function add_designation_form() {
    // alert( $('#add_designation1').val())
if( $('#add_designation').val() == null){
    alert('Enter designation');
    return false;
}
// 
var url = 'addDesignation';
var form = 'add_designation_form';
var err = 'print-error-msg_add_res_city';
execute4(url,form,err);
}

function execute4(url,form,err){



var url = url;
var form = form;
var err = err;

$.ajax(  {
    url:"/"+url,
    type:"GET",
    data:                     
        $('#'+form).serialize() ,
    
    success: function(data) {
    
        var d = data.data;
        console.log(d);
        $('#designation').append('<option value="'+d.dd_designation+'" selected> '+d.designation_name+'</option>'); 
        $('#add_designation').val('');      
        $('#modal9').modal('close');
    }
});    


}




$(document).ready(function(){
        hideShowNoticePeriod()
      });

</script>