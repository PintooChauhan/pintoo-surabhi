<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
      
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/4.1.0/css/fixedColumns.dataTables.min.css">

      <style>
  select
{
    display: block !important;
}

</style>


      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">
    
        @php
        $check_s_add = 0;
        $check_s_edit = 0;
        $check_s_delete = 0;
        @endphp

        @if(isset($access_check) && !empty($access_check) )
          @foreach($access_check as $ackey=>$acvalue)
            @php
              $check_s_add = $acvalue->s_add;
              $check_s_edit = $acvalue->s_edit;
              $check_s_delete = $acvalue->s_delete;
            @endphp
          @endforeach
        @endif

              <div class="row">
                
                <form action="/seachByMobile" method="post">
                     @csrf
                <div class="col s6 m6">
                        <div class="input-field col l4 m4 s12" id="InputsWrapper2">
                            <label  class="active">Enter Mobile No : </label>
                            @php $mobile_number1=''; @endphp
                                @if(isset($mobile_number) && !empty($mobile_number)) @php $mobile_number1 = $mobile_number; @endphp  @endif 
                            <input type="number" class="validate" name="mobile_number" id="mobile_number" value='{{$mobile_number1}}'  placeholder="Enter"  required >                             
                        </div>  
                        <div class="input-field col l2 m2 s12" id="InputsWrapper2">                        
                            <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action"> Search </button>
                        </div>  
                        <div class="input-field col l2 m2 s12" id="InputsWrapper2">                        
                            <a href="/pc-master" class="waves-effect waves-green btn-small" style="background-color: red;">Clear</a>
                        </div>  

                </div>
            </form>
                <div class="col s2 m2"></div>
                @if(!empty($check_s_add) )
                @if($check_s_add==1)
                    <div class="col s4 m4" style="text-align:right">
                    <a href="/add-new-pc" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> Add NEW PC</a>
                    </div>
                </div>
               @endif
               @endif


 <div class="row">
    <div class="col s12">
        <div class="card">
          
            <div class="card-content">

                @php
                $check_s_delete = 0;
                @endphp
  
                @if(isset($access_check) && !empty($access_check) )
                  @foreach($access_check as $ackey=>$acvalue)
                    @php
                      $check_s_delete = $acvalue->s_delete;
                    @endphp
                  @endforeach
                @endif

                <div class="row">
                    <div class="col s12">
                        <div class="tableFixHead">
                            <table id="scroll-vert-hor41" class="display nowrap table-hover">
                            <thead>
                            <tr class="pin">
                                <th >Company Name</th>
                                
                                <th >Branches</th>
                                <th >Member</th>
                                <!-- <th >Designation</th> -->
                                <!-- <th >Contact No.</th> -->
                                <!-- <th >Member Rating</th> -->
                                <th >Location</th>
                                <th >Sub Location</th>
                                <th >City</th>                                            
                                <th >Address</th>                                
                                <th >Years</th>
                                <!-- <th >Operaing From</th>                                 -->
                                <!-- <th >Branch Status</th>                                 -->
                                <th >RERA No.</th>  
                                <th >Username</th>
                                <th >Last Updated Date</th>
                                
                                @if(!empty($check_s_delete) )
                                    @if($check_s_delete==1)
                                    <th>Action</th>
                                    @endif
                                @endif

                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($getPcData) && !empty($getPcData))
                                        @foreach($getPcData as $val)

                                        <?php 
                                            $office_address = $val->office_address;
                                            $finalNameAdd = '';
                                            $sub2 = substr($office_address, 0, 35);

                                            if(strlen($office_address) > 35){
                                              $finalNameAdd =  $sub2;
                                            }else{
                                              $finalNameAdd =  $office_address;
                                            }
                                        ?>

                                      
                                            <tr>
                                                <td>
                                                    @if($check_s_edit==1)
                                                    <a target="_blank" href="/add-new-pc?pc_id={{$val->property_consultant_id}}"> {{ucwords($val->company_name)}}</a>
                                                    @else
                                                    <a href="javascript:void(0);"> {{ucwords($val->company_name)}}</a>
                                                    @endif
                                                </td>

                                                <td>
                                                    @if($check_s_edit==1)
                                                    <a  target="_blank" href="/add-branch-information?pc_id={{$val->property_consultant_id}}">{{$val->branch_cnt}}</a>
                                                    @else
                                                    <a href="javascript:void(0);">{{$val->branch_cnt}}</a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($check_s_edit==1)
                                                    <a target="_blank" href="/add-employee-information?pc_id={{$val->property_consultant_id}}"> {{$val->employee_cnt}}</a>
                                                    @else
                                                    <a href="javascript:void(0);"> {{$val->employee_cnt}}</a>
                                                    @endif
                                                </td>

                                                <td>{{ucwords($val->location1)}}</td>
                                                <td>{{ucwords($val->sub_location1)}}</td>
                                                <td>{{ucwords($val->city1)}}</td>     
                                                <td  title="<?= $office_address; ?>">{{ucwords($finalNameAdd)}}</td>
                                                <td>{{$val->no_of_years}}</td>
                                                
                                                
                                                <td>{{$val->rera_number}}</td>                                                                                                           
                                                <td>{{$val->usernanme}}</td>
                                                <td>{{$val->created_date}}</td>

                                                @if(!empty($check_s_delete) )
                                                    @if($check_s_delete==1)

                                                    <td>                                                  
                                                        <a href='javascript:void(0)' onClick='confirmDelPC({{$val->property_consultant_id}})' title="Delete"><i class="material-icons dp48">delete</i></a>
                                                    </td>
                                                    @endif
                                                @endif
                                                
                                            </tr>
                                        @endforeach
                                    @endif
                               

                            </tbody>
                            
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        <div class="modal" id="deletePCModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-content">
                  <h5 style="text-align: center">Delete record</h5>
                  <hr>        
                  <form method="post" id="delete_PC" >
                      <input type="hidden" class="property_consultant_id2" id="property_consultant_id2" name="property_consultant_id">
                      <input type="hidden"  id="form_type" name="form_type" value="PC">
                      <div class="modal-body">
                          <h5>Have you transferred all employees?</h5>
                          <h5>Are you sure want to delete this record?</h5>
                      
                      </div>
              
                      </div>
              
                  <div class="modal-footer" style="text-align: left;">
                          <div class="row">
                          <div class="input-field col l2 m2 s6 display_search">
                              <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                              <a href="javascript:void(0)" onClick="deletePC()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                          </div>    
                          
                          <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                              <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                          </div>    
                      </div> 
                  </div>
              </form>
          </div>

  <script>

function confirmDelPC(params) {
  // alert(params); return;   
    var property_consultant_id = params;
    $('.property_consultant_id2').val(property_consultant_id);
   
    $('#deletePCModal').modal('open');
}


function deletePC() {
    var url = 'deletePartially';
    var form = 'delete_PC';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            window.location.reload();
            
        }
    }); 
}

</script>
<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    
<script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
 
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/4.1.0/js/dataTables.fixedColumns.min.js"></script>
    <!-- END PAGE LEVEL JS-->    <!-- END PAGE LEVEL JS-->
