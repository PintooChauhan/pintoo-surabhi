<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}


</style>

      </style>
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
        <div class="row">                                        
                <div class="col l3 s12">
                <a href="/add-new-pc?pc_id={{$property_consultant_id}}" style="color:#000000b3">
                    <ul class="collapsible">
                        <li >
                        <div class="collapsible-header" id="col_company_info" >Company Info</div>
                        </li>                                        
                    </ul>
                </a>
                </div>

                <div class="col l3 s12">
                <a href="/add-branch-information?pc_id={{$property_consultant_id}}" style="color:#000000b3">
                    <ul class="collapsible">
                        <li >
                        <div class="collapsible-header" id="col_branch_info" >Branch Info</div>
                        
                        </li>                                        
                    </ul>
                </a>    
                </div>

                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="employee_info()">
                        <div class="collapsible-header" id="col_employee_info" tabindex="0" style="background-color: orange;">Member Info</div>
                        
                        </li>                                        
                    </ul>
                </div>

                <div class="col l3 s12">
                    <a href="/pc-master" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> Back To Home Page</a>
                </div>

          </div> 
            


         

        <input type="hidden"  id="pc_employees_id" value="{{$pc_employees_id}}">
        <input type="hidden"  id="property_consultant_id" value="{{$property_consultant_id}}">

        
        <div class="collapsible-body"  id='budget_loan' style="display:block" >
                <div class="row">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                     <select class="select2  browser-default"  id="branch"  name="branch">
                                <option value="" selected>Select</option>
                                  @foreach($branch as $val1)
                                        <option value="{{$val1->pc_branch_id}}"  @if(isset($getSinglePc) && !empty($getSinglePc)) @if(isset($getSinglePc[0]->pc_branch_id)) @if($val1->pc_branch_id == $getSinglePc[0]->pc_branch_id) selected  @endif @endif  @endif>{{ucwords($val1->sub_location_name)}}</option>
                                    @endforeach
                                </select>
                                <label for="leadtype37  " class="active">Branch Name </label>
                            
                        </div>

                 
                        <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                <label for="cus_name active" class="dopr_down_holder_label active">Member Name 
                                <span class="red-text">*</span></label>
                                <div  class="sub_val no-search">
                                    <select  id="cus_name_initial" name="cus_name_initial" class="select2 browser-default ">
                                    @foreach($initials as $initials)
                                    <option value="{{$initials->initials_id}}" @if(isset($getSinglePc)) @if( isset($getSinglePc[0]->initials) ) @if( $initials->initials_id == $getSinglePc[0]->initials) selected @endif @endif @endif >{{ucwords($initials->initials_name)}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                @php $employee_name=''; @endphp
                            @if(isset($getSinglePc))@if(isset($getSinglePc[0]->employee_name) && !empty($getSinglePc[0]->employee_name)) @php $employee_name = $getSinglePc[0]->employee_name; @endphp @endif @endif
                            <input type="text" class="mobile_number" name="employee_name" id="employee_name" value="{{$employee_name}}"   placeholder="Enter"  >                            
                            <!-- onkeypress="return /[a-z]/i.test(event.key)" -->
                            </div>
                        

                     

                            <div class="input-field col l3 m4 s12" id="InputsWrapper">
                    <label for="contactNum1" class="dopr_down_holder_label active">Primary Mobile Number: <span class="red-text">*</span></label>
                    <div  class="sub_val no-search">
                        <select  id="mobile_conuntry_code" name="mobile_conuntry_code" class="select2 browser-default">
                        @foreach($country as $country1)
                        <option value="{{$country1->country_code_id}}" @if(isset($getSinglePc)) @if( isset($getSinglePc[0]->primary_country_code_id) ) @if( $country1->country_code_id == $getSinglePc[0]->primary_country_code_id) selected @endif @endif @endif  >{{ucwords($country1->country_code)}}</option>
                        @endforeach
                        </select>
                    </div>
                       @php $primary_mobile_number=''; @endphp
                       @if(isset($getSinglePc))@if(isset($getSinglePc[0]->primary_mobile_number) && !empty($getSinglePc[0]->primary_mobile_number)) @php $primary_mobile_number = $getSinglePc[0]->primary_mobile_number; @endphp @endif @endif
                    <input type="text" onkeypress="return onlyNumberKey(event)" class="mobile_number" name="mobile_number_primary" id="mobile_number_primary"  placeholder="Enter" value="{{$primary_mobile_number}}"> 
                    @if(isset($getSinglePc) && !empty($getSinglePc))
                        @if(isset($getSinglePc[0]->seccondary_mobile_number) && !empty($getSinglePc[0]->seccondary_mobile_number))
                      
                        @else
                        <div id="AddMoreFileId" class="addbtn">
                            <a href="#" id="AddMoreFileBox" class="" style="color: grey !important">+</a> 
                        </div>
                        @endif
                    @else
                        <div id="AddMoreFileId" class="addbtn">
                            <a href="#" id="AddMoreFileBox" class="" style="color: grey !important">+</a> 
                        </div>
                    @endif

                    <div id="AddMoreFileId" class="addbtn plusButton1" style="display:none">
                        <a href="#" id="AddMoreFileBox" class="" style="color: grey !important">+</a> 
                    </div>

                </div>
                <div class="" id="display_inputs"></div>
                    @if(isset($getSinglePc) && !empty($getSinglePc))
                        @if(isset($getSinglePc[0]->seccondary_mobile_number) && !empty($getSinglePc[0]->seccondary_mobile_number))
                        <div class="input-field col l3 m4 s12" id="secondaryMobile">
                            <label for="contactNum11" class="dopr_down_holder_label active">Secondary Mobile Number: <span class="red-text">*</span></label>
                                <div  class="sub_val">
                                    <select  id="secondary_country_code_id" name="country_code" class="select2 browser-default">
                                        @foreach($country as $country1)
                                        <option value="{{$country1->country_code_id}}" @if(isset($getSinglePc)) @if( isset($getSinglePc[0]->secondary_country_code_id) ) @if( $country1->country_code_id == $getSinglePc[0]->primary_country_code_id) selected @endif @endif @endif  >{{ucwords($country1->country_code)}}</option>
                                        @endforeach    
                                    </select>
                                </div>
                                <input type="text" onkeypress="return onlyNumberKey(event)" class="mobile_number" name="seccondary_mobile_number" id="seccondary_mobile_number" required value="{{$getSinglePc[0]->seccondary_mobile_number}}" > 
                                <a href="javascript:void(0)" class="removeclass rmvbtn" onClick="removeDivSecMo()" style="color: red !important">-</a>
                        </div>
                        @endif
                    @endif
                        

                   

                        
                       
                </div>
                

                <div class="row">

             
                  

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                    <div  class="sub_val no-search">
                        <select  id="country_code_wa" name="country_code_wa" class="select2 browser-default">
                        @foreach($country as $country2)
                        <option value="{{$country2->country_code_id}}" @if(isset($getSinglePc)) @if( isset($getSinglePc[0]->secondary_wa_countryc_code) ) @if( $country2->country_code_id == $getSinglePc[0]->primary_wa_countryc_code) selected @endif @endif @endif>{{ucwords($country2->country_code)}}</option>
                        @endforeach
                        </select>
                    </div>
                    <label for="whatsapp_number_primary" class="dopr_down_holder_label active">Primary Whatsapp Number: <span class="red-text">*</span></label>
                       @php $primary_wa_number=''; @endphp
                       @if(isset($getSinglePc))@if(isset($getSinglePc[0]->primary_wa_number) && !empty($getSinglePc[0]->primary_wa_number)) @php $primary_wa_number = $getSinglePc[0]->primary_wa_number; @endphp @endif @endif
                    <input type="text" class="mobile_number" onkeypress="return onlyNumberKey(event)" name="whatsapp_number_primary" id="whatsapp_number_primary"  placeholder="Enter" value="{{$primary_wa_number}}">
                    <div  class="addbtn"  style="top: 2px !important; right: 27px !important;width: 16px !important;"  >
                        <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                    </div>

                    @if(isset($getSinglePc) && !empty($getSinglePc))
                        @if(isset($getSinglePc[0]->secondary_wa_number) && !empty($getSinglePc[0]->secondary_wa_number))
                        @else
                            <div id="AddMoreFileIdWhatsapp" class="addbtn" >                    
                            <a href="#" id="AddMoreFileBoxWhatsapp" class="" style="color: grey !important">+</a> 
                            </div>
                        @endif
                    @else
                    <div id="AddMoreFileIdWhatsapp" class="addbtn" >                    
                    <a href="#" id="AddMoreFileBoxWhatsapp" class="" style="color: grey !important">+</a> 
                    </div>
                    @endif
                    
                    <div id="AddMoreFileIdWhatsapp" class="addbtn plusButton2" style="display:none" >                    
                        <a href="#" id="AddMoreFileBoxWhatsapp" class="" style="color: grey !important">+</a> 
                    </div>       

                </div> 
                <div class="" id="display_inputs_whatsapp1"></div>

                @if(isset($getSinglePc) && !empty($getSinglePc))
                    @if(isset($getSinglePc[0]->secondary_wa_number) && !empty($getSinglePc[0]->secondary_wa_number))
                    <div class="input-field col l3 m4 s12 secondaryWaMobile" id="InputsWrapperr2 ">
                        <label for="alterWhatsapp" class="dopr_down_holder_label active">Secondary Whatsapp  Number: <span class="red-text">*</span></label>
                            <div  class="sub_val">
                                <select  id="secondary_wa_countryc_code" name="country_code" class="select2 browser-default">
                                    @foreach($country as $country2)
                                    <option value="{{$country2->country_code_id}}" @if(isset($getSinglePc)) @if( isset($getSinglePc[0]->primary_wa_countryc_code) ) @if( $country2->country_code_id == $getSinglePc[0]->primary_wa_countryc_code) selected @endif @endif @endif>{{ucwords($country2->country_code)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="text"  onkeypress="return onlyNumberKey(event)" class="mobile_number" name="secondary_wa_number" id="secondary_wa_number" required value="{{$getSinglePc[0]->secondary_wa_number}}" > 
                            <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                <input type="checkbox" id="copyAlterwhatsapp" data-toggle="tooltip" title="Check if alternate whatsapp number is same" onClick="copyAlterMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                                </div> <a href="javascript::void(0)" onClick="removeDivSecWaMo()" class="removeclassW rmvbtn" style="color: red !important">-</a></div>
                    @endif
               @endif


               <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                    <label for="primary_email active" class="active">Primary Email Address: <span class="red-text">*</span></label>
                       @php $primary_email=''; @endphp
                       @if(isset($getSinglePc))@if(isset($getSinglePc[0]->primary_email) && !empty($getSinglePc[0]->primary_email)) @php $primary_email = $getSinglePc[0]->primary_email; @endphp @endif @endif
                    <input type="email" class="validate" name="primary_email" id="primary_email"  placeholder="Enter" value="{{$primary_email}}">
                    
                    @if(isset($getSinglePc) && !empty($getSinglePc))
                        @if(isset($getSinglePc[0]->secondary_email) && !empty($getSinglePc[0]->secondary_email))
                        @else
                        <div id="AddMoreFileIdemail" class="addbtn">
                            <a href="#" id="AddMoreFileBox3" class="" style="color: grey !important">+</a> 
                        </div>
                        @endif

                    @else
                    <div id="AddMoreFileIdemail" class="addbtn">
                        <a href="#" id="AddMoreFileBox3" class="" style="color: grey !important">+</a> 
                    </div>                    
                    @endif

                    <div id="AddMoreFileIdemail" class="addbtn plusButton3" style="display:none">
                        <a href="#" id="AddMoreFileBox3" class="" style="color: grey !important">+</a> 
                    </div>

                </div>
                <div class="" id="display_inputs3"></div> 

                @if(isset($getSinglePc) && !empty($getSinglePc))
                    @if(isset($getSinglePc[0]->secondary_email) && !empty($getSinglePc[0]->secondary_email))
                    <div class="input-field col l3 m4 s12 secondaryEmail" id="InputsWrapper3">
                        <label for="secondary_email active" class="active">Secondary Email: <span class="red-text">*</span></label>
                            <input type="email" class="validate" name="secondary_email" id="secondary_email" required value="{{$getSinglePc[0]->secondary_email}}">
                            <a  style="color: red !important" href="javascript::void(0)" onClick="removeDivSecEmail()" class="removeclass13 rmvbtn">-</a></div>
                    @endif
               @endif
                     
                        
                </div>

                <div class="row">

              

               <div class="input-field col l3 m4 s12 display_search">
                                
                                <select  id="approachable" name="approachable" class="validate select2 browser-default" onChange="getsublocation(this)">
                                    <option value="" selected>Select</option>
                                    <option value="yes" @if(isset($getSinglePc) && !empty($getSinglePc)) @if(isset($getSinglePc[0]->approachable)) @if( $getSinglePc[0]->approachable == 'yes') selected  @endif @endif  @endif>Yes</option>
                                    <option value="no" @if(isset($getSinglePc) && !empty($getSinglePc)) @if(isset($getSinglePc[0]->approachable)) @if( $getSinglePc[0]->approachable == 'no') selected  @endif @endif  @endif>No</option>
                                </select>
                                <label class="active">Approachable</label>
                        </div>

                <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  name="designation" id="designation">
                                <option value=""  selected>Select</option>
                                @foreach($designation as $designation)
                                <option value="{{$designation->dd_designation1_id}}"   @if(isset($getSinglePc) && !empty($getSinglePc)) @if(isset($getSinglePc[0]->designation)) @if($designation->dd_designation1_id == $getSinglePc[0]->designation) selected  @endif @endif  @endif >{{ucwords($designation->designation)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">Designation </label>
                            
                        </div>

                     
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Date of Birth  : </label>
                            @php $date_of_birth=''; @endphp
                            @if(isset($getSinglePc))@if(isset($getSinglePc[0]->date_of_birth) && !empty($getSinglePc[0]->date_of_birth)) @php $date_of_birth = $getSinglePc[0]->date_of_birth; @endphp @endif @endif
                            <input type="text" class="datepicker" name="date_of_birth" id="date_of_birth" value="{{$date_of_birth}}"   placeholder="Enter"  >                            
                        </div>


                       <div class="input-field col l3 m4 s12 display_search">                            
                            <select class="select2  browser-default"  id="status"  name="status">
                                    <option value="" selected >Select</option>
                                    <option value="yes"  @if(isset($getSinglePc) && !empty($getSinglePc)) @if(isset($getSinglePc[0]->status)) @if( $getSinglePc[0]->status == 'yes') selected  @endif @endif  @endif>Yes</option>
                                    <option value="no" @if(isset($getSinglePc) && !empty($getSinglePc)) @if(isset($getSinglePc[0]->status)) @if( $getSinglePc[0]->status == 'no') selected  @endif @endif  @endif>No</option>
                                </select>
                                <label for="leadtype37  " class="active">Member Active Status  </label>
                        </div>

                        
                </div>
                

               
                <div class="row">


                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">About him (Past,Present & Residence Address )  : </label>
                            @php $about_him=''; @endphp
                            @if(isset($getSinglePc))@if(isset($getSinglePc[0]->about_him) && !empty($getSinglePc[0]->about_him)) @php $about_him = $getSinglePc[0]->about_him; @endphp @endif @endif
                            <input type="text" class="validate" name="about_him" id="about_him" value="{{$about_him}}"   placeholder="Enter"  >                            
                        </div>
                       
                       

                        <div class="input-field col l3 m4 s12 display_search" style="display:none">                                
                            <label  class="active">Residence Address : </label>
                            @php $residence_address=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->residence_address) && !empty($getSinglePc[0]->residence_address)) @php $residence_address = $getSinglePc[0]->residence_address; @endphp @endif @endif
                            <input type="text" class="validate" name="residence_address" id="residence_address"    placeholder="Enter"  value="{{$residence_address}}">                             
                        </div>

                        
                        <div class="input-field col l3 m4 s12 display_search">                            
                            <select class="select2  browser-default"  id="moved_to"  name="moved_to">
                            <option value=""   selected>Select</option>
                                @foreach($branch as $branch)
                                    <option value="{{ $branch->pc_branch_id }}"  >{{ucfirst($branch->branch_name)}} - {{ucfirst($branch->company_name)}}</option>
                                @endforeach

                                <!-- @if(isset($getSinglePc) && !empty($getSinglePc)) @if(isset($getSinglePc[0]->moved_to)) @if( $branch->pc_branch_id  == $getSinglePc[0]->moved_to ) selected  @endif @endif  @endif -->
                                </select>
                                <label for="leadtype37  " class="active">Moved To  </label>
                        </div> 

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Photo </label>
                            <input type="file" name="photo"  id="photo" class="dropify"  data-default-file="" style="padding-top: 14px;">

                            @php $photo = '';@endphp 
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->photo) && !empty($getSinglePc[0]->photo)) @php 
                             $photo = $getSinglePc[0]->photo; @endphp @endif @endif
                             @if( !empty($photo) )
                             <a href="{{ asset('emp_photo/'.$photo ) }} " target="_blank">Photo</a>
                             @endif
                        </div>

                        <div class="col l3 s12">
                            <ul class="collapsible">
                                <li onClick="company_review()">
                                <div class="collapsible-header" id="col_company_review" >Employee Review & Rating</div>                            
                                </li>                                        
                            </ul>
                        </div>

                                              
                </div>
              

                  
                <div class="collapsible-body"  id='company_review' style="display:none">
                    <div class="row">
                      <input type="hidden" id="pc_employees_review_id">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
     
                            <label  class="active">Parameters:</label>
                               @php $parameters=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->parameters) && !empty($getSinglePc[0]->parameters)) @php $parameters = $getSinglePc[0]->parameters; @endphp @endif @endif
                            <input type="text" class="validate" name="parameters" id="parameters"    placeholder="Enter"  value="{{$parameters}}">                             
                       </div> 
                     
                      <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            
                            <label  class="active">Review: </label>
                            <input type="text" class="validate" name="review" id="review"    placeholder="Enter"  >                             

                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                           
                           <select  id="rating" name="rating" class="validate select2 browser-default rating">
                               <option value=""  selected>Select</option>
                               @foreach($rating as $val)
                                   <option value="{{$val->rating_id}}" >{{$val->rating}}</option>                                    
                                @endforeach
                               </select>
                               <label  class="active">Rating:</label>
                       </div> 

                        <!---->
                            <div class="input-field col l1 m1 s6 display_search">
                                <button class="save_saveReview btn-small  waves-effect waves-light" id="save_saveReview" onClick="saveReview()" type="button" name="action">@if(isset($pc_branch_id) && !empty($pc_branch_id)) Update @else Save @endif</button>                        

                            </div>    
                            
                            <div class="input-field col l2 m2 s6 display_search">
                            <a href="javascript:void(0)" onClick="clear_saveReview()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_saveReview"><i class="material-icons dp48">clear</i></a>

                            </div>    
                    </div><br>
                    <div class="row"> 
                      <table class="bordered" id="review_table" style="width: 100% !important" >
                          <thead>
                          <tr >
                          <th  style="color: white;background-color: #ffa500d4;">Sr No. </th>
                          <th  style="color: white;background-color: #ffa500d4;">Parameters</th>
                          <th  style="color: white;background-color: #ffa500d4;">Review</th>
                          <th  style="color: white;background-color: #ffa500d4;">Rating</th>
                          
                          <th  style="color: white;background-color: #ffa500d4;">Username</th>
                          <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                          <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                          </tr>
                          </thead>
                          <tbody>
                          @php $incID1 = count($getReviewData); @endphp
                              @if(isset($getReviewData) && !empty($getReviewData))
                              
                               @php $i3=count($getReviewData); @endphp
                                  @foreach($getReviewData as $val)
                                      <tr id="tr1_{{$val->pc_employees_review_id}}">
                                          <td id="sr1_{{$val->pc_employees_review_id}}">@php $incID= $i3--; @endphp {{$incID}}</td>
                                          <td  id="pa1_{{$val->pc_employees_review_id}}">{{$val->parameters}}</td>
                                          <td  id="re1_{{$val->pc_employees_review_id}}">{{$val->review}}</td>
                                          <td  id="rt1_{{$val->pc_employees_review_id}}">{{$val->rating}}</td>
                                      
                                          <td  id="us1_{{$val->pc_employees_review_id}}">{{$val->usernanme}}</td>
                                          <td  id="lu1_{{$val->pc_employees_review_id}}">{{$val->created_date}}</td>
                                          <td><a href='javascript:void(0)' onClick='updateReview({{$val->pc_employees_review_id}})' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelReview({{$val->pc_employees_review_id}})'>Delete</a>
                                          </td>
                                          
                                      </tr>
                                  @endforeach
                              @endif
                          </tbody>
                      </table>
                      <input type="hidden" id="incID1" value="{{$incID1}}">
                    </div>

                </div>

            

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="saveEmployee()" type="button" name="action">@if(isset($pc_employees_id) && !empty($pc_employees_id)) Update @else Save @endif</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <!-- <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Cancel</button>                         -->
                        <a href="/add-employee-information?pc_id={{$property_consultant_id}}" class="waves-effect waves-green btn-small" style="background-color: red;">Cancel</a>

                    </div>    
                </div> 

                <br>
                <div class="row"><span>Company Name : {{ucfirst($company[0]->company_name)}}</span></div>

                <div class="row">
                    <div class="tableFixHead">
                        <table class="bordered" id="looking_since_table" style="width: 100% !important" >
                            <thead>
                            <tr >
                            <th  style="color: white;background-color: #ffa500d4;">Member ID. </th>
                            <th  style="color: white;background-color: #ffa500d4;">Branch Name</th>
                            <th  style="color: white;background-color: #ffa500d4;"> Member Name </th>
                            <th  style="color: white;background-color: #ffa500d4;">Approachable</th>
                            <th  style="color: white;background-color: #ffa500d4;">Primary mobile  number</th>
                            <th  style="color: white;background-color: #ffa500d4;">Secondary mobile  number</th>
                            <th  style="color: white;background-color: #ffa500d4;">Primary Email address</th>
                            <!-- <th  style="color: white;background-color: #ffa500d4;">Secondary Whatsapp number</th> -->
                            <th  style="color: white;background-color: #ffa500d4;">Designation</th>
                            
                            <th  style="color: white;background-color: #ffa500d4;">DOB</th>
                            <!-- <th  style="color: white;background-color: #ffa500d4;">Rating</th> -->
                            <!-- <th  style="color: white;background-color: #ffa500d4;">Comment</th> -->
                            <th  style="color: white;background-color: #ffa500d4;">Member Active Status</th>
                            <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                            <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                            <!-- <th  style="color: white;background-color: #ffa500d4;">Focused Unit Type</th>
                            <th  style="color: white;background-color: #ffa500d4;">Focused Unit</th>
                            <th  style="color: white;background-color: #ffa500d4;">Focused Location</th>
                            <th  style="color: white;background-color: #ffa500d4;">Focused Sub Location</th>
                            <th  style="color: white;background-color: #ffa500d4;">Focused Sub Complex</th> -->
                            </tr>
                            </thead>
                            <tbody>
                                @if(isset($getPcData) && !empty($getPcData))
                                    @foreach($getPcData as $val)
                                    <?php
                              

                              $loc1 = array();
                                $decodeEle1 = json_decode($val->focused_location) ;
                                if(!empty($decodeEle1)){
                                  if(!empty($decodeEle1[0])){
                                  $get1 = DB::select("select location_name from dd_location where location_id in(".$decodeEle1[0].")  ");
                                  foreach($get1  as $key =>  $val1){
                                      // echo ).', ';
                                      array_push($loc1,ucwords($val1->location_name));
                                  } 
                                }
                              } 
                                $loc = array();
                                $decodeEle1 = json_decode($val->focused_sub_location) ;
                                if(!empty($decodeEle1)){
                                  if(!empty($decodeEle1[0])){
                                  $get1 = DB::select("select sub_location_name from dd_sub_location where sub_location_id in(".$decodeEle1[0].")  ");
                                  foreach($get1  as $key =>  $val1){
                                      // echo ).', ';
                                      array_push($loc,ucwords($val1->sub_location_name));
                                  } 
                                }
                              }
                              ?>

                                        <tr>
                                            <td>E-{{$val->pc_employees_id}}</td>
                                            <td>{{ucwords($val->sub_location_name)}}</td>
                                            <td>{{ucwords($val->employee_name)}}</td>
                                            <td>{{ucfirst($val->approachable)}}</td>
                                            <td> @if(!empty($val->primary_mobile_number)) ({{$val->primary_country_code}}) {{$val->primary_mobile_number}} @endif</td>
                                            <td> @if(!empty($val->seccondary_mobile_number)) ({{$val->secondary_country_code}}) {{$val->seccondary_mobile_number}} @endif</td>
                                            <td>{{$val->primary_email }}</td>
                                            
                                            <td>{{ucwords($val->designation1)}}</td>
                                            <td>{{$val->date_of_birth}}</td>
                                            <!-- <td>{{$val->employee_rating}}</td> -->
                                            <!-- <td>{{$val->comment}}</td> -->
                                            <td>{{ucwords($val->status)}}</td>
                                            <td>{{$val->created_date}}</td>
                                            <td><a href="/add-employee-information?pce_id={{$val->pc_employees_id}}&pc_id={{$val->property_consultant_id}}"><i class="material-icons dp48">edit</i></a> |
                                            <a href='javascript:void(0)' onClick='confirmDelEmp({{$val->pc_employees_id}})' title="Delete"><i class="material-icons dp48">delete</i></a>
                                            </td>
                                            <!-- <td></td>
                                            <td></td>
                                            <td><?php //print_r(implode(', ',$loc1));?></td>
                                            <td><?php// print_r(implode(', ',$loc));?></td> -->
                                            <td></td>
                                            
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                <input type="hidden" id="incID" value="">
          
            </div>
           
         
            

         
         
        </div>

        </div>


        
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->

<div class="modal" id="deleteReviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_Review" >
            <input type="hidden" class="pc_employees_review_id2" id="pc_employees_review_id2" name="pc_employees_review_id">
            <input type="hidden"  id="form_type" name="form_type" value="reviewBPC">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteReview()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>

<div class="modal" id="deleteEmpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-content">
                  <h5 style="text-align: center">Delete record</h5>
                  <hr>        
                  <form method="post" id="delete_Branch" >
                      <input type="hidden" class="pc_employees_id2" id="pc_employees_id2" name="pc_employees_id">
                      <input type="hidden"  id="form_type" name="form_type" value="PC_Emp">
                      <div class="modal-body">
                      <h5>Have you transferred all employee?</h5>
                          <h5>Are you sure want to delete this record?</h5>
                      
                      </div>
              
                      </div>
              
                  <div class="modal-footer" style="text-align: left;">
                          <div class="row">
                          <div class="input-field col l2 m2 s6 display_search">
                              <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                              <a href="javascript:void(0)" onClick="deleteBranch()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                          </div>    
                          
                          <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                              <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                          </div>    
                      </div> 
                  </div>
              </form>
          </div>
        <script>
            

function confirmDelReview(params) {
    var pc_employees_review_id = params;
    $('.pc_employees_review_id2').val(pc_employees_review_id);   
    $('#deleteReviewModal').modal('open');
}

function deleteReview() {
    var url = 'deleteCompletly';
    var form = 'delete_Review';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            var pc_employees_review_id = data.pc_employees_review_id;
            $('#tr1_'+pc_employees_review_id).remove();
            // $('#deleteAwardModal').hide();
            $('#deleteReviewModal').modal('close');
            
        }
    }); 
}

function confirmDelEmp(params) {
    var pc_employees_id = params;
    $('.pc_employees_id2').val(pc_employees_id);   
    $('#deleteEmpModal').modal('open');
}


function deleteBranch() {
    var url = 'deletePartially';
    var form = 'delete_Branch';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            window.location.reload();
            // window.location.href = "/add-employee-information?pc_id="+property_consultant_id; 
            
        }
    }); 
}


            
function copyMobileNo(){
   var checkBox = document.getElementById("copywhatsapp");
   var mobile_number = $('#mobile_number_primary').val();
   if (checkBox.checked == true){
       if(mobile_number != ''){
           $('#whatsapp_number_primary').val(mobile_number);   
       }else{
           alert("Enter mobile number first");
           document.getElementById("copywhatsapp").checked = false;
       }        
     } else {
       $('#whatsapp_number_primary').val('');
     }
}

function copyAlterMobileNo(){
    
   var checkBox = document.getElementById("copyAlterwhatsapp");
   var mobile_number = $('#seccondary_mobile_number').val();
  
   if( typeof mobile_number !=='undefined'){    
       if (checkBox.checked == true){
           if(mobile_number != ''){
               $('#secondary_wa_number').val(mobile_number);   
           }else{
               alert("Enter alternate mobile number first");
               document.getElementById("copyAlterwhatsapp").checked = false;
           }        
       } else {
           $('#secondary_wa_number').val('');
       }
   }else{
       
       alert("Enter alternate mobile number is not set");
       document.getElementById("copyAlterwhatsapp").checked = false;
   }
}

        function getCity(argument) {
            var city_id = argument;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(city_id) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {city_id:city_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                       
                        $('#state').val(data[0].dd_state_id).trigger('change'); 
                    // });
                  }else{
                    $('#city').empty();
                  }
                  }
                });
            }else{
              $('#city').empty();
            }
        }

        function getlocation(argument) {
            var location_id = argument;
           // alert(location_id); return;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(location_id) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {location_id:location_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data);// return;
                      if(data){
                        $('#city').val(data[0].city_id).trigger('change'); 
                        getCity(data[0].city_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }
        
        function getsublocation(argument) {
            var SublocationID = argument.value;
            // alert(SublocationID);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){

                        $('#location').val(data[0].location_id).trigger('change');  
                        getlocation(data[0].location_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }


function apply_css11(skip,attr, val=''){   
    var id =returnColArray11();           
    id.forEach(function(entry) {
        
        if(entry==skip){
            // alert(11)
            $('#'+skip).css(attr,'orange','!important');
            // $('#'+skip).attr('style', 'background-color: orange !important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){

            }else{
                $('#'+entry).css(attr,val);
            }
            
            //action_to_hide();            
            
        }        
    });
}

function hide_n_show11(skip){
   var id = collapsible_body_ids11();
   collapsible_body_ids11().forEach(function(entry) {
       // console.log(id);
       if(entry==skip){
           // alert(skip);
           var x = document.getElementById(skip);  
           $('#'+skip).css('background-color','rgb(234 233 230)');
           // alert(x)          
           if (x.style.display === "none") {
               // alert(1)
               
               x.style.display = "block";
           } else {
               // alert(2)
               // alert(skip);
               // $('#col_lead_info').removeAttr("style");
               // $('#col_lead_info').css('background-color','white',"!important");
               x.style.display = "none";
               $('#col_'+skip).css('background-color','white');

           }
       }else{          
           $('#'+entry).hide();
       }
       
   });
}

function returnColArray11(){
   var a = Array('col_company_review');
   return a;
}

function collapsible_body_ids11(){
   var b = Array('company_review');
   return b;
}
function company_review(){    
   apply_css11('col_company_review','background-color','');
   hide_n_show11("company_review");     

}



function saveReview() {
  var form_data = new FormData();
      form_data.append("parameters", document.getElementById('parameters').value);
      form_data.append("rating", document.getElementById('rating').value);
      form_data.append("review", document.getElementById('review').value);
      form_data.append("form_type", 'review');
      form_data.append("pc_employees_id", $('#pc_employees_id').val());
      
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/saveDataReview",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data);//return;

        //  const obj = JSON.parse(data);

         // console.log(obj); //return;
        //  $('#project_id2').val(data.project_id);
         // if(typeof obj['company_id'] !== 'undefined'){
            $('#pc_employees_id').val(data.pc_employees_id);
         // }
         //$('#uploaded_image').html(data);
         var incPros = parseInt($('#incID1').val()) +1;
         var html = '<tr id="sr1_'+data.pc_employees_review_id+'">';
         html += '<td>' + incPros + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="pa1_'+data.pc_employees_review_id+'">' + data.parameters + '</td>';// obj['company_pros'] +'</td>';
         html += '<td id="re1_'+data.pc_employees_review_id+'">' + data.review + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="rt1_'+data.pc_employees_review_id+'">' + data.rating + '</td>';
         html += '<td id="us1_'+data.pc_employees_review_id+'">' + data.username + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="lu1_'+data.pc_employees_review_id+'">' + data.updated_date + '</td>';// obj['company_cons'] +'</td>';         
         html += "<td> <a href='javascript:void(0)' onClick='updateReview("+data.pc_employees_review_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelReview("+data.pc_employees_review_id+")'>Delete</a></td>";   
         html += '</tr>';
         $('#review_table').prepend(html);
         $('#rating').val('').trigger('change');
         $('#review').val('');
         $('#parameters').val('');
        //  $('#incPros').val(incPros);

        //  $('#loader1').css('display','none');
        //  $('#submitPros').css('display','block');
      }
      });
}


function updateReview(param) {
   var pc_employees_review_id = param;
      var form_data = new FormData();
      form_data.append("form_type", 'review');
      form_data.append("pc_employees_review_id", pc_employees_review_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
      $.ajax({
         url:"/getDataReview",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
            { 
               var data  = data[0];
               console.log(data); //return;
               $('#pc_employees_review_id').val(data.pc_employees_review_id);
               // $('#looking_since1').val(data.looking_since).trigger('change');
               $('#parametrs').val(data.parametrs);
           
               $('#rating').val(data.rating).trigger('change');
               $('#review').val(data.review);
               var anchor=document.getElementById("save_saveReview");
               anchor.innerHTML="Update";
               $("#save_saveReview").attr("onclick","updateReview1()");
            }
      });
}

function updateReview1() {


  // $('#loader3').css('display','block');
  // $('#submitLooking').css('display','none');

   var form_data = new FormData();
   


   form_data.append("form_type", 'review');
   form_data.append('pc_employees_review_id', $('#pc_employees_review_id').val()); 
   form_data.append("parameters", document.getElementById('parameters').value); 
   form_data.append("rating", document.getElementById('rating').value);
   form_data.append("review", document.getElementById('review').value);

   $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
   });

   $.ajax({
      url:"/updateTableRecordsReview",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
          console.log(data);// return;
         var pc_employees_review_id = data.pc_employees_review_id;
         $('#pa1_'+pc_employees_review_id).html(data.data['parameters']);
         $('#re1_'+pc_employees_review_id).html(data.data['review']);
         $('#rt1_'+pc_employees_review_id).html(data.data['rating']);
         $('#us1_'+pc_employees_review_id).html(data.data['username']);
         $('#lu1_'+pc_employees_review_id).html(data.data['updated_date']);
        //  $('#loader3').css('display','none');
        //  $('#submitLooking').css('display','block');         
      }
   });

}

function clear_saveReview() {
  $('#pc_employees_review_id').val('');
  $('#rating').val('').trigger('change');
  $('#review').val('');
  $('#parameters').val('');

  var anchor=document.getElementById("save_saveReview");
   anchor.innerHTML="Save";
   $("#save_saveReview").attr("onclick","saveReview()");
}

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}


function saveEmployee() {
// var tenat_tenant_id = $('#tenat_tenant_id').val();
// alert(tenat_tenant_id); return;

if($('#branch').val() == ''){
    alert('Select Branch');
    return false;
}


 var form_data = new FormData();
 
 
form_data.append('pc_employees_id', $('#pc_employees_id').val());
form_data.append('property_consultant_id', $('#property_consultant_id').val());
form_data.append('branch', $('#branch').val());
form_data.append('employee_name', $('#employee_name').val());
form_data.append('approachable', $('#approachable').val());
form_data.append('primary_country_code_id', $('#mobile_conuntry_code').val());

form_data.append('secondary_country_code_id', $('#secondary_country_code_id').val());


form_data.append('primary_wa_countryc_code', $('#country_code_wa').val());
//form_data.append('primary_wa_number', $('#primary_wa_number').val());
form_data.append('secondary_wa_countryc_code', $('#secondary_wa_countryc_code').val());

var primary_mobile_number = $('#mobile_number_primary').val();
if(typeof primary_mobile_number != 'undefined'){
    form_data.append('primary_mobile_number', primary_mobile_number);
}else{
    form_data.append('primary_mobile_number', '');
}

var seccondary_mobile_number = $('#seccondary_mobile_number').val();
if(typeof seccondary_mobile_number != 'undefined'){
    form_data.append('seccondary_mobile_number', seccondary_mobile_number);
}else{
    form_data.append('seccondary_mobile_number', '');
}

var primary_wa_number = $('#whatsapp_number_primary').val();
if(typeof primary_wa_number != 'undefined'){
    form_data.append('primary_wa_number', primary_wa_number);
}else{
    form_data.append('primary_wa_number', '');
}


var secondary_wa_number = $('#secondary_wa_number').val();
if(typeof secondary_wa_number != 'undefined'){
    form_data.append('secondary_wa_number', secondary_wa_number);
}else{
    form_data.append('secondary_wa_number', '');
}



var designation = $('#designation').val();
if(designation != null){
    form_data.append('designation',designation );
}else{
    form_data.append('designation','' );
}

var primary_email = $('#primary_email').val()
if(typeof primary_email != 'undefined'){
    form_data.append('primary_email', primary_email);
}else{
    form_data.append('primary_email', '');
}

var secondary_email = $('#secondary_email').val()
if(typeof secondary_email != 'undefined'){
    form_data.append('secondary_email', secondary_email);
}else{
    form_data.append('secondary_email', '');
}

// form_data.append('primary_email',   $('#primary_email').val());
// form_data.append('secondary_email',   $('#secondary_email').val());
// alert(primary_email);

form_data.append('date_of_birth', $('#date_of_birth').val());
form_data.append('rating', $('#rating').val());
// form_data.append('comment', $('#comment').val());
form_data.append('status', $('#status').val());
form_data.append('about_him', $('#about_him').val());

form_data.append('residence_address', $('#residence_address').val());
form_data.append('moved_to', $('#moved_to').val());
 form_data.append("photo", document.getElementById('photo').files[0]);


$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$.ajax({
url:"/saveEmployee",
method:"POST",
data: form_data,
contentType: false,
cache: false,
processData: false,
success:function(data)
{  
    var property_consultant_id =  $('#property_consultant_id').val();
    console.log(data); // return;
    if(data.error == 'failed'){
        console.log(data.msg); 
    }else{
        // window.location.reload();
        window.location.href = "/add-employee-information?pc_id="+property_consultant_id; 

    }
    //return;
    //location.reload();
    
    
    
}
});


}
  </script>
        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     