<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

      @if($getSinglePc)
            @if(isset($getSinglePc[0]->focused_sub_location ) && !empty($getSinglePc[0]->focused_sub_location ))
                        @php $config2 = json_decode($getSinglePc[0]->focused_sub_location);
                        $config22  = $config2[0];        
                        @endphp                
                    <input type="hidden" id="SubLoc" value="<?php print_r($config22);?>" style="display:none !important">
            @endif            

            @if(isset($getSinglePc[0]->focused_complex ) && !empty($getSinglePc[0]->focused_complex ))
                        @php $config23 = json_decode($getSinglePc[0]->focused_complex);
                        $config232  = $config23[0];        
                        @endphp                
                    <input type="hidden" id="Compl" value="<?php print_r($config232);?>" style="display:none !important">
            @endif 
        @endif  

          @if(isset($getSinglePc) && !empty($getSinglePc)) 
            @if( isset($getSinglePc[0]->focused_location) ) 
            <script>
                $(document).ready(function(){
                    getD();
                });
            </script>                
                         
            @endif 

            @if( isset($getSinglePc[0]->focused_sub_location) ) 
            <script>
                $(document).ready(function(){
                    
                    var SubLoc = $('#SubLoc').val();
                    if( SubLoc != null || SubLoc !='' || typeof SubLoc != 'undefined' ){ 
                        var numbersArray = SubLoc.split(',');   
                        getD(numbersArray);
                        // $('.focused_sub_location').val(numbersArray).trigger("change");
                    }
                    


                });
            </script>            
            @endif 



        @endif

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">

    <div class="row">
          <!-- <div id="loader" class="center"></div> -->
          <div class="row">                                        
                <div class="col l3 s12">
                <a href="/add-new-pc?pc_id={{$property_consultant_id}}" style="color:#000000b3">
                    <ul class="collapsible">
                        <li >
                        <div class="collapsible-header" id="col_company_info" >Company Info</div>
                        </li>                                        
                    </ul>
                </a>
                </div>

                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li >
                        <div class="collapsible-header" id="col_branch_info" tabindex="0" style="background-color: orange;">Branch Info</div>
                        
                        </li>                                        
                    </ul>
                </div>

                <div class="col l3 s12">
                <a href="/add-employee-information?pc_id={{$property_consultant_id}}" style="color:#000000b3">
                    <ul class="collapsible">
                        <li onClick="employee_info()">
                        <div class="collapsible-header" id="col_employee_info">Member Info</div>
                        
                        </li>                                        
                    </ul>
                </a>
                </div>

                <div class="col l3 s12">
                    <a href="/pc-master" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> Back To Home Page</a>
                </div>

          </div> 
        <div class="row">
           
           
            
               

         

        <input type="hidden"  id="pc_branch_id" value="{{$pc_branch_id}}">
        <div class="collapsible-body"  id='budget_loan' style="display:block" >
                <div class="row">
                
                     <div class="input-field col l3 m4 s12 display_search" style="display:none !important">                                
                                <select  id="company_name" name="company_name" class="validate select2 browser-default" onChange="showintoB(this)" >
                                <!-- <option value="" disabled selected>Select</option> -->
                                @if(isset($company) && !empty($company))
                                    @foreach($company as $company2)
                                    <option value="{{$company2->property_consultant_id}}"  selected>{{ ucwords($company2->company_name)}}</option>
                                    @endforeach
                                @endif
                                </select>
                                <label class="active">Company Name</label>
                        </div>

                         <div class="input-field col l3 m4 s12" id="InputsWrapper2" style="display:none !important">
                            <label  class="active">Branch ID: </label>
                            @php $pc_branch_id=''; @endphp
                            @if(isset($getSinglePc))@if(isset($getSinglePc[0]->pc_branch_id) && !empty($getSinglePc[0]->pc_branch_id)) @php $pc_branch_id = 'B-'.$getSinglePc[0]->pc_branch_id; @endphp @endif @endif
                            <input type="text" readonly class="validate" name="pc_branch_id" id="pc_branch_id" value="{{$pc_branch_id}}"   placeholder=" "  >                            
                        </div> 
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2" style="display:none !important">
                        <label  class="active">Branch / Sub Location name: </label>
                        @php $branch_name=''; @endphp
                        @if(isset($getSinglePc))@if(isset($getSinglePc[0]->branch_name) && !empty($getSinglePc[0]->branch_name)) @php $branch_name = $getSinglePc[0]->branch_name; @endphp @endif @endif
                        <input type="text" class="validate" name="branch_name" id="branch_name" value="{{$branch_name}}"   placeholder="Enter"  >
                            
                        </div>

                       
         
                  
                        

                      <div class="input-field col l3 m4 s12 display_search"> 
                            @if(isset($getSinglePc))
                                    @if(isset($getSinglePc[0]->focused_location ) && !empty($getSinglePc[0]->focused_location ))
                                        @php $focused_location2 = json_decode($getSinglePc[0]->focused_location ); 
                                            $focused_location2 = explode(",",$focused_location2[0]);     
                                        @endphp
                                    @endif
                                @endif
                            <select class="select2  browser-default" multiple="multiple" id="focused_location"  name="focused_location" onChange="getD(this)">
                           
                                @if(isset($location) && !empty($location))
                                    @foreach($location as $location1)
                                    <option value="{{$location1->location_id}}"    @if(isset($getSinglePc) && !empty($getSinglePc)) @if(in_array( $location1->location_id,$focused_location2)) selected  @endif    @endif>{{ ucwords($location1->location_name)}}</option>
                                    @endforeach
                                @endif
                                </select>
                                <label for="leadtype37  " class="active">Focused Location  </label>
                        </div>

                        <script>
                           
                           var configId = [];
                           var tempVal = [];
                            function getD(numbersArray){
                                $("#focused_location option").each(function() {
                                var val = $(this).val();
                                var tempVal = $("#focused_location").val();
                            
                                
                                if(tempVal.indexOf(val) >= 0 && configId.indexOf(val) < 0) {
                                configId.push(val);
                                } else if(tempVal.indexOf(val) < 0 && configId.indexOf(val) >= 0) {
                                configId.splice(configId.indexOf(val) , 1);
                                }
                                
                                })

                                
                            
                                $.ajax({
                                    url: '/getFocusedSublocation/',
                                    type: "GET",
                                    data : {locationIds:configId},
                                    // dataType: "json",
                                    success:function(data) {
                                        // console.log('numbersArray'+ ); //return;
                                    if(data.fetSublocation){
                                        
                                        $('.focused_sub_location').empty();
                                        $('.focused_sub_location').focus;
                                        //$('.focused_sub_location').append('<option value="" selected disabled> Select </option>'); 
                                        $.each(data.fetSublocation, function(key, value){
                                        $('select[name="focused_sub_location"]').append('<option value="'+ value.sub_location_id +'">' + value.sub_location_name.charAt(0).toUpperCase()+ value.sub_location_name.slice(1)+  '</option>');               
                                        }); 
                                        

                

                                        if(numbersArray !='' || typeof numbersArray == 'undefined' || numbersArray != null){ 
                                            $('.focused_sub_location').val(numbersArray).trigger('change');
                                            
                                        }

                                        }else{
                                        
                                            $('.focused_sub_location').empty();
                                        }

                              
                                }
                            });

                            }
                        </script>

                        <div class="input-field col l3 m4 s12 display_search">  
                         <!-- @if(isset($getSinglePc))
                                 @if(isset($getSinglePc[0]->focused_sub_location ) && !empty($getSinglePc[0]->focused_sub_location ))
                                    @php $focused_sub_location2 = json_decode($getSinglePc[0]->focused_sub_location ); 
                                        $focused_sub_location2 = explode(",",$focused_sub_location2[0]);     
                                    @endphp
                                @endif
                            @endif                           -->
                            <select class="select2  browser-default focused_sub_location" multiple="multiple" id="focused_sub_location"  name="focused_sub_location" onChange="getC(this)" >
                                  <!-- @foreach($sub_location as $val1)
                                        <option value="{{$val1->sub_location_id}}"  @if(isset($getSinglePc) && !empty($getSinglePc)) @if(in_array( $val1->sub_location_id,$focused_sub_location2)) selected  @endif    @endif>{{ucwords($val1->sub_location_name)}}</option>                                    
                                    @endforeach -->
                                </select>
                                <label for="leadtype37  " class="active">Focused Sub Location  </label>
                        </div>


                        <div class="input-field col l3 m4 s12 display_search">      
                           <!-- @if(isset($getSinglePc))
                        
                                 @if(isset($getSinglePc[0]->focused_complex ) && !empty($getSinglePc[0]->focused_complex ))
                                    @php $focused_complex2 = json_decode($getSinglePc[0]->focused_complex ); 
                                        $focused_complex2 = explode(",",$focused_complex2[0]);     
                                    @endphp
                                @endif
                            @endif  -->

                            <select class="select2  browser-default focused_complex"  id="focused_complex"  name="focused_complex" multiple='true'>
                                    <!-- @foreach($complex as $complex)
                                        <option value="{{$complex->project_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if(in_array( $complex->project_id, $focused_complex2)) selected  @endif @endif>{{$complex->project_complex_name}}</option>
                                    @endforeach -->
                                </select>
                                <label for="leadtype37  " class="active">Focused Complex  </label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                            
                        @if(isset($getSinglePc))
                                 @if(isset($getSinglePc[0]->deal_type ) && !empty($getSinglePc[0]->deal_type ))
                                    @php $deal_type2 = json_decode($getSinglePc[0]->deal_type ); 
                                        $deal_type2 = explode(",",$deal_type2[0]);     
                                    @endphp
                                @endif
                            @endif   

                            <select class="select2  browser-default"  id="deal_type"  name="deal_type" multiple="true">
                            @foreach($deal_type as $deal_type1)
                                        <option value="{{$deal_type1->deal_type_id}}"  @if(isset($getSinglePc) && !empty($getSinglePc)) @if(in_array( $deal_type1->deal_type_id, $deal_type2)) selected  @endif @endif >{{ucwords($deal_type1->deal_type)}}</option>
                                    @endforeach
                                </select>
                                <label for="leadtype37  " class="active"> Transaction type  </label>
                        </div>
               
                        
                       
                </div>
                
                
                <script>
                           
                           var configId1 = [];
                           var tempVal1 = [];
                            function getC(numbersArray1=null){
                                $("#focused_sub_location option").each(function() {
                                var val = $(this).val();
                                var tempVal1 = $("#focused_sub_location").val();
                            
                                
                                if(tempVal1.indexOf(val) >= 0 && configId1.indexOf(val) < 0) {
                                configId1.push(val);
                                } else if(tempVal.indexOf(val) < 0 && configId1.indexOf(val) >= 0) {
                                configId1.splice(configId1.indexOf(val) , 1);
                                }
                                
                                })
                            
                                $.ajax({
                                    url: '/getFocusedComplex/',
                                    type: "GET",
                                    data : {sublocationIds:configId1},
                                    // dataType: "json",
                                    success:function(data) {
                                        console.log(data);// return;
                                   
                                        if(data){
                                                console.log('test')
                                                $('.focused_complex').empty();
                                                $('.focused_complex').focus;
                                                //$('.focused_complex').append('<option value="" selected disabled> Select </option>'); 
                                                $.each(data, function(key, value){
                                                    var ids = value.society_id +'-'+value.project_id;
                                                    var bn ='';
                                                    if(value.building_name != ''){
                                                        bn = ' - '+value.building_name.charAt(0).toUpperCase()+ value.building_name.slice(1);
                                                    }
                                                $('select[name="focused_complex"]').append('<option value="'+ ids +'">' + value.project_complex_name.charAt(0).toUpperCase()+ value.project_complex_name.slice(1)+bn+'</option>');               
                                                });     

                                                var Compl = $('#Compl').val();
                                                //   alert(Compl)
                                                    if( Compl != null && typeof Compl != 'undefined' ){ 
                                                        var numbersArray1 = Compl.split(',');   
                                                        if(numbersArray1 !='' || typeof numbersArray1 == 'undefined' || numbersArray1 != null){ 
                                                            // getC(numbersArray1);
                                                            // alert(numbersArray1)
                                                            $('.focused_complex').val(numbersArray1).trigger('change');
                                                        }
                                                    
                                                    }
                                                
                                                
                                            

                                        }else{
                                        
                                            $('.focused_complex').empty();
                                        }
                                }
                            });

                            }
                        </script>


                <div class="row">

                        <div class="input-field col l3 m4 s12 display_search"> 
                            @if(isset($getSinglePc))
                                 @if(isset($getSinglePc[0]->focused_unit_type ) && !empty($getSinglePc[0]->focused_unit_type ))
                                    @php $focused_unit_type2 = json_decode($getSinglePc[0]->focused_unit_type ); 
                                        $focused_unit_type2 = explode(",",$focused_unit_type2[0]);     
                                    @endphp
                                @endif
                            @endif                             
                            <select class="select2  browser-default" multiple="multiple" id="focused_unit_type1"  name="focused_unit_type">
                                    <!-- <option value="" selected >Select</option> -->
                                    @foreach($unit_type as $unit_type1)
                                        <option value="{{$unit_type1->unit_type_id}}"  @if(isset($getSinglePc) && !empty($getSinglePc)) @if(in_array( $unit_type1->unit_type_id, $focused_unit_type2)) selected  @endif @endif >{{ucwords($unit_type1->unit_type)}}</option>
                                    @endforeach
                                </select>
                                <label for="leadtype37  " class="active">Focused Unit type  </label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                            
                            @if(isset($getSinglePc))
                                 @if(isset($getSinglePc[0]->focused_unit_status ) && !empty($getSinglePc[0]->focused_unit_status ))
                                    @php $focused_unit_status2 = json_decode($getSinglePc[0]->focused_unit_status ); 
                                        $focused_unit_status2 = explode(",",$focused_unit_status2[0]);     
                                    @endphp
                                @endif
                            @endif   

                            <select class="select2  browser-default" multiple="multiple" id="focused_unit_status"  name="focused_unit_status">
              
                            @foreach($unit_status as $unit_status1)
                                        <option value="{{$unit_status1->unit_status_id}}"  @if(isset($getSinglePc) && !empty($getSinglePc)) @if(in_array( $unit_status1->unit_status_id, $focused_unit_status2)) selected  @endif @endif>{{strtoupper($unit_status1->unit_status)}}</option>
                                    @endforeach

                                </select>
                                <label for="leadtype37  " class="active">Focused Unit status  </label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                            
                            <select class="select2  browser-default"  id="ownership_status"  name="ownership_status">
                                <option value="" selected>Select</option>
                                <option value="operating from home-owned" @if(isset($getSinglePc) && !empty($getSinglePc[0]->ownership_status)) @if($getSinglePc[0]->ownership_status == 'operating from home-owned') selected  @endif @endif>Operating from Home-Owned</option>
                                <option value="operating from home-rented" @if(isset($getSinglePc) && !empty($getSinglePc[0]->ownership_status)) @if($getSinglePc[0]->ownership_status == 'operating from home-rented') selected  @endif @endif >Operating from Home-rented</option>
                                <option value="rented office" @if(isset($getSinglePc) && !empty($getSinglePc[0]->ownership_status)) @if($getSinglePc[0]->ownership_status == 'rented office') selected  @endif @endif>Rented Office</option>
                                <option value="owned office" @if(isset($getSinglePc) && !empty($getSinglePc[0]->ownership_status)) @if($getSinglePc[0]->ownership_status == 'owned office') selected  @endif @endif >Owned Office</option>
                                </select>
                                <label for="leadtype37  " class="active"> Operating From  </label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                            
                            <select class="select2  browser-default"  id="office_status"  name="office_status">
                                <option value="" selected>Select</option>
                                <option value="yes" @if(isset($getSinglePc) && !empty($getSinglePc[0]->office_status)) @if($getSinglePc[0]->office_status == 'yes') selected  @endif @endif >Yes</option>
                                <option value="no" @if(isset($getSinglePc) && !empty($getSinglePc[0]->office_status)) @if($getSinglePc[0]->office_status == 'no') selected  @endif @endif>No</option>
                                </select>
                                <label for="leadtype37  " class="active"> Branch Active Status  </label>
                        </div>


                        
                </div>
                

               
                <div class="row">


                        <div class="input-field col l3 m4 s12 display_search" style="display:none !important">

                        @if(isset($getSinglePc))
                                 @if(isset($getSinglePc[0]->lead_source_name ) && !empty($getSinglePc[0]->lead_source_name ))
                                    @php $lead_source_name2 = json_decode($getSinglePc[0]->lead_source_name ); 
                                        $lead_source_name2 = explode(",",$lead_source_name2[0]);     
                                    @endphp
                                @endif
                            @endif   
                                
                                <select class="select2  browser-default"  id="lead_source_name"  name="lead_source_name" multiple='true' >
                                   
                                    @foreach($lead_source as $leadSource)
                                        <option value="{{$leadSource->lead_source_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if(in_array( $leadSource->lead_source_id, $lead_source_name2)) selected  @endif @endif>{{ucwords($leadSource->lead_source_name)}}</option>
                                        
                                    @endforeach    
                                </select>
                                <label for="leadtype5" class="active">Marketing Activity
                                </label>                                             
                            </div> 

                            <div class="input-field col l3 m4 s12 display_search">        
                  
                  @if(isset($getSinglePc))
                      @if(isset($getSinglePc[0]->campaign_id ) && !empty($getSinglePc[0]->campaign_id ))
                          @php $campaign_id2 = json_decode($getSinglePc[0]->campaign_id ); 
                              $campaign_id2 = explode(",",$campaign_id2[0]);     
                          @endphp
                      @endif
                  @endif   

          <select class="select2  browser-default"  id="lead_campaign_name"  name="lead_campaign_name"  multiple='true' >
                  @foreach($lead_campaign as $leadCampaign)
                      <option value="{{$leadCampaign->lead_campaign_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if(in_array( $leadCampaign->lead_campaign_id, $campaign_id2)) selected  @endif @endif>{{ucwords($leadCampaign->lead_campaign_name)}}</option>
                      
              @endforeach    
          </select>

      
          <label for="lead_campaign_name" class="active">Campaign Name
          </label>                                             
      </div>


      <div class="input-field col l3 m4 s12 display_search">                            
                            <select class="select2  browser-default"  id="account_owner"  name="account_owner">
                                    <option value="" selected>Select</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $user->id == $getSinglePc[0]->account_owner) selected @endif @endif>{{ucfirst($user->name)}}</option>
                                    @endforeach
                                </select>
                                <label for="leadtype37  " class="active">Surabhi Realtor Member Name  </label>
                        </div>

                  
      
                        <div class="col l3 s12">
                          <ul class="collapsible" style="margin-top: 0px;">
                              <li onClick="company_address()" >
                              <div class="collapsible-header" id="col_company_address" >Address</div>                            
                              </li>                                        
                          </ul>
                        </div> 


                        <div class="col l3 s12">
                            <ul class="collapsible" style="margin-top: 0px;">
                                <li onClick="company_review()">
                                <div class="collapsible-header" id="col_company_review" >Branch Review & Rating</div>                            
                                </li>                                        
                            </ul>
                        </div>


                           

                </div>
                    
                <div class="collapsible-body"  id='company_address' style="display:none">
                        <div class="row">
                        <div class="input-field col l3 m4 s12 display_search">
                                
                                <select  id="sub_location" name="sub_location" class="validate select2 browser-default" onChange="getsublocation(this)">
                                <option value="" selected>Select</option>
                                    @foreach($sub_location as $val)
                                        <option value="{{$val->sub_location_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $val->sub_location_id == $getSinglePc[0]->sub_location) selected @endif @endif >{{ucwords($val->sub_location_name)}}</option>                                    
                                    @endforeach
                                </select>
                                <label class="active">Sub Location</label>
                        </div>

                         <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="location1" name="location" class="validate  browser-default"  >
                                <option value="" disabled selected>Select</option>
                                @if(isset($location) && !empty($location))
                                    @foreach($location as $location2)
                                    <option value="{{$location2->location_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $location2->location_id == $getSinglePc[0]->location) selected @endif @endif >{{ ucwords($location2->location_name)}}</option>
                                    @endforeach
                                @endif
                                </select>
                                <label class="active">Location</label>
                        </div>
               
                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="city" name="city" class="validate  browser-default" >
                                 @if(isset($city) && !empty($city))
                                    @foreach($city  as $cty)
                                    <option value="{{$cty->city_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $cty->city_id == $getSinglePc[0]->city) selected @endif @endif >{{ ucfirst($cty->city_name) }}</option>
                                    @endforeach
                                   @endif                              
                                </select>
                                <label class="active">City</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="state" name="state" class="validate  browser-default" >
                                <option value="" disabled selected>Select</option>
                                     @foreach($state as $state)
                                    <option value="{{$state->dd_state_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $state->dd_state_id == $getSinglePc[0]->state) selected @endif @endif  >{{ ucwords($state->state_name)}}</option>
                                    @endforeach                                  
                                </select>
                                <label class="active">State</label>
                        </div>

                        

                        <div class="input-field col l3 m4 s12 display_search">                                
                            <label  class="active">Latitude & Longitude: </label>
                            @php $latitude_and_longitude=''; @endphp
                                @if(isset($getSinglePc))@if(isset($getSinglePc[0]->latitude_and_longitude) && !empty($getSinglePc[0]->latitude_and_longitude)) @php $latitude_and_longitude = $getSinglePc[0]->latitude_and_longitude; @endphp @endif @endif
                                <input type="text" class="validate" name="lat_and_long" id="lat_and_long"    placeholder="Enter"  value="{{$latitude_and_longitude}}">                             
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                            <label  class="active">Branch Postal Address: </label>
                            @php $postal_address=''; @endphp
                                @if(isset($getSinglePc))@if(isset($getSinglePc[0]->postal_address) && !empty($getSinglePc[0]->postal_address)) @php $postal_address = $getSinglePc[0]->postal_address; @endphp @endif @endif
                                <input type="text" class="validate" name="postal_address" id="postal_address"    placeholder="Enter"  value="{{$postal_address}}">                             
                        </div>
                        </div>
                </div>


                
                <div class="collapsible-body"  id='company_review' style="display:none">
                    <div class="row">
                      <input type="hidden" id="property_consultant_review_id">
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
        
                                <label  class="active">Parameters:</label>
                                @php $parameters=''; @endphp
                                @if(isset($getSinglePc))@if(isset($getSinglePc[0]->parameters) && !empty($getSinglePc[0]->parameters)) @php $parameters = $getSinglePc[0]->parameters; @endphp @endif @endif
                                <input type="text" class="validate" name="parameters" id="parameters"    placeholder="Enter"  value="{{$parameters}}">                             
                        </div> 
                        
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                
                                <label  class="active">Review: </label>
                                <input type="text" class="validate" name="review" id="review"    placeholder="Enter"  >                             

                            </div>

                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            
                            <select  id="rating" name="rating" class="validate select2 browser-default rating">
                                <option value=""  selected>Select</option>
                                @foreach($rating as $val)
                                    <option value="{{$val->rating_id}}" >{{$val->rating}}</option>                                    
                                    @endforeach
                                </select>
                                <label  class="active">Rating:</label>
                        </div> 

                        <!---->
                            <div class="input-field col l1 m1 s6 display_search">
                                <button class="save_saveReview btn-small  waves-effect waves-light" id="save_saveReview" onClick="saveReview()" type="button" name="action">Save</button>                        

                            </div>    
                            
                            <div class="input-field col l1 m1 s6 display_search">
                            <a href="javascript:void(0)" onClick="clear_saveReview()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_saveReview"><i class="material-icons dp48">clear</i></a>

                            </div>    
                    </div><br>
                    <div class="row"> 
                      <table class="bordered" id="review_table" style="width: 100% !important" >
                          <thead>
                          <tr >
                          <th  style="color: white;background-color: #ffa500d4;">Sr No. </th>
                          <th  style="color: white;background-color: #ffa500d4;">Parameters</th>
                          <th  style="color: white;background-color: #ffa500d4;">Review</th>
                          <th  style="color: white;background-color: #ffa500d4;">Rating</th>
                          <th  style="color: white;background-color: #ffa500d4;">Username</th>
                          <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                          <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                          </tr>
                          </thead>
                          <tbody>
                          @php $incID1 = count($getReviewData); @endphp
                              @if(isset($getReviewData) && !empty($getReviewData))
                              
                               @php $i3=count($getReviewData); @endphp
                                  @foreach($getReviewData as $val)
                                      <tr id="tr1_{{$val->property_consultant_review_id}}">
                                          <td id="sr1_{{$val->property_consultant_review_id}}">@php $incID= $i3--; @endphp {{$incID}}</td>
                                          <td  id="pa1_{{$val->property_consultant_review_id}}">{{$val->parameters}}</td>
                                          <td  id="re1_{{$val->property_consultant_review_id}}">{{$val->review}}</td>
                                          <td  id="rt1_{{$val->property_consultant_review_id}}">{{$val->rating}}</td>
                                          <td  id="us1_{{$val->property_consultant_review_id}}">{{$val->usernanme}}</td>
                                          <td  id="lu1_{{$val->property_consultant_review_id}}">{{$val->created_date}}</td>
                                          <td><a href='javascript:void(0)' onClick='updateReview({{$val->property_consultant_review_id}})' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelReview({{$val->property_consultant_review_id}})'>Delete</a>
                                          </td>
                                          
                                      </tr>
                                  @endforeach
                              @endif
                          </tbody>
                      </table>
                      <input type="hidden" id="incID1" value="{{$incID1}}">
                    </div>

                </div>
            

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="saveBranch()" type="button" name="action">@if(isset($pc_branch_id) && !empty($pc_branch_id)) Update @else Save @endif</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <!-- <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Cancel</button>                         -->
                        <a href="/add-branch-information?pc_id={{$property_consultant_id}}" class="waves-effect waves-green btn-small" style="background-color: red;">Cancel</a>

                    </div>    
                </div> 

                <br>
                <div class="row"><span>Company Name : @if(isset($company) && !empty($company)) {{ucfirst($company[0]->company_name)}} @endif</span></div>
                <div class="row">
                <table class="bordered" id="looking_since_table" style="width: 100% !important" >
                    <thead>
                    <tr >
                    <th  style="color: white;background-color: #ffa500d4;">Branch ID. </th>
                    <th  style="color: white;background-color: #ffa500d4;">Branch Sub Location</th>
                    <th  style="color: white;background-color: #ffa500d4;">Branch Address</th>
                    <th  style="color: white;background-color: #ffa500d4;">Focused Sub Location</th>
                    <th  style="color: white;background-color: #ffa500d4;">Focused Location</th>
                    <th  style="color: white;background-color: #ffa500d4;">Focused Complex</th>
                    <th  style="color: white;background-color: #ffa500d4;">Transaction Type</th>
                    <th  style="color: white;background-color: #ffa500d4;">Focused Unit Type</th>
                    <th  style="color: white;background-color: #ffa500d4;">Focused Unit Staus</th>
                    <th  style="color: white;background-color: #ffa500d4;">Operating From</th>                    
                    <th  style="color: white;background-color: #ffa500d4;">Branch Active Status</th>
                    <th  style="color: white;background-color: #ffa500d4;">SR Account Owner</th>
                    <th  style="color: white;background-color: #ffa500d4;">Campaign Name</th>
                    <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                    <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                    </tr>
                    </thead>
                    <tbody>
                        @if(isset($getPcData) && !empty($getPcData))
                            @foreach($getPcData as $val)
                                <tr>
                                    <td>B-{{$val->pc_branch_id}}</td>
                                    <td>{{ucwords($val->sub_location_name)}}</td>
                                    <td>{{$val->postal_address}}</td>                                
                                    <td>
                                    <?php
                                            $decodeEle1 = json_decode($val->focused_sub_location) ;
                                            if(!empty($decodeEle1)){
                                              if(!empty($decodeEle1[0])){
                                              $get5 = DB::select("select sub_location_name from dd_sub_location where sub_location_id in(".$decodeEle1[0].")  ");
                                              foreach($get5 as $val1){
                                                echo ucwords($val1->sub_location_name).', ';
                                              } 
                                            }
                                          }
                                          ?>

                                    </td>
                                    <td>
                                    <?php
                                            $decodeEle2 = json_decode($val->focused_location) ;
                                            if(!empty($decodeEle2)){
                                              if(!empty($decodeEle2[0])){
                                              $get6 = DB::select("select location_name from dd_location where location_id in(".$decodeEle2[0].")  ");
                                              foreach($get6 as $val2){
                                                echo ucwords($val2->location_name).', ';
                                              } 
                                            }
                                          }
                                          ?>


                                    </td>
                                    <td>
                                    <?php
                                            $decodeEle22 = json_decode($val->focused_complex) ;
                                            if(!empty($decodeEle22)){
                                              if(!empty($decodeEle22[0])){
                                                // print_r($decodeEle22[0]);
                                                $ex = explode(',',$decodeEle22[0]);
                                                // print_r($ex);
                                                // $ex1 = implode(',',$ex);
                                                // foreach($ex as $v){
                                                //     $ar  =str_replace('-',',',$v);
                                                //     $fi = explode(',',$ar);
                                                //     $society_id = $fi[0];
                                                //     $project_id = $fi[1];
                                                //     $building_name  = '';
                                                //     $get62 = DB::select("select project_complex_name from op_project where project_id =".$project_id."  ");
                                                //     if(!empty($society_id)){
                                                //         $get63 = DB::select("select building_name from op_society where society_id =".$society_id."  ");
                                                //         $building_name = $get63[0]->building_name;
                                                //     }

                                                //     if(!empty($building_name )){
                                                //         $final_name = ucwords($get62[0]->project_complex_name).' - '. ucwords($building_name).',';
                                                //     }else{
                                                //         $final_name = ucwords($get62[0]->project_complex_name).',';
                                                //     }
                                                //    echo $final_name ;
                                                // }
                                            //   $get62 = DB::select("select project_complex_name from op_project where project_id in(".$decodeEle22[0].")  ");
                                            //   foreach($get62 as $val22){
                                            //     echo ucwords($val22->project_complex_name).', ';
                                            //   } 
                                            }
                                          }
                                          ?>

                                   </td>

                                    <td>
                                        <?php 
                                            $decodeEle4D = json_decode($val->deal_type) ;
                                            $decodeEle4D = explode(',',$decodeEle4D[0]);
                                           if(!empty($decodeEle4D[0])){
                                            $get6D = DB::select("select deal_type from dd_deal_type where deal_type_id in(".$decodeEle4D[0].")  ");
                                            foreach($get6D as $val2D){
                                              echo ucwords($val2D->deal_type).', ';
                                            } 
                                        }
                                        ?>
                                    </td>

                                    
                                    <td>
                                        <?php 
                                            $decodeEle4DF = json_decode($val->focused_unit_type) ;
                                            $decodeEle4DF = explode(',',$decodeEle4DF[0]);
                                           if(!empty($decodeEle4DF[0])){
                                            $get6DF = DB::select("select unit_type from dd_unit_type where unit_type_id in(".$decodeEle4DF[0].")  ");
                                            foreach($get6DF as $val2DF){
                                              echo ucwords($val2DF->unit_type).', ';
                                            } 
                                        }
                                        ?>
                                    </td>

                                    <td>
                                        <?php 
                                            $decodeEle4 = json_decode($val->focused_unit_status) ;
                                            $decodeEle4 = explode(',',$decodeEle4[0]);
                                           if(!empty($decodeEle4[0])){
                                            $get6 = DB::select("select unit_status from dd_unit_status where unit_status_id in(".$decodeEle4[0].")  ");
                                            foreach($get6 as $val2){
                                              echo strtoupper($val2->unit_status).', ';
                                            } 
                                        }
                                        ?>
                                    </td>
                                   
                                    <td>{{ucfirst($val->ownership_status)}}</td>
                                    <td>{{ucfirst($val->office_status)}}</td>
                                    <td>{{ucfirst($val->sr_account_owner)}}</td>    
                                    <td>
                                        <?php 
                                               $decodeEle15 = json_decode($val->campaign_id) ;
                                               if(!empty($decodeEle15)){
                                                 if(!empty($decodeEle15[0])){
                                                 $get55 = DB::select("select lead_campaign_name from dd_lead_campaign where lead_campaign_id in(".$decodeEle15[0].")  ");
                                                 foreach($get55 as $val15){
                                                   echo ucwords($val15->lead_campaign_name).', ';
                                                 } 
                                               }
                                             }
                                        ?>


                                    </td>                               
                                    <td>{{$val->created_date}}</td>
                                    <td><a href="/add-branch-information?bi_id={{$val->pc_branch_id}}&pc_id={{$val->property_consultant_id}}"title="Edit"><i class="material-icons dp48">edit</i></a> 
                                    <a href='javascript:void(0)' onClick='confirmDelPC({{$val->pc_branch_id}})' title="Delete"><i class="material-icons dp48">delete</i></a>
                                    </td>
                                    
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <input type="hidden" id="incID" value="">
          
            </div>
           
         
            

         
         
        </div>

        </div>


        
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->
        
        <div class="modal" id="deletePCModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-content">
                  <h5 style="text-align: center">Delete record</h5>
                  <hr>        
                  <form method="post" id="delete_PC" >
                      <input type="hidden" class="pc_branch_id2" id="pc_branch_id2" name="pc_branch_id">
                      <input type="hidden"  id="form_type" name="form_type" value="PC_branch">
                      <div class="modal-body">
                      <h5>Have you transferred all employees?</h5>
                          <h5>Are you sure want to delete this record?</h5>
                      
                      </div>
              
                      </div>
              
                  <div class="modal-footer" style="text-align: left;">
                          <div class="row">
                          <div class="input-field col l2 m2 s6 display_search">
                              <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                              <a href="javascript:void(0)" onClick="deletePC()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                          </div>    
                          
                          <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                              <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                          </div>    
                      </div> 
                  </div>
              </form>
          </div>


          <!-- review modal -->

<div class="modal" id="deleteReviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_Review" >
            <input type="hidden" class="property_consultant_review_id2" id="property_consultant_review_id2" name="property_consultant_review_id">
            <input type="hidden"  id="form_type" name="form_type" value="reviewPC">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteReview()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>

        <script>

function confirmDelPC(params) {
    var pc_branch_id = params;
    $('.pc_branch_id2').val(pc_branch_id);   
    $('#deletePCModal').modal('open');
}

function deletePC() {
    var url = 'deletePartially';
    var form = 'delete_PC';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            window.location.reload();
            
        }
    }); 
}

                function showintoB(params) {
                    $('#branch_name').val(params.options[params.selectedIndex].text);
                }

            function hideShowCampaign(){
    var x = $('#lead_source_name').val();
    if(x==1 || x==2 || x==3 || x==4 || x==5 || x==6 || x==7 ){
        $("#lead_campaign_name").prop('disabled', false);
        
    }else{
        $("#lead_campaign_name").val('').trigger('change');
        $("#lead_campaign_name").prop('disabled', true);
    }
}

        function getCity(argument) {
            var city_id = argument;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(city_id) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {city_id:city_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                       
                        $('#state').val(data[0].dd_state_id).trigger('change'); 
                    // });
                  }else{
                    $('#state').empty();
                  }
                  }
                });
            }else{
              $('#state').empty();
            }
        }

        function getlocation(argument) {
            var location_id = argument;
           // alert(location_id); return;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(location_id) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {location_id:location_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data);// return;
                      if(data){
                        $('#city').val(data[0].city_id).trigger('change'); 
                        getCity(data[0].city_id);
                    // });
                  }else{
                    $('#city').empty();
                  }
                  }
                });
            }else{
              $('#city').empty();
            }
        }
        
        function getsublocation(argument) {
            var SublocationID = argument.value;
            // alert(SublocationID);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){

                        $('#location1').val(data[0].location_id).trigger('change');  
                        getlocation(data[0].location_id);
                    // });
                  }else{
                    $('#location1').empty();
                  }
                  }
                });
            }else{
              $('#location1').empty();
            }
        }


        
function apply_css22(skip,attr, val=''){   
    var id =returnColArray22();           
    id.forEach(function(entry) {
        if(entry==skip){
            $('#'+skip).css(attr,'orange','!important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){
            }else{
                $('#'+entry).css(attr,val);
            }
        }        
    });
}

function hide_n_show22(skip){
   var id = collapsible_body_ids22();
   collapsible_body_ids22().forEach(function(entry) {
       if(entry==skip){
           var x = document.getElementById(skip);  
           $('#'+skip).css('background-color','rgb(234 233 230)');
           if (x.style.display === "none") {
               x.style.display = "block";
           } else {
               x.style.display = "none";
               $('#col_'+skip).css('background-color','white');
           }
       }else{          
           $('#'+entry).hide();
       }
   });
}

function returnColArray22(){
   var a = Array('col_company_address','col_company_review','col_social_media');
   return a;
}

function collapsible_body_ids22(){
   var b = Array('company_address','company_review','social_media');
   return b;
}

function company_address() {
  apply_css22('col_company_address','background-color','');
   hide_n_show22("company_address");     
}


function company_review(){    
   apply_css22('col_company_review','background-color','');
   hide_n_show22("company_review");     

}


function saveBranch() {
// var tenat_tenant_id = $('#tenat_tenant_id').val();
// alert(tenat_tenant_id); return;
 var form_data = new FormData();
 
form_data.append('pc_branch_id', $('#pc_branch_id').val());
form_data.append('company_name', $('#company_name').val());
form_data.append('branch_name', $('#branch_name').val());
// form_data.append('latitude_and_longitude', $('#latitude_and_longitude').val());
form_data.append('address', $('#address').val());

var focused_sub_location = $('#focused_sub_location').val();
if(focused_sub_location != null){
    form_data.append('focused_sub_location[]',focused_sub_location );
}else{
    form_data.append('focused_sub_location[]','' );
}

var focused_location = $('#focused_location').val();
if(focused_location != null){
    form_data.append('focused_location[]',focused_location );
}else{
    form_data.append('focused_location[]','' );
}

var focused_complex = $('#focused_complex').val();
if(focused_complex != null){
    form_data.append('focused_complex[]',focused_complex );
}else{
    form_data.append('focused_complex[]','' );
}

// form_data.append('focused_complex', $('#focused_complex').val());

var focused_unit_type = $('#focused_unit_type1').val();
if(focused_unit_type != null){
    form_data.append('focused_unit_type[]',focused_unit_type );
}else{
    form_data.append('focused_unit_type[]','' );
}



var sub_location = $('#sub_location').val();
if(sub_location != null){
    form_data.append('sub_location',sub_location );
}else{
    form_data.append('sub_location','' );
}

var location = $('#location1').val();
if(location != null){
    form_data.append('location1',location );
}else{
    form_data.append('location1','' );
}

var city = $('#city').val();
if(city != null){
    form_data.append('city',city );
}else{
    form_data.append('city','' );
}


var state = $('#state').val();
if(state != null){
    form_data.append('state',state );
}else{
    form_data.append('state','' );
}

var focused_unit_status = $('#focused_unit_status').val();
if(focused_unit_status != null){
    form_data.append('focused_unit_status[]',focused_unit_status );
}else{
    form_data.append('focused_unit_status[]','' );
}

form_data.append('account_owner', $('#account_owner').val());
form_data.append('office_status', $('#office_status').val());
form_data.append('ownership_status', $('#ownership_status').val());


var campaign_id = $('#lead_campaign_name').val();
if(campaign_id != null){
    form_data.append('campaign_id[]',campaign_id );
}else{
    form_data.append('campaign_id[]','' );
}

var lead_source_name = $('#lead_source_name').val();
if(lead_source_name != null){
    form_data.append('lead_source_name[]',lead_source_name );
}else{
    form_data.append('lead_source_name[]','' );
}


form_data.append('latitude_and_longitude', $('#lat_and_long').val());
form_data.append('postal_address', $('#postal_address').val());
form_data.append('deal_type[]', $('#deal_type').val());


$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$.ajax({
url:"/saveBranch",
method:"POST",
data: form_data,
contentType: false,
cache: false,
processData: false,
success:function(data)
{  
    console.log(data); //return;
    window.location.reload();
   // window.location.href = "/add-branch-information?pc_id"; 
    
    
}
});


}



function saveReview() {
  var form_data = new FormData();
      form_data.append("parameters", document.getElementById('parameters').value);
      form_data.append("rating", document.getElementById('rating').value);
      form_data.append("review", document.getElementById('review').value);
      form_data.append("form_type", 'review');
      form_data.append("pc_branch_id", $('#pc_branch_id').val());
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/saveData",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data);//return;

        //  const obj = JSON.parse(data);

         // console.log(obj); //return;
        //  $('#project_id2').val(data.project_id);
         // if(typeof obj['company_id'] !== 'undefined'){
            // $('#pc_branch_id').val(data.pc_branch_id);
         // }
         //$('#uploaded_image').html(data);
         var incPros = parseInt($('#incID1').val()) +1;
         var html = '<tr id="tr1_'+data.property_consultant_review_id+'">';
         html += '<td>' + incPros + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="pa1_'+data.property_consultant_review_id+'">' + data.parameters + '</td>';// obj['company_pros'] +'</td>';
         html += '<td id="re1_'+data.property_consultant_review_id+'">' + data.review + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="rt1_'+data.property_consultant_review_id+'">' + data.rating + '</td>';
         html += '<td id="us1_'+data.property_consultant_review_id+'">' + data.username + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="lu1_'+data.property_consultant_review_id+'">' + data.updated_date + '</td>';// obj['company_cons'] +'</td>';         
         html += "<td> <a href='javascript:void(0)' onClick='updateReview("+data.property_consultant_review_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelReview("+data.property_consultant_review_id+")'>Delete</a></td>";   
         html += '</tr>';
         $('#review_table').prepend(html);
         $('#rating').val('').trigger('change');
         $('#review').val('');
         $('#parameters').val('');
         
        //  $('#incPros').val(incPros);

        //  $('#loader1').css('display','none');
        //  $('#submitPros').css('display','block');
      }
      });
}


function updateReview(param) {
   var property_consultant_review_id = param;
      var form_data = new FormData();
      form_data.append("form_type", 'review');
      form_data.append("property_consultant_review_id", property_consultant_review_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
      $.ajax({
         url:"/getData2",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
            { 
               var data  = data[0];
               console.log(data); //return;
               $('#property_consultant_review_id').val(data.property_consultant_review_id);
               // $('#looking_since1').val(data.looking_since).trigger('change');
            //   alert()
           
               $('#parameters').val(data.parameters);
                
                $('#rating').val(data.rating).trigger('change');
                $('#review').val(data.review);
               var anchor=document.getElementById("save_saveReview");
               anchor.innerHTML="Update";
               $("#save_saveReview").attr("onclick","updateReview1()");
            }
      });
}

function updateReview1() {


  // $('#loader3').css('display','block');
  // $('#submitLooking').css('display','none');

   var form_data = new FormData();
   


   form_data.append("form_type", 'review');
   form_data.append('property_consultant_review_id', $('#property_consultant_review_id').val()); 

   form_data.append("parameters", document.getElementById('parameters').value); 
   form_data.append("rating", document.getElementById('rating').value);
   form_data.append("review", document.getElementById('review').value);

   $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
   });

   $.ajax({
      url:"/updateTableRecords1",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
          console.log(data);// return;
         var property_consultant_review_id = data.property_consultant_review_id;
         $('#pa1_'+property_consultant_review_id).html(data.data['parameters']);
         $('#re1_'+property_consultant_review_id).html(data.data['review']);
         $('#rt1_'+property_consultant_review_id).html(data.data['rating']);
         $('#us1_'+property_consultant_review_id).html(data.data['username']);
         $('#lu1_'+property_consultant_review_id).html(data.data['updated_date']);
        //  $('#loader3').css('display','none');
        //  $('#submitLooking').css('display','block');         
      }
   });

}

function clear_saveReview() {
  $('#property_consultant_review_id').val('');
  $('#rating').val('').trigger('change');
  $('#review').val('');
  $('#parameters').val('');

  var anchor=document.getElementById("save_saveReview");
   anchor.innerHTML="Save";
   $("#save_saveReview").attr("onclick","saveReview()");
}


function confirmDelReview(params) {
    var property_consultant_review_id2 = params;
    $('.property_consultant_review_id2').val(property_consultant_review_id2);   
    $('#deleteReviewModal').modal('open');
}


function deleteReview() {
    var url = 'deleteCompletly';
    var form = 'delete_Review';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            var property_consultant_review_id = data.property_consultant_review_id;
            $('#tr1_'+property_consultant_review_id).remove();
            // $('#deleteAwardModal').hide();
            $('#deleteReviewModal').modal('close');
            
        }
    }); 
}
  </script>
        


<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     