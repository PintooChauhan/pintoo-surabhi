<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <!-- <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}"> -->
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/css/select.dataTables.min.css"> -->
  
      <!-- <script src="app-assets/js/scripts/data-tables.js"></script> -->
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.0/css/fixedColumns.dataTables.min.css"> -->
      <!-- END PAGE LEVEL JS-->


<style>
#scroll-vert-hor{
  width: 100% !important;
}
div.dataTables_wrapper {
    width: 100% !important;
    margin: 0 auto;
}
select {
    display: initial !important; ;
}

/*::-webkit-scrollbar {
  display: none;
}*/
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.dataTables_filter input {
    border: 1px solid #aaa;
    border-radius: 3px;
    padding: 0px !important;
}
 


.dataTables_scrollBody{
    height: auto !important;
}




/* .tableFixHead {
  overflow: auto;
 
 
}*/

.tableFixHead table {
  border-collapse: collapse;
  width: 100%;
}

.tableFixHead th,
.tableFixHead td {
  padding: 8px 22px;
}


td:first-child, th:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:white;
}
td:nth-child(2),th:nth-child(2)  { 
position:sticky;
  left:67px;
  z-index:1;
  background-color:white;
  }
 
  td:nth-child(3),th:nth-child(3)  { 
  position:sticky;
  left:133px;
  z-index:1;
  background-color:white;
  
  }

  td:nth-child(4),th:nth-child(4)  { 
  position:sticky;
   left: 265px;/*301px; */
  z-index:1;
  background-color:white;
  }
 
/*
  td:nth-child(5),th:nth-child(5)  { 
  position:sticky;
  left:340px;
  z-index:1;
  background-color:white;
  }

  td:nth-child(6),th:nth-child(6)  { 
  position:sticky;
  left:460px;
  z-index:1;
  background-color:white;
  }

  td:nth-child(7),th:nth-child(7)  { 
  position:sticky;
  left:544px;
  z-index:1;
  background-color:white;
  }

  td:nth-child(8),th:nth-child(8)  { 
  position:sticky;
  left:627px;
  z-index:1;
  background-color:white;
  }
 */
.tableFixHead th {
  position: sticky;
  top: 0;
  background: #eee;
  z-index:2
}
th:first-child , th:nth-child(2) , th:nth-child(3), th:nth-child(4)  {
  z-index:3
  } 
  /*, th:nth-child(5), th:nth-child(6) , th:nth-child(7), th:nth-child(8) 
  
  table.dataTable thead .sorting_asc {
    background-image: none;
}*/
/*  */
</style>



      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">
          <div class="row">
            <div class="col s10"></div>
            <div class="col s2"><a href="/add-new-pc"  class="btn-small  waves-effect waves-light">Add NEW PC</a></div>
          </div>
 <br>
 <div class="row">
    <div class="col s12">
        <!-- <div class="card"> -->
            <!-- <div class="card-content"> -->
                
                <div class="row">
                    <div class="col s12">
                        <div class="tableFixHead">
                          <div class="row">
                            <table class="bordered" id="looking_since_table" style="width: 100% !important" >
                                <thead>
                                <tr >
                                <th  style="color: white;background-color: #ffa500d4;">PC ID </th>
                                <th  style="color: white;background-color: #ffa500d4;">Company Name</th>
                                <th  style="color: white;background-color: #ffa500d4;">No. Of Branch</th>
                                <th  style="color: white;background-color: #ffa500d4;">Regd. Office address</th>
                                <th  style="color: white;background-color: #ffa500d4;">No. of Employees</th>
                                <th  style="color: white;background-color: #ffa500d4;">No. of years</th>
                                <th  style="color: white;background-color: #ffa500d4;">Location</th>
                                <th  style="color: white;background-color: #ffa500d4;">Sub Location</th>
                                <th  style="color: white;background-color: #ffa500d4;">City</th>            
                                <th  style="color: white;background-color: #ffa500d4;">Rating</th>
                                <th  style="color: white;background-color: #ffa500d4;">RERA No.</th>  
                                <th  style="color: white;background-color: #ffa500d4;">Username</th>
                                <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                                <th  style="color: white;background-color: #ffa500d4;">Action </th>               
                                </tr>
                                </thead>
                                <tbody>
                                    @if(isset($getPcData) && !empty($getPcData))
                                        @foreach($getPcData as $val)

                                      
                                            <tr>
                                                <td id="sr_{{$val->property_consultant_id}}">PC- {{$val->property_consultant_id}}</td>
                                                <td  id="co_{{$val->property_consultant_id}}"><a href="/add-new-pc?pc_id={{$val->property_consultant_id}}"> {{ucwords($val->company_name)}} - {{ucwords($val->firm_type)}}</a></td>
                                                <td id="nob_{{$val->property_consultant_id}}">{{$val->branch_count}}</td>
                                                <td  id="rea_{{$val->property_consultant_id}}">{{$val->office_address}}</td>
                                                <td  id="noy_{{$val->property_consultant_id}}">{{$val->employee_count}}</td>
                                                <td  id="noy_{{$val->property_consultant_id}}">{{$val->no_of_years}}</td>
                                                <td  id="fi_{{$val->property_consultant_id}}">{{ucwords($val->location1)}}</td>
                                                <td  id="fi_{{$val->property_consultant_id}}">{{ucwords($val->sub_location1)}}</td>
                                                <td  id="fi_{{$val->property_consultant_id}}">{{ucwords($val->city1)}}</td>                                 
                                                <td  id="rat_{{$val->property_consultant_id}}">{{$val->company_rating}}</td>                          
                                                <td  id="rat_{{$val->property_consultant_id}}">{{$val->rera_number}}</td>  
                                                <td  id="us_{{$val->property_consultant_id}}">{{$val->usernanme}}</td>
                                                <td  id="lu_{{$val->property_consultant_id}}">{{$val->created_date}}</td>
                                                <td>
                                                  <!-- <a href="/add-new-pc?pc_id={{$val->property_consultant_id}}" title="Edit"><i class="material-icons dp48">edit</i></a> | -->
                                                <a href='javascript:void(0)' onClick='confirmDelPC({{$val->property_consultant_id}})' title="Delete"><i class="material-icons dp48">delete</i></a>
                                                </td>
                                                
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <input type="hidden" id="incID" value="">
                          </div>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
        <!-- </div> -->
    </div>
</div>


                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
            </div> -->

            <div class="modal" id="deletePCModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-content">
                  <h5 style="text-align: center">Delete record</h5>
                  <hr>        
                  <form method="post" id="delete_PC" >
                      <input type="hidden" class="property_consultant_id2" id="property_consultant_id2" name="property_consultant_id">
                      <input type="hidden"  id="form_type" name="form_type" value="PC">
                      <div class="modal-body">
                          
                          <h5>Are you sure want to delete this record?</h5>
                      
                      </div>
              
                      </div>
              
                  <div class="modal-footer" style="text-align: left;">
                          <div class="row">
                          <div class="input-field col l2 m2 s6 display_search">
                              <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                              <a href="javascript:void(0)" onClick="deletePC()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                          </div>    
                          
                          <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                              <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                          </div>    
                      </div> 
                  </div>
              </form>
          </div>

  <script>

function confirmDelPC(params) {
    var property_consultant_id = params;
    $('.property_consultant_id2').val(property_consultant_id);   
    $('#deletePCModal').modal('open');
}


function deletePC() {
    var url = 'deletePartially';
    var form = 'delete_PC';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            window.location.reload();
            
        }
    }); 
}


  </script>


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <!-- <script src="app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> -->
    <!-- <script src="app-assets/vendors/data-tables/js/dataTables.select.min.js"></script> -->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <!-- <script src="app-assets/js/plugins.js"></script> -->
    <!-- <script src="app-assets/js/search.js"></script> -->
    <!-- <script src="app-assets/js/custom/custom-script.js"></script> -->
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <!-- END PAGE LEVEL JS-->