<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
            
               <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">@if(isset($property_consultant_id) && !empty($property_consultant_id)) UPDATE @else ADD  @endif NEW PC</span><hr>  </div>

         

       
        <div class="collapsible-body"  id='budget_loan' style="display:block" >
        <input type="hidden"  id="property_consultant_id" value="{{$property_consultant_id}}">
                <div class="row">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Company name: </label>
                        @php $company_name=''; @endphp
                        @if(isset($getSinglePc))@if(isset($getSinglePc[0]->company_name) && !empty($getSinglePc[0]->company_name)) @php $company_name = $getSinglePc[0]->company_name; @endphp @endif @endif
                        <input type="text" class="validate" name="company_name" id="company_name" value="{{$company_name}}"   placeholder="Enter"  >
                            
                        </div>
                        

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                           
                            <select  id="firm_type" name="firm_type" class="validate select2 browser-default firm_type">
                                <option value=""  selected>Select</option>
                                    <option value="propritor"  @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $getSinglePc[0]->firm_type == 'propritor' ) selected @endif @endif >Propritor</option>                                    
                                    <option value="llp"   @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $getSinglePc[0]->firm_type == 'llp' ) selected @endif @endif >LLP</option>
                                    <option value="pvt ltd"   @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $getSinglePc[0]->firm_type == 'pvt ltd' ) selected @endif @endif >Pvt Ltd</option>
                                    <option value="indivisual"   @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $getSinglePc[0]->firm_type == 'indivisual' ) selected @endif @endif >Indivisual</option>
                                </select>
                                <label  class="active">Firm Type:</label>
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">RERA Number : </label>
                            @php $rera_number=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->rera_number) && !empty($getSinglePc[0]->rera_number)) @php $rera_number = $getSinglePc[0]->rera_number; @endphp @endif @endif
                            <input type="text" class="validate" name="rera_number" id="rera_number"   placeholder="Enter" value="{{$rera_number}}" >                             
                        </div>  

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Registered Office Add. : </label>
                             @php $office_address=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->office_address) && !empty($getSinglePc[0]->office_address)) @php $office_address = $getSinglePc[0]->office_address; @endphp @endif @endif
                            <input type="text" class="validate" name="reg_office_address" id="reg_office_address"   placeholder="Enter"  value="{{$office_address}}">                             
                        </div>

                        
                       
                </div>
                

                <div class="row">

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Web Site : </label>
                            @php $web_site=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->web_site) && !empty($getSinglePc[0]->web_site)) @php $web_site = $getSinglePc[0]->web_site; @endphp @endif @endif
                            <input type="text" class="validate" name="web_site" id="web_site"   placeholder="Enter"  value="{{$web_site}}">                             
                        </div>  

                        <div class="input-field col l3 m4 s12" id="InputsWrapper">
                            <label for="contactNum1" class="dopr_down_holder_label active">Office Number : <span class="red-text">*</span></label>
                            <div  class="sub_val no-search">
                                <select  id="mobile_conuntry_code" name="mobile_conuntry_code" class="select2 browser-default">
                                @foreach($country as $country1)
                                <option value="{{$country1->country_code_id}}" @if(isset($getCustomerData)) @if( isset($getCustomerData[0]->primary_country_code_id) ) @if( $country1->country_code_id == $getCustomerData[0]->primary_country_code_id) selected @endif @endif @endif  >{{ucwords($country1->country_code)}}</option>
                                @endforeach
                                </select>
                            </div>
                            @php $office_number=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->office_number) && !empty($getSinglePc[0]->office_number)) @php $office_number = $getSinglePc[0]->office_number; @endphp @endif @endif
                            <input type="text" class="validate" name="office_number" id="office_number"   placeholder="Enter" value="{{$office_number}}" >                             

                        </div>

                       

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Office Email ID : </label>
                            @php $email_id=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->email_id) && !empty($getSinglePc[0]->email_id)) @php $email_id = $getSinglePc[0]->email_id; @endphp @endif @endif
                            <input type="email" class="validate" name="office_email_id" id="office_email_id"   placeholder="Enter"  value="{{$email_id}}">                             
                        </div>  

                      
                        <div class="input-field col l3 m4 s12 display_search">
                                
                                <select  id="sub_location" name="sub_location" class="validate select2 browser-default" onChange="getsublocation(this)">
                                <option value="" selected>Select</option>
                                    @foreach($sub_location as $val)
                                        <option value="{{$val->sub_location_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $val->sub_location_id == $getSinglePc[0]->sub_location) selected @endif @endif >{{ucwords($val->sub_location_name)}}</option>                                    
                                    @endforeach
                                </select>
                                <label class="active">Sub Location</label>
                        </div>

                        
                </div>

                <div class="row">

                         <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="location" name="location" class="validate  browser-default"  >
                                <option value="" disabled selected>Select</option>
                                @if(isset($location) && !empty($location))
                                    @foreach($location as $location)
                                    <option value="{{$location->location_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $location->location_id == $getSinglePc[0]->location) selected @endif @endif >{{ ucwords($location->location_name)}}</option>
                                    @endforeach
                                @endif
                                </select>
                                <label class="active">Location</label>
                        </div>
               
                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="city" name="city" class="validate  browser-default" >
                                 @if(isset($city) && !empty($city))
                                    @foreach($city  as $cty)
                                    <option value="{{$cty->city_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $cty->city_id == $getSinglePc[0]->city) selected @endif @endif >{{ ucfirst($cty->city_name) }}</option>
                                    @endforeach
                                   @endif                              
                                </select>
                                <label class="active">City</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="state" name="state" class="validate  browser-default" >
                                <option value="" disabled selected>Select</option>
                                     @foreach($state as $state)
                                    <option value="{{$state->dd_state_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $state->dd_state_id == $getSinglePc[0]->state) selected @endif @endif  >{{ ucwords($state->state_name)}}</option>
                                    @endforeach                                  
                                </select>
                                <label class="active">State</label>
                        </div> 

                        <div class="input-field col l3 m4 s12 display_search">                                
                            <label  class="active">Residence Address : </label>
                            @php $residence_address=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->residence_address) && !empty($getSinglePc[0]->residence_address)) @php $residence_address = $getSinglePc[0]->residence_address; @endphp @endif @endif
                            <input type="text" class="validate" name="residence_address" id="residence_address"    placeholder="Enter"  value="{{$residence_address}}">                             
                        </div>

                        
                </div>
                

               
                <div class="row">
                         <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">About Company : </label>
                            @php $about_company=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->about_company) && !empty($getSinglePc[0]->about_company)) @php $about_company = $getSinglePc[0]->about_company; @endphp @endif @endif
                            <input type="text" class="validate" name="about_company" id="about_company"    placeholder="Enter" value="{{$about_company}}" >                             
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">About Owner : </label>
                            @php $about_owner=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->about_owner) && !empty($getSinglePc[0]->about_owner)) @php $about_owner = $getSinglePc[0]->about_owner; @endphp @endif @endif
                            <input type="text" class="validate" name="about_owner" id="about_owner"    placeholder="Enter"  value="{{$about_owner}}">                             
                              
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                            
                            <label  class="active">Inception  Year  : </label>
                            @php $inception_year=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->inception_year) && !empty($getSinglePc[0]->inception_year)) @php $inception_year = $getSinglePc[0]->inception_year; @endphp @endif @endif
                            <input type="text" class="datepicker" name="inception_year" id="inception_year"    placeholder="Enter" value="{{$inception_year}}" >                             
                        </div>  

                        <div class="input-field col l3 m4 s12 display_search">                                
                        <label  class="active">No of Years: </label>
                        @php $no_of_years=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->no_of_years) && !empty($getSinglePc[0]->no_of_years)) @php $no_of_years = $getSinglePc[0]->no_of_years; @endphp @endif @endif
                            <input type="text" class="validate" name="no_of_years" id="no_of_years"    placeholder="Enter"  value="{{$no_of_years}}">                             
                        </div>

                        
                </div>

                <div class="row">

                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="company_review()">
                            <div class="collapsible-header" id="col_company_review" >Company Review & Rating</div>                            
                            </li>                                        
                        </ul>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        
                        <label  class="active">Comments: </label>
                        @php $comment=''; @endphp
                             @if(isset($getSinglePc))@if(isset($getSinglePc[0]->comment) && !empty($getSinglePc[0]->comment)) @php $comment = $getSinglePc[0]->comment; @endphp @endif @endif
                        <input type="text" class="validate" name="comments" id="comments"    placeholder="Enter" value="{{$comment}}" >                             

                    </div>  

                                  
                </div>
                <div class="collapsible-body"  id='company_review' style="display:none">
                    <div class="row">

                        Fields not mentioned in excel.
                        <!-- <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            
                            <label  class="active">Review: </label>
                            <input type="text" class="validate" name="review" id="review"    placeholder="Enter"  >                             

                        </div>


                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                           
                           <select  id="rating" name="rating" class="validate select2 browser-default rating">
                               <option value=""  selected>Select</option>
                               @foreach($rating as $val)
                                   <option value="{{$val->rating_id}}" >{{$val->rating}}</option>                                    
                                @endforeach
                               </select>
                               <label  class="active">Rating:</label>
                       </div> -->

                    </div>

                </div>

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="savePc()" type="button" name="action">@if(isset($property_consultant_id) && !empty($property_consultant_id)) Update @else Save @endif</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <!-- <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Cancel</button>                         -->
                        <a href="/add-new-pc" class="waves-effect waves-green btn-small" style="background-color: red;">Cancel</a>

                    </div>    
                </div> 

                <br>

                <div class="row">
                <table class="bordered" id="looking_since_table" style="width: 100% !important" >
                    <thead>
                    <tr >
                    <th  style="color: white;background-color: #ffa500d4;">Sr No. </th>
                    <th  style="color: white;background-color: #ffa500d4;">Unit Type</th>
                    <th  style="color: white;background-color: #ffa500d4;">Unit</th>
                    <th  style="color: white;background-color: #ffa500d4;">Sublocation/location</th>
                    <th  style="color: white;background-color: #ffa500d4;">Company Name</th>
                    <th  style="color: white;background-color: #ffa500d4;">Contact Person</th>
                    <th  style="color: white;background-color: #ffa500d4;">Mobile No.</th>
                    <th  style="color: white;background-color: #ffa500d4;">Whatsapp</th>
                    <th  style="color: white;background-color: #ffa500d4;">Rating</th>
                    <th  style="color: white;background-color: #ffa500d4;">Sellers</th>
                    <th  style="color: white;background-color: #ffa500d4;">Tenants</th>
                    <th  style="color: white;background-color: #ffa500d4;">Owners</th>
                    <th  style="color: white;background-color: #ffa500d4;">Employees</th>
                    <th  style="color: white;background-color: #ffa500d4;">Last Updated Date</th>
                    <th  style="color: white;background-color: #ffa500d4;">Action</th>               
                    </tr>
                    </thead>
                    <tbody>
                        @if(isset($getPcData) && !empty($getPcData))
                            @foreach($getPcData as $val)
                                <tr>
                                    <td>{{$val->property_consultant_id}}</td>
                                    <td>?</td>
                                    <td>?</td>
                                    <td>{{ucwords($val->sub_location1)}} / {{ucwords($val->location1)}}</td>
                                    <td>{{$val->company_name}}</td>
                                    <td>?</td>
                                    <td>{{$val->office_number}}</td>
                                    <td>?</td>
                                    <td>?</td>
                                    <td>?</td>
                                    <td>?</td>
                                    <td>?</td>
                                    <td>?</td>
                                    <td>{{$val->created_date}}</td>
                                    <td><a href="/add-new-pc?pc_id={{$val->property_consultant_id}}">Edit</a></td>
                                    
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <input type="hidden" id="incID" value="">
          
            </div>
           
         
            

         
         
        </div>

        </div>


        
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->

        <script>
        function getCity(argument) {
            var city_id = argument;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(city_id) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {city_id:city_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                       
                        $('#state').val(data[0].dd_state_id).trigger('change'); 
                    // });
                  }else{
                    $('#city').empty();
                  }
                  }
                });
            }else{
              $('#city').empty();
            }
        }

        function getlocation(argument) {
            var location_id = argument;
           // alert(location_id); return;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(location_id) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {location_id:location_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data);// return;
                      if(data){
                        $('#city').val(data[0].city_id).trigger('change'); 
                        getCity(data[0].city_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }
        
        function getsublocation(argument) {
            var SublocationID = argument.value;
            // alert(SublocationID);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){

                        $('#location').val(data[0].location_id).trigger('change');  
                        getlocation(data[0].location_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }


        function apply_css11(skip,attr, val=''){   
    var id =returnColArray11();           
    id.forEach(function(entry) {
        
        if(entry==skip){
            // alert(11)
            $('#'+skip).css(attr,'orange','!important');
            // $('#'+skip).attr('style', 'background-color: orange !important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){

            }else{
                $('#'+entry).css(attr,val);
            }
            
            //action_to_hide();            
            
        }        
    });
}

function hide_n_show11(skip){
   var id = collapsible_body_ids11();
   collapsible_body_ids11().forEach(function(entry) {
       // console.log(id);
       if(entry==skip){
           // alert(skip);
           var x = document.getElementById(skip);  
           $('#'+skip).css('background-color','rgb(234 233 230)');
           // alert(x)          
           if (x.style.display === "none") {
               // alert(1)
               
               x.style.display = "block";
           } else {
               // alert(2)
               // alert(skip);
               // $('#col_lead_info').removeAttr("style");
               // $('#col_lead_info').css('background-color','white',"!important");
               x.style.display = "none";
               $('#col_'+skip).css('background-color','white');

           }
       }else{          
           $('#'+entry).hide();
       }
       
   });
}

function returnColArray11(){
   var a = Array('col_company_review');
   return a;
}

function collapsible_body_ids11(){
   var b = Array('company_review');
   return b;
}
function company_review(){    
   apply_css11('col_company_review','background-color','');
   hide_n_show11("company_review");     

}



  </script>
        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     