<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
      
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/4.1.0/css/fixedColumns.dataTables.min.css">

      <style>
  select
{
    display: block !important;
}
#scroll-vert-hor41{
    width:100% !important;
}
.green_color{
    color:#4caf50 ;
}

.red_color{
    color:#f44336;
}

</style>


      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">
    
        @php
        $check_s_add = 0;
        $check_s_edit = 0;
        $check_s_delete = 0;
        @endphp

        @if(isset($access_check) && !empty($access_check) )
          @foreach($access_check as $ackey=>$acvalue)
            @php
              $check_s_add = $acvalue->s_add;
              $check_s_edit = $acvalue->s_edit;
              $check_s_delete = $acvalue->s_delete;
            @endphp
          @endforeach
        @endif

              <div class="row">
                
                <form action="/seachByMobile" method="post">
                     @csrf
                <div class="col s6 m6">
                        <div class="input-field col l4 m4 s12" id="InputsWrapper2">
                            <label  class="active">Enter Mobile No : </label>
                            @php $mobile_number1=''; @endphp
                                @if(isset($mobile_number) && !empty($mobile_number)) @php $mobile_number1 = $mobile_number; @endphp  @endif 
                            <input type="number" class="validate" name="mobile_number" id="mobile_number" value='{{$mobile_number1}}'  placeholder="Enter"  required >                             
                        </div>  
                        <div class="input-field col l2 m2 s12" id="InputsWrapper2">                        
                            <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action"> Search </button>
                        </div>  
                        <div class="input-field col l2 m2 s12" id="InputsWrapper2">                        
                            <a href="/pc-master" class="waves-effect waves-green btn-small" style="background-color: red;">Clear</a>
                        </div>  

                </div>
            </form>
                <div class="col s2 m2"></div>

                @if(!empty($check_s_add) )
                    @if($check_s_add==1)
                    <div class="col s4 m4" style="text-align:right">
                    <a href="{{route('user.addUser')}}" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> Add New User</a>
                    </div>
                    @endif
                @endif
               </div>


 <div class="row">
    <div class="col s12">
        <div class="card">
          
            <div class="card-content">

                <div class="row">
                    <div class="col s12">
                        <div class="tableFixHead">
                            <table id="scroll-vert-hor41" class="display nowrap table-hover">
                            <thead>
                            <tr class="pin">
                                <th >Name</th>
                                <th >Email</th>
                                <th >Role</th>
                                <th >Last Updated Date</th>
                                @if($check_s_add==1 || $check_s_edit==1 || $check_s_delete==1 )
                                <th >Action </th>
                                @endif

                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($getUserData) && !empty($getUserData))
                                @foreach($getUserData as $key=>$val)

                                    <tr>                                                                                                           
                                        <td>{{$val->name}}</td>
                                        <td>{{$val->email}}</td>
                                        <td>{{$val->role}}</td>
                                        <td>{{$val->updated_at}}</td>

                                        @if($check_s_add==1 || $check_s_edit==1 || $check_s_delete==1 )
                                        <td>
                                            @if(!empty($check_s_edit) )
                                                @if($check_s_edit==1)
                                                <a href='javascript:void(0)' onClick='changePasswordUser({{$val->id}})' title="Change Password"><i class="material-icons dp48">lock</i></a>
                                                <a href="{{asset('addUser?user_id='.$val->id)}}" title="Edit"><i class="material-icons dp48">edit</i></a>
                                                @if($val->status=='1')
                                                    @php $status_color='green_color'; @endphp
                                                @else
                                                    @php $status_color='red_color'; @endphp
                                                @endif
                                                <a href='javascript:void(0)' onClick='confirmStatusUser({{$val->id}})' title="Enable/Disable" ><i class="material-icons dp48 {{$status_color}}">check</i></a>
                                                
                                                @if(!empty($check_s_delete) )
                                                    @if($check_s_delete==1)
                                                    <a href='javascript:void(0)' onClick='confirmDelUser({{$val->id}})' title="Delete User"><i class="material-icons dp48">delete</i></a>
                                                    @endif
                                                @endif

                                                @endif
                                            @endif
                                        </td>
                                        @endif

                                    </tr>
                                @endforeach
                                @endif
                               

                            </tbody>
                            
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        <div class="modal" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-content">
                  <h5 style="text-align: center">Delete record</h5>
                  <hr>        
                  <form method="post" id="delete_user" >
                      <input type="hidden" class="user_id" id="user_id" name="user_id">
                      <input type="hidden"  id="form_type" name="form_type" value="delete_user">
                      <div class="modal-body">
                          {{-- <h5>Have you transferred all employees?</h5> --}}
                          <h5>Are you sure want to delete this record?</h5>
                      
                      </div>
              
                      </div>
              
                  <div class="modal-footer" style="text-align: left;">
                          <div class="row">
                          <div class="input-field col l2 m2 s6 display_search">
                              <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                              <a href="javascript:void(0)" onClick="deleteUser()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                          </div>    
                          
                          <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                              <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                          </div>    
                      </div> 
                  </div>
              </form>
          </div>

          {{-- status Modal --}}

          <div class="modal" id="statusUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-content">
                <h5 style="text-align: center">Change Status</h5>
                <hr>        
                <form method="post" id="status_user" >
                    <input type="hidden" class="user_id" id="user_id" name="user_id">
                    <input type="hidden"  id="form_type" name="form_type" value="status_user">
                    <div class="modal-body">
                        {{-- <h5>Have you transferred all employees?</h5> --}}
                        <h5>Are you sure want to change status of this record?</h5>
                    
                    </div>
            
                    </div>
            
                <div class="modal-footer" style="text-align: left;">
                    <div class="row">
                        <div class="input-field col l2 m2 s6 display_search">
                            <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                            <a href="javascript:void(0)" onClick="statusUser()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                        </div>    
                        
                        <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                            <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                        </div>
    
                    </div> 
{{-- 
                    <div class="row">
                        <div class="col-12">
                            <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                                <p class="status_response"></p>
                            </div>    
                        </div>    
                    </div> --}}

                </div>
            </form>
        </div>

        {{-- Change Password Modal --}}
        <div class="modal" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-content">
                <h5 style="text-align: center">Change Password</h5>
                <hr>        
                <form method="post" id="status_user" >
                    <input type="hidden" class="user_id" id="user_id" name="user_id">
                    <input type="hidden"  id="form_type" name="form_type" value="status_user">
                    <div class="modal-body">
                        <div class="row" style="display:flex;justify-content:center;align-items:center;flex-direction:column;">
                            <div class="col-5">
                                
                                <div class="input-field col l11 m4 s12" id="InputsWrapper2">
                                    <label  class="active">New Password: <span class="red-text">*</span></label>
                                    <input type="password" class="validate" name="new_password" id="new_password" value=""   placeholder="Enter"  required />
                                        
                                </div>
                                <div class="input-field col l11 m4 s12" id="InputsWrapper2">
                                    <label  class="active">Confirm New Password: <span class="red-text">*</span></label>
                                    <input type="password" class="validate" name="confirm_new_password" id="confirm_new_password" value=""   placeholder="Enter"  required />
                                        
                                </div>

                            </div>
                        </div>

                        <div class="row change_update_form_error">
                            <div class="input-field col-12 l2 m2 s6 pl-1">
                                <ul></ul>
        
                            </div>   
                        </div> 

                    </div>
            
                </div>
            
                <div class="modal-footer" style="text-align: left;">
                        <div class="row">
                        <div class="input-field col l2 m2 s6 display_search">
                            <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                            <a href="javascript:void(0)" onClick="updatePasswordUser()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                        </div>    
                        
                        <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                            <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                        </div>    
                    </div> 
                </div>
            </form>
        </div>

<script>



function changePasswordUser(params) {
  // alert(params); return;   
    var user_id = params;
    $('.user_id').val(user_id);
    
    $('#changePasswordModal').modal('open');
}

function updatePasswordUser() 
{
    var form_data = new FormData();
    
    form_data.append('user_id', $('#user_id').val());
    form_data.append('new_password', $('#new_password').val());
    form_data.append('confirm_new_password', $('#confirm_new_password').val());

    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
    url:"/updatePasswordUser",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    success:function(data)
    {  
        console.log(data); //return;
        var err = 'print-error-msg';
        if($.isEmptyObject(data.error)){
            $(".change_update_form_error ul li").remove();
            $(".change_update_form_error").find("ul").append('<li class="green-text">'+data.success+'</li>');
            setTimeout(function () {
                // alert('Reloading Page');
                location.reload(true);
            }, 2000);
            // window.location.href = "/user_master";
        }else{
            printErrorMsg(data.error,err);
        }
        
    }
    });

    function printErrorMsg(msg,err) {
        $(".change_update_form_error ul li").remove();
        $number_sequence = 1; 
        $.each( msg, function( key, value ) {
            $(".change_update_form_error").find("ul").append('<li class="red-text">'+$number_sequence+') '+value+'</li>');
            $number_sequence++;
        });                
    }
}

function confirmDelUser(params) {
  // alert(params); return;   
    var user_id = params;
    $('.user_id').val(user_id);
    
    $('#deleteUserModal').modal('open');
}


function confirmStatusUser(params) {
  // alert(params); return;   
    var user_id = params;
    $('.user_id').val(user_id);
    
    $('#statusUserModal').modal('open');
}

function statusUser() {
    var url = 'statusUser';
    var form = 'status_user';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            if($.isEmptyObject(data.error)){
                alert(data.success);
                // console.log(data.success);
                // $(".status_response").add("green_color");
                // $(".status_response").html(data.success);
                window.location.href = "/user_master";
            }else{
                console.log(data.error);
                alert(data.error);
                // printErrorMsg(data.error,err);
                // $(".status_response").add("red_color");
                // $(".status_response").html(data.error);
            }
            // window.location.reload();
            
        }
    }); 
}

function deleteUser() {
    var url = 'deleteUser';
    var form = 'delete_user';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            window.location.reload();
            
        }
    }); 
}

</script>
<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    
<script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
 
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/4.1.0/js/dataTables.fixedColumns.min.js"></script>
    <!-- END PAGE LEVEL JS-->    <!-- END PAGE LEVEL JS-->
