<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/css/select.dataTables.min.css"> -->
  
      <!-- <script src="app-assets/js/scripts/data-tables.js"></script> -->
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.0/css/fixedColumns.dataTables.min.css"> -->
      <!-- END PAGE LEVEL JS-->






      <style>


div.dataTables_wrapper {
    width: 100% !important;
    margin: 0 auto;
}
select {
    display: initial !important; ;
}

/*::-webkit-scrollbar {
  display: none;
}*/
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.dataTables_filter input {
    border: 1px solid #aaa;
    border-radius: 3px;
    padding: 0px !important;
}
 


.dataTables_scrollBody{
    height: auto !important;
}




/* .tableFixHead {
  overflow: auto;
 
 
}*/
/* 
.tableFixHead table {
  border-collapse: collapse;
  width: 100%;
}

.tableFixHead th,
.tableFixHead td {
  padding: 8px 16px;
}


td:first-child, th:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:white;
}
td:nth-child(2),th:nth-child(2)  { 
position:sticky;
  left:102px;
  z-index:1;
  background-color:white;
  }
.tableFixHead th {
  position: sticky;
  top: 0;
  background: #eee;
  z-index:2
}
th:first-child , th:nth-child(2) {
  z-index:3
  } */

</style>
<style>
      
    </style>


      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">
         <div class="row">
          <!-- <div id="loader" class="center"></div> -->
          <div class="row">                                        
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li >
                            <div class="collapsible-header" id="col_company_info" tabindex="0" style="background-color: orange;">User Info</div>
                        </li>                                        
                    </ul>
                </div>
                @if(isset($property_consultant_id) && !empty($property_consultant_id))
                <div class="col l3 s12">
                    <a href="/add-branch-information?pc_id={{$property_consultant_id}}" style="color:#000000b3">
                     <ul class="collapsible">
                        <li >
                        <div class="collapsible-header" id="col_branch_info">Branch Info</div>
                        
                        </li>                                        
                    </ul>
                </a>
                </div>

                <div class="col l3 s12">
                  <a href="/add-employee-information?pc_id={{$property_consultant_id}}" style="color:#000000b3">
                    <ul class="collapsible">
                        <li onClick="employee_info()">
                        <div class="collapsible-header" id="col_employee_info">Member Info</div>
                        
                        </li>                                        
                    </ul>
                    </a>
                </div>
                @endif
                <div class="col l3 s12">
                    <a href="{{route('user.userView')}}" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> Back To Home Page</a>
                </div>
          </div> 

        <div class="collapsible-body"  id='company_info' style="display:block">
            @php $user_id=''; @endphp
            @if(isset($getSingleUser))@if(isset($getSingleUser[0]->user_id) && !empty($getSingleUser[0]->user_id)) @php $user_id = $getSingleUser[0]->user_id; @endphp @endif @endif
            <input type="hidden"  id="user_id" value="{{$user_id}}">

        <div class="row">

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Name: <span class="red-text">*</span></label>
                @php $name=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->name) && !empty($getSingleUser[0]->name)) @php $name = $getSingleUser[0]->name; @endphp @endif @endif
                <input type="text" class="validate" name="name" id="name" value="{{$name}}"   placeholder="Enter"  required />
                    
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Date Of Birth: </label>
                @php $dob=''; @endphp
                @if(isset($getSingleUser))
                @if(isset($getSingleUser[0]->dob) && !empty($getSingleUser[0]->dob)) 
                
                @php $dob = DateTime::createFromFormat('Y-m-d', $getSingleUser[0]->dob)->format('d/m/Y'); @endphp 
                
                @endif 
                @endif
                <input type="text" class="datepicker validate" name="dob" id="dob" value="{{$dob}}"   placeholder="Enter"  />
                    
            </div>
            
            <div class="input-field col l3 m4 s12 display_search" id="InputsWrapper">
                <label for="contactNum1" class="dopr_down_holder_label active">Phone Number : <span class="red-text">*</span></label>
                <div  class="sub_val no-search">
                    <select  id="mobile_conuntry_code" name="mobile_conuntry_code" class="select2 browser-default" required >
                    @foreach($country as $country1)
                    <option value="{{$country1->country_code_id}}" @if(isset($getCustomerData)) @if( isset($getCustomerData[0]->primary_country_code_id) ) @if( $country1->country_code_id == $getCustomerData[0]->primary_country_code_id) selected @endif @endif @endif  >{{ucwords($country1->country_code)}}</option>
                    @endforeach
                    </select>
                </div>
                @php $phone_number=''; @endphp
                    @if(isset($getSingleUser))@if(isset($getSingleUser[0]->phone_number) && !empty($getSingleUser[0]->phone_number)) @php $phone_number = $getSingleUser[0]->phone_number; @endphp @endif @endif
                <input type="text" class="mobile_number" name="phone_number" id="phone_number"   placeholder="Enter" value="{{$phone_number}}" />                             
                
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Email ID : <span class="red-text">*</span></label>
                @php $email=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->email) && !empty($getSingleUser[0]->email)) @php $email = $getSingleUser[0]->email; @endphp @endif @endif
                <input type="email" class="validate" name="email" id="email" placeholder="Enter"  value="{{$email}}" required/>                             
            </div>
        </div>

        <div class="row">
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                @php $branch_id=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->branch_id) && !empty($getSingleUser[0]->branch_id)) @php $branch_id = $getSingleUser[0]->branch_id; @endphp @endif @endif
                <select  id="branch_id" name="branch_id" class="validate select2 browser-default firm_type" onchange="getReportingManagerData(this.value);">
                    <option value=""  selected>Select</option>
                    @foreach($branch AS $bkey=>$bvalue)
                    <option value="{{$bvalue->pc_branch_id}}" @if($bvalue->pc_branch_id==$branch_id) @php echo 'selected'; @endphp @endif >{{$bvalue->branch_name}}</option>                                    
                    @endforeach
                </select>
                    <label  class="active">Branch:</label>
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                @php $designation_id=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->designation_id) && !empty($getSingleUser[0]->designation_id)) @php $designation_id = $getSingleUser[0]->designation_id; @endphp @endif @endif
                <select  id="designation_id" name="designation_id" class="validate select2 browser-default firm_type">
                    <option value=""  selected>Select</option>
                        @foreach($designation AS $deskey=>$desvalue)
                        <option value="{{$desvalue->dd_designation1_id}}" @if($desvalue->dd_designation1_id==$designation_id) @php echo 'selected'; @endphp @endif >{{$desvalue->designation}}</option>                                    
                        @endforeach
                </select>
                    <label  class="active">Designation:</label>
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                @php $pc_employees_id=''; @endphp
                @if(isset($reporting_manager_data))@if(isset($reporting_manager_data[0]->pc_employees_id) && !empty($reporting_manager_data[0]->pc_employees_id)) @php $pc_employees_id = $getSingleUser[0]->pc_employees_id; @endphp @endif @endif
                <select  id="pc_employees_id" name="pc_employees_id" class="validate select2 browser-default firm_type">
                    <option value="" >Select</option>
                    @if(isset($getSingleUser))
                    @if(isset($getSingleUser[0]->branch_id) && !empty($getSingleUser[0]->branch_id))
                        @if(isset($reporting_manager_data) && !empty($reporting_manager_data))
                            @foreach($reporting_manager_data AS $rmdkey=>$rmdvalue)
                            <option value="{{$rmdvalue->pc_employees_id}}" @if($rmdvalue->pc_employees_id==$pc_employees_id) @php echo 'selected'; @endphp @endif >{{$rmdvalue->employee_name}}</option>                                    
                            @endforeach
                        @endif
                    @endif 
                    @endif
                    
                </select>
                    <label  class="active">Reporting Manager:</label>
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                @php $role_id=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->role_id) && !empty($getSingleUser[0]->role_id)) @php $role_id = $getSingleUser[0]->role_id; @endphp @endif @endif
                <select  id="role_id" name="role_id" class="validate select2 browser-default firm_type" required>
                    <option value=""  selected>Select</option>
                    @foreach($role AS $rolkey=>$rolvalue)
                    <option value="{{$rolvalue->id}}" data-role="{{$rolvalue->session_name}}" @if(isset($role_id) && !empty($role_id)) @if($rolvalue->id==$role_id) @php echo 'selected'; @endphp @endif @endif >{{$rolvalue->role_name}}</option>                                    
                    @endforeach
                </select>
                <label  class="active">Access Level Group: <span class="red-text">*</span></label>
            </div>


        </div>

        <div class="row">

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Basic Salary: </label>
                @php $basic_salary=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->basic_salary) && !empty($getSingleUser[0]->basic_salary)) @php $basic_salary = $getSingleUser[0]->basic_salary; @endphp @endif @endif
                <input type="text" class="validate" name="basic_salary" id="basic_salary" value="{{$basic_salary}}"   placeholder="Enter"  />
                    
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Photo: </label>
                <input type="file" name="photo" id="photo" class="dropify" data-default-file="" style="padding-top: 14px;">  
                @if(isset($getSingleUser)) 
                @if(isset($getSingleUser[0]->photo) && !empty($getSingleUser[0]->photo)) 
                @php $photo = $getSingleUser[0]->photo; @endphp

                @if(isset($photo) && !empty($photo))
                    @php $j=1; @endphp 

                    @php $y = pathinfo($photo, PATHINFO_FILENAME); @endphp
                    <span class="{{$y}}"><a href="{{ asset('users/photos/'.$photo) }}" target="_blank"> Image {{$j}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$photo}}#img')">X</a> &nbsp; , </span> 
                    
                @endif

                @endif
                @endif

            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Resume: </label>
                <input type="file" name="resumes[]" id="resumes" class="dropify" multiple="true" data-default-file="" style="padding-top: 14px;">  
                @if(isset($getSingleUser)) 
                @if(isset($getSingleUser[0]->resumes) && !empty($getSingleUser[0]->resumes)) 
                @php $resumes = $getSingleUser[0]->resumes; @endphp

                @if(isset($resumes) && !empty($resumes))
                    @php $resumes = json_decode($resumes); $i=1; @endphp 
                    @if(is_array($resumes))
                        @foreach($resumes as $resumes)
                        @php $x = pathinfo($resumes, PATHINFO_FILENAME); @endphp
                        <span class="{{$x}}"><a href="{{ asset('users/resumes/'.$resumes) }}" target="_blank"> Resume {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$resumes}}#resume')">X</a> &nbsp; , </span>
                        
                        @php $i++; @endphp 
                        @endforeach    
                    @endif
                @endif

                @endif
                @endif
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Temporary Address: </label>
                @php $temporary_address=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->temporary_address) && !empty($getSingleUser[0]->temporary_address)) @php $temporary_address = $getSingleUser[0]->temporary_address; @endphp @endif @endif
                <input type="text" class="validate" name="temporary_address" id="temporary_address" value="{{$temporary_address}}"   placeholder="Enter"  />
                    
            </div>


        </div>

        <div class="row">

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Permanent Address: </label>
                @php $permanent_address=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->permanent_address) && !empty($getSingleUser[0]->permanent_address)) @php $permanent_address = $getSingleUser[0]->permanent_address; @endphp @endif @endif
                <input type="text" class="validate" name="permanent_address" id="permanent_address" value="{{$permanent_address}}"   placeholder="Enter"  />
                    
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Description: </label>
                @php $description=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->description) && !empty($getSingleUser[0]->description)) @php $description = $getSingleUser[0]->description; @endphp @endif @endif
                <input type="text" class="validate" name="description" id="description" value="{{$description}}"   placeholder="Enter"  />
                    
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Comment: </label>
                @php $comment=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->comment) && !empty($getSingleUser[0]->comment)) @php $comment = $getSingleUser[0]->comment; @endphp @endif @endif
                <input type="text" class="validate" name="comment" id="comment" value="{{$comment}}"   placeholder="Enter"  />
                    
            </div>

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Internal Comment: </label>
                @php $internal_comment=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->internal_comment) && !empty($getSingleUser[0]->internal_comment)) @php $internal_comment = $getSingleUser[0]->internal_comment; @endphp @endif @endif
                <input type="text" class="validate" name="internal_comment" id="internal_comment" value="{{$internal_comment}}"   placeholder="Enter"  />
                    
            </div>


            {{-- @if(isset($getSingleUser)) 
            @if(isset($getSingleUser[0]->user_id) && !empty($getSingleUser[0]->user_id))
            
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                @php $role_id=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->role_id) && !empty($getSingleUser[0]->role_id)) @php $role_id = $getSingleUser[0]->role_id; @endphp @endif @endif
                <select  id="role_id" name="role_id" class="validate select2 browser-default firm_type" required>
                    <option value=""  selected>Select</option>
                    @foreach($role AS $rolkey=>$rolvalue)
                    <option value="{{$rolvalue->id}}" data-role="{{$rolvalue->role_name}}" @if(isset($role_id) && !empty($role_id)) @if($rolvalue->id==$role_id) @php echo 'selected'; @endphp @endif @endif >{{$rolvalue->role_name}}</option>                                    
                    @endforeach
                </select>
                <label  class="active">Role: <span class="red-text">*</span></label>
            </div>
            
            @endif
            @endif --}}
                       
        </div>

        @if(isset($getSingleUser)) 
        @if(!isset($getSingleUser[0]->id))
        <div class="row">
        

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Password: <span class="red-text">*</span></label>
                <input type="password" class="validate" name="password" id="password" value=""   placeholder="Enter"  required />
                    
            </div>
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <label  class="active">Confirm Password: <span class="red-text">*</span></label>
                <input type="password" class="validate" name="confirm_password" id="confirm_password" value=""   placeholder="Enter"  required />
                    
            </div>

        </div>
        @endif
        @endif

        {{-- @if(isset($getSingleUser)) 
        @if(!isset($getSingleUser[0]->user_id) && empty($getSingleUser[0]->user_id))
        <div class="row">
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                @php $role_id=''; @endphp
                @if(isset($getSingleUser))@if(isset($getSingleUser[0]->role_id) && !empty($getSingleUser[0]->role_id)) @php $role_id = $getSingleUser[0]->role_id; @endphp @endif @endif
                <select  id="role_id" name="role_id" class="validate select2 browser-default firm_type" required>
                    <option value=""  selected>Select</option>
                    @foreach($role AS $rolkey=>$rolvalue)
                    <option value="{{$rolvalue->id}}" data-role="{{$rolvalue->role_name}}" @if(isset($role_id) && !empty($role_id)) @if($rolvalue->id==$role_id) @php echo 'selected'; @endphp @endif @endif >{{$rolvalue->role_name}}</option>                                    
                    @endforeach
                </select>
                <label  class="active">Role: <span class="red-text">*</span></label>
            </div>
                   
        </div>
        @endif
        @endif --}}

                <script>
                    function calDays() {
                        // var inception_year = $('#inception_year').val();
                        var date_of_visit = $('#inception_year').val();
                        const date = date_of_visit;
                        const [day, month, year] = date.split('/');
                        const result = [year, month, day].join('-');

                        var y1 = new Date(result).getFullYear();
                            var y2 = new Date().getFullYear();

                            var years = [];
                            if(y1 < y2){
                            for(var i = y1; i <= y2; i++){
                                years.push(i);
                                }
                            }
                            else{
                            for(var i = y2; i <= y1; i++){
                                years.push(i);
                                }
                            }
                            var no_of_years = years.length;
                            if(no_of_years == 0){
                                $('#no_of_years').val(0);
                            }else{
                                $('#no_of_years').val(no_of_years-1);    
                            }
                            
                    }
                </script>


                <div class="collapsible-body"  id='company_address' style="display:none">
                    <div class="row">
                        <div class="input-field col l3 m4 s12 display_search">
                                
                                <select  id="sub_location" name="sub_location" class="validate select2 browser-default" onChange="getsublocation(this)">
                                <option value="" selected>Select</option>
                                    @foreach($sub_location as $val)
                                        <option value="{{$val->sub_location_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $val->sub_location_id == $getSinglePc[0]->sub_location) selected @endif @endif >{{ucwords($val->sub_location_name)}}</option>                                    
                                    @endforeach
                                </select>
                                <label class="active">Sub Location</label>
                        </div>

                         <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="location" name="location" class="validate  browser-default"  >
                                <option value="" disabled selected>Select</option>
                                @if(isset($location) && !empty($location))
                                    @foreach($location as $location)
                                    <option value="{{$location->location_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $location->location_id == $getSinglePc[0]->location) selected @endif @endif >{{ ucwords($location->location_name)}}</option>
                                    @endforeach
                                @endif
                                </select>
                                <label class="active">Location</label>
                        </div>
               
                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="city" name="city" class="validate  browser-default" >
                                 @if(isset($city) && !empty($city))
                                    @foreach($city  as $cty)
                                    <option value="{{$cty->city_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $cty->city_id == $getSinglePc[0]->city) selected @endif @endif >{{ ucfirst($cty->city_name) }}</option>
                                    @endforeach
                                   @endif                              
                                </select>
                                <label class="active">City</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="state" name="state" class="validate  browser-default" >
                                <option value="" disabled selected>Select</option>
                                     @foreach($state as $state)
                                    <option value="{{$state->dd_state_id}}" @if(isset($getSinglePc) && !empty($getSinglePc)) @if( $state->dd_state_id == $getSinglePc[0]->state) selected @endif @endif  >{{ ucwords($state->state_name)}}</option>
                                    @endforeach                                  
                                </select>
                                <label class="active">State</label>
                        </div>
                    </div>  
                    <div class="row">

                        <div class="input-field col l3 m4 s12 display_search">                                
                            <label  class="active">Latitude & Longitude: </label>
                            @php $lattitde_longitude=''; @endphp
                                @if(isset($getSinglePc))@if(isset($getSinglePc[0]->lattitde_longitude) && !empty($getSinglePc[0]->lattitde_longitude)) @php $lattitde_longitude = $getSinglePc[0]->lattitde_longitude; @endphp @endif @endif
                                <input type="text" class="validate" name="lattitde_longitude" id="lattitde_longitude"    placeholder="Enter"  value="{{$lattitde_longitude}}">                             
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Registered Office Add. : </label>
                                @php $office_address=''; @endphp
                                @if(isset($getSinglePc))@if(isset($getSinglePc[0]->office_address) && !empty($getSinglePc[0]->office_address)) @php $office_address = $getSinglePc[0]->office_address; @endphp @endif @endif
                            <input type="text" class="validate" name="reg_office_address" id="reg_office_address"   placeholder="Enter"  value="{{$office_address}}">                             
                        </div>
                    </div>
                </div>

              

                <div class="collapsible-body"  id='social_media' style="display:none">
                <div class="row">
                <input type="hidden" id="property_consultant_social_media_id">
                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">URL: </label>
                            <input type="text" class="validate" name="social_url" id="social_url"    placeholder="Enter"  >                             

                        </div>
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                           
                           <select  id="social_site" name="social_site" class="validate select2 browser-default rating">
                               <option value=""  selected>Select</option>
                               @foreach($social_site as $val)
                                   <option value="{{$val->social_site_id}}" >{{ucfirst($val->social_site)}}</option>                                    
                                @endforeach
                               </select>
                               <label  class="active">Social Site:</label>
                       </div> 
                     
                     

                        <!---->
                            <div class="input-field col l2 m2 s6 display_search">
                                <button class="btn-small  waves-effect waves-light" id="save_saveSocial" onClick="saveSocial()" type="button" name="action">@if(isset($pc_branch_id) && !empty($pc_branch_id)) Update @else Save @endif</button>                        

                            </div>    
                            
                            <div class="input-field col l2 m2 s6 display_search">
                            <a href="javascript:void(0)" onClick="clear_saveSocial()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_saveSocial"><i class="material-icons dp48">clear</i></a>
                            </div>    
                    </div><br>
                    

                </div>
                <div class="row form_error">
                    <div class="input-field col-12 l2 m2 s6 pl-1">
                        <ul></ul>

                    </div>   
                </div> 

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="saveUser()" type="button" name="action">@if(isset($user_id) && !empty($user_id)) Update @else Save @endif</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <a href="{{route('user.userView')}}" class="waves-effect waves-green btn-small" style="background-color: red;">Cancel</a>

                    </div>    
                </div> 

               

     
              
  </div>
            
  

                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


<!-- Social Modal -->

<div class="modal" id="deleteSocialModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_Social" >
            <input type="hidden" class="property_consultant_social_media_id2" id="property_consultant_social_media_id2" name="property_consultant_social_media_id">
            <input type="hidden"  id="form_type" name="form_type" value="socialPC">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteSocial()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>


<script>
  
  function apply_css11(skip,attr, val=''){   
    var id =returnColArray11();           
    id.forEach(function(entry) {
        if(entry==skip){
            $('#'+skip).css(attr,'orange','!important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){
            }else{
                $('#'+entry).css(attr,val);
            }
        }        
    });
}

function hide_n_show11(skip){
   var id = collapsible_body_ids11();
   collapsible_body_ids11().forEach(function(entry) {
       if(entry==skip){
           var x = document.getElementById(skip);  
           $('#'+skip).css('background-color','rgb(234 233 230)');
           if (x.style.display === "none") {
               x.style.display = "block";
           } else {
               x.style.display = "none";
               $('#col_'+skip).css('background-color','white');
           }
       }else{          
           $('#'+entry).hide();
       }
   });
}

function returnColArray11(){
   var a = Array('col_company_info','col_branch_info','col_employee_info');
   return a;
}

function collapsible_body_ids11(){
   var b = Array('company_info','branch_info','employee_info');
   return b;
}

function company_info(){    
  // alert()
   apply_css11('col_company_info','background-color','');
   hide_n_show11("company_info");     

}

function branch_info(){    
   apply_css11('col_branch_info','background-color','');
   hide_n_show11("branch_info");     

}

function employee_info(){    
   apply_css11('col_employee_info','background-color','');
   hide_n_show11("employee_info");     

}


function apply_css22(skip,attr, val=''){   
    var id =returnColArray22();           
    id.forEach(function(entry) {
        if(entry==skip){
            $('#'+skip).css(attr,'orange','!important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){
            }else{
                $('#'+entry).css(attr,val);
            }
        }        
    });
}

function hide_n_show22(skip){
   var id = collapsible_body_ids22();
   collapsible_body_ids22().forEach(function(entry) {
       if(entry==skip){
           var x = document.getElementById(skip);  
           $('#'+skip).css('background-color','rgb(234 233 230)');
           if (x.style.display === "none") {
               x.style.display = "block";
           } else {
               x.style.display = "none";
               $('#col_'+skip).css('background-color','white');
           }
       }else{          
           $('#'+entry).hide();
       }
   });
}

function returnColArray22(){
   var a = Array('col_company_address','col_company_review','col_social_media');
   return a;
}

function collapsible_body_ids22(){
   var b = Array('company_address','company_review','social_media');
   return b;
}

function company_address() {
  apply_css22('col_company_address','background-color','');
   hide_n_show22("company_address");     
}

function company_review() {
  apply_css22('col_company_review','background-color','');
   hide_n_show22("company_review");     
}

function social_media() {
  apply_css22('col_social_media','background-color','');
   hide_n_show22("social_media");     
}






function saveSocial() {
  var form_data = new FormData();
      form_data.append("social_url", document.getElementById('social_url').value);
      form_data.append("social_site", document.getElementById('social_site').value);
      form_data.append("form_type", 'social');
      form_data.append("property_consultant_id", $('#property_consultant_id').val());
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/saveData",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data);//return;

        //  const obj = JSON.parse(data);

         // console.log(obj); //return;
        //  $('#project_id2').val(data.project_id);
         // if(typeof obj['company_id'] !== 'undefined'){
          $('#property_consultant_id').val(data.property_consultant_id);
         // }
         //$('#uploaded_image').html(data);
         var incPros1 = parseInt($('#incID11').val()) +1;
         var html = '<tr id="tr2_'+data.property_consultant_social_media_id+'">';
         html += '<td>' + incPros1 + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="sur1_'+data.property_consultant_social_media_id+'">' + data.url + '</td>';// obj['company_pros'] +'</td>';
         html += '<td id="ssi1_'+data.property_consultant_social_media_id+'">' + data.social_site + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="slu1_'+data.property_consultant_social_media_id+'">' + data.updated_date + '</td>';// obj['company_cons'] +'</td>';         
         html += "<td> <a href='javascript:void(0)' onClick='updateSocial("+data.property_consultant_social_media_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelSocial("+data.property_consultant_social_media_id+")'>Delete</a></td>";   
         html += '</tr>';
         $('#social_table').prepend(html);
         $('#social_site').val('').trigger('change');
         $('#social_url').val('');
        //  $('#incPros').val(incPros);

        //  $('#loader1').css('display','none');
        //  $('#submitPros').css('display','block');
      }
      });
}

function updateSocial(param) {
   var property_consultant_social_media_id = param;
      var form_data = new FormData();
      form_data.append("form_type", 'social');
      form_data.append("property_consultant_social_media_id", property_consultant_social_media_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
      $.ajax({
         url:"/getData2",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
            { 
               var data  = data[0];
               console.log(data); //return;
               $('#property_consultant_social_media_id').val(data.property_consultant_social_media_id);
               // $('#looking_since1').val(data.looking_since).trigger('change');
              
           
               $('#social_site').val(data.social_site).trigger('change');
               $('#social_url').val(data.url);
               var anchor=document.getElementById("save_saveSocial");
               anchor.innerHTML="Update";
               $("#save_saveSocial").attr("onclick","updateSocial1()");
            }
      });
}

function updateSocial1() {


// $('#loader3').css('display','block');
// $('#submitLooking').css('display','none');

   var form_data = new FormData();
   


   form_data.append("form_type", 'social');
   form_data.append('property_consultant_social_media_id', $('#property_consultant_social_media_id').val()); 

   form_data.append("social_site", document.getElementById('social_site').value);
   form_data.append("social_url", document.getElementById('social_url').value);

   $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
   });

   $.ajax({
      url:"/updateTableRecords1",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
          console.log(data);// return;
         var property_consultant_social_media_id = data.property_consultant_social_media_id;
         $('#sur1_'+property_consultant_social_media_id).html(data.data['url']);
         $('#ssi1_'+property_consultant_social_media_id).html(data.data['social_site']);
         $('#slu1_'+property_consultant_social_media_id).html(data.data['updated_date']);
        //  $('#loader3').css('display','none');
        //  $('#submitLooking').css('display','block');         
      }
   });

}

function clear_saveSocial() {
  $('#property_consultant_social_media_id').val('');
  $('#social_site').val('').trigger('change');
  $('#social_url').val('');

  var anchor=document.getElementById("save_saveSocial");
   anchor.innerHTML="Save";
   $("#save_saveSocial").attr("onclick","saveSocial()");
}


        function getCity(argument) {
            var city_id = argument;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(city_id) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {city_id:city_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                       
                        $('#state').val(data[0].dd_state_id).trigger('change'); 
                    // });
                  }else{
                    $('#city').empty();
                  }
                  }
                });
            }else{
              $('#city').empty();
            }
        }

        function getlocation(argument) {
            var location_id = argument;
           // alert(location_id); return;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(location_id) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {location_id:location_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data);// return;
                      if(data){
                        $('#city').val(data[0].city_id).trigger('change'); 
                        getCity(data[0].city_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }
        
        function getsublocation(argument) {
            var SublocationID = argument.value;
            // alert(SublocationID);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){

                        $('#location').val(data[0].location_id).trigger('change');  
                        getlocation(data[0].location_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }

// Get Reporting Manager data

function getReportingManagerData(param) 
{
    var branch_id = param;
    console.log("branch_id : ",branch_id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#pc_employees_id .reporting_manger_option').remove();

    if(branch_id!='') {
        $.ajax({
            url: '/getUserReportingManagerData/',
            type: "GET",
            data : {branch_id:branch_id},
            // dataType: "json",
            success:function(data) {
                console.log("data : ",data); //return;
                if(data){
                
                    $.each(data,function(index, value) {
                        $('#pc_employees_id').append("<option value="+value.pc_employees_id+" class='reporting_manger_option'>"+value.employee_name+"</option>"); 
                    });
                
            // });
            }else{
                $('#reporting_manager_id .reporting_manger_option').remove();
            }
            }
        });
    }else{
        $('#reporting_manager_id .reporting_manger_option').remove();
    }

}

function saveUser() 
{

    var form_data = new FormData();
    
    form_data.append('user_id', $('#user_id').val());
    form_data.append('name', $('#name').val());
    form_data.append('dob', $('#dob').val());
    form_data.append('phone_number', $('#phone_number').val());
    
    form_data.append('email', $('#email').val());
    form_data.append('branch_id', $('#branch_id').val());
    form_data.append('designation_id', $('#designation_id').val());
    form_data.append('pc_employees_id', $('#pc_employees_id').val());
    form_data.append('basic_salary', $('#basic_salary').val());
    form_data.append('photo', $('#photo').val());
    // form_data.append('resumes', $('#resumes').val());

    var totalPhotofiles = document.getElementById('photo').files.length;
    for (var i = 0; i < totalPhotofiles; i++) {
        form_data.append("photo", document.getElementById('photo').files[i]);
    }

    var totalResumefiles = document.getElementById('resumes').files.length;
    for (var j = 0; j < totalResumefiles; j++) {
        form_data.append("resumes[]", document.getElementById('resumes').files[j]);
    }

    form_data.append('temporary_address', $('#temporary_address').val());
    form_data.append('permanent_address', $('#permanent_address').val());
    form_data.append('description', $('#description').val());
    form_data.append('comment', $('#comment').val());
    form_data.append('internal_comment', $('#internal_comment').val());
    form_data.append('password', $('#password').val());
    form_data.append('confirm_password', $('#confirm_password').val());
    form_data.append('role_id', $('#role_id').val());
    form_data.append('role_name', $('#role_id option:selected').attr('data-role'));

    // var sub_location = $('#sub_location').val();
    // if(sub_location != null){
    //     form_data.append('sub_location',sub_location );
    // }else{
    //     form_data.append('sub_location','' );
    // }

    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
    url:"/saveUser",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    success:function(data)
    {  
        console.log(data); //return;
        var err = 'print-error-msg';
        if($.isEmptyObject(data.error)){
            window.location.href = "/user_master";
        }else{
            printErrorMsg(data.error,err);
        }
        
    }
    });

    function printErrorMsg(msg,err) {
        $(".form_error ul li").remove();
        $number_sequence = 1; 
        $.each( msg, function( key, value ) {
            $(".form_error").find("ul").append('<li class="red-text">'+$number_sequence+') '+value+'</li>');
            $number_sequence++;
        });                
    }

}

// delete user image and resumes

function delImage(params) {
      const myArray = params.split("#");
      var image = myArray[0];
      var type = myArray[1];
      // alert(myArray[1]); return;
      // alert('#'+params);//delImage
      if(type=='img'){
        var warning ="Are you sure you want to delete this image?";
      }else{
        var warning ="Are you sure you want to delete this resume?";
      }

      if (confirm(warning)) {
         var user_id = $('#user_id').val();
         const file = image;
         const filename = file.split('.').slice(0, -1).join('.');
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delUserImage",
            method:"POST",
            data: {image:image,user_id:user_id,type:type},
            // contentType: false,
            // cache: false,
            // processData: false,
            success:function(data)
            {
                console.log("delete data : ",data);
                if($.isEmptyObject(data.error)){
                    $('.'+filename).remove();
                    
                }
            }
         });
      }else{
         return false;
      }
   }





function confirmDelSocial(params) {
    var property_consultant_social_media_id = params;
    $('.property_consultant_social_media_id2').val(property_consultant_social_media_id);   
    $('#deleteSocialModal').modal('open');
}

function deleteSocial() {
    var url = 'deleteCompletly';
    var form = 'delete_Social';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            var property_consultant_social_media_id = data.property_consultant_social_media_id;
            $('#tr2_'+property_consultant_social_media_id).remove();
            // $('#deleteAwardModal').hide();
            $('#deleteSocialModal').modal('close');
            
        }
    }); 
}


</script>

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <!-- <script src="app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> -->
    <!-- <script src="app-assets/vendors/data-tables/js/dataTables.select.min.js"></script> -->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <!-- <script src="app-assets/js/plugins.js"></script> -->
    <!-- <script src="app-assets/js/search.js"></script> -->
    <!-- <script src="app-assets/js/custom/custom-script.js"></script> -->
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <!-- END PAGE LEVEL JS-->