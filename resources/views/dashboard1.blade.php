
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->

      @php
      $check_s_view_buyer = 0;
      $check_s_view_tenant = 0;
      $check_s_view_property_consultant = 0;
      $check_s_view_building_master = 0;
      $check_s_view_users = 0;
      $check_s_view_roles = 0;
      @endphp

      @if(isset($access_check_buyer) && !empty($access_check_buyer))
         @foreach($access_check_buyer as $acbvalue)
            @php
            $check_s_view_buyer = $acbvalue->s_view;
            @endphp
         @endforeach
      @endif

      @if(isset($access_check_tenant) && !empty($access_check_tenant))
         @foreach($access_check_tenant as $actvalue)
            @php
            $check_s_view_tenant = $actvalue->s_view;
            @endphp
         @endforeach
      @endif

      @if(isset($access_check_property_consultant) && !empty($access_check_property_consultant))
         @foreach($access_check_property_consultant as $acpcvalue)
            @php
            $check_s_view_property_consultant = $acpcvalue->s_view;
            @endphp
         @endforeach
      @endif

      @if(isset($access_check_building_master) && !empty($access_check_building_master))
         @foreach($access_check_building_master as $acbmvalue)
            @php
            $check_s_view_building_master = $acbmvalue->s_view;
            @endphp
         @endforeach
      @endif

      @if(isset($access_check_users) && !empty($access_check_users))
         @foreach($access_check_users as $acuvalue)
            @php
            $check_s_view_users = $acuvalue->s_view;
            @endphp
         @endforeach
      @endif

      @if(isset($access_check_roles) && !empty($access_check_roles))
         @foreach($access_check_roles as $acrvalue)
            @php
            $check_s_view_roles = $acrvalue->s_view;
            @endphp
         @endforeach
      @endif

    @if($check_s_view_buyer==1 || $check_s_view_tenant==1 || $check_s_view_property_consultant==1 || 
      $check_s_view_building_master==1 || $check_s_view_users==1 || $check_s_view_roles==1 )
      
        <x-sidebar-layout></x-sidebar-layout>
    @endif

      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
    
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/4.1.0/css/fixedColumns.dataTables.min.css">

<style>
  select
{
    display: block !important;
}

</style>

      <!-- BEGIN: Page Main class="main-full"-->



      <div id="main" class="main-full" style="min-height: auto">
  
        <div class="row" style="padding: 20px 0;">

            <div class="col l12 s12">
                <h1 style="text-align: center;">Welcome to Dashboard</h1>
            </div>
        </div>
      </div>

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
 
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/4.1.0/js/dataTables.fixedColumns.min.js"></script>
    <!-- END PAGE LEVEL JS-->

