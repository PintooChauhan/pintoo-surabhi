<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      
      <script src="{{ asset('app-assets/js/custom/tenant.js') }}"></script>
      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
      <div id="main" class="main-full" style="min-height: auto">
         <div class="row">
         <input type="hidden" value="@if(isset($buyerId) && !empty($buyerId)) {{$buyerId['buyer_tenant_id']}} @endif" id="buyer_tenant_id">
         <input type="hidden"  id="customer_id" value="@if(isset($getCustomerData) && !empty($getCustomerData)) {{$getCustomerData[0]->id}} @endif">
           
            <!-- <div class="col s12"> -->
               <!-- <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">BUYER INPUT FORM</span><hr>  </div> -->
                 <x-tenantrow1-layout  :buyerId="$buyerId" :getBuyerData="$getBuyerData" :getCustomerData="$getCustomerData" :getLeadSourceData="$getLeadSourceData"></x-tenantrow1-layout>                 
                 <x-tenantrow2-layout  :getBuyerData="$getBuyerData" :getPropertySeenData="$getPropertySeenData" :getSpecificChoiceData="$getSpecificChoiceData"></x-tenantrow2-layout>
                 <x-tenantrow3-layout  :getBuyerData="$getBuyerData" :getSpecificChoiceData="$getSpecificChoiceData" :getCustomerData="$getCustomerData"></x-tenantrow3-layout>
                 <x-tenantrow4-layout :getBuyerData="$getBuyerData" :getSpecificChoiceData="$getSpecificChoiceData" :getCustomerData="$getCustomerData"></x-tenantrow4-layout>
                 <x-tenantrow5-layout :getBuyerData="$getBuyerData"></x-tenantrow5-layout>
                  <hr>
                 <x-tenantrow6-layout  :getBuyerData="$getBuyerData" :getChallengesData="$getChallengesData" :getmemberData="$getmemberData" ></x-tenantrow6-layout>
              
      

                 <x-all_modals-layout></x-all_modals-layout>               
                 <hr>

                  <div class="row">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">

                        <div class="row" >
                           <div class="input-field col l2 m2 s6 display_search">
                           <div class="preloader-wrapper small active" id="loader5" style="display:none">
                                 <div class="spinner-layer spinner-green-only">
                                 <div class="circle-clipper left">
                                    <div class="circle"></div>
                                 </div><div class="gap-patch">
                                    <div class="circle"></div>
                                 </div><div class="circle-clipper right">
                                    <div class="circle"></div>
                                 </div>
                                 </div>
                           </div>
                           </div>
                        </div>

                        <div class="row" id="submitMain">
                           <div class="col l4 m4">
                                 <a href="javascript:void(0)" onClick="saveTenant()" class=" save_challeges_solution btn-small  waves-effect waves-light green darken-1 " id="save_challeges_solution">Save Tenant</a>
                           </div>
                           
                        </div>
                     </div> 
                  </div>
            

                
                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     