<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
<!-- BEGIN: SideNav-->
<x-sidebar-layout></x-sidebar-layout>
<!-- END: SideNav-->
<style>
   ::-webkit-scrollbar {
   display: none;
   }
   input:focus::placeholder {
   color: transparent;
   }
   .select2-results__options{
    line-height: 0px;
   }

   .select2-container--default .select2-selection--multiple:before {
   content: ' ';
   display: block;
   position: absolute;
   border-color: #888 transparent transparent transparent;
   border-style: solid;
   border-width: 5px 4px 0 4px;
   height: 0;
   right: 6px;
   margin-left: -4px;
   margin-top: -2px;top: 50%;
   width: 0;cursor: pointer
   }
   .select2-container--open .select2-selection--multiple:before {
   content: ' ';
   display: block;
   position: absolute;
   border-color: transparent transparent #888 transparent;
   border-width: 0 4px 5px 4px;
   height: 0;
   right: 6px;
   margin-left: -4px;
   margin-top: -2px;top: 50%;
   width: 0;cursor: pointer
   }
   .card .card-content {
   padding-top: 0px;
   min-height: auto !important;
   }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script>
            $(document).ready(function() {                 
               $('#sub_location_uc').val(<?= $sub_location_idP; ?>).trigger('change');
               $('#sub_location_r').val(<?= $sub_location_idP; ?>).trigger('change');            
             });
         </script>
   
<!-- BEGIN: Page Main class="main-full"-->
<!-- <div id="container1"><div id="container2"> -->
<div id="main" class="main-full" style="min-height: auto">
<div class="row">
   <!--             
      <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
           
               <span class="userselect">ADD COMPANY MASTER</span><hr> 
       </div> -->
   <div class="collapsible-body"  id='budget_loan' style="display:block" >
      <div class="row" style="margin-top: -30px;font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
         <span class="userselect">@if(isset($society_id) && !empty($society_id)) UPDATE BUILDING MASTER ( B-{{str_pad($society_id,3, '0', STR_PAD_LEFT)}} ) @else ADD SOCIETY MASTER @endif </span>
      </div>
      <br>
      @if(isset($society_id) && !empty($society_id))
      <form method="post" id="society_master_form_update">
     
         
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
         <?php //print_r( $parking); ?>
         <script>
            $(document).ready(function() {  
            //   var prk = $('#prk').val();
            //   var prk1 = JSON.parse("[" + prk + "]");
            //   $('#parking_uc').val(prk1).trigger('change');

            //   var fundedby = $('#fundedby').val();
            //   var fundedby1 = JSON.parse("[" + fundedby + "]");
            //   $('#funded_by_uc').val(fundedby1).trigger('change');


            //   var unitstatus = $('#unitstatus').val();
            //   var unitstatus1 = JSON.parse("[" + unitstatus + "]");
            //   $('#unit_status').val(unitstatus1).trigger('change');

            //   var categoryid = $('#categoryid').val();
            //   var categoryid1 = JSON.parse("[" + categoryid + "]");
            //   $('#building_category_r').val(categoryid1).trigger('change');     
            // getsublocation(3);
            // if(sub_location_idP)
            
            // $('#sub_location_uc').val(<?= $sub_location_idP; ?>).trigger('change');
            
             })
         </script>
   
         @else
      <form method="post" id="society_master_form">
         @endif    
         <input type="hidden" name="project_id" id="project_id" value="{{$project_id}}" >
         <input type="hidden" name="society_id" id="society_id" value="{{$society_id}}" >
            
         @if( $building_status == 1 )
         <?php// echo $building_status; ?>
         <style>
            #registered{
            display:block ;
            }
            #registered1{
            display:block ;
            }
            #under_construction{
            display:none ;
            }
         </style>
         @elseif( $building_status == 2)
         <?php //echo $building_status; ?>
         <style>
            #registered{
            display:none ;
            }
            #registered1{
            display:none ;
            }
            #under_construction{
            display:block ;
            }
         </style>
         @else
         <style>
            #registered{
            display:block ;
            }
            #under_construction{
            display:none ;
            }
         </style>
         @endif
         
         @csrf

         <div class="row">
            <div class="input-field col l3 m4 s12 display_search">
             
               <select  id="project_idd1" name="project_idd1" class="browser-default" onChange="ref(this)" >
                  <!-- <option value="" disabled selected>Select</option> -->
                  @foreach($getAllProjects as $getAllProjects2)                               
                  <option value="{{$getAllProjects2->project_id}}"  @if(isset($project_id) && !empty($project_id) )  @if($getAllProjects2->project_id == $project_id ) selected  @endif  @endif  >{{ ucfirst ($getAllProjects2->project_complex_name)}}</option>
                  @endforeach                                 
               </select>
               <label class="active">Complex Name</label>
            </div>
         </div>

         <div class="row">
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <label  class="active">Building  Name</label>
               <input type="text" class="validate" name="building_name" id="building_name1"   placeholder="Enter" @if(isset($building_name) && !empty($building_name)) value="{{$building_name}}" @endif >
            </div>
            <div class="input-field col l3 m4 s12 display_search">
               <select  id="building_status" name="building_status" onChange="hide_show_forms()" class="browser-default">
               <option value="" disabled selected>Select</option>
               @if(isset($building_status) && !empty($building_status) )
                  @if( $building_status == 1)
                     <option value="1" @if(isset($building_status) && !empty($building_status) )  @if($building_status == 1 ) selected  @endif  @endif >Registered</option>                     
                  @else
                     @foreach($building_type as $building_type)                                                    
                           <option value="{{$building_type->building_type_id}}" @if(isset($building_status) && !empty($building_status) )  @if($building_type->building_type_id == $building_status ) selected  @endif  @endif >{{ ucwords($building_type->building_type_name)}}</option>                     
                     @endforeach                                
                  @endif
               
               @else
                  @foreach($building_type as $building_type)                                                    
                        <option value="{{$building_type->building_type_id}}" @if(isset($building_status) && !empty($building_status) )  @if($building_type->building_type_id == $building_status ) selected  @endif  @endif >{{ ucwords($building_type->building_type_name)}}</option>                     
                  @endforeach                                
               @endif
               
                  
               </select>
               <label class="active">Building Status</label>
            </div>
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <!-- <label  class="active">Building Owner</label>
               <input type="text" class="validate" name="building_owner"  placeholder="Enter"  @if(isset($building_owner) && !empty($building_owner)) value="{{$building_owner}}" @endif>                                                              -->
               <select  id="building_owner" name="building_owner" class="browser-default">
                  <option value="" selected>Select</option>
                  @foreach($dd_building_owner as $dd_building_owner)                               
                  <option value="{{$dd_building_owner->dd_building_owner_id}}" @if(isset($building_owner) && !empty($building_owner) )  @if($dd_building_owner->dd_building_owner_id == $building_owner ) selected  @endif  @endif >{{ ucwords($dd_building_owner->building_owner)}}</option>
                  @endforeach
                 
               </select>
               <label class="active">Building Ownership</label>
            </div>
            <div class="input-field col l3 m4 s12 display_search">
            @if(isset($unit_status) && !empty($unit_status) )
               <input type="hidden"id="unitstatus" value="{{$unit_status}}">
            @endif

               @if(isset($unit_status) && !empty($unit_status))
                  @php $unit_status2 = json_decode($unit_status); 
                     $unit_status2 = explode(",",$unit_status2[0]);            
                  @endphp
               @endif

               <select  id="unit_status" name="unit_status" class="browser-default" multiple="true">
                  <!-- <option value="" disabled selected>Select</option> -->
                  @foreach($unit_status1 as $unit_status1)                               
                  <option value="{{$unit_status1->unit_status_id}}" @if(isset($unit_status) && !empty($unit_status) ) @if(in_array($unit_status1->unit_status_id,$unit_status2)) selected  @endif  @endif >{{ strtoupper($unit_status1->unit_status)}}</option>
                  @endforeach
               </select>
               <label class="active">Unit Status</label>
            </div>
            <!-- -->
         </div>
        <div class="row" id="under_construction">
           <br>
         <div class="row">
            <div class="input-field col l3 m4 s12 display_search">
            @if(isset($category_id) && !empty($category_id))
               @php $category_id2 = json_decode($category_id); 
                  $category_id2 = explode(",",$category_id2[0]);            
               @endphp
               @endif
               <select  id="building_category_uc" name="building_category" class="browser-default" multiple="true">
                  <!-- <option value="" disabled selected>Select</option> -->
                  @foreach($category as $category2)                               
                  <option value="{{$category2->category_id}}" @if(isset($category_id) && !empty($category_id) )  @if(isset($category_id2))  @if(in_array($category2->category_id,$category_id2)) selected @endif @endif   @endif  >{{ ucfirst ($category2->category_name)}}</option>
                  @endforeach                                 
               </select>
               <label class="active">Building Type</label>
            </div>
            <div class="input-field col l3 m4 s12 display_search">

            @if(isset($building_type_id) && !empty($building_type_id) && $building_type_id != 'null')    
                     @php $building_type_id2 = json_decode($building_type_id); 
                     
                        $building_type_id2 = explode(",",$building_type_id2[0]);     
                        
                     @endphp
                    @endif

               <select  id="building_type_uc" name="building_type"  multiple="multiple" class="validate select2 browser-default">
                  <!-- <option value="" disabled selected>Select</option> -->
                  @if(isset($dd_elevation1) && !empty($dd_elevation1))
                  @foreach($dd_elevation1 as $dd_elevation)
                    <option value="{{ $dd_elevation->dd_elevation_id }}" @if(isset($building_type_id2))  @if(in_array($dd_elevation->dd_elevation_id,$building_type_id2)) selected @endif @endif  >{{ ucwords($dd_elevation->elevation_name) }}</option>                                    
                    @endforeach                      
                  @endif
               </select>
               <label class="active">Building Elevation</label>
            </div>
            
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHidebuilding_config_details_uc()">
                        <div class="collapsible-header"  id="col_building_config_details_uc1" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Building Unit Info</h5> </div>
                  </li>
               </ul>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHideamenities1()">
                     <div class="collapsible-header"  id="col_amenities1"  > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Building Ameneties </h5></div>
                  </l1i>
               </ul>
               <!-- <div class="collapsible-header"  id="col_amenities1"  > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Building Ameneties </h5></div> -->
            </div>         
     
         </div>
         <div class="row" style=" display:none;background-color:white !important" id="ameneties_div1" >
                   <br> 
                        <!-- <a href="javascript:void(0)">Click</a> -->
                        <div class="row">
                           <div class="col l2 m4 s12">
                                 <a href="javascript:void(0)" onClick="updateAmenityListUc()"  class="waves-effect waves-light  modal-trigger" style="color: red !important"> Update Amenity List   </a> 
                           </div>
                           <div class="col l2 m4 s12">
                                 <a href="javascript:void(0)" onClick="addAmenityListUc()"  class="waves-effect waves-light  modal-trigger" style="color: red !important"> Add New   </a>                            
                           </div>
                        </div>
                   <br>
                   
                   <div id="appendAmenityUcN"> 
                   @if(isset($project_amenities) && !empty($project_amenities))   
                   <div style="text-align:center;font-weight: 600;">Project Amenity</div ><br>
                     <div class="row">
                        @foreach($project_amenities as $ame)            
                             
                        

                        <div class="col l4 m4 s12">
                                 <div class="col l6 m s12 input-field col l3 m4 s12 display_search">{{ucfirst($ame->amenities)}}</div>
                                 
                                 <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                                    <label  class="active">Description: </label>
                                       <input type="text" class="validate" name="description_ame[]" id="description_ame_id" value="{{$ame->description}}"   placeholder="Enter" style="height: 36px;"  readonly >
                                 </div>
                              
                           
                           </div>  
                           
                        @endforeach
                        </div><hr> 
                    @endif
                     
                   
                     <div id="appendAmenityUc">
                     <div style="text-align:center;font-weight: 600;">Society Amenity</div><br>
                     @if(isset($society_amnities) && !empty($society_amnities))   
                     @foreach($society_amnities as $sam)            
                     

                         <div class="col l4 m4 s12">
                              <div class="col l6 m s12 input-field col l3 m4 s12 display_search">{{ucfirst($sam->amenities)}}</div>
                              
                              <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                                 <label  class="active">Description: </label>
                                    <input type="text" class=" description_ameUC validate" name="description_ameUC[]" id="description_ameUC" value="{{$sam->description}}"   placeholder="Enter" style="height: 36px;"   >
                              </div>

                              <div class="col l3 m3 s12" style="display:none">
                                 <label><input type="checkbox" name="is_availableUC[]" id="is_availableUC" value="{{$sam->society_amnities_id}}" checked ><span>Available</span></label> updateAmenityDesc
                              </div>
               
                        
                        </div>  
                        
                     @endforeach
                     @endif

                     </div>
                  </div>
                    <!-- <div id="appendAmenity"></div> -->

                
         </div>

            <div class="row" id="building_config_details_div_uc1" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)">
               <br>
               Building Name : @if(isset($filterData) && !empty($filterData)) @if(!empty($filterData[0]->building_name)) {{ $filterData[0]->building_name }} @endif @endif  
               <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                            <!-- <th>Building Name</th> -->
                            <th>Wing Name</th>
                            <th>Unit No</th>
                            <th>Configuration</th>
                            <th>Area</th>
                            <th>Configuration Size</th>
                            <th>Balcony</th>
                            <th>Direction</th>
                            <th>Unit View</th>
                            <th>Unit Type</th>
                            @if(isset($building_status) && !empty($building_status) )
                                @if( $building_status == 2 ) 
                                 <th>Show Unit</th>
                                @endif
                            @endif
                            <!-- <th>Unit Status</th> -->
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($filterData as $unitdata)
                        <tr>                            
                            <td>{{$unitdata->wing_name}}</td>
                            <td>
                              <?php
                                 if($unitdata->floor_no <= 0){
                                    $unit_code = abs($unitdata->unit_code)/100;
                                    $unit_code = str_pad($unit_code,3, '0', STR_PAD_LEFT);
                                   }else{
                                       $unit_code = $unitdata->unit_code;
                                   }

                                   echo $unit_code;
                              
                              ?>

                            </td>
                            <td>{{$unitdata->configuration_name}}</td>
                            <td>
                               @if( !empty($unitdata->carpet_area) )
                                  Carpet Area :  {{$unitdata->carpet_area}}
                               @endif
                              <br>
                               @if( !empty($unitdata->build_up_area	) )
                                  Build Up Area :  {{$unitdata->build_up_area	}}
                               @endif
                              
                              <br>
                               @if( !empty($unitdata->mofa_carpet_area) )
                                 Rera Carpet Area :  {{$unitdata->mofa_carpet_area}}
                               @endif
                               
                              
                              </td>
                            <td>{{ ucwords($unitdata->configuration_size) }}</td>
                            <td>@if($unitdata->balcony == 't') Yes @else No @endif</td>
                            <td>{{ ucwords($unitdata->direction) }}</td>
                            <td>{{$unitdata->unit_view}}</td>
                            <td>{{ ucwords($unitdata->unit_type) }}</td>

                                 @if( $unitdata->building_status == 2 ) 
                                 <td>@if($unitdata->show_unit == 't') Yes @else No @endif </td>
                                 @endif
                          

                            <!-- <td>@if($unitdata->status == 't') Active @else Inactive @endif</td> -->
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
            </div>
         <br>
         <div class="row">
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <select  id="construction_technology_uc" name="construction_technology" class="browser-default">
                  <option value="" disabled selected>Select</option>
                  @foreach($construction_technology1 as $construction_technology2)
                  <option value="{{$construction_technology2->construction_technology_id}}" @if(isset($construction_technology) && !empty($construction_technology) )  @if( $construction_technology2->construction_technology_id == $construction_technology ) selected  @endif  @endif >{{ ucwords($construction_technology2->construction_technology_name)}}</option>
                  @endforeach                                  
               </select>
               <label class="active">Construction technology </label>
            </div>
            @if(isset($parking) && !empty($parking) && $parking != 'null')
            @php $parking11 = json_decode($parking); 
               $parking11 = explode(",",$parking11[0]);            
            @endphp
            @endif
            <div class="input-field col l3 m4 s12 display_search" >
              
               <select  id="parking_uc"    multiple="multiple" class="validate select2 browser-default">
                  <!-- <option value="" disabled selected>Select</option> -->
                  @if(isset($parking1) && !empty($parking1))
                     @foreach($parking1 as $parking1)
                     <option value="{{ $parking1->parking_id  }}" @if(isset($parking11))  @if(in_array($parking1->parking_id,$parking11)) selected @endif @endif   >{{ ucwords($parking1->parking_name) }}</option>
                     @endforeach                              
                  @endif
               </select>
               <label class="active">Parking Type</label>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px;">
                  <li onClick="ShowHideconstruction()">
                     <div class="collapsible-header"  id="col_construction" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Building Construction Update </h5> </div>
                  </li>
               </ul>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px;">
                  <li onClick="ShowHidefloor()">
                     <div class="collapsible-header"  id="col_floor" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Floor rice</h5></div>
                  </li>
               </ul>
            </div>       

         </div>
         <div class="row" id="construcition_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)">
          <br>
             <div class="row">
               <input type="hidden" id="society_construction_stage_id">
                 <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                     <select  id="building_construction_stage_u" name="building_construction_stage"   multiple="multiple" class="validate select2 browser-default">
                        <!-- <option value="" disabled selected>Select</option> -->
                        @foreach($dd_construction_stage as $dd_construction_stage)
                           <option value="{{$dd_construction_stage->construction_stage_id}}">{{ucwords($dd_construction_stage->construction_stage)}}</option>
                        @endforeach 
                                                   
                     </select>
                     <label class="active">Construction Stage </label>
                  </div>
                  <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                     <label  class="active">Construction Stage dated </label>
                     <input type="text" class="datepicker" name="building_construction_stage_date" id="building_construction_stage_date_u"   placeholder="Enter"   @if(isset($construction_stage_date) && !empty($construction_stage_date)) value="{{$construction_stage_date}}" @endif >
                  </div>
                  <div class="input-field col l3 m4 s12 display_search">
                     <label  class="active">Construction stage Images   </label>
                     <input type="file" name="construction_stage_images" multiple="true"  id="construction_stage_images_u" class="dropify" data-default-file="" style="padding-top: 14px;">                        
                     @if(isset($getSocietyPhotosData) && !empty($getSocietyPhotosData))
                     @php $i=1; @endphp 
                     @foreach($getSocietyPhotosData as $csi)
                     @if($csi->meta_key == 'construction_stage_images')
                     
                        <span class="{{$csi->society_photos_id}}"> <a href="{{ asset('images/'.$csi->url) }}" target="_blank"> Construction Stage Images {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$csi->society_photos_id}}')">X</a> &nbsp; , </span>
                        @php $i++; @endphp 
                        @endif
                     
                     @endforeach    
                     @endif
                  </div>
                  <div class="input-field col l2 m4 s12" id="InputsWrapper2">                        
                     <label  class="active">Comments </label>
                     <input type="text" class="validate" name="comments" id="comments_bc_u"   placeholder="Enter"  @if(isset($comments) && !empty($comments)) value="{{$comments}}" @endif >
                  </div>

                  <div class="row" >
                        <div class="input-field col l2 m2 s6 display_search">
                        <div class="preloader-wrapper small active" id="loader1" style="display:none">
                           <div class="spinner-layer spinner-green-only">
                           <div class="circle-clipper left">
                              <div class="circle"></div>
                           </div><div class="gap-patch">
                              <div class="circle"></div>
                           </div><div class="circle-clipper right">
                              <div class="circle"></div>
                           </div>
                           </div>
                        </div>
                  </div></div>
                  
                  <div id="submitConstruction">
                     <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                           <a href="javascript:void(0)" onClick="saveConstructionUpdate()" class="save_construction btn-small  waves-effect waves-light green darken-1" id="save_construction" >Save</a>
                     </div> 
                     <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                     <a href="javascript:void(0)" onClick="clear_construction()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_construction"><i class="material-icons dp48">clear</i></a>
                     </div> 
                  </div> 


            </div>
            <!-- <div class="row" id="submitPros">
               <div class="col l4 m4">
               <a href="javascript:void(0)" onClick="saveConstructionUpdate()" class="save_construction btn-small  waves-effect waves-light green darken-1" id="save_construction" >Save</a>
               </div>
               <div class="col l3 m3">
                        <a href="javascript:void(0)" onClick="clear_proscons()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_proscons"><i class="material-icons dp48">clear</i></a>
               </div>
            </div> -->



            <br>
                        <div class="row">
                          <table class="bordered" id="building_construction_table" style="font-size: unset;">
                              <thead>
                              <tr style="color: white;background-color: #ffa500d4;">
                              <th>Sr No. </th>
                              <th>Building construction stage</th>
                              <th>Building construction stage date</th>
                              <th>Construction stage Images</th>
                              <th>Comment</th>
                              <th>Last Updated Date</th>
                              <th>Action</th>               
                              </tr>
                              </thead>
                              <tbody>
                                 @php $incConstruction1= count($getConstructionStage); @endphp
                                 @if(isset($getConstructionStage))
                                 @php $ic3=count($getConstructionStage); @endphp
                                    @foreach($getConstructionStage as $val)                                 
                                          @php 
                                          $building_construction_stage2 = json_decode($val->building_construction_stage); 
                                          $building_construction_stage2 = explode(",",$building_construction_stage2[0]);     
                                       @endphp

                                       <tr id="bctr_{{$val->society_construction_stage_id}}">
                                        
                                         <td >@php $incConstruction= $ic3--; @endphp {{$incConstruction}}</td>
                                          <td id="cs_{{$val->society_construction_stage_id}}"> 
                                                         
                                             <?php
                                             if(isset($val->building_construction_stage[0]) && !empty($val->building_construction_stage[0]) ){
                                                $get = DB::select("select construction_stage from dd_construction_stage where construction_stage_id in (".json_decode($val->building_construction_stage)[0].")");

                                                foreach ($get as $key => $value) {
                                                   if(!empty($value->construction_stage)){
                                                      echo ucwords($value->construction_stage).',';
                                                   }
                                                }
                                             }
                                            ?>
                                          </td>
                                          <td id="cd_{{$val->society_construction_stage_id}}"> {{$val->building_construction_stage_date}}</td>
                                          <td id="i_{{$val->society_construction_stage_id}}">
                                             @if(isset($val->construction_stage_images) && !empty($val->construction_stage_images))
                                                @php $construction_stage_images = json_decode($val->construction_stage_images); $i=0; @endphp 
                                                @foreach($construction_stage_images as $construction_stage_images)
                                                @php $x = pathinfo($construction_stage_images, PATHINFO_FILENAME); @endphp
                                                <span class="{{$x}}"><a href="{{ asset('images/'.$construction_stage_images) }}"  target="_blank"> Construction Image {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImageConst('{{$construction_stage_images}}#{{$val->society_construction_stage_id}}')">X</a> &nbsp; , </span>
                                                   @php $i++; @endphp 
                                                @endforeach    
                                             @endif   
                                          </td>
                                          <td id="c_{{$val->society_construction_stage_id}}"> {{$val->comments_bc}}</td>
                                          <td id="ud_{{$val->society_construction_stage_id}}"> {{$val->created_date}}</td>
                                          <td>
                                            <a href="javascript:void(0)"  class="waves-effect waves-light" onClick="updateConstructionStage('{{$val->society_construction_stage_id}}')"  >Edit</a> |
                                             <a href='javascript:void(0)' class="waves-effect waves-light"   onClick="confirmDelConstructionStage('{{$val->society_construction_stage_id}}')" >Delete
                                          </td>
                                       </tr>
                                    @endforeach
                                 @endif                                       
                              </tbody>
                           </table>
                           <input type="hidden" id="incConstruction1" value="{{$incConstruction1}}">
                        </div>

         </div>
         <div class="row"  id='floor_div' style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"> <br>
            <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":false}'>
               <div class="clonable" data-ss="1">
                  <div class="row">
                     <input type="hidden" value="0" name="society_floor_rise_id_uc" id="society_floor_rise_id_uc">
                     <div class="col l3"></div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Floor slab </label>
                        <input type="text" class="  validate" name="floor_slab_from_uc" id=floor_slab_from_uc  placeholder="Enter" >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Floor slab </label>
                        <input type="text" class="  validate" name="floor_slab_to_uc" id="floor_slab_to_uc"  placeholder="Enter" >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Configuration </label>
                        <input type="text" class=" validate" name="configuration_uc" id="configuration_uc"   placeholder="Enter" >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Area </label>
                        <input type="text" class=" validate" name="area_uc" id="area_uc"   placeholder="Enter" >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Rs. Per Sq. ft. </label>
                        <input type="text" class="  validate" name="rs_per_sq_ft_uc"  id="rs_per_sq_ft_uc"   placeholder="Enter" >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Box price </label>
                        <input type="text" class="  validate" name="box_price_uc" id="box_price_uc"   placeholder="Enter" >                                
                     </div>
                     <div class="input-field col l2 m4 s12">    
                        <a href="javascript:void(0)" id="hap" class="clonable-button-add" title="Add"   style="color: red !important"> <i class="material-icons  dp48">add</i></a> 
                        <a href="javascript:void(0)" class="clonable-button-close" title="Remove"  style="color: red !important"> <i class="material-icons  dp48">remove</i></a> 
                     </div>
                  </div>
               </div>
            </div>
            <br>
            @if(isset($getFloorRiseData) && !empty($getFloorRiseData))
            @foreach($getFloorRiseData as $getFloorRiseData)
            <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":false}'>
               <div class="clonable" data-ss="1">
                  <div class="row">
                     <input type="hidden" value="{{$getFloorRiseData->society_floor_rise_id}}" name="society_floor_rise_id_uc" id="society_floor_rise_id_uc">
                     <div class="col l3"></div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Floor slab </label>
                        <input type="text" class="  validate" name="floor_slab_from_uc" id="floor_slab_from_uc"   placeholder="Enter" value="{{$getFloorRiseData->floor_slab_from}}" >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Floor slab </label>
                        <input type="text" class="  validate" name="floor_slab_to_uc" id="floor_slab_to_uc"  placeholder="Enter" value="{{$getFloorRiseData->floor_slab_to}}"  >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Configuration </label>
                        <input type="text" class=" validate" name="configuration_uc" id="configuration_uc"  placeholder="Enter" value="{{$getFloorRiseData->configuration}}" >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Area </label>
                        <input type="text" class=" validate" name="area_uc" id="area_uc"   placeholder="Enter" value="{{$getFloorRiseData->area}}" >                                
                     </div>

                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Rs. Per Sq. ft. </label>
                        <input type="text" class="  validate" name="rs_per_sq_ft_uc" id="rs_per_sq_ft_uc"   placeholder="Enter" value="{{$getFloorRiseData->rs_per_sq_ft}}" >                                
                     </div>
                     <div class="input-field col l1 m4 s12">
                        <label  class="active">Box price </label>
                        <input type="text" class="  validate" name="box_price_uc" id="box_price_uc"  placeholder="Enter" value="{{$getFloorRiseData->box_price}}" >                                
                     </div>
                     <div class="input-field col l2 m4 s12">    
                        <!-- <a href="javascript:void(0)" id="hap" class="clonable-button-add" title="Add"   style="color: red !important;display:block !important"> <i class="material-icons  dp48">add</i></a>  -->
                        <a href="javascript:void(0)" class="clonable-button-close" title="Remove"  style="color: red !important; display:inline-block !important"> <i class="material-icons  dp48">remove</i></a> 
                        
                        </div>
                  </div>
               </div>
            </div>
            @endforeach
            @endif
         </div>
            <br>
         <div class="row">
            <div class="input-field col l3 m4 s12" id="InputsWrapper2" >
                @if(isset($funded_by) && !empty($funded_by))    
                  <input type="hidden" id="fundedby" value="{{$funded_by}}">
                  @endif

                  @if(isset($funded_by) && !empty($funded_by))    
                     @php $funded_by22 = json_decode($funded_by); 
                        $funded_by22 = explode(",",$funded_by22);            
                     @endphp
                    @endif

               <select  id="funded_by_uc" name="funded_by[]"   multiple="multiple" class="browser-default">
                  <option value=""  >Select</option>
                  @foreach($funded_by1 as $funded_by)                            
                  <option value="{{$funded_by->funded_by_id}}"  @if(isset($funded_by22))  @if(in_array($funded_by->funded_by_id,$funded_by22)) selected @endif @endif >{{$funded_by->funded_by_name}}</option>
                  @endforeach
                 
               </select>
               <label  class="active">Funded by </label>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHidepayment()">
                     <div class="collapsible-header"  id="col_payment" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Payment Plan</h5> </div>
                  </li>
               </ul>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHidebrokerage()">
                     <div class="collapsible-header"  id="col_brokerage" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Brokerage fees </h5> </div>
                  </li>
               </ul>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHidedesc()">
                     <div class="collapsible-header"  id="col_desc"> <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;"> Building Description </h5> </div>
                  </li>
               </ul>
            </div>
         </div>
        
         <div class="row" id="payment_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"><br>
         @if(isset($payment_type1) && !empty($payment_type1) )
         @php $payment_type1 = json_decode($payment_type1); 
            $exp = explode(",",$payment_type1[0]);            
         @endphp
         @endif
            <div class="input-field col l3 m4 s12 display_search">
               <select  id="payment_plan_uc" name="payment_plan[]"  class="browser-default" multiple="true">
                  <option value="" disabled >Select</option>
                  @foreach($payment_type as $payment_type)                            
                     <option value="{{$payment_type->payment_type_id}}" @if(isset($exp))  @if(in_array($payment_type->payment_type_id,$exp)) selected @endif @endif     >{{$payment_type->payment_name}}</option>
                  @endforeach
               </select>
               <label class="active"> Payment Plan</label>
            </div>
            <div class="input-field col l3 m3 s3 display_search" >
               <input type="text" class="input_select_size" name="payment_plan_comment" id="payment_plan_comment_uc"  placeholder="Enter" @if(isset($building_constructed_area) && !empty($building_constructed_area)) value="{{$building_constructed_area}}" @endif  >
               <label> Comment</label>
            </div>
         </div>
         <div class="row"  id='brokerage_div' style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"><br>
            <div class="input-field col l3">
            <label  class="active">Comment </label>
                  <input type="text" class="validate" name="brokarage_comment_uc" id="brokarage_comment_uc"  value="@if(isset($brokarage_comments)) {{$brokarage_comments}}  @endif"  placeholder="Enter" >                                
            </div>
            <div class="input-field col l3">
            <label  class="active">From date </label>
                  <input type="text" class="datepicker" name="brokarage_from_date_uc" id="brokarage_from_date_uc"  value="@if(isset($brokarage_from_date)) {{$brokarage_from_date}}  @endif"  placeholder="Enter" >                                
            </div>

            <div class="input-field col l3">
            <label  class="active">To date </label>
                  <input type="text" class="datepicker" name="brokarage_to_date_uc" id="brokarage_to_date_uc"  value="@if(isset($brokarage_to_date)) {{$brokarage_to_date}}  @endif"  placeholder="Enter" >                                
            </div>

            <div class="col l3">
               <table class="bordered" style="font-size: unset;">
                  <thead>
                     <tr style="color: white;background-color: #ffa500d4;">
                        <th >No. of Booking</th>
                        <th>Brokerage %</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>0 to 2
                        </td>
                        <td><input class="col l10" type="number" min="0" class="  validate" name="zero_to_two_uc" id="zero_to_two_uc"   placeholder="Enter" @if(isset($getBrokerageFeeData[0]->zero_to_two) && !empty($getBrokerageFeeData[0]->zero_to_two))  value="{{$getBrokerageFeeData[0]->zero_to_two}}" @endif ></td>
                     </tr>
                     <tr>
                        <td>3 to 5
                        </td>
                        <td><input class="col l10" type="number" min="0" class="  validate" name="three_to_five_uc" id="three_to_five_uc"   placeholder="Enter" @if(isset($getBrokerageFeeData[0]->three_to_five) && !empty($getBrokerageFeeData[0]->three_to_five))  value="{{$getBrokerageFeeData[0]->three_to_five}}" @endif ></td>
                     </tr>
                     <tr>
                        <td>6 to 10
                        </td>
                        <td><input class="col l10" type="number" min="0" class="  validate" name="six_to_ten_uc" id="six_to_ten_uc"   placeholder="Enter" @if(isset($getBrokerageFeeData[0]->six_to_ten) && !empty($getBrokerageFeeData[0]->six_to_ten))  value="{{$getBrokerageFeeData[0]->six_to_ten}}" @endif  ></td>
                     </tr>
                     <tr>
                        <td>11 and above
                        </td>
                        <td><input class="col l10" type="number" min="0" class="  validate" name="above_eleven_uc" id="above_eleven_uc"   placeholder="Enter" @if(isset($getBrokerageFeeData[0]->above_eleven) && !empty($getBrokerageFeeData[0]->above_eleven))  value="{{$getBrokerageFeeData[0]->above_eleven}}" @endif ></td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <div class="row" id="desc_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"><br>
            <section class="full-editor">
               <div class="row">
                  <div class="col s12">
                     <div class="card">
                        <br>
                        <div class="card-content">
                           Building  Description
                           <div class="row">
                              <div class="col s12 l10 m4">
                                 <textarea style="border-color: #5b73e8; padding-top: 12px;width: 1211px; height: 90px;overflow-y: scroll;" placeholder="Enter" name="building_description_uc" id="building_description_uc"  > @if(isset($building_description) && !empty($building_description)) {{$building_description}} @endif</textarea>
                                 <!-- <div id="full-wrapper" name="html_editor2">
                                    <div id="full-container" name="html_editor1">
                                        <div class="editor" name="html_editor">
                                            
                                            
                                        </div>
                                    </div>
                                    </div> -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
            <br>
         <div class="row">
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHideaddress()">
                     <div class="collapsible-header"  id="col_address_uc" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Building Address</h5></div>
                  </li>
               </ul>
            </div>
        
            <div class="input-field col l3 m4 s12" >
               <label  class="active">Maintenance Amount @ on carpet area </label>
               <div class="rupeeSign1" >
                  <input type="text" min="0" class="validate" name="maintenance_amount_uc" id="maintenance_amount_uc"    value="{{$maintenance_amount}}"  >
               </div>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHidelandparcel()">
                     <div class="collapsible-header"  id="col_landparcel" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;"> Building Land Parcel </h5> </div>
                  </li>
               </ul>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHideproscons()">
                     <div class="collapsible-header"  id="col_proscons" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;"> Building Pros & Cons </h5></div>
                  </li>
               </ul>
            </div>
       
            <!--    -->
         </div>
         <div class="row" id="address_div_uc" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)">
            <br>
            <div class="input-field col l3 m4 s12" >
               <input type="text" class="validate" name="society_geo_location_lat_uc" id="society_geo_location_lat_uc"  placeholder="Enter LAT & LONG" @if(isset($lattitde) && !empty($lattitde)) value="{{$lattitde}}" @endif >
               <label  class="active">Building Geo Location </label>
            </div>

            <!-- <div class="col m3 l3 s12">
               <div class="row">
                  <div class="input-field col l6 m6 s8" style="padding: 0 10px;">
                     <label  class="active">Building Geo Location </label>
                     <input type="text" class="validate" name="society_geo_location_lat_uc" id="society_geo_location_lat_uc"  placeholder="Enter LAT" @if(isset($lattitde) && !empty($lattitde)) value="{{$lattitde}}" @endif >
                  </div>
                  <div class="input-field col l6 m6 s4 mobile_view" style="padding: 0 10px;">
                     <label  class="active">Building Geo Location </label>
                     <input type="text" class="validate" name="society_geo_location_long_uc" id="society_geo_location_long_uc"   placeholder="Enter LONG" @if(isset($longitude) && !empty($longitude)) value="{{$longitude}}" @endif>
                  </div>
               </div>
            </div> -->
            

            <!-- <div class="input-field col l3 m4 s12 display_search">
               <select  id="state_uc" name="state_uc" class="browser-default" onChange="getCity(this)">
                  <option value=""  selected>Select</option>
                  @if(isset($state) && !empty($state))
                     @foreach($state as $state1)
                     <option value="{{$state1->dd_state_id}}" @if(isset($state_id) && !empty($state_id) )  @if( $state1->dd_state_id == $state_id ) selected  @endif  @endif>{{ ucwords($state1->state_name)}}</option>
                     @endforeach                         
                  @endif         
               </select>
               <label class="active">State </label>
            </div>

            <div class="input-field col l3 m4 s12 display_search">
               <select  id="city_uc" name="city_uc" class="browser-default" onChange="getlocation(this)">
                  <option value="" disabled selected>Select</option>
                  @if(isset($city) && !empty($city))
                     @foreach($city as $city1)
                     <option value="{{$city1->city_id}}" @if(isset($city_id) && !empty($city_id) )  @if( $city1->city_id == $city_id ) selected  @endif  @endif >{{ ucwords($city1->city_name)}}</option>
                     @endforeach                       
                  @endif           
               </select>
               <label class="active">City </label>
            </div>

            <div class="input-field col l3 m4 s12 display_search">
               <select  id="location_uc" name="location_uc" class="browser-default" onChange="getsublocation(this)">
                  <option value="" disabled selected>Select</option>
                  @if(isset($location) && !empty($location))
                     @foreach($location as $location1)
                     <option value="{{$location1->location_id}}" @if(isset($location_id) && !empty($location_id) )  @if( $location1->location_id == $location_id ) selected  @endif  @endif >{{ ucwords($location1->location_name)}}</option>
                     @endforeach
                  @endif
               </select>
               <label class="active">Location </label>
            </div>

            <div class="input-field col l3 m4 s12 display_search">
               <select  id="sub_location_uc" name="sub_location_uc" class="browser-default">
                  <option value="" disabled selected>Select</option>
                  @if(isset($sub_location) && !empty($sub_location))
                     @foreach($sub_location as $sub_location1)
                     <option value="{{$sub_location1->sub_location_id}}" @if(isset($sub_location_id) && !empty($sub_location_id) )  @if( $sub_location1->sub_location_id == $sub_location_id ) selected  @endif  @endif >{{ ucwords($sub_location1->sub_location_name)}}</option>
                     @endforeach 
                  @endif                                
               </select>
               <label class="active">Sub Location </label>
            </div> -->


            <div class="input-field col l3 m4 s12 display_search">                                
                    
                    <select  id="sub_location_uc" name="sub_location" class="validate  browser-default" onChange="getsublocationUC(this)">
                    <option value="" disabled selected>Select</option>
                    @if(isset($sub_locationR) && !empty($sub_locationR))
                        @foreach($sub_locationR as $sub_location1)
                        <option value="{{$sub_location1->sub_location_id}}" @if(isset($sub_location_id))  @if($sub_location1->sub_location_id == $sub_location_id)  selected @endif @endif  >{{ ucwords($sub_location1->sub_location_name)}}</option>
                        <!--  -->
                        @endforeach                              
                    @endif   
                    </select>
                    <label class="active">Sub Location</label>
            </div>


            <div class="input-field col l3 m4 s12 display_search">                                
                    <select  id="location_uc" name="location" class="validate  browser-default"  >
                    <option value="" disabled selected>Select</option>
                    @if(isset($locationR) && !empty($locationR))
                        @foreach($locationR as $location1)
                        <option value="{{$location1->location_id}}" @if(isset($location_id))  @if($location1->location_id == $location_id)  selected @endif @endif >{{ ucwords($location1->location_name)}}</option>
                        @endforeach
                    @endif
                    </select>
                    <label class="active">Location</label>
            </div>
   
            <div class="input-field col l3 m4 s12 display_search">                                
                    <select  id="city_uc" name="city" class="validate  browser-default" >
                     @if(isset($cityR) && !empty($cityR))
                        @foreach($cityR  as $cty)
                        <option value="{{$cty->city_id}}" @if(isset($city_id)) @if($cty->city_id == $city_id) selected @endif @endif>{{ ucfirst($cty->city_name) }}</option>
                        @endforeach
                       @endif                              
                    </select>
                    <label class="active">City</label>
            </div>

            <div class="input-field col l3 m4 s12 display_search">                                
                    <select  id="state_uc" name="state" class="validate  browser-default" >
                    <option value="" disabled selected>Select</option>
                         @foreach($stateR as $state1)
                        <option value="{{$state1->dd_state_id}}" @if(isset($state_id)) @if($state1->dd_state_id == $state_id) selected @endif @endif >{{ ucwords($state1->state_name)}}</option>
                        @endforeach                                  
                    </select>
                    <label class="active">State</label>
            </div>
          
            <div class="input-field col l3 m4 s12" >
               <input type="text"  class="input_select_size" name="building_office_address_uc" id="building_office_address_uc"  placeholder="Enter" @if(isset($sales_office_addressP) && !empty($sales_office_addressP)) value="{{$sales_office_addressP}}" @endif  >
               <label> Building office address</label>
            </div>
         </div>
         <div class="row" id="landparcel_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"><br>
            <div class="input-field col l3 m4 s12" id="InputsWrapper">
               <div class="row">
                  <div class="input-field col m8 s8" style="padding: 0 10px;">
                     <input type="number" min="0" class="input_select_size" name="building_land_parcel_area_uc" id="building_land_parcel_area_uc"  placeholder="Enter" @if(isset($building_land_parcel_area) && !empty($building_land_parcel_area)) value="{{$building_land_parcel_area}}" @endif  >
                     <label>
                        Building Land Parcel
                  </div>
                  <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                  <select  id="building_land_parcel_area_unit" name="building_land_parcel_area_unit" class=" browser-default ">
                  @foreach($unit as $un)
                  <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                  @endforeach
                  </select>
                  </div>
               </div>
            </div>
            <div class="input-field col l3 m4 s12" id="InputsWrapper">
            <div class="row">
            <div class="input-field col m8 s8" style="padding: 0 10px;">
            <input type="number" min="0" class="input_select_size" name="building_open_space_uc" id="building_open_space_uc"  placeholder="Enter" @if(isset($building_open_space) && !empty($building_open_space)) value="{{$building_open_space}}" @endif >
            <label> Building Open space</label>
            </div>
            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
            <select  id="building_open_space_unit" name="building_open_space_unit" class=" browser-default ">
            @foreach($unit as $un)
            <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
            @endforeach
            </select>
            </div>
            </div>
            </div> 
            <div class="input-field col l3 m4 s12" id="InputsWrapper">
               <div class="row">
                  <div class="input-field col m8 s8" style="padding: 0 10px;">
                     <input type="number" min="0" class="input_select_size" name="building_constructed_area_uc" id="building_constructed_area_uc"  placeholder="Enter" @if(isset($building_constructed_area) && !empty($building_constructed_area)) value="{{$building_constructed_area}}" @endif  >
                     <label> Building Constructed Area</label>
                  </div>
                  <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                     <select  id="building_constructed_area_unit" name="building_constructed_area_unit" class=" browser-default ">
                        @foreach($unit as $un)
                        <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div>
         </div>
         <div class="row" id="proscons_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)" ><br>
                  <div class="row">
                     <input type="hidden" id="op_society_pros_cons_id">
                        <div class="input-field col l3 m34 s12" id="InputsWrapper2">
                        <label  class="active">Pros of Building </label>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px;width:104% !important" placeholder="Enter" name="sales_office_address" rows='20' col='40'></textarea> -->
                        <input type="text" class="validate pros_of_building" name="pros_of_building" id="pros_of_building"  placeholder="Enter" @if(isset($pros) && !empty($pros)) value="{{$pros}}" @endif >
                        </div>
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Cons of Building </label>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px;width:104% !important" placeholder="Enter" name="sales_office_address" rows='20' col='40'></textarea> -->
                        <input type="text" class="validate cons_of_building" name="cons_of_building" id="cons_of_building"   placeholder="Enter"  @if(isset($cons) && !empty($cons)) value="{{$cons}}" @endif>
                        </div>
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                              <!-- <a href="javascript:void(0)" onClick="saveProsCons1()" class="btn-small  waves-effect waves-light green darken-1" >Save</a> -->

                              <div class="row" >
                                 <div class="input-field col l2 m2 s6 display_search">
                                 <div class="preloader-wrapper small active" id="loader2" style="display:none">
                                    <div class="spinner-layer spinner-green-only">
                                    <div class="circle-clipper left">
                                       <div class="circle"></div>
                                    </div><div class="gap-patch">
                                       <div class="circle"></div>
                                    </div><div class="circle-clipper right">
                                       <div class="circle"></div>
                                    </div>
                                    </div>
                                 </div>
                              </div></div>



                              <div class="row"  id="submitProsUC">
                                <div class="col l4 m4">
                                    <a href="javascript:void(0)" onClick="saveProsCons1()" class=" save_proscons1 btn-small  waves-effect waves-light green darken-1 " id="save_proscons1">Save</a>
                                </div>
                                <div class="col l3 m3">
                                        <a href="javascript:void(0)" onClick="clear_proscons1()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_proscons1"><i class="material-icons dp48">clear</i></a>
                                </div>
                            </div>
                        </div> 
                   </div>
                      <br>
                        <div class="row">
                          <table class="bordered" id="pros_cons_table" style="font-size: unset;">
                              <thead>
                              <tr style="color: white;background-color: #ffa500d4;">
                              <th>Sr No. uc </th>
                              <th>Pros</th>
                              <th>Cons</th>
                              <th>Last Updated Date</th>
                              <th>Action</th>               
                              </tr>
                              </thead>
                              <tbody>
                              @php $incPros1= count($getProjectProsConsData); @endphp
                                 @if(isset($getProjectProsConsData))
                                 @php $i3=count($getProjectProsConsData); @endphp
                                    @foreach($getProjectProsConsData as $val)
                                       <tr id="pctruc_{{$val->op_society_pros_cons_id}}">

                                          <td >@php $incPros= $i3--; @endphp {{$incPros}}</td>
                                          <td id="puc_{{$val->op_society_pros_cons_id}}"> {{$val->pros_of_building}}</td>
                                          <td id="cuc_{{$val->op_society_pros_cons_id}}"> {{$val->cons_of_building}}</td>
                                          <td id="ud2uc_{{$val->op_society_pros_cons_id}}"> {{$val->updated_date}}</td>
                                          <td>
                                            <a href="javascript:void(0)"  class="waves-effect waves-light" onClick="updateProsCons('{{$val->op_society_pros_cons_id}}')"  >Edit</a> |
                                             <a href='javascript:void(0)' class="waves-effect waves-light"   onClick="confirmDelProsCons('{{$val->op_society_pros_cons_id}}')" >Delete
                                          </td>
                                       </tr>
                                    @endforeach
                                 @endif                                       
                              </tbody>
                           </table>
                           <input type="hidden" id="incPros" value="{{$incPros1}}">
                        </div>
         </div>
          <br>
         <div class="row">
            <div class="input-field col l3 m4 s12 display_search">
               <select   name="building_rating_uc" id="building_rating_uc" class="browser-default">
                  <option value="" disabled selected>Select</option>
                  @foreach($rating as $rating1)
                  <option value="{{ $rating1->rating_id  }}" @if(isset($building_rating) && !empty($building_rating) )  @if( $rating1->rating_id == $building_rating ) selected  @endif  @endif>{{ $rating1->rating }}</option>                                    
                  @endforeach                                    
               </select>
               <label class="active">Building Rating</label>
            </div>
            <div class="input-field col l3 m4 s12 display_search">
               <select   name="indian_green_building_uc" id="indian_green_building_uc" class="browser-default">
                  <option value="" disabled selected>Select</option>
                  <option value="yes" @if(isset($indian_green_building) && !empty($indian_green_building)) @if($indian_green_building=='yes')  selected @endif @endif>Yes</option>
                        <option value="no" @if(isset($indian_green_building) && !empty($indian_green_building)) @if($indian_green_building=='no')  selected @endif @endif>No</option>
               </select>
               <label class="active">The Indian Green Building Council</label>
            </div>
            <div class="input-field col l3 m4 s12" >
               <label  class="active">Building Launch Date </label>
               <input type="text" class="datepicker" name="building_lauch_date" id="building_lauch_date_uc"   placeholder=""  @if(isset($building_launch_date) && !empty($building_launch_date)) value="{{$building_launch_date}}" @endif > 
            </div>
            <div class="input-field col l3 m4 s12" >
               <!-- <select  id="builder_completion_year" name="builder_completion_year" class="browser-default">
                  <option value=""  selected>Select</option>
                  <?php 
                     $CY = date('Y');
                     for($i=0; $i < 15; $i++){ $f = $CY+$i ;?>
                  <option value="{{$f}}" @if(isset($builder_completion_year) && !empty($builder_completion_year)) @if($f == $builder_completion_year) selected  @endif @endif>{{ $f }}</option>                                    
                  <?php        }                            
                     ?>
               </select>
               <label class="active">Builder Completion Year </label> -->
               <label  class="active">Builder Completion Year </label>
                  <input type="text" class="datepicker" name="builder_completion_year_uc" id="builder_completion_year_uc"   placeholder=""  @if(isset($builder_completion_year) && !empty($builder_completion_year)) value="{{$builder_completion_year}}" @endif  >
            </div>
         </div>
            <br>
         <div class="row" >
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <label  class="active"> Rera Completion Year </label>
               <input type="text" class="datepicker" name="rera_completion_year_uc" id="rera_completion_year_uc"   placeholder="" @if(isset($rera_completion_year) && !empty($rera_completion_year)) value="{{$rera_completion_year}}" @endif >
            </div>
            <div class="input-field col l3 m4 s12 display_search">
               <label  class="active">RERA Certificate  </label>
               <input type="file" name="rera_certificate_uc"  id="rera_certificate_uc" multiple="true" class="dropify" data-default-file="" style="padding-top: 14px;">
               @if(isset($getSocietyPhotosData) && !empty($getSocietyPhotosData))
               @php $ib=1; @endphp 
               @foreach($getSocietyPhotosData as $csi)
               @if($csi->meta_key == 'rera_certificate')
               <span class="{{$csi->society_photos_id}}"> <a href="{{ asset('images/'.$csi->url) }}" target="_blank"> RERA Cert. {{$ib}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$csi->society_photos_id}}')">X</a> &nbsp; , </span>
               @php $ib++; @endphp 
               @endif
               
               @endforeach    
               @endif
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHideshowflat()">
                     <div class="collapsible-header"  id="col_showflat" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;"> Show Flat Images </h5> </div>
                  </li>
               </ul>
            </div>
            <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="ShowHideimagesvideo()">
                     <div class="collapsible-header"  id="col_imagesvideo" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;"> Building Brochure, Images, Video And Video Link </h5></div>
                  </li>
               </ul>
            </div>        

         </div>
         <div class="row" id="showflat_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"><br>
            <div class="input-field col l3 m4 s12 display_search">
               <label  class="active">Show Flat Images  </label>
               <input type="file" name="flat_images_uc" multiple="true" id="flat_images_uc" class="dropify" data-default-file="" style="padding-top: 14px;">
               @if(isset($getSocietyPhotosData) && !empty($getSocietyPhotosData))
               @php $if=1; @endphp 
               @foreach($getSocietyPhotosData as $csi)
               @if($csi->meta_key == 'flat_images')
               
                  <span class="{{$csi->society_photos_id}}"> <a href="{{ asset('images/'.$csi->url) }}" target="_blank">  Flat Images {{$if}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$csi->society_photos_id}}')">X</a> &nbsp; , </span>
                  @php $if++; @endphp 
               @endif
               
               @endforeach    
               @endif
            </div>
            
            
         </div>
         <div class="row" id="imagesvideo_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"><br>
            <div class="input-field col l3 m4 s12 display_search">
               <label  class="active">Building Images  </label>
               <input type="file" name="building_images_uc"  multiple="true" id="building_images_uc" class="dropify" data-default-file="" style="padding-top: 14px;">                        
               @if(isset($getSocietyPhotosData) && !empty($getSocietyPhotosData))
               @php $ib1=1; @endphp 
               @foreach($getSocietyPhotosData as $csi)
               @if($csi->meta_key == 'building_images')
               
               <span class="{{$csi->society_photos_id}}"> <a href="{{ asset('images/'.$csi->url) }}" target="_blank">  Building Images {{$ib1}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$csi->society_photos_id}}')">X</a> &nbsp; , </span>
               @php $ib1++; @endphp 
               @endif
               
               @endforeach    
               @endif
            </div>
            <div class="input-field col l3 m4 s12 display_search">
               <label  class="active">Building Video  </label>
               <input type="file" name="building_video_uc" multiple="true" id="building_video_uc" class="dropify" data-default-file="" style="padding-top: 14px;">                        
               @if(isset($getSocietyPhotosData) && !empty($getSocietyPhotosData))
               @php $iv1=1; @endphp 
               @foreach($getSocietyPhotosData as $csi)
               @if($csi->meta_key == 'building_video')
               
               <span class="{{$csi->society_photos_id}}"> <a href="{{ asset('images/'.$csi->url) }}" target="_blank">  Building Video {{$iv1}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$csi->society_photos_id}}')">X</a> &nbsp; , </span>
               @php $iv1++; @endphp 
               @endif
               
               @endforeach    
               @endif
            </div>
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <label  class="active">Building Video  Link</label>
               <input type="text" class="validate" name="building_video_links_uc"  id="building_video_links_uc"    placeholder="Enter"  @if(isset($building_video_links) && !empty($building_video_links)) value="{{$building_video_links}}" @endif>
            </div>
         </div>
         <br>
         <div class="row" >
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <label  class="active">Comment </label>
               <input type="text" class="validate" name="comment_uc" id="comment_uc"   placeholder="Enter" @if(isset($comment) && !empty($comment)) value="{{$comment}}" @endif>
               <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="brokarage_comments" rows='20' col='40'></textarea> -->
            </div>
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <label  class="active">Total Flat Available </label>
               <input type="number" class="validate" name="total_flat_available_uc" id="total_flat_available_uc" readonly    placeholder="" @if(isset($unitCount) && !empty($unitCount)) value="{{$unitCount}}" @endif>
               <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="brokarage_comments" rows='20' col='40'></textarea> -->
            </div>
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <select  id="account_manager_uc" name="account_manager_uc" class=" browser-default ">
                  <option value="" disabled selected>Select</option>
                  @foreach($employee1 as $employee)
                  <option value="{{$employee->employee_id}}" @if(isset($employee_id) && !empty($employee_id) )  @if($employee->employee_id == $employee_id ) selected  @endif  @endif >{{ucfirst($employee->employee_name)}}</option>
                  @endforeach
               </select>
               <label class="active">Account Manager</label>
            </div>
            
            <!-- <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
               <ul class="collapsible" style="margin-top: -2px;width: 321px">
                  <li onClick="UnitAmenity()">
                     <div class="collapsible-header"  id="col_unit_uc_amenity" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Unit Amenity </h5></div>
                  </li>
               </ul>
            </div>  -->

         </div>
         <div class="row" id="unit_amenity_uc_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"><br>

                  <br> 
                  
                        <div class="col l2 m4 s12">
                           <a href="javascript:void(0)" onClick="unitAmnityUc()"  class="waves-effect waves-light  modal-trigger" style="color: red !important"> Update Unit Amenity List   </a> 
                        </div>
                        <div class="col l2 m4 s12">
                           <a href="javascript:void(0)" onClick="addUnitAmenityListUc()"  class="waves-effect waves-light  modal-trigger" style="color: red !important"> Add New   </a>                            
                        </div>
                   <br>


                   <div id="appendUnitAmenityUc">
                        @if(isset($unit_amnities) && !empty($unit_amnities))   
                        @foreach($unit_amnities as $unitam)            
                        

                            <div class="col l4 m4 s12">
                                <div class="col l6 m s12 input-field col l3 m4 s12 display_search">{{ucfirst($unitam->amenities)}}</div>
                                
                                <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                                    <label  class="active">Description: </label>
                                        <input type="text" class=" description_ameUc validate" name="description_ameUc[]" id="description_ameUc" value="{{$unitam->description}}"   placeholder="Enter" style="height: 36px;"   >
                                </div>

                                <div class="col l3 m3 s12" style="display:none">
                                    <label><input type="checkbox" name="is_unit_availableUc[]" id="is_unit_availableUc" value="{{$unitam->society_unit_amnities_id}}" checked ><span>Available</span></label> updateAmenityDesc
                                </div>
                
                            
                            </div>  
                            
                        @endforeach
                        @endif

                     </div>
        
         </div>
        </div>

        <div class="row" id="registered" >
           <br>
            <div class="row">
                <div class="input-field col l3 m4 s12 display_search">
                @if(isset($category_id) && !empty($category_id))
                @php $category_id = json_decode($category_id); 
                     $category_id11 = explode(",",$category_id[0]);            
                  @endphp
                @endif
                <select  id="building_category_r" name="building_category" multiple="true" class="browser-default">
                    <!-- <option value="" disabled selected>Select</option> -->
                    @foreach($category as $category1)                               
                    <option value="{{$category1->category_id}}" @if(isset($category_id) && !empty($category_id) )  @if(isset($category_id11))  @if(in_array($category1->category_id,$category_id11)) selected @endif @endif   @endif  >{{ ucfirst ($category1->category_name)}}</option>
                    @endforeach                                 
                </select>
                <label class="active">Building Type</label>
                </div>
                <div class="input-field col l3 m4 s12 display_search">
                        
                @if(isset($building_type_id) && !empty($building_type_id) && $building_type_id != 'null')    
                     @php $building_type_id22 = json_decode($building_type_id); 
                     
                        $building_type_id22 = explode(",",$building_type_id22[0]);     
                        
                     @endphp
                    @endif
                    

                <select  id="building_type_r" name="building_type" multiple="multiple" class="validate select2 browser-default">
                    <!-- <option value="" disabled selected>Select</option> -->
                    @foreach($dd_elevation2 as $dd_elevation2)
                    <option value="{{ $dd_elevation2->dd_elevation_id }}"  @if(isset($building_type_id22))  @if(in_array($dd_elevation2->dd_elevation_id,$building_type_id22)) selected @endif @endif >{{ ucwords($dd_elevation2->elevation_name) }}</option>                                    
                    @endforeach                       
                </select>
                <label class="active">Building Elevation</label>
                </div>
                <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                    <ul class="collapsible" style="margin-top: -2px;width: 321px">
                        <li onClick="ShowHidebuilding_config_details()">
                            <div class="collapsible-header"  id="col_building_config_details" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Building Unit Info</h5> </div>
                        </li>
                    </ul>
                </div>
                <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                <ul class="collapsible" style="margin-top: -2px;width: 321px">
                    <li onClick="ShowHideamenities1_r()">
                        <div class="collapsible-header"  id="col_amenities_r1" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Building Ameneties</h5></div>
                    </li>
                </ul>
                <!-- <div class="collapsible-header"  id="col_amenities_r1" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Building Ameneties</h5></div> -->
                </div>          
              
        
            </div>

                <div class="row"  id="ameneties_r_div1" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)">
                   <br> 
                        <!-- <a href="javascript:void(0)">Click</a> -->
                        <div class="col l2 m4 s12">
                           <a href="javascript:void(0)" onClick="updateAmenityListR()"  class="waves-effect waves-light  modal-trigger" style="color: red !important"> Update Amenity List   </a> 
                        </div>
                        <div class="col l2 m4 s12">
                           <a href="javascript:void(0)" onClick="addAmenityListR()"  class="waves-effect waves-light  modal-trigger" style="color: red !important"> Add New   </a>                            
                        </div>
                   <br>

                   <div id="appendAmenityN">
                    
                   @if(isset($project_amenities) && !empty($project_amenities))   
                        <div style="text-align:center;font-weight: 600;">Project Amenity</div><br>
                   <div class="row">
                    @foreach($project_amenities as $ame)            
                   

                    <div class="col l4 m4 s12">
                           <div class="col l6 m s12 input-field col l3 m4 s12 display_search">{{ucfirst($ame->amenities)}}</div>
                           
                           <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                              <label  class="active">Description: </label>
                                 <input type="text" class="validate" name="description_ame[]" id="description_ame_id" value="{{$ame->description}}"   placeholder="Enter" style="height: 36px;"  readonly >
                           </div>
                          
                     
                     </div>  
                     
                    @endforeach
                    </div><hr> 
                    @endif
                     
                    <div style="text-align:center;font-weight: 600;">Society Amenity</div><br>
                     <div id="appendAmenity">
                     @if(isset($society_amnities) && !empty($society_amnities))   
                     @foreach($society_amnities as $sam)            
                     

                         <div class="col l4 m4 s12">
                              <div class="col l6 m s12 input-field col l3 m4 s12 display_search">{{ucfirst($sam->amenities)}}</div>
                              
                              <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                                 <label  class="active">Description: </label>
                                    <input type="text" class=" description_ameR validate" name="description_ameR[]" id="description_ameR" value="{{$sam->description}}"   placeholder="Enter" style="height: 36px;"   >
                              </div>

                              <div class="col l3 m3 s12" style="display:none">
                                 <label><input type="checkbox" name="is_availableR[]" id="is_availableR" value="{{$sam->society_amnities_id}}" checked ><span>Available</span></label> updateAmenityDesc
                              </div>
               
                        
                        </div>  
                        
                     @endforeach
                     @endif

                     </div>

                    
                  </div>
                    <!-- <div id="appendAmenity"></div> -->

                    @if(isset($project_amenities_society) && !empty($project_amenities_society))
                    @foreach($project_amenities_society as $ame)            
                    <!-- <div class="col l6 m6 s12">
                    <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">{{ucwords($ame->amenities)}}</div>
                    <div class="col l3 m3 s12">
                        <label><input type="checkbox" name="is_available[]"  value="{{$ame->project_amenities_id}}"><span>Available</span></label>
                    </div>
                    <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">
                        <label  class="active">Description2 </label>
                        <input type="text" class="validate" name="description_ame[]" value="{{$ame->description}}"   placeholder="Enter" style="height: 36px;" >
                    </div>
                    </div> -->
                    @endforeach     
                    @endif
                </div>

                <div class="row" id="building_config_details_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)">
                    <br>
                   Building Name : @if(isset($filterData) && !empty($filterData)) @if(!empty($filterData[0]->building_name)) {{ $filterData[0]->building_name }} @endif @endif  
                     <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                            <!-- <th>Building Name</th> -->
                            <th>Wing Name</th>
                            <th>Unit No</th>
                            <th>Configuration</th>
                            <th>Area</th>
                            <th>Configuration Size</th>
                            <th>Balcony</th>
                            <th>Direction</th>
                            <th>Unit View</th>
                            <th>Unit Type</th>
                            @if(isset($building_status) && !empty($building_status) )
                                @if( $building_status == 2 ) 
                                 <th>Show Unit</th>
                                @endif
                            @endif
                            <!-- <th>Unit Status</th> -->
                        </tr>
                    </thead>
                    <tbody>
                       @foreach($filterData as $unitdata)
                        <tr>                            
                            <td>{{$unitdata->wing_name}}</td>
                            <td><?php  
                                 if($unitdata->floor_no <= 0){
                                    $unit_code = abs($unitdata->unit_code)/100;
                                    $unit_code = str_pad($unit_code,3, '0', STR_PAD_LEFT);
                                   }else{
                                       $unit_code = $unitdata->unit_code;
                                   }

                                   echo $unit_code; ?></td>
                            <td>{{$unitdata->configuration_name}}</td>
                            <td>
                               @if( !empty($unitdata->carpet_area) )
                                  Carpet Area :  {{$unitdata->carpet_area}}
                                  <br>
                               @endif
                              
                               @if( !empty($unitdata->build_up_area	) )
                                  Build Up Area :  {{$unitdata->build_up_area	}}
                                  <br>
                               @endif
                              
                              
                               @if( !empty($unitdata->mofa_carpet_area) )
                                 Rera Carpet Area :  {{$unitdata->mofa_carpet_area}}
                               @endif
                               
                              
                              </td>
                            <td>{{ ucwords($unitdata->configuration_size) }}</td>
                            <td>@if($unitdata->balcony == 't') Yes @else No @endif</td>
                            <td>{{ ucwords($unitdata->direction) }}</td>
                            <td>{{$unitdata->unit_view}}</td>
                            <td>{{ ucwords($unitdata->unit_type) }}</td>

                                 @if( $unitdata->building_status == 2 ) 
                                    <td>@if($unitdata->show_unit == 't') Yes @else No @endif </td>
                                 @endif

                            <!-- <td>@if($unitdata->status == 't') Active @else Inactive @endif</td> -->
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                 <br>       
                <div class="row">
                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                    <select  id="construction_technology_r" name="construction_technology" class="browser-default">
                        <option value="" disabled selected>Select</option>
                        @foreach($construction_technology1 as $construction_technology2)
                        <option value="{{$construction_technology2->construction_technology_id}}" @if(isset($construction_technology) && !empty($construction_technology) )  @if( $construction_technology2->construction_technology_id == $construction_technology ) selected  @endif  @endif >{{ ucwords($construction_technology2->construction_technology_name)}}</option>
                        @endforeach                                  
                    </select>
                    <label class="active">Construction technology </label>
                    </div>
                    <div class="input-field col l3 m4 s12 display_search" >
                    @if(isset($parking) && !empty($parking) && $parking != 'null')    
                     @php $parking22 = json_decode($parking); 
                        $parking22 = explode(",",$parking22[0]);            
                     @endphp
                    @endif
                    <select  id="parking_r" name="parking"   multiple="multiple" class=" select2 browser-default">
                        <!-- <option value="" disabled selected>Select</option> -->
                        @foreach($parking2 as $parking2)
                        <option value="{{ $parking2->parking_id  }}"   @if(isset($parking22))  @if(in_array($parking2->parking_id,$parking22)) selected @endif @endif >{{ ucwords($parking2->parking_name) }}</option>
                        @endforeach                              
                    </select>
                    <label class="active">Parking Type</label>
                    </div>
                    <div class="input-field col l3 m4 s12" >
                        <label  class="active">Maintenance Amount on carpet area (sq. ft)  </label>
                        <!-- <input type="text" min="0" class="validate" name="maintenance_amount" id="maintenance_amount_r" value="@if(isset($maintenance_amount)) {{$maintenance_amount}} @endif"   placeholder="Enter" > -->
                        <div class="rupeeSign1" >
                           <input type="text" min="0" class="validate" name="maintenance_amount" id="maintenance_amount_r" value="@if(isset($maintenance_amount)) {{$maintenance_amount}} @endif"     >
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                    <ul class="collapsible" style="margin-top: -2px;width: 321px">
                        <li onClick="ShowHidelandparcel_r()">
                            <div class="collapsible-header"  id="col_landparcel_r"><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Building Land Parcel </h5></div>
                        </li>
                    </ul>
                    </div>
                </div>  
                
                <div class="row" id="landparcel_r_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)"> 
                   <br>
                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                    <div class="row">
                        <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="number" min="0" class="input_select_size" name="building_land_parcel_area" id="building_land_parcel_area_r"  placeholder="Enter" @if(isset($building_land_parcel_area) && !empty($building_land_parcel_area)) value="{{$building_land_parcel_area}}" @endif  >
                            <label>
                                Building Land Parcel
                        </div>
                        <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                        <select  id="building_land_parcel_area_unit_r" name="building_land_parcel_area_unit" class=" browser-default ">
                        @foreach($unit as $un)
                        <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                        @endforeach
                        </select>
                        </div>
                    </div>
                    </div>
                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                    <div class="row">
                    <div class="input-field col m8 s8" style="padding: 0 10px;">
                    <input type="number" min="0" class="input_select_size" name="building_open_space" id="building_open_space_r"  placeholder="Enter" @if(isset($building_open_space) && !empty($building_open_space)) value="{{$building_open_space}}" @endif >
                    <label> Building Open space</label>
                    </div>
                    <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                    <select  id="building_open_space_unit_r" name="building_open_space_unit" class=" browser-default ">
                    @foreach($unit as $un)
                    <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                    @endforeach
                    </select>
                    </div>
                    </div>
                    </div> 
                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                    <div class="row">
                        <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="number" min="0" class="input_select_size" name="building_constructed_area" id="building_constructed_area_r"  placeholder="Enter" @if(isset($building_constructed_area) && !empty($building_constructed_area)) value="{{$building_constructed_area}}" @endif  >
                            <label> Building Constructed Area</label>
                        </div>
                        <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                            <select  id="building_constructed_area_unit_r" name="building_constructed_area_unit" class=" browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    </div>
                </div>

                 <br>
                <div class="row">
                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                        <ul class="collapsible" style="margin-top: -2px;width: 321px">
                            <li onClick="ShowHideage()">
                                <div class="collapsible-header"  id="col_age" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Building Age </h5> </div>
                            </li>
                        </ul>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                        <ul class="collapsible" style="margin-top: -2px;width: 321px">
                            <li onClick="ShowHidedesc_r()">
                                <div class="collapsible-header"  id="col_desc_r" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" > Building Description </h5> </div>
                            </li>
                        </ul>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                    <ul class="collapsible" style="margin-top: -2px;width: 321px">
                        <li onClick="ShowHideaddress_r()">
                            <div class="collapsible-header"  id="col_address_r"> <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Building Address</h5></div>
                        </li>
                    </ul>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                    <ul class="collapsible" style="margin-top: -2px;width: 321px">
                        <li onClick="ShowHideproscons_r()">
                            <div class="collapsible-header"  id="col_proscons_r" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" > Building Pros & Cons </h5></div>
                        </li>
                    </ul>
                    </div>
                </div>   
     

                <div class="row" id="age_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)" >
                  <br>
                  <div class="row">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Physical Possession year  </label>
                        <input type="text" class="datepicker" name="possession_year_r" id="possession_year_r" onchange="calculatePossessionYear()"  placeholder="" @if(isset($possession_year) && !empty($possession_year)) value="{{$possession_year}}" @endif >
                     </div>
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Building Age as per Possession year </label>
                        <input type="text" class="input_select_size" name="building_year_possession_year_r" id="building_year_possession_year_r" readonly   placeholder="" @if(isset($building_year_as_per_possession) && !empty($building_year_as_per_possession)) value="{{$building_year_as_per_possession}}" @endif >
                     </div>
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Occupation certificate dated  </label>
                        <input type="text" class="datepicker" name="occupation_certi_dated_r" id="occupation_certi_dated_r"  onchange="calculateOccupationYear()"  placeholder="" @if(isset($occupation_certi_dated) && !empty($occupation_certi_dated)) value="{{$occupation_certi_dated}}" @endif >
                     </div>
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Building Age  as per OC dated</label>
                        <input type="text" class="input_select_size" name="building_year_oc_dated_r" id="building_year_oc_dated_r" readonly   placeholder="" @if(isset($building_year_as_per_oc) && !empty($building_year_as_per_oc)) value="{{$building_year_as_per_oc}}" @endif >
                     </div>
                  </div>
                </div>
                

                <div class="row" id="desc_r_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)" >
                    <section class="full-editor">
                    <div class="row">
                        <div class="col s12">
                            <div class="card">
                                <br>
                                <div class="card-content">
                                Building  Description
                                <div class="row">
                                    <div class="col s12 l10 m4">
                                        <textarea style="border-color: #5b73e8; padding-top: 12px;width: 1211px; height: 90px;overflow-y: scroll;" placeholder="Enter" name="building_description" id="building_description_r"  > @if(isset($building_description) && !empty($building_description)) {{$building_description}} @endif</textarea>
                                        <!-- <div id="full-wrapper" name="html_editor2">
                                            <div id="full-container" name="html_editor1">
                                                <div class="editor" name="html_editor">
                                                    
                                                    
                                                </div>
                                            </div>
                                            </div> -->
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </section>
                </div>

               <div class="row" id="address_r_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)" >
                  <br>
                  <div class="row">
                  <div class="input-field col l3 m4 s12" >
                  <input type="text" class="validate" name="society_geo_location_lat" id="society_geo_location_lat_r"   placeholder="Enter LAT & LONG" @if(isset($lattitde) && !empty($lattitde)) value="{{$lattitde}}" @endif >
                     <label  class="active">Building Geo Location </label>
                  </div>

                     <!-- <div class="col m3 l3 s12">
                        <div class="row">
                              <div class="input-field col l6 m6 s8" style="padding: 0 10px;">
                                 <label  class="active">Building Geo Location </label>
                                 <input type="text" class="validate" name="society_geo_location_lat" id="society_geo_location_lat_r"   placeholder="Enter LAT" @if(isset($lattitde) && !empty($lattitde)) value="{{$lattitde}}" @endif >
                              </div>
                              <div class="input-field col l6 m6 s4 mobile_view" style="padding: 0 10px;">
                                 <label  class="active">Building Geo Location </label>
                                 <input type="text" class="validate" name="society_geo_location_long" id="society_geo_location_long_r"   placeholder="Enter LONG" @if(isset($longitude) && !empty($longitude)) value="{{$longitude}}" @endif>
                              </div>
                        </div>
                     </div> -->

                     <div class="input-field col l3 m4 s12 display_search">                      
                              <select  id="sub_location_r" name="sub_location" class="validate  browser-default" onChange="getsublocation(this)">
                              <option value="" disabled selected>Select</option>
                              @if(isset($sub_locationR) && !empty($sub_locationR))
                                 @foreach($sub_locationR as $sub_location1)
                                 <option value="{{$sub_location1->sub_location_id}}" @if(isset($sub_location_id))  @if($sub_location1->sub_location_id == $sub_location_id)  selected @endif @endif  >{{ ucwords($sub_location1->sub_location_name)}}</option>
                                 <!--  -->
                                 @endforeach                              
                              @endif   
                              </select>
                              <label class="active">Sub Location</label>
                     </div>


                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="location_r" name="location" class="validate  browser-default"  >
                                <option value="" disabled selected>Select</option>
                                @if(isset($locationR) && !empty($locationR))
                                    @foreach($locationR as $location1)
                                    <option value="{{$location1->location_id}}" @if(isset($location_id))  @if($location1->location_id == $location_id)  selected @endif @endif >{{ ucwords($location1->location_name)}}</option>
                                    @endforeach
                                @endif
                                </select>
                                <label class="active">Location</label>
                        </div>
               
                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="city_r" name="city" class="validate  browser-default" >
                                 @if(isset($cityR) && !empty($cityR))
                                    @foreach($cityR  as $cty)
                                    <option value="{{$cty->city_id}}" @if(isset($city_id)) @if($cty->city_id == $city_id) selected @endif @endif>{{ ucfirst($cty->city_name) }}</option>
                                    @endforeach
                                   @endif                              
                                </select>
                                <label class="active">City</label>
                        </div>
                  </div>      
              
                  <div class="row">
                           <div class="input-field col l3 m4 s12 display_search">                                
                                 <select  id="state_r" name="state" class="validate  browser-default" >
                                 <option value="" disabled selected>Select</option>
                                       @foreach($stateR as $state1)
                                       <option value="{{$state1->dd_state_id}}" @if(isset($state_id)) @if($state1->dd_state_id == $state_id) selected @endif @endif >{{ ucwords($state1->state_name)}}</option>
                                       @endforeach                                  
                                 </select>
                                 <label class="active">State</label>
                           </div>
                  
                        <div class="input-field col l3 m4 s12" >
                        <input type="text" class="input_select_size" name="building_office_address" id="building_office_address_r"  placeholder="Enter" @if(isset($sales_office_addressP) && !empty($sales_office_addressP)) value="{{$sales_office_addressP}}" @endif >
                        <label> Building office address</label>
                        </div>
                  </div>  
               </div>

                <div class="row" id="proscons_r_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)">
                   <br>
                   <div class="row">
                        <input type="hidden" id="op_society_pros_cons_id_r">
                        <div class="input-field col l3 m34 s12" id="InputsWrapper2">
                           <label  class="active">Pros of Building </label>
                           <!-- <textarea style="border-color: #5b73e8; padding-top: 12px;width:104% !important" placeholder="Enter" name="sales_office_address" rows='20' col='40'></textarea> -->
                           <input type="text" class="validate pros_of_building" name="pros_of_building" id="pros_of_building_r"  placeholder="Enter" @if(isset($pros) && !empty($pros)) value="{{$pros}}" @endif >
                        </div>
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                           <label  class="active">Cons of Building </label>
                           <!-- <textarea style="border-color: #5b73e8; padding-top: 12px;width:104% !important" placeholder="Enter" name="sales_office_address" rows='20' col='40'></textarea> -->
                           <input type="text" class="validate cons_of_building" name="cons_of_building" id="cons_of_building_r"   placeholder="Enter"  @if(isset($cons) && !empty($cons)) value="{{$cons}}" @endif>
                        </div>
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                              <!-- <a href="javascript:void(0)" onClick="saveProsCons()" class="btn-small  waves-effect waves-light green darken-1" >Save</a> -->

                              <div class="row" >
                                 <div class="input-field col l2 m2 s6 display_search">
                                 <div class="preloader-wrapper small active" id="loader1" style="display:none">
                                    <div class="spinner-layer spinner-green-only">
                                    <div class="circle-clipper left">
                                       <div class="circle"></div>
                                    </div><div class="gap-patch">
                                       <div class="circle"></div>
                                    </div><div class="circle-clipper right">
                                       <div class="circle"></div>
                                    </div>
                                    </div>
                                 </div>
                              </div></div>

                              <div class="row" id="submitProsR">
                                 <div class="col l4 m4">
                                       <a href="javascript:void(0)" onClick="saveProsCons()" class=" save_proscons btn-small  waves-effect waves-light green darken-1 " id="save_proscons">Save</a>
                                 </div>
                                 <div class="col l3 m3">
                                          <a href="javascript:void(0)" onClick="clear_proscons()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_proscons"><i class="material-icons dp48">clear</i></a>
                                 </div>
                              </div> 
                        </div>
                     </div>
                   <br>
                        <div class="row">
                          <table class="bordered" id="pros_cons_table_r" style="font-size: unset;">
                              <thead>
                              <tr style="color: white;background-color: #ffa500d4;">
                              <th>Sr No. </th>
                              <th>Pros</th>
                              <th>Cons</th>
                              <th>Last Updated Date</th>
                              <th>Action</th>               
                              </tr>
                              </thead>
                              <tbody>
                              @php $incPros111= count($getProjectProsConsData);  @endphp
                                 @if(isset($getProjectProsConsData))
                                 @php $i31=count($getProjectProsConsData); @endphp
                                    @foreach($getProjectProsConsData as $val)
                                       <tr id="pctrr_{{$val->op_society_pros_cons_id}}">
                                          <td >@php $incPros1= $i31--; @endphp {{$incPros1}}</td>
                                          <td id="pr_{{$val->op_society_pros_cons_id}}"> {{$val->pros_of_building}}</td>
                                          <td id="cr_{{$val->op_society_pros_cons_id}}"> {{$val->cons_of_building}}</td>
                                          <td id="ud1_{{$val->op_society_pros_cons_id}}"> {{$val->updated_date}}</td>
                                          <td>
                                            <a href="javascript:void(0)"  class="waves-effect waves-light" onClick="updateProsCons_r('{{$val->op_society_pros_cons_id}}')"  >Edit</a> |
                                             <a href='javascript:void(0)' class="waves-effect waves-light"   onClick="confirmDelProsCons_r1('{{$val->op_society_pros_cons_id}}')" >Delete
                                          </td>
                                       </tr>
                                    @endforeach
                                 @endif                                       
                              </tbody>
                           </table>
                           <input type="hidden" id="incPros1" value="{{$incPros111}}">
                        </div>
                </div>
                   <br>        
                <div class="row">
                    <div class="input-field col l3 m4 s12 display_search">
                    <select   name="building_rating" id="building_rating_r" class="browser-default">
                        <option value="" disabled selected>Select</option>
                        @foreach($rating as $rating)
                        <option value="{{ $rating->rating_id  }}" @if(isset($building_rating) && !empty($building_rating) )  @if( $rating->rating_id == $building_rating ) selected  @endif  @endif>{{ $rating->rating }}</option>                                    
                        @endforeach                                    
                    </select>
                    <label class="active">Building Rating</label>
                    </div>
                    <div class="input-field col l3 m4 s12 display_search">
                    <select   name="indian_green_building" id="indian_green_building_r" class="browser-default">
                        <option value="" disabled selected>Select</option>
                        <option value="yes" @if(isset($indian_green_building) && !empty($indian_green_building)) @if($indian_green_building=='yes')  selected @endif @endif>Yes</option>
                        <option value="no" @if(isset($indian_green_building) && !empty($indian_green_building)) @if($indian_green_building=='no')  selected @endif @endif>No</option>
                    </select>
                    <label class="active">The Indian Green Building Council</label>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Shitfing charges  </label>
                        <div class="rupeeSign1" >
                           <input type="text" class="validate" name="shifting_charges" id="shifting_charges_r"    @if(isset($shifting_charges) && !empty($shifting_charges)) value="{{$shifting_charges}}" @endif >
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <select  id="preferred_tenant_r" name="prefered_tenant" class="browser-default">
                            <option value="" disabled selected>Select</option>
                            @foreach($preferred_tenant as $preferred_tenant)
                            <option value="{{ $preferred_tenant->preferred_tenant_id  }}"  @if($preferred_tenant->preferred_tenant_id == $prefered_tenant ) selected  @endif>{{ $preferred_tenant->preferred_tenant }}</option>
                            @endforeach                               
                        </select>
                        <label class="active">Preferred tenant </label>                 
                    </div>
                </div>
                  <br>      
                <div class="row">
                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                        <ul class="collapsible" style="margin-top: -2px;width: 321px">
                        <li onClick="ShowHidekeymembers()">
                        <div class="collapsible-header"  id="col_keymembers" > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Building Key Members </h5></div>               
                        </li>                                        
                        </ul>
                    </div>   

                    <div class="input-field col l3 m4 s12 display_search">
                        <label  class="active">OC Certificte  </label>
                        <input type="file" name="oc_certificate"  id="oc_certificate_r" class="dropify" multiple="true" data-default-file="" style="padding-top: 14px;">
                        
                        @if(isset($oc_certificate) && !empty($oc_certificate))
                           @php $iv21=1; $oc_certificate = json_decode($oc_certificate); @endphp 
                              @if(is_array($oc_certificate))
                              @foreach($oc_certificate as $csi1)
                              @php $x = pathinfo($csi1, PATHINFO_FILENAME); @endphp
                              <span class="{{$x}}"> <a href="{{ asset('images/'.$csi1) }}" target="_blank">  OC Certificate {{$iv21}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImageRS('{{$csi1}}#oc')">X</a> &nbsp; , </span>
                            
                                    @php $iv21++; @endphp 
                              @endforeach    
                           @endif
                        @endif

                    </div>
                    <div class="input-field col l3 m4 s12 display_search">
                        <label  class="active">Registration Certificate  </label>
                        <input type="file" name="registration_certificate"  id="registration_certificate_r" multiple="true" class="dropify" data-default-file="" style="padding-top: 14px;">
                        
                        @if(isset($registration_certificate) && !empty($registration_certificate))
                           @php $iv=1; $registration_certificate = json_decode($registration_certificate); @endphp 
                           @foreach($registration_certificate as $csi2)
                           @php $x = pathinfo($csi2, PATHINFO_FILENAME); @endphp
                           <span class="{{$x}}">  <a href="{{ asset('images/'.$csi2) }}" target="_blank"> Registration Certificate {{$iv}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImageRS('{{$csi2}}#rc')">X</a> &nbsp; , </span>
                            
                             
                           @php $iv++; @endphp 
                           @endforeach    
                        @endif

                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label  class="active">Conveyance Deed  </label>
                        <input type="file" name="conveyance_deed"  id="conveyance_deed_r" multiple="true" class="dropify" data-default-file="" style="padding-top: 14px;">

                        @if(isset($conveyance_deed) && !empty($conveyance_deed))
                           @php $iv=1; $conveyance_deed = json_decode($conveyance_deed); @endphp 
                           @foreach($conveyance_deed as $csi3)
                           @php $x = pathinfo($csi3, PATHINFO_FILENAME); @endphp
                           <span class="{{$x}}"><a href="{{ asset('images/'.$csi3) }}" target="_blank">  Conveyance Deed {{$iv}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImageRS('{{$csi3}}#cd')">X</a> &nbsp; , </span>
                            
                             
                           @php $iv++; @endphp 
                           @endforeach    
                        @endif
                    </div>   
                </div> 

                <div class="row" id="keymembers_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)">
                         <br> <br> <br>  
                         <div class="row"> </div>
                        <div class="row"> 
                           <input type="hidden" id="society_working_days_time_id">
                           <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                 <select  id="working_days_and_time" name="working_days_and_time" class="browser-default">
                                    <option value="" selected  >Select </option> 
                                    @foreach($days as $day)      
                                     <option value="{{$day->day_id}}" >{{$day->day}}</option>                                    
                                    @endforeach
                                 </select>
                                 <label class="active">Working day </label>                 
                           </div>
                           <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                 <label  class="active">Office Timing From </label>
                                 <input type="time"  class="validate" id="office_timing" name="office_timing" placeholder="">
                           </div>  

                           <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                 <label  class="active">Office Timing To </label>
                                 <input type="time"  class="validate" id="office_timing_to" name="office_timing_to" placeholder="">
                           </div>  
                           
                           <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                           
                                 
                              <div class="row" >
                              <div class="input-field col l2 m2 s6 display_search">
                              <div class="preloader-wrapper small active" id="loaderWD" style="display:none">
                                 <div class="spinner-layer spinner-green-only">
                                 <div class="circle-clipper left">
                                    <div class="circle"></div>
                                 </div><div class="gap-patch">
                                    <div class="circle"></div>
                                 </div><div class="circle-clipper right">
                                    <div class="circle"></div>
                                 </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                                 <!-- <a href="javascript:void(0)" onClick="saveHierarchy()" class="btn-small  waves-effect waves-light green darken-1" >Save</a> -->
                                
                                  

                                  <div class="row" id="submitWD">
                                       <div class="col l4 m4">
                                       <a href="javascript:void(0)" onClick="saveOfficeDays()" class=" save_OfficeDays btn-small  waves-effect waves-light green darken-1 " id="save_OfficeDays">Save</a>                          
                                       </div>
                                       <div class="col l3 m3">
                                          <a href="javascript:void(0)" onClick="clear_OfficeDays()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_OfficeDays" ><i class="material-icons dp48">clear</i></a>
                                       </div>
                                 </div>
                              </div>

                        </div><br>
                        <div class="row">
                        <table class="bordered" id="officetiming_table" style="font-size: unset;" style="font-size: unset;">
                           <thead>
                           <tr style="color: white;background-color: #ffa500d4;">
                              <th>Sr. No</th>
                              <th>Working day </th>                        
                              <th>Office Timing From</th>
                              <th>Office Timing To</th>
                              <th>Last Updated Date</th>                   
                              <th>Action</th>
                           </tr>
                           </thead>
                           <tbody>
                           @php $incOfficeTime1= count($getWorkingDaysTime); @endphp
                              @if(isset($getWorkingDaysTime))
                              @php $ik12=count($getWorkingDaysTime); @endphp
                              @foreach($getWorkingDaysTime as $getWorkingDaysTime)
                                 <tr id="wdid_{{$getWorkingDaysTime->society_working_days_time_id}}">
                                    <td >@php $incOfficeTime= $ik12--; @endphp {{$incOfficeTime}}</td>
                                    <td id="wd_{{$getWorkingDaysTime->society_working_days_time_id}}">{{$getWorkingDaysTime->day}}</td>
                                    <td id="wt_{{$getWorkingDaysTime->society_working_days_time_id}}">{{$getWorkingDaysTime->office_timing}}</td>
                                    <td id="wtt_{{$getWorkingDaysTime->society_working_days_time_id}}">{{$getWorkingDaysTime->office_timing_to}}</td>
                                    <td id="wud_{{$getWorkingDaysTime->society_working_days_time_id}}">{{$getWorkingDaysTime->updated_date}}</td>
                                   
                                    <td><a action="javascript:void(0)" onClick="updateWorkingTime({{$getWorkingDaysTime->society_working_days_time_id}})">Edit</a> | <a action="javascript:void(0)" onClick="confirmDelWorkingTime({{$getWorkingDaysTime->society_working_days_time_id}})">Delete</a></td>     
                              
                                 </tr> 
                              
                                 @endforeach
                              @endif
                           </tbody> 
                        </table> 
                        <input type="hidden" id="incOfficeTime" value="{{$incOfficeTime1}}">

                        </div>

                                
                        <br>      <hr>
                        <div class="row">
                           <input type="hidden" id="society_keymembers_id">
                           <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                              <label for="cus_name active" class="dopr_down_holder_label active">Name 
                              </label>
                              <div  class="sub_val no-search">
                                 <select  id="name_initial" name="name_initial" class=" browser-default ">
                                 @foreach($initials as $ini)
                                       <option value="{{$ini->initials_id}}">{{ucfirst($ini->initials_name)}}</option>
                                 @endforeach
                                 </select>
                              </div>
                              <input type="text" class="validate mobile_number" name="name_h" id="name_h"  placeholder="Enter"  >
                           </div>

                           <div class="input-field col l3 m4 s12" id="InputsWrapper">
                              <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number </label>
                              <div  class="sub_val no-search">
                                 <select  id="country_code1" name="country_code" class=" browser-default">
                                       @foreach($country_code as $cou)
                                       <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                       @endforeach
                                 </select>
                              </div>
                              <input type="text" class="validate mobile_number" name="mobile_number_h" id="mobile_number_h"  placeholder="Enter" >
                           </div>

                           <div class="input-field col l3 m4 s12" id="InputsWrapper">
                              <label for="contactNum1" class="dopr_down_holder_label active">Whatsapp Number </label>
                              <div  class="sub_val no-search">
                                 <select  id="country_code" name="country_code" class=" browser-default">
                                       @foreach($country_code as $cou)
                                       <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                       @endforeach
                                 </select>
                              </div>
                              <input type="text" class="validate mobile_number" name="whatsapp_number_h" id="whatsapp_number_h"  placeholder="Enter" >
                              <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                                <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo_s()"  style="opacity: 1 !important;pointer-events:auto">
                                            </div>
                           </div>
                           <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                                 <label for="primary_email active" class="active">Email Address </label>
                              <input type="email" class="validate" name="primary_email_h" id="primary_email_h"  placeholder="Enter" >
                           </div>
                        </div>
                        <div class="row">                                    

                              <div class="input-field col l3 m4 s12 display_search">                    
                                 <select class="  browser-default designation_h"  data-placeholder="Select" name="designation_h" id="designation_h">
                                    <option value="" selected>Select</option>
                                    @foreach($designation as $des)
                                          <option value="{{$des->designation_id}}">{{ucfirst($des->designation_name)}}</option>
                                    @endforeach
                                 </select>
                                 <label for="possession_year" class="active">Designation </label>
                                    <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                                    <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                                    </div>
                              </div>                       

                              <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                                 <label for="residence_address_h active" class="active">Comment/Residence Address</label>
                                 <input type="text" class="validate" name="residence_address_h" id="residence_address_h"  placeholder="Enter" >
                              </div>
                              <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                 <!-- <a href="javascript:void(0)" onClick="saveHierarchy()" class="btn-small  waves-effect waves-light green darken-1" >Save</a> -->

                                 <div class="row" >
                                    <div class="input-field col l2 m2 s6 display_search">
                                    <div class="preloader-wrapper small active" id="loaderMem" style="display:none">
                                       <div class="spinner-layer spinner-green-only">
                                       <div class="circle-clipper left">
                                          <div class="circle"></div>
                                       </div><div class="gap-patch">
                                          <div class="circle"></div>
                                       </div><div class="circle-clipper right">
                                          <div class="circle"></div>
                                       </div>
                                       </div>
                                    </div>
                                 </div></div>
                                 
                                 
                                 <div class="row" id="submitMem">
         
                                    <div class="input-field col l4 m4 s6 display_search" >
                                       <!-- <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action">Save</button>                         -->
                                       <a href="javascript:void(0)" onClick="saveHierarchy()" class=" save_heirarchy btn-small  waves-effect waves-light green darken-1 " id="save_heirarchy">Save</a>                          

                                    </div>
                                    <div class="input-field col l2 m2 s6 display_search">
                                    <a href="javascript:void(0)" onClick="clear_heirarchy()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_heirarchy" style="margin-top:3px"><i class="material-icons dp48">clear</i></a>

                                    </div>
                                 </div>
                              </div>                       
                        </div>
                        <br>
                        <div class="row">
                        <table class="bordered" id="heirarchy_table" style="font-size: unset;" style="font-size: unset;">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                           <th>Sr. No</th>
                           <th>Name </th>                        
                           <th>Mobile No.</th>
                           <th>Whatsapp No.</th>
                           <th>Email Id</th>
                           <th>Designation</th>
                           <th>Comment/Residence Address</th>    
                           <th>Last Updated Date</th>                   
                           <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $incKeyMem1= count($getKeyMembersData); @endphp
                        @if(isset($getKeyMembersData))
                        @php $ik1=count($getKeyMembersData); @endphp
                        @foreach($getKeyMembersData as $getKeyMembersData)
                           <tr id="kmid_{{$getKeyMembersData->society_keymembers_id}}">
                              <td >@php $incKeyMem= $ik1--; @endphp {{$incKeyMem}}</td>
                              <td id="n_{{$getKeyMembersData->society_keymembers_id}}">{{ucwords($getKeyMembersData->initials_name)}} {{$getKeyMembersData->name}}</td>
                              <td id="m_{{$getKeyMembersData->society_keymembers_id}}">{{$getKeyMembersData->mobile_number}}</td>
                              <td id="w_{{$getKeyMembersData->society_keymembers_id}}">{{$getKeyMembersData->whatsapp_number}}</td>
                              <td id="e_{{$getKeyMembersData->society_keymembers_id}}">{{$getKeyMembersData->email_id}}</td>
                              <td id="d_{{$getKeyMembersData->society_keymembers_id}}">{{$getKeyMembersData->designation_name}}</td>          
                              <td id="r_{{$getKeyMembersData->society_keymembers_id}}">{{$getKeyMembersData->residence_address}}</td>                             
                              <td id="ud3_{{$getKeyMembersData->society_keymembers_id}}">{{$getKeyMembersData->updated_date}}</td>                             
                              <td><a action="javascript:void(0)" onClick="updateHierarchy({{$getKeyMembersData->society_keymembers_id}})">Edit</a> | <a action="javascript:void(0)" onClick="confirmDelHirarchy({{$getKeyMembersData->society_keymembers_id}})">Delete</a></td>     
                        
                           </tr> 
                            @php $i++; @endphp
                            @endforeach
                        @endif
                            </tbody> 
                        </table> 
                        <input type="hidden" id="incKeyMem" value="{{$incKeyMem1}}">

                    </div> 
                     
                                

                          
                     </div>
                </div>
                  <br>
                <div class="row" id='registered1'>
                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                        <ul class="collapsible" style="margin-top: -2px;width: 321px">
                            <li onClick="ShowHideimagesvideo_r()">
                                <div class="collapsible-header"  id="col_imagesvideo_r" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >  Building Brochure, Images, Video And Video Link </h5></div>
                            </li>
                        </ul>
                    </div>        


                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Comment </label>
                        <input type="text" class="validate" name="comment_r" id="comment_r"   placeholder="Enter" @if(isset($comment) && !empty($comment)) value="{{$comment}}" @endif>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="brokarage_comments" rows='20' col='40'></textarea> -->
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <select  id="account_manager_r" name="account_manager" class="browser-default ">
                            <option value="" disabled selected>Select</option>
                            @foreach($employee1 as $employee)
                            <option value="{{$employee->employee_id}}" @if(isset($employee_id) && !empty($employee_id) )  @if($employee->employee_id == $employee_id ) selected  @endif  @endif >{{ucfirst($employee->employee_name)}}</option>
                            @endforeach
                        </select>
                        <label class="active">Account Manager</label>
                        </div>
                        <!-- <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Building Code </label>
                        <input type="text" class="validate" name="building_code" id="building_code_r" readonly  placeholder=""   value="B-{{str_pad($society_id,3, '0', STR_PAD_LEFT)}}" >
                        </div> -->

                </div>

                <div class="row" id="imagesvideo_r_div" style="display:none;padding-bottom: 19px;padding-right: 18px;margin-right: -4px;margin-left: 12px;background-color: rgb(234, 233, 230)">
                     <br>
                     <div class="row">
                           <div class="input-field col l3 m4 s12 display_search">
                              <label  class="active">Building Images  </label>
                              <input type="file" name="building_images" multiple="true" id="building_images_r" class="dropify" data-default-file="" style="padding-top: 14px;">                        
                              @if(isset($getSocietyPhotosData) && !empty($getSocietyPhotosData))
                              @php $ib11=1; @endphp 
                              @foreach($getSocietyPhotosData as $csi)
                              @if($csi->meta_key == 'building_images')
                              <span class="{{$csi->society_photos_id}}"> <a href="{{ asset('images/'.$csi->url) }}" target="_blank">  Building Images {{$ib11}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$csi->society_photos_id}}')">X</a> &nbsp; , </span>
                              @php $ib11++; @endphp 
                              @endif
                            
                              @endforeach    
                              @endif
                        </div>
                        <div class="input-field col l3 m4 s12 display_search">
                              <label  class="active">Building Video  </label>
                              <input type="file" name="building_video" multiple="true" id="building_video_r" class="dropify" data-default-file="" style="padding-top: 14px;">                        
                              @if(isset($getSocietyPhotosData) && !empty($getSocietyPhotosData))
                              @php $iv11=1; @endphp 
                              @foreach($getSocietyPhotosData as $csi)
                              @if($csi->meta_key == 'building_video')
                              
                              <span class="{{$csi->society_photos_id}}"> <a href="{{ asset('images/'.$csi->url) }}" target="_blank">Building Video {{$iv11}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$csi->society_photos_id}}')">X</a> &nbsp; , </span>
                              @php $iv11++; @endphp 
                              @endif
                              
                              @endforeach    
                              @endif
                        </div>
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                           <label  class="active">Building Video Link  </label>
                           <input type="text" class="validate" name="building_video_links" id="building_video_links_r"   placeholder="Enter"  @if(isset($building_video_links) && !empty($building_video_links)) value="{{$building_video_links}}" @endif>
                        </div>
                     </div>
                </div>

        </div>
      

         <div class="row">
            <div class="input-field col l6 m6 s6 display_search">
               <span style="color:red" id="st_error"></span>
               <div class="alert alert-danger print-error-msg" style="display:none;color:red">
                  <ul></ul>
               </div>
            </div>
         </div>
         <div class="row" >
            <div class="input-field col l2 m2 s6 display_search">
               <div class="preloader-wrapper small active" id="loader1" style="display:none">
                  <div class="spinner-layer spinner-green-only">
                     <div class="circle-clipper left">
                        <div class="circle"></div>
                     </div>
                     <div class="gap-patch">
                        <div class="circle"></div>
                     </div>
                     <div class="circle-clipper right">
                        <div class="circle"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row" id="submitButton">
            <div class="row" >
                  <div class="input-field col l2 m2 s6 display_search">
                  <div class="preloader-wrapper small active" id="loaderForm" style="display:none">
                  <div class="spinner-layer spinner-green-only">
                  <div class="circle-clipper left">
                        <div class="circle"></div>
                  </div><div class="gap-patch">
                        <div class="circle"></div>
                  </div><div class="circle-clipper right">
                        <div class="circle"></div>
                  </div>
                  </div>
                  </div>
               </div></div>
               <div class="row"  id="submitForm">                        
                  <div class="input-field col l1 m2 s6 display_search" >
                     <!-- <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action">Save</button>                         -->
                     <a href="javascript:void(0)"  onClick="saveForm()" class="btn-small  waves-effect waves-light green darken-1">@if(isset($society_id) && !empty($society_id)) UPDATE @else Save @endif</a>

                     
                  </div>
                  <div class="input-field col l2 m2 s6 display_search">
                     <a href="/company-master" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
                  </div>
               </div>
         </div>
      </form>
      <br>
   </div>
   <div class="content-overlay"></div>
</div>


<div class="modal" id="deleteProsModal_r1" >
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_pros_r" >
            <input type="hidden" class="op_society_pros_cons_id2_r" id="op_society_pros_cons_id2_r" name="op_society_pros_cons_id">
            <input type="hidden"  id="form_type" name="form_type" value="pros_cons">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deletePros_r()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>

<div class="modal" id="deleteWorkingTimeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_office_timing" >
            <input type="hidden" class="society_working_days_time_id2" id="society_working_days_time_id2" name="society_working_days_time_id">
            <input type="hidden"  id="form_type" name="form_type" value="office_timing">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteWorkingTime()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>

<div class="modal" id="deleteHeirarchyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_hirarchy" >
            <input type="hidden" class="society_keymembers_id2" id="society_keymembers_id2" name="society_keymembers_id">
            <input type="hidden"  id="form_type" name="form_type" value="hirarchy">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteHirarchy()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>

<div class="modal" id="deleteConstructionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_constructionStage" >
            <input type="hidden" class="society_construction_stage_id2" id="society_construction_stage_id2" name="society_construction_stage_id">
            <input type="hidden"  id="form_type" name="form_type" value="constructionStage">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteconstructionStage()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>

<div class="modal" id="deleteProsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_pros" >
            <input type="hidden" class="op_society_pros_cons_id2" id="op_society_pros_cons_id2" name="op_society_pros_cons_id">
            <input type="hidden"  id="form_type" name="form_type" value="pros_cons">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deletePros()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>




<div id="modal93" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Update Pros & Cons</h5>
        <hr>        
        <form method="post" id="pros_update" >
        <input type="hidden" class="op_society_pros_cons_id1" id="op_society_pros_cons_id1" name="op_society_pros_cons_id">
            <div class="row" style="margin-right: 0rem !important">
                 <div class="input-field col l4 m4 s12" id="InputsWrapper2">
                     <label  class="active">Pros </label>
                     <input type="text" class="pros_of_building1 validate" name="pros_of_building" id="pros_of_building1"    placeholder="Enter"  >                             
                  </div>            

                  <div class="input-field col l4 m4 s12" id="InputsWrapper2">
                     <label  class="active">Cons </label>
                     <input type="text" class="cons_of_building1 validate" name="cons_of_building" id="cons_of_building1"    placeholder="Enter"  >                             
                  </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_update_pros" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="errors" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            
            <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                        <a href="javascript:void(0)" onClick="updateProsCons1()" class="btn-small  waves-effect waves-light green darken-1" >Update</a>

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search" style="margin-left: -15px;">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>
<script>

function addAmenityR(){
//   
   var array = Array();
   $("input:checkbox[id=amenityR]:checked").each(function() {
         array.push($(this).val());
   });
   // $('#appendAmenity').append(array);return;
   // alert(array); 
   return array;

}

// function addAmenityListUc()(params) {
//    var array = Array();
//    $("input:checkbox[id=amenityUc]:checked").each(function() {
//          array.push($(this).val());
//    });
//    return array;
// }

function updateAmenityR() {
   // alert(); return;
   // console.log(addAmenity()); return;
    var society_id1 = $('#society_id1').val();

     $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });

      $('#loaderR2').css('display','block','!important');
      $('#submitUnit2').css('display','none'); 
      // return;
      
   if( society_id1 == '' ){
      var project_id = $('#project_id').val();

      // alert(project_id); //return;
      $.ajax({
      url:"/createEmptySociety",
      method:"POST",
      data: {project_id:project_id},
     
      success:function(data1)
      {
         console.log(data1);

         if(data1 == 0){
            alert('Error while creating society');
            return false;
         }else{
            $('#society_id').val(data1);
           // console.log('in else '+data1);
            var society_id11 = data1;

            var form_data = new FormData();   
            form_data.append('project_amenities_id',addAmenityR());    
            form_data.append('society_id', society_id11);
            form_data.append('form_type','reg');             

               $.ajax({
               url:"/updateAmenity",
               method:"POST",
               data: form_data,
               contentType: false,
               cache: false,
               processData: false,
               success:function(data)
               {
                 // console.log(data); //return;
               
                  $('#appendAmenity').html(data);
                  $('#updateAmenityListR').modal('close');

                  $('#loaderR2').css('display','none');
                  $('#submitUnit2').css('display','block');

               }
               });
               
         }
         
      }
      }); 
      
   }else{
      //console.log('in else');
            var society_id11 = society_id1;
            var form_data = new FormData();   
            form_data.append('project_amenities_id',addAmenityR());    
            form_data.append('society_id', society_id11);
            form_data.append('form_type','reg');             

               $.ajax({
               url:"/updateAmenity",
               method:"POST",
               data: form_data,
               contentType: false,
               cache: false,
               processData: false,
               success:function(data)
               {
                  console.log(data); //return;
               
                  $('#appendAmenity').html(data);
                  $('#updateAmenityListR').modal('close');
                  $('#loaderR2').css('display','none');
                  $('#submitUnit2').css('display','block');

               }
               });
   }
   
   // var society_id1 = $('#society_id1').val();
   // alert('out '+society_id11); return;
   

}

function addAmenityUC1(){
   var array = Array();
   $("input:checkbox[id=amenityUC]:checked").each(function() {
         array.push($(this).val());
   });
   return array;
}


function addUnitAmenityUC(){
   var array = Array();
   $("input:checkbox[id=unitamenityUC]:checked").each(function() {
         array.push($(this).val());
   });
   return array;
}

function updateAmenityUc() {
  
   var society_id1 = $('#society_id1').val();
   $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });

      // $('#loaderuc1').css('display','block');
      //  $('#submitUnit').css('display','none');
   
      
   if( society_id1 == '' ){
      var project_id = $('#project_id').val();

      // alert(project_id); //return;
      $.ajax({
      url:"/createEmptySociety",
      method:"POST",
      data: {project_id:project_id},
     
      success:function(data1)
      {
         console.log(data1);

         if(data1 == 0){
            alert('Error while creating society');
            return false;
         }else{
            $('#society_id').val(data1);
            var society_id11 = data1;
            var form_data = new FormData();   
            form_data.append('project_amenities_id',addAmenityUC1());    
            form_data.append('society_id',society_id11);
            form_data.append('form_type','uc');
            
               $.ajax({
               url:"/updateAmenity",
               method:"POST",
               data: form_data,
               contentType: false,
               cache: false,
               processData: false,
               success:function(data)
               {
                  console.log(data); //return;               
                  $('#appendAmenityUc').html(data);
                  $('#updateAmenityListUc').modal('close');
                  $('#loaderuc1').css('display','none');
                  $('#submit').css('display','block'); 

               }
               });  
         }
         
      }
      }); 
      
   }else{
      var society_id11 = society_id1;

      var form_data = new FormData();   
      form_data.append('project_amenities_id',addAmenityUC1());    
      form_data.append('society_id',society_id11);
      form_data.append('form_type','uc');
      
         $.ajax({
         url:"/updateAmenity",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            console.log(data); //return;
         
            $('#appendAmenityUc').html(data);
                  $('#updateAmenityListUc').modal('close');
                  $('#loaderuc1').css('display','none');
                  $('#submit').css('display','block');
         }
         });  

   }
   
   // var society_id11 = $('#society_id1').val();
   
   
}
</script>



<div id="modal9" class="modal">
   <div class="modal-content">
      <h5 style="text-align: center">Add Designation</h5>
      <hr>
      <form method="post" id="add_designation_form">
         <div class="row" style="margin-right: 0rem !important">
            <div class="input-field col l4 m4 s12 display_search">
               <label for="lead_assign" class="active">Designation </label>
               <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
               <span class="add_designation_err"></span>
            </div>
         </div>
         <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
         </div>
   </div>
   <div class="modal-footer" style="text-align: left;">
   <span class="errors" style='color:red'></span>
   <span class="success11" style='color:green;'></span>
   <div class="row">
   <div class="input-field col l3 m3 s6 display_search">
   <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        
   </div>    
   <div class="input-field col l3 m3 s6 display_search">
   <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
   </div>    
   </div> 
   </div>
   </form>
</div>

<div class="modal" id="addAmenityListR" >
    <div class="modal-content" id="addAmenityListRef">
        <h5 style="text-align: center">Add Amenity</h5>
        <hr>        
        <form method="post" id="add_amenity_formR" >
                <div class="modal-body">                    
                <div class="row"  style="margin-right: 0rem !important">
                        <div class="input-field col l4 m4 s12 display_search">                                
                             <label class="active"> Amenity Name </label>
                            <input type="text" class="validate" name="add_new_amenityR" id="add_new_amenityR"   placeholder="Enter" >
                        </div>
                    
                </div>
                
                </div>
    
   </div>
            <div class="modal-footer" style="text-align: left;">
               <span class="errors" style='color:red'></span>
               <span class="success1" style='color:green;'></span>
            
               <div class="row">
                     <div class="input-field col l2 m2 s6 display_search">
                           <a href="javascript:void(0)" onClick="addNewAmenityR()" class="btn-small  waves-effect waves-light green darken-1" >Update</a>
                     </div>    
                     
                     <div class="input-field col l2 m2 s6 display_search" style="margin-left: -15px;">
                           <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                     </div>    
               </div> 
            </div>
        </form>
    
</div>

<div class="modal" id="addAmenityListUc" >
    <div class="modal-content" id="addAmenityListRefUc">
        <h5 style="text-align: center">Add Amenity</h5>
        <hr>        
        <form method="post" id="add_amenity_formUc" >
                <div class="modal-body">                    
                <div class="row"  style="margin-right: 0rem !important">
                        <div class="input-field col l4 m4 s12 display_search">                                
                             <label class="active"> Amenity Name </label>
                            <input type="text" class="validate" name="add_new_amenityUc" id="add_new_amenityUc"   placeholder="Enter" >
                        </div>
                    
                </div>
                
                </div>
    
      </div>
            <div class="modal-footer" style="text-align: left;">
               <span class="errors" style='color:red'></span>
               <span class="success1" style='color:green;'></span>
            
               <div class="row">
                     <div class="input-field col l2 m2 s6 display_search">
                           <a href="javascript:void(0)" onClick="addNewAmenityUc()" class="btn-small  waves-effect waves-light green darken-1" >Update</a>
                     </div>    
                     
                     <div class="input-field col l2 m2 s6 display_search" style="margin-left: -15px;">
                           <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                     </div>    
               </div> 
            </div>
        </form>
    
</div>


<div class="modal" id="updateAmenityListR" >
    <div class="modal-content" id="updateAmenityListRefR">
        <h5 style="text-align: center">Update Amenity</h5>
        <hr>        
        <form method="post" id="delete_pros" >
            <input type="hidden" class="op_society_pros_cons_id2" id="op_society_pros_cons_id2" name="op_society_pros_cons_id">
            <input type="hidden"  id="form_type" name="form_type" value="pros_cons">
            <div class="modal-body">
                
               <div class="row"  style="margin-right: 0rem !important">
                  <input type="hidden" name="society_id1" id="society_id1" value="{{$society_id}}" >
                  <?php $ar = array();
                  if(isset($society_amnities) && !empty($society_amnities)) {
                     
                     foreach($society_amnities as $sep){
                        array_push($ar,$sep->amenities_id);
                     }
                  }
                  //print_r($society_amnities);

                  ?>
                  @if(isset($allamenities) && !empty($allamenities))
                  @foreach($allamenities as $allamenities)   
                  @if(in_array($allamenities->project_amenities_id,$ar))
                        <div class="col l6 m6 s12">
                              <div class="col l4 m4 s12 input-field col l3 m4 s12 display_search">{{ucwords($allamenities->amenities)}}</div>
                              <div class="col l3 m3 s12">
                                    <label><input type="checkbox" id="amenityR" checked onClick="addAmenityR({{$allamenities->project_amenities_id}})" value="{{$allamenities->project_amenities_id}}"><span>Available</span></label>
                              </div>
                        </div> 
                        @else
                        <div class="col l6 m6 s12">
                           <div class="col l4 m4 s12 input-field col l3 m4 s12 display_search">{{ucwords($allamenities->amenities)}}</div>
                              <div class="col l3 m3 s12">
                                    <label><input type="checkbox" id="amenityR" onClick="addAmenityR({{$allamenities->project_amenities_id}})" value="{{$allamenities->project_amenities_id}}"><span>Available</span></label>
                              </div>
                        </div> 
                        @endif
                  
                           
                  @endforeach
                  @endif
                  
               </div>
            
            </div>
    
   </div>
   <div class="modal-footer" style="text-align: left;">
      <span class="errors" style='color:red'></span>
      <span class="success1" style='color:green;'></span>

      <div class="row" >
                        <div class="input-field col l2 m2 s6 display_search">
                        <div class="preloader-wrapper small active" id="loaderR2" style="display:none">
                        <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                           <div class="circle"></div>
                        </div><div class="gap-patch">
                           <div class="circle"></div>
                        </div><div class="circle-clipper right">
                           <div class="circle"></div>
                        </div>
                        </div>
                        </div>
                     </div></div>
   
      <div class="row" id="submitUnit2">
            <div class="input-field col l2 m2 s6 display_search">
                  <a href="javascript:void(0)" onClick="updateAmenityR()" class="btn-small  waves-effect waves-light green darken-1" >Update</a>
            </div>    
            
            <div class="input-field col l2 m2 s6 display_search" style="margin-left: -15px;">
                  <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
            </div>    
      </div> 
   </div>
    
</div>

<div class="modal" id="updateAmenityListUc" >
   <div class="modal-content" id="updateAmenityListRefUc">
        <h5 style="text-align: center">Update Amenity</h5>
        <hr>        
        <form method="post" id="delete_pros" >
            <input type="hidden" class="op_society_pros_cons_id2" id="op_society_pros_cons_id2" name="op_society_pros_cons_id">
            <input type="hidden"  id="form_type" name="form_type" value="pros_cons">
            <div class="modal-body">
                
               <div class="row"  style="margin-right: 0rem !important">
                  <input type="hidden" name="society_id1" id="society_id1" value="{{$society_id}}" >
                  <?php  
                     $ar1= array();
                     if(isset($society_amnities) && !empty($society_amnities)){
                     foreach($society_amnities as $sep1){
                        array_push($ar1,$sep1->amenities_id);
                     }
                  }
                  ?>
                  @if(isset($allamenities1) && !empty($allamenities1))
                  @foreach($allamenities1 as $allamenities2)   
                  @if(in_array($allamenities2->project_amenities_id,$ar1))
                        <div class="col l6 m6 s12">
                              <div class="col l4 m4 s12 input-field col l3 m4 s12 display_search">{{ucwords($allamenities2->amenities)}}</div>
                              <div class="col l3 m3 s12">
                                    <label><input type="checkbox" id="amenityUC" checked onClick="addAmenityUC({{$allamenities2->project_amenities_id}})" value="{{$allamenities2->project_amenities_id}}"><span>Available</span></label>
                              </div>
                        </div> 
                        @else
                        <div class="col l6 m6 s12">
                           <div class="col l4 m4 s12 input-field col l3 m4 s12 display_search">{{ucwords($allamenities2->amenities)}}</div>
                              <div class="col l3 m3 s12">
                                    <label><input type="checkbox" id="amenityUC" onClick="addAmenityUC({{$allamenities2->project_amenities_id}})" value="{{$allamenities2->project_amenities_id}}"><span>Available</span></label>
                              </div>
                        </div> 
                        @endif
                  
                           
                  @endforeach
                  @endif
                  
               </div>
            
   </div>

   <div class="modal-footer" style="text-align: left;">
      <span class="errors" style='color:red'></span>
      <span class="success1" style='color:green;'></span>

      <div class="row"  >
         <div class="input-field col l2 m2 s6 display_search">
         <div class="preloader-wrapper small active" id="loaderuc1" style="display:none">
         <div class="spinner-layer spinner-green-only">
         <div class="circle-clipper left">
            <div class="circle"></div>
         </div><div class="gap-patch">
            <div class="circle"></div>
         </div><div class="circle-clipper right">
            <div class="circle"></div>
         </div>
         </div>
         </div>
      </div></div>
   
      <div class="row"  id="submitUnit">
            <div class="input-field col l2 m2 s6 display_search">
                  <a href="javascript:void(0)" onClick="updateAmenityUc()" class="btn-small  waves-effect waves-light green darken-1" >Update</a>
            </div>    
            
            <div class="input-field col l2 m2 s6 display_search" style="margin-left: 3px;">
                  <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
            </div>    
      </div> 
   </div>
    
</div>
  








<script>
function unitAmnityUc() {
      //  alert()
      $('#unitAmnityUc').modal('open');
}

function ref(argument) {
   $('#project_id').val(argument.value);
   // debugger;
   var project_id = $('#project_id').val();
   // alert(project_id);
   // debugger;
   $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
   // debugger;
    $.ajax({
        url: '/getSublocationId/',
        type:"POST",
        data:{project_id:project_id},        
        success: function(data) {
     //    debugger;
            console.log(data); //return;        

            $('#sub_location_uc').val(data).trigger('change');
            $('#sub_location_r').val(data).trigger('change');
            
        }
    });   


}


function addAmenityListR() {
   // alert()
      $('#addAmenityListR').modal('open');
      // $('#deleteProsModal_r1').modal('open');
}

function addNewAmenityR() {
    var url = 'addProjectAmenity';
    var add_new_amenity  = $('#add_new_amenityR').val();
    // alert(add_new_amenity);
    // var form = 'add_amenity_form';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:{add_new_amenity:add_new_amenity},        
        success: function(data) {
            console.log(data); //return;  
            if(data==1){
               $('#addAmenityListR').modal('close');
               $("#updateAmenityListRefR").load(" #updateAmenityListRefR");
               $('#add_new_amenityR').val('');
            }else{
               alert('Amenity already exist.');
               $('#add_new_amenityR').val('');
            }
            
        }
    });   

}
function updateAmenityListR() {
   $('#updateAmenityListR').modal('open');
// $('#deleteProsModal_r1').modal('open');
}

function addAmenityListUc() {
   // alert()
      $('#addAmenityListUc').modal('open');
      // $('#deleteProsModal_r1').modal('open');
}

function addNewAmenityUc() {
    var url = 'addProjectAmenity';
    var add_new_amenity  = $('#add_new_amenityUc').val();
    // alert(add_new_amenity);
    // var form = 'add_amenity_form';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:{add_new_amenity:add_new_amenity},        
        success: function(data) {
            console.log(data); //return;        
            if(data==1){
               $('#addAmenityListUc').modal('close');
               $("#updateAmenityListRefUc").load(" #updateAmenityListRefUc");
               $('#add_new_amenityUc').val('');
            }else{
               alert('Amenity already exist.');
               $('#add_new_amenityUc').val('');
            }
            
        }
    });   

}

   function updateAmenityListUc() {
      // alert();
      $('#updateAmenityListUc').modal('open');
      // $('#deleteProsModal_r1').modal('open');
   }

 
            function calculatePossessionYear() {
               var possession_year_r = $('#possession_year_r').val();
               var arr1 = possession_year_r.split('/');
               var currentYear = new Date().getFullYear();
               var arr3 = arr1[2].split(',');                
               var final_possessionYear = currentYear - arr3;
               $('#building_year_possession_year_r').val(final_possessionYear);
            }

            function calculateOccupationYear() {
               var possession_year_r = $('#occupation_certi_dated_r').val();
               var arr1 = possession_year_r.split('/');
               var currentYear = new Date().getFullYear();
               var arr3 = arr1[2].split(',');                
               var final_possessionYear = currentYear - arr3;
               $('#building_year_oc_dated_r').val(final_possessionYear);
            }

      </script>
<script src="https://code.jquery.com/jquery-3.4.0.slim.min.js"  crossorigin="anonymous"></script>
      <script src="{{ asset('app-assets/js/jquery.cloner.js') }}"></script>     
  


  <script>

function delImage(params) {
      if (confirm("Are you sure you want to delete this image?")) {
                 
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delImageS",
            method:"POST",
            data: {society_photos_id:params},
           
            success:function(data)
            {
               $('.'+params).remove();
               console.log(data);
            }
         });
      }else{
         return false;
      }
      
}

function delImageRS(params) {
      const myArray = params.split("#");
      var image = myArray[0];
      var type = myArray[1];
      // alert(myArray[1]); return;
      // alert('#'+params);//delImage
      if (confirm("Are you sure you want to delete this image?")) {
         var society_id = $('#society_id').val();
         const file = image;
         const filename = file.split('.').slice(0, -1).join('.');
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delImageRS",
            method:"POST",
            data: {image:image,society_id:society_id,type:type},
            // contentType: false,
            // cache: false,
            // processData: false,
            success:function(data)
            {
               $('.'+filename).remove();
               console.log(data);
            }
         });
      }else{
         return false;
      }
   }

function delImageConst(params) {
   const myArray = params.split("#");
      var image = myArray[0];
      var id = myArray[1];
      
      if (confirm("Are you sure you want to delete this image?")) {
        
         const file = image;
         const filename = file.split('.').slice(0, -1).join('.');
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delImageConst",
            method:"POST",
            data: {id:id,image:image},
           
            success:function(data)
            {
               $('.'+filename).remove();
               console.log(data);
            }
         });
      }else{
         return false;
      }
      
}


        function getCity(argument) {
            var city_id = argument;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(city_id) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {city_id:city_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        // $('#city').empty();
                        // $('#city').focus;
                        // $('#city').append('<option value=""> Select City</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="city"]').append('<option value="'+ value.city_id +'">' + value.city_name.charAt(0).toUpperCase()+value.city_name.slice(1) +'</option>');
                        $('#state_r').val(data[0].dd_state_id).trigger('change'); 
                    // });
                  }else{
                    $('#state_r').empty();
                  }
                  }
                });
            }else{
              $('#state_r').empty();
            }
        }

        function getlocation(argument) {
            var location_id = argument;
           // alert(location_id); return;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(location_id) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {location_id:location_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data);// return;
                      if(data){
                        // $('#location').empty();
                        // $('#location').focus;
                        // $('#location').append('<option value=""> Select location</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="location"]').append('<option value="'+ value.location_id +'">' + value.location_name.charAt(0).toUpperCase()+ value.location_name.slice(1)+ '</option>');
                        $('#city_r').val(data[0].city_id).trigger('change'); 
                        getCity(data[0].city_id);
                    // });
                  }else{
                    $('#city_r').empty();
                  }
                  }
                });
            }else{
              $('#city_r').empty();
            }
        }
        
        function getsublocation(argument) {
            var SublocationID = argument.value;
            // alert(SublocationID);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        // $('#location').empty();
                        // $('#location').focus;
                        // $('#location').append('<option value=""> Select location</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="location"]').append('<option value="'+ value.location_id +'">' + value.location_name.charAt(0).toUpperCase()+ value.location_name.slice(1)+ '</option>');

                        $('#location_r').val(data[0].location_id).trigger('change');  
                        getlocation(data[0].location_id);
                    // });
                  }else{
                    $('#location_r').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }


        function getCityUC(argument) {
            var city_id = argument;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(city_id) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {city_id:city_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        // $('#city').empty();
                        // $('#city').focus;
                        // $('#city').append('<option value=""> Select City</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="city"]').append('<option value="'+ value.city_id +'">' + value.city_name.charAt(0).toUpperCase()+value.city_name.slice(1) +'</option>');
                        $('#state_uc').val(data[0].dd_state_id).trigger('change'); 
                    // });
                  }else{
                    $('#state_uc').empty();
                  }
                  }
                });
            }else{
              $('#state_uc').empty();
            }
        }

        function getlocationUC(argument) {
            var location_id = argument;
           // alert(location_id); return;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(location_id) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {location_id:location_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data);// return;
                      if(data){
                        // $('#location').empty();
                        // $('#location').focus;
                        // $('#location').append('<option value=""> Select location</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="location"]').append('<option value="'+ value.location_id +'">' + value.location_name.charAt(0).toUpperCase()+ value.location_name.slice(1)+ '</option>');
                        $('#city_uc').val(data[0].city_id).trigger('change'); 
                        getCityUC(data[0].city_id);
                    // });
                  }else{
                    $('#city_uc').empty();
                  }
                  }
                });
            }else{
              $('#city_uc').empty();
            }
        }
        
        function getsublocationUC(argument) {
            var SublocationID = argument.value;
            // alert(SublocationID);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        // $('#location').empty();
                        // $('#location').focus;
                        // $('#location').append('<option value=""> Select location</option>'); 
                        // $.each(data, function(key, value){
                        // $('select[name="location"]').append('<option value="'+ value.location_id +'">' + value.location_name.charAt(0).toUpperCase()+ value.location_name.slice(1)+ '</option>');

                        $('#location_uc').val(data[0].location_id).trigger('change');  
                        getlocationUC(data[0].location_id);
                    // });
                  }else{
                    $('#location_uc').empty();
                  }
                  }
                });
            }else{
              $('#location_uc').empty();
            }
        }

  </script>
<script>
   //for Reg only
   function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}


function saveProsCons() {
   var pros_of_building = $('#pros_of_building_r').val();
   var cons_of_building = $('#cons_of_building_r').val();
   var society_id = $('#society_id').val();
   var project_id = $('#project_id').val();
   if(pros_of_building =='' && cons_of_building==''){
      // $('#pros_of_building_r').css('border-color','red');
      return false;
   }  
   $('#loader1').css('display','block');
   $('#submitProsR').css('display','none');
   var form_data = new FormData();
   var ext = name.split('.').pop().toLowerCase();

      form_data.append("pros_of_building", document.getElementById('pros_of_building_r').value);
      form_data.append("cons_of_building", document.getElementById('cons_of_building_r').value);
      form_data.append("form_type", 'pros_cons');

      if( typeof society_id == 'undefined'){
         society_id = '';
      }

      if( typeof project_id == 'undefined'){
         project_id = '';
      }

      form_data.append("society_id", society_id);
      form_data.append("project_id", project_id);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
         url:"/add-society-master",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            console.log(data); //return;
            var incPros1 = parseInt($('#incPros1').val()) +1;
            $('#society_id').val(data.society_id);       
            var html = '<tr id="pctrr_'+data.op_society_pros_cons_id+'">';
            html += '<td>' + incPros1 + '</td>';
            html += '<td id="pr_'+data.op_society_pros_cons_id+'">' + data.pros_of_building + '</td>';// obj['company_pros'] +'</td>';
            html += '<td id="cr_'+data.op_society_pros_cons_id+'">' + data.cons_of_building + '</td>';// obj['company_cons'] +'</td>';         
            html += '<td id="ud1_'+data.op_society_pros_cons_id+'">' + data.updated_date + '</td>';// obj['company_cons'] +'</td>';         
            html += "<td> <a href='javascript:void(0)' onClick='updateProsCons_r("+data.op_society_pros_cons_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelProsCons_r1("+data.op_society_pros_cons_id+")'>Delete</a></td>";   
            html += '</tr>';
            $('#pros_cons_table_r').prepend(html);
            $('#pros_of_building_r').val('');
            $('#cons_of_building_r').val('');
            $('#incPros1').val(incPros1);
            $('#loader1').css('display','none');
            $('#submitProsR').css('display','block');
         }
      });
}

function clear_proscons() {
   $('#pros_of_building_r').val('');
   $('#cons_of_building_r').val('');
   $('#op_society_pros_cons_id_r').val(''); 
   var anchor=document.getElementById("save_proscons");
   anchor.innerHTML="Save";
   $("#save_proscons").attr("onclick","saveProsCons()");
}

function updateProsCons_r(param) {
    var op_society_pros_cons_id = param;
    var form_data = new FormData();
      form_data.append("form_type", 'pros_cons');
      form_data.append("op_society_pros_cons_id", op_society_pros_cons_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
    $.ajax({
         url:"/getDataSociety",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            var data  = data[0];
            console.log(data); //return;
            $('#op_society_pros_cons_id_r').val(data.op_society_pros_cons_id);
            $('#pros_of_building_r').val(data.pros_of_building);
            $('#cons_of_building_r').val(data.cons_of_building);

            var anchor=document.getElementById("save_proscons");
               anchor.innerHTML="Update";
               $("#save_proscons").attr("onclick","updateProsCons1_r()");
            
            // $('#modal93').modal('open');
      }
      });
    
}

function updateProsCons1_r() {
    var pros_of_building = $('#pros_of_building_r').val();
    var cons_of_building = $('#cons_of_building_r').val();
    var op_society_pros_cons_id = $('#op_society_pros_cons_id_r').val();
    $('#loader1').css('display','block');
   $('#submitProsR').css('display','none');

    var form_data = new FormData();
     form_data.append("pros_of_building", pros_of_building);
     form_data.append("cons_of_building", cons_of_building);
     form_data.append("op_society_pros_cons_id", op_society_pros_cons_id);
     form_data.append("form_type", 'pros_cons');
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
     });

     $.ajax({
        url:"/updateSocietyFormRecords",
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success:function(data)
        {      
            console.log(data);        //   return; 
            var pcid = data.op_society_pros_cons_id;           
            $('#pctrr_'+pcid).html(data.project_pro_cons_id);
            $('#pr_'+pcid).html(data.data['pros_of_building']);
            $('#cr_'+pcid).html(data.data['cons_of_building']);      
            $('#ud1_'+pcid).html(data.data['updated_date']);  
            // $('#modal93').modal('close'); 
            $('#loader1').css('display','none');
            $('#submitProsR').css('display','block');       
            
        }
    });
}

function confirmDelProsCons_r1(params) {
    var project_pro_cons_id = params;
   //  alert(params)
   $('#deleteProsModal_r1').modal('open');
    $('.op_society_pros_cons_id2_r').val(project_pro_cons_id);   
   //  
}


function deletePros_r() {
   var url = 'deleteSocietyFormRecords';
   var form = 'delete_pros_r';
   $.ajaxSetup({
   headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
   });
   $.ajax({
      url:"/"+url,
      type:"POST",
      data:                     
      $('#'+form).serialize() ,      
      success: function(data) {
         console.log(data); //return;
      // $("#award_table").load(window.location + " #award_table");    
      var pctr = data.op_society_pros_cons_id;
      $('#pctrr_'+pctr).remove();
      // $('#deleteAwardModal').hide();
      $('#deleteProsModal_r1').modal('close');
      // console.log(data);return;
         
      }
   });   

}


//for UC only
function saveProsCons1() {
   // alert()
   var pros_of_building = $('#pros_of_building').val();
   var cons_of_building = $('#cons_of_building').val();
   var society_id = $('#society_id').val();
   var project_id = $('#project_id').val();
   if( pros_of_building == '' && cons_of_building == '' ){      
      return false;
   }  
   $('#loader2').css('display','block');
   $('#submitProsUC').css('display','none');
   var form_data = new FormData();
   var ext = name.split('.').pop().toLowerCase();

      form_data.append("pros_of_building", document.getElementById('pros_of_building').value);
      form_data.append("cons_of_building", document.getElementById('cons_of_building').value);
      form_data.append("form_type", 'pros_cons');

      if( typeof society_id == 'undefined'){
         society_id = '';
      }

      if( typeof project_id == 'undefined'){
         project_id = '';
      }

      form_data.append("society_id", society_id);
      form_data.append("project_id", project_id);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
         url:"/add-society-master",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            console.log(data); //sreturn;
            $('#society_id').val(data.society_id);    
            var incPros = parseInt($('#incPros').val()) +1;   
            var html = '<tr id="pctruc_'+data.op_society_pros_cons_id+'">';
            html += '<td >' + incPros + '</td>';// obj['company_pro_cons_id'] +'</td>';
            html += '<td id="puc_'+data.op_society_pros_cons_id+'">' + data.pros_of_building + '</td>';// obj['company_pros'] +'</td>';
            html += '<td id="cuc_'+data.op_society_pros_cons_id+'">' + data.cons_of_building + '</td>';// obj['company_cons'] +'</td>';         
            html += '<td id="ud2uc_'+data.op_society_pros_cons_id+'">' + data.updated_date + '</td>';// obj['company_cons'] +'</td>';         
            html += "<td> <a href='javascript:void(0)' onClick='updateProsCons("+data.op_society_pros_cons_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelProsCons("+data.op_society_pros_cons_id+")'>Delete</a></td>";   
            html += '</tr>';
            $('#pros_cons_table').prepend(html);
            $('#pros_of_building').val('');
            $('#cons_of_building').val('');
            $('#incPros').val(incPros);
            $('#loader2').css('display','none');
            $('#submitProsUC').css('display','block');
         }
      });

}
function clear_proscons1() {
   $('#pros_of_building').val('');
   $('#cons_of_building').val('');
   $('#op_society_pros_cons_id').val(''); 
   var anchor=document.getElementById("save_proscons1");
   anchor.innerHTML="Save";
   $("#save_proscons1").attr("onclick","saveProsCons1()");
}

function updateProsCons(param) {
    var op_society_pros_cons_id = param;
    var form_data = new FormData();
      form_data.append("form_type", 'pros_cons');
      form_data.append("op_society_pros_cons_id", op_society_pros_cons_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
    $.ajax({
         url:"/getDataSociety",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            var data  = data[0];
            console.log(data); //return;
            $('#op_society_pros_cons_id').val(data.op_society_pros_cons_id);
            $('#pros_of_building').val(data.pros_of_building);
            $('#cons_of_building').val(data.cons_of_building);

            var anchor=document.getElementById("save_proscons1");
               anchor.innerHTML="Update";
               $("#save_proscons1").attr("onclick","updateProsCons1()");
            
            // $('#modal93').modal('open');
      }
      });
    
}

function updateProsCons1() {
    var pros_of_building = $('#pros_of_building').val();
    var cons_of_building = $('#cons_of_building').val();
    var op_society_pros_cons_id = $('#op_society_pros_cons_id').val();
    $('#loader2').css('display','block');
    $('#submitProsUC').css('display','none');

    var form_data = new FormData();
     form_data.append("pros_of_building", pros_of_building);
     form_data.append("cons_of_building", cons_of_building);
     form_data.append("op_society_pros_cons_id", op_society_pros_cons_id);
     form_data.append("form_type", 'pros_cons');
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
     });

     $.ajax({
        url:"/updateSocietyFormRecords",
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success:function(data)
        {      
            console.log(data);         
            var pcid = data.op_society_pros_cons_id;           
            $('#pctruc_'+pcid).html(data.project_pro_cons_id);
            $('#puc_'+pcid).html(data.data['pros_of_building']);
            $('#cuc_'+pcid).html(data.data['cons_of_building']);      
            $('#ud2uc_'+pcid).html(data.data['updated_date']);  
            // $('#modal93').modal('close');        
            $('#loader2').css('display','none');
            $('#submitProsUC').css('display','block');
            
        }
    });
}

function confirmDelProsCons(params) {
    var project_pro_cons_id = params;
    $('.op_society_pros_cons_id2').val(project_pro_cons_id);   
    $('#deleteProsModal').modal('open');
}

function deletePros() {
   var url = 'deleteSocietyFormRecords';
   var form = 'delete_pros';
   $.ajaxSetup({
   headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
   });
   $.ajax({
      url:"/"+url,
      type:"POST",
      data:                     
      $('#'+form).serialize() ,      
      success: function(data) {
         console.log(data); //return;
      // $("#award_table").load(window.location + " #award_table");    
      var pctr = data.op_society_pros_cons_id;
      $('#pctruc_'+pctr).remove();
      // $('#deleteAwardModal').hide();
      $('#deleteProsModal').modal('close');
      // console.log(data);return;
         
      }
   });   

}

function saveOfficeDays() {
   
   var working_days_and_time = $('#working_days_and_time').val();
   var office_timing = $('#office_timing').val();
   var office_timing_to = $('#office_timing_to').val();
   var society_id = $('#society_id').val();
   var project_id = $('#project_id').val();

   // alert(society_id)
   if( working_days_and_time == ''){
      alert("Select working day.")
      // $('#working_days_and_time').css('border-color','red');
      return false;
    }
    $('#loaderWD').css('display','block');
   $('#submitWD').css('display','none');

    var form_data = new FormData();     
    form_data.append('working_day',working_days_and_time);
    form_data.append('office_timing',office_timing);
    form_data.append('office_timing_to',office_timing_to);
    form_data.append('society_id',society_id);
    form_data.append('project_id',project_id);
    form_data.append("form_type", 'office_timing');

    $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });

      $.ajax({
      url:"/add-society-master",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {   
         console.log(data); //return;

         var incOfficeTime = parseInt($('#incOfficeTime').val()) +1;
         var html = '<tr id="wdid_'+data.society_working_days_time_id+'">';
         html += '<td>' + incOfficeTime + '</td>';
         html += '<td id="wd_'+data.society_working_days_time_id+'">' + data.day +'</td>'; // obj['name'] +'</td>';
         html += '<td id="wt_'+data.society_working_days_time_id+'">' + data.office_timing +'</td>'; // obj['mobile_number'] +'</td>';
         html += '<td id="wtt_'+data.society_working_days_time_id+'">' + data.office_timing_to +'</td>'; // obj['mobile_number'] +'</td>';
         html += '<td id="wud_'+data.society_working_days_time_id+'">' + data.updated_date +'</td>'; // obj['whatsapp_number'] +'</td>';
         html += "<td> <a href='javascript:void(0)' onClick='updateWorkingTime("+data.society_working_days_time_id+")' >Edit</a> |<a href='javascript:void(0)' onClick='confirmDelWorkingTime("+data.society_working_days_time_id+")' >Delete</a></td>";
         html += '</tr>';
         // console.log(html);
         $('#officetiming_table').prepend(html);
         $('#loaderWD').css('display','none');
         $('#submitWD').css('display','block');
         clear_OfficeDays();
      }
      });  
    
}

function clear_OfficeDays() {
   $('#society_working_days_time_id').val('');
   $('#working_days_and_time').val('').trigger('change');  
   $('#office_timing').val('');   
   $('#office_timing_to').val('');    
   var anchor=document.getElementById("save_OfficeDays");
   anchor.innerHTML="Save";
   $("#save_OfficeDays").attr("onclick","saveOfficeDays()");
}

function updateWorkingTime(param) {
    var society_working_days_time_id = param;
    var form_data = new FormData();
      form_data.append("form_type", 'office_timing');
      form_data.append("society_working_days_time_id", society_working_days_time_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
    $.ajax({
         url:"/getDataSociety",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            
            var data  = data[0];
            console.log(data); //return;
            // $('#modal94').modal('open');
           
            console.log(data);// return;
            $('#society_working_days_time_id').val(data.society_working_days_time_id);
            // $('#working_days_and_time').val(data.name);
            $('#office_timing').val(data.office_timing);
            $('#office_timing_to').val(data.office_timing_to);

            $('#working_days_and_time').val(data.day_id).trigger('change');        
           

            var anchor=document.getElementById("save_OfficeDays");
               anchor.innerHTML="Update";
               $("#save_OfficeDays").attr("onclick","updateWorkingTime1()");
      }
      });
    
}

function updateWorkingTime1() {
   // alert();
    // var user_typei = $('#user_typei1').val();
    var society_working_days_time_id = $('#society_working_days_time_id').val();
    var office_timing = $('#office_timing').val();
    var office_timing_to = $('#office_timing_to').val();
    var working_days_and_time = $('#working_days_and_time').val();    
    var project_id = $('#project_id').val();
    var society_id = $('#society_id').val();

    if( working_days_and_time == ''){
      alert("Select working day.")
      // $('#working_days_and_time').css('border-color','red');
      return false;
    }
    $('#loaderWD').css('display','block');
   $('#submitWD').css('display','none');

    var form_data = new FormData();   
    form_data.append('society_working_days_time_id',society_working_days_time_id);
    form_data.append('working_days_and_time',working_days_and_time);
    form_data.append('office_timing',office_timing);  
    form_data.append('office_timing_to',office_timing_to); 
    form_data.append('project_id',project_id);
    form_data.append('society_id',society_id);
  
    form_data.append("form_type", 'office_timing');
    $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/updateSocietyFormRecords",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data); //return;
       
        $('#wd_'+data.society_working_days_time_id).html(data.data['day']);
        $('#wt_'+data.society_working_days_time_id).html(data.data['office_timing']);
        $('#wtt_'+data.society_working_days_time_id).html(data.data['office_timing_to']);
        $('#wud_'+data.society_working_days_time_id).html(data.data['updated_date']);       
      //   $('#modal94').modal('close'); 

      // $('#office_timing').val('');
      // $('#office_timing_to').val('');
      // $('#working_days_and_time').val('');    

      
  

      $('#loaderWD').css('display','none');
      $('#submitWD').css('display','block');

      }
      });   
}

function confirmDelWorkingTime(params) {
    var society_working_days_time_id = params;
    $('.society_working_days_time_id2').val(society_working_days_time_id);   
    $('#deleteWorkingTimeModal').modal('open');
}

function deleteWorkingTime() {
    var url = 'deleteSocietyFormRecords';
    var form = 'delete_office_timing';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
        // $("#award_table").load(window.location + " #award_table");    
        var kmid = data.society_working_days_time_id;
        $('#wdid_'+kmid).remove();
      //   $('#deleteWorkingTimeModal').hide();
      $('#deleteWorkingTimeModal').modal('close');
      //   $('#deleteHeirarchyModal').modal('close');
        // console.log(data);return;
            
        }
    }); 
}



function saveHierarchy() {
   var name_initial  = $('#name_initial').val();
    var name_h = $('#name_h').val();
    var country_code  = $('#country_code1').val(); 
    var mobile_number_h = $('#mobile_number_h').val();
    var whatsapp_number_h = $('#whatsapp_number_h').val();
    var primary_email_h = $('#primary_email_h').val();
    var designation_h = $('#designation_h').val();
    var residence_address_h = $('#residence_address_h').val();
    var project_id = $('#project_id').val();
    var society_id = $('#society_id').val();

    // alert(project_id);
    if( name_h == ''){
      $('#name_h').css('border-color','red');
      return false;
    }else{
      $('#name_h').css('border-color','');
    }

    if(IsEmail(primary_email_h)==false){
         $('#primary_email_h').css('border-color','red');
          return false;
   }else{
      $('#primary_email_h').css('border-color','');
   }

   $('#loaderMem').css('display','block');
   $('#submitMem').css('display','none');

    var form_data = new FormData();  
    form_data.append("name_initial", name_initial);    
    form_data.append('name_h',name_h);
    form_data.append("country_code", country_code); 
    form_data.append('mobile_number_h',mobile_number_h);
    form_data.append('whatsapp_number_h',whatsapp_number_h);
    form_data.append('primary_email_h',primary_email_h);
    form_data.append('designation_h',designation_h);
    form_data.append('residence_address_h',residence_address_h);
    form_data.append('project_id',project_id);
    form_data.append('society_id',society_id);
  
    form_data.append("form_type", 'hierarchy');
    $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/add-society-master",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data); //return;
         var incKeyMem = parseInt($('#incKeyMem').val()) +1;
         var ini = data.initials_name;
        var html = '<tr id="kmid_'+data.society_keymembers_id+'">';
        html += '<td>' + incKeyMem + '</td>';
        html += '<td id="n_'+data.society_keymembers_id+'">' + ini.charAt(0).toUpperCase() + ini.slice(1) +" "+ data.name +'</td>'; // obj['name'] +'</td>';
        html += '<td id="m_'+data.society_keymembers_id+'">' + data.mobile_number +'</td>'; // obj['mobile_number'] +'</td>';
        html += '<td id="w_'+data.society_keymembers_id+'">' + data.whatsapp_number +'</td>'; // obj['whatsapp_number'] +'</td>';
        html += '<td id="e_'+data.society_keymembers_id+'">' + data.email_id +'</td>'; // obj['email_id'] +'</td>';
        html += '<td id="d_'+data.society_keymembers_id+'">' + data.designation_name +'</td>'; // obj['designation_id'] +'</td>';
        html += '<td id="r_'+data.society_keymembers_id+'">' + data.residence_address +'</td>'; // obj['reports_to'] +'</td>';
        html += '<td id="ud3_'+data.society_keymembers_id+'">' + data.updated_date +'</td>'; // obj['reports_to'] +'</td>';
        html += "<td> <a href='javascript:void(0)' onClick='updateHierarchy("+data.society_keymembers_id+")' >Edit</a> |<a href='javascript:void(0)' onClick='confirmDelHirarchy("+data.society_keymembers_id+")' >Delete</a></td>";
        html += '</tr>';
        // console.log(html);
        $('#heirarchy_table').prepend(html);
        $('#name_h').val('');
        $('#mobile_number_h').val('');
        $('#whatsapp_number_h').val('');
        $('#primary_email_h').val('');
        $('#designation_h').val('').trigger('change'); 
        $('#residence_address_h').val('');
        $("#copywhatsapp").prop("checked", false);
        $('#incKeyMem').val(incKeyMem);
        $('#loaderMem').css('display','none');
        $('#submitMem').css('display','block');
     }
      });   


}

function clear_heirarchy() {
   $('#society_keymembers_id').val('');   
   $('#name_initial').val(1).trigger('change');       
        $('#name_h').val('');
        $('#country_code1').val(1).trigger('change');       
        $('#mobile_number_h').val('');
        $('#whatsapp_number_h').val('');
        $('#primary_email_h').val('');           
        $('#designation_h').val('').trigger('change');     
        $('#residence_address_h').val('');
        $("#copywhatsapp").prop("checked", false);      
     
      var anchor=document.getElementById("save_heirarchy");
      anchor.innerHTML="Save";
      $("#save_heirarchy").attr("onclick","saveHierarchy()");
}

function updateHierarchy(param) {
    var society_keymembers_id = param;
    var form_data = new FormData();
      form_data.append("form_type", 'hierarchy');
      form_data.append("society_keymembers_id", society_keymembers_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
    $.ajax({
         url:"/getDataSociety",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            // console.log(data); return;
            var data  = data[0];
            
            // $('#modal94').modal('open');
           
            console.log(data);// return;
            $('#society_keymembers_id').val(data.society_keymembers_id);
            $('#name_initial').val(data.name_initial).trigger('change');      
            $('#name_h').val(data.name);
            $('#country_code1').val(data.country_code_id).trigger('change');            
            $('#mobile_number_h').val(data.mobile_number);
            $('#whatsapp_number_h').val(data.whatsapp_number);
            $('#primary_email_h').val(data.email_id);
            $('#designation_h').val(data.designation_id).trigger('change');
            // $('#designation_h1').val(data.designation_id);
          //  $('#reporting_to_h1').val(data.reports_to).trigger('change');
            // $('#reporting_to_h1').val(data.reporting_to);
            $('#residence_address_h').val(data.residence_address);
            // $('#modal94').modal('open');

            var anchor=document.getElementById("save_heirarchy");
               anchor.innerHTML="Update";
               $("#save_heirarchy").attr("onclick","updateHierarchy1()");
      }
      });
    
}

function updateHierarchy1() {
   // alert();
    // var user_typei = $('#user_typei1').val();
    var society_keymembers_id = $('#society_keymembers_id').val();
    var name_initial  = $('#name_initial').val();
    var name_h = $('#name_h').val();
    var country_code  = $('#country_code1').val();
    var mobile_number_h = $('#mobile_number_h').val();
    var whatsapp_number = $('#whatsapp_number_h').val();
    var primary_email_h = $('#primary_email_h').val();
    var designation_h = $('#designation_h').val();
    var residence_address_h = $('#residence_address_h').val();
    
    var project_id = $('#project_id').val();
    var society_id = $('#society_id').val();

    if( name_h == ''){
      $('#name_h1').css('border-color','red');
      return false;
    }
    $('#loaderMem').css('display','block');
   $('#submitMem').css('display','none');
    var form_data = new FormData();   
    form_data.append('society_keymembers_id',society_keymembers_id);
    form_data.append('name_initial',name_initial);
    form_data.append('name_h',name_h);
    form_data.append('country_code',country_code);
    form_data.append('mobile_number_h',mobile_number_h);
    form_data.append('whatsapp_number',whatsapp_number);
    form_data.append('primary_email_h',primary_email_h);
    form_data.append('designation_h',designation_h);
    form_data.append('residence_address_h',residence_address_h);   
    form_data.append('project_id',project_id);
    form_data.append('society_id',society_id);
  
    form_data.append("form_type", 'hierarchy');
    $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/updateSocietyFormRecords",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data); //return;
         var ini = data.data['initials_name'];
        $('#n_'+data.society_keymembers_id).html(ini.charAt(0).toUpperCase() + ini.slice(1) +" "+data.data['name']);
        $('#m_'+data.society_keymembers_id).html(data.data['mobile_number']);
        $('#w_'+data.society_keymembers_id).html(data.data['whatsapp_number']);
        $('#e_'+data.society_keymembers_id).html(data.data['email_id']);
        $('#d_'+data.society_keymembers_id).html(data.data['designation_name']);
        $('#r_'+data.society_keymembers_id).html(data.data['residence_address']);
        $('#ud3_'+data.society_keymembers_id).html(data.data['updated_date']);
      //   $('#modal94').modal('close'); 
      $('#loaderMem').css('display','none');
      $('#submitMem').css('display','block');

      }
      });   
}

function confirmDelHirarchy(params) {
    var society_keymembers_id = params;
    $('.society_keymembers_id2').val(society_keymembers_id);   
    $('#deleteHeirarchyModal').modal('open');
}

function deleteHirarchy() {
    var url = 'deleteSocietyFormRecords';
    var form = 'delete_hirarchy';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
        // $("#award_table").load(window.location + " #award_table");    
        var kmid = data.society_keymembers_id;
        $('#kmid_'+kmid).remove();
        // $('#deleteAwardModal').hide();
        $('#deleteHeirarchyModal').modal('close');
        // console.log(data);return;
            
        }
    }); 
}

function saveConstructionUpdate() {
   var building_construction_stage_u = $('#building_construction_stage_u').val();
   var project_id = $('#project_id').val();
    var society_id = $('#society_id').val();
   if(building_construction_stage_u==''){
      $('#building_construction_stage_u').css('border-color','red');
      return false;
   }
   //    alert(pros_of_complex);

   // $(document).on('change', '#input-file-now', function(){
   //   var name = document.getElementById("input-file-now").files[0].name;
   var form_data = new FormData();
   var ext = name.split('.').pop().toLowerCase();

      form_data.append("building_construction_stage[]", building_construction_stage_u);
      form_data.append("building_construction_stage_date", document.getElementById('building_construction_stage_date_u').value);

      var totalfiles1 = document.getElementById('construction_stage_images_u').files.length;
      for (var index1 = 0; index1 < totalfiles1; index1++) {
         form_data.append("construction_stage_images[]", document.getElementById('construction_stage_images_u').files[index1]);
      }
      form_data.append("comments_bc", document.getElementById('comments_bc_u').value);
      
      form_data.append("form_type", 'construction_stage');
      form_data.append("society_id", society_id);
      form_data.append("project_id", project_id);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/add-society-master",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data); //return;

         var incPros1 = parseInt($('#incConstruction1').val()) +1;
         var html = '<tr id="bctr_'+data.society_construction_stage_id+'">';
         html += '<td id="bcid_'+data.society_construction_stage_id+'">' +incPros1 + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="cs_'+data.society_construction_stage_id+'">' +data.building_construction_stage       
         
         + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="cd_'+data.society_construction_stage_id+'">' + data.building_construction_stage_date + '</td>';// obj['company_pros'] +'</td>';

         
             
         const img = data.construction_stage_images1;                   
         var result = "";
         // 
         img.forEach((item, index)=>{
            result = result + " <a  target='_blank' href='images/" + item + "'>Construction Image "+ [index] + "</a>, ";
         })
   
         html += '<td id="i_'+data.society_construction_stage_id+'">' + result + '</td>'; 
         html += '<td id="c_'+data.society_construction_stage_id+'">' + data.comments_bc + '</td>';
         html += '<td id="ud_'+data.society_construction_stage_id+'">' + data.created_date + '</td>';
         html += "<td> <a href='javascript:void(0)' onClick='updateConstructionStage("+data.society_construction_stage_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelConstructionStage("+data.society_construction_stage_id+")'>Delete</a></td>";   
         html += '</tr>';
         console.log( html);
         $('#building_construction_table').prepend(html);
         $('#building_construction_stage_u').val('').trigger('change');
         $('#building_construction_stage_date_u').val('');
         $('#construction_stage_images_u').val('');
         $('#comments_bc_u').val('');
      }
      });

}

function clear_construction() {
   $('#building_construction_stage_u').val('').trigger('change');
   $('#construction_stage_images_u').val('');
   $('#society_construction_stage_id').val(''); 
   $('#building_construction_stage_date_u').val('');
   $('#comments_bc_u').val(''); 
   var anchor=document.getElementById("save_construction");
   anchor.innerHTML="Save";
   $("#save_construction").attr("onclick","saveConstructionUpdate()");
}

function updateConstructionStage(param) {
   var society_construction_stage_id = param;
    var form_data = new FormData();
      form_data.append("form_type", 'construction_stage');
      form_data.append("society_construction_stage_id", society_construction_stage_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
    $.ajax({
         url:"/getDataSociety",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {  
            console.log(data);
            var data  = data[0];
            console.log(data.building_construction_stage ); //return;

            var constStg = JSON.parse(data.building_construction_stage );
            console.log(constStg[0] ); //return;
            // console.log(constStg[0] ); //return;

            let text = constStg[0];
            const myArray = text.split(",");
            console.log(myArray);
              


            $('#society_construction_stage_id').val(data.society_construction_stage_id);
            $('#building_construction_stage_u').val(myArray).trigger('change');

            //$('#building_construction_stage_u1').val(data.building_construction_stage);
            // $('#building_construction_stage_u').val(data.building_construction_stage).trigger('change');
            $('#building_construction_stage_date_u').val(data.building_construction_stage_date);
            $('#comments_bc_u').val(data.comments_bc);
            // $('#modal95').modal('open');
            var anchor=document.getElementById("save_construction");
               anchor.innerHTML="Update";
               $("#save_construction").attr("onclick","updateConstructionStage1()");

            
      }
      });
   
}

function updateConstructionStage1() {
   
   var form_data = new FormData();
   form_data.append("society_construction_stage_id", document.getElementById('society_construction_stage_id').value);
   form_data.append("building_construction_stage", document.getElementById('building_construction_stage_u').value);
   form_data.append("building_construction_stage_date", document.getElementById('building_construction_stage_date_u').value);
   form_data.append("comments_bc", document.getElementById('comments_bc_u').value);
   form_data.append("form_type", 'construction_stage');
   var totalfiles = document.getElementById('construction_stage_images_u').files.length;
   for (var index = 0; index < totalfiles; index++) {
      form_data.append("construction_stage_images[]", document.getElementById('construction_stage_images_u').files[index]);
   }
   $.ajaxSetup({
   headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/updateSocietyFormRecords",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data); //return;
         var pcid = data.society_construction_stage_id;           
     
         // $('#bcid_'+pcid).html(data.society_construction_stage_id);
         // $('#bcid_'+pcid).html(data.data['pros_of_building']);
         $('#cs_'+pcid).html(data.data['building_construction_stage']);      
         $('#cd_'+pcid).html(data.data['building_construction_stage_date']); 
         console.log(data.data1['img']);
         if( typeof data.data1['img'] !== 'undefined'){
         const img = data.data1['img'];
         var myStringArray = img.reverse();
         var result = ""
         for (var i = 0; i < myStringArray.length; i++) {
            result = result + " <a  target='_blank' href='construction_stage_images/" + myStringArray[i] + "'>Construction Image "+ [i] + "</a>, ";
         }
         $("#i_"+pcid).html(result);
         }
         // $('#i_'+pcid).html(data.data['construction_stage_images']);      

         $('#c_'+pcid).html(data.data['comments_bc']);  
         $('#ud_'+pcid).html(data.data['created_date']);  
             
         $('#modal95').modal('close');

  
      }
      });
}

function confirmDelConstructionStage (params) {
// alert(params); return;
    var society_construction_stage_id = params;
    $('.society_construction_stage_id2').val(society_construction_stage_id);       
    $('#deleteConstructionModal').modal('open');
}

function deleteconstructionStage() {
   var url = 'deleteSocietyFormRecords';
    var form = 'delete_constructionStage';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
        // $("#award_table").load(window.location + " #award_table");    
        var ppid = data.society_construction_stage_id;
        $('#bctr_'+ppid).remove();
        // $('#deleteAwardModal').hide();
        $('#deleteConstructionModal').modal('close');
            // var incPros = parseInt($('#incPros').val()) - 1;
            //  $('#incPros').val(incPros);
        // console.log(data);return;
            
        }
    });   

}


function hide_show_forms(){
    var building_status = $('#building_status').val();  
    // var building_status = document.getElementById("building_status");  
    // alert(building_status) 

    if(building_status=='1'){
      //  alert(1)
      //unit_status_id

        $('#registered').css('display','block',);
        $('#registered1').css('display','block',);
        $('#under_construction').css('display','none');
        $('#unit_status').val(1).trigger('change');
    }else{
      // alert(2)
        $('#registered').css('display','none');
        $('#registered1').css('display','none');
        $('#under_construction').css('display','block');
        
    }
    
}

function addAmenity(param){
    var array1 = Array();
   $("input:checkbox[id=is_availableR]:checked").each(function() {
         array1.push($(this).val());
   });

   return array1;
}

function addAmenityUC(param){
    var array1 = Array();
   $("input:checkbox[id=is_availableUC]:checked").each(function() {
         array1.push($(this).val());
   });

   return array1;
}

function saveForm() {   

   // var floor_slab_from_uc = $('#floor_slab_from_uc').val();
   // alert(floor_slab_from_uc); return;
   var building_name = $('#building_name1').val() ;  

   if(building_name == ""){
      $('#building_name1').css('border-color','red');
      return false;
   }

   var building_status = $('#building_status').val();
   // alert(building_status); return;

   if(building_status == "" || building_status == null){
      $('#st_error').html("Building status field is required");
      return false;
   }else{
      $('#st_error').html("");
   }
   
   // $('#loaderForm').css('display','block');
   //  $('#submitForm').css('display','none');

   var building_status = $('#building_status').val() ;
   var building_owner = $('#building_owner').val() ;
   var unit_status = $('#unit_status').val() ;
    var project_id = $('#project_id').val();
    var society_id = $('#society_id').val();
    if( typeof society_id == 'undefined'){
         society_id = '';
   }

   if( typeof project_id == 'undefined'){
      project_id = '';
   }

   // 1=  registered  2= UC
   var form_data = new FormData();    
   form_data.append('building_name',building_name);
   form_data.append('building_status',building_status);
   form_data.append('building_owner',building_owner);
   form_data.append('unit_status[]',unit_status); //JSON.stringify(unit_status)

   if(building_status == '1'){
      var building_category_r = $('#building_category_r').val();
      var building_type_r = $('#building_type_r').val();
      if(building_type_r == null){
         building_type_r ="";
      }else{
         building_type_r = building_type_r;
      }
      var construction_technology_r = $('#construction_technology_r').val();
      var parking_r = $('#parking_r').val();
      var maintenance_amount_r = $('#maintenance_amount_r').val();
      var building_land_parcel_area_r = $('#building_land_parcel_area_r').val();
      var building_land_parcel_area_unit_r = $('#building_land_parcel_area_unit_r').val();
      var building_open_space_r = $('#building_open_space_r').val();
      var building_open_space_unit_r = $('#building_open_space_unit_r').val();
      var building_constructed_area_r = $('#building_constructed_area_r').val();
      var building_constructed_area_unit_r = $('#building_constructed_area_unit_r').val();
      var possession_year_r = $('#possession_year_r').val();
      var building_year_possession_year_r = $('#building_year_possession_year_r').val();
      var occupation_certi_dated_r = $('#occupation_certi_dated_r').val();
      var building_year_oc_dated_r = $('#building_year_oc_dated_r').val();
      var building_description_r = $('#building_description_r').val();
      var society_geo_location_lat_r = $('#society_geo_location_lat_r').val();
      var society_geo_location_long_r = $('#society_geo_location_long_r').val();
      var location_r = $('#location_r').val();
      var sub_location_r = $('#sub_location_r').val();
      var city_r = $('#city_r').val();
      var state_r = $('#state_r').val();
      var building_office_address = $('#building_office_address').val();
      var building_rating_r = $('#building_rating_r').val();
      if(building_rating_r == null){
         building_rating_r ="";
      }else{
         building_rating_r = building_rating_r;
      }
    
      var description_ameR = Array();
      $(".description_ameR").each(function () {
         // alert($(this).val());
         description_ameR.push($(this).val());
      });

      var is_availableR = addAmenity();

      var indian_green_building_r = $('#indian_green_building_r').val();
      var shifting_charges_r = $('#shifting_charges_r').val();
      var preferred_tenant_r = $('#preferred_tenant_r').val();
      var oc_certificate_r = $('#oc_certificate_r').val();
      var registration_certificate_r = $('#registration_certificate_r').val();
      var conveyance_deed_r = $('#conveyance_deed_r').val();
      var comment_r = $('#comment_r').val();
      var account_manager_r = $('#account_manager_r').val();
      var building_code_r = $('#building_code_r').val();
      var building_images_r = $('#building_images_r').val();
      var building_video_r = $('#building_video_r').val();
      var building_video_links_r = $('#building_video_links_r').val();
      // alert(building_video_links_r);

      if( typeof building_video_links_r == 'undefined' || building_video_links_r == ""){
         building_video_links_r = '';
      }else{
         building_video_links_r = building_video_links_r;
      }
      // alert(building_video_links_r); return;

      form_data.append('building_category_r[]',building_category_r);
      form_data.append('building_type_r[]',building_type_r);
      form_data.append('construction_technology_r',construction_technology_r);
      form_data.append('parking_r[]',parking_r);
      form_data.append('maintenance_amount_r',maintenance_amount_r);
      form_data.append('building_land_parcel_area_r',building_land_parcel_area_r);
      form_data.append('building_land_parcel_area_unit_r',building_land_parcel_area_unit_r);
      form_data.append('building_open_space_r',building_open_space_r);
      form_data.append('building_constructed_area_r',building_constructed_area_r);
      form_data.append('building_constructed_area_unit_r',building_constructed_area_unit_r);
      form_data.append('possession_year_r',possession_year_r);
      form_data.append('building_year_possession_year_r',building_year_possession_year_r);
      form_data.append('occupation_certi_dated_r',occupation_certi_dated_r);
      form_data.append('building_year_oc_dated_r',building_year_oc_dated_r);
      form_data.append('building_description_r',building_description_r);
      form_data.append('society_geo_location_lat_r',society_geo_location_lat_r);
      form_data.append('society_geo_location_long_r',society_geo_location_long_r);
      form_data.append('location_r',location_r);
      form_data.append('sub_location_r',sub_location_r);
      form_data.append('city_r',city_r);
      form_data.append('state_r',state_r);      
      form_data.append('building_office_address',building_office_address);
      form_data.append('building_rating_r',building_rating_r);
      form_data.append('indian_green_building_r',indian_green_building_r);
      form_data.append('shifting_charges_r',shifting_charges_r);
      form_data.append('preferred_tenant_r',preferred_tenant_r);
      form_data.append('is_availableR',is_availableR);
      form_data.append('description_ameR',description_ameR);

      var totalfiles1 = document.getElementById('oc_certificate_r').files.length;
      for (var index1 = 0; index1 < totalfiles1; index1++) {
         form_data.append("oc_certificate_r[]", document.getElementById('oc_certificate_r').files[index1]);
      }

      // form_data.append('registration_certificate_r',society_geo_location_lat_r);

      var totalfiles2 = document.getElementById('registration_certificate_r').files.length;
      for (var index2 = 0; index2 < totalfiles2; index2++) {
         form_data.append("registration_certificate_r[]", document.getElementById('registration_certificate_r').files[index2]);
      }

      // form_data.append('conveyance_deed_r',society_geo_location_long_r);
      var totalfiles3 = document.getElementById('conveyance_deed_r').files.length;
      for (var index3 = 0; index3 < totalfiles3; index3++) {
         form_data.append("conveyance_deed_r[]", document.getElementById('conveyance_deed_r').files[index3]);
      }

      form_data.append('comment_r',comment_r);
      form_data.append('account_manager_r',account_manager_r);
      form_data.append('building_code_r',society_geo_location_lat_r);
      // form_data.append('building_images_r',society_geo_location_long_r);
      var totalfiles4 = document.getElementById('building_images_r').files.length;
      for (var index4 = 0; index4 < totalfiles4; index4++) {
         form_data.append("building_images_r[]", document.getElementById('building_images_r').files[index4]);
      }

      // form_data.append('building_video_r',society_geo_location_lat_r);
      var totalfiles5 = document.getElementById('building_video_r').files.length;
      for (var index5 = 0; index5 < totalfiles5; index5++) {
         form_data.append("building_video_r[]", document.getElementById('building_video_r').files[index5]);
      }
      form_data.append('building_video_links_r',building_video_links_r);

   }else{
      var element = document.getElementsByName("floor_slab_from_uc");
      var floor_slab_from_uc=  Array();
      for(var i=0; i<element.length; i++) {
         if(element[i].value != ""){
            floor_slab_from_uc.push(element[i].value);
         }else{
            floor_slab_from_uc.push(0);
         }
      }
      //return;


      
      var element1 = document.getElementsByName("floor_slab_to_uc");
      var floor_slab_to_uc=  Array();
      for(var i1=0; i1<element1.length; i1++) {
         if(element1[i1].value != ""){
            floor_slab_to_uc.push(element1[i1].value);
         }else{
            floor_slab_to_uc.push(0);
         }
      }
      // alert(floor_slab_to_uc);

      var element2 = document.getElementsByName("configuration_uc");
      var configuration_uc=  Array();
      for(var i2=0; i2<element2.length; i2++) {
         if(element2[i2].value != ""){
            configuration_uc.push(element2[i2].value);
         }else{
            configuration_uc.push(0);
         }
      }

      var element3 = document.getElementsByName("area_uc");
      var area_uc=  Array();
      for(var i3=0; i3<element3.length; i3++) {
         if(element3[i3].value != ""){
            area_uc.push(element3[i3].value);
         }else{
            area_uc.push(0);
         }
      }

      var element4 = document.getElementsByName("rs_per_sq_ft_uc");
      var rs_per_sq_ft_uc=  Array();
      for(var i4=0; i4<element4.length; i4++) {
         if(element4[i4].value != ""){
            rs_per_sq_ft_uc.push(element4[i4].value);
         }else{
            rs_per_sq_ft_uc.push(0);
         }         
      }

      var element5 = document.getElementsByName("box_price_uc");
      var box_price_uc=  Array();
      for(var i5=0; i5<element5.length; i5++) {
         if(element5[i5].value != ""){
            box_price_uc.push(element5[i5].value);
         }else{
            box_price_uc.push(0);
         }
      }

      var element6 = document.getElementsByName("society_floor_rise_id_uc");
      var society_floor_rise_id_uc=  Array();
      for(var i6=0; i6<element6.length; i6++) {
         if(element6[i6].value != ""){
            society_floor_rise_id_uc.push(element6[i6].value);
         }else{
            society_floor_rise_id_uc.push(0);
         }
      }



      

      var building_category_uc = $('#building_category_uc').val();
      var building_type_uc = $('#building_type_uc').val();
      if(building_type_uc == null){
         building_type_uc ='';
      }else{
         building_type_uc = building_type_uc;
      }
      var building_configuration_details_uc = $('#building_configuration_details_uc').val();
      var construction_technology_uc = $('#construction_technology_uc').val();
      var parking_uc = $('#parking_uc').val();

      var funded_by_uc = $("#funded_by_uc").val();
      var payment_plan_uc = $("#payment_plan_uc").val();
      var payment_plan_comment_uc = $("#payment_plan_comment_uc").val();
      var brokarage_comment_uc = $("#brokarage_comment_uc").val();
      var brokarage_from_date_uc = $("#brokarage_from_date_uc").val();
      var brokarage_to_date_uc = $("#brokarage_to_date_uc").val();

      var zero_to_two_uc = $("#zero_to_two_uc").val();
      var three_to_five_uc = $("#three_to_five_uc").val();
      var six_to_ten_uc = $("#six_to_ten_uc").val();
      var above_eleven_uc = $("#above_eleven_uc").val();

      var building_description_uc = $('#building_description_uc').val();      
      var maintenance_amount_uc = $('#maintenance_amount_uc').val();      
      var society_geo_location_lat_uc = $('#society_geo_location_lat_uc').val();      
      var society_geo_location_long_uc = $('#society_geo_location_long_uc').val();      
      var location_uc = $('#location_uc').val();      
      var sub_location_uc = $('#sub_location_uc').val();      
      var city_uc = $('#city_uc').val();      
      var state_uc = $('#state_uc').val();      
      var building_office_address_uc = $('#building_office_address_uc').val();      
      var building_land_parcel_area_uc = $('#building_land_parcel_area_uc').val();      
      var building_open_space_uc = $('#building_open_space_uc').val();      
      var building_constructed_area_uc = $('#building_constructed_area_uc').val();
      
      

      var building_rating_uc = $('#building_rating_uc').val();
      if(building_rating_uc == null ){
         building_rating_uc = '';
      }else{
         building_rating_uc = building_rating_uc;
      }
      var indian_green_building_uc = $('#indian_green_building_uc').val();
      var building_lauch_date_uc = $('#building_lauch_date_uc').val();
      var builder_completion_year_uc = $('#builder_completion_year_uc').val();
      var rera_completion_year_uc = $('#rera_completion_year_uc').val();
      // var rera_certificate_uc = $('#rera_certificate_uc').val();  //img
      // var flat_images_uc = $('#flat_images_uc').val();  //img
      // var building_images_uc = $('#building_images_uc').val();  //img
      // var building_video_uc = $('#building_video_uc').val();  //img
      var building_video_links_uc = $('#building_video_links_uc').val();
      if( typeof building_video_links_uc == 'undefined' || building_video_links_uc == ""){
         building_video_links_uc = '';
      }else{
         building_video_links_uc = building_video_links_uc;
      }
      var comment_uc = $('#comment_uc').val();
      var total_flat_available_uc = $('#total_flat_available_uc').val();
      var account_manager_uc = $('#account_manager_uc').val();
      var building_code_uc = $('#building_code_uc').val();

      var is_availableUC = addAmenityUC1();

      var description_ameUC = Array();
      $(".description_ameUC").each(function () {
         //
         description_ameUC.push($(this).val());
      });

      // alert( description_ameUC ); return;
      


      // var is_availableR = addAmenity();

      form_data.append('building_category_uc[]',building_category_uc);
      form_data.append('building_type_uc[]',building_type_uc);
      form_data.append('building_configuration_details_uc',building_configuration_details_uc);
      form_data.append('construction_technology_uc',construction_technology_uc);
      form_data.append('parking_uc[]',parking_uc);
      form_data.append('funded_by_uc',funded_by_uc);
      form_data.append('payment_plan_uc[]',payment_plan_uc);
      form_data.append('payment_plan_comment_uc',payment_plan_comment_uc);
      form_data.append('brokarage_comment_uc',brokarage_comment_uc);
      form_data.append('brokarage_from_date_uc',brokarage_from_date_uc);
      form_data.append('brokarage_to_date_uc',brokarage_to_date_uc);
      form_data.append('zero_to_two_uc',zero_to_two_uc);
      form_data.append('three_to_five_uc',three_to_five_uc);
      form_data.append('six_to_ten_uc',six_to_ten_uc);
      form_data.append('above_eleven_uc',above_eleven_uc);
      form_data.append('building_description_uc',building_description_uc);
      form_data.append('maintenance_amount_uc',maintenance_amount_uc);
      form_data.append('society_geo_location_lat_uc',society_geo_location_lat_uc);
      form_data.append('society_geo_location_long_uc',society_geo_location_long_uc);
      form_data.append('location_uc',location_uc);
      form_data.append('sub_location_uc',sub_location_uc);
      form_data.append('city_uc',city_uc);
      form_data.append('state_uc',state_uc);
      form_data.append('building_office_address_uc',building_office_address_uc);
      form_data.append('building_land_parcel_area_uc',building_land_parcel_area_uc);
      form_data.append('building_open_space_uc',building_open_space_uc);
      form_data.append('building_constructed_area_uc',building_constructed_area_uc);
      form_data.append('building_rating_uc',building_rating_uc);
      form_data.append('indian_green_building_uc',indian_green_building_uc);
      form_data.append('building_lauch_date_uc',building_lauch_date_uc);
      form_data.append('builder_completion_year_uc',builder_completion_year_uc);
      form_data.append('rera_completion_year_uc',rera_completion_year_uc);
      form_data.append('building_video_links_uc',building_video_links_uc);
      form_data.append('comment_uc',comment_uc);
      form_data.append('total_flat_available_uc',total_flat_available_uc);
      form_data.append('account_manager_uc',account_manager_uc);
      form_data.append('building_code_uc',building_code_uc);
      form_data.append('floor_slab_from_uc',floor_slab_from_uc);
      form_data.append('floor_slab_to_uc',floor_slab_to_uc);
      form_data.append('configuration_uc',configuration_uc);
      form_data.append('area_uc',area_uc);
      form_data.append('rs_per_sq_ft_uc',rs_per_sq_ft_uc);
      form_data.append('box_price_uc',box_price_uc);
      form_data.append('society_floor_rise_id_uc',society_floor_rise_id_uc);      
      form_data.append('is_availableUC',is_availableUC);
      form_data.append('description_ameUC',description_ameUC);    


      var totalfiles1 = document.getElementById('rera_certificate_uc').files.length;
      for (var index1 = 0; index1 < totalfiles1; index1++) {
         form_data.append("rera_certificate_uc[]", document.getElementById('rera_certificate_uc').files[index1]);
      }

      var totalfiles2 = document.getElementById('flat_images_uc').files.length;
      for (var index2 = 0; index2 < totalfiles2; index2++) {
         form_data.append("flat_images_uc[]", document.getElementById('flat_images_uc').files[index2]);
      }

      var totalfiles3 = document.getElementById('building_images_uc').files.length;
      for (var index3 = 0; index3 < totalfiles3; index3++) {
         form_data.append("building_images_uc[]", document.getElementById('building_images_uc').files[index3]);
      }
      
      var totalfiles4 = document.getElementById('building_video_uc').files.length;
      for (var index4 = 0; index4 < totalfiles4; index4++) {
         form_data.append("building_video_uc[]", document.getElementById('building_video_uc').files[index4]);
      }

   }



   form_data.append("form_type", 'other_form');
      form_data.append("society_id", society_id);
      form_data.append("project_id", project_id);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
         url:"/add-society-master",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {  
            console.log(data); //return;
            console.log(data.lastID); 
            // $('#company_id2').val(data.company_id); 
            var err = 'print-error-msg';

            if($.isEmptyObject(data.error)){
               window.location.href = '/company-master'; 

               // if (confirm('Are you sure you want to continue!')) {
               //       window.location.href = '/wing-master/?society_id='+data.lastID;//http://127.0.0.1:8000/  /
               //       console.log('Thing was saved to the database.');
               //       $('#loaderForm').css('display','none');
               //       $('#submitForm').css('display','block');
               // } else {
               //    window.location.href = '/company-master';                  
               // }

            }else{
              
                printErrorMsg(data.error,err);
                $('#loaderForm').css('display','none');
                $('#submitForm').css('display','block');
            }
         
         }
      });
      function printErrorMsg (msg,err) {

         $("."+err).find("ul").html('');
         $("."+err).css('display','block');
         $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
         });                
      }

}

function copyMobileNo_s(){
    var checkBox = document.getElementById("copywhatsapp");
    var mobile_number = $('#mobile_number_h').val();
    if (checkBox.checked == true){
        if(mobile_number != ''){
            $('#whatsapp_number_h').val(mobile_number);   
        }else{
            alert("Enter mobile number first");
            document.getElementById("copywhatsapp").checked = false;
        }        
      } else {
        $('#whatsapp_number_h').val('');
      }
}

function add_designation_form(){
   var url = 'addDesignationCompanyMaster';
   var form = 'add_designation_form';
   var err = 'print-error-msg_add_designation';
   var desig = $('#add_designation').val();
   // alert(desig);
   $.ajax(  {
      url:"/"+url,
      type:"GET",
      data:  {add_designation:desig}      ,             
         //  $('#'+form).serialize() ,
      
      success: function(data) {
         // console.log(data); return;
         var a = data.a;
         var option = a.option;
         var value = a.value; 

         var newOption= new Option(option,value, true, false);
         // Append it to the select
         $(".designation_h").append(newOption);//]].trigger('change');
         // $('#designation_h').append($('<option>').val(optionValue).text(optionText));


         // console.log(data.success);return;
         if($.isEmptyObject(data.error)){
            $('.success11').html(data.success);
            //  alert(data.success);
            //  location.reload();
         }else{
               printErrorMsg(data.error,err);
         }
      }
   });    
   
   function printErrorMsg (msg,err) {

      $("."+err).find("ul").html('');
      $("."+err).css('display','block');
      $.each( msg, function( key, value ) {
         $("."+err).find("ul").append('<li>'+value+'</li>');
      });                
   }
}

   function apply_css1(skip,attr, val=''){   
    var id = returnColArray1();  
    id.forEach(function(entry) {
        
        if(entry==skip){
            $('#'+skip).css(attr,'orange','!important'); //#BECF3F
        }else{            
            $('#'+entry).css(attr,val);
            action_to_hide();            
            
        }        
    });
}

function hide_n_show1(skip){
    var id = collapsible_body_ids1();
    collapsible_body_ids1().forEach(function(entry) {
        if(entry==skip){
            var x = document.getElementById(skip);  
            console.log(skip)
            if(skip != 'ameneties_r_div1') {
                //$('#'+skip).css('background-color','rgb(234 233 230)');
            }
            // $('#'+skip).css('background-color','rgb(234 233 230)');
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
             
               var s = skip.replace("_div", "");                
                x.style.display = "none";
                $('#col_'+s).css('background-color','white');
            }
        }else{          
            $('#'+entry).hide();
        }
        
    });
}

function returnColArray1(){
    var a = Array('col_amenities1','col_amenities_r1','col_construction','col_floor','col_payment','col_brokerage','col_desc','col_desc_r','col_address_uc','col_address_r','col_landparcel','col_landparcel_r','col_proscons','col_proscons_r','col_showflat','col_imagesvideo','col_imagesvideo_r','col_age','col_keymembers','col_building_config_details','col_building_config_details_uc1','col_unit_uc_amenity');
    return a;
}

function collapsible_body_ids1(){
    var b = Array('ameneties_div1','ameneties_r_div1','construcition_div','floor_div','payment_div','brokerage_div','desc_div','desc_r_div','address_r_div','address_div_uc','landparcel_div','landparcel_r_div','proscons_div','proscons_r_div','showflat_div','imagesvideo_div','imagesvideo_r_div','age_div','keymembers_div','building_config_details_div','building_config_details_div_uc1','unit_amenity_uc_div');
    return b;
}
   
function ShowHideamenities1() {
   apply_css1('col_amenities1','background-color',''); 
   hide_n_show1("ameneties_div1");    
}

function ShowHideamenities1_r() {
   apply_css1('col_amenities_r1','background-color',''); 
   hide_n_show1("ameneties_r_div1");   
}

function ShowHideconstruction() {
   apply_css1('col_construction','background-color',''); 
   hide_n_show1("construcition_div"); 
}

function ShowHidefloor() {
   apply_css1('col_floor','background-color',''); 
   hide_n_show1("floor_div"); 
}

function ShowHidepayment() {
   apply_css1('col_payment','background-color',''); 
   hide_n_show1("payment_div");
}


function ShowHidebrokerage() {
   apply_css1('col_brokerage','background-color',''); 
   hide_n_show1("brokerage_div");
}

function ShowHidedesc() {
   apply_css1('col_desc','background-color',''); 
   hide_n_show1("desc_div");
}

function ShowHidedesc_r() {
   apply_css1('col_desc_r','background-color',''); 
   hide_n_show1("desc_r_div");
}

function ShowHideaddress() {
   apply_css1('col_address_uc','background-color',''); 
   hide_n_show1("address_div_uc");
}

function ShowHideaddress_r() {
   apply_css1('col_address_r','background-color',''); 
   hide_n_show1("address_r_div");
}

function ShowHidelandparcel() {
   apply_css1('col_landparcel','background-color',''); 
   hide_n_show1("landparcel_div");
}

function ShowHidelandparcel_r() {
   apply_css1('col_landparcel_r','background-color',''); 
   hide_n_show1("landparcel_r_div");
}

function ShowHideproscons() {
   apply_css1('col_proscons','background-color',''); 
   hide_n_show1("proscons_div");
}

function ShowHideproscons_r() {
   apply_css1('col_proscons_r','background-color',''); 
   hide_n_show1("proscons_r_div");
}

function ShowHideshowflat() {
   apply_css1('col_showflat','background-color',''); 
   hide_n_show1("showflat_div");
}

function ShowHideimagesvideo() {
   apply_css1('col_imagesvideo','background-color',''); 
   hide_n_show1("imagesvideo_div");            
}

function ShowHideimagesvideo_r() {
   apply_css1('col_imagesvideo_r','background-color',''); 
   hide_n_show1("imagesvideo_r_div");
}

function ShowHideage() {
   apply_css1('col_age','background-color',''); 
   hide_n_show1("age_div");
}

function ShowHidekeymembers() {
   apply_css1('col_keymembers','background-color',''); 
   hide_n_show1("keymembers_div");      
}

function ShowHidebuilding_config_details() {
   apply_css1('col_building_config_details','background-color',''); 
   hide_n_show1("building_config_details_div");               
}

function ShowHidebuilding_config_details_uc() {
   // alert();
   apply_css1('col_building_config_details_uc','background-color',''); 
   hide_n_show1("building_config_details_div_uc1");               
}

function UnitAmenity(){
   apply_css1('col_unit_uc_amenity','background-color',''); 
   hide_n_show1("unit_amenity_uc_div");    
}

            
</script> 
<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->