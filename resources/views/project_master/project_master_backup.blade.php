<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.-container--default .-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.-container--open .-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}
.card .card-content {
    padding-top: 0px;
    min-height: auto !important;
}

.collapsible-header:hover {
    background-color:rgb(63 207 104) !important;
  }
      </style>
       <link href="{{ asset('app-assets/css/summernote/summernote-bs4.css') }} " rel="stylesheet">

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">          
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->    

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

        <div class="row" style="font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
                    
                        <span class="userselect"> @if(isset($project_id) && !empty($project_id)) UPDATE @else ADD @endif PROJECT MASTER</span>
                </div><br>
                
                @if(isset($project_id) && !empty($project_id) )
                    <form id="project_master_form_update" method="post"   enctype="multipart/form-data"> 
                    <input text="hidden" value="{{$project_id}}" name="project_id" style="display:none">
                @else
                    <form id="project_master_form" method="post"   enctype="multipart/form-data"> 
                @endif
                 @csrf
                <div class="row">
                       <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="group_name" name="group_name" class="validate  browser-default">
                                <option value="" disabled selected>Select</option>
                                  @foreach($group_name as $group_name)
                                    <option value="{{$group_name->company_id}}" @if(isset($company_id) && !empty($company_id) )  @if($group_name->company_id == $company_id ) selected  @endif  @endif>{{ ucwords($group_name->group_name)}} </option>
                                    @endforeach                            
                                </select>
                                <label class="active">Group Name</label>
                        </div>  
                        
                        <div class="input-field col l3 m4 s12 display_search">                                
                             <label class="active"> Company Name </label>
                            <input type="text" class="validate" name="company_name_final" id="company_name_final"   placeholder="Enter" @if(isset($company_name)) value="{{$company_name}}" @endif >
                        </div>

                        

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Complex Name </label>
                        <input type="text" class="validate" name="complex_name" id="complex_name" @if(isset($project_complex_name)) value="{{$project_complex_name}}" @endif   placeholder="Enter"  >                                                         
                    </div>

                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                        <ul class="collapsible" style="margin-top: -2px;width: 321px;">
                        <li onClick="ShowHideaddress()">
                        <div class="collapsible-header"  id="col_address" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Address</h5></div>               
                        </li>                                        
                        </ul>
                    </div>


                    </div>
                    <div class="row" id="address_div" style="display:none">
                    <br>


                    <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="sub_location" name="sub_location" class="validate  browser-default">
                                <option value="" disabled selected>Select</option>
                                @if(isset($sub_location) && !empty($sub_location))
                                    @foreach($sub_location as $sub_location)
                                    <option value="{{$sub_location->sub_location_id}}" @if(isset($sub_location_id))  @if($sub_location->sub_location_id == $sub_location_id)  selected @endif @endif >{{ ucwords($sub_location->sub_location_name)}}</option>
                                    @endforeach                              
                                @endif   
                                </select>
                                <label class="active">Sub Location</label>
                        </div>


                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="location" name="location" class="validate  browser-default"  onChange="getsublocation(this)">
                                <option value="" disabled selected>Select</option>
                                @if(isset($location) && !empty($location))
                                    @foreach($location as $location)
                                    <option value="{{$location->location_id}}" @if(isset($location_id))  @if($location->location_id == $location_id)  selected @endif @endif >{{ ucwords($location->location_name)}}</option>
                                    @endforeach
                                @endif
                                </select>
                                <label class="active">Location</label>
                        </div>
               
                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="city" name="city" class="validate  browser-default" onChange="getlocation(this)">
                                 @if(isset($city) && !empty($city))
                                    @foreach($city  as $cty)
                                    <option value="{{$cty->city_id}}" @if(isset($city_id)) @if($cty->city_id == $city_id) selected @endif @endif>{{ ucfirst($cty->city_name) }}</option>
                                    @endforeach
                                   @endif                              
                                </select>
                                <label class="active">City</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="state" name="state" class="validate  browser-default" onChange="getCity(this)">
                                <option value="" disabled selected>Select</option>
                                     @foreach($state as $state)
                                    <option value="{{$state->dd_state_id}}" @if(isset($state_id)) @if($state->dd_state_id == $state_id) selected @endif @endif >{{ ucwords($state->state_name)}}</option>
                                    @endforeach                                  
                                </select>
                                <label class="active">State</label>
                        </div>

                     

                        
                     

                     

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                             <label  class="active">Sales Office Address</label>
                             <input type="text" class="validate" name="sales_office_address" id="sales_office_address"   placeholder="Enter" @if(isset($sales_office_address)) value="{{$sales_office_address}}" @endif >
                            <!-- <textarea style="border-color: #5b73e8; padding-top: 12px;width:104% !important" placeholder="Enter" name="sales_office_address" rows='20' col='40'></textarea> -->
                        </div>
        

                    </div>
                    <br>
                    
                    <div class="row">
                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="category" name="category" class="validate  browser-default" multiple="true">
                                <!-- <option value="" disabled selected>Select</option> -->
                                     @foreach($category as $category)
                                    <option value="{{$category->category_id}}" @if(isset($category_id))  @if($category->category_id == $category_id)  selected @endif @endif >{{ ucwords($category->category_name)}}</option>
                                    @endforeach                                 
                                </select>
                                <label class="active">Complex Category </label>
                        </div>

                        
                        <div class="input-field col l3 m4 s12 display_search">                                
                                <!-- <select  id="category_type" name="unit_status_of_complex" class="validate  browser-default">
                                <option value="" disabled selected>Select</option>                                    
                                     @foreach($unit_status as $unit_status)
                                    <option value="{{$unit_status->unit_status_id}}" @if(isset($unit_status_of_complex))  @if($unit_status->unit_status_id == $unit_status_of_complex)  selected @endif @endif >{{ ucwords($unit_status->unit_status)}}</option>
                                    @endforeach                                                            
                                </select>
                                <label class="active">Unit Status of Complex </label> -->
                                <label  class="active">Unit Status of Complex </label>
                                        <input type="text" class="validate" name="unit_status_of_complex" id="unit_status_of_complex" value="{{$unit_status_of_complex}}"  readonly placeholder="" >
                        </div>
                            
                        <div class="input-field col l3 m4 s12 display_search"  style="margin-bottom: -11px;">
                            <ul class="collapsible" style="margin-top: -2px;width: 321px">
                            <li onClick="ShowHideamenities()">
                            <div class="collapsible-header"  id="col_amenities" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Complex Amenities</h5></div>               
                            </li>                                        
                            </ul>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search"  style="margin-bottom: -11px;">
                            <ul class="collapsible" style="margin-top: -2px;width: 321px">
                            <li onClick="ShowHidelandparcel()">
                            <div class="collapsible-header"  id="col_landparcel" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" > Land Parcel</h5></div>               
                            </li>                                        
                            </ul>
                        </div>
                </div>
              
                <div class="row" id="amenities_div" style="display:none">   
                <br>
                    @if(isset($getProjectAmenitiesData) && !empty($getProjectAmenitiesData))     
                        @foreach($getProjectAmenitiesData as $getProjectAmenitiesData)
                            @foreach($project_amenities as $ame)     
                                @if($getProjectAmenitiesData->amenities_id == $ame->project_amenities_id) 
                                    <div class="col l6 m6 s12">
                                        <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">{{ucfirst($ame->amenities)}}</div>
                                        <div class="col l3 m3 s12">
                                            <label><input type="checkbox" name="is_available[]" id="is_available" value="{{$ame->project_amenities_id}}" checked ><span>Available</span></label>
                                        </div>
                                        <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">
                                            <label  class="active">Description: </label>
                                        <input type="text" class="validate" name="description_ame[]" value="{{$getProjectAmenitiesData->description}}"   placeholder="Enter" style="height: 36px;" >
                                        </div>
                                    
                                    </div>  
                                    @else

                                    <div class="col l6 m6 s12">
                                        <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">{{ucfirst($ame->amenities)}}</div>
                                        <div class="col l3 m3 s12">
                                            <label><input type="checkbox" name="is_available[]" value="{{$ame->project_amenities_id}}" ><span>Available</span></label>
                                        </div>
                                        <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">
                                            <label  class="active">Description: </label>
                                        <input type="text" class="validate" name="description_ame[]"   placeholder="Enter" style="height: 36px;" >
                                        </div>                            
                                    </div>                      
                                @endif   
                                 
                            @endforeach
                      
                        @endforeach
                        
                    @else
                        @foreach($project_amenities as $ame) 
                            <div class="col l6 m6 s12">
                                <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">{{ucfirst($ame->amenities)}}</div>
                                <div class="col l3 m3 s12">
                                    <label><input type="checkbox" name="is_available[]" value="{{$ame->project_amenities_id}}" ><span>Available</span></label>
                                </div>
                                <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">
                                    <label  class="active">Description: </label>
                                <input type="text" class="validate" name="description_ame[]"   placeholder="Enter" style="height: 36px;" >
                                </div>
                            
                            </div> 

                        @endforeach

                    @endif
                    <br>
                  
                </div>
                <div class="row" id="landaprcel_div" style="display:none">
                  <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="number" class="input_select_size" name="complex_land_parcel" id="complex_land_parcel"  placeholder="Enter" @if(isset($complex_land_parcel)) value="{{$complex_land_parcel}}" @endif >
                            <!-- <input type="text" id="complex_land_parcel_con"> -->
                                <label>Complex Land Parcel
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="carpet_area_unit1" name="complex_land_parcel_unit" class=" browser-default" >  <!-- onChange="unitConvertor(this)" -->
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            
                            </div>
                        </div>
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="number" class="input_select_size" name="complex_open_space" id="open_space"  placeholder="Enter" @if(isset($complex_open_space)) value="{{$complex_open_space}}" @endif>
                                <label> Complex Open Space
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="carpet_area_unit" name="open_space_unit" class=" browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div> 
                </div>
                
                <br>
                <div class="row">                   
                        <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                            <ul class="collapsible" style="margin-top: -2px;width: 321px">
                            <li onClick="ShowHideproscons()">
                            <div class="collapsible-header"  id="col_proscons" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" > Complex Pros & Cons</h5></div>               
                            </li>                                        
                            </ul>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="project_rating" name="project_rating" class="validate  browser-default" onfocus='this.size=2'>
                                <option value="" disabled selected>Select</option>
                                   @foreach($rating as $rating)
                                    <option value="{{$rating->rating_id}}"  @if(isset($complex_rating))  @if($rating->rating_id == $complex_rating)  selected @endif @endif >{{ ucwords($rating->rating)}}</option>
                                    @endforeach                                 
                                </select>
                                <label class="active">Complex rating</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                            <ul class="collapsible" style="margin-top: -2px;width: 321px">
                            <li onClick="ShowHidedesc()">
                            <div class="collapsible-header"  id="col_desc" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" > Complex Description</h5></div>               
                            </li>                                        
                            </ul>
                        </div>

           

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">GST certificate </label>
                            <input type="file" name="gst_certificate[]"  id="input-file-now" class="dropify" multiple='true'  data-default-file="" style="padding-top: 14px;">
                        </div>

                      

                        
                </div>
               
                <div class="row" id="proscons_div" style="display:none;padding-bottom: 19px;margin-right: -4px;margin-left: 12px">
                <br>
                    <div class="row">
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                    
                            <label  class="active">Pros of Complex </label>
                            <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="pros_of_complex" rows='20' col='40'></textarea> -->
                            <input type="text" class="validate" name="pros_of_complex" id="pros_of_complex"    placeholder="Enter" @if(isset($project_pros)) value="{{$project_pros}}" @endif >
                        </div> 

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                    
                            <label  class="active">Cons of Complex </label>
                            <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="cons_of_complex" rows='20' col='40'></textarea> -->
                            <input type="text" class="validate" name="cons_of_complex" id="cons_of_complex"   placeholder="Enter" @if(isset($project_cons)) value="{{$project_cons}}" @endif >
                        </div> 

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <a href="javascript:void(0)" onClick="saveProsCons()" class="btn-small  waves-effect waves-light green darken-1" >Save</a>
                        </div> 
                    </div>
                    <br>
                    <div class="row">
                    <table class="bordered" id="pros_cons_table" style="font-size: unset;">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Sr No. </th>
                        <th>Pros</th>
                        <th>Cons</th>
                        <th>Action</th>               
                        </tr>
                        </thead>
                        <tbody>
                            <!-- getProjectProsConsData -->
                             @if(isset($getProjectProsConsData))
                                    @foreach($getProjectProsConsData as $getProjectProsConsData) 
                                    <tr id="pp_{{$getProjectProsConsData->project_pro_cons_id}}">
                                        <td id="pid_{{$getProjectProsConsData->project_pro_cons_id}}">{{$getProjectProsConsData->project_pro_cons_id}}</td>
                                        <td id="p_{{$getProjectProsConsData->project_pro_cons_id}}">{{$getProjectProsConsData->pros}}</td>
                                        <td id="c_{{$getProjectProsConsData->project_pro_cons_id}}">{{$getProjectProsConsData->cons}}</td>
                                        <td>
                                        <a href="javascript:void(0)"  class="waves-effect waves-light" onClick="updateProsCons('{{$getProjectProsConsData->project_pro_cons_id}}')"  >Edit</a> |
                                        <a href='javascript:void(0)' class="waves-effect waves-light"   onClick="confirmDelProsCons('{{$getProjectProsConsData->project_pro_cons_id}}')" >Delete</a>
                                        </td>
                                    </tr>    
                                    @endforeach
                                @endif
                     
                        </tbody>
                    </table>
            </div>

                    
                </div>
                  <!-- put in below row html editor  -->
                <div class="row" id="desc_div" style="display:none">
                
                     <section class="full-editor">
                            <div class="row">
                                <div class="col s12">
                              
                                    <div class="card">
                                    <br>
                                        <div class="card-content">
                                        Complex Description

                                           
                                            <div class="row">
                                                <div class="col s12">
                                               
                                                    <div id="full-wrapper" name="html_editor2">
                                                        <div id="full-container" name="html_editor1">
                                                        <textarea style="border-color: #5b73e8; padding-top: 12px;width: 1238px; height: 46px;" placeholder="Enter" name="complex_description"  > @if(isset($complex_description)) {{$complex_description}} @endif</textarea>
                                                            <!-- <textarea class="editor" > --> 
                                                            <!-- <div class="editor" name="html_editor">
                                                                
                                                                
                                                            </div> complex_description -->
                                                            <!-- </textarea> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                </div>

                <br>   
                <div class="row">
                
                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                        <ul class="collapsible" style="margin-top: -2px;width: 321px">
                        <li onClick="ShowHideimages()">
                        <div class="collapsible-header"  id="col_images"  ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Images, Brochure ,Video &  Video Link </h5></div>               
                        </li>                                        
                        </ul>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">  
                        <label  class="active">Comments </label>
                        <input type="text" class="validate" name="comments"   placeholder="Enter" @if(isset($comments)) value="{{$comments}}" @endif >
                    </div>  

                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                        <ul class="collapsible" style="margin-top: -2px;width: 321px">
                        <li onClick="ShowHideheirarchy()">
                        <div class="collapsible-header"  id="col_heirarchy"  ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;" >Complex team Hierarchy</h5></div>               
                        </li>                                        
                        </ul>
                    </div>

                      
                </div>

    
                <div class="row" id="images_div" style="display:none">
                  <br>
                     <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Complex Images  </label>
                            <input type="file" name="upload_photos[]"  id="input-file-now" class="dropify" multiple='true'  data-default-file="" style="padding-top: 14px;">
                            
                            @if(isset($getProjectPhotoData) && !empty($getProjectPhotoData))
                            @php $i=1; @endphp 
                                @foreach($getProjectPhotoData as $getProjectPhotoData)
                                     @if($getProjectPhotoData->meta_key == 'upload_photos')
                                     <a href="{{ asset('complex/'.$getProjectPhotoData->url) }}" target="_blank"> Uploaded Photo {{$i}} </a>
                                     <!-- <input type="text" value="{{$getProjectPhotoData->url}}" name="upload_photos1[]"> -->
                                     <br>
                                     @php $i++; @endphp 
                                    @endif
                                @endforeach
                            @endif
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Complex Video  </label>
                            <input type="file" name="upload_video[]"  id="input-file-now" class="dropify"  multiple='true'  data-default-file="" style="padding-top: 14px;">
                            @if(isset($getProjectPhotoData1) && !empty($getProjectPhotoData1))
                            @php $i=1; @endphp 
                                @foreach($getProjectPhotoData1 as $getProjectPhotoData1)
                                     @if($getProjectPhotoData1->meta_key == 'upload_video')
                                     <a href="{{ asset('complex/'.$getProjectPhotoData1->url) }}" target="_blank"> Uploaded Video {{$i}} </a>
                                     <!-- <input type="text" value="{{$getProjectPhotoData->url}}" name="upload_photos1[]"> -->
                                     <br>
                                     @php $i++; @endphp 
                                    @endif
                                @endforeach
                            @endif
                        </div>

                        
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Add Video Link </label>
                            <input type="text" class="validate" name="video_link"   placeholder="Enter"  @if(isset($video_links)) value="{{$video_links}}" @endif>
                        </div> 


                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">E Brochure  </label>
                            <input type="file" name="e_brochure[]"  id="input-file-now"  class="dropify"  multiple='true'  data-default-file="" style="padding-top: 14px;">
                            @if(isset($e_brochure) && !empty($e_brochure))
                            @php $e_brochure = json_decode($e_brochure); $i=1; @endphp 
                            @foreach($e_brochure as $e_brochure)
                                <a href="{{ asset('complex/'.$e_brochure) }}" target="_blank"> E Brochure {{$i}}</a>
                                <br>
                               @php $i++; @endphp 
                            @endforeach    
                            @endif
                            
                              
                        </div>

                       


                  
                </div>
                <!-- <div class="row">   
                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">GST No.: </label>
                        <input type="text" class="validate" name="gst_no"    placeholder="Enter" @if(isset($gst_no)) value="{{$gst_no}}" @endif>
                    </div> 
                </div>   -->
                
            <div class="row" id="heirarchy_div" style="display:none">
            <br>
                   
                    <div class="row" >
                        <div class="col l3 m3 s12">
                            <label><input type="radio" name="user_type" id="user_typei" value="inhouse" checked  onChange="hideshowCN('inhouse')" ><span>INHOUSE</span></label>
                        </div>

                        <div class="col l3 m3 s12">
                            <label><input type="radio" name="user_type" id="user_typeo" onChange="hideshowCN('outsource')" value="outsource"  ><span>OUTSOURCE</span></label>
                        </div>

                        <div class="col l3 m3 s12" id='company_div' style="display:none">
                        <label  class="active">Company Name </label>
                            <input type="text" name="company_name1" id="company_name1" class="validate"  >
                        </div>                    

                    </div>
                    <br>
                    <div class="row">     
                                     
                            <div class="row">
                                <div class="col l11">
                                    <div class="row">
                                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                            <label for="cus_name active" class="dopr_down_holder_label active">Name 
                                            </label>
                                            <div  class="sub_val no-search">
                                                <select  id="name_initial" name="name_initial" class=" browser-default ">
                                                @foreach($initials as $ini)
                                                    <option value="{{$ini->initials_id}}">{{ucfirst($ini->initials_name)}}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <input type="text" class="validate mobile_number" name="name_h" id="name_h"  placeholder="Enter"  >
                                        </div>

                                        <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                            <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: </label>
                                            <div  class="sub_val no-search">
                                                <select  id="country_code" name="country_code" class=" browser-default">
                                                    @foreach($country_code as $cou)
                                                    <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <input type="text" class="validate mobile_number" name="mobile_number_h" id="mobile_number_h"  placeholder="Enter" >
                                        </div>

                                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                            <div  class="sub_val no-search">
                                                <select  id="country_code_w" name="country_code_w" class=" browser-default">
                                                @foreach($country_code as $cou)
                                                    <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: </label>
                                            <input type="text" class="validate mobile_number" name="wap_number" id="wap_number"  placeholder="Enter" >
                                            <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                                <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                                            </div>
                                        </div> 

                                        <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                                            <label for="primary_email active" class="active">Email Address: </label>
                                            <input type="email" class="validate" name="primary_email_h" id="primary_email_h"  placeholder="Enter" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col l3 m4 s12 display_search">                    
                                            <select class="browser-default designation_h"  data-placeholder="Select" name="designation_h" id="designation_h">
                                                <option value=""  selected>Select</option>
                                                @foreach($designation as $des)
                                                    <option value="{{$des->designation_id}}">{{ucfirst($des->designation_name)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="possession_year" class="active">Designation </label>
                                            <div id="AddMoreFileId3" class="addbtn" style="right: -11px !important;" >
                                            <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                                            </div>
                                        </div>

                                        <div class="input-field col l3 m4 s12 display_search">                    
                                            <select class="  browser-default"  data-placeholder="Select" name="reporting_to_h" id="reporting_to_h">
                                                <option value=""  selected>Select</option>
                                                @foreach($company_hirarchy1 as $hei)
                                                    <option value="{{$hei->project_hirarchy_id}}">{{ucfirst($hei->name)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="possession_year" class="active">Reporting to </label>                           
                                        </div>

                                        <div class="input-field col l3 m4 s12 display_search">
                                            <label  class="active">Upload Photo : </label>
                                            <input type="file" name="photo_h"  id="photo_h" class="dropify" data-default-file="" style="padding-top: 14px;">
                                        </div>

                                        <div class="input-field col l3 m4 s12 display_search">                    
                                            <label for="moved_to_h active" class="active">Moved to </label>
                                            <input type="text" class="validate" name="moved_to_h" id="moved_to_h"  placeholder="Search" >
                                        </div>
                                    </div>
                                        
                                </div>
                                <div class="col l1">                          
                                <a href="javascript:void(0)" onClick="saveHierarchy()" class="btn-small  waves-effect waves-light green darken-1" >Save</a>
                            </div> 
                    </div>                    
                    <br>    
                    <div class="row">
                        <table class="bordered" id="heirarchy_table" style="font-size: unset;" style="font-size: unset;">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Name </th>                        
                        <th>Mobile No.</th>
                        <th>Whatsapp No.</th>
                        <th>Email Id</th>
                        <th>Designation</th>
                        <th>Reporting to</th>                        
                        <th>Photo</th>
                        <th>Moved to</th>
                        <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                       
                        @if(isset($getProjecthirarchyData))
                        @php $i=1; @endphp
                        @foreach($getProjecthirarchyData as $getProjecthirarchyData)
                           <tr id="hid_{{$getProjecthirarchyData->project_hirarchy_id}}">
                              <td id="n_{{$getProjecthirarchyData->project_hirarchy_id}}">{{$getProjecthirarchyData->name}}</td>
                              <td id="m_{{$getProjecthirarchyData->project_hirarchy_id}}">{{$getProjecthirarchyData->mobile_number}}</td>
                              <td id="w_{{$getProjecthirarchyData->project_hirarchy_id}}">{{$getProjecthirarchyData->whatsapp_number}}</td>
                              <td id="e_{{$getProjecthirarchyData->project_hirarchy_id}}">{{$getProjecthirarchyData->email_id}}</td>
                              <td id="d_{{$getProjecthirarchyData->project_hirarchy_id}}">{{$getProjecthirarchyData->designation_name}}</td>          
                              <td id="r_{{$getProjecthirarchyData->project_hirarchy_id}}">{{$getProjecthirarchyData->reports_to}}</td>                             
                              <td id="p_{{$getProjecthirarchyData->project_hirarchy_id}}"><a href="{{ asset('photo_h/'.$getProjecthirarchyData->photo) }}" target="_blank"> Photo</a></td>
                              <td id="mt_{{$getProjecthirarchyData->project_hirarchy_id}}">{{$getProjecthirarchyData->moved_to}}</td>
                              <td><a action="javascript:void(0)" onClick="alert({{$getProjecthirarchyData->project_hirarchy_id}})">View</a> | <a action="javascript:void(0)" onClick="updateHierarchy({{$getProjecthirarchyData->project_hirarchy_id}})">Edit</a> | <a action="javascript:void(0)" onClick="confirmDelHirarchy({{$getProjecthirarchyData->project_hirarchy_id}})">Delete</a></td>     
                        
                           </tr> 
                            @php $i++; @endphp
                            @endforeach
                        @endif
                            </tbody> 
                        </table> 

                    </div> 
                    </div>


            </div>
        </div>
        




   

        <div class="row">   <div class="input-field col l6 m6 s6 display_search">
         <div class="alert alert-danger print-error-msg" style="display:none;color:red">
           <ul></ul>
        </div>
      </div>  </div> 

      <div class="row" ><div class="input-field col l2 m2 s6 display_search">
      <div class="preloader-wrapper small active" id="loader1" style="display:none">
         <div class="spinner-layer spinner-green-only">
         <div class="circle-clipper left">
            <div class="circle"></div>
         </div><div class="gap-patch">
            <div class="circle"></div>
         </div><div class="circle-clipper right">
            <div class="circle"></div>
         </div>
         </div>
      </div>
   </div></div>

   <div class="row" id="submitButton" style="margin-left: 25px;">
   <input type="hidden" name="project_id2" id="project_id2" @if(isset($project_id)) value="{{$project_id}}" @endif>
   <input type="hidden" name="company_id2" id="company_id2" @if(isset($company_id)) value="{{$company_id}}" @endif>
                    <div class="input-field col l1 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action"> @if(isset($project_id) && !empty($project_id)) Update @else Save @endif</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <a href="/company-master" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
                    </div>    
                </div> 

        <br>
        
         
            

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->

<div id="modal93" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Update Pros & Cons</h5>
        <hr>        
        <form method="post" id="pros_update" >
        <input type="hidden" class="project_pro_cons_id1" id="project_pro_cons_id1" name="project_pro_cons_id">
            <div class="row" style="margin-right: 0rem !important">
                 <div class="input-field col l4 m4 s12" id="InputsWrapper2">
                     <label  class="active">Pros </label>
                     <input type="text" class="pros_of_complex1 validate" name="pros_of_complex" id="pros_of_complex1"    placeholder="Enter"  >                             
                  </div>            

                  <div class="input-field col l4 m4 s12" id="InputsWrapper2">
                     <label  class="active">Cons </label>
                     <input type="text" class="cons_of_complex1 validate" name="cons_of_complex" id="cons_of_complex1"    placeholder="Enter"  >                             
                  </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_update_pros" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="errors" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            
            <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                        <a href="javascript:void(0)" onClick="updateProsCons1()" class="btn-small  waves-effect waves-light green darken-1" >Update</a>

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search" style="margin-left: -15px;">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>
<div id="modal94" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Update Hierarchy</h5>
        <hr>        
        <form method="post" id="hirarchy_update" >
        <input type="hidden" class="project_hirarchy_id1" id="project_hirarchy_id1" name="project_hirarchy_id">

                    <div class="row" >
                        <div class="col l3 m3 s12">
                            <label><input type="radio" name="user_type1" id="user_typei1"  value="inhouse"   onChange="hideshowCN('inhouse')" ><span>INHOUSE</span></label>
                        </div>

                        <div class="col l3 m3 s12">
                            <label><input type="radio" name="user_type1" id="user_typeo1" onChange="hideshowCN('outsource')" value="outsource"  ><span>OUTSOURCE</span></label>
                        </div>

                        <div class="col l3 m3 s12" id='company_div' style="display:none">
                        <label  class="active">Company Name </label>
                            <input type="text" name="company_name11" id="company_name11" class="validate"  >
                        </div>                    

                    </div>
                    <br>
                    <div class="row">
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label for="cus_name active" class="dopr_down_holder_label active">Name 
                            </label>
                            <div  class="sub_val no-search">
                                <select  id="name_initial1" name="name_initial" class=" browser-default ">
                                @foreach($initials as $ini)
                                    <option value="{{$ini->initials_id}}">{{ucfirst($ini->initials_name)}}</option>
                                @endforeach
                                </select>
                            </div>
                            <input type="text" class="validate mobile_number" name="name_h1" id="name_h1"  placeholder="Enter"  >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper">
                            <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: </label>
                            <div  class="sub_val no-search">
                                <select  id="country_code1" name="country_code" class=" browser-default">
                                    @foreach($country_code as $cou)
                                    <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="text" class="validate mobile_number" name="mobile_number_h1" id="mobile_number_h1"  placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <div  class="sub_val no-search">
                                <select  id="country_code_w1" name="country_code_w" class=" browser-default">
                                @foreach($country_code as $cou)
                                    <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                @endforeach
                                </select>
                            </div>
                            <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: </label>
                            <input type="text" class="validate mobile_number" name="wap_number1" id="wap_number1"  placeholder="Enter" >
                            <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                            </div>
                        </div> 

                        <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                            <label for="primary_email active" class="active">Email Address: </label>
                            <input type="email" class="validate" name="primary_email_h1" id="primary_email_h1"  placeholder="Enter" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="browser-default designation_h"  data-placeholder="Select" name="designation_h1" id="designation_h1">
                                <option value=""  selected>Select</option>
                                @foreach($designation as $des)
                                    <option value="{{$des->designation_id}}">{{ucfirst($des->designation_name)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">Designation </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -11px !important;" >
                            <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                            </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="  browser-default"  data-placeholder="Select" name="reporting_to_h1" id="reporting_to_h1">
                                <option value=""  selected>Select</option>
                                @foreach($company_hirarchy1 as $hei)
                                    <option value="{{$hei->project_hirarchy_id}}">{{ucfirst($hei->name)}}</option>
                                @endforeach
                            </select>
                            <label for="possession_year" class="active">Reporting to </label>                           
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Upload Photo : </label>
                            <input type="file" name="photo_h1"  id="photo_h1" class="dropify" data-default-file="" style="padding-top: 14px;">
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <label for="moved_to_h active" class="active">Moved to </label>
                            <input type="text" class="validate" name="moved_to_h1" id="moved_to_h1"  placeholder="Search" >
                        </div>
                    </div> 
            
           
            <div class="alert alert-danger print-error-msg_update_pros" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="errors" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            
            <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                        <a href="javascript:void(0)" onClick="updateHierarchy1()" class="btn-small  waves-effect waves-light green darken-1" >Update</a>

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search" style="margin-left: -15px;">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>

<div class="modal" id="deleteProsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_pros" >
            <input type="hidden" class="project_pro_cons_id2" id="project_pro_cons_id2" name="project_pro_cons_id">
            <input type="hidden"  id="form_type" name="form_type" value="pros_cons">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deletePros()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>

<div class="modal" id="deleteHeirarchyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_hirarchy" >
            <input type="hidden" class="project_hirarchy_id2" id="project_hirarchy_id2" name="project_hirarchy_id">
            <input type="hidden"  id="form_type" name="form_type" value="hirarchy">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteHirarchy()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>

    <div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation </label>
                    <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="errors" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>

    <div id="modal10" class="modal">
            <div class="modal-content">
            <h5 style="text-align: center"> Flow Chart</h5>
            
            <hr>
            
            <html>
            <head>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                function cli() {
                    
              
                google.charts.load('current', {packages:["orgchart"]});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Name');
                data.addColumn('string', 'Manager');
                data.addColumn('string', 'ToolTip');
                data.addColumn('string', 'ToolTipw');
                $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
                var a = '123';
                $.ajax({
      url:"/add-company-master",
      method:"GET",
      data: {a:a},
        //   contentType: false,
        //   cache: false,
        //   processData: false,
        success:function(resp)
        {
            alert(resp)
            console.log(resp);
            // data.addRows(data1);
        },
        error:function(params) {
            console.log(params);
        }
        });
        data.addRows([
                    // For each orgchart box, provide the name, manager, and tooltip to show.
                        // [{'v':'Mike', 'f':'Mike'},
                        // '', ''],
                        // [{'v':'Jim', 'f':'Jim<div style="color:red; font-style:italic">Vice President</div>'},
                        // 'Mike', 'VP'],
                        ['Mike', '', '',''],
                        ['Jim', 'Mike', '',''],
                        ['Alice', 'Mike', '',''],
                    ['Prasanna', 'Alice', '',''],
                    
                    ['XYZ', 'Kiran', '',''],
                        ['Bob', 'Jim', 'Bob Sponge',''],		  
                        ['Carol', 'Bob', '',''],
                    ['Prasanna2', 'Carol', '',''],
                    ['Kiran', 'Prasanna', '',''],
                    ['ABC', 'Kiran', '','']
                    ]);

                    // Create the chart.
                    var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
                    // Draw the chart, setting the allowHtml option to true for the tooltips.
                    chart.draw(data, {'allowHtml':true});
                    }
                }
                    </script>
                    </head>
                <body>
                    <div id="chart_div"></div>
                </body>
                </html>

                <br>
                <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>
                </div>
                <br>
                
    </div>
<script>
    function hideshowCN(params) {
        if(params=='outsource'){
            $('#company_div').css('display','block');   
        }else{
            $('#company_div').css('display','none')   
        }
    }

</script>

<script>

function unitConvertor(argument) {
    var unitName = argument.value;
    console.log(unitName);
    
    var complex_land_parcel = $('#complex_land_parcel').val();
    var complex_land_parcel_con = $('#complex_land_parcel_con').val();
    if(complex_land_parcel_con == "" || complex_land_parcel != complex_land_parcel_con ){
        $('#complex_land_parcel_con').val(complex_land_parcel);
    }
    
    if(unitName == 'sq.mt'){
        var complex_land_parcel_con = $('#complex_land_parcel_con').val();
        var convert = complex_land_parcel_con / 10.764;
         $('#complex_land_parcel').val(Math.round(convert.toFixed(2)));
        // alert(convert.toFixed(2));
    }
    if(unitName == 'sq.ft'){
        var complex_land_parcel_con = $('#complex_land_parcel_con').val();
        // var convert = complex_land_parcel_con * 10.764;
         $('#complex_land_parcel').val(Math.round(complex_land_parcel_con));
    }

    if( unitName == 'acres'){
        var complex_land_parcel_con = $('#complex_land_parcel_con').val();
        var convert = complex_land_parcel_con / 43560;
        $('#complex_land_parcel').val(Math.round(convert.toFixed(4)));
    }


}


        function getCity(argument) {
            var stateID = argument.value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(stateID) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {stateID:stateID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        $('#city').empty();
                        $('#city').focus;
                        $('#city').append('<option value=""> Select City</option>'); 
                        $.each(data, function(key, value){
                        $('select[name="city"]').append('<option value="'+ value.city_id +'">' + value.city_name.charAt(0).toUpperCase()+value.city_name.slice(1) +'</option>');
                    });
                  }else{
                    $('#city').empty();
                  }
                  }
                });
            }else{
              $('#city').empty();
            }
        }

        function getlocation(argument) {
            var cityID = argument.value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(cityID) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {cityID:cityID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        $('#location').empty();
                        $('#location').focus;
                        $('#location').append('<option value=""> Select location</option>'); 
                        $.each(data, function(key, value){
                        $('select[name="location"]').append('<option value="'+ value.location_id +'">' + value.location_name.charAt(0).toUpperCase()+ value.location_name.slice(1)+ '</option>');
                    });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }
        
        function getsublocation(argument) {
            var SublocationID = argument.value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                        $('#location').empty();
                        $('#location').focus;
                        $('#location').append('<option value=""> Select sub_location</option>'); 
                        $.each(data, function(key, value){
                        $('select[name="location"]').append('<option value="'+ value.location_id +'">' + value.location_name.charAt(0).toUpperCase()+ value.location_name.slice(1)+ '</option>');
                    });
                  }else{
                    $('#sub_location').empty();
                  }
                  }
                });
            }else{
              $('#sub_location').empty();
            }
        }

  </script>
<script>

function saveProsCons() {
   var pros_of_complex = $('#pros_of_complex').val();
   var company_id = $('#company_id2').val();
   var project_id = $('#project_id2').val();
   if(pros_of_complex==''){
      $('#pros_of_complex').css('border-color','red');
      return false;
   }
   //    alert(pros_of_complex);

   // $(document).on('change', '#input-file-now', function(){
   //   var name = document.getElementById("input-file-now").files[0].name;
   var form_data = new FormData();
   var ext = name.split('.').pop().toLowerCase();

      form_data.append("pros_of_complex", document.getElementById('pros_of_complex').value);
      form_data.append("cons_of_complex", document.getElementById('cons_of_complex').value);
      form_data.append("form_type", 'pros_cons');
      form_data.append("company_id", company_id);
      form_data.append("project_id", project_id);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/add-project-master",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data); //return;

         // const obj = JSON.parse(data);

         // console.log(obj); //return;
         $('#project_id2').val(data.project_id);
         // if(typeof obj['company_id'] !== 'undefined'){
         //    $('#company_id2').val(obj['company_id']);
         // }
         //$('#uploaded_image').html(data);
         var html = '<tr id="pp_'+data.project_pro_cons_id+'">';
         html += '<td id="pid_'+data.project_pro_cons_id+'">' + data.project_pro_cons_id + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="p_'+data.project_pro_cons_id+'">' + data.pros + '</td>';// obj['company_pros'] +'</td>';
         html += '<td id="c_'+data.project_pro_cons_id+'">' + data.cons + '</td>';// obj['company_cons'] +'</td>';         
         html += "<td> <a href='javascript:void(0)' onClick='updateProsCons("+data.project_pro_cons_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelProsCons("+data.project_pro_cons_id+")'>Delete</a></td>";   
         html += '</tr>';
         $('#pros_cons_table').append(html);
         $('#pros_of_complex').val('');
         $('#cons_of_complex').val('');
      }
      });

}

function updateProsCons(param) {
    var project_pro_cons_id = param;
    var form_data = new FormData();
      form_data.append("form_type", 'pros_cons');
      form_data.append("project_pro_cons_id", project_pro_cons_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
    $.ajax({
         url:"/getDataProjet",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            var data  = data[0];
            console.log(data); //return;
            $('#project_pro_cons_id1').val(data.project_pro_cons_id);
            $('#pros_of_complex1').val(data.pros);
            $('#cons_of_complex1').val(data.cons);
            $('#modal93').modal('open');
      }
      });
    
}

function updateProsCons1() {
    var pros_of_complex = $('#pros_of_complex1').val();
    var cons_of_complex = $('#cons_of_complex1').val();
    var project_pro_cons_id = $('#project_pro_cons_id1').val();

    var form_data = new FormData();
     form_data.append("pros_of_complex", pros_of_complex);
     form_data.append("cons_of_complex", cons_of_complex);
     form_data.append("project_pro_cons_id", project_pro_cons_id);
     form_data.append("form_type", 'pros_cons');
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
     });

     $.ajax({
        url:"/updateProjectFormRecords",
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success:function(data)
        {      
            console.log(data);         
            var ppcid = data.project_pro_cons_id;           
            $('#pid_'+ppcid).html(data.project_pro_cons_id);
            $('#p_'+ppcid).html(data.data['pros']);
            $('#c_'+ppcid).html(data.data['cons']);      
            $('#modal93').modal('close');        
            
        }
    });
}

function confirmDelProsCons(params) {
    var project_pro_cons_id = params;
    $('.project_pro_cons_id2').val(project_pro_cons_id);   
    $('#deleteProsModal').modal('open');
}

function deletePros() {

    var url = 'deleteProjectFormRecords';
    var form = 'delete_pros';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
        // $("#award_table").load(window.location + " #award_table");    
        var ppid = data.project_pro_cons_id;
        $('#pp_'+ppid).remove();
        // $('#deleteAwardModal').hide();
        $('#deleteProsModal').modal('close');
        // console.log(data);return;
            
        }
    });   

}

function saveHierarchy() {
    var user_type = $('#user_type').val();
    var company_name1 = $('#company_name1').val();
    var name_h = $('#name_h').val();
    var mobile_number_h = $('#mobile_number_h').val();
    var wap_number = $('#wap_number').val();
    var primary_email_h = $('#primary_email_h').val();
    var designation_h = $('#designation_h').val();
    var reporting_to_h = $('#reporting_to_h').val();
    var photo_h = $('#photo_h').val();
    var moved_to_h = $('#moved_to_h').val();
    var project_id = $('#project_id2').val();
    var company_id = $('#company_id2').val();

    // alert(project_id);
    if( name_h == ''){
      $('#name_h').css('border-color','red');
      return false;
    }

    var form_data = new FormData();   
    form_data.append('user_type',user_type);
    form_data.append('company_name1',company_name1);
    form_data.append('name_h',name_h);
    form_data.append('mobile_number_h',mobile_number_h);
    form_data.append('wap_number',wap_number);
    form_data.append('primary_email_h',primary_email_h);
    form_data.append('designation_h',designation_h);
    form_data.append('reporting_to_h',reporting_to_h);
    form_data.append("photo_h", document.getElementById('photo_h').files[0]);
    form_data.append('moved_to_h',moved_to_h);
    form_data.append('project_id',project_id);
    form_data.append('company_id',company_id);
  
    form_data.append("form_type", 'hierarchy');
    $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/add-project-master",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data);
         var value = data.project_hirarchy_id; 
         var option = data.name;
         var newOption= new Option(option,value, true, false);
         $("#reporting_to_h").append(newOption);//

         var html = '<tr id="hid_'+data.project_hirarchy_id+'">';
        html += '<td id="n_'+data.project_hirarchy_id+'">' + data.name +'</td>'; // obj['name'] +'</td>';
        html += '<td id="m_'+data.project_hirarchy_id+'">' + data.mobile_number +'</td>'; // obj['mobile_number'] +'</td>';
        html += '<td id="w_'+data.project_hirarchy_id+'">' + data.whatsapp_number +'</td>'; // obj['whatsapp_number'] +'</td>';
        html += '<td id="e_'+data.project_hirarchy_id+'">' + data.email_id +'</td>'; // obj['email_id'] +'</td>';
        html += '<td id="d_'+data.project_hirarchy_id+'">' + data.designation_id +'</td>'; // obj['designation_id'] +'</td>';
        html += '<td id="r_'+data.project_hirarchy_id+'">' + data.reports_to +'</td>'; // obj['reports_to'] +'</td>';
        
        html += '<td id="p_'+data.project_hirarchy_id+'"><a href='+'photo_h/'+ data.photo +' target="_blank">Photo</a></td>'; // obj['photo'] +'</td>';
        html += '<td id="mt_'+data.project_hirarchy_id+'">' + data.moved_to +'</td>'; // obj['comments'] +'</td>';
        html += "<td> <a href='javascript:void(0)' onClick='alert(in Progress)' >View</a> |<a href='javascript:void(0)' onClick='updateHierarchy("+data.project_hirarchy_id+")' >Edit</a> |<a href='javascript:void(0)' onClick='confirmDelHirarchy("+data.project_hirarchy_id+")' >Delete</a></td>";
        html += '</tr>';
        // console.log(html);
        $('#heirarchy_table').append(html);

        $('#company_name1').val('');
        $('#name_h').val('');
        $('#mobile_number_h').val('');
        $('#wap_number').val('');
        $('#primary_email_h').val('');
        $('#designation_h').val('');
        $('#reporting_to_h').val('');
        $('#photo_h').val('');
        $('#moved_to_h').val('');
     }
      });   


}

function updateHierarchy(param) {
    var project_hirarchy_id = param;
    var form_data = new FormData();
      form_data.append("form_type", 'hierarchy');
      form_data.append("project_hirarchy_id", project_hirarchy_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
    $.ajax({
         url:"/getDataProjet",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            
            var data  = data[0];
            console.log(data);
            // $('#modal94').modal('open');
            var user_type = data.user_type;
            if(user_type=="inhouse"){                
                $("#user_typei1").prop("checked", true);
            }
            console.log(data.name_h);// return;
            $('#project_hirarchy_id1').val(data.project_hirarchy_id);
            $('#company_name1').val(data.company_name);
            $('#name_h1').val(data.name);
            $('#mobile_number_h1').val(data.mobile_number);
            $('#wap_number1').val(data.whatsapp_number);
            $('#primary_email_h1').val(data.email_id);
            $('#designation_h1').val(data.designation_id).trigger('change');
            // $('#designation_h1').val(data.designation_id);
            $('#reporting_to_h1').val(data.reports_to).trigger('change');
            // $('#reporting_to_h1').val(data.reporting_to);
            $('#moved_to_h1').val(data.moved_to);
            $('#modal94').modal('open');
      }
      });
    
}

function updateHierarchy1() {
    // var user_typei = $('#user_typei1').val();
    var project_hirarchy_id = $('#project_hirarchy_id1').val();
    var company_name1 = $('#company_name11').val();
    var name_h = $('#name_h1').val();
    var mobile_number_h = $('#mobile_number_h1').val();
    var wap_number = $('#wap_number1').val();
    var primary_email_h = $('#primary_email_h1').val();
    var designation_h = $('#designation_h1').val();
    var reporting_to_h = $('#reporting_to_h1').val();
    var photo_h = $('#photo_h1').val();
    var moved_to_h = $('#moved_to_h1').val();
    var project_id = $('#project_id2').val();
    var company_id = $('#company_id2').val();
    var user_type =   $('input[name="user_type1"]:checked').val();

    // alert(user_type);
    if( name_h == ''){
      $('#name_h1').css('border-color','red');
      return false;
    }

    var form_data = new FormData();   
    form_data.append('project_hirarchy_id',project_hirarchy_id);
    form_data.append('user_type',user_type);
    form_data.append('company_name1',company_name1);
    form_data.append('name_h',name_h);
    form_data.append('mobile_number_h',mobile_number_h);
    form_data.append('wap_number',wap_number);
    form_data.append('primary_email_h',primary_email_h);
    form_data.append('designation_h',designation_h);
    form_data.append('reporting_to_h',reporting_to_h);
    form_data.append("photo_h", document.getElementById('photo_h1').files[0]);
    form_data.append('moved_to_h',moved_to_h);
    form_data.append('project_id',project_id);
    form_data.append('company_id',company_id);
  
    form_data.append("form_type", 'hierarchy');
    $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/updateProjectFormRecords",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data);
        //  console.log('#n_'+data.project_hirarchy_id);
        //  var value = data.project_hirarchy_id; 
        //  var option = data.data['name'];
        //  var newOption= new Option(option,value, true, false);
        //  $("#reporting_to_h").append(newOption);//
        $('#n_'+data.project_hirarchy_id).html(data.data['name']);
        $('#m_'+data.project_hirarchy_id).html(data.data['mobile_number']);
        $('#w_'+data.project_hirarchy_id).html(data.data['whatsapp_number']);
        $('#e_'+data.project_hirarchy_id).html(data.data['email_id']);
        $('#d_'+data.project_hirarchy_id).html(data.data['designation_name']);
        $('#r_'+data.project_hirarchy_id).html(data.data['reports_to']);
        $('#p_'+data.project_hirarchy_id).html("<a href='photo_h/"+data.data['photo']+"' target='_blank' > Photo </a> ");
        $('#mt_'+data.project_hirarchy_id).html(data.data['moved_to']);   
        $('#modal94').modal('close'); 

      }
      });   
}

function confirmDelHirarchy(params) {
    var project_hirarchy_id = params;
    $('.project_hirarchy_id2').val(project_hirarchy_id);   
    $('#deleteHeirarchyModal').modal('open');
}

function deleteHirarchy() {
    var url = 'deleteProjectFormRecords';
    var form = 'delete_hirarchy';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
        // $("#award_table").load(window.location + " #award_table");    
        var hid = data.project_hirarchy_id;
        $('#hid_'+hid).remove();
        // $('#deleteAwardModal').hide();
        $('#deleteHeirarchyModal').modal('close');
        // console.log(data);return;
            
        }
    }); 
}

function add_designation_form(){
    var url = 'addDesignationCompanyMaster';
    var form = 'add_designation_form';
    var err = 'print-error-msg_add_designation';
     var desig = $('#add_designation').val();
    // alert(desig);
    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:  {add_designation:desig}      ,             
            //  $('#'+form).serialize() ,
        
        success: function(data) {
            // console.log(data); return;
            var a = data.a;
            var option = a.option;
            var value = a.value; 

            var newOption= new Option(option,value, true, false);
            // Append it to the select
            $(".designation_h").append(newOption);//]].trigger('change');
            // $('#designation_h').append($('<option>').val(optionValue).text(optionText));


            // console.log(data.success);return;
            if($.isEmptyObject(data.error)){
               $('.success1').html(data.success);
               //  alert(data.success);
               //  location.reload();
            }else{
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
    // execute(url,form,err);
}


function apply_css1(skip,attr, val=''){   
    var id =returnColArray1();  
   //  alert(id); return;
    id.forEach(function(entry) {
        
        if(entry==skip){
            // alert(11)
            $('#'+skip).css(attr,'#3FCF68','!important');
            // $('#'+skip).css('border-color','#3FCF68','!important');
            // $('#'+skip).attr('style', 'background-color: orange !important');
        }else{            
            $('#'+entry).css(attr,val);
            // $('#'+entry).css('border-color','#38294ee','!important');
            action_to_hide();            
            
        }        
    });
}

function hide_n_show1(skip){
    var id = collapsible_body_ids1();
    collapsible_body_ids1().forEach(function(entry) {
        // console.log(id);
        if(entry==skip){
            // alert(skip);
            var x = document.getElementById(skip);  
            if(skip != 'amenities_div') {
                $('#'+skip).css('background-color','rgb(234 233 230)');
            }
                  
            if (x.style.display === "none") {
                
                x.style.display = "block";
            } else {
                
                var s = skip.replace("_div", "");                
                x.style.display = "none";
                $('#col_'+s).css('background-color','white');
                // alert(x.style.backgroundColor);
                // alert(skip);
                // $('#col_'+skip).css('background-color','white');
                // $('#col_lead_info').css('background-color','white',"!important");
                // x.style.backgroundColor = "lightblue";
            }
        }else{          
            // alert(entry)
            $('#'+entry).hide();
        }
        
    });
}


function returnColArray1(){
    var a = Array('col_address','col_amenities','col_landparcel','col_proscons','col_desc','col_images','col_heirarchy');
    return a;
}

function collapsible_body_ids1(){
    var b = Array('address_div','amenities_div','landaprcel_div','proscons_div','desc_div','images_div','heirarchy_div');
    return b;
}

function ShowHideaddress() {
             apply_css1('col_address','background-color',''); 
            hide_n_show1("address_div");    
            // var x = document.getElementById("address_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_address').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_address').css('background-color','white');        
            // }
         }

         function ShowHideamenities() {
            apply_css1('col_amenities','background-color',''); 
            hide_n_show1("amenities_div");  
            // var x = document.getElementById("amenities_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_amenities').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_amenities').css('background-color','white');        
            // }
         }

         function ShowHidelandparcel() {
            apply_css1('col_landparcel','background-color',''); 
            hide_n_show1("landaprcel_div");  
            // var x = document.getElementById("landaprcel_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_landparcel').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_landparcel').css('background-color','white');        
            // }
         }

         function ShowHideproscons() {
            apply_css1('col_proscons','background-color',''); 
            hide_n_show1("proscons_div");
            // var x = document.getElementById("proscons_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_proscons').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_proscons').css('background-color','white');        
            // }
         }

         function ShowHidedesc() {
            apply_css1('col_desc','background-color',''); 
            hide_n_show1("desc_div");
            // var x = document.getElementById("desc_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_desc').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_desc').css('background-color','white');        
            // }
         }

         function ShowHideimages() {
            apply_css1('col_images','background-color',''); 
            hide_n_show1("images_div");
            // var x = document.getElementById("images_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_images').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_images').css('background-color','white');        
            // }
         }

         function ShowHideheirarchy() {
            apply_css1('col_heirarchy','background-color',''); 
            hide_n_show1("heirarchy_div");
            // var x = document.getElementById("heirarchy_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_heirarchy').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_heirarchy').css('background-color','white');        
            // }
         }


</script> 
        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     