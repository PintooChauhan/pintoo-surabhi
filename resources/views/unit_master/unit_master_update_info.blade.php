<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-results__options{
    line-height: 0px;
   }

.-container--default .-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.-container--open .-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->

         <style>
            .building_name{
                padding-bottom: 5px;
                background-color: green;
                color: white;
                padding-top: 5px;
                padding-left: 5px;
            }
         </style>

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

       
            

        <div class="row " style="margin-top: -30px;font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">UNIT MASTER</span>
                </div><br>
                <?php 
                //print_r($unit_code); die();
                    if($floor_no <= 0){
                        $unit_code = abs($unit_code)/100;
                        $unit_code = str_pad($unit_code,3, '0', STR_PAD_LEFT);
                    }else{
                        $unit_code = $unit_code;
                    }
                
                ?>
                <h5 style="text-align: center;font-size: medium;color:red"><span>(You have selected unit no : {{$unit_code}} . On Floor No: @if( isset($floor_no) ) {{ $floor_no }} @endif )</span></h5>
                <br>
                <script>
                    function unitView(wingId) {
                        // alert(wingId)

                        $.ajax({
                        type:'GET',
                        url: '/append_unit_structure',
                        data: {wingId:wingId},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html = response;
                            $('#unitView').css('display','block');
                            $('#unitView').html(html);
                            $('#unitSideView').html('');
                            
                            },
                            error: function(response){
                                console.log(response);
                                    // $('#image-input-error').text(response.responseJSON.errors.file);
                            }
                        });
                    }

                    function sideunitView(params1,params2) {
                       var params1 = params1;
                       var params2 = params2;
                       
                       $.ajax({
                        type:'GET',
                        url: '/append_unit_structure_side_view',
                        data: {params1:params1,params2:params2},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html = response;
                            $('#unitSideView').html(html);
                            $("#panel_table").reset();
                        
                            },
                            error: function(response){
                                console.log(response);
                                    // $('#image-input-error').text(response.responseJSON.errors.file);
                            }
                        });
                       

                    }

                    
                </script>

            <div class="row">  
                        <div class="col l3 m3 s12">   Wing Name :  {{$wingName}}</div>
                        <div class="col l3 m3 s12">   Building  Name :  {{$building_name}}</div>
                        <div class="col l6 m6 s12"> Comment : {{$wingComment}}</div>
            </div><br>
           
            <form method="post" id="add_unit_master_form" enctype="multipart/form-data">  @csrf
              <input type="hidden" name="unit_id" id="unit_id" value="{{$unit_id}}">
              <input type="hidden" name="wing_id" id=wing_id value="{{$wing_id}} ">
                <div class="row">   

                <script>
                    function change_unit_type(params) {
                        var unit_type = params.value;

                        // alert(unit_type);
                        if(unit_type == 'commercial'){
                            $("#configuration").prop("disabled", true);
                            $("#configuration_size1").prop("disabled", true);
                            $("#balcony").prop("disabled", true);       
                            $('#unit_type_com_op').css('display','block');
                            $('#unit_type_res_op').css('display','none');      
                            $('#configuration').val('').trigger('change');
                            $('#configuration_size1').val('').trigger('change');
                            $('#balcony').val('').trigger('change');
                            $('#unit_type_com').val('').trigger('change');

                            $('#carpet_area').val('');
                            $('#build_up_area').val('');

                            $('#rera_carpet_arera').val('');
                            $('#mofa_carpet_area').val('');
                            $('#direction1').val('').trigger('change');
                            $('#unit_view').val('');
                            $('#show_unit').val('').trigger('change');
                            
                            
                        }else{
                            $("#configuration").prop("disabled", false);
                            $("#configuration_size1").prop("disabled", false);
                            $("#balcony").prop("disabled", false);                           
                            $('#unit_type_com_op').css('display',"none");
                            $('#unit_type_res_op').css('display',"block");
                            $('#unit_type_res').val('').trigger('change');

                            $('#carpet_area').val('');
                            $('#build_up_area').val('');

                            $('#rera_carpet_arera').val('');
                            $('#mofa_carpet_area').val('');
                            $('#direction1').val('').trigger('change');
                            $('#unit_view').val('');
                            $('#show_unit').val('').trigger('change');
                        }

                    }

                function hide_all_info(params){
                        
                    var refugee_unit = params.value;

                      if(refugee_unit=='t'){
                            $("#show_unit").prop("disabled", true);
                            $("#unit_available").prop("disabled", true);
                            $("#status").prop("disabled", true);       
                            
                            $('#show_unit').val('n').trigger('change'); 
                            $('#unit_available').val('f').trigger('change'); 
                            
                        }else{
                            $("#show_unit").prop("disabled", false);
                            $("#unit_available").prop("disabled", false);
                            $("#status").prop("disabled", false);       
                            $('#show_unit').val('t').trigger('change');      
                            $('#unit_available').val('t').trigger('change');                
                        }
                }

                </script>
                       

                        <div class="input-field col l3 m3 s12 ">
                            <select class="  browser-default" id="unit_type"  data-placeholder="Select" name="unit_types" onChange="change_unit_type(this)">
                                <option value="" disabled selected>Select</option>
                                <option value="residencial"  @if($unit_type=='residencial') selected @else selected @endif>Residential</option>
                                <option value="commercial" @if($unit_type=='commercial') selected @endif>Commercial</option>
                            </select>
                            <label for="property_seen" class="active">Unit Type</label>
                        </div>
                         
                        @if(isset($unit_type))
                            @if($unit_type =='residencial' )
                                <div class="input-field col l3 m3 s12 " id="unit_type_res_op" >
                                    <select class="  browser-default" id="unit_type_res"  data-placeholder="Select" name="unit_type_res" >
                                        <option value="" disabled selected>Select</option>
                                        @foreach($dd_unit_type_residential as $dd_unit_type_residential)
                                            <option value="{{ $dd_unit_type_residential->unit_type_residential_id }}" @if(isset($unit_type_id)) @if( $dd_unit_type_residential->unit_type_residential_id == $unit_type_id) selected @endif @elseif($dd_unit_type_residential->unit_type_residential_id==1) selected @endif>{{ ucwords($dd_unit_type_residential->unit_type_residential_name) }}</option>
                                        @endforeach
                                    </select>
                                    <label for="property_seen" class="active">Unit Type Residential</label>
                                </div>

                                <div class="input-field col l3 m3 s12 " id="unit_type_com_op" style="display:none" >
                                    <select class="  browser-default" id="unit_type_com"  data-placeholder="Select" name="unit_type_com" >
                                        <option value="" disabled selected>Select</option>
                                        @foreach($dd_unit_type_commercial as $dd_unit_type_commercial)
                                            <option value="{{ $dd_unit_type_commercial->unit_type_commercial_id }}" @if(isset($unit_type_id)) @if( $dd_unit_type_commercial->unit_type_commercial_id == $unit_type_id) selected @endif @endif>{{ ucwords($dd_unit_type_commercial->unit_type_commercial_name) }}</option>
                                        @endforeach
                                    </select>
                                    <label for="property_seen" class="active">Unit Type Commercial</label>
                                </div>

                                
                            @endif

                            @if($unit_type =='commercial')
                                <div class="input-field col l3 m3 s12 " id="unit_type_com_op"  >
                                    <select class="  browser-default" id="unit_type_com"  data-placeholder="Select" name="unit_type_com" >
                                        <option value="" disabled selected>Select</option>
                                        @foreach($dd_unit_type_commercial as $dd_unit_type_commercial)
                                            <option value="{{ $dd_unit_type_commercial->unit_type_commercial_id }}" @if(isset($unit_type_id)) @if( $dd_unit_type_commercial->unit_type_commercial_id == $unit_type_id) selected @endif @endif>{{ ucwords($dd_unit_type_commercial->unit_type_commercial_name) }}</option>
                                        @endforeach
                                    </select>
                                    <label for="property_seen" class="active">Unit Type Commercial</label>
                                </div>

                                <div class="input-field col l3 m3 s12 " id="unit_type_res_op" style="display:none" >
                                    <select class="  browser-default" id="unit_type_res"  data-placeholder="Select" name="unit_type_res" >
                                        <option value="" disabled selected>Select</option>
                                        @foreach($dd_unit_type_residential as $dd_unit_type_residential)
                                            <option value="{{ $dd_unit_type_residential->unit_type_residential_id }}" @if(isset($unit_type_id)) @if( $dd_unit_type_residential->unit_type_residential_id == $unit_type_id) selected @endif @elseif($dd_unit_type_residential->unit_type_residential_id==1) selected @endif>{{ ucwords($dd_unit_type_residential->unit_type_residential_name) }}</option>
                                        @endforeach
                                    </select>
                                    <label for="property_seen" class="active">Unit Type Residential</label>
                                </div>
                            @endif
                        @else
                                <div class="input-field col l3 m3 s12 " id="unit_type_res_op" >
                                    <select class="  browser-default" id="unit_type_res"  data-placeholder="Select" name="unit_type_res" >
                                        <option value="" disabled selected>Select</option>
                                        @foreach($dd_unit_type_residential as $dd_unit_type_residential)
                                            <option value="{{ $dd_unit_type_residential->unit_type_residential_id }}" @if(isset($unit_type_id)) @if( $dd_unit_type_residential->unit_type_residential_id == $unit_type_id) selected @endif @elseif($dd_unit_type_residential->unit_type_residential_id==1) selected @endif>{{ ucwords($dd_unit_type_residential->unit_type_residential_name) }}</option>
                                        @endforeach
                                    </select>
                                    <label for="property_seen" class="active">Unit Type Residential</label>
                                </div>

                                <div class="input-field col l3 m3 s12 " id="unit_type_com_op" style="display:none" >
                                    <select class="  browser-default" id="unit_type_com"  data-placeholder="Select" name="unit_type_com" >
                                        <option value="" disabled selected>Select</option>
                                        @foreach($dd_unit_type_commercial as $dd_unit_type_commercial)
                                            <option value="{{ $dd_unit_type_commercial->unit_type_commercial_id }}" @if(isset($unit_type_id)) @if( $dd_unit_type_commercial->unit_type_commercial_id == $unit_type_id) selected @endif @endif>{{ ucwords($dd_unit_type_commercial->unit_type_commercial_name) }}</option>
                                        @endforeach
                                    </select>
                                    <label for="property_seen" class="active">Unit Type Commercial</label>
                                </div>
                        @endif



                        
                       
                        
                        <div class="input-field col l3 m3 s12 display_search">
                            <label for="lead_assign" class="active">Unit No </label>
                            <input type="text" class="validate" name="custom_unit_code" id="custom_unit_code"  value="@if( !empty($custom_unit_code) )  {{$custom_unit_code}} @else {{$unit_code}} @endif"   placeholder="Enter" >                        
                            <input type="hidden" class="validate" name="unit_no" id="unit_no" readonly value="{{$unit_code}}"   placeholder="Enter" >                        
                        </div>

                        <!-- <div class="input-field col l3 m3 s12 display_search">
                            <label for="lead_assign" class="active">Floor No.: </label>
                            <input type="text" class="validate" name="floor_no" id="floor_no" readonly value="@if( isset($floor_no) ) {{ $floor_no }} @endif"  placeholder="Enter" >                        
                        </div>    -->

                        <div class="input-field col l3 m3 s12 display_search">
                            <select class="validate  browser-default" id="configuration"  name="configuration" @if($unit_type=='commercial') disabled @endif>
                                <option value="" >Select</option>
                                @foreach($configuration as $configuration)
                                      <option value="{{ $configuration->configuration_id }}" @if(isset($configuration_id))  @if($configuration->configuration_id == $configuration_id  ) selected  @endif  @endif> {{ ucfirst($configuration->configuration_name) }}</option>
                                @endforeach      
                                    </select>
                            <label class="active">Configuration</label>               
                        </div>

                        
                        
                    </div>
                    <br>
                    <div class="row">

                       

                        <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                            <ul class="collapsible" style="margin-top: -2px;width: 321px">
                            <li onClick="ShowHidearea()">
                            <div class="collapsible-header"  id="col_area" style="border: 1px solid #8294ee;">Area</div>               
                            </li>                                        
                            </ul>
                        </div>

                        <div class="input-field col l3 m3 s12 display_search">
                            <select class="validate  browser-default configuration_size1" id="configuration_size1"  name="configuration_size" @if($unit_type=='commercial') disabled @endif>
                                <option value="" >Select</option>
                                @foreach($configuration_size as $configuration_size1)
                                <option value="{{ $configuration_size1->configuration_size_id }}" @if(isset($configuration_size11))  @if($configuration_size1->configuration_size_id == $configuration_size11  ) selected  @endif  @endif> {{ ucfirst($configuration_size1->configuration_size) }}</option>
                                @endforeach  
                                    </select>
                            <label class="active">Configuration Size</label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -11px !important;" >
                                            <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                                            </div>               
                        </div>

                        <div class="input-field col l3 m3 s12 display_search">
                            <select class="validate  browser-default" id="direction1"  name="direction_id">
                                <option value="" >Select</option>
                                @foreach($direction as $dir)
                                    <option value="{{$dir->direction_id	}}" @if($dir->direction_id == $direction_id)  selected @endif>{{ucfirst($dir->direction)}}</option>
                                 @endforeach
                                    </select>
                            <label class="active">Direction</label>               
                        </div>

                        <div class="input-field col l3 m3 s12 display_search">
                            <select class="validate  browser-default" id="balcony"  name="balcony" @if($unit_type=='commercial') disabled @endif>
                                <option value="" >Select</option>
                                <option value="t"  @if($balcony=='t') selected @endif>Yes</option>
                                 <option value="n" @if($balcony=='n') selected @endif>No</option>
                                    </select>
                            <label class="active">Balcony</label>               
                        </div>
                    </div>
                    <div class="row" id="area_div" style="display:none">
                      <br>

                      <div class="input-field col l3 m4 s12" id="InputsWrapper">
                            <div class="row">
                                <div class="input-field col m8 s8" style="padding: 0 10px;">
                                <input type="text" class="input_select_size" name="carpet_area" id="carpet_area"  placeholder="Enter" @if(isset($carpet_area)) value="{{$carpet_area}}" @endif>
                                    <label> Carpet Area
                                </div>
                                <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                    <select  id="carpet_area" name="carpet_area1" class="browser-default ">
                                    @foreach($unit as $un)
                                    <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>

                        
                        <div class="input-field col l3 m4 s12" id="InputsWrapper">
                            <div class="row">
                                <div class="input-field col m8 s8" style="padding: 0 10px;">
                                <input type="text" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area"  placeholder="Enter" @if(isset($mofa_carpet_area)) value="{{$mofa_carpet_area}}" @endif>
                                    <label> RERA CARPET AREA                             
                                </div>
                                <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                    <select  id="mofa_carpet_area" name="mofa_carpet_area1" class="browser-default ">
                                    @foreach($unit as $un)
                                    <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div> 

                        
                        <div class="input-field col l3 m3 s12 display_search">
                            <label for="lead_assign" class="active">Comment </label>
                            <input type="text" class="validate" name="rera_carpet_area" id="rera_carpet_area"  value="@if( !empty($rera_carpet_area) )  {{$rera_carpet_area}}  @endif"   placeholder="Enter" >                        
                        </div>


                        <div class="input-field col l3 m4 s12" id="InputsWrapper">
                            <div class="row">
                                <div class="input-field col m8 s8" style="padding: 0 10px;">
                                <input type="text" class="input_select_size" name="build_up_area" id="build_up_area"  placeholder="Enter" @if(isset($build_up_area)) value="{{$build_up_area}}" @endif>
                                    <label> Build Up Area
                                </div>
                                <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                    <select  id="build_up_area" name="build_up_area1" class="browser-default ">
                                    @foreach($unit as $un)
                                    <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>   
                      
                        
                        

                    </div>
                    <br>
                    <div class="row">

                    @if( $wingtotal_floors > 7 )    
                    <div class="input-field col l3 m3 s12 ">
                        <select class=" validate   browser-default"  id="refugee_unit"  data-placeholder="Select" name="refugee_unit"  onChange="hide_all_info(this)" ><!-- onchange="" -->
                            <!-- <option value="" disabled selected>Select</option> -->
                            <option value="t" @if(isset($refugee_unit) && !empty($refugee_unit)) @if($refugee_unit == 't') selected @endif @endif >Yes</option>
                            <option value="f" @if(isset($refugee_unit) && !empty($refugee_unit)) @if($refugee_unit == 'f') selected  @endif @else selected @endif>No</option>
                        </select>
                        <label for="property_seen1" class="active">Refugee Unit</label>
                    </div>
                    @endif
                        
                         
                        
                        <div class="input-field col l3 m3 s12 display_search">
                            <label for="lead_assign" class="active">    Unit View </label>
                            <input type="text" class="validate" name="unit_view" id="unit_view"   value="@if( isset($unit_view) ) {{ $unit_view }} @endif" placeholder="Enter">                        
                        </div>
                        

                        @if( $building_status != 1 )    
                        <div class="input-field col l3 m3 s12 display_search">
                            <!-- <label for="lead_assign" class="active"> Show Unit  </label>
                            <input type="text" class="validate" name="show_unit" id="show_unit"   value="@if( isset($unit_view) ) {{ $unit_view }} @endif" placeholder="Enter">                         -->
                            
                            <select class="validate  browser-default" id="show_unit"  name="show_unit" @if(isset($refugee_unit)) @if($refugee_unit=='t') disabled  @endif @endif>
                                <option value="" >Select</option>
                                <option value="t" @if($show_unit=='t') selected @endif >Yes</option>
                                 <option value="n" @if(isset($refugee_unit)) selected @if($refugee_unit=='t') disabled  @endif  @elseif($show_unit=='n') selected @else selected  @endif>No</option>
                                    </select>
                            <label class="active">Show Unit</label>  
                        </div>
                        @endif
                        <div class="input-field col l3 m3 s12 ">
                        <select class="validate   browser-default"  id="unit_available"  data-placeholder="Select" name="unit_available" @if(isset($refugee_unit)) @if($refugee_unit=='t') disabled  @endif @endif>
                            <!-- <option value="" disabled selected>Select</option> -->
                            <option value="t" @if(isset($refugee_unit))  @if( $refugee_unit =='f' ) selected  @endif @elseif(isset($unit_available)  && !empty($unit_available) ) @if($unit_available == "t")  selected @endif  @endif  >Yes</option>
                            <option value="f" @if(isset($refugee_unit))  @if( $refugee_unit =='t' ) disabled  @endif @if(isset($unit_available)  && !empty($unit_available) ) @if($unit_available == "f")  selected @endif  @endif @endif>No</option>
                             
                                

                        </select>
                        <label for="property_seen" class="active">Unit Available  </label>
                    </div>
                        
                    



                    </div></br>
                    <div class="row">

                    <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
                        <ul class="collapsible" style="margin-top: -2px;width: 321px">
                        <li onClick="panel_opening()">
                        <div class="collapsible-header"  id="col_panel" style="border: 1px solid #8294ee;">Unit Detailing </div>               
                        </li>                                        
                        </ul>
                    </div>
                    
                   
                        
                </div>


            
                <div class='row' id='panel_table' style="display:none" >
                <br>
                    
                    <div class='row'>
                        <div class="col l2 m2">Room   <a href="#modal19" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="font-size: 16px;color: red !important;font-weight: 600"> +</a>  </div>
                        <div class="col l2 m2">Dimension</div>
                        <div class="col l2 m2">Direction</div>
                        <div class="col l2 m2">View</div>          
                        <div class="col l2 m2">Comment</div>
                    </div> 

                    <div class='row'>
                    <input type="hidden" name="bedroom_id" id="bedroom_id">
                        <div class="col l2 m2">
                            <select class="validate  browser-default room" id="room"  name="room">
                                <option value="" >Select</option>
                                @foreach($room as $room)
                                <option value="{{  $room->room_id }}">{{ ucfirst($room->room_name) }}</option>
                                @endforeach
                            </select>   
                            
                        </div>
                        <div class="col l2 m2">
                            <input type="text" class="validate" name="dimension" id="dimension" >
                        </div>
                        <div class="col l2 m2">
                            <select class="validate  browser-default" id="direction"  name="direction">
                                <option value="" >Select</option>
                                @foreach($direction1 as $direction)
                                <option value="{{  $direction->direction_id }}">{{ ucfirst($direction->direction) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col l2 m2">
                            <input type="text" class="validate" name="view" id="view" >
                        </div>
                        
                        
                        <div class="col l2 m2">
                            <input type="text" class="validate" name="comment" id="comment" >
                        </div>
                        
                        <div class="col l2 m2 s12" id="InputsWrapper2">
                                <div class="row" >
                                <div class="input-field col l2 m2 s6 display_search">
                                <div class="preloader-wrapper small active" id="loader1" style="display:none">
                                <div class="spinner-layer spinner-green-only">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div><div class="gap-patch">
                                    <div class="circle"></div>
                                </div><div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                                </div>
                                </div>
                            </div></div>

                            <div class="row" id="submitUnit">
                                <div class="col l4 m4">
                                    <a href="javascript:void(0)" onClick="saveUnit()" class=" save_unit btn-small  waves-effect waves-light green darken-1 " id="save_unit">Save</a>
                                </div>
                                <div class="col l3 m3" style="margin-left: 33px;">
                                        <a href="javascript:void(0)" onClick="clear_unit()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_unit"><i class="material-icons dp48">clear</i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <br>
                    <div class="row">
                        <table class="bordered" id="unit_table" style="font-size: unset;">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Sr No. </th>
                        <th>Room</th>                        
                        <th>Dimension</th>
                        <th>Direction</th>
                        <th>View</th>
                        <th>Comment</th>
                        <th>Last Updated Date</th>
                        <th>Action</th>               
                        </tr>
                        </thead>
                        <tbody>
                            <!-- getProjectProsConsData -->
                           @php $incPros1= count($getUnitBedroomData); @endphp
                             @if(isset($getUnitBedroomData))
                             @php $i3=count($getUnitBedroomData); @endphp
                                    @foreach($getUnitBedroomData as $getUnitBedroomData) 
                                    <tr id="bid_{{$getUnitBedroomData->bedroom_id}}">
                                        <td >@php $incPros= $i3--; @endphp {{$incPros}}</td>
                                        <td id="r_{{$getUnitBedroomData->bedroom_id}}">{{$getUnitBedroomData->room_name}}</td>                                       
                                        <td id="di_{{$getUnitBedroomData->bedroom_id}}">{{$getUnitBedroomData->dimension}}</td>
                                        <td id="d_{{$getUnitBedroomData->bedroom_id}}">{{$getUnitBedroomData->direction}}</td>
                                        <td id="v_{{$getUnitBedroomData->bedroom_id}}">{{$getUnitBedroomData->view}}</td>
                                        <td id="c_{{$getUnitBedroomData->bedroom_id}}">{{$getUnitBedroomData->comment}}</td>
                                        <td id="ld_{{$getUnitBedroomData->bedroom_id}}">{{$getUnitBedroomData->created_date}}</td>
                                        <td>
                                        <a href="javascript:void(0)"  class="waves-effect waves-light" onClick="updateUnit('{{$getUnitBedroomData->bedroom_id}}')"  >Edit</a> |
                                        <a href='javascript:void(0)' class="waves-effect waves-light"   onClick="confirmDelUnit('{{$getUnitBedroomData->bedroom_id}}')" >Delete</a>
                                        </td>
                                    </tr>    
                                    @endforeach
                                @endif
                     
                        </tbody>
                    </table>
                    <input type="hidden" id="incPros" value="{{$incPros1}}">
              </div>
                   
                    <div id="display_inputs_Challenge_panel"></div> 

                </div>

            </div>   


            </div>

            <div class="row">   <div class="input-field col l6 m6 s6 display_search">
                <div class="alert alert-danger print-error-msg" style="display:none;color:red">
                <ul></ul>
                </div>
            </div>  </div> 

      <div class="row">
         <div class="input-field col l2 m2 s6 display_search">
            <button class="btn-small  waves-effect waves-light green darken-1" type="submit" name="action">Save</button>                        
         </div>
         <div class="input-field col l2 m2 s6 display_search">
         <a href="/company-master" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
         </div>
      </div>
    </form> 
                
           
           <script>
            function setModalValues(value){
                $('#floor_no').val(value);
            }
           
           </script>
          
    <div id="modal19" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Room</h5>
        <hr>
        
        <form method="post" id="add_room_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active"> Room </label>
                    <input type="text" class="validate" name="add_room" id="add_room"   placeholder="Enter">
                    <span class="add_room_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_room" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="1" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_room_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>


         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


<div class="modal" id="deleteUnitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_unit" >
            <input type="hidden" class="bedroom_id2" id="bedroom_id2" name="bedroom_id">
            <input type="hidden"  id="form_type" value="single_record" name="form_type">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteUnit()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>     


<div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Configuration Size</h5>
        <hr>
        
        <form method="post" id="add_configuration_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active"> Configuration Size </label>
                    <input type="text" class="validate" name="add_configuration" id="add_configuration"   placeholder="Enter">
                    <span class="add_configuration_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_configuration" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="1" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_configuration_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>

<script>

    function add_room_form() {
        var url = 'add_room';
        var form = 'add_room_form';
        var err = 'print-error-msg_add_room';
        var config = $('#add_room').val();
        
        $.ajax(  {
            url:"/"+url,
            type:"POST",
            data:  {add_room:config}      ,             
                //  $('#'+form).serialize() ,
            
            success: function(data) {
                console.log(data);// return;
                if(typeof data.a != 'undefined'){
                var a = data.a;
                var option = a.option;
                var value = a.value; 

                var newOption= new Option(option,value, true, false);
                // Append it to the select
                $("#room").prepend(newOption);//]].trigger('change');
                // $('#designation_h').append($('<option>').val(optionValue).text(optionText));

                }   
                // console.log(data.success);return;
                if($.isEmptyObject(data.error)){
                   
                // $('.success1').html(data.success);
                // alert(data.success);
                $('#add_room').val('');
                $('#modal19').modal('close');
                //  location.reload();
                }else{
                    alert('Enter Room'); return;
                    
                }
            }
        }); 
    }

    function add_configuration_form() {
        var url = 'add_configuration';
        var form = 'add_configuration_form';
        var err = 'print-error-msg_add_configuration';
        var config = $('#add_configuration').val();
        
        $.ajax(  {
            url:"/"+url,
            type:"POST",
            data:  {add_configuration:config}      ,             
                //  $('#'+form).serialize() ,
            
            success: function(data) {
                console.log(data.a);// return;
                if(typeof data.a != 'undefined'){
                var a = data.a;
                var option = a.option;
                var value = a.value; 

                var newOption= new Option(option,value, true, false);
                // Append it to the select
                $("#configuration_size1").append(newOption);//]].trigger('change');
                // $('#designation_h').append($('<option>').val(optionValue).text(optionText));

                }   
                // console.log(data.success);return;
                if($.isEmptyObject(data.error)){
                   
                // $('.success1').html(data.success);
                // alert(data.success);
                $('#add_configuration').val('');
                $('#modal9').modal('close');
                //  location.reload();
                }else{
                    alert('Enter Configuration Size'); return;
                    
                }
            }
        });    
    
    }

    function panel_opening(){   
        var x = document.getElementById("panel_table");   

        if (x.style.display === "none") {
            x.style.display = "block";    
            $('#col_panel_opening').css('background-color','lightblue');       
            $('#matching_properties_table').css('display','');        
        } else {
            x.style.display = "none";        
            $('#col_panel_opening').css('background-color','white');        
        }
    }

    function hide_fields1(){
        var unit_type = $('#unit_type').val();
        
        if(unit_type=='commercial'){
            // alert(unit_type); //return;
            $('#configuration').prop('disabled', true); 
            $('#configuration_size').prop('disabled', true); 
            $('#balcony').prop('disabled', true); 

        }else{
            $('#configuration').prop('disabled', false); 
            $('#configuration_size').prop('disabled', false); 
            $('#balcony').prop('disabled', false); 
        }
    }

    function ShowHidearea() {
            var x = document.getElementById("area_div");   

            if (x.style.display === "none") {
               x.style.display = "block";
               $('#col_area').css('display',''); 
            } else {
               x.style.display = "none";        
               $('#col_area').css('background-color','white');        
            }
    }



function saveUnit() {
   var room = $('#room').val();
   var view = $('#view').val();
   var dimension = $('#dimension').val();
   var direction = $('#direction').val();
   var comment = $('#comment').val();

    
   var unit_id = $('#unit_id').val();
   
   

   if(room == ''){
    //   $('#pros_of_complex').css('border-color','red');
      return false;
   }
       $('#loader1').css('display','block');
       $('#submitUnit').css('display','none');
   
       var form_data = new FormData();
       var ext = name.split('.').pop().toLowerCase();

      form_data.append("room", room);
      form_data.append("view", view);
      form_data.append("dimension", dimension);
      form_data.append("direction", direction);
      form_data.append("comment", comment);
      form_data.append("unit_id", unit_id);
      
      form_data.append("form_type", 'single_unit');
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/save-unit",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
        //  console.log(data.bedroom_id);//return;

        //  const obj = JSON.parse(data);

        
         var incPros = parseInt($('#incPros').val()) +1;
         var html = '<tr id="bid_'+data.bedroom_id+'">';
         html += '<td>' + incPros + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="r_'+data.bedroom_id+'">' + data.room_name + '</td>';// obj['company_pros'] +'</td>';        
         html += '<td id="di_'+data.bedroom_id+'">' + data.dimension + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="d_'+data.bedroom_id+'">' + data.direction_name + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="v_'+data.bedroom_id+'">' + data.view + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="c _'+data.bedroom_id+'">' + data.comment + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="ld _'+data.bedroom_id+'">' + data.updated_date + '</td>';// obj['company_cons'] +'</td>';         
         html += "<td> <a href='javascript:void(0)' onClick='updateUnit("+data.bedroom_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelUnit("+data.bedroom_id+")'>Delete</a></td>";   
         html += '</tr>';
         $('#unit_table').prepend(html);
         $('#room').val('').trigger('change');
         $('#view').val('');
         $('#dimension').val('');
         $('#direction').val('').trigger('change');
         $('#comment').val('');
         $('#incPros').val(incPros);

         $('#loader1').css('display','none');
         $('#submitUnit').css('display','block');
      }
      });

}


function updateUnit(param) {
   var bedroom_id = param;
      var form_data = new FormData();
      
      form_data.append("bedroom_id", bedroom_id);
      form_data.append("form_type", 'single_record');
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
      $.ajax({
         url:"/getDataUnitData",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
            {
               var data  = data[0];
               console.log(data); //return;
               $('#bedroom_id').val(data.bedroom_id);

               $('#room').val(data.room_id).trigger('change');
               $('#view').val(data.view);
               $('#dimension').val(data.dimension);
               $('#direction').val(data.direction_id).trigger('change');
               $('#comment').val(data.comment);
               // $('#modal92').modal('open');
               var anchor=document.getElementById("save_unit");
               anchor.innerHTML="Update";
               $("#save_unit").attr("onclick","updateUnit1()");
            }
      });
}

function updateUnit1() {
   var bedroom_id = $('#bedroom_id').val();
   var room = $('#room').val();
   var view = $('#view').val();
   var dimension = $('#dimension').val();
   var direction = $('#direction').val();
   var comment = $('#comment').val();
 

   $('#loader3').css('display','block');
   $('#submitUnit').css('display','none');

      var form_data = new FormData();
      var ext = name.split('.').pop().toLowerCase();

      form_data.append("bedroom_id", bedroom_id);
      form_data.append("room", room);
      form_data.append("view", view);
      form_data.append("dimension", dimension);
      form_data.append("direction", direction);
      form_data.append("comment", comment);
      form_data.append("form_type", 'single_record');

      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });

      $.ajax({
         url:"/updateTableUnitDetails",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            console.log(data); //return;
            var aid = data.bedroom_id;
            $('#bid_'+aid).html(data.data['bedroom_id']);
            $('#r_'+aid).html(data.data['room_name']);
            $('#v_'+aid).html(data.data['view']);
            $('#di_'+aid).html(data.data['dimension']);
            $('#d_'+aid).html(data.data['direction_name']);
            $('#c_'+aid).html(data.data['comment']);
            $('#ld_'+aid).html(data.data['updated_date']);
            // $('#modal92').modal('close');
            // $('#company_pros1').val('');
            // $('#company_cons1').val('');   
            $('#loader3').css('display','none');
            //  $('#submitPros').css('display','block');         
             $('#submitUnit').css('display','block');
         }
      });
}

function clear_unit () {
    $('#bedroom_id').val('');
    $('#view').val('');
    $('#dimension').val('');
    $('#comment').val('');   

    $('#room').val('').trigger('change');       
    $('#direction').val('').trigger('change');     
     
   var anchor=document.getElementById("save_unit");
   anchor.innerHTML="Save";
   $("#save_unit").attr("onclick","saveUnit()");
}

function confirmDelUnit(params) {
    var bedroom_id = params;
    $('.bedroom_id2').val(bedroom_id);   
    $('#deleteUnitModal').modal('open');
}

function deleteUnit() {
    
    var url = 'deleteUnitFormRecords';
    var form = 'delete_unit';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
        // $("#award_table").load(window.location + " #award_table");    
        var hid = data.bedroom_id;
        $('#bid_'+hid).remove();
        // $('#deleteAwardModal').hide();
        $('#deleteUnitModal').modal('close');
        // console.log(data);return;
            
        }
    }); 
}
</script>

        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     