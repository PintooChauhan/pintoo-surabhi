<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      
      <script src="{{ asset('app-assets/js/custom/seller.js') }}"></script>
      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
      <div id="main" class="main-full" style="min-height: auto">
         <div class="row">
         <!-- <input type="hidden" value="@if(isset($buyerId) && !empty($buyerId)) {{$buyerId['buyer_tenant_id']}} @endif" id="buyer_tenant_id"> -->
           
            <!-- <div class="col s12"> -->
               <!-- <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">BUYER INPUT FORM</span><hr>  </div> -->
                 <x-sellerrow1-layout></x-sellerrow1-layout>
                 
                 <x-sellerrow2-layout></x-sellerrow2-layout>
                 <x-sellerrow3-layout></x-sellerrow3-layout>
                 <x-sellerrow4-layout></x-sellerrow4-layout>
                 <hr>
                 <x-sellerrow5-layout></x-sellerrow5-layout>
              
            
            <div class="row">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">

                        <div class="row" >
                           <div class="input-field col l2 m2 s6 display_search">
                           <div class="preloader-wrapper small active" id="loader5" style="display:none">
                                 <div class="spinner-layer spinner-green-only">
                                 <div class="circle-clipper left">
                                    <div class="circle"></div>
                                 </div><div class="gap-patch">
                                    <div class="circle"></div>
                                 </div><div class="circle-clipper right">
                                    <div class="circle"></div>
                                 </div>
                                 </div>
                           </div>
                           </div>
                        </div>

                        <div class="row" id="submitMain">
                           <div class="col l4 m4">
                                 <a href="javascript:void(0)" onClick="saveTenant()" class=" save_challeges_solution btn-small  waves-effect waves-light green darken-1 " id="save_challeges_solution">Save Tenant</a>
                           </div>
                           
                        </div>
                     </div> 
                  </div>
                
                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     