<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<style>p:first-letter {text-transform:capitalize;}</style>
<script>
            $(document).ready(function(){
             
                onleadbyChange();
            });
        </script>
<?php //print_r($buyerId['buyer_tenant_id']); die(); ?>
<div class="row">                                        
   

    <?php  $id=''; ?>

    @if(isset($buyerId)) @if(isset($buyerId['buyer_tenant_id']) && !empty($buyerId['buyer_tenant_id']))
        
        @php
            if($buyerId['buyer_tenant_id'] <= 100){
            
                $id = str_pad($buyerId['buyer_tenant_id'],3, '0', STR_PAD_LEFT);
            }else{
                $id = $buyerId['buyer_tenant_id'];
            }
    @endphp
     @endif @endif

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="customer_name()">

                @php $customer_name1= "";   $initials1=""; @endphp

                @if(isset($getCustomerData))

                    @if( isset($getCustomerData[0]->customer_name) ) @php $customer_name1 = $getCustomerData[0]->customer_name; @endphp @endif 
                    @if( isset($getCustomerData[0]->initials) ) @php $initials1 = $getCustomerData[0]->initials; @endphp @endif 

                
                @endif

            <div class="collapsible-header" id="col_customer_name">Lead Info  @if( !empty($customer_name1) ) ( <?= $customer_name1; ?> ) @endif </div>
            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="lead_info()">
            <div class="collapsible-header" id="col_lead_info" >Lead Source @if( !empty($id) ) ( B <?= $id; ?> ) @endif</div>                            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li >
            <div class="collapsible-header" style="background-color: greenyellow;pointer-events: none;">BUYER INPUT FORM</div>
            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
  
    </div>

               

    <!-- <div class="input-field col l3 m4 s12 display_search" style="width:23.5%">
        <label for="lead_code" class="active">Lead code:  <span class="red-text">*</span></label>
        <input type="text" class="validate" name="lead_code" id="lead_code" Readonly='true'   placeholder="" value="B {{$id}}" >
    </div> -->


 
</div> 

        <div class="collapsible-body"  id='lead_info' style="display:none">
                  
            <div class="row">
            
            <!-- <div class="input-field col l3 m4 s12">
                    <select  id="leadtype" name="leadtype" class="select2 browser-default validate" data-minimum-results-for-search="Infinity" >
                        <option value="" >Select</option>
                        @foreach($lead_type as $leadType)
                        <option value="{{$leadType->lead_type_id}}" @if($leadType->lead_type_name == 'buyer') selected @endif >{{ucwords($leadType->lead_type_name)}}</option>
                        @endforeach
                        
                    </select>
                    <label for="leadtype" class="active">Lead type: <span class="red-text">*</span></label>
                </div> -->


  
                <div class="input-field col l3 m4 s12">
                    <select class="validate browser-default" id="leadby" onChange="onleadbyChange()" >
                        <option value="" >Select</option>
                        @foreach($lead_by as $leadBy)                     
                            <option value="{{$leadBy->lead_by_id}}" @if(isset($getLeadSourceData)) @if(isset($getLeadSourceData[0]->lead_by) && !empty($getLeadSourceData[0]->lead_by)) @if($getLeadSourceData[0]->lead_by == $leadBy->lead_by_id) selected @endif @endif  @else  @if( $leadBy->lead_by_name =='surabhi realtor') selected @endif @endif >{{ ucwords($leadBy->lead_by_name)}}</option>  
                        @endforeach
                    </select>
                    
                    <label for="leadby" class="active">Lead by</label>
                </div>
            

             
                
                <div class="input-field col l3 m4 s12 display_search">
                             @if($getLeadSourceData)
                                @if(isset($getLeadSourceData[0]->lead_by_pc_company ) && !empty($getLeadSourceData[0]->lead_by_pc_company ))
                                         @php $lead_by_pc_company2 = json_decode($getLeadSourceData[0]->lead_by_pc_company);
                                            $lead_by_pc_company22  = $lead_by_pc_company2[0];        
                                         @endphp                
                                        <input type="hidden" id="PcCompanySel" value="<?php print_r($lead_by_pc_company22);?>" style="display:none !important">
                                @endif            
                            @endif
                    <select class="select2 browser-default" multiple="multiple" id="pc_company_name"   name="pc_company_name">
                    

                            @foreach($property_consultant as $property_consultant)                     
                                        <option value="{{$property_consultant->property_consultant_id}}"  >{{ ucwords($property_consultant->company_name)}},{{ ucwords($property_consultant->location_name)}},{{ ucwords($property_consultant->about_owner)}},{{ ucwords($property_consultant->office_number) }}</option>  
                            @endforeach
                    </select>
                    <label for="leadtype3" class="active">PC Company Name </label>                                             
                    <!-- <div id="add_pc_name" class="addbtn" >
                              <a href="#modal1" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                    </div>                             -->
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                     
                     <select class="select2  browser-default"  id="lead_source_name"  name="lead_source_name" onChange="hideShowCampaign()" >
                         <option value=""  selected>Select</option>
                         @foreach($lead_source as $leadSource)
                             <option value="{{$leadSource->lead_source_id}}" @if(isset($getLeadSourceData)) @if(isset($getLeadSourceData[0]->lead_source_name) && !empty($getLeadSourceData[0]->lead_source_name)) @if($getLeadSourceData[0]->lead_source_name == $leadSource->lead_source_id) selected @endif @endif @endif>{{ucwords($leadSource->lead_source_name)}}</option>
                             
                         @endforeach    
                     </select>
                     <label for="leadtype5" class="active">Lead Source Name
                     </label>                                             
                 </div> 

                 <div class="input-field col l3 m4 s12 display_search">        
                     
                 @if(isset($getLeadSourceData) && !empty($getLeadSourceData))
                     @if(isset($getLeadSourceData[0]->lead_by) && !empty($getLeadSourceData[0]->campaign_id))
                    
                        
                        <select class="select2  browser-default"  id="lead_campaign_name"  name="lead_campaign_name" >
                            <option value=""  selected>Select</option>
                            @foreach($lead_campaign as $leadCampaign)
                                <option value="{{$leadCampaign->lead_campaign_id}}" @if(isset($getLeadSourceData)) @if(isset($getLeadSourceData[0]->lead_by) && !empty($getLeadSourceData[0]->campaign_id)) @if($getLeadSourceData[0]->campaign_id == $leadCampaign->lead_campaign_id) selected @endif @endif @endif>{{ucwords($leadCampaign->lead_campaign_name)}}</option>
                                
                             @endforeach    
                        </select>                           
                      

                        @else 
                        <select class="select2  browser-default"  id="lead_campaign_name"  name="lead_campaign_name" >
                            <option value=""  selected>Select</option>
                            @foreach($lead_campaign as $leadCampaign)
                                <option value="{{$leadCampaign->lead_campaign_id}}" @if(isset($getLeadSourceData)) @if(isset($getLeadSourceData[0]->lead_by) && !empty($getLeadSourceData[0]->campaign_id)) @if($getLeadSourceData[0]->campaign_id == $leadCampaign->lead_campaign_id) selected @endif @endif @endif>{{ucwords($leadCampaign->lead_campaign_name)}}</option>
                                
                             @endforeach    
                        </select>                           
                    @endif
                 @else 
                    <select class="select2  browser-default"  id="lead_campaign_name"  name="lead_campaign_name" >
                            <option value=""  selected>Select</option>
                            @foreach($lead_campaign as $leadCampaign)
                                <option value="{{$leadCampaign->lead_campaign_id}}" @if(isset($getLeadSourceData)) @if(isset($getLeadSourceData[0]->lead_by) && !empty($getLeadSourceData[0]->campaign_id)) @if($getLeadSourceData[0]->campaign_id == $leadCampaign->lead_campaign_id) selected @endif @endif @endif>{{ucwords($leadCampaign->lead_campaign_name)}}</option>
                                
                         @endforeach    
                     </select>
                 @endif

                 
                     <label for="lead_campaign_name" class="active">Lead Campaign Name
                     </label>                                             
                 </div>


            </div>

            
           

            <div class="row">
               
                
            
            <div class="input-field col l3 m4 s12 display_search">
                      <label for="lead_reached_from1">Lead reached from which property: <span class="red-text">*</span></label>
                       @php $lead_reached_from=''; @endphp
                       @if(isset($getLeadSourceData))@if(isset($getLeadSourceData[0]->lead_reached_from) && !empty($getLeadSourceData[0]->lead_reached_from)) @php $lead_reached_from = $getLeadSourceData[0]->lead_reached_from; @endphp @endif @endif
                        <input type="text"  name="lead_reached_from" placeholder="Enter" id="lead_reached_from" value="{{$lead_reached_from}}" >
                   
                </div>



                <div class="input-field col l3 m4 s12 display_search">
                    <select class=" validate select2  browser-default"  id="lead_assigned_name"  name="lead_assigned_name">
                        <?php 
                        if($role != 'executive'){ ?>
                            <option value="" >Select</option>
                        <?php } ?>
                        
                        @foreach($users as $users)
                                <option value="{{$users->id}}" @if(isset($getLeadSourceData)) @if(isset($getLeadSourceData[0]->lead_assigned_id) && !empty($getLeadSourceData[0]->lead_assigned_id)) @if($users->id == $getLeadSourceData[0]->lead_assigned_id) selected @endif @endif @endif>{{ucwords($users->name)}}</option>
                                
                         @endforeach    
                    </select>
                    <label for="leadtype7" class="active">Lead Assigned Name
                    </label>                                             
                </div>

                

               
              

               

              
            </div>
            
        </div>

        <div class="collapsible-body" id='customer_name' style="display:none">
            <div class="row">
                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                    <label for="cus_name active" class="dopr_down_holder_label active">Lead Name 
                    <span class="red-text">*</span></label>
                    <div  class="sub_val no-search">
                        <select  id="cus_name_initial" name="cus_name_initial" class="select2 browser-default ">
                        @foreach($initials as $initials)
                        <option value="{{$initials->initials_id}}" @if(isset($getCustomerData)) @if( isset($getCustomerData[0]->initials) ) @if( $initials->initials_id == $getCustomerData[0]->initials) selected @endif @endif @endif >{{ucwords($initials->initials_name)}}</option>
                        @endforeach
                        </select>
                    </div>
                        @php $customer_name= ""; @endphp
                        @if(isset($getCustomerData))@if( isset($getCustomerData[0]->customer_name) ) @php $customer_name = $getCustomerData[0]->customer_name; @endphp @endif @endif
                        <input type="text" placeholder="Enter" class="mobile_number" name="customer_name1" id="customer_name1"  value="{{$customer_name}}">
                </div>


                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                    <label for="contactNum1" class="dopr_down_holder_label active">Primary Mobile Number: <span class="red-text">*</span></label>
                    <div  class="sub_val no-search">
                        <select  id="mobile_conuntry_code" name="mobile_conuntry_code" class="select2 browser-default">
                        @foreach($country as $country1)
                        <option value="{{$country1->country_code_id}}" @if(isset($getCustomerData)) @if( isset($getCustomerData[0]->primary_country_code_id) ) @if( $country1->country_code_id == $getCustomerData[0]->primary_country_code_id) selected @endif @endif @endif  >{{ucwords($country1->country_code)}}</option>
                        @endforeach
                        </select>
                    </div>
                       @php $primary_mobile_number=''; @endphp
                       @if(isset($getCustomerData))@if(isset($getCustomerData[0]->primary_mobile_number) && !empty($getCustomerData[0]->primary_mobile_number)) @php $primary_mobile_number = $getCustomerData[0]->primary_mobile_number; @endphp @endif @endif
                    <input type="text" onkeypress="return onlyNumberKey(event)" class="mobile_number" name="mobile_number_primary" id="mobile_number_primary"  placeholder="Enter" maxlength="10"  value="{{$primary_mobile_number}}" > 
                    @if(isset($getCustomerData) && !empty($getCustomerData))
                        @if(isset($getCustomerData[0]->seccondary_mobile_number) && !empty($getCustomerData[0]->seccondary_mobile_number))
                      
                        @else
                        <div id="AddMoreFileId" class="addbtn">
                            <a href="#" id="AddMoreFileBox" class="" style="color: grey !important">+</a> 
                        </div>
                        @endif
                    @else
                        <div id="AddMoreFileId" class="addbtn">
                            <a href="#" id="AddMoreFileBox" class="" style="color: grey !important">+</a> 
                        </div>
                    @endif

                    <div id="AddMoreFileId" class="addbtn plusButton1" style="display:none">
                        <a href="#" id="AddMoreFileBox" class="" style="color: grey !important">+</a> 
                    </div>

                </div>
                <div class="" id="display_inputs"></div>
                
               @if(isset($getCustomerData) && !empty($getCustomerData))
                    @if(isset($getCustomerData[0]->seccondary_mobile_number) && !empty($getCustomerData[0]->seccondary_mobile_number))
                    <div class="input-field col l3 m4 s12" id="secondaryMobile">
                        <label for="contactNum11" class="dopr_down_holder_label active">Secondary Mobile Number: <span class="red-text">*</span></label>
                            <div  class="sub_val">
                                <select  id="secondary_country_code_id" name="country_code" class="select2 browser-default">
                                    @foreach($country as $country1)
                                    <option value="{{$country1->country_code_id}}" @if(isset($getCustomerData)) @if( isset($getCustomerData[0]->secondary_country_code_id) ) @if( $country1->country_code_id == $getCustomerData[0]->primary_country_code_id) selected @endif @endif @endif  >{{ucwords($country1->country_code)}}</option>
                                    @endforeach    
                                </select>
                            </div>
                            <input type="text" onkeypress="return onlyNumberKey(event)" class="mobile_number" name="seccondary_mobile_number" id="seccondary_mobile_number" required value="{{$getCustomerData[0]->seccondary_mobile_number}}" > 
                            <a href="javascript:void(0)" class="removeclass rmvbtn" onClick="removeDivSecMo()" style="color: red !important">-</a>
                    </div>
                    @endif
               @endif

            </div>    

            <div class="row">
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                    <div  class="sub_val no-search">
                        <select  id="country_code_wa" name="country_code_wa" class="select2 browser-default">
                        @foreach($country as $country2)
                        <option value="{{$country2->country_code_id}}" @if(isset($getCustomerData)) @if( isset($getCustomerData[0]->secondary_wa_countryc_code) ) @if( $country2->country_code_id == $getCustomerData[0]->primary_wa_countryc_code) selected @endif @endif @endif>{{ucwords($country2->country_code)}}</option>
                        @endforeach
                        </select>
                    </div>
                    <label for="whatsapp_number_primary" class="dopr_down_holder_label active">Primary Whatsapp Number: <span class="red-text">*</span></label>
                       @php $primary_wa_number=''; @endphp
                       @if(isset($getCustomerData))@if(isset($getCustomerData[0]->primary_wa_number) && !empty($getCustomerData[0]->primary_wa_number)) @php $primary_wa_number = $getCustomerData[0]->primary_wa_number; @endphp @endif @endif
                    <input type="text" class="mobile_number" onkeypress="return onlyNumberKey(event)" name="whatsapp_number_primary" id="whatsapp_number_primary"  placeholder="Enter" value="{{$primary_wa_number}}">
                    

                  

                    <div  class="addbtn"  style="top: 2px !important; right: 27px !important;width: 16px !important;"  >
                        <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                    </div>

                    @if(isset($getCustomerData) && !empty($getCustomerData))
                        @if(isset($getCustomerData[0]->secondary_wa_number) && !empty($getCustomerData[0]->secondary_wa_number))
                        @else
                            <div id="AddMoreFileIdWhatsapp" class="addbtn" >                    
                            <a href="#" id="AddMoreFileBoxWhatsapp" class="" style="color: grey !important">+</a> 
                            </div>
                        @endif
                    @else
                    <div id="AddMoreFileIdWhatsapp" class="addbtn" >                    
                    <a href="#" id="AddMoreFileBoxWhatsapp" class="" style="color: grey !important">+</a> 
                    </div>
                    @endif
                    
                    <div id="AddMoreFileIdWhatsapp" class="addbtn plusButton2" style="display:none" >                    
                        <a href="#" id="AddMoreFileBoxWhatsapp" class="" style="color: grey !important">+</a> 
                    </div>       

                </div> 
                <div class="" id="display_inputs_whatsapp1"></div>

                @if(isset($getCustomerData) && !empty($getCustomerData))
                    @if(isset($getCustomerData[0]->secondary_wa_number) && !empty($getCustomerData[0]->secondary_wa_number))
                    <div class="input-field col l3 m4 s12 secondaryWaMobile" id="InputsWrapperr2 ">
                        <label for="alterWhatsapp" class="dopr_down_holder_label active">Secondary Whatsapp  Number: <span class="red-text">*</span></label>
                            <div  class="sub_val">
                                <select  id="secondary_wa_countryc_code" name="country_code" class="select2 browser-default">
                                    @foreach($country as $country2)
                                    <option value="{{$country2->country_code_id}}" @if(isset($getCustomerData)) @if( isset($getCustomerData[0]->primary_wa_countryc_code) ) @if( $country2->country_code_id == $getCustomerData[0]->primary_wa_countryc_code) selected @endif @endif @endif>{{ucwords($country2->country_code)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="text"  onkeypress="return onlyNumberKey(event)" class="mobile_number" name="secondary_wa_number" id="secondary_wa_number" required value="{{$getCustomerData[0]->secondary_wa_number}}" > 
                            <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                <input type="checkbox" id="copyAlterwhatsapp" data-toggle="tooltip" title="Check if alternate whatsapp number is same" onClick="copyAlterMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                                </div> <a href="javascript::void(0)" onClick="removeDivSecWaMo()" class="removeclassW rmvbtn" style="color: red !important">-</a></div>
                    @endif
               @endif

                <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                    <label for="primary_email active" class="active">Primary Email Address: <span class="red-text">*</span></label>
                       @php $primary_email=''; @endphp
                       @if(isset($getCustomerData))@if(isset($getCustomerData[0]->primary_email) && !empty($getCustomerData[0]->primary_email)) @php $primary_email = $getCustomerData[0]->primary_email; @endphp @endif @endif
                    <input type="email" class="validate" name="primary_email" id="primary_email"  placeholder="Enter" value="{{$primary_email}}">
                    
                    @if(isset($getCustomerData) && !empty($getCustomerData))
                        @if(isset($getCustomerData[0]->secondary_email) && !empty($getCustomerData[0]->secondary_email))
                        @else
                        <div id="AddMoreFileIdemail" class="addbtn">
                            <a href="#" id="AddMoreFileBox3" class="" style="color: grey !important">+</a> 
                        </div>
                        @endif

                    @else
                    <div id="AddMoreFileIdemail" class="addbtn">
                        <a href="#" id="AddMoreFileBox3" class="" style="color: grey !important">+</a> 
                    </div>                    
                    @endif

                    <div id="AddMoreFileIdemail" class="addbtn plusButton3" style="display:none">
                        <a href="#" id="AddMoreFileBox3" class="" style="color: grey !important">+</a> 
                    </div>

                </div>
                <div class="" id="display_inputs3"></div> 

                @if(isset($getCustomerData) && !empty($getCustomerData))
                    @if(isset($getCustomerData[0]->secondary_email) && !empty($getCustomerData[0]->secondary_email))
                    <div class="input-field col l3 m4 s12 secondaryEmail" id="InputsWrapper3">
                        <label for="secondary_email active" class="active">Secondary Email: <span class="red-text">*</span></label>
                            <input type="email" class="validate" name="secondary_email" id="secondary_email" required value="{{$getCustomerData[0]->secondary_email}}">
                            <a  style="color: red !important" href="javascript::void(0)" onClick="removeDivSecEmail()" class="removeclass13 rmvbtn">-</a></div>
                    @endif
               @endif

            </div>
        </div>
        <hr>
        <!-- Modal PC Company Name Structure -->
    <div id="modal1" class="modal" >
        <div class="modal-content">
        <h5 style="text-align: center">Add Property Consultant Details</h5>
        <hr>
        
        <form method="post" id="pc_company_name_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Company Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_company_name" id="add_pc_company_name"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>
                    
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                        <select class="select2  browser-default"  id="add_pc_sub_location_m1"  name="add_pc_sub_location_m1" onChange="findlocationWithsublocationID(this)">
                            <option disabled value="" selected>Select</option>
                            @foreach($sub_location as $sub_loc)
                                <option value="{{$sub_loc->sub_location_id}}" > {{ucwords($sub_loc->sub_location_name)}} </option>
                            @endforeach
                        </select>
                            <label for="leadtype37  " class="active">Sub Location </label>
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <select class="select2  browser-default"  id="add_pc_location_m1"  name="add_pc_location_m1">  </select>
                    <label for="leadtype37  " class="active">Location </label>
                </div>
            </div> 
            
            <div class="row" style="margin-right: 0rem !important">
                
               <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active"> Owner Name.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_name" id="add_pc_owner_name"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active"> Owner Mobile No: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_mobile_no" id="add_pc_mobile_no"   placeholder="Enter">
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active">Owner Email: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_email" id="add_pc_owner_email"   placeholder="Enter">
                </div>

               
            </div>

            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Excecutive Name.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_excecutive_name" id="add_pc_excecutive_name"   placeholder="Enter">
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" class="active">Excecutive Mobile No.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_pc_excecutive_mobile_no" id="add_pc_excecutive_mobile_no"  placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assignqq" class="active">Excecutive Email: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_pc_excecutive_email" id="add_pc_excecutive_email"   placeholder="Enter">
                 </div> 
                
            </div>
            <div class="alert alert-danger print-error-msg_pc_company_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                    <button class="btn-small  waves-effect waves-light" onClick="pc_company_name_btn()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            </div>
        </form>
    </div>

     <!-- Modal Pc Executive Name Structure -->
     <div id="modal2" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add PC Executive Name </h5>
        <hr>
        
        <form method="post" id="pc_executive_name_form">
        <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Company Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_company_name" id="add_exe_pc_company_name"   placeholder="Enter">
                    <span class="add_exe_pc_company_name_err"></span>
                    
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_name"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Mobile No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_mobile"   placeholder="Enter">
                </div>
            </div> 
            
            <div class="row" style="margin-right: 0rem !important">
                
               <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Whatsapp No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_whatsapp_no"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active">PC Owner Email: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_email"   placeholder="Enter">
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Branch Location.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_branch_location"   placeholder="Enter">
                </div>

                
            </div>

            <div class="row" style="margin-right: 0rem !important">

            <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Employee Name.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_employee_name"   placeholder="Enter">
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" class="active">Employee Mobile No.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_owner_mobile"   placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assignqq" class="active">Employee Whatsapp No: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_whatsapp_no"   placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" disabled class="active">Employee Email.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_employee_email"   placeholder="Enter">
                 </div>
            </div>
                <div class="alert alert-danger print-error-msg_pc_executive_name" style="display:none">
                <ul style="color:red"></ul>
                </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="pc_excecutive_name_btn()" type="button" name="action">Submit</button>
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>   
                                        
                                  
            </div>
        </form>
    </div>
