<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>

::-webkit-scrollbar {
  display: none;
}

input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      
     
      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->

         <style>
            .building_name{
                padding-bottom: 5px;
                background-color: green;
                color: white;
                padding-top: 5px;
                padding-left: 5px;
            }
         </style>

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

       
            

        <div class="row " style="margin-top: -30px;font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
                    
                        <span class="userselect"> @if(isset($wingData) && !empty($wingData)) UPDATE @else ADD @endif WING MASTER</span>
                </div><br>
                @if(isset($wingData) && !empty($wingData))
                    <form method="post" id="wing_master_form_update">
                @else
                    <form method="post" id="wing_master_form">
                @endif
                <input type="hidden" name="society_id" value="$society_id">

                
                    
                @csrf
                 <div class="row">
                    <div class="col l8">Building Elevation :
                        @if(!empty($getElevation))
                            @foreach($getElevation as $val)
                                {{ ucwords($val->elevation_name) }},
                            @endforeach
                        @endif
                    </div>
                    <div class="col l4">
                        <a href="/company-master" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> Back To Home Page</a>
                    </div>                        
                </div>
                <br> 
                
               <div id="" >
                <div class="row building_name" > <lable style="font-weight: 700;">{{$Finalbuilding_name}}</lable></div>
                    <!-- <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":false}'>
                        <div class="clonable" data-ss="1"> -->
                        <form method="post" id="wing_form"><br>
                            <div class="row">
                              <input type="hidden" name="wing_id[]" value="0" >
                                
                                <input type="hidden" class="validate" name="society_id" id="society_id" value="{{$society_id}}" >
                                <div class="col l11">
                                    <!-- <a href="/add-wing-details" id="add_location" class=" modal-trigger" style="color: red !important"> -->
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Wing Name </label>
                                        <input type="text" class="validate" name="wing_name" id="wing_name"   placeholder="Enter" >
                                    </div>
                                    <!-- </a> -->
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Total Floor </label>
                                        <input type="number" class="validate" name="total_floor" id="total_floor"   placeholder="Enter" >
                                    </div>

                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Habitable From </label>
                                        <input type="number" class="validate" name="haitable_floor" id="haitable_floor"   placeholder="Enter" >
                                    </div>
                                    
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Refugee unit </label>
                                        <input type="number" class="validate" name="refugee_unit" id="refugee_unit"   placeholder="Enter" value="0" readonly >                            
                                    </div>
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Total Unit </label>
                                        <input type="number" class="validate" name="total_unit" id="total_unit" readonly  placeholder="" >
                                    </div>
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="active" > No. of Lifts  </label>
                                        <input type="number" class="validate" name="lifts" id="lifts"   placeholder="Enter" >
                                    </div>
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Comment </label>
                                        <input type="text" class="validate" name="comments" id="comments"   placeholder="Enter" >
                                        <!-- <textarea style="border-color: #5b73e8; " placeholder="Enter" name="comments" rows='20' col='40'></textarea> -->
                                        
                                    </div>
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Lobby size </label>
                                        <input type="number" class="validate" name="unit_per_floor" id="unit_per_floor"   placeholder="Enter"  >
                                    </div>
                                    <div class="input-field col l2 m4 s12" id="display_search">
                                        <label  class="active" style="font-size: 10px !important;">Floor Plan </label>
                                        <input type="file" name="floor_plan" id="floor_plan" multiple="true"  style="padding-top: 14px;">
                                    </div>
                                    <div class="input-field col l2 m4 s12" id="InputsWrapper2">
                                        <label  class="active" style="font-size: 10px !important;" >Name Board </label>
                                        <input type="file" name="name_board" id="name_board" multiple="true" style="padding-top: 14px;">
                                    </div>                                   
                                   
                                </div>    
                                <div class="col l1">
                                <div class="row" >
                                        <div class="input-field col l2 m2 s6 display_search">
                                        <div class="preloader-wrapper small active" id="loader1" style="display:none">
                                        <div class="spinner-layer spinner-green-only">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div><div class="gap-patch">
                                            <div class="circle"></div>
                                        </div><div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                        </div>
                                        </div>
                                    </div></div>
                                    <div class="row" id="submitWing">
                                        <a href="javascript:void(0)" onClick="saveWing()" class="btn-small  waves-effect waves-light green darken-1" >Save</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="input-field col l6 m6 s6 display_search">
                                    <div class="alert alert-danger print-error-msg" style="display:none;color:red">
                                    <ul></ul>
                                    </div>
                                </div>  
                            </div>
                        </form>
                        <!-- </div>
                    </div> -->
                    <br>
                    
                    @if(isset($wingData) && !empty($wingData))
                    @foreach($wingData as $wingData)
                    <!-- <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":false}'>
                        <div class="clonable" data-ss="1"> -->
                            <div class="row" id="wing_{{$wingData->wing_id}}">
                                <input type="hidden" name="wing_id" id="wing_id1" value="{{$wingData->wing_id}}" >   
                                                                                 
                                <div class="col l11">
                                    <!-- <a href="/add-wing-details" id="add_location" class=" modal-trigger" style="color: red !important"> -->
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Wing Name</label>
                                        <input type="text" class="validate" name="wing_name" id="wing_name_{{$wingData->wing_id}}" value="@if(isset($wingData->wing_name)) {{$wingData->wing_name}}  @endif "  placeholder="Enter" >
                                        
                                    </div>
                                    
                                    <!-- </a> -->
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Total Floor</label>
                                        <input type="text" class="validate" name="total_floor" id="total_floor_{{$wingData->wing_id}}"  value="@if(isset($wingData->total_floors)) {{$wingData->total_floors}}  @endif "   placeholder="Enter" >
                                    </div>

                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Habitable From </label>
                                        <input type="text" class="validate" name="haitable_floor" id="haitable_floor_{{$wingData->wing_id}}"   value="@if(isset($wingData->habitable_floors)) {{$wingData->habitable_floors}}  @endif " placeholder="Enter">
                                    </div>
                                   
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Refugee unit</label>
                                        <input type="text" class="validate" name="refugee_unit" id="refugee_unit_{{$wingData->wing_id}}"  value="@if(isset($wingData->refugee_units)) {{$wingData->refugee_units}}  @endif "  placeholder="Enter" >                            
                                    </div>
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Total Unit</label>
                                        <input type="text" class="validate" name="total_unit" id="total_unit_{{$wingData->wing_id}}" value="@if(isset($wingData->total_units)) {{$wingData->total_units}}  @endif "  placeholder="Enter" >
                                    </div>
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="active" > No. of Lifts </label>
                                        <input type="text" class="validate" name="lifts" id="lifts_{{$wingData->wing_id}}" value="@if(isset($wingData->no_of_lifts)) {{$wingData->no_of_lifts}}  @endif "  placeholder="Enter" >                               
                                    </div>
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Comment </label>
                                        <input type="text" class="validate" name="comments" id="comments_{{$wingData->wing_id}}" value="@if(isset($wingData->comments )) {{$wingData->comments }}  @endif "  title="{{$wingData->comments }}"  placeholder="Enter" >                                        
                                    </div>
                                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                        <label  class="labelClass">Lobby size</label>
                                        <input type="text" class="validate" name="unit_per_floor" id="unit_per_floor_{{$wingData->wing_id}}" value="@if(isset($wingData->units_per_floor)) {{$wingData->units_per_floor}}  @endif "   placeholder="Enter" >
                                    </div>
                                    <div class="input-field col l2 m4 s12" id="display_search">
                                        <label  class="active" style="font-size: 10px !important;">Floor Plan </label>
                                        <input type="file" name="floor_plan" id="floor_plan_{{$wingData->wing_id}}" multiple="true"  style="padding-top: 14px;" onChange="uploadImgF({{$wingData->wing_id}})">
                                        @if(isset($wingData->floor_plan) && !empty($wingData->floor_plan))          
                                            @php $floor_plan = json_decode($wingData->floor_plan); $i=0; @endphp 
                                            @foreach($floor_plan as $floor_plan)
                                            @php $x = pathinfo($floor_plan, PATHINFO_FILENAME); @endphp
                                            <span class="{{$x}}"><a href="{{ asset('floor_plan/'.$floor_plan) }}"  target="_blank">Floor Plan {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImageW('{{$floor_plan}}#fp#{{$wingData->wing_id}}')">X</a> &nbsp; , </span>
                                                @php $i++; @endphp 
                                            @endforeach 
                                        @endif

                                        <!-- <div class="input-field col l2 m2 s6 display_search"> -->
                                            <div class="preloader-wrapper small active" id="loaderFimg_{{$wingData->wing_id}}" style="display:none">
                                                <div class="spinner-layer spinner-green-only">
                                                    <div class="circle-clipper left">
                                                        <div class="circle"></div>
                                                    </div>
                                                    <div class="gap-patch">
                                                        <div class="circle"></div>
                                                    </div>
                                                    <div class="circle-clipper right">
                                                        <div class="circle"></div>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        <!-- </div> -->
                                    </div>
                                    <div class="input-field col l2 m4 s12" id="InputsWrapper2">
                                        <label  class="active" style="font-size: 10px !important;" >Name Board  </label>
                                        <input type="file" name="name_board" id="name_board_{{$wingData->wing_id}}" multiple="true" style="padding-top: 14px;" onChange="uploadImgN({{$wingData->wing_id}})">
                                        @if(isset($wingData->name_board) && !empty($wingData->name_board))          
                                            @php $name_board = json_decode($wingData->name_board); $i=0; @endphp 
                                            @foreach($name_board as $name_board)
                                            @php $x = pathinfo($name_board, PATHINFO_FILENAME); @endphp
                                            <span class="{{$x}}">  <a href="{{ asset('name_board/'.$name_board) }}"  target="_blank">Name Board {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImageW('{{$name_board}}#nb#{{$wingData->wing_id}}')">X</a> &nbsp; , </span>
                                                @php $i++; @endphp 
                                            @endforeach 
                                        @endif

                                        <div class="preloader-wrapper small active" id="loaderNimg_{{$wingData->wing_id}}" style="display:none">
                                                <div class="spinner-layer spinner-green-only">
                                                    <div class="circle-clipper left">
                                                        <div class="circle"></div>
                                                    </div>
                                                    <div class="gap-patch">
                                                        <div class="circle"></div>
                                                    </div>
                                                    <div class="circle-clipper right">
                                                        <div class="circle"></div>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                    </div>
                                    
                                
                                </div>
                                <div class="col l1"> 
                                    
                                    <div class="row" >
                                        <div class="input-field col l2 m2 s6 display_search">
                                            <div class="preloader-wrapper small active" id="loaderDel_{{$wingData->wing_id}}" style="display:none">
                                                <div class="spinner-layer spinner-green-only">
                                                    <div class="circle-clipper left">
                                                        <div class="circle"></div>
                                                    </div>
                                                    <div class="gap-patch">
                                                        <div class="circle"></div>
                                                    </div>
                                                    <div class="circle-clipper right">
                                                        <div class="circle"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                <div class="row" id="submitDel_{{$wingData->wing_id}}">
                                    <div class="row"   >
                                        <div class="col l3 m3"  >
                                            <a href="javascript:void(0)" title="Copy" onClick="replicaWing({{$wingData->wing_id}})"><i class="material-icons dp48">add_to_photos</i></a>
                                        </div> 
                                        
                                        <div class="col l3 m3"  >
                                            <a href="javascript:void(0)" title="Delete" onClick="removeWing({{$wingData->wing_id}})" style='color:red'><i class="material-icons dp48">remove</i></a>
                                        </div>

                                        <div class="col l3 m3">
                                            <a href="javascript:void(0)" title="Update" onClick="updateWing({{$wingData->wing_id}})"><i class="material-icons dp48">refresh</i></a>
                                        </div>
                                       
                                    </div>
                                    
                                    <div class="row">              
                                        <div class="col l3 m3">
                                            <a href="javascript:void(0)"  title="View Building Structure" onClick="unitView({{$wingData->wing_id}})"><i class="material-icons dp48">location_city</i></a>
                                        </div>
                                        <div class="col l3 m3">
                                            <a href="javascript:void(0)" title="View Unit Details" onClick="unitConfigDetails({{$wingData->wing_id}})"><i class="material-icons dp48">visibility</i></a>
                                        </div>
                                        <div class="col l3 m3">
                                            <a href="javascript:void(0)" title="Setup Wing Structure" onClick="setupStructure({{$wingData->wing_id}})"><i class="material-icons dp48">build</i></a>
                                        </div>

                                        
                                    </div>                                    

                                    
                                </div>
                                    <!-- <a href="javascript:void(0)" onClick="removeWing({{$wingData->wing_id}})" style='color:red'><i class="material-icons dp48">remove</i></a>
                                    <a href="javascript:void(0)" onClick="updateWing({{$wingData->wing_id}})"><i class="material-icons dp48">refresh</i></a>
                                    <a href="javascript:void(0)" onClick="unitView({{$wingData->wing_id}})"><i class="material-icons dp48">location_city</i></a> -->
                                </div>             
                            </div>
                            <br>
                        <!-- </div>
                    </div> -->
                    @endforeach
                    @endif
                    <div id="attached_wing"></div>
                    
                    <br>
                    <hr>
                    <div class="row">
                        <div class="col l11 m11">
                            <div id="unitView" style="display:none;"></div>
                            <!-- overflow-x: hidden;overflow-y: auto; text-align: justify -->
                        </div>
                        
                    </div>

                </div>
                </div>
                <!-- <div class="" id="display_inputs_wing_master"></div> -->
               <!-- </div>  -->


<div id="modal93" class="modal" style="width: 90% !important">
        <div class="modal-content">
        <h5 style="text-align: center">Unit Configuration Details</h5>
        <hr>        
        <form method="post" id="pros_update" >
        
            <div class="row" style="margin-right: 0rem !important">
            <div id="unitConfig"></div>
           
                        
                </div> 
            
           
         
            
          <br>
            
            <div class="row">
                      
                    
                    <div class="input-field col l2 m2 s6 display_search" style="margin-left: -15px;">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>


<div id="modal94" class="modal" style="width: 90% !important">
        <div class="modal-content">
        <h5 style="text-align: center">Setup Wing Structure</h5>
        <hr>        
        
        
            <div class="row" style="margin-right: 0rem !important">
            <div id="generateStructure"></div>
           
                        
                </div> 
            
           
         
            
        
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script>
function uploadImgF(params) {
    // console.log(params);
    // var name = $('#floor_plan_'+params).files[0];
    var form_data = new FormData();
    $('#loaderFimg_'+params).css('display','block');

    var totalfiles = document.getElementById('floor_plan_'+params).files.length;
    // alert(totalfiles); return;
                    for (var index = 0; index < totalfiles; index++) {
                        form_data.append("floor_plan[]", document.getElementById('floor_plan_'+params).files[index]);
                    }
                    form_data.append("wing_id",params);

                    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    });
                    $.ajax({
                    url:"/uploadImg",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        console.log(data.floor_plan);
                        const obj = JSON.parse(data.floor_plan);
                        console.log(obj);
                        alert("Floor plan updated successfully");
                        location.reload();
                        // var totalfiles = obj.length;
                        // console.log(totalfiles);
                        // for (var index = 0; index <= totalfiles; index++) {
                        //     console.log(index++);
                        //    /// form_data.append("floor_plan[]", document.getElementById('floor_plan').files[index]);
                        // }

                        // var myStringArray = obj.reverse();
                        // var result = ""
                        // for (var i = 0; i < myStringArray.length; i++) {
                        //     const file = myStringArray[i];
                        //     const filename = file.split('.').slice(0, -1).join('.');
                        //     // result = result + " <a  target='_blank' href='construction_stage_images/" + myStringArray[i] + "'>Construction Image "+ [i] + "</a>, ";
                        //     result = result + "<span class='"+filename+"'> <a  target='_blank' href='floor_plan/" + file + "'>Floor Plan "+ [index] + "</a> &nbsp; <a href='javascript:void(0)' style='color: red;' onClick=delImageW('"+file+"#"+data.wing_id+"')>X</a> &nbsp; , </span> " ;
                        // }
                        // console.log(result);
                        // $('#Fimg').html(result);
                        
                    }
                    });
    
}

function uploadImgN(params) {
    // console.log(params);
    // var name = $('#floor_plan_'+params).files[0];
    var form_data = new FormData();
    $('#loaderNimg_'+params).css('display','block');
    var totalfiles = document.getElementById('name_board_'+params).files.length;
    // alert(totalfiles); return;
                    for (var index = 0; index < totalfiles; index++) {
                        form_data.append("name_board[]", document.getElementById('name_board_'+params).files[index]);
                    }
                    form_data.append("wing_id",params);

                    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    });
                    $.ajax({
                    url:"/uploadImg",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        alert("Name board updated successfully");
                        console.log(data);
                        location.reload();
                    }
                    });
    
}
                


function delImageW(params) {
      const myArray = params.split("#");
      var image = myArray[0];
      var type = myArray[1];
      var wing_id = myArray[2];
      // alert(myArray[1]); return;
      // alert('#'+params);//delImage
      if (confirm("Are you sure you want to delete this image?")) {
        //  var wing_id = $('#wing_id1').val();
         const file = image;
         const filename = file.split('.').slice(0, -1).join('.');
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delImageW",
            method:"POST",
            data: {image:image,wing_id:wing_id,type:type},
            // contentType: false,
            // cache: false,
            // processData: false,
            success:function(data)
            {
               $('.'+filename).remove();
               console.log(data);
            }
         });
      }else{
         return false;
      }
   }
                function CheckBeforeProceed(params) {
                    
                    if ( confirm("Entire flat series data will be erased or replaced?") ) {
                        window.open(params, '_blank');
                    }else{
                       return false;
                    }
      
                }

                function CheckBeforeProceed1(params) {
                    
                    if ( confirm("Entire building data will be erased or replaced?") ) {
                        window.open(params, '_blank');
                    }else{
                       return false;
                    }
      
                }




            function setupStructure(params) {
                
               
                $.ajax({
                        type:'GET',
                        url: '/generate_structure',
                        data: {wingId:params},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                             $('#generateStructure').html(response);
                             $('#modal94').modal('open');

                            },
                            error: function(response){
                                console.log(response);
                                    // $('#image-input-error').text(response.responseJSON.errors.file);
                            }
                        });
            }

            function unitConfigDetails(params) {
              
                $.ajax({
                        type:'GET',
                        url: '/getUnitConfigDetails1',
                        data: {wingId:params},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                             $('#unitConfig').html(response);
                             $('#modal93').modal('open');

                            },
                            error: function(response){
                                console.log(response);
                                    // $('#image-input-error').text(response.responseJSON.errors.file);
                            }
                        });
            }


                function unitView(wingId) {
                        // alert(wingId)
                        $('#loaderDel_'+wingId).css('display','block');
                        $('#submitDel_'+wingId).css('display','none');
                        $.ajax({
                        type:'GET',
                        url: '/append_unit_structure',
                        data: {wingId:wingId},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html = response;
                            $('#unitView').css('display','block');
                            $('#unitView').html(html);
                            $('#unitSideView').html('');
                            $('#loaderDel_'+wingId).css('display','none');
                            $('#submitDel_'+wingId).css('display','block');

                            },
                            error: function(response){
                                console.log(response);
                                    // $('#image-input-error').text(response.responseJSON.errors.file);
                            }
                        });
                    }
                function calculateTotalUnit() {
                    var total_floor = $('#total_floor').val();
                    var refugee_unit = $('#refugee_unit').val();
                    // var unit_per_floor = $('#unit_per_floor').val();
                    var haitable_floor = $('#haitable_floor').val();
                    // if(haitable_floor == 0){
                    //     var Ftotal_floor =  parseInt(total_floor);//+ 1;
                    // }else{
                       
                    // }
                     var Ftotal_floor =  parseInt(total_floor) ;

                    console.log('Ftotal_floorr '+Ftotal_floor);

                    // var total_unit = (parseInt(Ftotal_floor)  * parseInt(unit_per_floor))- parseInt(refugee_unit);

                     var Ftotal_floor =  parseInt(total_floor) + 1;
                        var a =parseInt(haitable_floor) * parseInt(unit_per_floor);

                    var total_unit = (parseInt(Ftotal_floor)  * parseInt(unit_per_floor))- parseInt(refugee_unit) - a;
                   // $('#total_unit_'+param).val(total_unit);
                   if(total_unit == 0){
                        total_unit = unit_per_floor;
                    }else{
                        total_unit = total_unit;
                    }


                    console.log('a ' +a);
                    console.log('total_floor ' +total_floor);
                    console.log('refugee_unit '+refugee_unit);
                    console.log('unit_per_floor '+ unit_per_floor);
                    console.log('total_unit '+ total_unit);
                    
                    $('#total_unit').val(total_unit);
                    // alert(total_unit);
                }                

                function calculateTotalUnits(param) {
                    // alert(param)
                    var total_floor = $('#total_floor_'+param).val();
                    var refugee_unit = $('#refugee_unit_'+param).val();
                    var unit_per_floor = $('#unit_per_floor_'+param).val();
                    var haitable_floor = $('#haitable_floor_'+param).val();

                    // if(haitable_floor == 0){
                    //     var Ftotal_floor =  parseInt(total_floor) + 1;
                    // }else{
                    //     var Ftotal_floor =  parseInt(total_floor) ;
                    // }
                    var Ftotal_floor =  parseInt(total_floor) + 1;
                        var a =parseInt(haitable_floor) * parseInt(unit_per_floor);

                    var total_unit = (parseInt(Ftotal_floor)  * parseInt(unit_per_floor))- parseInt(refugee_unit) - a;
                    if(total_unit == 0){
                        total_unit = unit_per_floor;
                    }else{
                        total_unit = total_unit;
                    }
                    $('#total_unit_'+param).val(total_unit);
                    // alert(total_unit);
                    console.log('Ftotal_floor ' +Ftotal_floor);
                    console.log('a ' +a);
                    console.log('total_floor ' +total_floor);
                    console.log('refugee_unit '+refugee_unit);
                    console.log('unit_per_floor '+ unit_per_floor);
                    console.log('total_unit '+ total_unit);
                }           

                function removeWing(wing_id) {
                    var wingID = wing_id;
                    $('#loaderDel_'+wing_id).css('display','block');
                    $('#submitDel_'+wing_id).css('display','none');
                    if(confirm("Are you sure wanted to delete the selected Wing?")){ 
                        // $('#loaderDel').css('display','none');
                        //     $('#submitDel').css('display','false');
                        // return true;

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '/removeWing',
                            type: "POST",
                            data : {wingID:wingID},
                            // dataType: "json",
                            success:function(data) {
                                console.log(data);
                                $('#wing_'+data).remove();
                                $('#unitMasterView_'+data).remove();
                                $('#loaderDel_'+wing_id).css('display','none');
                                $('#submitDel_'+wing_id).css('display','block');
                                $('#unitView').html('');
                            }
                        });
                    }else{
                        $('#loaderDel_'+wing_id).css('display','none');
                        $('#submitDel_'+wing_id).css('display','block');
                        return false;
                    }

                }

                function replicaWing(wing_id) {
                    var wingID = wing_id;
                    $('#loaderDel_'+wing_id).css('display','block');
                    $('#submitDel_'+wing_id).css('display','none');
                    if(confirm("Are you sure wanted to make replica of same wing?")){ 
                        // $('#loaderDel').css('display','none');
                        //     $('#submitDel').css('display','false');
                        // return true;

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '/replicaWing',
                            type: "POST",
                            data : {wingID:wingID},
                            // dataType: "json",
                            success:function(data) {
                                
                                var obj = JSON.parse(data);   
                                console.log(obj);
                                $('#wing_name').val(obj.wing_name);
                                $('#total_floor').val(obj.total_floors);
                                $('#haitable_floor').val(obj.habitable_floors);
                                $('#unit_per_floor').val(obj.units_per_floor);
                                $('#refugee_unit').val(obj.refugee_units);
                                $('#total_unit').val(obj.total_units);
                                $('#lifts').val(obj.no_of_lifts);
                                $('#comments').val(obj.comments);
                                $('#loaderDel_'+wing_id).css('display','none');
                                $('#submitDel_'+wing_id).css('display','block');
                            }
                        });
                    }else{
                        $('#loaderDel_'+wing_id).css('display','none');
                        $('#submitDel_'+wing_id).css('display','block');
                        return false;
                    }
                }


                function saveWing() {
                    var wing_name = $('#wing_name').val();
                    var total_floor = $('#total_floor').val();
                    var haitable_floor = $('#haitable_floor').val();
                    var unit_per_floor = $('#unit_per_floor').val();
                    var refugee_unit = $('#refugee_unit').val();
                    var total_unit = $('#total_unit').val();
                    var lifts = $('#lifts').val();
                    var comments = $('#comments').val();
                    var society_id = $('#society_id').val();

                    $('#loader1').css('display','block');
                    $('#submitWing').css('display','none');


                    var form_data = new FormData();
                    // var ext = name.split('.').pop().toLowerCase();   
                    var totalfiles = document.getElementById('floor_plan').files.length;
                    for (var index = 0; index < totalfiles; index++) {
                        form_data.append("floor_plan[]", document.getElementById('floor_plan').files[index]);
                    }

                    var totalfiles1 = document.getElementById('name_board').files.length;
                    for (var index1 = 0; index1 < totalfiles1; index1++) {
                        form_data.append("name_board[]", document.getElementById('name_board').files[index1]);
                    }

                    form_data.append("wing_name", wing_name);
                    form_data.append("total_floor", total_floor);
                    form_data.append("haitable_floor", haitable_floor);
                    form_data.append("unit_per_floor", unit_per_floor);
                    form_data.append("refugee_unit", refugee_unit);
                    form_data.append("total_unit", total_unit);
                    form_data.append("lifts", lifts);
                    form_data.append("comments", comments);
                    form_data.append("society_id", society_id);

                    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    });
                    $.ajax({
                    url:"/add-wing-master",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        // console.log(data['error']);
                        console.log(data); //return;
                    //    var obj = JSON.parse(data);
                       // return;
                     

                        if (typeof  data['error'] === 'undefined') {  
                            // $('#errors').html(data['error']);
                         $('#wing_name').val('');
                         $('#total_floor').val('');
                         $('#haitable_floor').val('');
                         $('#unit_per_floor').val('');
                         $('#refugee_unit').val('');
                         $('#total_unit').val('');
                         $('#lifts').val('');
                         $('#comments').val('');
                         $('#floor_plan').val('');
                         $('#name_board').val('');
                         
                        $('#attached_wing').append(data);

                        // $('#attached_wing').parent().append(data);

                        $(".print-error-msg").find("ul").html('');

                        $('#loader1').css('display','none');
                        $('#submitWing').css('display','block');
                            
                        }else{
                            printErrorMsg(data['error']);
                            $('#loader1').css('display','none');
                            $('#submitWing').css('display','block');
                            // return;
                        }

                        
                       
                        }
                    });   

                    function printErrorMsg (msg) {
                    $(".print-error-msg").find("ul").html('');
                    $(".print-error-msg").css('display','block');
                    $.each( msg, function( key, value ) {
                        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                        console.log(value)
                    });
                    }
                }

                function updateWing(wingID) {
                    
                if(confirm("Are you sure wanted to update wing?")){ 
                    $('#loaderDel_'+wingID).css('display','block');
                    $('#submitDel_'+wingID).css('display','none');
                    
                    var wingID = wingID;
                    var wing_name = $('#wing_name_'+wingID).val();
                    var total_floor = $('#total_floor_'+wingID).val();
                    var haitable_floor = $('#haitable_floor_'+wingID).val();
                    var unit_per_floor = $('#unit_per_floor_'+wingID).val();
                    var refugee_unit = $('#refugee_unit_'+wingID).val();
                    var total_unit = $('#total_unit_'+wingID).val();
                    var lifts = $('#lifts_'+wingID).val();
                    var comments = $('#comments_'+wingID).val();
                    var society_id = $('#society_id').val();
                    // alert(society_id); return;

                    var form_data = new FormData();
                    // var ext = name.split('.').pop().toLowerCase();   
                    var totalfiles = document.getElementById('floor_plan_'+wingID).files.length;
                    for (var index = 0; index < totalfiles; index++) {
                        form_data.append("floor_plan[]", document.getElementById('floor_plan_'+wingID).files[index]);
                    }

                    var totalfiles1 = document.getElementById('name_board_'+wingID).files.length;
                    for (var index1 = 0; index1 < totalfiles1; index1++) {
                        form_data.append("name_board[]", document.getElementById('name_board_'+wingID).files[index1]);
                    }

                    form_data.append("wing_name", wing_name);
                    form_data.append("total_floor", total_floor);
                    form_data.append("haitable_floor", haitable_floor);
                    form_data.append("unit_per_floor", unit_per_floor);
                    form_data.append("refugee_unit", refugee_unit);
                    form_data.append("total_unit", total_unit);
                    form_data.append("lifts", lifts);
                    form_data.append("comments", comments);
                    form_data.append("wing_id", wingID);
                    form_data.append("society_id", society_id);

                    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    });
                    $.ajax({
                    url:"/update-wing-master",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        console.log(data);//return;
                       location.reload();
                        // $('#wing_name').val('');
                        //  $('#total_floor').val('');
                        //  $('#haitable_floor').val('');
                        //  $('#unit_per_floor').val('');
                        //  $('#refugee_unit').val('');
                        //  $('#total_unit').val('');
                        //  $('#lifts').val('');
                        //  $('#comments').val('');
                        //  $('#floor_plan').val('');
                        //  $('#name_board').val('');
                         
                        // $('#attached_wing').append(data);

                        $('#loaderDel_'+wingID).css('display','none');
                        $('#submitDel_'+wingID).css('display','block');
                       
                        }
                    });  
                }else{
                    return false;
                }


                }


            </script>
             

               <br>
               <!-- <div class="row">
                     <div class="alert alert-danger print-error-msg" style="display:none">
                      <ul style="color:red"></ul>
                     </div>
                </div> -->
                <div class="row" ><div class="input-field col l2 m2 s6 display_search">
                    <div class="preloader-wrapper small active" id="loader1" style="display:none">
                        <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                        </div>
                    </div>
                </div></div>

   <!-- <div class="row" id="submitButton"> 
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action">Save</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <a href="#" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
                    </div>    
                </div> 
         
 
         
         
        </div> -->
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->
<script>
     




</script>

        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     