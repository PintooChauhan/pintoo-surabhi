<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script> -->
<form action="/storeStructure" method="post">@csrf
          <input type="hidden" value="{{$wingId}}" name="wing_id">
   

        @if(isset($getDataStructure) && !empty($getDataStructure))

        <div class="row">
          <div class="copyrow">
              

                <div class="col l3 m3">
                    <a href="javascript:void(0)" onClick="addBtn({{count($getDataStructure)}})" class=" save_proscons1 btn-small  waves-effect waves-light green darken-1 " id="save_proscons1">+</a>
                </div>
            </div>
          
       </div>
       <br>
            @foreach($getDataStructure   as $key => $value1)
            <input type="hidden" value="{{$value1->op_wing_structure_id}}" name="wing_structure_id[]">
            <div class="row" id="d_{{$key}}">               
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">From</label>
                            <input type="number" class="validate" name="floor_from[]" id="floor_from_{{$key}}"  value="{{$value1->floor_from}}" required  placeholder="Enter"  >
                        </div>
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">To</label>
                            <input type="number" class="validate" name="floor_to[]" id="floor_to_{{$key}}" value="{{$value1->floor_to}}"  required placeholder="Enter"  >
                        </div>
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">No of Flats</label>
                            <input type="number" class="validate" name="no_of_flat[]" id="no_of_flat" value="{{$value1->no_of_flat}}"  required  placeholder="Enter"  >
                        </div>

                        <div class="col l3 m3">
                            <a href="javascript:void(0)" onClick="removeBtn({{$key}})" class=" save_proscons1 btn-small  waves-effect waves-light red darken-1 " id="save_proscons1">-</a>
                        </div>
                 
                
            </div>
            @endforeach
        @else
        <div class="row">
          <div class="copyrow">
                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                    <label  class="active">From</label>
                    <input type="number" class="validate" name="floor_from[]" id="floor_from_0" required  oninput="changeMinFrom(0)" max="{{$habitable_floors}}" min="{{$habitable_floors}}" value="{{$habitable_floors}}" placeholder="Enter"  min="{{}}">
                </div>
                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                    <label  class="active">To</label>
                    <input type="number" class="validate" name="floor_to[]" id="floor_to_0" oninput="changeMinTo(0)" required placeholder="Enter"  >
                </div>
                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                    <label  class="active">No of Flats</label>
                    <input type="number" class="validate" name="no_of_flat[]" id="no_of_flat" min="1" required  placeholder="Enter"  >
                </div>

                <div class="col l3 m3">
                    <a href="javascript:void(0)" onClick="addBtn()" class=" save_proscons1 btn-small  waves-effect waves-light green darken-1 " id="save_proscons1">+</a>
                </div>
            </div>
          
       </div>
        @endif

       <div class="pasteRow"></div>
      

       <hr>
       <div class="row">
        <div class="col l3 m3">
            <input type="submit" >
       </div>
       </div>
</form>
<input type="hidden" id="inc" value='0'>
<script>

    function changeMinFrom(val){

        var floor_from = $('#floor_from_'+val).val();
        console.log(floor_from);

        var floor_to = parseInt(val);//+1;
        $('#floor_to_'+floor_to).attr("min",parseInt(floor_from)+1);

    }

    function changeMinTo(val){
        var floor_from = parseInt(0)+1;
        var floor_to = $('#floor_to_'+val).val();
        console.log('floor_to '+floor_to);
        console.log('floor_from '+floor_from);

        $('#floor_from_'+floor_from).attr("min",parseInt(floor_to)+1);


    }

    function addBtn(cnt=null) {
        var max_fields      = 10;
        var wrapper         = $(".pasteRow"); //Fields wrapper
        var x = 1; 
        if(x < max_fields){ //max input box allowed
            // alert(x)
            //number box increment
           var  inC  = parseInt($('#inc').val());
           var incval =  inC + 1 ;

           var getVal = parseInt($('#floor_to_'+inC).val()) +1;
        //    alert('floor_to_'+inC);

        if(cnt != null){
            incval = cnt;
            var calId = parseInt(cnt)-1;
            getVal  =  $('#floor_to_'+calId).val();
            console.log('floor_to_'+calId);
            // $('#floor_from_'+cnt).attr("min",parseInt();

        }else{
            incval = incval;
            getVal = getVal;
        }
           $('#inc').val( incval );
            $(wrapper).append('<div class="row" id="d_'+ incval +'"><div class="input-field col l3 m4 s12" id="InputsWrapper2"><label  class="active">From</label><input type="number" class="validate" name="floor_from[]" required id="floor_from_'+incval+'" min="'+getVal+'" oninput="changeMinFrom('+(incval)+')" placeholder="Enter"  > </div> <div class="input-field col l3 m4 s12" id="InputsWrapper2">  <label  class="active">To</label><input type="number" class="validate" name="floor_to[]" required id="floor_to_'+incval+'" oninput="changeMinTo('+(incval+1)+')"  placeholder="Enter"  ></div><div class="input-field col l3 m4 s12" id="InputsWrapper2"> <label  class="active">No of Flats</label> <input type="number" class="validate" name="no_of_flat[]" required min="1" id="no_of_flat"   placeholder="Enter"  ></div><div class="col l3 m3"><a href="javascript:void(0)" onClick="removeBtn('+ incval +')" class=" save_proscons1 btn-small  waves-effect waves-light red darken-1 " id="save_proscons1">-</a></div></div></div>'); //add input box
            //<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>
          
        }
    }


    function removeBtn(x){
        // alert(x);
        $('#d_'+x).remove();
    }
   




</script>

        


     