<div id="modal4" class="modal" style="width:84% !important">
        <div class="modal-content">
            <h5 style="text-align: center"><b>UNIT INFORMATION</b></h5>
            <hr>
        
            <form method="post" id="add_unit_info_form">@csrf
                <input type="text" value="" name="society_id">
                <input type="text" value="" name="wing_id">
            <div class="row" style="margin-right: 0rem !important">

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default" id="unit_available"  data-placeholder="Select" name="unit_available">
                        <option value="" disabled selected>Select</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <label for="property_seen" class="active">Unit Available ?</label>
                </div>

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default"  id="refugee_unit" onChange="hide_all_info()"  data-placeholder="Select" name="refugee_unit">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <label for="property_seen1" class="active">Refugee Unit</label>
                </div>
            </div>
            <div id="not_refugee">
                <div class="row" style="margin-right: 0rem !important" >  
            
                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Property No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="property_no" id="property_no"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Floor No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="floor_no" id="floor_no"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                            

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="numeric" class="input_select_size" name="rera_carpet_area" id="rera_carpet_area" required placeholder="Enter" >
                                <label>RERA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="rera_carpet_area_unit" name="rera_carpet_area_unit" class="select2 browser-default ">
                               
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="numeric" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area" required placeholder="Enter"  >
                                <label>MOFA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="mofa_carpet_area_unit" name="mofa_carpet_area_unit" class="select2 browser-default ">
                                
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Door Facing Direction: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="door_facing_direction" id="door_facing_direction"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Configuration: <span class="red-text">*</span></label>
                
                    <select  name="configuration" class="validate select2 browser-default">
                                  
                                </select>
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                
                    <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default"  id="re"  data-placeholder="Select" name="unit_type">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Residencial</option>
                        <option value="no">Commercial</option>
                    </select>
                    <label for="property_seen" class="active">Unit Type</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Build Up Area: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="build_up_area" id="build_up_area"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">No.of Bedrooms with Sizes: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="no_of_bedrooms" id="no_of_bedrooms"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>


                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">No.of Bathrooms: <span class="red-text">*</span></label>
                    <input type="numeric" class="validate" name="no_of_bathrooms" id="no_of_bathrooms"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
            
                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Hall Size: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="hall_size" id="hall_size"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Kitchen Size: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="kitchen_size" id="kitchen_size"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Parking : <span class="red-text">*</span></label>
                    <input type="numeric" class="validate" name="parking" id="parking"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Parking Info: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="parking_info" id="parking_info"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
            

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">    Unit View: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="unit_view" id="unit_view"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="comment" id="comment"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                </div>             
            </div>           
            <div class="alert alert-danger print-error-msg_add_unit_info" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style="color:red"></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light"  type="submit" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            
            
                
            </div>
      
    </div> 
         