

   


    
   




        <!-- Modal Unit amenity Structure -->
         <div id="modal13" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Unit Amenity</h5>
        <hr>
        
        <form method="post" id="add_unit_amenity">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Amenity: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>
                    
                </div>
            </div>             
           
            <div class="alert alert-danger print-error-msg_add_unit_amenity" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="add_unit_amenity()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            
            
                
            </div>
        </form>
    </div>


    <!-- Modal Building amenity Structure -->
    <div id="modal13" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Building Amenity</h5>
        <hr>
        
        <form method="post" id="add_building_amenity">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Amenity: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="building_amenity" id="building_amenity"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>
                    
                </div>
            </div>             
           
            <div class="alert alert-danger print-error-msg_add_building_amenity" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="add_building_amenity()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>             
            
                
            </div>
        </form>
    </div>




    <!-- Modal Company address - state Structure -->
    <div id="modal7" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add State</h5>
        <hr>
        
        <form method="post" id="pc_company_name_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">State: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_company_city" id="add_company_city"   placeholder="Enter">
                    <span class="add_company_city_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_company_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <button class="btn-small  waves-effect waves-light" onClick="alert()" type="button" name="action">Submit</button>                        
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
            </div>
        </form>
    </div>

     <!-- Modal Current resi address - state Structure -->
     <div id="modal8" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add State</h5>
        <hr>
        
        <form method="post" id="pc_company_name_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">State: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_company_name" id="add_pc_company_name"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_pc_company_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <button class="btn-small  waves-effect waves-light" onClick="alert()" type="button" name="action">Submit</button>                        
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
            </div>
        </form>
    </div>

  


    
      <!-- Modal Add new user Structure -->
      <div id="modal13" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Industry Name</h5>
        <hr>
        
        <form method="post" id="add_industry_form">
        
            
           
            <div class="alert alert-danger print-error-msg_add_industry_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_industry_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>



 

 <!-- Modal Add Suffix -->
 <div id="modal16" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Suffix</h5>
        <hr>
        
        <form method="post" id="add_suffix_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Suffix: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_suffix" id="add_suffix"   placeholder="Enter">                                       
                </div>                
            </div>     
            
           
            <div class="alert alert-danger print-error-msg_add_suffix" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_suffix_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>  

 <!-- Modal Add Preferred Tenant -->
 <div id="modal17" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Preferred Tenant</h5>
        <hr>
        
        <form method="post" id="add_preferried_tenant">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active"> Preferred Tenant: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_preferred_tenant" id="add_preferred_tenant"   placeholder="Enter">                                       
                </div>                
            </div>     
            
           
            <div class="alert alert-danger print-error-msg_add_preferried_tenant" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_preferried_tenant_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>  

<!-- Modal Add Shifting Charges -->
<div id="modal18" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Shifting Charges</h5>
        <hr>
        
        <form method="post" id="add_preferried_tenant">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active"> Shifting Charges: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_preferred_tenant" id="add_preferred_tenant"   placeholder="Enter">                                       
                </div>                
            </div>     
            
           
            <div class="alert alert-danger print-error-msg_add_preferried_tenant" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_preferried_tenant_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>  





