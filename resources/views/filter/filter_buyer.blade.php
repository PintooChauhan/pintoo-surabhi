
<div class="row" style="padding: 20px 0;">

    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">        
             
        <select class="select2  browser-default"  id="lead_campaign_name"  name="lead_campaign_name" >
          <option value=""  selected>Select</option>
          @foreach($leadCampaignData as $key=>$lcdvalue)
          <option value="{{$lcdvalue->lead_campaign_id}}">{{ucwords($lcdvalue->lead_campaign_name)}}</option>
                       
          @endforeach    
        </select>
    
    
        
            <label for="lead_campaign_name" class="active">Lead Campaign Name
            </label>                                             
        </div>
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">
        <select class=" validate select2  browser-default"  id="lead_assigned_name"  name="lead_assigned_name">
            <?php 
            if($role != 'executive'){ ?>
                <option value="" >Select</option>
            <?php } ?>
            
            @foreach($leadAssignedData as $ladkey=>$ladvalue)
                    <option value="{{$ladvalue->id}}">{{ucwords($ladvalue->name)}}</option>
             @endforeach    
        </select>
        <label for="leadtype7" class="active">Lead Assigned Name
        </label>                                             
      </div>
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 ">
          <select class="select2  browser-default"  id="unit_type" name="unit_type" >
            @foreach($unitTypeData as $utdkey=>$utdvalue)
                    <option value="{{$utdvalue->unit_type_id}}">{{ucwords($utdvalue->unit_type)}}</option>
             @endforeach
          </select>
          <label for="unit_type" class="active"> Unit type</label>
      </div>
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">   
                                   
        <select class="select2  browser-default" multiple="multiple"   name="door_facing_direction" id="door_facing_direction">
    
            @foreach($doorDirectionData as $dddkey=>$dddvalue)
                <option value="{{$dddvalue->direction_id}}">{{ucwords($dddvalue->direction)}}</option>
            @endforeach
    
        </select>
        <label for="purpose" class="active">Door Direction</label>
      </div>
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">                            
        <select class="select2  browser-default"   name="floor_choice" id="floor_choice"  multiple="multiple">
    
          @foreach($floorChoiceData as $fcdkey=>$fcdvalue)
              <option value="{{$fcdvalue->dd_floor_choice_id}}" >{{ucwords($fcdvalue->floor_choice)}}</option>
          @endforeach
    
        </select>
        <label for="floor_choice" class="active">Floor Choice    </label>
      </div> 
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l3 m4 s12 display_search">
        <select class="select2  browser-default" multiple="multiple"   name="purpose" id="purpose1">
        @if(isset($purposeData))     
            @foreach($purposeData as $pdvalue)
                <option value="{{$pdvalue->dd_purpose_id}}" >{{ ucwords($pdvalue->purpose)}}</option>
            @endforeach
        @endif   
                
            </select>
            <label for="purpose" class="active">Purpose
            </label>
        </div>  
      </div>  
    
    </div>