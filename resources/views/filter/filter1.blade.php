<form id="filter_form">

  <div class="row" style="padding: 20px 0;">

    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">        
             
        <select class="select2  browser-default"  id="lead_campaign_name"  name="lead_campaign_name" >
          <option value=""  selected>Select </option>
          @if(isset($leadCampaignData))
          @foreach($leadCampaignData as $key=>$lcdvalue)
          <option value="{{$lcdvalue->lead_campaign_id}}" @if( isset($_GET['lead_campaign_name']) && !empty($_GET['lead_campaign_name']) ) @if($_GET['lead_campaign_name']==$lcdvalue->lead_campaign_id) selected @endif @endif >{{ucwords($lcdvalue->lead_campaign_name)}} </option>
                       
          @endforeach
          @endif
        </select>
    
            <label for="lead_campaign_name" class="active">Lead Campaign Name
            </label>                                             
        </div>
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">
        <select class=" validate select2  browser-default"  id="lead_assigned_name"  name="lead_assigned_name">
            <?php 
            
            if($role != 'executive'){ 
            
            ?>
                <option value="" >Select</option>
            <?php }  ?>
            
            @foreach($leadAssignedData as $ladkey=>$ladvalue)
              <option value="{{$ladvalue->id}}" @if( isset($_GET['lead_assigned_name']) && !empty($_GET['lead_assigned_name']) ) @if($_GET['lead_assigned_name']==$ladvalue->id) selected @endif @endif >{{ucwords($ladvalue->name)}}</option>
            @endforeach
              
        </select>
        <label for="leadtype7" class="active">Lead Assigned Name
        </label>                                             
      </div>
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 ">
          <select class="select2  browser-default"  id="unit_type" name="unit_type" >
            @foreach($unitTypeData as $utdkey=>$utdvalue)
              <option value="{{$utdvalue->unit_type}}" @if( isset($_GET['unit_type']) && !empty($_GET['unit_type']) ) @if($_GET['unit_type']==$utdvalue->unit_type) selected @endif @endif >{{ucwords($utdvalue->unit_type)}}</option>
             @endforeach
          </select>
          <label for="unit_type" class="active"> Unit type</label>
      </div>
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">   
                                   
        <select class="select2  browser-default" multiple="multiple"   name="door_facing_direction" id="door_facing_direction">
          @if( isset($_GET['door_facing_direction']) && !empty($_GET['door_facing_direction']) )
            @php

            $door_facing_direction = $_GET['door_facing_direction'];
            $door_facing_direction_arr = explode(",",$door_facing_direction);

            @endphp

            {{-- @if(count($door_facing_direction_arr)>0) --}}
              @foreach($doorDirectionData as $dddkey=>$dddvalue)
                  <option value="{{$dddvalue->direction_id}}">{{ucwords($dddvalue->direction)}}</option>
              @endforeach
            {{-- @endif --}}
          
          @else
            @foreach($doorDirectionData as $dddkey=>$dddvalue)
            <option value="{{$dddvalue->direction_id}}">{{ucwords($dddvalue->direction)}}</option>
            @endforeach
          @endif 
        </select>
        <label for="door_facing_direction" class="active">Door Direction</label>
      </div>
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">                            
        <select class="select2  browser-default"   name="floor_choice" id="floor_choice"  multiple="multiple">
    
          @foreach($floorChoiceData as $fcdkey=>$fcdvalue)
              <option value="{{$fcdvalue->dd_floor_choice_id}}" >{{ucwords($fcdvalue->floor_choice)}}</option>
          @endforeach
    
        </select>
        <label for="floor_choice" class="active">Floor Choice    </label>
      </div> 
    </div>
    
    <div class="col l3 s12">
      <div class="input-field col l12 m4 s12 display_search">
        <select class="select2  browser-default" multiple="multiple"   name="purpose" id="purpose">
        @if(isset($purposeData))     
            @foreach($purposeData as $pdvalue)
                <option value="{{$pdvalue->dd_purpose_id}}" >{{ ucwords($pdvalue->purpose)}}</option>
            @endforeach
        @endif   
                
            </select>
            <label for="purpose" class="active">Purpose
            </label>
        </div>  
      </div>

      <div class="col l3 s12">
        <div class="input-field col l12 m4 s12 display_search">
            <select class="select2  browser-default"  name="own_contribution" id="own_contribution">
              <option value="" >Select</option>
            @if(isset($ownContributionData))     
                @foreach($ownContributionData as $ocvalue)
                <option value="{{$ocvalue->own_contribution}}" @if( isset($_GET['own_contribution']) && !empty($_GET['own_contribution']) ) @if($_GET['own_contribution']==$ocvalue->own_contribution) selected @endif @endif >{{ ucwords($ocvalue->own_contribution)}}</option>
                @endforeach
            @endif
                  
            </select>
            <label for="own_contribution" class="active">Own Contribution Amount:</label>

          </div>  
        </div>

        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                <select class="select2  browser-default" multiple="multiple"   name="contribution_source" id="contribution_source">
                @if(isset($contributionSourceData))     
                    @foreach($contributionSourceData as $csdvalue)
                    <option value="{{$csdvalue->dd_own_contribution_source_id}}" >{{ ucwords($csdvalue->own_contribution_source)}}</option>
                    @endforeach
                @endif   
                      
                </select>
                <label for="contribution_source" class="active">Own Contribution Source:</label>
    
              </div>  
        </div>

        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                <select class="select2  browser-default" name="oc_time_period" id="oc_time_period">
                  <option value="" >Select</option>
                @if(isset($ocTimePeriodData))     
                    @foreach($ocTimePeriodData as $octpdvalue)
                    <option value="{{$octpdvalue->dd_oc_time_period_id}}" @if( isset($_GET['oc_time_period']) && !empty($_GET['oc_time_period']) ) @if($_GET['oc_time_period']==$octpdvalue->dd_oc_time_period_id) selected @endif @endif >{{ ucwords($octpdvalue->oc_time_period)}}</option>
                    @endforeach
                @endif   
                      
                </select>
                <label for="oc_time_period" class="active">Own Contribution Time Period:</label>
    
              </div>  
        </div>


        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                <select class="select2  browser-default" name="loan_status" id="loan_status">
                    <option value="0" @if( !isset($_GET['loan_status']) || empty($_GET['loan_status']) ) selected @endif   @if( isset($_GET['loan_status']) && !empty($_GET['loan_status']) ) @if($_GET['loan_status']=='0') selected @endif @endif >Select</option>
                    <option value="sanctioned" @if( isset($_GET['loan_status']) && !empty($_GET['loan_status']) ) @if($_GET['loan_status']=='sanctioned') selected @endif @endif >Sanctioned</option>
                    <option value="in_process" @if( isset($_GET['loan_status']) && !empty($_GET['loan_status']) ) @if($_GET['loan_status']=='in_process') selected @endif @endif >In Process</option>
                    <option value="need_assiatance" @if( isset($_GET['loan_status']) && !empty($_GET['loan_status']) ) @if($_GET['loan_status']=='need_assiatance') selected @endif @endif >Need Assistance</option>
                </select>
                <label for="loan_status" class="active">Loan Status:</label>
    
              </div>  
        </div>

        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                
                <select class="select2  browser-default"    name="looking_since" id="looking_since">
                       <option selected  value="">Select</option>
                       @foreach($looking_since as $looking_since1)
                            <option value="{{$looking_since1->looking_since_id}}" @if( isset($_GET['looking_since']) && !empty($_GET['looking_since']) ) @if($_GET['looking_since']==$looking_since1->looking_since_id) selected @endif @endif >{{ ucwords($looking_since1->looking_since) }} </option> 
                        @endforeach 
                </select>
                <label for="looking_since" class="active">Looking Since</label>
           </div>
        </div>

        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                
                <select class="select2  browser-default"    name="building_type" id="building_type">
                       <option selected  value="">Select</option>
                       @foreach($building_type as $btvalue)
                            <option value="{{$btvalue->building_type_id}}" @if( isset($_GET['building_type']) && !empty($_GET['building_type']) ) @if($_GET['building_type']==$btvalue->building_type_id) selected @endif @endif >{{ ucwords($btvalue->building_type_name) }} </option> 
                        @endforeach 
                </select>
                <label for="building_type" class="active">Building Type</label>
           </div>
        </div>

        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                
                <select class="select2  browser-default" name="parking_type" id="parking_type" multiple="multiple">
                    {{-- <option selected  value="">Select</option> --}}
                    @foreach($parkingData as $pdvalue)
                        <option value="{{$pdvalue->parking_id}}">{{ ucwords($pdvalue->parking_name) }} </option> 
                    @endforeach 
                       
                </select>
                <label for="parking_type" class="active">Parking</label>
           </div>
        </div>

        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                
                <select class="select2  browser-default" name="property_seen" id="property_seen">
                       <option  @if( !isset($_GET['property_seen']) || empty($_GET['property_seen']) ) selected @endif value="">Select</option>
                       <option value="property consultant" @if( isset($_GET['property_seen']) && !empty($_GET['property_seen']) ) @if($_GET['property_seen']=='property consultant') selected @endif @endif >Property Consultant</option>
                       <option value="direct" @if( isset($_GET['property_seen']) && !empty($_GET['property_seen']) ) @if($_GET['property_seen']=='direct') selected @endif @endif >Direct</option>
                       <option value="surabhi realtor" @if( isset($_GET['property_seen']) && !empty($_GET['property_seen']) ) @if($_GET['property_seen']=='surabhi realtor') selected @endif @endif >Surabhi realtor</option>
                       
                </select>
                <label for="property_seen" class="active">Property Seen</label>
           </div>
        </div>

        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                
                <select class="select2  browser-default" name="payment_plan" id="payment_plan">
                    <option selected  value="">Select</option>
                    @foreach($paymentPlanData as $ppdvalue)
                    <option value="{{$ppdvalue->dd_payment_plan_id}}" @if( isset($_GET['payment_plan']) && !empty($_GET['payment_plan']) ) @if($_GET['payment_plan']==$ppdvalue->dd_payment_plan_id) selected @endif @endif >{{ ucwords($ppdvalue->payment_plan) }}</option> 
                    @endforeach
                       
                </select>
                <label for="payment_plan" class="active">Payment Plan</label>
           </div>
        </div>

        <div class="col l3 s12">
            <div class="input-field col l12 m4 s12 display_search">
                <input type="text"  class="datepicker adfsdf" name="expected_closure_date" value="@if( isset($_GET['expected_closure_date']) && !empty($_GET['expected_closure_date']) ) @php echo $_GET['expected_closure_date']; @endphp @endif" Placeholder="Enter" id="expected_closure_date_buyer">
                <label for="expected_closure_date" class="active">Expected Closure Date</label>
           </div>
        </div>
    
    </div>

    <div class="row" style="padding: 20px 10px;">
      <div class="col l2 s12">
        <div class="input-field col l12 m4 s12 display_search">
          <button type="submit" class="btn btn-primary btn-md" style="margin-left: -12px; " >
            Submit
          </button>
          
       </div>
      </div>
    </div>

  </form>