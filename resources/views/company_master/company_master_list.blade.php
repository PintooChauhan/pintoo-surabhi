
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/css/select.dataTables.min.css"> -->
  
      <!-- <script src="app-assets/js/scripts/data-tables.js"></script> -->
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.0/css/fixedColumns.dataTables.min.css"> -->
      <!-- END PAGE LEVEL JS-->
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/4.1.0/css/fixedColumns.dataTables.min.css">

<style>
  select
{
    display: block !important;
}

</style>



      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">
    

 <div class="row">

    {{-- check access available or not storing in one var --}}
    @php
    $check_s_add = 0;
    $check_s_edit = 0;
    $check_s_delete = 0;
    @endphp

    @if(isset($access_check) && !empty($access_check) )

      @foreach($access_check as $ackey=>$acvalue)
        @php
          $check_s_add = $acvalue->s_add;
          $check_s_edit = $acvalue->s_edit;
          $check_s_delete = $acvalue->s_delete;
        @endphp
      @endforeach

    @endif
                 
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                
                <div class="row">
                    <div class="col s12">
                        <div class="table-responsive">
                            <table id="scroll-vert-hor31" class="display nowrap">
                                    <thead>
                                        <tr class="pin">
                                            <th><a href="/company-list" style="color:#6b6f82"> Group Name</a> </th>
                                            <th><a href="/project-list" style="color:#6b6f82"> Complex Name </a></th>
                                            <th>No.of Bldg </th>
                                            <th><a href="/society-list" style="color:#6b6f82"> Building Name</a></th>
                                            <th>No. of Wings</th>                           
                                            <th>Building Type</th>
                                            <th>Building Status</th>
                                            <th>Sub Location</th>
                                            <th>Location</th>
                                            <th>City</th>
                                            <th>Building Age</th>
                                            <th>Possession Year</th>
                                            <th > <a href='/add-company-master-form' style="color: #6b6f82;; " >Add Group</a></th>

                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $data1)
                                    <?php
                                        $group_name = $data1->group_name;
                                        $finalgroup_name = '';
                                        $sub2 = substr($group_name, 0, 20);
      
                                        if(strlen($group_name) > 20){
                                          $finalgroup_name =  $sub2;
                                        }else{
                                          $finalgroup_name =  $group_name;
                                        }

                                        $project_complex_name = $data1->project_complex_name;
                                        $finalproject_complex_name = '';
                                        $sub2 = substr($project_complex_name, 0, 20);
      
                                        if(strlen($project_complex_name) > 20){
                                          $finalproject_complex_name =  $sub2;
                                        }else{
                                          $finalproject_complex_name =  $project_complex_name;
                                        }

                                        $building_name = $data1->building_name;
                                        $finalbuilding_name = '';
                                        $sub2 = substr($building_name, 0, 20);
      
                                        if(strlen($building_name) > 20){
                                          $finalbuilding_name =  $sub2;
                                        }else{
                                          $finalbuilding_name =  $building_name;
                                        }
                                    
                                    ?>


                                    <tr>
                                        <td title="<?= $group_name; ?>">
                                            @if($check_s_edit==1)
                                            <a href="add-company-master-form?company_id={{$data1->company_id}}" target="_blank"> {{ ucwords($finalgroup_name) }} </a>
                                            @else
                                            <a href="javascript:void(0);">  {{ ucwords($finalgroup_name) }}</a>
                                            @endif
                                        </td>
                                        <td title="<?= $project_complex_name; ?>">
                                            @if($check_s_edit==1)
                                            <a href="project-master?project_id={{$data1->project_id}}" target="_blank"> {{ ucwords($finalproject_complex_name) }} </a>
                                            @else
                                            <a href="javascript:void(0);">  {{ ucwords($finalproject_complex_name) }}</a>
                                            @endif
                                        </td>
                                        <td>{{$data1->societyCnt}}</td>
                                        <td title="<?= $building_name; ?>">
                                            @if($check_s_edit==1)
                                            <a href="society-master?society_id={{$data1->society_id}}" target="_blank"> {{ ucwords($finalbuilding_name) }} </a>
                                            @else
                                            <a href="javascript:void(0);">  {{ ucwords($finalbuilding_name) }}</a>
                                            @endif
                                        </td>
                                        <td> 
                                            @if($data1->wingCnt > 0 ) 
                                            @if($check_s_edit==1)
                                            <a href="wing-master?society_id={{$data1->society_id}}" target="_blank"> @if(isset($data1->building_name) && !empty($data1->building_name)) {{$data1->wingCnt}} @endif</a> @endif</td>                                       
                                            @else
                                            <a href="javascript:void(0);"> @if(isset($data1->building_name) && !empty($data1->building_name)) {{$data1->wingCnt}} @endif </a>
                                            @endif
                                        
                                        <td>
                                        <?php 
                                            $decodeUnit = json_decode($data1->category_id) ;
                                                if(!empty($decodeUnit)){
                                                if(!empty($decodeUnit[0])){
                                                $get1 = DB::select("select * from dd_category where category_id in(".$decodeUnit[0].")  ");
                                                foreach($get1 as $val1){
                                                    echo ucwords($val1->category_name ).',';
                                                } 
                                                }
                                            }
                                            
                                            
                                            ?>

                                        </td>
                                        <td> {{ucwords($data1->building_type_name)}}  </td>
                                        <td>{{ucwords($data1->sub_location_name)}}</td>
                                        <td>{{ucwords($data1->location_name)}}</td>
                                        <td>{{ucwords($data1->city_name)}}</td>

                                        
                                        <td>{{$data1->building_year_as_per_possession}}</td>
                                        <td>{{$data1->possession_year}}</td>
                                       
                                        <td> 
                                        
                                        + 
                                        @if($check_s_edit==1)
                                        <a href='/project-master?c_id={{$data1->company_id}}'  target="_blank"> Complex</a>
                                        @else
                                        <a href='javascript:void(0);' > Complex</a> 
                                        @endif

                                        <?php if( isset($data1->project_id) && !empty($data1->project_id) && $data1->project_complex_name != null){  
                                            if(empty($data1->building_name) && !empty($data1->society_id)){ ?>
                                                |
                                                @if($check_s_edit==1)
                                                <a href='/society-master?society_id={{$data1->society_id}}'  target="_blank" > Building</a> 
                                                @else
                                                <a href='javascript:void(0);' > Building</a> 
                                                @endif
                                            <?php }else{ ?>
                                                |
                                                @if($check_s_edit==1)
                                                <a href='/society-master?p_id={{$data1->project_id}}'  target="_blank" > Building</a>
                                                @else
                                                <a href='javascript:void(0);' > Building</a> 
                                                @endif
                                            <?php }
                                            ?>
                                        
                                        
                                        <?php } ?>
                                        @if($check_s_edit==1)
                                        <a href="wing-master?society_id={{$data1->society_id}}" target="_blank"> @if(isset($data1->building_name) && !empty($data1->building_name)) |  Wings @endif</a>
                                        @else
                                        <a href='javascript:void(0);' > @if(isset($data1->building_name) && !empty($data1->building_name)) |  Wings @endif</a> 
                                        @endif
                                    </td>
                                       
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <!-- <script src="app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> -->
    <!-- <script src="app-assets/vendors/data-tables/js/dataTables.select.min.js"></script> -->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <!-- <script src="app-assets/js/plugins.js"></script> -->
    <!-- <script src="app-assets/js/search.js"></script> -->
    <!-- <script src="app-assets/js/custom/custom-script.js"></script> -->
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/4.1.0/js/dataTables.fixedColumns.min.js"></script>
    <!-- END PAGE LEVEL JS-->

