<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Header Layout end -->
<!-- BEGIN: SideNav-->
<x-sidebar-layout></x-sidebar-layout>
<!-- END: SideNav-->
<style>
   /* ::-webkit-scrollbar {
   display: none;
   } */

   .select2-results__options{
    line-height: 0px;
   }
   input:focus::placeholder {
   color: transparent;
   }
   .select2-container--default .select2-selection--multiple:before {
   content: ' ';
   display: block;
   position: absolute;
   border-color: #888 transparent transparent transparent;
   border-style: solid;
   border-width: 5px 4px 0 4px;
   height: 0;
   right: 6px;
   margin-left: -4px;
   margin-top: -2px;top: 50%;
   width: 0;cursor: pointer
   }
   .select2-container--open .select2-selection--multiple:before {
   content: ' ';
   display: block;
   position: absolute;
   border-color: transparent transparent #888 transparent;
   border-width: 0 4px 5px 4px;
   height: 0;
   right: 6px;
   margin-left: -4px;
   margin-top: -2px;top: 50%;
   width: 0;cursor: pointer
   }

  .collapsible-header:hover {
    background-color: orange !important; 
    /* #3FCFC8 */
  }

   
</style>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- BEGIN: Page Main class="main-full"-->
<!-- <div id="container1"><div id="container2"> -->
<div id="main" class="main-full" style="min-height: auto">
<div class="row">
   <!--             
      <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
           
               <span class="userselect">ADD COMPANY MASTER</span><hr> 
       </div> -->
   <div class="collapsible-body"  id='budget_loan' style="display:block" >
      <div class="row" style="font-weight: 600;margin-top: -30px;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
         <span class="userselect">@if(isset($company_id) && !empty($company_id))  UPDATE  GROUP MASTER &nbsp;&nbsp;(&nbsp;G-{{ str_pad($company_id,3,'0',STR_PAD_LEFT) }}&nbsp;)@else ADD COMPANY MASTER @endif </span>
      </div>
      <br>
   @if(isset($company_id1) && !empty($company_id1)) id=")">
      <form method="post" id="update_company_form" enctype="multipart/form-data">
      <input text="hidden" name="company_id1" value="{{$company_id1}}" style="display:none">
   @else
      <form method="post" id="add_company_form" enctype="multipart/form-data">
   @endif
   @csrf
      <div class="row">
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Group Name </label>
            <input type="text" class="validate" name="group_name" id="group_name"   placeholder="Enter" @if(isset($group_name)) value="{{ $group_name }}" @endif  >
                                    
         </div>

         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Key member Name & Number </label>
            <input type="text" class="validate" name="key_member"  id="key_member"    placeholder="Enter" @if(isset($key_member)) value="{{ $key_member }}" @endif  >                             
         </div>

         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Reg. Office Address</label>
            <input type="text" class="validate" name="reg_office_address" id="reg_office_address"    placeholder="Enter"  @if(isset($regd_office_address)) value="{{ $regd_office_address }}" @endif >                             
         </div>
         <div class="input-field col l3 m4 s12 display_search">
            <label for="contactNum1" class="dopr_down_holder_label active">Office Phone No.</label>
            <div  class="sub_val no-search">
               <select  id="office_phone_extension" name="office_phone_extension" class="browser-default">
                  @foreach($country_code as $cou)
                  <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                  @endforeach
               </select>
            </div>
            <input type="text" class="validate mobile_number" name="office_phone" id="office_phone"  placeholder="Enter" @if(isset($office_phone)) value="{{ $office_phone }}" @endif >
         </div>
      </div>
      <br>
   <div class="row">
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Office Email Address </label>
            <input type="email" class="validate" name="office_email_id" id="office_email_id"    placeholder="Enter" @if(isset($office_email_id)) value="{{ $office_email_id }}" @endif  >                             
         </div>
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Helpdesk email Address </label>
            <input type="email" class="validate" name="help_desk_email" id="help_desk_email"    placeholder="Enter" @if(isset($help_desk_email)) value="{{ $help_desk_email }}" @endif >                             
         </div>

         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Website  Address </label>
            <input type="text" class="validate" name="web_site_url"  id="web_site_url"    placeholder="Enter" @if(isset($web_site_url)) value="{{ $web_site_url }}" @endif >                             
         </div>

         <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
            <ul class="collapsible" style="margin-top: -2px;width: 321px;">
               <li onClick="hideShowAward()">
               <div class="collapsible-header"  id="col_award" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Awards & Recognition</h5> </div>               
               </li>                                        
            </ul>
         </div>     
      </div>
   
      <div class="row" style="display:none" id="awadard_div">
      <br>
         <!-- <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
         <div class="clonable" data-ss="1"> -->
            <div class="row">
                  <input type="hidden" name="award_id" id="award_id">
                  <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                     <label  class="active">Name Awards & Recognition </label>
                     <input type="text" class="validate" name="awards_reco_name" id="awards_reco_name"    placeholder="Enter"  >                             
                  </div>

                  <div class="input-field col l2 m4 s12 display_search">
                     <label  class="active">Images </label>
                     <input type="file" name="awards_reco_image[]"  id="awards_reco_image" class="dropify" multiple="true" data-default-file="" style="padding-top: 14px;">                     
                  </div>

                  <div class="input-field col l2 m4 s12" id="InputsWrapper2">
                     <label  class="active">Year </label>
                     <input type="number" min="0" class="validate" name="awards_reco_year" id="awards_reco_year"    placeholder="Enter"  >                             
                  </div>

                  <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                     <label  class="active">Award’s Description </label>
                     <input type="text" class="validate" name="awards_reco_desc" id="awards_reco_desc"    placeholder="Enter"  >                             
                  </div>

                  <div class="input-field col l2 m4 s12" id="InputsWrapper2">

                  <div class="row" >
                     <div class="input-field col l2 m2 s6 display_search">
                     <div class="preloader-wrapper small active" id="loader2" style="display:none">
                        <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                           <div class="circle"></div>
                        </div><div class="gap-patch">
                           <div class="circle"></div>
                        </div><div class="circle-clipper right">
                           <div class="circle"></div>
                        </div>
                        </div>
                     </div>
                  </div></div>

                     <div class="row" id="submitAward">
                     <div class="col l6 m6">
                        <a href="javascript:void(0)" onClick="saveAwards()" class="save_awards btn-small  waves-effect waves-light green darken-1 " id="save_awards">Save</a>
                     </div>
                     <div class="col l6 m6">
                         <a href="javascript:void(0)" onClick="clear_awards()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_awards"><i class="material-icons dp48">clear</i></a>
                     </div>
                     </div>
                  

                  </div>

                 
            </div>
         <br>
            <div class="row" >
                    <table class="bordered"  id="award_table">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Sr.No </th>
                        <th>Year </th>
                        <th>Name of Award</th>
                        <th>Award’s Description</th>
                        <th>Image</th>
                        <th>Last Updated Date</th>
                        <th>Action</th>               
                        </tr>
                        </thead>
                        <tbody>
                        @php $incID1 = count($getAwardData); @endphp
                        @if(isset($getAwardData) && !empty($getAwardData))
                        @php $i2=count($getAwardData); @endphp
                       
                        @foreach($getAwardData as $getAwardData)
                        <tr id="a_{{$getAwardData->id}}">
                           <td> @php $incID = $i2--; @endphp {{$incID}}</td>
                           <td id="y_{{$getAwardData->id}}">{{$getAwardData->awards_reco_year}}</td>
                           <td id="n_{{$getAwardData->id}}">{{$getAwardData->awards_reco_name}}</td>
                           <td id="d_{{$getAwardData->id}}">{{$getAwardData->awards_reco_desc}}</td>
                          
                           <td id="i_{{$getAwardData->id}}" style="width: 300px">
                              <div style="overflow-y: scroll;height: 55px;">
                                    @if(isset($getAwardData->awards_reco_image) && !empty($getAwardData->awards_reco_image))
                                       @php $awards_reco_image = json_decode($getAwardData->awards_reco_image); $i=1; @endphp 
                                       @if(is_array($awards_reco_image))
                                       @foreach($awards_reco_image as $awards_reco_image)
                                          @php $x = pathinfo($awards_reco_image, PATHINFO_FILENAME); @endphp
                                          <span class="{{$x}}"><a href="{{ asset('awards_reco_image/'.$awards_reco_image) }}"  target="_blank"> Image {{$i++}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImageAward('{{$awards_reco_image}}#{{$getAwardData->id}}')">X</a> &nbsp; , </span>
                                    
                                       @endforeach    
                                       @endif
                                    @endif
                              </div>
                           </td>
                           <td id="ud1_{{$getAwardData->id}}">{{$getAwardData->updated_date}}</td>
                           
                           <td><a href="javascript:void(0)" onClick="updateAwards('{{$getAwardData->id}}')">Edit</a> | 
                              <!-- <a  id="l_{{$getAwardData->id}}"  href="#modal91"  class=" btn-edit-award waves-effect waves-light" onClick="alert( {{$getAwardData->id}},'{{$getAwardData->awards_reco_name}}','{{$getAwardData->awards_reco_year}}','{{$getAwardData->awards_reco_desc}}','{{$getAwardData->awards_reco_image}}' )"   >Edit</a> | -->
                           <!-- <a href="#modal9" id="add_com_state" modal91 class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a>  -->
                           <a id="ld_{{$getAwardData->id}}" href='javascript:void(0)' onClick="confirmDelAward('{{$getAwardData->id}}')"  >Delete</a></td>
                        </tr>
                        @endforeach
                        @endif
                          
                        </tbody>
                    </table>
                    <input type="hidden" id="incID" value="{{$incID1}}">
                  
            </div>

           
      
      </div>
      <br>
      <div class="row">

         <div class="input-field col l3 m4 s12 display_search">
               <!-- <select  id="physical_tat_period" name="physical_tat_period" class=" browser-default">
                  <option value="" disabled selected>Select</option>
                  <option value="1" @if(isset($tat_period)) @if($tat_period==1) selected @endif @endif >1</option>
                  <option value="2" @if(isset($tat_period)) @if($tat_period==2) selected @endif @endif >2</option>
                  <option value="3" @if(isset($tat_period)) @if($tat_period==3) selected @endif @endif >3</option>
                  <option value="4" @if(isset($tat_period)) @if($tat_period==4) selected @endif @endif >4</option>
                  <option value="5" @if(isset($tat_period)) @if($tat_period==5) selected @endif @endif >5</option>
               </select>
               <label class="active">Physical TAT Period(Days) </label>  -->

               <label  class="active">Physical TAT Period(Days) </label>
            <input type="number" class="validate" min="0" max="150" name="physical_tat_period"  id="physical_tat_period"      placeholder="Enter" @if(isset($physical_tat_period)) value="{{ $physical_tat_period }}" @endif  >                             
            </div>



         <div class="input-field col l3 m4 s12 display_search">
            <!-- <select  id="tat_period" name="tat_period" class="browser-default">
               <option value="" disabled selected>Select</option>
               <option value="1" @if(isset($tat_period)) @if($tat_period==1) selected @endif @endif >1</option>
               <option value="2" @if(isset($tat_period)) @if($tat_period==2) selected @endif @endif >2</option>
               <option value="3" @if(isset($tat_period)) @if($tat_period==3) selected @endif @endif >3</option>
               <option value="4" @if(isset($tat_period)) @if($tat_period==4) selected @endif @endif >4</option>
               <option value="5" @if(isset($tat_period)) @if($tat_period==5) selected @endif @endif >5</option>
            </select>
            <label class="active">Virtual TAT Period(Days) </label>  -->
            <label  class="active">Virtual TAT Period(Days) </label>
            <input type="number" class="validate" min="0" max="150" name="tat_period" id="tat_period"    placeholder="Enter" @if(isset($tat_period)) value="{{ $tat_period }}" @endif  >                             
         </div>

         <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
            <ul class="collapsible" style="margin-top: -2px;width: 321px;">
               <li onClick="hideShowPros()">
               <div class="collapsible-header"  id="col_pros_cons"  > <h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Company Pros & Cons</h5></div>               
               </li>                                        
            </ul>
         </div> 
         
       <script>
            // function ratingSelect(){
            //    //alert(this.value)
            // }
       </script>
         <div class="input-field col l3 m4 s12 display_search">
            <div class="row">
                  <div class=" col l12 m4 s12 input-field display_search "> 
                        <select  id="company_rating" name="company_rating"   class="validate  browser-default">
                           <!--  onChange="ratingSelect(this)"-->
                        <option @if($company_rating=='') selected @endif ></option> 
                        <option value="1" @if($company_rating==1) selected @endif >1</option>
                        <option value="2" @if($company_rating==2) selected @endif  >2</option>
                        <option value="3" @if($company_rating==3) selected @endif  >3</option>
                        <option value="4" @if($company_rating==4) selected @endif  >4</option>
                        <option value="5" @if($company_rating==5) selected @endif  >5</option>
                     </select>
                     <label class="active">Company Rating</label>

                  </div>
                  <!-- <div class=" col l1 m4 s12 ">  
                        <a style="font-size:24px;border: none;background-color: transparent;margin-top: 7px" onClick="clearDropdown('company_rating')"  ><i class="material-icons dp48" style="color: green;margin-top: 10px;">refresh</i></a>
                  </div> -->
            </div>              
         </div>  
      </div>

      <div class="row" id="pros_cons_div" style="display:none">
      <br>
         <div class="row">
            <input type="hidden" id="company_pro_cons_id">
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <label  class="active">Company Pros </label>
               <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="company_pros" rows='20' col='40'></textarea> -->
               <input type="text" class="validate" name="company_pros" id="company_pros"    placeholder="Enter" @if(isset($company_pros)) value="{{ $company_pros }}" @endif  >                             
            </div>
            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
               <label  class="active">Company Cons </label>
               <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="company_cons" rows='20' col='40'></textarea> -->
               <input type="text" class="validate" name="company_cons" id="company_cons"    placeholder="Enter" @if(isset($company_cons)) value="{{ $company_cons }}" @endif  >                             
            </div> 

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">

                  <div class="row" >
                     <div class="input-field col l2 m2 s6 display_search">
                     <div class="preloader-wrapper small active" id="loader3" style="display:none">
                        <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                           <div class="circle"></div>
                        </div><div class="gap-patch">
                           <div class="circle"></div>
                        </div><div class="circle-clipper right">
                           <div class="circle"></div>
                        </div>
                        </div>
                     </div>
                  </div></div>

               <div class="row" id="submitPros">
               <div class="col l4 m4">
                  <a href="javascript:void(0)" onClick="saveProsCons()" class=" save_proscons btn-small  waves-effect waves-light green darken-1 " id="save_proscons">Save</a>
               </div>
               <div class="col l3 m3">
                     <a href="javascript:void(0)" onClick="clear_proscons()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_proscons"><i class="material-icons dp48">clear</i></a>
               </div>
               </div>
            </div> 

         </div> 
         
         <br>
            <div class="row">
                    <table class="bordered" id="pros_cons_table">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Sr No. </th>
                        <th>Pros</th>
                        <th>Cons</th>
                        <th>Last Updated Date</th>
                        <th>Action</th>               
                        </tr>
                        </thead>
                        <tbody>
                           @php $incPros1=count($getProsConsData); @endphp
                          @if(isset($getProsConsData))
                          @php $i3=count($getProsConsData); @endphp
                             @foreach($getProsConsData as $getProsConsData) 
                              <tr id="pcidtr_{{$getProsConsData->company_pro_cons_id}}">
                                 <td >@php $incPros= $i3--; @endphp {{$incPros}}</td>
                                 <td id="p_{{$getProsConsData->company_pro_cons_id}}">{{$getProsConsData->company_pros}}</td>
                                 <td id="c_{{$getProsConsData->company_pro_cons_id}}">{{$getProsConsData->company_cons}}</td>
                                 <td id="ud_{{$getProsConsData->company_pro_cons_id}}">{{$getProsConsData->updated_date}}</td>
                                 <td>
                                 <a href="javascript:void(0)"  class="waves-effect waves-light " onClick="updatePros({{$getProsConsData->company_pro_cons_id}})" >Edit</a> |
                                 <a href='javascript:void(0)'  onClick="confirmDelProsCons({{$getProsConsData->company_pro_cons_id}})"   >Delete</a>
                                    <!-- <a href='javascript:void(0)' onClick="alert({{$getProsConsData->company_pro_cons_id}})" >Edit</a> |<a href='javascript:void(0)' onClick="alert({{$getProsConsData->company_pro_cons_id}})" >Delete</a> -->
                                 </td>
                              </tr>    
                             @endforeach
                          @endif
                        </tbody>
                    </table>
                    <input type="hidden" id="incPros" value="{{$incPros1}}">
            </div>

            <br>
      </div>
      <br>
      <div class="row">
         <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
            <ul class="collapsible" style="margin-top: -2px;width: 321px;">
               <li onClick="hideShowPayment()">
               <div class="collapsible-header"  id="col_payment" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;"> Payment Process</h5></div>               
               </li>                                        
            </ul>
         </div>

      

         <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">CP empanelment agreement  </label>
            <input type="file" name="cp_empanelment_agreement"  id="cp_empanelment_agreement" multiple="true" class="dropify" data-default-file="" style="padding-top: 14px;">              

               @if(isset($cp_empanelment_agreement) && !empty($cp_empanelment_agreement))
                  @php $cp_empanelment_agreement = json_decode($cp_empanelment_agreement); $i=1; @endphp 
                  @if(is_array($cp_empanelment_agreement))
                  @foreach($cp_empanelment_agreement as $cp_empanelment_agreement)
                     @php $x = pathinfo($cp_empanelment_agreement, PATHINFO_FILENAME); @endphp
                     <span class="{{$x}}"><a href="{{ asset('cp_empanelment_agreement/'.$cp_empanelment_agreement) }}" target="_blank"> CP Agrement {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$cp_empanelment_agreement}}#cp')">X</a> &nbsp; , </span>
                  
                     @php $i++; @endphp 
                  @endforeach    
               @endif
               @endif

         </div>

         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">CP Code  </label>
            <input type="text" class="validate" name="cp_code" id="cp_code" maxlength="20"   placeholder="Enter" @if(isset($cp_code)) value="{{ $cp_code }}" @endif >                             
         </div>

         <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">Images</label>
            <input type="file" name="images[]" multiple="true" id="images" class="dropify" data-default-file="" style="padding-top: 14px;">
            @if(isset($images) && !empty($images))
               @php $images = json_decode($images); $i=1; @endphp 
               @if(is_array($images))
               @foreach($images as $images)
               @php $x = pathinfo($images, PATHINFO_FILENAME); @endphp
               <span class="{{$x}}"><a href="{{ asset('images/'.$images) }}" target="_blank"> Images {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$images}}#img')">X</a> &nbsp; , </span>
                
                  @php $i++; @endphp 
               @endforeach    
            @endif
            @endif
         </div>

      </div>

      <div class="row" id="payment_div" style="display:none">
      <br>
         <div class="row">
            <div class="input-field col l3 m4 s12 display_search">
               <label  class="active">Builder Invoice/Proforma  format</label>
               <input type="file" name="builder_invoice_format"  id="builder_invoice_format" multiple="true" class="dropify" data-default-file="" style="padding-top: 14px;">

                  @if(isset($builder_invoice_format) && !empty($builder_invoice_format))
                     @php $builder_invoice_format = json_decode($builder_invoice_format); $i=1; @endphp 
                     @if(is_array($builder_invoice_format))
                     @foreach($builder_invoice_format as $builder_invoice_format)
                        @php $x = pathinfo($builder_invoice_format, PATHINFO_FILENAME); @endphp
                        <span class="{{$x}}"><a href="{{ asset('builder_invoice_format/'.$builder_invoice_format) }}" target="_blank"> Builder Invoice {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$builder_invoice_format}}#bif')">X</a> &nbsp; , </span>
                    
                        @php $i++; @endphp 
                     @endforeach    
                  @endif
                  @endif
            </div>     

            <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
            <label  class="active">Payment TAT Period </label>
         
            <input type="text" class="validate" name="payment_tat_period" id="payment_tat_period"    placeholder="Enter" @if(isset($payment_tat_period)) value="{{ $payment_tat_period }}" @endif >                             
         </div>

           
            <div class="input-field col l3 m4 s12 display_search">
               <label  class="active">Builder Payment Process</label>
               <input type="file" name="builder_payment_process"  id="builder_payment_process" multiple="true" class="dropify" data-default-file="" style="padding-top: 14px;">
                  
                  @if(isset($builder_payment_process) && !empty($builder_payment_process))
                     @php $builder_payment_process = json_decode($builder_payment_process); $i=1; @endphp 
                     @if(is_array($builder_payment_process))
                     @foreach($builder_payment_process as $builder_payment_process)
                        @php $x = pathinfo($builder_payment_process, PATHINFO_FILENAME); @endphp
                        <span class="{{$x}}"><a href="{{ asset('builder_payment_process/'.$builder_payment_process) }}" target="_blank"> Payment Process {{$i}}</a>&nbsp; <a href="javascript:void(0)" style="color: red;" onClick="delImage('{{$builder_payment_process}}#bpp')">X</a> &nbsp; , </span>
                   
                        @php $i++; @endphp 
                     @endforeach    
                  @endif
                  @endif
            </div>
         </div>    
        
      </div>
      <br>
      <div class="row">
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
            <label  class="active">Comments </label>
            <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea> -->
            <input type="text" class="validate" name="comments" id="comments"    placeholder="Enter" @if(isset($comments)) value="{{ $comments }}" @endif >                             
         </div>

         <div class="input-field col l3 m4 s12 display_search" style="margin-bottom: -11px;">
            <ul class="collapsible" style="margin-top: -2px;width: 321px;">
               <li onClick="ShowHidehierarchy()">
               <div class="collapsible-header"  id="col_hierarchy" ><h5 style="font-size: 13px;line-height: 110%; margin: -0.18rem 0 0.356rem 0;">Company Hierarchy</h5></div>               
               </li>                                        
            </ul>
         </div>



      </div>
      
      <div class="row" id="hierarchy_div" style="display:none">
      <br>
      <!-- <hr> -->
                    
                <!-- <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
				<div class="clonable" data-ss="1"> -->
                  <div class="row">
                    <input type="hidden" id="company_hirarchy_id" >
                     <div class="col l11">
                                <div class="row">
                                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                    <label for="cus_name active" class="dopr_down_holder_label active">Name 
                                    </label>
                                    <div  class="sub_val no-search">
                                        <select  id="name_initial" name="name_initial" class="browser-default ">
                                        @foreach($initials as $ini)
                                            <option value="{{$ini->initials_id}}">{{ucfirst($ini->initials_name)}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <input type="text" class="validate mobile_number" name="name_h" id="name_h"   placeholder="Enter"  onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)"  >
                                </div>

                          <div class="input-field col l3 m4 s12" id="InputsWrapper">
                              <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: </label>
                              <div  class="sub_val no-search">
                                  <select  id="country_code" name="country_code" class="browser-default">
                                    @foreach($country_code as $cou)
                                       <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                    @endforeach
                                  </select>
                              </div>
                              <input type="number" min="0"  class="validate mobile_number" name="mobile_number_h" id="mobile_number_h"  placeholder="Enter" >
                              <!-- maxlength="10" -->
                          </div>

                          <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <div  class="sub_val no-search">
                                <select  id="country_code_w" name="country_code_w" class="browser-default">
                                 @foreach($country_code as $cou)
                                    <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                  @endforeach
                                </select>
                            </div>
                            <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: </label>
                            <input type="text" min="0" class="validate mobile_number" name="wap_number" id="wap_number"  placeholder="Enter" >
                            <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                            </div>
                          </div> 

                          <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                            <label for="primary_email active" class="active">Email Address: </label>
                            <input type="text" class="validate" name="primary_email_h" id="primary_email_h"  placeholder="Enter" >
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col l3 m4 s12 display_search">                    
                              <select class=" browser-default designation_h"  data-placeholder="Select" name="designation_h" id="designation_h">
                                  <option value=""  selected>Select</option>
                                  @foreach($designation as $des)
                                    <option value="{{$des->designation_id}}">{{ucfirst($des->designation_name)}}</option>
                                  @endforeach
                              </select>
                              <label for="possession_year" class="active">Designation </label>
                              <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                              <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                              </div>
                          </div>

                          <div class="input-field col l3 m4 s12 display_search">                    
                              <select class="  browser-default"  data-placeholder="Select" name="reporting_to_h" id="reporting_to_h">
                                  <option value=""  selected>Select</option>
                                  @foreach($company_hirarchy as $hei)
                                    <option value="{{$hei->company_hirarchy_id}}">{{ucfirst($hei->name)}}</option>
                                  @endforeach
                              </select>
                              <label for="possession_year" class="active">Reporting to </label>                           
                          </div>

                          <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                            <label  class="active">Comments: </label>
                            <input type="text" name="comments_h"  id="comments_h" placeholder="Enter">
                          </div>

                          <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Upload Photo : </label>
                            <input type="file" name="photo_h"  id="photo_h" class="dropify" data-default-file="" style="padding-top: 14px;">
                          </div>
                        </div>
                            
                      </div>

                      <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        
                      <div class="row" >
                        <div class="input-field col l2 m2 s6 display_search">
                        <div class="preloader-wrapper small active" id="loader4" style="display:none">
                           <div class="spinner-layer spinner-green-only">
                           <div class="circle-clipper left">
                              <div class="circle"></div>
                           </div><div class="gap-patch">
                              <div class="circle"></div>
                           </div><div class="circle-clipper right">
                              <div class="circle"></div>
                           </div>
                           </div>
                        </div>
                     </div></div>

                       <div class="row" id="submitHei" style="margin-left: 10px;">
                           <a href="javascript:void(0)" onClick="saveHierarchy()" class=" save_heirarchy btn-small  waves-effect waves-light green darken-1 " id="save_heirarchy">Save</a>
                          
                           <a href="javascript:void(0)" onClick="clear_heirarchy()" class="btn-small  waves-effect waves-light red darken-1 " id="clear_heirarchy" style="margin-top:5px"><i class="material-icons dp48">clear</i></a>
                        </div>

                        </div>

                       
                  </div>
                  <br>
                  <div class="row">
                          <table class="bordered" id="heirarchy_table">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Sr no.</th>
                        <th>Name </th>                        
                        <th>Mobile No.</th>
                        <th>Whatsapp No.</th>
                        <th>Email Id</th>
                        <th>Designation</th>
                        <th>Reporting to</th>
                        <th>Comment</th>
                        <th>Photo</th>
                        <th>Last Updated Date</th>
                        <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @php $incHei1= count($getHirarchyData); @endphp
                        @if(isset($getHirarchyData))
                        @php $i4=count($getHirarchyData); @endphp
                        @foreach($getHirarchyData as $getHirarchyData)
                           <tr id="chid_{{$getHirarchyData->company_hirarchy_id}}">
                              <td >@php $incHei= $i4--; @endphp {{$incHei}}</td>
                              <td id="n_{{$getHirarchyData->company_hirarchy_id}}">{{ucwords($getHirarchyData->initials_name)}} {{$getHirarchyData->name}}</td>
                              <td id="m_{{$getHirarchyData->company_hirarchy_id}}">{{$getHirarchyData->mobile_number}}</td>
                              <td id="w_{{$getHirarchyData->company_hirarchy_id}}">{{$getHirarchyData->whatsapp_number}}</td>
                              <td id="e_{{$getHirarchyData->company_hirarchy_id}}">{{$getHirarchyData->email_id}}</td>
                              <td id="d_{{$getHirarchyData->company_hirarchy_id}}">{{$getHirarchyData->designation_name}}</td>          
                              <td id="r_{{$getHirarchyData->company_hirarchy_id}}">{{$getHirarchyData->reporting_to}}</td>
                              <td id="c_{{$getHirarchyData->company_hirarchy_id}}">{{$getHirarchyData->comments}}</td>
                              <td id="p_{{$getHirarchyData->company_hirarchy_id}}">                              
                                    @if(isset($getHirarchyData->photo) && !empty($getHirarchyData->photo))
                                       <span class="{{$getHirarchyData->company_hirarchy_id}}"><a href="{{ asset('photo_h/'.$getHirarchyData->photo) }}" target="_blank"> Photo</a>&nbsp;<a href="javascript:void(0)" style="color: red;" onClick="delImageHei('{{$getHirarchyData->company_hirarchy_id}}')">X</a>  </span>
                                    @endif                                
                              </td>
                              <td id="ud2_{{$getHirarchyData->company_hirarchy_id}}">{{$getHirarchyData->updated_date}}</td>
                              <td><a action="javascript:void(0)" onClick="openModal({{$getHirarchyData->company_hirarchy_id}})">View</a> | <a action="javascript:void(0)" onClick="updateHierarchy({{$getHirarchyData->company_hirarchy_id}})">Edit</a> | <a action="javascript:void(0)" onClick="confirmDelHirarchy({{$getHirarchyData->company_hirarchy_id}})">Delete</a></td>     
                        
                           </tr>                         
                        @endforeach
                    @endif
                        </tbody> 
                    </table> 
                     <input type="hidden" id='incHei' value="{{$incHei1}}">
                  </div>                  
        
               
                </div>
        </div>




      <div class="row">   <div class="input-field col l6 m6 s6 display_search">
         <div class="alert alert-danger print-error-msg" style="display:none;color:red">
           <ul></ul>
        </div>
      </div>  </div> 
   <div class="row" >
      <div class="input-field col l2 m2 s6 display_search">
      <div class="preloader-wrapper small active" id="loader1" style="display:none">
         <div class="spinner-layer spinner-green-only">
         <div class="circle-clipper left">
            <div class="circle"></div>
         </div><div class="gap-patch">
            <div class="circle"></div>
         </div><div class="circle-clipper right">
            <div class="circle"></div>
         </div>
         </div>
      </div>
   </div></div>

   
      <div class="row" id="submitButton" style="margin-left: 25px;">
         <input type="hidden" name="company_id2" id="company_id2" @if(isset($company_id)) value="{{$company_id}}" @endif>
         <div class="input-field col l1 m1 s6 display_search">
            <a class="btn-small  waves-effect waves-light green darken-1" onClick="saveForm()" action="javascript:void(0)"> @if(isset($company_id) && !empty($company_id)) Update @else Save @endif</a>
         </div>
         <div class="input-field col l2 m2 s6 display_search">
            <a href="/company-master" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
         </div>
      </div>
    </form>  
      <br>
   </div>
   <div class="content-overlay"></div>
</div>


<!-- Modal Delete user delete-branch-->
<!-- <form action="javascript:void(0)" method="post" id="delete-franchise"> -->
        <div class="modal" id="deleteAwardModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-content">
               <h5 style="text-align: center">Delete record</h5>
               <hr>        
               <form method="post" id="delete_award" >
                  <input type="hidden" class="award_id2" id="award_id2" name="award_id">
                  <input type="hidden" class="form_type" id="form_type" name="form_type" value="award_reco">
                  <div class="modal-body">
                     
                     <h5>Are you sure want to delete this record?</h5>
                  
                  </div>
            
                   </div>
            
               <div class="modal-footer" style="text-align: left;">
                     <div class="row">
                     <div class="input-field col l2 m2 s6 display_search">
                           <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                           <a href="javascript:void(0)" onClick="deleteAward()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                     </div>    
                     
                     <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                           <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                     </div>    
                  </div> 
               </div>
           </form>
        </div>

        <div class="modal" id="deleteProsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-content">
               <h5 style="text-align: center">Delete record</h5>
               <hr>        
               <form method="post" id="delete_pros" >
                  <input type="hidden" class="company_pro_cons_id2" id="company_pro_cons_id2" name="company_pro_cons_id">
                  <input type="hidden" class="form_type" id="form_type" name="form_type" value="pros_cons">
                  <div class="modal-body">
                     
                     <h5>Are you sure want to delete this record?</h5>
                  
                  </div>
            
                   </div>
            
               <div class="modal-footer" style="text-align: left;">
                     <div class="row">
                     <div class="input-field col l2 m2 s6 display_search">
                           <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                           <a href="javascript:void(0)" onClick="deletePros()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                     </div>    
                     
                     <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                           <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                     </div>    
                  </div> 
               </div>
           </form>
        </div>

        <div class="modal" id="deleteHeirarchyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-content">
        <h5 style="text-align: center">Delete record</h5>
        <hr>        
        <form method="post" id="delete_hirarchy" >
            <input type="hidden" class="company_hirarchy_id2" id="company_hirarchy_id2" name="company_hirarchy_id">
            <input type="hidden"  id="form_type" name="form_type" value="hirarchy">
            <div class="modal-body">
                
                <h5>Are you sure want to delete this record?</h5>
            
            </div>
    
            </div>
    
        <div class="modal-footer" style="text-align: left;">
                <div class="row">
                <div class="input-field col l2 m2 s6 display_search">
                    <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                    <a href="javascript:void(0)" onClick="deleteHirarchy()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                </div>    
                
                <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div> 
        </div>
    </form>
</div>
<!-- </form> -->
    <!-- End Modal Delete user-->



<!-- Modal Designation Structure -->
<div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation: </label>
                    <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="errors" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>



<div id="modal10" class="modal">
            <div class="modal-content">
            <h5 style="text-align: center"> Flow Chart</h5>
            
            <hr>
            <html>
        <head>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
    
      function drawChart() {


        $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
        var a = $('#phi').val();
        var company_id = $('#company_id2').val();
                $.ajax({
      url:"/flow_chart_company",//add_project_master
      method:"GET",
      data: {company_id:company_id},
        //   contentType: false,
        //   cache: false,
        //   processData: false,
        success:function(resp)
        {
            // alert(resp)
            console.log(resp);
            var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');
        // data.addRows([
        //   [{'v':'Mike', 'f':'Mike<div style="color:red; font-style:italic">President</div>'},
        //    'Mike', ''],
        //   [{'v':'Jim', 'f':'Jim<div style="color:red; font-style:italic">Vice President</div>'},
        //    'Mike', 'VP'],

        //    [{'v':'Alice', 'f':'Alice<div style="color:red; font-style:italic">Vice Alice</div>'},
        //    'Mike', 'VP'],

        //    [{'v':'Four',"f":"four<div style='color:red; font-style:italic'>SE</div>"},"Mike",""],

        // //   ['Alice', 'Mike', ''],
        //   ['Bob', 'Jim', 'Bob Sponge'],
        //   ['Carol', 'Bob', '']
        // ]);

        data.addRows(
            // ['', 'Mike', ''],
            JSON.parse(resp)
            
        );

        // Create the chart.
        var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
        // Draw the chart, setting the allowHtml option to true for the tooltips.
        chart.draw(data, {'allowHtml':true});
            // data.addRows(data1);
        },
        error:function(params) {
            console.log(params);
        }
        });

         
        
      }
     </script>
            </head>
        <body>
            <div id="chart_div"></div>
        </body>
        </html>


                <br>
                <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>
                </div>
                <br>
                
    </div>

      
<script>

   function delImage(params) {
      const myArray = params.split("#");
      var image = myArray[0];
      var type = myArray[1];
      // alert(myArray[1]); return;
      // alert('#'+params);//delImage
      if (confirm("Are you sure you want to delete this image?")) {
         var company_id = $('#company_id2').val();
         const file = image;
         const filename = file.split('.').slice(0, -1).join('.');
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delImage",
            method:"POST",
            data: {image:image,company_id:company_id,type:type},
            // contentType: false,
            // cache: false,
            // processData: false,
            success:function(data)
            {
               $('.'+filename).remove();
               console.log(data);
            }
         });
      }else{
         return false;
      }
   }

   function delImageHei(params) {
      if (confirm("Are you sure you want to delete this image?")) {
         var company_id = $('#company_id2').val();
         // const file = params;
         // const filename = file.split('.').slice(0, -1).join('.');
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delImageHei",
            method:"POST",
            data: {company_hirarchy_id:params,company_id:company_id},
           
            success:function(data)
            {
               $('.'+params).remove();
               console.log(data);
            }
         });
      }else{
         return false;
      }
   }

   function delImageAward(params) {

      const myArray = params.split("#");
      var image = myArray[0];
      var id = myArray[1];
      
      if (confirm("Are you sure you want to delete this image?")) {
         var company_id = $('#company_id2').val();
         const file = image;
         const filename = file.split('.').slice(0, -1).join('.');
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delImageAward",
            method:"POST",
            data: {id:id,image:image},
           
            success:function(data)
            {
               $('.'+filename).remove();
               console.log(data);
            }
         });
      }else{
         return false;
      }
      
   }

function openModal(param) {
    google.charts.load('current', {packages:["orgchart"]});
      google.charts.setOnLoadCallback(drawChart);

   //  $('#phi').val(param);
    // $('#modal10').load(' #modal10');
      $('#modal10').modal('open');
      
      // $('#deleteProsModal_r1').modal('open');
} 

function clearDropdown(param){
        dropdownID = param;
        $('#'+dropdownID).val('').trigger('change'); 

    }
function saveAwards() {
      var awards_reco_name = $('#awards_reco_name').val();
      var company_id1 = $('#company_id2').val();
      
      if(awards_reco_name==''){
         $('#awards_reco_name').css('border-color','red');
         return false;
      }else{
         $('#awards_reco_name').css('border-color','');
      }
      $('#loader2').css('display','block');
      $('#submitAward').css('display','none');
      
      var form_data = new FormData();
      // var ext = name.split('.').pop().toLowerCase();   
      var totalfiles = document.getElementById('awards_reco_image').files.length;
      for (var index = 0; index < totalfiles; index++) {
         form_data.append("awards_reco_image[]", document.getElementById('awards_reco_image').files[index]);
      }

      form_data.append("awards_reco_name", document.getElementById('awards_reco_name').value);
      form_data.append("awards_reco_year", document.getElementById('awards_reco_year').value);
      form_data.append("awards_reco_desc", document.getElementById('awards_reco_desc').value);
      form_data.append("form_type", 'award_reco');
      form_data.append("company_id1", company_id1);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
         url:"/add-company-master",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            console.log( data);//return;
            var incID = parseInt($('#incID').val()) + 1;
            $('#company_id2').val(data.company_id);         

            var html = '<tr id="a_'+data.award_id+'">';
            html += '<td >' +incID + '</td>'; 
            html += '<td id="y_'+data.award_id+'">' + data.awards_reco_year + '</td>'; 
            html += '<td id="n_'+data.award_id+'">' + data.awards_reco_name + '</td>'; 
            html += '<td id="d_'+data.award_id+'">' + data.awards_reco_desc + '</td>'; 
           
            // html += '<td id="i_'+data.award_id+'">' + data.awards_reco_image + '</td>'; 
         
         const img = data.awards_reco_image1;
          
         var myStringArray = img;//.reverse();
         // console.log(myStringArray);
         var result = "";
         // console.log( myStringArray.length);
         if (typeof  myStringArray != 'undefined') {
            img.forEach((item, index)=>{
               const file = item;
               const filename = file.split('.').slice(0, -1).join('.');
            result = result + "<span class='"+filename+"'> <a  target='_blank' href='awards_reco_image/" + item + "'>Image "+ [index] + "</a> &nbsp; <a href='javascript:void(0)' style='color: red;' onClick=delImageAward('"+file+"#"+data.award_id+"')>X</a> &nbsp; , </span> " ;
            })
         }
         
         // return;
         // for (var i = 0; i < myStringArray.length; i++) {
         //    result = result + " <a  target='_blank' href='awards_reco_image/" + myStringArray[i] + "'>Image "+ [i] + "</a>, ";
         // }
         html += '<td id="i_'+data.award_id+'">' + result + '</td>'; 
         html += '<td id="ud1_'+data.award_id+'">' + data.updated_date + '</td>'; 
         // console.log(result); 
        // $("#i_"+data.award_id).html(result);

         
            html += "<td> <a href='javascript:void(0)' onClick='updateAwards("+data.award_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelAward("+data.award_id+")'  >Delete</a></td>";
            html += '</tr>';
            $('#award_table').prepend(html);
            $('#awards_reco_image').val('');
            $('#awards_reco_name').val('');
            $('#awards_reco_year').val('');
            $('#awards_reco_desc').val('');  
            $('#incID').val(incID);  
            $('#loader2').css('display','none');
            $('#submitAward').css('display','block');  
         
         }
      });

}

function clear_awards() {
   $('#awards_reco_image').val('');
   $('#awards_reco_name').val('');
   $('#awards_reco_year').val('');
   $('#awards_reco_desc').val(''); 
   $('#award_id').val(''); 
   var anchor=document.getElementById("save_awards");
   anchor.innerHTML="Save";
   $("#save_awards").attr("onclick","saveAwards()");
}

function updateAwards(val1) {
      var award_id = val1;
      var form_data = new FormData();
      form_data.append("form_type", 'award_reco');
      form_data.append("award_id", award_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
      $.ajax({
         url:"/getData",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
            {
               var data  = data[0];
               console.log(data);
               $('#award_id').val(data.id);
               $('#awards_reco_name').val(data.awards_reco_name);
               $('#awards_reco_year').val(data.awards_reco_year);
               $('#awards_reco_desc').val(data.awards_reco_desc);
               // $('#save_awards').val('Update');
               var anchor=document.getElementById("save_awards");
               anchor.innerHTML="Update";
               $("#save_awards").attr("onclick","updateAwards1()");
               // document.getElementById("save_awards").value="Close Curtain"; 
               // $('#modal91').modal('open');
            }
      });

}
function updateAwards1(){
   // $("#save_awards").prop('disabled', true);
   $('#loader2').css('display','block');
      $('#submitAward').css('display','none');
   
   var awards_reco_name = $('#awards_reco_name').val();
   var award_id = $('#award_id').val();  
   var form_data = new FormData();
   var ext = name.split('.').pop().toLowerCase();
   var totalfiles = document.getElementById('awards_reco_image').files.length;
   for (var index = 0; index < totalfiles; index++) {
      form_data.append("awards_reco_image[]", document.getElementById('awards_reco_image').files[index]);
   }
   form_data.append("awards_reco_name", document.getElementById('awards_reco_name').value);
   form_data.append("awards_reco_year", document.getElementById('awards_reco_year').value);
   form_data.append("awards_reco_desc", document.getElementById('awards_reco_desc').value);
   form_data.append("form_type", 'award_reco');
   form_data.append("award_id", award_id);
   $.ajaxSetup({
   headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
   });
  
   $.ajax({
      url:"/updateTableFormRecords",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         const img = data.data1['img'];
         console.log(data);
         var aid = data.award_id;
         console.log('#y_'+aid);
         $('#y_'+aid).html(data.data['awards_reco_year']);
         $('#n_'+aid).html(data.data['awards_reco_name']);
         $('#d_'+aid).html(data.data['awards_reco_desc']);
         $('#ud1_'+aid).html(data.data['updated_date']);
         if (typeof  data.data1['img'] != 'undefined') {
            var myStringArray = img.reverse();
         var result = ""
         for (var i = 0; i < myStringArray.length; i++) {
            const file = myStringArray[i];
            const filename = file.split('.').slice(0, -1).join('.');
            result = result + " <span class='"+filename+"'> <a  target='_blank' href='awards_reco_image/" + myStringArray[i] + "'>Image "+ [i] + "</a>&nbsp; <a href='javascript:void(0)' style='color: red;' onClick=delImageAward('"+file+"#"+aid+"')>X</a> &nbsp; , </span> ";
         }
         }
        
         $("#i_"+aid).html(result);
         // $('#modal91').modal('close');
         // $('#awards_reco_name1').val('');     
         // $('#awards_reco_year1').val('');
         // $('#awards_reco_desc1').val('');     
         // $("#save_awards").prop('disabled', false);
         $('#loader2').css('display','none');
            $('#submitAward').css('display','block'); 
         
      }
   });

}
function confirmDelAward(params) {
    var award_id = params;
    $('.award_id2').val(award_id);   
    $('#deleteAwardModal').modal('open');
}
function deleteAward() {
   var url = 'deleteTableFormRecords';
    var form = 'delete_award';
    $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
      $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
             $('#'+form).serialize() ,      
        success: function(data) {     
           console.log(data);
         var aid = data.award_id;
         $('#a_'+aid).remove();        
         $('#deleteAwardModal').modal('close');            
        }
      });   

}
function saveProsCons() {
   var company_pros = $('#company_pros').val();
   var company_cons = $('#company_cons').val();
   var company_id1 = $('#company_id2').val();
   if(company_pros=='' && company_cons == ''){
      //$('#company_pros').css('border-color','red');
      return false;
   }

   $('#loader3').css('display','block');
   $('#submitPros').css('display','none');

   var form_data = new FormData();
   var ext = name.split('.').pop().toLowerCase();

      form_data.append("company_pros", document.getElementById('company_pros').value);
      form_data.append("company_cons", document.getElementById('company_cons').value);
      form_data.append("form_type", 'pros_cons');
      form_data.append("company_id1", company_id1);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/add-company-master",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data); //return;

         // const obj = JSON.parse(data);

         // console.log(obj); //return;
         $('#company_id2').val(data.company_id);
         // if(typeof obj['company_id'] !== 'undefined'){
         //    $('#company_id2').val(obj['company_id']);
         // }
         //$('#uploaded_image').html(data);
         var incPros = parseInt($('#incPros').val()) +1;
         var html = '<tr id="pcidtr_'+data.company_pro_cons_id+'">';
         html += '<td>' + incPros + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="p_'+data.company_pro_cons_id+'">' + data.company_pros + '</td>';// obj['company_pros'] +'</td>';
         html += '<td id="c_'+data.company_pro_cons_id+'">' + data.company_cons + '</td>';// obj['company_cons'] +'</td>';
         html += '<td id="ud_'+data.company_pro_cons_id+'">' + data.updated_date + '</td>';// obj['company_cons'] +'</td>';
         //html += "<td> <a href='#"+data.company_pro_cons_id+"' >Edit</a> |<a href='#"+data.company_pro_cons_id+"' >Delete</a></td>";
         html += "<td> <a href='javascript:void(0)' onClick='updatePros("+data.company_pro_cons_id+")' >Edit</a> | <a href='javascriot:void(0)' onClick='confirmDelProsCons("+data.company_pro_cons_id+")'   >Delete</a></td>";
         html += '</tr>';
         $('#pros_cons_table').prepend(html);
         $('#company_pros').val('');
         $('#company_cons').val('');
         $('#incPros').val(incPros);
         $('#loader3').css('display','none');
         $('#submitPros').css('display','block'); 
      }
      });

}

function clear_proscons() {
   $('#company_pros').val('');
   $('#company_cons').val('');
   $('#company_pro_cons_id').val(''); 
   var anchor=document.getElementById("save_proscons");
   anchor.innerHTML="Save";
   $("#save_proscons").attr("onclick","saveProsCons()");
}

function updatePros(param) {
   var company_pro_cons_id = param;
      var form_data = new FormData();
      form_data.append("form_type", 'pros_cons');
      form_data.append("company_pro_cons_id", company_pro_cons_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
      $.ajax({
         url:"/getData",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
            {
               var data  = data[0];
               console.log(data);
               $('#company_pro_cons_id').val(data.company_pro_cons_id);
               $('#company_pros').val(data.company_pros);
               $('#company_cons').val(data.company_cons);
               // $('#modal92').modal('open');
               var anchor=document.getElementById("save_proscons");
               anchor.innerHTML="Update";
               $("#save_proscons").attr("onclick","updatePros1()");
            }
      });
}
function updatePros1() {
   var company_pro_cons_id = $('#company_pro_cons_id').val();
   var company_pros = $('#company_pros').val();
   var company_cons = $('#company_cons').val();

   $('#loader3').css('display','block');
   $('#submitPros').css('display','none');

      var form_data = new FormData();
      var ext = name.split('.').pop().toLowerCase();

      form_data.append("company_pros", company_pros);
      form_data.append("company_cons", company_cons);
      form_data.append("company_pro_cons_id", company_pro_cons_id);
      form_data.append("form_type", 'pros_cons');

      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });

      $.ajax({
         url:"/updateTableFormRecords",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            // console.log(data); return;
            var aid = data.company_pro_cons_id;
            $('#pcid_'+aid).html(data.data['company_pro_cons_id']);
            $('#p_'+aid).html(data.data['company_pros']);
            $('#c_'+aid).html(data.data['company_cons']);
            $('#ud_'+aid).html(data.data['updated_date']);
            // $('#modal92').modal('close');
            // $('#company_pros1').val('');
            // $('#company_cons1').val('');   
            $('#loader3').css('display','none');
             $('#submitPros').css('display','block');         
         }
      });

}
function confirmDelProsCons(params) {
    var company_pro_cons_id = params;
    $('.company_pro_cons_id2').val(company_pro_cons_id);   
    $('#deleteProsModal').modal('open');
}
function deletePros() {

   var url = 'deleteTableFormRecords';
   var form = 'delete_pros';
   $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
   });
   $.ajax({
      url:"/"+url,
      type:"POST",
      data:                     
            $('#'+form).serialize() ,      
      success: function(data) {
         console.log(data);
         ////$("#pros_cons_table").load(window.location + " #pros_cons_table");    
         var aid = data.company_pro_cons_id;
         $('#pcidtr_'+aid).remove();
         // $('#deleteProsModal').hide();
         $('#deleteProsModal').modal('close');
         // var incPros = parseInt($('#incPros').val()) - 1;
         // $('#incPros').val(incPros);

         console.log(data);return;
            
      }
   });   


}

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}


function  saveHierarchy() {
   var company_id1 = $('#company_id2').val();
   // var company_id1 = $('#company_hirarchy_id').val();
   var name_initial  = $('#name_initial').val();
   var name_h  = $('#name_h').val();
   var country_code  = $('#country_code').val(); 
   var mobile_number_h  = $('#mobile_number_h').val();
   var country_code_w  = $('#country_code_w').val();
   var wap_number  = $('#wap_number').val();
   var primary_email_h  = $('#primary_email_h').val();
   var designation_h  = $('#designation_h').val();
   var reporting_to_h  = $('#reporting_to_h').val();
   var comments_h  = $('#comments_h').val();
   var photo_h  = $('#photo_h').val();

  
   

   if( name_h == ''){
      $('#name_h').css('border-color','red');
      return false;
   }else{
      $('#name_h').css('border-color','');
    
   }

   if(mobile_number_h.length != 10){
      $('#mobile_number_h').css('border-color','red');
      return false;
   }else{
      $('#mobile_number_h').css('border-color','');
    
   }

   if(IsEmail(primary_email_h)==false){
         // $('#primary_email_h').css('border-color','red');
         //  return false;
         primary_email_h = '';
         $('#loader4').css('display','none');
      $('#submitHei').css('display','block');
   }else{
      $('#primary_email_h').css('border-color','');
   }

   $('#loader4').css('display','block');
   $('#submitHei').css('display','none');

   

   var form_data = new FormData();   
   form_data.append("name_initial", name_initial);  
   form_data.append("name_h", name_h);  
   form_data.append("country_code", country_code);  
   form_data.append("mobile_number_h", mobile_number_h);  
   form_data.append("country_code_w", country_code_w);  
   form_data.append("wap_number", wap_number);  
   form_data.append("primary_email_h", primary_email_h);  
   form_data.append("designation_h", designation_h);  
   form_data.append("reporting_to_h", reporting_to_h);  
   form_data.append("comments_h", comments_h);  
   form_data.append("photo_h", document.getElementById('photo_h').files[0]);

      form_data.append("form_type", 'hierarchy');
      form_data.append("company_id1", company_id1);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/add-company-master",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data); //return;
         $('#company_id2').val(data.company_id);
         var value = data.company_hirarchy_id; 
         var option = data.name; 
         var newOption= new Option(option,value, true, false);
       
         $("#reporting_to_h").append(newOption);
         var incHei = parseInt($('#incHei').val()) + 1;
      var ini = data.initials_name;
      var html = '<tr  id="chid_'+data.company_hirarchy_id+'">';      
      html += '<td >' + incHei +'</td>'; 
      html += '<td  id="n_'+data.company_hirarchy_id+'">' +ini.charAt(0).toUpperCase() + ini.slice(1) +" "+ data.name +'</td>'; 
      html += '<td  id="m_'+data.company_hirarchy_id+'">' + data.mobile_number +'</td>'; 
      html += '<td  id="w_'+data.company_hirarchy_id+'">' + data.whatsapp_number +'</td>'; 
      html += '<td  id="e_'+data.company_hirarchy_id+'">' + data.email_id +'</td>'; 
      html += '<td  id="d_'+data.company_hirarchy_id+'">' + data.designation_name +'</td>'; 
      html += '<td  id="r_'+data.company_hirarchy_id+'">' + data.reports_name +'</td>'; 
      html += '<td  id="c_'+data.company_hirarchy_id+'">' + data.comments +'</td>'; 
      if(data.photo !=""){
      html += '<td  id="p_'+data.company_hirarchy_id+'"><span class="'+data.company_hirarchy_id+'"><a href='+'photo_h/'+ data.photo +' target="_blank">Photo</a>&nbsp;<a href="javascript:void(0)" style="color: red;" onClick="delImageHei('+data.company_hirarchy_id+')">X</a>  </span></td>'; 
      }else{
         html += '<td  id="p_'+data.company_hirarchy_id+'"></td>'; 
      }
      html += '<td  id="ud2_'+data.company_hirarchy_id+'">' + data.updated_date +'</td>'; 
      html += "<td><a href='javascript:void(0)' onClick='openModal("+data.company_hirarchy_id+")' >View</a> | <a href='javascript:void(0)' onClick='updateHierarchy("+data.company_hirarchy_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelHirarchy("+data.company_hirarchy_id+")' >Delete</a></td>";
      html += '</tr>';
      $('#heirarchy_table').prepend(html);
      $('#name_initial').val('');
      $('#name_h').val('');
      $('#country_code').val('');
      $('#mobile_number_h').val('');
      $('#country_code_w').val('');
      $('#wap_number').val('');
      $('#primary_email_h').val('');
      $('#reporting_to_h').val('').trigger('change');       
      $('#designation_h').val('').trigger('change');     
      $('#comments_h').val('');
      $('#photo_h').val('');
      $("#copywhatsapp").prop("checked", false);
      $('#incHei').val(incHei);
      $('#loader4').css('display','none');
      $('#submitHei').css('display','block');
   }
   });

}

function clear_heirarchy() {
   $('#company_hirarchy_id').val('');      
      $('#name_initial').val(1).trigger('change');       
      $('#name_h').val('');
      $('#country_code').val(1).trigger('change');       
      $('#mobile_number_h').val('');
      $('#country_code_w').val('');
      $('#wap_number').val('');
      $('#primary_email_h').val('');
      $('#reporting_to_h').val('').trigger('change');       
      $('#designation_h').val('').trigger('change');     
      $('#comments_h').val('');
      $('#photo_h').val('');
      $("#copywhatsapp").prop("checked", false);
     
   var anchor=document.getElementById("save_heirarchy");
   anchor.innerHTML="Save";
   $("#save_heirarchy").attr("onclick","saveHierarchy()");
}

function updateHierarchy(params) {
   var company_hirarchy_id = params;
   var form_data = new FormData();
      form_data.append("form_type", 'hierarchy');
      form_data.append("company_hirarchy_id", company_hirarchy_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
    $.ajax({
         url:"/getData",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
            
            var data  = data[0];
            console.log(data);
            $('#company_hirarchy_id').val(data.company_hirarchy_id);           
            $('#name_initial').val(data.name_initial).trigger('change');            
            $('#name_h').val(data.name);
            $('#country_code').val(data.country_code_id).trigger('change');            
            $('#mobile_number_h').val(data.mobile_number);
            $('#wap_number').val(data.whatsapp_number);
            $('#primary_email_h').val(data.email_id);
            $('#designation_h').val(data.designation_id).trigger('change');            
            $('#reporting_to_h').val(data.reports_to).trigger('change');            
            $('#comments_h').val(data.comments);

            var anchor=document.getElementById("save_heirarchy");
               anchor.innerHTML="Update";
               $("#save_heirarchy").attr("onclick","updateHierarchy1()");

            // $('#modal94').modal('open');
         }
      });

}

function updateHierarchy1() {

   var company_hirarchy_id1 = $('#company_hirarchy_id').val(); 
    var name_initial1  = $('#name_initial').val();
    var name_h1 = $('#name_h').val();
    var country_code1  = $('#country_code').val();
    var mobile_number_h1 = $('#mobile_number_h').val();
    var wap_number1 = $('#wap_number').val();
    var primary_email_h1 = $('#primary_email_h').val();
    var designation_h1 = $('#designation_h').val();
    var reporting_to_h1 = $('#reporting_to_h').val();
    var photo_h1 = $('#photo_h').val();
    var comment_h1 = $('#comments_h').val();
   //  var company_id = $('#company_id2').val();
   if( name_h1 == ''){
      $('#name_h1').css('border-color','red');
      return false;
    }
    if(IsEmail(primary_email_h1)==false){
      $('#primary_email_h').val();
      primary_email_h1 = '';
         $('#loader4').css('display','none');
      $('#submitHei').css('display','block');
   }else{
      $('#primary_email_h').css('border-color','');
   }

   

    $('#loader4').css('display','block');
   $('#submitHei').css('display','none');
   
    var form_data = new FormData(); 
       form_data.append('company_hirarchy_id1',company_hirarchy_id1);
       form_data.append('name_initial1',name_initial1);
       form_data.append('name_h1',name_h1);
       form_data.append('country_code1',country_code1);
       form_data.append('mobile_number_h1',mobile_number_h1);
       form_data.append('wap_number1',wap_number1);
       form_data.append('primary_email_h1',primary_email_h1);
       form_data.append('designation_h1',designation_h1);
       form_data.append('reporting_to_h1',reporting_to_h1);
       form_data.append("photo_h1", document.getElementById('photo_h').files[0]);
       form_data.append('comments_h1',comment_h1); 
       form_data.append("form_type", 'hierarchy');
       $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
       });
       $.ajax({
      url:"/updateTableFormRecords",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data);
         console.log(data.error);
         if(typeof data.error !== 'undefined'){
            $('#primary_email_h1').css('border-color','red');
            $('#loader4').css('display','none');
            $('#submitHei').css('display','block');
         }else{
         console.log(data); //return
         // $('#chid_'+data.company_hirarchy_id).html(data.data['company_hirarchy_id']);
         var ini = data.data['initials_name'];
         $('#n_'+data.company_hirarchy_id).html(ini.charAt(0).toUpperCase() + ini.slice(1) +" "+data.data['name']);
         $('#m_'+data.company_hirarchy_id).html(data.data['mobile_number']);
         $('#w_'+data.company_hirarchy_id).html(data.data['whatsapp_number']);
         $('#e_'+data.company_hirarchy_id).html(data.data['email_id']);
         $('#d_'+data.company_hirarchy_id).html(data.data['designation_name']);
         $('#r_'+data.company_hirarchy_id).html(data.data['reports_name']);
         $('#c_'+data.company_hirarchy_id).html(data.data['comments']);
         if( data.data['photo'] != ""){
            $('#p_'+data.company_hirarchy_id).html("<span class='"+data.company_hirarchy_id+"'><a href='photo_h/"+data.data['photo']+"' target='_blank' > Photo </a>&nbsp;<a href='javascript:void(0)' style='color: red;' onClick=delImageHei('"+data.company_hirarchy_id+"')>X</a>  </span> ");
         }else{
            $('#p_'+data.company_hirarchy_id).html("");
         }
         $('#ud2_'+data.company_hirarchy_id).html(data.data['updated_date']);
         // $('#modal94').modal('close');
         $('#loader4').css('display','none');
         $('#submitHei').css('display','block');
      }
      }
      });

   
}

function confirmDelHirarchy(params) {
   var company_hirarchy_id = params;
    $('.company_hirarchy_id2').val(company_hirarchy_id);   
    $('#deleteHeirarchyModal').modal('open');
   
}

function deleteHirarchy() {
   var url = 'deleteTableFormRecords';
   var form = 'delete_hirarchy';
   $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
   });
   $.ajax({
      url:"/"+url,
      type:"POST",
      data:                     
            $('#'+form).serialize() ,      
      success: function(data) {
         console.log(data);
         ////$("#pros_cons_table").load(window.location + " #pros_cons_table");    
         var aid = data.company_hirarchy_id;
         $('#chid_'+aid).remove();
         // $('#deleteProsModal').hide();
         $('#deleteHeirarchyModal').modal('close')
         console.log(data);return;
            
      }
   });   


}

function saveForm() {
   
  
   var group_name = $('#group_name').val();
   var key_member = $('#key_member').val();
   var reg_office_address = $('#reg_office_address').val();   
   var office_phone = $('#office_phone').val();
   var office_email_id = $('#office_email_id').val();
   var help_desk_email = $('#help_desk_email').val();
   var web_site_url = $('#web_site_url').val();
   var physical_tat_period = $('#physical_tat_period').val();
   var tat_period = $('#tat_period').val();
   var company_rating = $('#company_rating').val();
   var cp_code = $('#cp_code').val();
   var comments = $('#comments').val();
   var company_id1 = $('#company_id2').val();
   var payment_tat_period = $('#payment_tat_period').val();
   
   // alert(company_rating); return
   
   if(group_name == ''){
      $('#group_name').css('border-color','red');
      return false;
   }else{
      $('#group_name').css('border-color','');
   }
   // if(reg_office_address == ''){
   //    $('#reg_office_address').css('border-color','red');
   //    return false;
   // }else{
   //    $('#reg_office_address').css('border-color','');
   // }
   // if(tat_period == ''){
   //    $('#tat_period').css('border-color','red');
   //    return false;
   // }else{
   //    $('#tat_period').css('border-color','');
   // }

   if(IsEmail(office_email_id)==false){
         // $('#office_email_id').css('border-color','red');
         office_email_id = '';
         //  return false;
   }else{
      $('#office_email_id').css('border-color','');
   }

   if(IsEmail(help_desk_email)==false){
         // $('#help_desk_email').css('border-color','red');
          help_desk_email = '';
         
   }else{
      $('#help_desk_email').css('border-color','');
   }


   $('#loader1').css('display','block');
   $('#submitButton').css('display','none');
   var form_data = new FormData();  
   
   form_data.append('group_name', group_name);
   form_data.append('key_member', key_member);
   form_data.append('reg_office_address', reg_office_address);
   // form_data.append('office_phone_extension', office_phone_extension);
   form_data.append('office_phone', office_phone);
   form_data.append('office_email_id', office_email_id);
   form_data.append('help_desk_email', help_desk_email);
   form_data.append('web_site_url', web_site_url);
   form_data.append('physical_tat_period', physical_tat_period);
   form_data.append('tat_period', tat_period);
   form_data.append('company_rating', company_rating);
   form_data.append('cp_code', cp_code);
   form_data.append('comments', comments);
   form_data.append('payment_tat_period',payment_tat_period);

   var totalfiles1 = document.getElementById('cp_empanelment_agreement').files.length;
   for (var index1 = 0; index1 < totalfiles1; index1++) {
      form_data.append("cp_empanelment_agreement[]", document.getElementById('cp_empanelment_agreement').files[index1]);
   }

   var totalfiles2 = document.getElementById('images').files.length;
   for (var index2 = 0; index2 < totalfiles2; index2++) {
      form_data.append("images[]", document.getElementById('images').files[index2]);
   }
      
   var totalfiles3 = document.getElementById('builder_invoice_format').files.length;
   for (var index3 = 0; index3 < totalfiles3; index3++) {
      form_data.append("builder_invoice_format[]", document.getElementById('builder_invoice_format').files[index3]);
   }

   // var totalfiles4 = document.getElementById('builder_proforma_format').files.length;
   // for (var index4 = 0; index4 < totalfiles4; index4++) {
   //    form_data.append("builder_proforma_format[]", document.getElementById('builder_proforma_format').files[index4]);
   // }

   var totalfiles5 = document.getElementById('builder_payment_process').files.length;
   for (var index5 = 0; index5 < totalfiles5; index5++) {
      form_data.append("builder_payment_process[]", document.getElementById('builder_payment_process').files[index5]);
   }


   form_data.append("form_type", 'other_form');
      form_data.append("company_id1", company_id1);
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
         url:"/add-company-master",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {  
            console.log(data);
            console.log(data.company_id);// return;
            $('#company_id2').val(data.company_id); 
            var err = 'print-error-msg';

            if($.isEmptyObject(data.error)){
               // $('.success1').html(data.success);
               // alert(data.success);
               //  alert(data.success);
               // location.reload();
               window.location.href = "/company-master";
               // var id = <?php //echo $company_id; ?>0;

               // if (typeof  id == 'undefined' || id == 0) {
               //    if (confirm('Are you sure you want to continue!')) {
               //    window.location.href = 'http://surabhi.microlan.in/project-master?gr_id='+data.company_id;//http://127.0.0.1:8000/  http://surabhi.microlan.in/
               //    console.log('Thing was saved to the database.');
               //    } else {
               //       window.location.href = 'http://surabhi.microlan.in/company-master';                 
               //    }
               // }else{
               //    location.reload();
               // }

            
               

            }else{
               // if(data.error==''){
               //    alert();
               // }

               $('#loader1').css('display','none');
               $('#submitButton').css('display','block');

              
                printErrorMsg(data.error,err);
            }
           
            // if(typeof obj.messages['group_name'] !== 'undefined'){
            //       $('#group_name').html(obj.messages['group_name']);
            //       $('.group_name').css('border-color','red');
            // }else{
            //       $('#group_name').html('');
            //       $('.group_name').css('border-color','');
            // }
         }
      });
      function printErrorMsg (msg,err) {

         $("."+err).find("ul").html('');
         $("."+err).css('display','block');
         $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
         });                
      }
   
}




</script>
<script>
function add_designation_form(){
    var url = 'addDesignationCompanyMaster';
    var form = 'add_designation_form';
    var err = 'print-error-msg_add_designation';
    $.ajax({
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,      
        success: function(data) {
            // alert(data.success); 
            var a = data.a;
            var option = a.option;
            var value = a.value; 

            var newOption= new Option(option,value, true, false);
            // Append it to the select
            $(".designation_h").append(newOption);//.trigger('change');
            // $('#designation_h').append($('<option>').val(optionValue).text(optionText));


            // console.log(data.success);return;
            if($.isEmptyObject(data.error)){
               $('.success1').html(data.success);
               $('#modal9').modal('close');
               $('#add_designation').val('');
               $('.success1').html("");
               //  alert(data.success);
               //  location.reload();
            }else{
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
    // execute(url,form,err);
}
</script>    
<script>
function apply_css1(skip,attr, val=''){                                                                                          
    var id =returnColArray1();  
   //  alert(id); return;
    id.forEach(function(entry) {
        
        if(entry==skip){
            // alert(11)
            $('#'+skip).css(attr,'orange','!important'); //#3FCFC8
            // $('#'+skip).attr('style', 'background-color: orange !important');
        }else{            
            $('#'+entry).css(attr,val);
            action_to_hide();            
            
        }        
    });
}

  

function hide_n_show1(skip){
    var id = collapsible_body_ids1();
    collapsible_body_ids1().forEach(function(entry) {
        // console.log(id);
        if(entry==skip){
            // alert(skip);
            var x = document.getElementById(skip);  
            $('#'+skip).css('background-color','rgb(234 233 230)');
            // alert(x)          
            if (x.style.display === "none") {
                // alert(1)
                
                x.style.display = "block";
            } else {
               var s = skip.replace("_div", "");
                x.style.display = "none";
                $('#col_'+s).css('background-color','white');
                // alert(x.style.backgroundColor);
                // alert(skip);
                // $('#col_'+skip).css('background-color','white');
                // $('#col_lead_info').css('background-color','white',"!important");
                // x.style.backgroundColor = "lightblue";
            }
        }else{          
            // alert(entry)
            $('#'+entry).hide();
        }
        
    });
}


function returnColArray1(){
    var a = Array('col_award','col_pros_cons','col_payment','col_hierarchy');
    return a;
}

function collapsible_body_ids1(){
    var b = Array('awadard_div','pros_cons_div','payment_div','hierarchy_div');
    return b;
}


         function hideShowAward() {
            apply_css1('col_award','background-color',''); 
            hide_n_show1("awadard_div");    
         //    var x = document.getElementById("awadard_div");   
         //    $('#awadard_div').css('background-color','rgb(234 233 230)');
         //    if (x.style.display === "none") {
         //       x.style.display = "block";
         //       $('#col_award').css('display',''); 
         //    } else {
         //       x.style.display = "none";        
         //       $('#col_award').css('background-color','white');        
         //    }
         }

         function hideShowPros() {
            apply_css1('col_pros_cons','background-color',''); 
            hide_n_show1("pros_cons_div");    
            // var x = document.getElementById("pros_cons_div");   
            // $('#pros_cons_div').css('background-color','rgb(234 233 230)');
            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_pros_cons').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_pros_cons').css('background-color','white');        
            // }
         }

         function hideShowPayment() {
            apply_css1('col_payment','background-color',''); 
            hide_n_show1("payment_div"); 
            // var x = document.getElementById("payment_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_payment').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_payment').css('background-color','white');        
            // }
         }

         function ShowHidehierarchy() {
            apply_css1('col_hierarchy','background-color',''); 
            hide_n_show1("hierarchy_div"); 
            // var x = document.getElementById("hierarchy_div");   

            // if (x.style.display === "none") {
            //    x.style.display = "block";
            //    $('#col_hierarchy').css('display',''); 
            // } else {
            //    x.style.display = "none";        
            //    $('#col_hierarchy').css('background-color','white');        
            // }
         }

      </script>  
  
<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->