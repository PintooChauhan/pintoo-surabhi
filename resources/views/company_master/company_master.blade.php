<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->
                
         

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

        <div class="row">
          <form method="get" action="/searchData">
                    <div class="input-field col l6 m6 s12" id="InputsWrapper2">
                            <input type="text" class="validate" name="search" required   placeholder="Enter Project / Builders Company Name"  >                             
                    </div>
                      <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-large waves-effect waves-light green darken-1" type="submit"  style="line-height: 43px !important;height: 43px !important">Search</button>                                                
                    </div>  
                    <div class="input-field col l2 m2 s6 display_search">
                          <a href="/company-master" class="btn-large waves-effect waves-light red darken-1" type="submit"  style="line-height: 43px !important;height: 43px !important">Clear</a>                        
                        </div>

                        
            </form>
                   
                </div>
            <br>

                <div class="row">
                    <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th><a href="/company-list" style="color:white"> Group Name</a> </th>
                        <th><a href="/project-list" style="color:white"> Complex </a></th>
                        <th><a href="/society-list" style="color:white"> Building </a></th>
                        <th>No. of Wing</th>
                        <!-- <th>Status</th> -->
                        <!-- <th>Location</th> -->
                        <!-- <th>Sub Location</th> -->
                        <!-- <th>Types of Flats</th> -->
                        <!-- <th>Website</th> -->
                        <!-- <th>TAT Period</th> -->
                        <th>No. Of Building </th>
                        <!-- <th>Residential/Commerical  </th> -->
                        <!-- <th>No. of Units in Building</th> -->
                        <th>Building Elevation</th>
                        <th>Unit Status</th>
                        <th>Building Type</th>
                        <th>Building Age / Possession Year</th>
                        <th style="background-color: green !important"> <a href='/add-company-master-form' style="color: white;; " >Add Group</a></th>

                        
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $data1)
                          <tr>
                            <td><a href="add-company-master-form?company_id={{$data1->company_id}}" target="_blank"> {{ ucwords($data1->group_name) }} </a></td>
                            <td><a href="project-master?project_id={{$data1->project_id}}" target="_blank"> {{ ucwords($data1->project_complex_name) }} </a></td>
                            <td><a href="society-master?society_id={{$data1->society_id}}" target="_blank"> {{ ucwords($data1->building_name) }} </a></td>
                            <td> @if($data1->wingCnt > 0 )  <a href="wing-master?society_id={{$data1->society_id}}" target="_blank"> @if(isset($data1->building_name) && !empty($data1->building_name)) {{$data1->wingCnt}} @endif</a> @endif</td>
                            <!-- <td>{{ strtoupper($data1->unit_status) }}</td> -->
                            <!-- <td>{{ ucwords($data1->location_name) }}</td> -->
                            <!-- <td>{{ ucwords($data1->sub_location_name) }}</td> -->
                            <!-- <td>?</td>         -->
                            <!-- <td>{{$data1->web_site_url}}</td> -->
                            <!-- <td>{{$data1->tat_period}}</td> -->
                            <td>{{$data1->societyCnt}}</td>
                            
                            
                            <td>
                              <?php 
                                  $decodeEle = json_decode($data1->building_type_id) ;
                                    if(!empty($decodeEle)){
                                      if(!empty($decodeEle[0])){
                                      $get = DB::select("select * from dd_elevation where dd_elevation_id in(".$decodeEle[0].")  ");
                                      foreach($get as $val){
                                        echo ucwords($val->elevation_name).',';
                                      } 
                                    }
                                  }
                                  
                                
                                ?>
                            </td>
                            <td>
                            <?php 
                                  $decodeUnit = json_decode($data1->soc_unit_status) ;
                                    if(!empty($decodeUnit)){
                                      if(!empty($decodeUnit[0])){
                                      $get1 = DB::select("select * from dd_unit_status where unit_status_id in(".$decodeUnit[0].")  ");
                                      foreach($get1 as $val1){
                                        echo ucwords($val1->unit_status).',';
                                      } 
                                    }
                                  }
                                  
                                
                                ?>

                            </td>
                            <td><?php 
                                  $decodeCate = json_decode($data1->soc_category_id) ;
                                    if(!empty($decodeCate)){
                                      if(!empty($decodeCate[0])){
                                      $get2 = DB::select("select * from dd_category where category_id in(".$decodeCate[0].")  ");
                                      foreach($get2 as $val2){
                                        echo ucwords($val2->category_name).',';
                                      } 
                                    }
                                  }
                                  
                                
                                ?></td>
                            <td>{{$data1->rera_completion_year}}</td>
                            <td> 
                             
                              + <a href='/project-master?c_id={{$data1->company_id}}'  target="_blank"> Complex</a> 
                              <?php if( isset($data1->project_id) && !empty($data1->project_id) && $data1->project_complex_name != null){  
                                  if(empty($data1->building_name) && !empty($data1->society_id)){ ?>
                                    |
                                      <a href='/society-master?society_id={{$data1->society_id}}'  target="_blank" > Building</a> 
                                  <?php }else{ ?>
                                    |
                                      <a href='/society-master?p_id={{$data1->project_id}}'  target="_blank" > Building</a> 
                                   <?php }
                                ?>
                            
                            
                              <?php } ?>
                                <a href="wing-master?society_id={{$data1->society_id}}" target="_blank"> @if(isset($data1->building_name) && !empty($data1->building_name)) |  Wings @endif</a>
                            
                          </td>
                            <!-- <td><a href='#'  >View Company</a></td> -->
                                  <!-- /edit-company-master -->
                          </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br>
                    
                    <div class="row">
                        <div class="col m10 l10"></div>
                        <div class="col m1 l1">
                      <a href="{{ $data->previousPageUrl() }}" class="btn">Previous</a>

                        </div>
                        <div class="col m1 l1">
                        <div class="d-flex justify-content-center">  <a href="{{ $data->nextPageUrl() }}" class="btn">Next</a> </div>

                        </div>
                    </div>
                    <!-- <nav class="toolbox toolbox-pagination">
                      <div class="toolbox-item toolbox-show"> -->

                      <!-- </div> -->
                    
                    <!-- </nav> -->
  
                </div>

    
       
        
        

         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


 

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     