<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->
                
         

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

        <div class="row">
          <form method="get" action="/search-Company-List">
                    <div class="input-field col l6 m6 s12" id="InputsWrapper2">
                            <input type="text" class="validate" name="search" required   placeholder="Enter Company Name"  >                             
                    </div>
                      <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-large waves-effect waves-light green darken-1" type="submit"  style="line-height: 43px !important;height: 43px !important">Search</button>                                                
                    </div>  
                    <div class="input-field col l2 m2 s6 display_search">
                          <a href="/company-list" class="btn-large waves-effect waves-light red darken-1" type="submit"  style="line-height: 43px !important;height: 43px !important">Clear</a>                        
                        </div>
                        <div class="input-field col l2 m2 s6 display_search">
                          <a href="/company-master" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> Back To Home Page</a>
                        </div>
            </form>
                   
                </div>
            <br>

                <div class="row">
                    <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Company ID </th>
                        <th>Group Name</th>
                        <th>Office Phone</th>
                        <th>Office Email</th>
                        <th>Action</th>
                        <!-- <th> </th> -->
                        <!-- <th style="background-color: green !important"> <a href='/add-company-master-form' style="color: white;; " >Add Company</a></th> -->

                        
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $data1)
                          <tr>
                            <td> C-{{  str_pad($data1->company_id,3, '0', STR_PAD_LEFT) }} </td>
                            <td>{{ ucwords($data1->group_name) }} </td>
                            <td>{{ ucwords($data1->office_phone) }} </td>
                            <td>{{ ucwords($data1->office_email_id) }} </td>
                    
                            <td><a href='/add-company-master-form?company_id={{$data1->company_id}}'  target="_blank">Edit</a> |
                             <!-- <a href='/add-company-master-form?company_id={{$data1->company_id}}' >Delete</a> -->
                             <a  href='javascript:void(0)' onClick="confirmDelAward('{{$data1->company_id}}')"  >Delete</a></td>
                            </td>
                            <!-- <td><a href='#'  >View Company</a></td> -->
                                  <!-- /edit-company-master -->
                          </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br>
                    
                    <div class="row">
                        <div class="col m10 l10"></div>
                        <div class="col m1 l1">
                      <a href="{{ $data->previousPageUrl() }}" class="btn">Previous</a>

                        </div>
                        <div class="col m1 l1">
                        <div class="d-flex justify-content-center">  <a href="{{ $data->nextPageUrl() }}" class="btn">Next</a> </div>

                        </div>
                    </div>
                    <!-- <nav class="toolbox toolbox-pagination">
                      <div class="toolbox-item toolbox-show"> -->

                      <!-- </div> -->
                    
                    <!-- </nav> -->
  
                </div>

    
       
        
        

         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->

        <div class="modal" id="deleteCompanyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-content">
               <h5 style="text-align: center">Delete record</h5>
               <hr>        
               <form method="post" id="delete_company" >
                  <input type="hidden" class="company_id" id="company_id" name="company_id">                
                  <input type="hidden"  id="form_type" name="form_type" value="company">

                  <div class="modal-body">
                     
                     <h5>Are you sure want to delete this record?</h5>
                  
                  </div>
            
                   </div>
            
               <div class="modal-footer" style="text-align: left;">
                     <div class="row">
                     <div class="input-field col l2 m2 s6 display_search">
                           <!-- <button class="btn-small  waves-effect waves-light" onClick="updateAwards()" type="button" name="action">Update</button>                         -->
                           <a href="javascript:void(0)" onClick="deleteAward()" class="btn-small  waves-effect waves-light green darken-1" >Yes</a>

                     </div>    
                     
                     <div class="input-field col l2 m2 s6 display_search" style="margin-left: -35px;">
                           <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                     </div>    
                  </div> 
               </div>
           </form>
        </div>

<script>
    function confirmDelAward(params) {
    var award_id = params;
    $('.company_id').val(award_id);   
    $('#deleteCompanyModal').modal('open');
}

function deleteAward() {
   var url = 'deleteTableFormRecords';
    var form = 'delete_company';
    $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
      $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
             $('#'+form).serialize() ,      
        success: function(data) {     
           console.log(data);
     
     
         $('#deleteCompanyModal').modal('close');      
         location.reload();      
        }
      });   

}

</script>
 

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     