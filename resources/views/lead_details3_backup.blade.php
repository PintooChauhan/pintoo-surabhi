<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>





::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

</style>

<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/data-tables.css') }} "> 
   <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/select.dataTables.min.css') }} "> 
   <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css') }} ">   
   <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css') }} ">


<script type="text/javascript" src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js') }}"></script>

   <script type="text/javascript" src="{{ asset('app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js') }}" ></script>

   <script type="text/javascript" src="{{ asset('app-assets/vendors/data-tables/js/dataTables.select.min.js') }}"  ></script>

   <script type="text/javascript" src="{{ asset('app-assets/js/scripts/data-tables.js')}}" ></script>

   <script type="text/javascript" src="{{ asset('app-assets/js/vendors.min.js') }}" ></script>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
      <div id="main" class="main-full" style="min-height: auto">
         <div class="row">
           
           
            <!-- <div class="col s12"> -->
               <!-- <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">BUYER INPUT FORM</span><hr>  </div> -->
                 <x-leadinfor1-layout></x-leadinfor1-layout>
                 
               
            <!-- </div>                                      -->
            

                <x-requirementr1-layout></x-requirementr1-layout>
                <x-requirementr2-layout></x-requirementr2-layout>
                <x-requirementr3-layout></x-requirementr3-layout>
                <x-requirementr4-layout></x-requirementr4-layout>
                <x-requirementr5-layout></x-requirementr5-layout>
            <hr>                   
            <x-requirementr6-layout></x-requirementr6-layout>
            <x-all_modals-layout></x-all_modals-layout>
             <hr>
             <!-- Code by avinash -->
             <div class="row">
                  <div class="row">
                     <div class="col s12">
                  <div class="card">
                      <div class="card-content">
                          <h4 class="card-title">Data table Html code</h4>
                          <div class="row" style=" width: 500px;
                                                   overflow-x: scroll;
                                                   margin-left: 5em;
                                                   overflow-y: visible;
                                                   padding: 0;">
                              <div class="col s12">
                                  <table id="page-length-option" class="display">
                                      <thead>
                                          <tr>
                                              <th class="headcol">Name</th>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>      <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td class="headcol">Tiger Nixon</td>
                                              <td>System Architect</td>
                                              <td>Edinburgh</td>
                                              <td>61</td>
                                              <td>2011/04/25</td>
                                              <td>$320,800</td>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>      <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                          </tr>
                                          <tr>
                                              <td class="headcol">Garrett Winters</td>
                                              <td>Accountant</td>
                                              <td>Tokyo</td>
                                              <td>63</td>
                                              <td>2011/07/25</td>
                                              <td>$170,750</td>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>      <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                          </tr>
                   
                                      </tbody>
                                      <tfoot>
                                          <tr>
                                              <th class="headcol">Name</th>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                              <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>      <th>Position</th>
                                              <th>Office</th>
                                              <th>Age</th>
                                              <th>Start date</th>
                                              <th>Salary</th>
                                          </tr>
                                      </tfoot>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>


   <style>
      
      table {
  border-collapse: separate;
  border-spacing: 0;
  border-top: 1px solid grey;
}

td, th {
  margin: 0;
  border: 1px solid grey;
  white-space: nowrap;
  border-top-width: 0px;
}

/* div {
  width: 500px;
  overflow-x: scroll;
  margin-left: 5em;
  overflow-y: visible;
  padding: 0;
} */

.headcol {
  position: absolute;
  width: 15em;
  left: 0;
  top: auto;
  border-top-width: 1px;
  /*only relevant for first row*/
  margin-top: -1px;
  /*compensate for top border*/
}

.headcol:before {
  content: 'Row ';
}

.long {
  background: yellow;
  letter-spacing: 1em;
}
   </style>





      </div>

             </div>


                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     