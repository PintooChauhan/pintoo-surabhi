<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/css/select.dataTables.min.css"> -->
  
      <!-- <script src="app-assets/js/scripts/data-tables.js"></script> -->
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.0/css/fixedColumns.dataTables.min.css"> -->
      <!-- END PAGE LEVEL JS-->
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/4.1.0/css/fixedColumns.dataTables.min.css">

<style>

</style>



      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">

        @php
        $check_s_add = 0;
        $check_s_edit = 0;
        $check_s_delete = 0;
        @endphp

        @if(isset($access_check) && !empty($access_check) )
          @foreach($access_check as $ackey=>$acvalue)
            @php
              $check_s_add = $acvalue->s_add;
              $check_s_edit = $acvalue->s_edit;
              $check_s_delete = $acvalue->s_delete;
            @endphp
          @endforeach
        @endif


           <div class="row">@if(!empty($check_s_add) )
            @if($check_s_add==1)
            <!-- tenant page button -->
          <div class="col s2" style="float: right;margin-left:-12px">
            <button type="button" class="btn btn-primary btn-md" style="margin-left: -12px; ">
              <a href="/tenant"  class="waves-effect waves-light  " style="color: white !important"> +  Add Tenant</a>
            </button>
          </div>
          @endif
          @endif
          <!-- tenant page button --> 
          
              <div class="col s12">
                  <div class="card">
                      <div class="card-content">
                          
                        @php
                        $check_s_delete = 0;
                        @endphp
          
                        @if(isset($access_check) && !empty($access_check) )
                          @foreach($access_check as $ackey=>$acvalue)
                            @php
                              $check_s_delete = $acvalue->s_delete;
                            @endphp
                          @endforeach
                        @endif

                          <div class="row">
                              <div class="col s12">
                                  <div class="table-responsive">
                                       <table id="scroll-vert-hor12" class="display nowrap">
                                                    <thead>
                                                        <tr class="pin">
                                    <th>Days</th>
                                    <th>Lead</th>
                                    <th>Name</th>
                                    <th>Cont. No.</th>
                                    <th>Configuration</th>
                                     <th>Unit</th>
                                    <th>Rent</th>          
                                    <th>Deposit</th>                                                  
                                    <th>Sub Location</th>      
                                    <th>Shifting Date</th>           
                                   
                                    <th>Bldg. Age</th>      
                                    <th>Furnish Status</th>      
                                    <th>Looking Since</th>      
                                    <th>Area</th>                        
                                    <th>Residence Address</th>
                                    <!-- <th>FUP Date  </th>       -->
                                    <th>Stage - Intensity</th>
                                    <!-- <th>F2F Done</th> -->
                                    <th>Lead Source</th>
                                    <?php 
                                      if( in_array($role,$roles) ){ ?>
                                        <th>Assigned Name</th>

                                    @if(!empty($check_s_delete) )
                                    @if($check_s_delete==1)
                                      <th>Action</th>
                                    @endif
                                    @endif

                                    <?php } ?>
                                  
                                </tr>
                                                    </thead>
                                                    <tbody>

                                @foreach($allData as $value)
                                @php
                                if($value->buyer_tenant_id <= 100){
                                        
                                        $id = str_pad($value->buyer_tenant_id,3, '0', STR_PAD_LEFT);
                                    }else{
                                        $id = $value->buyer_tenant_id;
                                    }
                                @endphp
                                <?php 
                                    $loc = array();
                                    $decodeEle1 = json_decode($value->sublocation_id) ;
                                    if(!empty($decodeEle1)){
                                      if(!empty($decodeEle1[0])){
                                      $get1 = DB::select("select sub_location_name from dd_sub_location where sub_location_id in(".$decodeEle1[0].")  ");
                                      foreach($get1  as $key =>  $val1){
                                          // echo ).', ';
                                          array_push($loc,ucwords($val1->sub_location_name));
                                      } 
                                    }
                                  }

                                  $lead_source_name = $value->lead_source_name;
                                  $finalName = '';
                                  $sub1 = substr($lead_source_name, 0, 25);
                                  if(strlen($sub1) < 25){
                                    $ct = 25- strlen($sub1);
                                 
                                    if(!empty($sub1)){
                                      $finalName =  $sub1.str_repeat('&nbsp;', $ct);
                                    }else{
                                      $finalName =  str_repeat('&nbsp;', 41);
                                    }
                                     
                                    
                                  }elseif(strlen($lead_source_name) > 25){
                                      $finalName =  substr($lead_source_name, 0, 25);
                                    
                                  }else{
                                      $finalName = str_repeat('&nbsp;', 41);
                                    
                                  }

                                  $customer_res_address = $value->customer_res_address;
                                  $finalNameAdd = '';
                                  $sub2 = substr($customer_res_address, 0, 35);

                                  if(strlen($customer_res_address) > 35){
                                    $finalNameAdd =  $sub2;
                                  }else{
                                    $finalNameAdd =  $customer_res_address;
                                  }


                                  $unt1 = array();
                                  if( $value->unit_type_id == 'residential' ){
                                          $decodeEle1 = json_decode($value->units) ;
                                          if(!empty($decodeEle1)){
                                            if(!empty($decodeEle1[0])){
                                            $get1 = DB::select("select unit_type_residential_name from dd_unit_type_residential where unit_type_residential_id in(".$decodeEle1[0].")  ");
                                            foreach($get1 as $val1){
                                              array_push($unt1,ucwords($val1->unit_type_residential_name));
                                            //  echo ucwords($val1->unit_type_residential_name).', ';
                                            } 
                                          }
                                        }
                                      }else{
                                        $decodeEle1 = json_decode($value->units) ;
                                          if(!empty($decodeEle1)){
                                            if(!empty($decodeEle1[0])){
                                            $get1 = DB::select("select unit_type_commercial_name from dd_unit_type_commercial where unit_type_commercial_id in(".$decodeEle1[0].")  ");
                                            foreach($get1 as $val1){
                                              // echo ucwords($val1->unit_type_commercial_name).', ';
                                              array_push($unt1,ucwords($val1->unit_type_commercial_name));
                                            } 
                                          }
                                        }
                                      }

                                      $conf = array();
                                      if( $value->unit_type_id == 'residential' ){
                                        $decodeEleConf = json_decode($value->configuration_id) ;
                                        if(!empty($decodeEleConf)){
                                          if(!empty($decodeEleConf[0])){
                                          $getConf = DB::select("select configuration_name from dd_configuration where configuration_id in(".$decodeEleConf[0].")  ");
                                          foreach($getConf as $valConf){
                                            array_push($conf,ucwords($valConf->configuration_name));
                                            // echo ucwords($val->configuration_name).', ';
                                          } 
                                        }
                                      }
                                    }

                                  
                                  ?>

                                <tr >
                                <td title="<?= date_format(date_create($value->c_date),"d-m-Y H:i"); ?>">  <?php $d = date('Y-m-d');
                                      $s = $value->c_date;
                                      $date = strtotime($s);
                                      $date1=date_create($d);
                                      $date2=date_create(date('Y-m-d', $date));
                                      $diff=date_diff($date1,$date2);
                                      echo $diff->format("%a");
                                      ?>    </td>
                                    <td><?php
                                            if( $value->lead_by_name == 'property consultant' ){
                                              echo "PC";
                                            }else{
                                              echo "SR";
                                            }
                                      ?></td>
                                     <td title="<?= $lead_source_name; ?>">
                                      @if($check_s_edit==1)
                                     <a target="_blank" href="/tenant?buyer_tenant_id={{$value->buyer_tenant_id}}"><?php  echo $finalName; ?></a>
                                     @else
                                     <a href="javascript:void(0);"><?php  echo $finalName; ?></a>
                                     @endif
                                    </td>
                                    <td>{{ $value->contact_no }}</td>
                                 
                                 
                                    <td title="<?php print_r( implode(', ',$conf) ); ?>"> <?php
                                          if(!empty($conf)) {
                                            $subconf = substr(implode(', ',$conf), 0, 10);
                                            if(strlen($subconf) < 10){
                                              echo  $subconf;
                                            }else{
                                              echo  $subconf.'...';
                                            }
                                          }
                                         ?>
                                    </td>
                                    <td title="<?php print_r( implode(', ',$unt1) ); ?>"> 
                                      <?php 
                                            if(!empty($unt1)) {
                                              $subunit1 = substr(implode(', ',$unt1), 0, 10);
                                              if(strlen($subunit1) < 10){
                                                echo  $subunit1;
                                              }else{
                                                echo  $subunit1.'...';
                                              }
                                            }
                                          ?>
                                    </td>
                                 
                                    <td>{{$value->min_rent_amount}} - {{$value->max_rent_amount}}</td>
                                    <td>{{$value->min_deposit_amount}} - {{$value->max_deposit_amount}}</td>
                                   
                                    
                                    
                            
                                     <td title="<?php print_r(implode(', ',$loc));?>">
                                          <?php 
                                          if(!empty($loc)) {
                                            $sub = substr(implode(', ',$loc), 0, 35);
                                            if(strlen($sub) < 35){
                                              echo  $sub;
                                            }else{
                                              echo  $sub.'...';
                                            }
                                          }
                                          ?>
                                    </td>

                                    
                                    <td>   {{$value->shifting_date}}  </td>
                                    <td>@if(!empty($value->building_age)) {{$value->building_age}} Yrs @endif</td>
                                    <td>{{ucwords($value->furnishing_status)}}</td>                                   
                                   
                                    <td>{{ucwords($value->looking_since)}}</td>
                                    
                                    <td>{{$value->min_carpet_area}} - {{$value->max_carpet_area}} </td>
                                   
                                  
                                    <td title="<?= $customer_res_address; ?>"><?php echo $finalNameAdd; ?></td>
                                    <!-- <td></td> -->
                                    <td title="{{ucwords($value->lead_stage)}} , {{ucwords($value->lead_intesity_id)}} , {{ucwords($value->lead_analysis)}}">{{ucwords($value->lead_stage)}}</td>
                                    <!-- <td>    </td> -->
                                    <td>   {{ucwords($value->lead_source_name1)}}  </td>

                                    <?php 
                                      if( in_array($role,$roles) ){ ?>
                                        <td>   {{ucwords($value->lead_assigned_name)}}  </td>
                                    
                                    @if(!empty($check_s_delete) )
                                      @if($check_s_delete==1)
                                      <td>
                                        <a href="/deleteTenant?buyer_tenant_id={{$value->buyer_tenant_id}}" onclick="return confirm('Are you sure you want to delete this tenant?');">Delete</a>
                                      </td>
                                      @endif
                                    @endif

                                    <?php } ?>
                                </tr>
                                @endforeach
 

                            </tbody>
                                                </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>                  <!-- START RIGHT SIDEBAR NAV -->
      </div>
       <div class="content-overlay"></div>
    </div>
 </div>
</div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <!-- <script src="app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> -->
    <!-- <script src="app-assets/vendors/data-tables/js/dataTables.select.min.js"></script> -->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <!-- <script src="app-assets/js/plugins.js"></script> -->
    <!-- <script src="app-assets/js/search.js"></script> -->
    <!-- <script src="app-assets/js/custom/custom-script.js"></script> -->
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
   
    <!-- END PAGE LEVEL JS-->
     <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/4.1.0/js/dataTables.fixedColumns.min.js"></script>
    