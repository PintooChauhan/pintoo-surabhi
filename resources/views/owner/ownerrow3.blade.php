<div class="row">                                        

               
                
                <div class="col l3 s12">
                   <ul class="collapsible">
                       <li onClick="price_seller()">
                       <div class="collapsible-header" id='col_price_seller'>Rent and Deposit </div>
                       
                       </li>                                        
                   </ul>
               </div> 
                               
               <div class="col l3 s12">
                   <ul class="collapsible">
                       <li onClick="license_period()">
                       <div class="collapsible-header" id="col_license_period">License Period</div>
                       
                       </li>                                        
                   </ul>
               </div>
               
               

                                    
               <div class="col l3 s12">
                   <ul class="collapsible">
                       <li onClick="f2f_address_seller()">
                       <div class="collapsible-header" id="col_f2f_address_seller">Address & F2F Appointment </div>
                       
                       </li>                                        
                   </ul>
               </div>

               <div class="col l3 s12"  >
                    <ul class="collapsible" >
                        <li onClick="tanant_and_shifing_charges()" >
                        <div class="collapsible-header" id='col_tanant_and_shifing_charges'>Preferred Tenant & Shifting Charges </div>
                        
                        </li>                                        
                    </ul>
                </div>

            
               

       </div> 

      
    <div class="collapsible-body"  id='price_seller' style="display:none">
        <div class="row">
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <div  class="sub_val no-search">
                                    <select  id="expected_price_amt" name="expected_price_amt" onChange="convert_expected_price()"  class="select2 browser-default">
                                        <option value="" disabled selected>Select</option>
                                        <option value="1000000">₹ 10L</option>
                                        <option value="2000000">₹ 20L</option>
                                        <option value="3000000">₹ 30L</option>
                                        
                                    </select>
                                </div>    
                                <label for="wap_number" class="dopr_down_holder_label active">Expected Rent: <span class="red-text">*</span></label>
                                <div class="rupeeSign">
                                <input  type="text" class="validate mobile_number" name="expected_price" id="expected_price1" required placeholder="Enter">
                                </div>
                                
                                <span id="expected_price_words"><br></span>                                
                                
                            </div>
                            

                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <div  class="sub_val no-search">
                                    <select  id="final_price_amt" name="final_price_amt" onChange="convert_final_price()"  class="select2 browser-default">
                                        <option value="" disabled selected>Select</option>
                                        <option value="1000000">₹ 10L</option>
                                        <option value="2000000">₹ 20L</option>
                                        <option value="3000000">₹ 30L</option>                                        
                                    </select>
                                </div>    
                                <label for="wap_number" class="dopr_down_holder_label active">Final Rent: <span class="red-text">*</span></label>
                                <div class="rupeeSign">
                                <input  type="text" class="validate mobile_number" name="final_price" id="final_price" required placeholder="Enter" >
                                </div>
                                <span id="final_price_words"></span>                                                                               
                                
                            </div>

                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <div  class="sub_val no-search">
                                    <select   id="expected_package_amt" name="expected_package_amt" onChange="convert_expected_package_amt()"   class="select2 browser-default">
                                        <option value=""  selected>Select</option>
                                        <option value="1000000">₹ 10L</option>
                                        <option value="2000000">₹ 20L</option>
                                        <option value="3000000">₹ 30L</option>
                                        
                                    </select>
                                </div>    
                                <label for="wap_number" class="dopr_down_holder_label active">Expected Deposit: <span class="red-text">*</span></label>
                                <div class="rupeeSign">
                                <input  type="text" class="mobile_number"  name="expected_package_amount" id="expected_package_amount"  placeholder="Enter"  >
                                </div>
                                <span id="expected_package_amount_words"></span>                  
                            </div>  

                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <div  class="sub_val no-search">
                                    <select   id="final_package_amt" name="final_package_amt" onChange="convert_final_package_amt()"   class="select2 browser-default">
                                        <option value=""  selected>Select</option>
                                        <option value="1000000">₹ 10L</option>
                                        <option value="2000000">₹ 20L</option>
                                        <option value="3000000">₹ 30L</option>
                                        
                                    </select>
                                </div>    
                                <label for="wap_number" class="dopr_down_holder_label active">Final Deposit: <span class="red-text">*</span></label>
                                <div class="rupeeSign">
                                <input  type="text" class="mobile_number"  name="final_package_amount" id="final_package_amount"  placeholder="Enter"  >
                                </div>
                                <span id="final_package_amount_words"></span>                  
                            </div>  
                        
                    </div>
                    

                    <div class="row">                  
                        <div class="input-field col l3 m4 s12 display_search">
                            <ul class="collapsible" style="margin-top: -2px">
                                <li onClick="matching_properties()">
                                <div class="collapsible-header"  id="col_matching_properties" >Matching Tenant</div>
                                
                                </li>                                        
                            </ul>
                        </div>


                    </div>
                    
                    <br>
                    
                    <div id="append_budget_range_table"></div>
                    

                    <div class='row' id='matching_properties_table' style="display:none">
                        <div class="col l8 s8 m8">
                            <table class="bordered  striped centered" id="example"  style=" background-color: white;">
                            <thead style=" background-color: lightblue;">
                            <tr>
                            <th>Society Name</th>
                            <th>Configuration</th>
                            <th>Area</th>
                            <th>Price</th>
                            <th>Lead By Details</th>
                            
                            </tr>
                            </thead>
                            <tbody>
                        
                        
                            </tbody>
                        </table>
                    
                        </div>
                    </div>
    </div>       
       
    <div class="collapsible-body"  id='license_period' style="display:none">
       <div class="row">
                      
                      <div class="input-field col l3 m4 s12 display_search">                
                          <select class="select2  browser-default"  data-placeholder="Select" name="license_period">          
                                 <option value="1">1 Year</option> 
                                 <option value="2">2 Year</option> 
                          </select>
                          <label for="purpose" class="active">License Period </label>
                      </div>        


                      <div class="input-field col l3 m4 s12 display_search">                
                          <select class="select2  browser-default"  data-placeholder="Select" name="lock_in_period">          
                                 <option value="1">6 Month</option> 
                                 <option value="2">1 Year</option> 
                          </select>
                          <label for="purpose" class="active">Lock In Period </label>
                      </div>

                      <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <div  class="sub_val no-search">
                            <select   id="rent_escalation_amt" name="rent_escalation" onChange="convert_rent_escalation()"   class="select2 browser-default">
                                <option value=""  selected>Select</option>
                                <option value="5">5%</option>
                                <option value="10">10%</option>                       
                                
                            </select>
                        </div>    
                        <label for="wap_number" class="dopr_down_holder_label active">Rent Escalation : <span class="red-text">*</span></label>
                            <div class="rupeeSign">
                            <input  type="text" class="mobile_number"  name="rent_escalation" id="rent_escalation"  placeholder="Enter"  >
                            </div>
                        <span id="rent_escalation_words"></span>                  
                    </div> 
  
                     
                     
            </div>
    </div>   

    <div class="collapsible-body"  id='f2f_address_seller' style="display:none">
    <div class="row">
                
                <div class="input-field col l3 m4 s12" >                    
                    <label for="wap_number" class="active">F2F Time & Date : <span class="red-text">*</span></label>
                    <input type="datetime-local"  name="f2f_time_and_date"  required placeholder="Calender" >                                                                                             
                </div>
                
            </div>

            <div class="row">
                <div class="input-field col l3 m4 s12" >  
                    <label for="lead_assign" class="active">Current Residence Address: <span class="red-text">*</span></label>
                    <!-- <textarea style="border-color: #5b73e8;padding-top: 12px;margin-bottom:-5px !important" Placeholder="Enter" name="current_residence_address"  rows='2' col='4'></textarea> -->
                    <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                </div>

                <div class="input-field col l3 m4 s12 display_search">                    
                    <select class="select2  browser-default"  data-placeholder="Select" name="city">
                        <option value="" disabled selected>Select</option>
                        <option value="option 1">Thane</option>
                        <option value="option 1">Dadar</option>
                    </select>
                    <label for="possession_year" class="active">City </label>
                    <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                    <a href="#modal5" id="add_cra_city" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                     </div>
                </div>

                <div class="input-field col l3 m4 s12 display_search">                    
                    <select class="select2  browser-default"  data-placeholder="Select" name="state">
                        <!-- <option value="" disabled selected>Select</option> -->
                        <option value="option 1" selected>Maharashtra</option>
                        <option value="option 1">Dadar</option>
                    </select>
                    <label for="possession_year" class="active">State </label>
                    <!-- <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                    <a href="#" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                     </div> -->
                </div>

                <div class="input-field col l3 m4 s12" >                    
                    <label for="wap_number" class="active">Pincode : <span class="red-text">*</span></label>
                    <input type="text"  name="current_pincode"  required Placeholder="Enter" >                                                                                             
                </div>
    </div>
    <div class="row">                

                <div class="input-field col l3 m4 s12" >                    
                    <label for="wap_number" class="active">Company Name & Address : <span class="red-text">*</span></label>
                    <input type="text"  name="company_name"  required Placeholder="Enter" >                                                                                             
                </div>


                <div class="input-field col l3 m4 s12 display_search">                    
                    <select class="select2  browser-default"  data-placeholder="Select" name="city">
                        <option value="" disabled selected>Select</option>
                        <option value="option 1">Thane</option>
                        <option value="option 1">Dadar</option>
                    </select>
                    <label for="possession_year" class="active">City </label>
                    <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                        <a href="#modal6" id="add_com_city" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                     </div>
                </div>

                <div class="input-field col l3 m4 s12 display_search">                    
                    <select class="select2  browser-default"  data-placeholder="Select" name="state">
                        <!-- <option value="" disabled selected>Select</option> -->
                        <option value="option 1" selected>Maharashtra</option>
                        <option value="option 1">Dadar</option>
                    </select>
                    <label for="possession_year" class="active">State </label>
                    <!-- <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                    <a href="#" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                     </div> -->
                </div>

                <div class="input-field col l3 m4 s12" >                    
                    <label for="wap_number" class="active">Pincode : <span class="red-text">*</span></label>
                    <input type="text"  name="company_pincode"  required Placeholder="Enter" >                                                                                             
                </div>


    </div>
    <div class="row">

               
                
                <div class="input-field col l3 m4 s12 display_search">                    
                    <select class="select2  browser-default"  data-placeholder="Select" name="designation">
                        <option value="" disabled selected>Select</option>
                        <option value="option 1" >Sales Manager</option>
                        <option value="option 1">Sales Executive</option>
                    </select>
                    <label for="possession_year" class="active">Designation </label>
                    <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                    <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                     </div>
                </div>

                <div class="input-field col l3 m4 s12 display_search">                    
                    <select class="select2  browser-default"  data-placeholder="Select" name="industry_name">
                        <option value="" disabled selected>Select</option>
                        <option value="option 1">IT</option>
                        <option value="option 1">Hospitality</option>
                    </select>
                    <label for="possession_year" class="active">Industry Name </label>
                    <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                    <a href="#modal10" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                     </div>
                </div>

                

                
                <div class="input-field col l3 m4 s12" >  
                    <label for="lead_assign" class="active">Information: <span class="red-text">*</span></label>
                    <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="information"  rows='2' col='4'></textarea> -->
                    <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                </div>
    </div>
    </div>


    
<div class="collapsible-body"  id='tanant_and_shifing_charges' style="display:none">
    <div class="row">
       

        <div class="input-field col l3 m4 s12 display_search">                
            <select class="select2  browser-default"  id="preferred_tenant" data-placeholder="Select" name="preferred_tenant">
                <option value="option 1">Family</option>
                <option value="option 1">Bachelor</option>
            </select>
            <label for="building_type" class="active">Preferred Tenant </label>
            <div id="AddMoreFileId3" class="addbtn"  style="top: 11px !important;right: -6px !important;">
                <a href="#modal17" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  
        </div>


        <div class="input-field col l3 m4 s12 display_search">                
            <input type="text"  name="nature_of_bussiness"  Placeholder="Enter" >
            <label for="building_type" class="active">Shifting Charges </label>
            <div id="AddMoreFileId3" class="addbtn"  style="top: 11px !important;right: -6px !important;">
                <a href="#modal18" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  
        </div>

        
        
    </div>
</div>



           <!-- Modal Company address - city Structure -->
           <div id="modal5" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add City</h5>
        <hr>
        
        <form method="post" id="add_res_city_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">City: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_res_city" id="add_res_city"   placeholder="Enter">
                    <span class="add_res_city_err"></span>
                    
                </div>
                <div class="input-field col l3 m4 s12 ">                        
                    <select  id="seller_dfd" name="seller_dfd" placeholder="Select" class="select2  browser-default" >
                        <option>option1</option>
                        <option>option2</option>
                    </select>
                    <label class="active">State</label>                
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_res_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_res_city_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div>    

            </div>
        </form>
    </div>

    <!-- Modal Company address - city Structure -->
    <div id="modal6" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add City</h5>
        <hr>
        
        <form method="post" id="add_Company_city_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">City: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_company_city" id="add_company_city"   placeholder="Enter">
                    <span class="add_company_city_err"></span>
                    
                </div>
                <div class="input-field col l3 m4 s12 ">                        
                    <select  id="seller_dfd" name="seller_dfd" placeholder="Select" class="select2  browser-default" >
                        <option>option1</option>
                        <option>option2</option>
                    </select>
                    <label class="active">State</label>                
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_company_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_Company_city_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div>    
            </div>
        </form>
    </div>

      <!-- Modal Designation Structure -->
      <div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>


      <!-- Modal Industry Name Structure -->
      <div id="modal10" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Industry Name</h5>
        <hr>
        
        <form method="post" id="add_industry_form">
            <div class="row" style="margin-right: 0rem !important">
            <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Industry Type: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_industry_type" id="add_industry_type"   placeholder="Enter">
                    <span class="add_industry_name_err"></span>                    
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Industry Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_industry_name" id="add_industry_name"   placeholder="Enter">
                    <span class="add_industry_name_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_industry_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_industry_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>