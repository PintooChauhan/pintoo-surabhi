<div class="row">                                        
    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="lead_info()">
            <div class="collapsible-header" id="col_lead_info" >Lead Info</div>                            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="customer_name()">
            <div class="collapsible-header" id="col_customer_name">Customer Info</div>
            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li >
            <div class="collapsible-header" style="    background-color: #2a2ea5;  pointer-events: none; color: white;">OWNER FORM</div>
            
            </li>                                        
        </ul>
    </div>

</div> 

        <div class="collapsible-body"  id='lead_info' style="display:none">
                  
            <div class="row"> 
                        
            

                <div class="input-field col l3 m4 s12">
                    <select  id="leadtype" name="leadtype" class="select2 browser-default validate" data-minimum-results-for-search="Infinity" required>
                        <option value="" >Select</option>
                        @foreach($lead_type as $leadType)
                        <option value="{{$leadType->lead_type_id}}" >{{ucwords($leadType->lead_type_name)}}</option>
                        @endforeach
                        
                    </select>
                    <label for="leadtype" class="active">Lead type: <span class="red-text">*</span></label>
                </div>

                <!-- <div class="input-field col l3 m4 s12">
                    <select class="select2 browser-default" id="leadby" onChange="hide_show_lead2()" >
                        <option value="" >Select</option>
                        @foreach($lead_by as $leadBy)                     
                            <option value="{{$leadBy->lead_by_id}}"  >{{ ucwords($leadBy->lead_by_name)}}</option>  
                        @endforeach
                        
                    </select>
                    
                    <label for="leadby" class="active">Lead by</label>
                </div> -->
                
                <div class="input-field col l3 m4 s12 display_search">
                    <!-- <select class="select2  browser-default" multiple="multiple" id="leadtype3" data-placeholder="Company name, Branch name" name="leadtype3"> -->
                    <select class="select2  browser-default" multiple="multiple" id="pc_company_name"   name="pc_company_name">
                        <option value="option 1">Surabhi Realtors, Brahmand</option>
                        <option value="option 1">Surabhi Realtors, Majiwada</option>
                        <option value="option 1">Surabhi Realtors, Kalyan</option>
                        <option value="option 1">Surabhi Realtors, Thane</option>
                    </select>
                    <label for="leadtype3" class="active">PC Company Name </label>                                             
                    <div id="add_pc_name" class="addbtn" >
                              <a href="#modal1" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                    </div>                            
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                     
                    <select class="select2  browser-default"  id="lead_source_name"  name="lead_source_name" onChange="hideShowCampaign()" >
                        <option value=""  selected>Select</option>
                        @foreach($lead_source as $leadSource)
                            <option value="{{$leadSource->lead_source_id}}">{{ucwords($leadSource->lead_source_name)}}</option>
                        @endforeach    
                    </select>
                    <label for="leadtype5" class="active">Lead Source Name
                    </label>                                             
                </div>        
                
                <div class="input-field col l3 m4 s12 display_search">                     
                     <select class="select2  browser-default"  id="lead_campaign_name"  name="lead_campaign_name"  disabled>
                         <option value=""  selected>Select</option>
                         @foreach($lead_campaign as $leadCampaign)
                             <option value="{{$leadCampaign->lead_campaign_id}}">{{ucwords($leadCampaign->lead_campaign_name)}}</option>
                             
                         @endforeach    
                     </select>
                     <label for="lead_campaign_name" class="active">Campaign Name
                     </label>                                             
                 </div>

               

                  


            </div>

            
           

            <div class="row">

                


                <div class="input-field col l3 m4 s12 display_search">
                    <select class=" validate select2  browser-default"  id="lead_assigned_name"  name="lead_assigned_name">
                        <option value="" >Select</option>
                        <option value="option 2">Suraj Pawar - Brahmand</option>
                        <option value="option 3">Arun Jadhav - station</option>
                        
                    </select>
                    <label for="leadtype7" class="active">Lead Assigned Name
                    </label>                                             
                </div>

               
            </div>
            
        </div>

        <div class="collapsible-body" id='customer_name' style="display:none">
            <div class="row">
                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                    <label for="cus_name active" class="dopr_down_holder_label active">Lead Name 
                    <span class="red-text">*</span></label>
                    <div  class="sub_val no-search">
                        <select  id="cus_name_seller" name="cus_name_seller" class="select2 browser-default ">
                            <option value="1">Mr.</option>
                            <option value="2">Miss.</option>
                            <option value="3">Mrs.</option>
                        </select>
                    </div>
                        <input type="text" class="validate mobile_number" name="contactNum" id="cus_name" required placeholder="Enter"  >
                </div>


                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                    <label for="contactNum1" class="dopr_down_holder_label active">Primary Mobile Number: <span class="red-text">*</span></label>
                    <div  class="sub_val no-search">
                        <select  id="country_code_seller" name="country_code_seller" class="select2 browser-default">
                            <option value="1">+91</option>
                            <option value="2">+1</option>
                            <option value="3">+3</option>
                        </select>
                    </div>
                    <input type="text" class="validate mobile_number" name="contactNum" id="mobile_number" required placeholder="Enter" >
                    <div id="AddMoreFileId" class="addbtn">
                        <a href="#" id="AddMoreFileBox" class="" style="color: grey !important">+</a> 
                    </div>
                </div>
                <div class="" id="display_inputs"></div>
            </div>
            <div class="row">
                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                    <div  class="sub_val no-search">
                        <select  id="country_code_seller1" name="country_code_seller1" class="select2 browser-default">
                            <option value="1">+91</option>
                            <option value="2">+1</option>
                            <option value="3">+3</option>
                        </select>
                    </div>
                    <label for="wap_number" class="dopr_down_holder_label active">Primary Whatsapp Number: <span class="red-text">*</span></label>
                    <input type="text" class="validate mobile_number" name="wap_number" id="wap_number" required placeholder="Enter" >
                    
                    <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                        <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                    </div>
                    
                    <div id="AddMoreFileIdWhatsapp" class="addbtn" >
                    
                    <a href="#" id="AddMoreFileBoxWhatsapp" class="" style="color: grey !important">+</a> 
                    </div>                                                
                </div> 
                <div class="" id="display_inputs_whatsapp1"></div>

                <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                    <label for="primary_email active" class="active">Primary Email Address: <span class="red-text">*</span></label>
                    <input type="email" class="validate" name="primary_email" id="primary_email" required placeholder="Enter" >
                    <div id="AddMoreFileIdemail" class="addbtn">
                        <a href="#" id="AddMoreFileBox3" class="" style="color: grey !important">+</a> 
                    </div>
                </div>
                <div class="" id="display_inputs3"></div>  
            </div>          
        </div>
        <hr>
        

              <!-- Modal PC Company Name Structure -->
    <div id="modal1" class="modal" >
        <div class="modal-content">
        <h5 style="text-align: center">Add Property Consultant Details</h5>
        <hr>
        
        <form method="post" id="pc_company_name_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Company Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_company_name" id="add_pc_company_name"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>
                    
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Location: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_name"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Sub Location: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_mobile"   placeholder="Enter">
                </div>
            </div> 
            
            <div class="row" style="margin-right: 0rem !important">
                
               <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Owner Name : <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_whatsapp_no"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active">Owner Mobile No: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_email"   placeholder="Enter">
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active">Owner Email: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="branch_location"   placeholder="Enter">
                </div>

               
            </div>

            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Excecutive Name.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_employee_name"   placeholder="Enter">
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" class="active">Excecutive Mobile No.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_pc_employee_mobile_no"   placeholder="Enter">
                 </div>
 
                
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" disabled class="active">Employee Email.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_pc_employee_email"   placeholder="Enter">
                 </div>
            </div>
            <div class="alert alert-danger print-error-msg_pc_company_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                    <button class="btn-small  waves-effect waves-light" onClick="pc_company_name_btn()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            </div>
        </form>
    </div>

     <!-- Modal Pc Executive Name Structure -->
     <div id="modal2" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add PC Executive Name </h5>
        <hr>
        
        <form method="post" id="pc_executive_name_form">
        <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Company Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_company_name" id="add_exe_pc_company_name"   placeholder="Enter">
                    <span class="add_exe_pc_company_name_err"></span>
                    
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_name"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Mobile No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_mobile"   placeholder="Enter">
                </div>
            </div> 
            
            <div class="row" style="margin-right: 0rem !important">
                
               <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Whatsapp No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_whatsapp_no"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active">PC Owner Email: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_email"   placeholder="Enter">
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Branch Location.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_branch_location"   placeholder="Enter">
                </div>

                
            </div>

            <div class="row" style="margin-right: 0rem !important">

            <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Employee Name.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_employee_name"   placeholder="Enter">
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" class="active">Employee Mobile No.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_owner_mobile"   placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assignqq" class="active">Employee Whatsapp No: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_whatsapp_no"   placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" disabled class="active">Employee Email.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_employee_email"   placeholder="Enter">
                 </div>
            </div>
                <div class="alert alert-danger print-error-msg_pc_executive_name" style="display:none">
                <ul style="color:red"></ul>
                </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="pc_excecutive_name_btn()" type="button" name="action">Submit</button>
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>   
                                        
                                  
            </div>
        </form>
    </div>


        <!-- Modal PC Company Name Structure -->
        <div id="modal1" class="modal" >
        <div class="modal-content">
        <h5 style="text-align: center">Add PC Company Name</h5>
        <hr>
        
        <form method="post" id="pc_company_name_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Company Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_company_name" id="add_pc_company_name"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>
                    
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_name"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Mobile No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_mobile"   placeholder="Enter">
                </div>
            </div> 
            
            <div class="row" style="margin-right: 0rem !important">
                
               <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Whatsapp No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_whatsapp_no"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active">PC Owner Email: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_owner_email"   placeholder="Enter">
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active">Branch Location: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="branch_location"   placeholder="Enter">
                </div>

               
            </div>

            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Employee Name.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_pc_employee_name"   placeholder="Enter">
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" class="active">Employee Mobile No.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_pc_employee_mobile_no"   placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assignqq" class="active">Employee Whatsapp No: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_pc_employee_whatsapp"   placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" disabled class="active">Employee Email.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_pc_employee_email"   placeholder="Enter">
                 </div>
            </div>
            <div class="alert alert-danger print-error-msg_pc_company_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                    <button class="btn-small  waves-effect waves-light" onClick="pc_company_name_btn()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            </div>
        </form>
    </div>

     <!-- Modal Pc Executive Name Structure -->
     <div id="modal2" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add PC Executive Name </h5>
        <hr>
        
        <form method="post" id="pc_executive_name_form">
        <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Company Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_company_name" id="add_exe_pc_company_name"   placeholder="Enter">
                    <span class="add_exe_pc_company_name_err"></span>
                    
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_name"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Mobile No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_mobile"   placeholder="Enter">
                </div>
            </div> 
            
            <div class="row" style="margin-right: 0rem !important">
                
               <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">PC Owner Whatsapp No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_whatsapp_no"   placeholder="Enter">
                </div>

                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assignqq" class="active">PC Owner Email: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_owner_email"   placeholder="Enter">
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Branch Location.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_branch_location"   placeholder="Enter">
                </div>

                
            </div>

            <div class="row" style="margin-right: 0rem !important">

            <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" disabled class="active">Employee Name.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_exe_pc_employee_name"   placeholder="Enter">
                </div>
                
                <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" class="active">Employee Mobile No.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_owner_mobile"   placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assignqq" class="active">Employee Whatsapp No: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_whatsapp_no"   placeholder="Enter">
                 </div>
 
                 <div class="input-field col l4 m4 s12 display_search">
                     <label for="lead_assign" disabled class="active">Employee Email.: <span class="red-text">*</span></label>
                     <input type="text" class="validate" name="add_exe_pc_employee_email"   placeholder="Enter">
                 </div>
            </div>
                <div class="alert alert-danger print-error-msg_pc_executive_name" style="display:none">
                <ul style="color:red"></ul>
                </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="pc_excecutive_name_btn()" type="button" name="action">Submit</button>
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                    <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>   
                                        
                                  
            </div>
        </form>
    </div>
