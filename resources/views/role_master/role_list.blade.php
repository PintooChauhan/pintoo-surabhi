<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
      
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/4.1.0/css/fixedColumns.dataTables.min.css">

      <style>
  select
{
    display: block !important;
}
#scroll-vert-hor41{
    width:100% !important;
}
.green_color{
    color:#4caf50 ;
}

.red_color{
    color:#f44336;
}

</style>


      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">    

    
        @php
        $check_s_add = 0;
        $check_s_edit = 0;
        $check_s_delete = 0;
        @endphp

        @if(isset($access_check) && !empty($access_check) )
          @foreach($access_check as $ackey=>$acvalue)
            @php
              $check_s_add = $acvalue->s_add;
              $check_s_edit = $acvalue->s_edit;
              $check_s_delete = $acvalue->s_delete;
            @endphp
          @endforeach
        @endif

 <div class="row">
    <div class="col s12">
        <div class="card">
          
            <div class="card-content">

                <div class="row">
                    <div class="col s12">
                        <div class="tableFixHead">
                            <table id="scroll-vert-hor41" class="display nowrap table-hover">
                            <thead>
                            <tr class="pin">
                                <th>Sr. No.</th>
                                <th >Role Name</th>
                                <th >Last Updated Date</th>
                                @if(!empty($check_s_add) )
                                    @if($check_s_add==1)
                                    <th>Action </th>
                                    @endif
                                @endif         
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($getRoleData) && !empty($getRoleData))
                                        @foreach($getRoleData as $key=>$val)

                                            <tr>                                                                                                       
                                                <td>{{$key+1}}</td>
                                                <td>{{$val->role_name}}</td>
                                                <td>{{$val->updated_at}}</td>
                                                @if(!empty($check_s_add) )
                                                    @if($check_s_add==1)
                                                    <td>
                                                        @php
                                                        // $id = Crypt::encryptString($val->id);
                                                        @endphp
                                                        <a href="{{asset('add_role_access_level?role_id='.$val->id)}}" title="Role Access Level"><i class="material-icons dp48">assignment_ind</i></a>
                                                    </td>
                                                    @endif
                                                @endif 
                                                
                                            </tr>
                                        @endforeach
                                    @endif
                               

                            </tbody>
                            
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


<script>

</script>
<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    
<script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
 
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/4.1.0/js/dataTables.fixedColumns.min.js"></script>
    <!-- END PAGE LEVEL JS-->    <!-- END PAGE LEVEL JS-->
