<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/css/select.dataTables.min.css"> -->
  
      <!-- <script src="app-assets/js/scripts/data-tables.js"></script> -->
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.0/css/fixedColumns.dataTables.min.css"> -->
      <!-- END PAGE LEVEL JS-->


      <style>


div.dataTables_wrapper {
    width: 100% !important;
    margin: 0 auto;
}
select {
    display: initial !important; ;
}

/*::-webkit-scrollbar {
  display: none;
}*/
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.dataTables_filter input {
    border: 1px solid #aaa;
    border-radius: 3px;
    padding: 0px !important;
}
 


.dataTables_scrollBody{
    height: auto !important;
}




/* .tableFixHead {
  overflow: auto;
 
 
}*/
/* 
.tableFixHead table {
  border-collapse: collapse;
  width: 100%;
}

.tableFixHead th,
.tableFixHead td {
  padding: 8px 16px;
}


td:first-child, th:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:white;
}
td:nth-child(2),th:nth-child(2)  { 
position:sticky;
  left:102px;
  z-index:1;
  background-color:white;
  }
.tableFixHead th {
  position: sticky;
  top: 0;
  background: #eee;
  z-index:2
}
th:first-child , th:nth-child(2) {
  z-index:3
  } */

</style>
<style>
      
    </style>


      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">
         <div class="row">
          <!-- <div id="loader" class="center"></div> -->
          <div class="row">                                        
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li >
                            <div class="collapsible-header" id="col_company_info" tabindex="0" style="background-color: orange;">Add Role Access Level</div>
                        </li>                                        
                    </ul>
                </div>

                <div class="col l3 s12">
                    <a href="{{route('role.role_master')}}" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> Back To Home Page</a>
                </div>
          </div> 

        <div class="collapsible-body"  id='company_info' style="display:block">
            @php $role_id=''; @endphp
            @if(isset($getRoleData))@if(isset($getRoleData[0]->id) && !empty($getRoleData[0]->id)) @php $role_id = $getRoleData[0]->id; @endphp @endif @endif
            <input type="hidden"  id="role_id" value="{{$role_id}}">

            @if(isset($getRoleMainAccessData) && !empty($getRoleMainAccessData))
            
            @php 
            $condition_name_all = '';
            @endphp

            @foreach($getRoleMainAccessData as $allkey=>$alldvalue)

            @php 
            $condition_name_all = $condition_name_all.$alldvalue->condition_name.'--';
            @endphp
            @endforeach

            <div class="row">
                <div class="input-field col l3 m4 s12 display_search" style="display: flex;align-items: center;font-weight: bold;">
                    <input type="checkbox" name="check_all_role_access_level_id" value="{{$condition_name_all}}" class="check_all_role_access_level" style="opacity:1;pointer-events:auto;width: 20px;position: relative;margin-right: 15px;margin-left: 5px;" /> Select All

                </div>
            </div>

            @endif

        <div class="row">

            {{-- <div class="form-group row">                               
                <div class="col-md-3">
                    <h4>Users</h4>
                        <div class="checkbox"><label><input checked="checked" type="checkbox" class="checkbox" name="perms[]" value="add_new_user"> Add</label></div>
                        <div class="checkbox"><label><input checked="checked" type="checkbox" class="checkbox" name="perms[]" value="edit_user"> Edit</label></div>
                        <div class="checkbox"><label><input checked="checked" type="checkbox" class="checkbox" name="perms[]" value="delete_user"> Delete</label></div>
                        <div class="checkbox"><label><input checked="checked" type="checkbox" class="checkbox" name="perms[]" value="user_list"> View Users</label></div>
                </div>

            </div> --}}
            @if(isset($getRoleMainAccessData) && !empty($getRoleMainAccessData))

            @php
            // $sub_access_data1 = $sub_access_data[0];
            // print_r($sub_access_data);
            // echo "count : ".count($sub_access_data);
            @endphp

            @foreach($getRoleMainAccessData as $rmadkey=>$rmadvalue)

            @php 
            
            $s_view = 0;
            $s_add = 0;
            $s_edit = 0;
            $s_delete = 0;

            // echo $sub_access_data[0][0]->s_add;
            // echo count($sub_access_data[0]);
            @endphp
                
                @if(isset($sub_access_data) && !empty($sub_access_data))
                @for($i=0; $i < count($sub_access_data); $i++)
                    @if($sub_access_data[$i][0]->main_access_level_id==$rmadvalue->main_access_level_id)
                            
                    @php
                        $s_view = $sub_access_data[$i][0]->s_view;
                        $s_add = $sub_access_data[$i][0]->s_add;
                        $s_edit = $sub_access_data[$i][0]->s_edit;
                        $s_delete = $sub_access_data[$i][0]->s_delete;
                        
                    @endphp   

                    @break
                    @else
                        @php
                        $s_view = 0;
                        $s_add = 0;
                        $s_edit = 0;
                        $s_delete = 0;
                        @endphp 

                    @endif
                @endfor
                @endif

                <div class="col l4">
                    <div class="row" style="display: flex;justify-content: center;align-items: center;">
                        <div class="input-field col l12 m4 s12 display_search" >

                            <div style="display: flex;justify-content: flex-start;align-items: center;font-weight: bold;">
                                <input type="checkbox" name="main_access_level_id-{{$rmadvalue->main_access_level_id}}" data-condition_name="{{$rmadvalue->condition_name}}" data-access_level_id="{{$rmadvalue->main_access_level_id}}" class="access_level_check_all access_level_block_check_all access_level_block_{{$rmadvalue->condition_name}}" style="opacity:1;pointer-events:auto;width: 20px;position: unset;margin-right: 15px;" />
                                <label>{{$rmadvalue->page_name}}</label>                                                                         
                                        
                            </div>

                            <div style="margin-left: 35px;">
                                <div style="display: flex;justify-content: flex-start;align-items: center;font-weight: bold;">
                                    <input type="checkbox" name="access_level_group[]" @if($s_view=='1') @php echo 'checked'; @endphp @endif value="{{$rmadvalue->condition_name}}--{{$rmadvalue->main_access_level_id}}--view--{{$s_view}}" class="access_level_check_all {{$rmadvalue->condition_name}}_view_check access_level_single_check" data-condition_name="{{$rmadvalue->condition_name}}" data-access_level_id="{{$rmadvalue->main_access_level_id}}" data-action="view" style="opacity:1;pointer-events:auto;width: 20px;position: unset;margin-right: 15px;" />
                                    <label>View</label>
                                </div>
                                <div style="display: flex;justify-content: flex-start;align-items: center;font-weight: bold;">
                                    <input type="checkbox" name="access_level_group[]" @if($s_add=='1') @php echo 'checked'; @endphp @endif value="{{$rmadvalue->condition_name}}--{{$rmadvalue->main_access_level_id}}--add--{{$s_add}}" class="access_level_check_all {{$rmadvalue->condition_name}}_add_check access_level_single_check" data-condition_name="{{$rmadvalue->condition_name}}" data-access_level_id="{{$rmadvalue->main_access_level_id}}" data-action="add" style="opacity:1;pointer-events:auto;width: 20px;position: unset;margin-right: 15px;" />
                                    <label>Add</label>
                                </div>
                                <div style="display: flex;justify-content: flex-start;align-items: center;font-weight: bold;">
                                    <input type="checkbox" name="access_level_group[]" @if($s_edit=='1') @php echo 'checked'; @endphp @endif value="{{$rmadvalue->condition_name}}--{{$rmadvalue->main_access_level_id}}--edit--{{$s_edit}}" class="access_level_check_all {{$rmadvalue->condition_name}}_edit_check access_level_single_check" data-condition_name="{{$rmadvalue->condition_name}}" data-access_level_id="{{$rmadvalue->main_access_level_id}}" data-action="edit" style="opacity:1;pointer-events:auto;width: 20px;position: unset;margin-right: 15px;" />
                                    <label>Edit</label>
                                </div>

                                @if($rmadvalue->condition_name =='roles' || $rmadvalue->condition_name =='building_master')
                                @else
                                <div style="display: flex;justify-content: flex-start;align-items: center;font-weight: bold;">
                                    <input type="checkbox" name="access_level_group[]" @if($s_delete=='1') @php echo 'checked'; @endphp @endif value="{{$rmadvalue->condition_name}}--{{$rmadvalue->main_access_level_id}}--delete--{{$s_delete}}" class="access_level_check_all {{$rmadvalue->condition_name}}_delete_check access_level_single_check" data-condition_name="{{$rmadvalue->condition_name}}" data-access_level_id="{{$rmadvalue->main_access_level_id}}" data-action="delete" style="opacity:1;pointer-events:auto;width: 20px;position: unset;margin-right: 15px;" />
                                    <label>Delete</label>
                                </div>
                                @endif


                            </div>
                        </div>

                    </div>
                </div>

                

            @endforeach
            @endif


        </div>



                <script>
                    function calDays() {
                        // var inception_year = $('#inception_year').val();
                        var date_of_visit = $('#inception_year').val();
                        const date = date_of_visit;
                        const [day, month, year] = date.split('/');
                        const result = [year, month, day].join('-');

                        var y1 = new Date(result).getFullYear();
                            var y2 = new Date().getFullYear();

                            var years = [];
                            if(y1 < y2){
                            for(var i = y1; i <= y2; i++){
                                years.push(i);
                                }
                            }
                            else{
                            for(var i = y2; i <= y1; i++){
                                years.push(i);
                                }
                            }
                            var no_of_years = years.length;
                            if(no_of_years == 0){
                                $('#no_of_years').val(0);
                            }else{
                                $('#no_of_years').val(no_of_years-1);    
                            }
                            
                    }
                </script>


                <div class="collapsible-body"  id='company_address' style="display:none">
                    <div class="row">

                    </div>  

                </div>

              

            <div class="collapsible-body"  id='social_media'>
            </div>
                <div class="row form_success">
                    <div class="input-field col-12 l2 m2 s6 pl-1">
                        <ul></ul>

                    </div>   
                </div>

                <div class="row form_error">
                    <div class="input-field col-12 l2 m2 s6 pl-1">
                        <ul></ul>

                    </div>   
                </div> 

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="saveRoleAccessGroup()" type="button" name="action">@if(isset($user_id) && !empty($user_id)) Update @else Save @endif</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <a href="{{route('role.role_master')}}" class="waves-effect waves-green btn-small" style="background-color: red;">Cancel</a>

                    </div>    
                </div> 
      
            
            
  

                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->

<script>
  
  function apply_css11(skip,attr, val=''){   
    var id =returnColArray11();           
    id.forEach(function(entry) {
        if(entry==skip){
            $('#'+skip).css(attr,'orange','!important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){
            }else{
                $('#'+entry).css(attr,val);
            }
        }        
    });
}

function hide_n_show11(skip){
   var id = collapsible_body_ids11();
   collapsible_body_ids11().forEach(function(entry) {
       if(entry==skip){
           var x = document.getElementById(skip);  
           $('#'+skip).css('background-color','rgb(234 233 230)');
           if (x.style.display === "none") {
               x.style.display = "block";
           } else {
               x.style.display = "none";
               $('#col_'+skip).css('background-color','white');
           }
       }else{          
           $('#'+entry).hide();
       }
   });
}

function returnColArray11(){
   var a = Array('col_company_info','col_branch_info','col_employee_info');
   return a;
}

function collapsible_body_ids11(){
   var b = Array('company_info','branch_info','employee_info');
   return b;
}

function company_info(){    
  // alert()
   apply_css11('col_company_info','background-color','');
   hide_n_show11("company_info");     

}

function branch_info(){    
   apply_css11('col_branch_info','background-color','');
   hide_n_show11("branch_info");     

}

function employee_info(){    
   apply_css11('col_employee_info','background-color','');
   hide_n_show11("employee_info");     

}


function apply_css22(skip,attr, val=''){   
    var id =returnColArray22();           
    id.forEach(function(entry) {
        if(entry==skip){
            $('#'+skip).css(attr,'orange','!important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){
            }else{
                $('#'+entry).css(attr,val);
            }
        }        
    });
}

function hide_n_show22(skip){
   var id = collapsible_body_ids22();
   collapsible_body_ids22().forEach(function(entry) {
       if(entry==skip){
           var x = document.getElementById(skip);  
           $('#'+skip).css('background-color','rgb(234 233 230)');
           if (x.style.display === "none") {
               x.style.display = "block";
           } else {
               x.style.display = "none";
               $('#col_'+skip).css('background-color','white');
           }
       }else{          
           $('#'+entry).hide();
       }
   });
}

function returnColArray22(){
   var a = Array('col_company_address','col_company_review','col_social_media');
   return a;
}

function collapsible_body_ids22(){
   var b = Array('company_address','company_review','social_media');
   return b;
}

function company_address() {
  apply_css22('col_company_address','background-color','');
   hide_n_show22("company_address");     
}

function company_review() {
  apply_css22('col_company_review','background-color','');
   hide_n_show22("company_review");     
}

function social_media() {
  apply_css22('col_social_media','background-color','');
   hide_n_show22("social_media");     
}






function saveSocial() {
  var form_data = new FormData();
      form_data.append("social_url", document.getElementById('social_url').value);
      form_data.append("social_site", document.getElementById('social_site').value);
      form_data.append("form_type", 'social');
      form_data.append("property_consultant_id", $('#property_consultant_id').val());
      $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      $.ajax({
      url:"/saveData",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
         console.log(data);//return;

        //  const obj = JSON.parse(data);

         // console.log(obj); //return;
        //  $('#project_id2').val(data.project_id);
         // if(typeof obj['company_id'] !== 'undefined'){
          $('#property_consultant_id').val(data.property_consultant_id);
         // }
         //$('#uploaded_image').html(data);
         var incPros1 = parseInt($('#incID11').val()) +1;
         var html = '<tr id="tr2_'+data.property_consultant_social_media_id+'">';
         html += '<td>' + incPros1 + '</td>';// obj['company_pro_cons_id'] +'</td>';
         html += '<td id="sur1_'+data.property_consultant_social_media_id+'">' + data.url + '</td>';// obj['company_pros'] +'</td>';
         html += '<td id="ssi1_'+data.property_consultant_social_media_id+'">' + data.social_site + '</td>';// obj['company_cons'] +'</td>';         
         html += '<td id="slu1_'+data.property_consultant_social_media_id+'">' + data.updated_date + '</td>';// obj['company_cons'] +'</td>';         
         html += "<td> <a href='javascript:void(0)' onClick='updateSocial("+data.property_consultant_social_media_id+")' >Edit</a> | <a href='javascript:void(0)' onClick='confirmDelSocial("+data.property_consultant_social_media_id+")'>Delete</a></td>";   
         html += '</tr>';
         $('#social_table').prepend(html);
         $('#social_site').val('').trigger('change');
         $('#social_url').val('');
        //  $('#incPros').val(incPros);

        //  $('#loader1').css('display','none');
        //  $('#submitPros').css('display','block');
      }
      });
}

function updateSocial(param) {
   var property_consultant_social_media_id = param;
      var form_data = new FormData();
      form_data.append("form_type", 'social');
      form_data.append("property_consultant_social_media_id", property_consultant_social_media_id);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
   
      $.ajax({
         url:"/getData2",
         method:"POST",
         data: form_data,
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
            { 
               var data  = data[0];
               console.log(data); //return;
               $('#property_consultant_social_media_id').val(data.property_consultant_social_media_id);
               // $('#looking_since1').val(data.looking_since).trigger('change');
              
           
               $('#social_site').val(data.social_site).trigger('change');
               $('#social_url').val(data.url);
               var anchor=document.getElementById("save_saveSocial");
               anchor.innerHTML="Update";
               $("#save_saveSocial").attr("onclick","updateSocial1()");
            }
      });
}

function updateSocial1() {


// $('#loader3').css('display','block');
// $('#submitLooking').css('display','none');

   var form_data = new FormData();
   


   form_data.append("form_type", 'social');
   form_data.append('property_consultant_social_media_id', $('#property_consultant_social_media_id').val()); 

   form_data.append("social_site", document.getElementById('social_site').value);
   form_data.append("social_url", document.getElementById('social_url').value);

   $.ajaxSetup({
      headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
   });

   $.ajax({
      url:"/updateTableRecords1",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
          console.log(data);// return;
         var property_consultant_social_media_id = data.property_consultant_social_media_id;
         $('#sur1_'+property_consultant_social_media_id).html(data.data['url']);
         $('#ssi1_'+property_consultant_social_media_id).html(data.data['social_site']);
         $('#slu1_'+property_consultant_social_media_id).html(data.data['updated_date']);
        //  $('#loader3').css('display','none');
        //  $('#submitLooking').css('display','block');         
      }
   });

}

function clear_saveSocial() {
  $('#property_consultant_social_media_id').val('');
  $('#social_site').val('').trigger('change');
  $('#social_url').val('');

  var anchor=document.getElementById("save_saveSocial");
   anchor.innerHTML="Save";
   $("#save_saveSocial").attr("onclick","saveSocial()");
}


        function getCity(argument) {
            var city_id = argument;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(city_id) {
                $.ajax({
                    url: '/findCityWithStateID/',
                    type: "GET",
                    data : {city_id:city_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){
                       
                        $('#state').val(data[0].dd_state_id).trigger('change'); 
                    // });
                  }else{
                    $('#city').empty();
                  }
                  }
                });
            }else{
              $('#city').empty();
            }
        }

        function getlocation(argument) {
            var location_id = argument;
           // alert(location_id); return;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(location_id) {
                $.ajax({
                    url: '/findlocationWithcityID/',
                    type: "GET",
                    data : {location_id:location_id},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data);// return;
                      if(data){
                        $('#city').val(data[0].city_id).trigger('change'); 
                        getCity(data[0].city_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }
        
        function getsublocation(argument) {
            var SublocationID = argument.value;
            // alert(SublocationID);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
            if(SublocationID) {
                $.ajax({
                    url: '/findsublocationWithlocationID/',
                    type: "GET",
                    data : {SublocationID:SublocationID},
                    // dataType: "json",
                    success:function(data) {
                        console.log(data); //return;
                      if(data){

                        $('#location').val(data[0].location_id).trigger('change');  
                        getlocation(data[0].location_id);
                    // });
                  }else{
                    $('#location').empty();
                  }
                  }
                });
            }else{
              $('#location').empty();
            }
        }

// Get Reporting Manager data

function getReportingManagerData(param) 
{
    var branch_id = param;
    console.log("branch_id : ",branch_id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#pc_employees_id .reporting_manger_option').remove();

    if(branch_id!='') {
        $.ajax({
            url: '/getUserReportingManagerData/',
            type: "GET",
            data : {branch_id:branch_id},
            // dataType: "json",
            success:function(data) {
                console.log("data : ",data); //return;
                if(data){
                
                    $.each(data,function(index, value) {
                        $('#pc_employees_id').append("<option value="+value.pc_employees_id+" class='reporting_manger_option'>"+value.employee_name+"</option>"); 
                    });
                
            // });
            }else{
                $('#reporting_manager_id .reporting_manger_option').remove();
            }
            }
        });
    }else{
        $('#reporting_manager_id .reporting_manger_option').remove();
    }

}

function saveRoleAccessGroup() 
{

    var form_data = new FormData();
    form_data.append('role_id', $('#role_id').val());
    // form_data.append('buyer', $('#buyer').val());
    // form_data.append('tenant', $('#tenant').val());
    // form_data.append('property_consultant', $('#property_consultant').val());
    // form_data.append('building_master', $('#building_master').val());
    // form_data.append('users', $('#users').val());
    // form_data.append('roles', $('#roles').val());

    // var access_level_group = [].filter.call(document.getElementsByName('access_level_group[]'), function(c) {
    //         return c.checked;
    //       }).map(function(c) {
    //         return c.value;
    //       });

    var access_level_group = [];
    i = 0;
    $('input:checkbox[name="access_level_group[]"]').each(function()
    {
            // alert($(this).attr('id'));
            access_level_group[i++] = $(this).val();
            //I should store id in an array
    });
    // console.log("access_level_group : ",access_level_group);

    form_data.append('access_level_group', access_level_group);
    // form_data.append('role_name', $('#role_id option:selected').attr('data-role'));



    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
    url:"/saveRoleAccessGroup",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    success:function(data)
    {  
        console.log("data : ",data); //return;
        var err = 'print-error-msg';
        var success = 'print-success-msg';
        if($.isEmptyObject(data.error)){
            printSuccessMsg(data.success,success);
            window.location.href = "/role_master";
        }else{
            printErrorMsg(data.error,err);
        }
        
    }
    });

    function printSuccessMsg(msg,success) {
        $(".form_success ul li").remove();
        $number_sequence = 1; 
        $(".form_success").find("ul").append('<li class="green-text">Role Added Successfully.</li>');
    }

    function printErrorMsg(msg,err) {
        $(".form_error ul li").remove();
        $number_sequence = 1; 
        $.each( msg, function( key, value ) {
            $(".form_error").find("ul").append('<li class="red-text">'+$number_sequence+') '+value+'</li>');
            $number_sequence++;
        });                
    }

}

// delete user image and resumes

function delImage(params) {
      const myArray = params.split("#");
      var image = myArray[0];
      var type = myArray[1];
      // alert(myArray[1]); return;
      // alert('#'+params);//delImage
      if(type=='img'){
        var warning ="Are you sure you want to delete this image?";
      }else{
        var warning ="Are you sure you want to delete this resume?";
      }

      if (confirm(warning)) {
         var user_id = $('#user_id').val();
         const file = image;
         const filename = file.split('.').slice(0, -1).join('.');
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
         });
         $.ajax({
            url:"/delUserImage",
            method:"POST",
            data: {image:image,user_id:user_id,type:type},
            // contentType: false,
            // cache: false,
            // processData: false,
            success:function(data)
            {
               $('.'+filename).remove();
               console.log(data);
            }
         });
      }else{
         return false;
      }
   }





function confirmDelSocial(params) {
    var property_consultant_social_media_id = params;
    $('.property_consultant_social_media_id2').val(property_consultant_social_media_id);   
    $('#deleteSocialModal').modal('open');
}

function deleteSocial() {
    var url = 'deleteCompletly';
    var form = 'delete_Social';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        url:"/"+url,
        type:"POST",
        data:                     
        $('#'+form).serialize() ,      
        success: function(data) {
            console.log(data); //return;
            var property_consultant_social_media_id = data.property_consultant_social_media_id;
            $('#tr2_'+property_consultant_social_media_id).remove();
            // $('#deleteAwardModal').hide();
            $('#deleteSocialModal').modal('close');
            
        }
    }); 
}


// remove selected value after append

function check_all_value(params) {
    alert('alert');
    // console.log("params: ",params);
    $("."+params).remove();
    $("."+params+"selected").attr('selected',"");
}


</script>



<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <!-- <script src="app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> -->
    <!-- <script src="app-assets/vendors/data-tables/js/dataTables.select.min.js"></script> -->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <!-- <script src="app-assets/js/plugins.js"></script> -->
    <!-- <script src="app-assets/js/search.js"></script> -->
    <!-- <script src="app-assets/js/custom/custom-script.js"></script> -->
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <!-- END PAGE LEVEL JS-->


<script>
    const log = console.log;
    $( document ).ready(function() {
        $(".check_all_role_access_level").click(function(){
            var isChecked = $(this).is(':checked');
            var check_all_val = $(this).val();
            // console.log(check_all_val);
            var remove_last_char = check_all_val.slice(0,-2)
            // var result = $(remove_last_char).text().split('--');
            var check_all_val1 = remove_last_char.split("--");
            console.log("check_all_val : ",check_all_val);

            if(isChecked) {
                $(".access_level_check_all").prop('checked', true );

                // console.log("document.getElementsByName('access_level_group[]') : ",document.getElementsByName('access_level_group[]'));
                var access_level_group = [].filter.call(document.getElementsByName('access_level_group[]'), function(c) {
                    return c.checked;
                }).map(function(c) {
                    return c.value;
                });

                for(var i=0; i< access_level_group.length; i++){

                    var access_level_group1 = access_level_group[i];
                    var access_level_group2 = access_level_group1.split("--");
                    var condition_name = access_level_group2[0];
                    var main_access_level_id = access_level_group2[1];
                    var action_name = access_level_group2[2];
                    var status = 1;
                    $("."+condition_name+"_"+action_name+"_check").val(condition_name+"--"+main_access_level_id+"--"+action_name+"--"+status);

                }


            }else{
                $(".access_level_check_all").prop('checked', false );

                $('input:checkbox[name="access_level_group[]"]:not(:checked)').each(function(){ 
                    var access_level_group = $(this).val(); 
                    var access_level_group1 = access_level_group;
                    var access_level_group2 = access_level_group1.split("--");
                    var condition_name = access_level_group2[0];
                    var main_access_level_id = access_level_group2[1];
                    var action_name = access_level_group2[2];
                    var status = 0;
                    $("."+condition_name+"_"+action_name+"_check").val(condition_name+"--"+main_access_level_id+"--"+action_name+"--"+status);

                });

            }
        });

        $(".access_level_block_check_all").click(function(){
            var isChecked = $(this).is(':checked');
            var condition_name = $(this).data("condition_name");
            var main_access_level_id = $(this).data("access_level_id");
            console.log("condition_name : ",condition_name);

            if(isChecked) {
                $("."+condition_name+"_view_check").prop('checked', true );
                $("."+condition_name+"_view_check").val(condition_name+"--"+main_access_level_id+"--view--1");
                $("."+condition_name+"_add_check").prop('checked', true );
                $("."+condition_name+"_add_check").val(condition_name+"--"+main_access_level_id+"--add--1");
                $("."+condition_name+"_edit_check").prop('checked', true );
                $("."+condition_name+"_edit_check").val(condition_name+"--"+main_access_level_id+"--edit--1");
                $("."+condition_name+"_delete_check").prop('checked', true );
                $("."+condition_name+"_delete_check").val(condition_name+"--"+main_access_level_id+"--delete--1");
            }else{
                $("."+condition_name+"_view_check").prop('checked', false );
                $("."+condition_name+"_view_check").val(condition_name+"--"+main_access_level_id+"--view--0");
                $("."+condition_name+"_add_check").prop('checked', false );
                $("."+condition_name+"_add_check").val(condition_name+"--"+main_access_level_id+"--add--0");
                $("."+condition_name+"_edit_check").prop('checked', false );
                $("."+condition_name+"_edit_check").val(condition_name+"--"+main_access_level_id+"--edit--0");
                $("."+condition_name+"_delete_check").prop('checked', false );
                $("."+condition_name+"_delete_check").val(condition_name+"--"+main_access_level_id+"--delete--0");
            }
        });
        
        
        $(".access_level_single_check").click(function(){
            var isChecked = $(this).is(':checked');
            var condition_name = $(this).data("condition_name");
            var main_access_level_id = $(this).data("access_level_id");
            var action = $(this).data("action");

            $(".access_level_block_"+condition_name).prop('checked', true );
            // console.log($("."+condition_name+"_add_check").is(':checked'));
            if( $( "."+condition_name+"_view_check").is(':checked') &&  $( "."+condition_name+"_add_check").is(':checked') && $( "."+condition_name+"_edit_check").is(':checked') &&  $( "."+condition_name+"_delete_check").is(':checked') ){
                $(".access_level_block_"+condition_name).prop('checked', true );
            }
            else{
                $(".access_level_block_"+condition_name).prop('checked', false );
            }

            if(isChecked){
                $("."+condition_name+"_"+action+"_check").val(condition_name+"--"+main_access_level_id+"--"+action+"--1");
            }else{
                $("."+condition_name+"_"+action+"_check").val(condition_name+"--"+main_access_level_id+"--"+action+"--0");
            }

        });

// $(".remove_page_access").click(function(){
//     alert("alert");
//     var val = $(this).val();
//     console.log("val : ",val);
// });

    });
</script>

