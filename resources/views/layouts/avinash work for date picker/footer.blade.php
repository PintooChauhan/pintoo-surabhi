 <!-- BEGIN: Footer-->
 <!-- <footer class="page-footer footer footer-static footer-light navbar-border navbar-shadow">
         <div class="footer-copyright">
            <div class="container"><span>&copy; 2020 <a href="http://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">PIXINVENT</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="https://pixinvent.com/">PIXINVENT</a></span></div>
         </div>
      </footer> -->
      <!-- END: Footer-->
      <!-- BEGIN VENDOR JS-->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" ></script>
<script type="text/javascript" src="https://cdn.datatables.net/s/ju/dt-1.10.10,b-1.1.0,fc-3.2.0,fh-3.1.0,r-2.0.0,sc-1.4.0/datatables.min.js" ></script> -->
<!-- <script>
  $('#example').DataTable({
    fixedColumns: {
      leftColumns: 2
    },
    scrollY:        400,
    scrollX:        true,
    fixedColumns:   true
  });
</script> -->
      <script src="{{ asset('app-assets/js/vendors.min.js') }}"></script>
      <script src="{{ asset('app-assets/js/comboTreePlugin.js') }}"></script>
      <!-- BEGIN VENDOR JS-->
      <!-- BEGIN PAGE VENDOR JS-->
      <script src="{{ asset('app-assets/vendors/materialize-stepper/materialize-stepper.min.js') }}"></script>
      <!-- END PAGE VENDOR JS-->
      <!-- BEGIN THEME  JS-->
      <script src="{{ asset('app-assets/js/plugins.js') }}"></script>
      <script src="{{ asset('app-assets/js/search.js') }}"></script>
      <script src="{{ asset('app-assets/js/custom/custom-script.js') }}"></script>
      <script src="{{ asset('app-assets/js/custom/customDesign.js') }}"></script>
      <script src="{{ asset('app-assets/js/custom/form_operation.js') }}"></script>
      <script src="{{ asset('app-assets/js/custom/modalOperation.js') }}"></script>
      <script src="{{ asset('app-assets/vendors/select2/select2.full.min.js') }}"></script>
      <script src="{{ asset('app-assets/js/scripts/form-select2.js') }}"></script> 
      <!-- END THEME  JS-->



       <!-- summer note -->
        <script src="{{ asset('app-assets/js/scripts/form-wizard.js') }}"></script>
        <script src="{{ asset('app-assets/js/summer/katex.min.js') }} "></script>
        <script src="{{ asset('app-assets/js/summer/highlight.min.js') }} "></script>
        <script src="{{ asset('app-assets/js/summer/quill.min.js') }} "></script>
        <script src="{{ asset('app-assets/js/summer/form-editor.js') }} "></script>
                      

        <!-- summernote end -->
        <script src="{{ asset('app-assets/js/summernote/summernote-bs4.js') }} "></script>

<script>
    // $(document).ready(function() {
    //      // on form submit
    //     $("#project_master_form").on('submit', function() {
    //         // to each unchecked checkbox
    //         $('input[type=checkbox]:not(:checked)').each(function () {
    //             // set value 0 and check it
    //             $(this).attr('checked', true).val(0);
    //         });
    //     })
    // })
</script>

      <script>  
         $(document).ready(function() {  
    //         var x=window.scrollX;
    // var y=window.scrollY;
    // window.onscroll=function(){window.scrollTo(x, y);};

        $('#pc_company_name').select2({
                placeholder: "search",
                allowClear: true,
                width: '100%',
            });


            $('.modal').modal({
                dismissible: true
            });

            // $('#modal13').modal({
            //     dismissible: false
            // });
            


         // comment part for html editor<<<<<

        $('select').select2();
        $('select').select2({minimumResultsForSearch: -1});
        $('#lead_assign').select2({minimumResultsForSearch: 1});
        $('#lead_assigned_name').select2({minimumResultsForSearch: 1});
        $('#seller_wing').select2({minimumResultsForSearch: 1});
        $('#lead_source_name').select2({minimumResultsForSearch: 1});
        $('#lead_campaign_name').select2({minimumResultsForSearch: 1});


        
        $('.minimum_carpet_area').select2({minimumResultsForSearch: 1});
        $('.maximum_carpet_area').select2({minimumResultsForSearch: 1});
        $('.minimum_budget_amt').select2({minimumResultsForSearch: 1});
        $('.maximum_budget_amt').select2({minimumResultsForSearch: 1});
        $('.own_contribution_amt').select2({minimumResultsForSearch: 1});
               
        // $('#minimum_carpet_area').select2({minimumResultsForSearch: 1});
        // $('#maximum_carpet_area').select2({minimumResultsForSearch: 1});
        $('.minumum_rent').select2({minimumResultsForSearch: 1});
        $('.maximum_rent').select2({minimumResultsForSearch: 1});
        $('.minimum_deposit').select2({minimumResultsForSearch: 1});
        $('.maximum_deposit').select2({minimumResultsForSearch: 1});

        $('#current_residence_city').select2({minimumResultsForSearch: 1});
        $('#current_residence_state').select2({minimumResultsForSearch: 1});

        $('#company_city').select2({minimumResultsForSearch: 1});
        $('#company_state').select2({minimumResultsForSearch: 1});

        $('#designation').select2({minimumResultsForSearch: 1});
        $('#industry_name').select2({minimumResultsForSearch: 1});

        
        

          // comment  part end for html editor<<<<<

          

         $(".acress_compain_names").hide(300);
         $("#acres").on("click",function() {
         //$(".acress_compain_names").toggle();
         $(".acress_compain_names").toggle(300);
         });
         
         $(".magic_bricks_compain_name").hide(300);
         $("#magic_brickss").on("click",function() {
         //$(".acress_compain_names").toggle();
         $(".magic_bricks_compain_name").toggle(300);
         });
         
         $(".common_floor_compain_name").hide(300);
         $("#common_floor").on("click",function() {
         //$(".acress_compain_names").toggle();
         $(".common_floor_compain_name").toggle(300);
         });
         
         
            /* $("input").focusout(function() {  
                 if($(this).val()=='') {  
                     $(this).css('border', 'solid 2px red'); 
         
                 } 
                 else { 
                       
                     // If it is not blank. 
                     $(this).css('border', 'solid 2px green');     
                 }     
             })  ; */
               componentHandler.upgradeAllRegistered();
         });  
      </script>
      <script type="text/javascript">
         var SampleJSONData = [
             {
                 id: 0,
                 title: 'Horse'
             }, {
                 id: 1,
                 title: 'Birds',
                 isSelectable: false,
                 subs: [
                     {
                         id: 11,
                         title: 'Parrot'
                     }, {
                         id: 12,
                         title: 'Owl'
                     }, {
                         id: 13,
                         title: 'Falcon'
                     }
                 ]
             },  {
                 id: 6,
                 title: 'Fish'
             }
         ];
          
         
         
         var comboTree1;
         
         jQuery(document).ready(function($) {
         
         /*new code*/
          $("#AddMoreFileId6").click(function(e) {
                     $(".lead_source_info").show();
                     e.stopPropagation();
                 });
          $("#lead_sourch_name").click(function(e) {
                     $(".lead_source_info").show();
                     e.stopPropagation();
                 });
          $("#lead_sourch_name").focus(function(e) {
                     $(".lead_source_info").show();
                     e.stopPropagation();
                 });
         
                 $(document).click(function(e) {
                     if (!$(e.target).is('.lead_source_info, .lead_source_info *')) {
                         $(".lead_source_info").hide();
                     }
                 });
          var $checkboxes = $('#lead_source_info .form-group .countable_chkboxex input[type="checkbox"]');
         
             $checkboxes.change(function() {
                 var maanu = $('.lead_source_info').find('input[type=checkbox]:checked:not(:hidden)').filter(':first').val();
         
                 var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
                 var total_count = (countCheckedCheckboxes - 1);
                 if (total_count <= 0) {
                     total_count = '';
                 } else {
                     total_count = ' (+' + total_count + ')';
                 }
                  if(maanu==undefined)
                 {
                  
                 	  //$('#lead_sourch_name').val('a');
                 	   $('#lead_sourch_name').css('border', 'solid 2px red');
                 	    $('#lead_sourch_name').val('');
                 }
                 else
                 {
                 	 $('#lead_sourch_name').css('border', 'solid 2px green');
                 	 $('#lead_sourch_name').val(maanu + ' ' + total_count);
                 }
                
         
                
         
                
                 $("#leadSourceName_label_id").empty();
             });
         
             /*new_code*/
                 /*comboTree1 = $('#lead_sourch_name').comboTree({
                     source : SampleJSONData,
                     isMultiple: true,
                     cascadeSelect: false,
                     collapse: true,
                     selectableLastNode: true
         
                 });*/
                 
          
          
         //$('select').material_select();
         var MaxInputs       = 1; //maximum extra input boxes allowed
         var InputsWrapper   = $("#display_inputs"); //Input boxes wrapper ID
         var AddButton       = $("#AddMoreFileBox"); //Add button ID
         
         var x = InputsWrapper.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton).click(function (e) {
         
                 //max input box allowed
                 if(x <= MaxInputs) {
                     FieldCount++; //text box added ncrement
                     //add input box
          
                     $(InputsWrapper).append('<div class="input-field col l3 m4 s12"><label for="contactNum11" class="dopr_down_holder_label active">Secondary Mobile Number: <span class="red-text">*</span></label><div  class="sub_val"><select  id="secondary_country_code_id" name="country_code" class="select2 browser-default"><option value="1">+91</option><option value="2">+1</option><option value="3">+3</option></select></div><input type="text" onkeypress="return onlyNumberKey(event)" class="validate mobile_number" name="seccondary_mobile_number" id="seccondary_mobile_number" required placeholder="Text" > <a href="#" class="removeclass rmvbtn" style="color: red !important">-</a></div>');
         
                    // $('#country_code656').select2();
                    $('#secondary_country_code_id').select2({minimumResultsForSearch: -1});
                     x++; //text box increment
                     
                     $("#AddMoreFileId").show();
                     
                     $('AddMoreFileBox').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x == 2) {
                         $("#AddMoreFileId").hide();
                         $("#lineBreak").html("<br>");
                     }
         
                 }
                 return false;
         });
          
         $("body").on("click",".removeclass", function(e){ //user click on remove text
                 if( x > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x--; //decrement textbox
                     
                         $("#AddMoreFileId").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBox').html("Add field");
                 }
             return false;
         }); 
         
          
        
         
          
         
         var MaxInputs3       = 1; //maximum extra input boxes allowed
         var InputsWrapper3   = $("#display_inputs3"); //Input boxes wrapper ID
         var AddButton3       = $("#AddMoreFileBox3"); //Add button ID
         
         var x3 = InputsWrapper3.length; //initlal text box count
         var FieldCount3=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton3).click(function (e) {
                 //max input box allowed
                 if(x3 <= MaxInputs3) {
                     FieldCount3++; //text box added ncrement
                     //add input box
                     $(InputsWrapper3).append(' <div class="input-field col l3 m4 s12" id="InputsWrapper3"><label for="primary_email active" class="active">Secondary Email Address: <span class="red-text">*</span></label><input type="email" class="validate" name="secondary_email" id="secondary_email" required placeholder="Text"><a href="#" style="color: red !important" class="removeclass3 rmvbtn">-</a></div>');
                     x3++; //text box increment
                     
                     $("#AddMoreFileIdemail").show();
                     
                     $('AddMoreFileBox3').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x3 == 2) {
                        //  alert();
                         $("#AddMoreFileIdemail").css('display','none');
                         $("#AddMoreFileIdemail").hide();
                         $("#lineBreak3").html("<br>");
                     }
                 }
                 return false;
         });
         
         $("body").on("click",".removeclass3", function(e){ //user click on remove text
                 if( x3 > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x3--; //decrement textbox
                     
                         $("#AddMoreFileIdemail").show();
                     
                         $("#lineBreak3").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBox3').html("Add field");
                 }
             return false;
         }) ;
         
           $('#contactNum1').formatter({
                 'pattern': '+91 {{999}}-{{999}}-{{999}}-{{9999}}',
                 'persistent': true
               });
         
         });
         $(".select2").select2({
             dropdownAutoWidth: false,
         	minimumResultsForSearch: Infinity,
         	width: 'resolve',
         	  minimumResultsForSearch: -1,
             width: '100%'
         });
         
         $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
           $(this).closest(".select2-container").siblings('select:enabled').select2('open');
         });
         
         // steal focus during close - only capture once and stop propogation
         $('select.select2').on('select2:closing', function (e) {
           $(e.target).data("select2").$selection.one('focus focusin', function (e) {
             e.stopPropagation();
           });
         });
         
        //  function myFunction() {
        //  document.getElementById("myForm").reset();
        //  }


//$('select').material_select();
       


        var MaxInputss       = 1; //maximum extra input boxes allowed
        var InputsWrapperr   = $("#display_inputs_whatsapp1"); //Input boxes wrapper ID
        var AddButtonn       = $("#AddMoreFileBoxWhatsapp"); //Add button ID
         
        var xx = InputsWrapperr.length; //initlal text box count
        var FieldCountt=1; //to keep track of text box added
         
         //on add input button click
         $(AddButtonn).click(function (e) {
            //  alert()
         
                 //max input box allowed
                 if(xx <= MaxInputss) {
                     FieldCountt++; //text box added ncrement
                     //add input box
          
                     $(InputsWrapperr).append('<div class="input-field col l3 m4 s12" id="InputsWrapperr2"><label for="alterWhatsapp" class="dopr_down_holder_label active">Secondary Whatsapp  Number: <span class="red-text">*</span></label><div  class="sub_val"><select  id="secondary_wa_countryc_code" name="country_code" class="select2 browser-default"><option value="1">+91</option><option value="2">+1</option><option value="3">+3</option></select></div><input type="text" onkeypress="return onlyNumberKey(event)" class="validate mobile_number" name="secondary_wa_number" id="secondary_wa_number" required placeholder="Text" > <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " ><input type="checkbox" id="copyAlterwhatsapp" data-toggle="tooltip" title="Check if alternate whatsapp number is same" onClick="copyAlterMobileNo()"  style="opacity: 1 !important;pointer-events:auto"></div> <a href="#" class="removeclassW rmvbtn" style="color: red !important">-</a></div>');
                    
                    // $('#alterWhatsapp1').select2();
                    $('#secondary_wa_countryc_code').select2({minimumResultsForSearch: -1});
                     xx++; //text box increment
                     
                     $("#AddMoreFileIdWhatsapp").show();
                     
                     $('AddMoreFileBoxWhatsapp').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(xx == 2) {
                         $("#AddMoreFileIdWhatsapp").hide();
                         $("#lineBreak").html("<br>");
                     }
         
                 }
                 return false;
         });
          
         $("body").on("click",".removeclassW", function(e){ //user click on remove text
                 if( xx > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         xx--; //decrement textbox
                     
                         $("#AddMoreFileIdWhatsapp").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxWhatsapp').html("Add field");
                 }
             return false;
         }); 


// /////////////////////////////////////////////


    var MaxInputs10  = 10; //maximum extra input boxes allowed
         var InputsWrapper10   = $("#display_inputs_Challenge"); //Input boxes wrapper ID
         var AddButton10       = $("#AddMoreFileBoxChallenge"); //Add button ID
         
         var x = InputsWrapper10.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added

    $(AddButton10).click(function (e) {         
         //max input box allowed
         if(x <= MaxInputs10) {
             FieldCount++; //text box added ncrement
             //add input box  
             $(InputsWrapper10).append('<div class="row" id="InputsChan"><div class="input-field col l3 m4 s12 display_search"><label for="lead_assign" class="active">Challenges : <span class="red-text">*</span></label><input type="text"  name="challenges"  Placeholder="Enter" ></div><div class="input-field col l3 m4 s12 display_search"><label for="lead_assign" class="active">Solutions : <span class="red-text">*</span></label><input type="text"  name="solutions"  Placeholder="Enter" ></div><div class="input-field col l3 m4 s12 display_search"><div id="AddMoreFileIdChallenge_panel" class="addbtn" ><a href="#" class="removeclassChallenge rmvbtn" style="color: red !important">-</a></div></div></div>');            
            // $('#alterWhatsapp1').select2();
            // $('#alterWhatsapp1').select2({minimumResultsForSearch: -1});
             x++; //text box increment
             
             $("#AddMoreFileIdChallenge").show();
             
             $('AddMoreFileBoxChallenge').html("Add field");
             
             // Delete the "add"-link if there is 3 fields.
             if(x == 2) {
                //  $("#AddMoreFileIdChallenge").hide();
                 $("#lineBreak").html("<br>");
             }
 
         }
         return false;
 });



        $("body").on("click",".removeclassChallenge", function(e){ //user click on remove text
                 if( x > 1 ) {
                        //  $(this).parent('div').remove(); //remove text box
                        // $('#InputsChan').remove(); //remove text box
                        var e = $(this).parent().parent().parent();;
                        e.remove();
                         x--; //decrement textbox
                     
                        //  $("#AddMoreFileIdWhatsapp").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxChallenge').html("Add field");
                 }
             return false;
        });

        //   Seller fORM


        //$('select').material_select();
        var MaxInputs11       = 1; //maximum extra input boxes allowed
         var InputsWrapper11   = $("#display_inputs_seller"); //Input boxes wrapper ID
         var AddButton11       = $("#AddMoreFileBoxSeller"); //Add button ID
         
         var x11 = InputsWrapper11.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton11).click(function (e) {
         
                 //max input box allowed
                 if(x11 <= MaxInputs11) {
                     FieldCount++; //text box added ncrement
                     //add input box
          
                     $(InputsWrapper11).append('<div class="input-field col l3 m4 s12"><label for="contactNum11" class="dopr_down_holder_label active">Alternate Mobile Number: <span class="red-text">*</span></label><div  class="sub_val"><select  id="country_code_seller11" name="country_code" class="select2 browser-default"><option value="1">+91</option><option value="2">+1</option><option value="3">+3</option></select></div><input type="text" class="validate mobile_number" name="alter_mobile_number" id="alter_mobile_number" required placeholder="Text" > <a href="#" class="removeclassSeller rmvbtn" style="color: red !important">-</a></div>');
         
                    // $('#country_code_seller11').select2();
                    $('#country_code_seller11').select2({minimumResultsForSearch: -1});
                     x11++; //text box increment
                     
                     $("#AddMoreFileIdSeller").show();
                     
                     $('AddMoreFileBoxSeller').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x11 == 2) {
                         $("#AddMoreFileIdSeller").hide();
                         $("#lineBreak").html("<br>");
                     }
         
                 }
                 return false;
         });
          
         $("body").on("click",".removeclassSeller", function(e){ //user click on remove text
                 if( x11 > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x11--; //decrement textbox
                     
                         $("#AddMoreFileIdSeller").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxSeller').html("Add field");
                 }
             return false;
         }); 
         



         
        var MaxInputs12       = 1; //maximum extra input boxes allowed
        var InputsWrapper12   = $("#display_inputs_whatsapp_seller"); //Input boxes wrapper ID
        var AddButton12       = $("#AddMoreFileBoxWhatsappSeller"); //Add button ID
         
        var x12 = InputsWrapper12.length; //initlal text box count
        var FieldCount=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton12).click(function (e) {
            //  alert()
         
                 //max input box allowed
                 if(x12 <= MaxInputs12) {
                     FieldCount++; //text box added ncrement
                     //add input box
          
                     $(InputsWrapper12).append('<div class="input-field col l3 m4 s12" id="InputsWrapper2"><label for="alterWhatsapp" class="dopr_down_holder_label active">Alternate Whatsapp  Number: <span class="red-text">*</span></label><div  class="sub_val"><select  id="alterWhatsapp1" name="country_code" class="select2 browser-default"><option value="1">+91</option><option value="2">+1</option><option value="3">+3</option></select></div><input type="text" class="validate mobile_number" name="alterWhatsapp" id="alterWhatsapp" required placeholder="Text" > <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " ><input type="checkbox" id="copyAlterwhatsapp" data-toggle="tooltip" title="Check if alternate whatsapp number is same" onClick="copyAlterMobileNo()"  style="opacity: 1 !important;pointer-events:auto"></div> <a href="#" class="removeclassWSeller rmvbtn" style="color: red !important">-</a></div>');
                    
                    // $('#alterWhatsapp1').select2();
                    $('#alterWhatsapp1').select2({minimumResultsForSearch: -1});
                     x12++; //text box increment
                     
                     $("#AddMoreFileIdWhatsappSeller").show();
                     
                     $('AddMoreFileBoxWhatsappSeller').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x12 == 2) {
                         $("#AddMoreFileIdWhatsappSeller").hide();
                         $("#lineBreak").html("<br>");
                     }
         
                 }
                 return false;
         });
          
         $("body").on("click",".removeclassWSeller", function(e){ //user click on remove text
                 if( x12 > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x12--; //decrement textbox
                     
                         $("#AddMoreFileIdWhatsappSeller").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxWhatsappSeller').html("Add field");
                 }
             return false;
         });


                   
         
         var MaxInputs13       = 1; //maximum extra input boxes allowed
         var InputsWrapper13   = $("#display_inputs_KeyHolderEmail"); //Input boxes wrapper ID
         var AddButton13       = $("#AddMoreFileBoxKeyHolderEmail"); //Add button ID
         
         var x13 = InputsWrapper13.length; //initlal text box count
         var FieldCount13=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton13).click(function (e) {
                 //max input box allowed
                 if(x13 <= MaxInputs13) {
                     FieldCount13++; //text box added ncrement
                     //add input box
                     $(InputsWrapper13).append(' <div class="input-field col l3 m4 s12" id="InputsWrapper3"><label for="primary_email active" class="active">Secondary Email: <span class="red-text">*</span></label><input type="email" class="validate" name="primary_email" id="primary_email" required placeholder="Text"><a href="#" style="color: red !important" class="removeclass13 rmvbtn">-</a></div>');
                     x13++; //text box increment
                     
                     $("#AddMoreFileIdKeyHolderEmail").show();
                     
                     $('AddMoreFileBoxKeyHolderEmail').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x13 == 2) {
                        //  alert();
                         $("#AddMoreFileIdKeyHolderEmail").css('display','none');
                         $("#AddMoreFileIdKeyHolderEmail").hide();
                         $("#lineBreak13").html("<br>");
                     }
                 }
                 return false;
         });
         
         $("body").on("click",".removeclass13", function(e){ //user click on remove text
            // alert();
                 if( x13 > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x13--; //decrement textbox
                     
                         $("#AddMoreFileIdKeyHolderEmail").show();
                     
                         $("#lineBreak13").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxKeyHolderEmail').html("Add field");
                 }
             return false;
         }) ;


         var MaxInputs14       = 1; //maximum extra input boxes allowed
         var InputsWrapper14   = $("#display_inputs_seller_number"); //Input boxes wrapper ID
         var AddButton14       = $("#AddMoreFileBoxSellerName"); //Add button ID
         
         var x14 = InputsWrapper14.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton14).click(function (e) {
         
                 //max input box allowed
                 if(x14 <= MaxInputs14) {
                     FieldCount++; //text box added ncrement
                     //add input box
          
                     $(InputsWrapper14).append('<div class="input-field col l3 m4 s12"><label for="contactNum11" class="dopr_down_holder_label active">Alternate Mobile Number: <span class="red-text">*</span></label><div  class="sub_val"><select  id="country_code656" name="country_code" class="select2 browser-default"><option value="1">+91</option><option value="2">+1</option><option value="3">+3</option></select></div><input type="text" class="validate mobile_number" name="alter_mobile_number" id="alter_mobile_number" required placeholder="Text" > <a href="#" class="removeclassSeller rmvbtn" style="color: red !important">-</a></div>');
         
                    // $('#country_code656').select2();
                    $('#country_code656').select2({minimumResultsForSearch: -1});
                     x14++; //text box increment
                     
                     $("#AddMoreFileIdSellerName").show();
                     
                     $('AddMoreFileBoxSellerName').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x14 == 2) {
                         $("#AddMoreFileIdSellerName").hide();
                         $("#lineBreak").html("<br>");
                     }
         
                 }
                 return false;
         });
          
         $("body").on("click",".removeclassSeller", function(e){ //user click on remove text
                 if( x14 > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x14--; //decrement textbox
                     
                         $("#AddMoreFileIdSellerName").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxSellerName').html("Add field");
                 }
             return false;
         }); 


         var MaxInputs15       = 1; //maximum extra input boxes allowed
         var InputsWrapper15   = $("#display_inputs_tenant_name"); //Input boxes wrapper ID
         var AddButton15       = $("#AddMoreFileBoxTenantName"); //Add button ID
         
         var x15 = InputsWrapper15.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton15).click(function (e) {
         
                 //max input box allowed
                 if(x15 <= MaxInputs15) {
                     FieldCount++; //text box added ncrement
                     //add input box
          
                     $(InputsWrapper15).append('<div class="input-field col l3 m4 s12"><label for="contactNum11" class="dopr_down_holder_label active">Alternate Mobile Number: <span class="red-text">*</span></label><div  class="sub_val"><select  id="country_code_tenant" name="country_code" class="select2 browser-default"><option value="1">+91</option><option value="2">+1</option><option value="3">+3</option></select></div><input type="text" class="validate mobile_number" name="alter_mobile_number" id="alter_mobile_number" required placeholder="Text" > <a href="#" class="removeclassSeller rmvbtn" style="color: red !important">-</a></div>');
         
                    // $('#country_code_tenant').select2();
                    $('#country_code_tenant').select2({minimumResultsForSearch: -1});
                     x15++; //text box increment
                     
                     $("#AddMoreFileIdTenantrName").show();
                     
                     $('AddMoreFileBoxTenantName').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x15 == 2) {
                         $("#AddMoreFileIdTenantrName").hide();
                         $("#lineBreak").html("<br>");
                     }
         
                 }
                 return false;
         });
          
         $("body").on("click",".removeclassSeller", function(e){ //user click on remove text
                 if( x15 > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x15--; //decrement textbox
                     
                         $("#AddMoreFileIdTenantrName").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxTenantName').html("Add field");
                 }
             return false;
         }); 


         var MaxInputs16     = 1; //maximum extra input boxes allowed
         var InputsWrapper16 = $("#display_inputs_tenant_email"); //Input boxes wrapper ID
         var AddButton16     = $("#AddMoreFileBoxTanent"); //Add button ID
         
         var x16 = InputsWrapper16.length; //initlal text box count
         var FieldCount16=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton16).click(function (e) {
                 //max input box allowed
                 if(x16 <= MaxInputs16) {
                     FieldCount16++; //text box added ncrement
                     //add input box
                     $(InputsWrapper16).append('<div class="input-field col l3 m4 s12" id="InputsWrapper3"><label for="primary_email active" class="active">Secondary Email: <span class="red-text">*</span></label><input type="email" class="validate" name="primary_email" id="primary_email" required placeholder="Text"><a href="#" style="color: red !important" class="removeclass30 rmvbtn">-</a></div>');
                     x16++; //text box increment
                     
                     $("#AddMoreFileIdemailTenant").show();
                     
                     $('AddMoreFileBoxTanent').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x16 == 2) {
                        //  alert();
                         $("#AddMoreFileIdemailTenant").css('display','none');
                         $("#AddMoreFileIdemailTenant").hide();
                         $("#lineBreak3").html("<br>");
                     }
                 }
                 return false;
         });
         
         $("body").on("click",".removeclass30", function(e){ //user click on remove text
                 if( x16 > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x16--; //decrement textbox
                     
                         $("#AddMoreFileIdemailTenant").show();
                     
                         $("#lineBreak3").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxTanent').html("Add field");
                 }
             return false;
         }) ;

        
         var MaxInputs17       = 1; //maximum extra input boxes allowed
         var InputsWrapper17   = $("#display_inputs_SellerEmail"); //Input boxes wrapper ID
         var AddButton17       = $("#AddMoreFileBoxKeysellerEmail"); //Add button ID
         
         var x17 = InputsWrapper17.length; //initlal text box count
         var FieldCount17=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton17).click(function (e) {
                 //max input box allowed
                 if(x17 <= MaxInputs17) {
                     FieldCount17++; //text box added ncrement
                     //add input box
                     $(InputsWrapper17).append(' <div class="input-field col l3 m4 s12" id="InputsWrapper3"><label for="primary_email active" class="active">Builder Email: <span class="red-text">*</span></label><input type="email" class="validate" name="primary_email" id="primary_email" required placeholder="Text"><a href="#" style="color: red !important" class="removeclass17 rmvbtn">-</a></div>');
                     x17++; //text box increment
                     
                     $("#AddMoreFileIdSellerEmail").show();
                     
                     $('AddMoreFileBoxKeysellerEmail').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x17 == 2) {
                        //  alert();
                         $("#AddMoreFileIdSellerEmail").css('display','none');
                         $("#AddMoreFileIdSellerEmail").hide();
                         $("#lineBreak17").html("<br>");
                     }
                 }
                 return false;
         });
         
         $("body").on("click",".removeclass17", function(e){ //user click on remove text
            // alert();
                 if( x17 > 1 ) {
                         $(this).parent('div').remove(); //remove text box
                         x17--; //decrement textbox
                     
                         $("#AddMoreFileIdSellerEmail").show();
                     
                         $("#lineBreak17").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxKeysellerEmail').html("Add field");
                 }
             return false;
         }) ;



         var MaxInputs18  = 18; //maximum extra input boxes allowed
         var InputsWrapper18   = $("#display_inputs_Challenge_panel"); //Input boxes wrapper ID
         var AddButton18       = $("#AddMoreFileBoxChallenge_Panel"); //Add button ID
         
         var x18 = InputsWrapper18.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added

            $(AddButton18).click(function (e) {         
                //max input box allowed
                if(x18 <= MaxInputs18) {
                    var a = FieldCount++ ;
                    //add input box  

                    $.ajax({
                        type:'GET',
                        url: '/viewDientionDirection',
                        data: {aa:a},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html1 = response;
                            $(InputsWrapper18).append(html1);
                            // $('#name_initial_'+a).select2({minimumResultsForSearch: -1});
                            // $('#country_code_'+a).select2({minimumResultsForSearch: -1});
                            // $('#country_code_w_'+a).select2({minimumResultsForSearch: -1});
                            // $('#reporting_to_'+a).select2({minimumResultsForSearch: -1});
                            // $('#designation_'+a).select2({minimumResultsForSearch: -1});                          
                            

                        },
                        error: function(response){
                            console.log(response);
                                // $('#image-input-error').text(response.responseJSON.errors.file);
                        }
                    });

                    
                    // $(InputsWrapper18).append('<tr><td><select class="validate select2 browser-default" id="seller-category" data-placeholder="Company name, Branch name" name="seller-category"><option value="option 1" >Select</option><option value="option 1">Bedroom 1</option><option value="option 1">Bedroom 2</option><option value="option 1">Hall</option></select><label for="seller-category" class="active">Room</label></td><td><input type="text" class="validate" name="lead_assign1" id="lead_assign1" style="margin-left: -18px;"></td><td><input type="text" class="validate" name="lead_assign" id="lead_assign" style="margin-left: -10px;"></td><td><select class="validate select2 browser-default" id="seller-category" data-placeholder="Company name, Branch name" name="seller-category"><option value="option 1" >Select</option><option value="option 1">West</option><option value="option 1">North West</option><option value="option 1">North East</option></select><label for="seller-category" class="active">Direction</label></td><td><a href="#" style="color: red !important" class="removeclass18">-</a></td></tr>');            
                    // $('#alterWhatsapp1').select2();
                    // $('#alterWhatsapp1').select2({minimumResultsForSearch: -1});
                    x18++; //text box increment
                    
                    $("#AddMoreFileIdChallenge_panel").show();
                    
                    $('AddMoreFileBoxChallenge_Panel').html("Add field");
                    
                    // Delete the "add"-link if there is 3 fields.
                    if(x18 == 2) {
                        //  $("#AddMoreFileIdChallenge_panel").hide();
                        $("#lineBreak").html("<br>");
                    }
        
                }
                return false;
        });



        $("body").on("click",".removeclassViewDirection", function(e){ //user click on remove text
                 if( x18 > 1 ) {
                        //  $(this).parent('div').remove(); //remove text box
                        // $('#InputsChan').remove(); //remove text box
                        var e = $(this).parent().parent();
                        e.remove();
                         x18--; //decrement textbox
                     
                        //  $("#AddMoreFileIdWhatsapp").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxChallenge_Panel').html("Add field");
                 }
             return false;
        });



        var MaxInputs19  = 19; //maximum extra input boxes allowed
         var InputsWrapper19   = $("#display_inputs_looking_since"); //Input boxes wrapper ID
         var AddButton19       = $("#AddMoreFileBoxlookingsince"); //Add button ID
         
         var x = InputsWrapper19.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added

    $(AddButton19).click(function (e) {         
         //max input box allowed
         if(x <= MaxInputs19) {
             FieldCount++; //text box added ncrement
             //add input box  
             $(InputsWrapper19).append('<div class="row"><div class="input-field col l3 m4 s12 display_search" id="p2" style="display:none"><label for="loan_amt" id="loan_amt" class="active">Unit Information: </label><input type="text" class="validate" name="no_of_tw_parking" Placeholder="Enter Building ,complex, wing, Flat no"  id="no_of_tw_parking"  > <div id="AddMoreFileId3" class="addbtn" ><a href="#modal4" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> </div> </div><div class="input-field col l3 m4 s12 display_search"  id="p3" style="display:none"><select class="select2  browser-default" onChange="property_seen_details_hide_show()"  data-placeholder="Select" name="visited_by" id="visited_by"><option value="" disabled selected>Select</option><option value="resale_pc">Property Consultant</option> </select><label for="visited_by" class="active">Property Visited By</label>     </div> <div class="input-field col l3 m4 s12" id="p4" style="display:none" ><label for="wap_number" class="active">Date of Visit: <span class="red-text">*</span></label><input type="text" class="datepicker" name="date_of_visit"  required placeholder="Calender" style="border: 1px solid #e8e45b !important;height: 41px;"><span id="calender_span" style="display:none;color: white; border: solid 1px green; background-color: green;padding: 1px 10px 2px 10px;">No of days : 10</span><span id="tat_within" style="display:none;border-radius: 3px;color: white; border: solid 1px red; background-color: red; padding: 1px 15px 2px 10px;">No of days left : 10</span><span id="tat_after" style="display:none;border-radius: 3px;color: white; border: solid 1px green; background-color: green;padding: 1px 15px 2px 10px;">No of days left : 10</span></div><div class="input-field col l3 m4 s12" id="p5" style="display:none" > <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label><textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="property_seen_comment" rows="2" col="4"></textarea></div></div>');            
            // $('#alterWhatsapp1').select2();
            // $('#alterWhatsapp1').select2({minimumResultsForSearch: -1});
             x++; //text box increment
             
             $("#AddMoreFileIdlookingsince").show();
             
             $('AddMoreFileBoxlookingsince').html("Add field");
             
             // Delete the "add"-link if there is 3 fields.
             if(x == 2) {
                //  $("#AddMoreFileIdlookingsince").hide();
                 $("#lineBreak").html("<br>");
             }
 
         }
         return false;
 });



        $("body").on("click",".removeclassChallenge", function(e){ //user click on remove text
                 if( x > 1 ) {
                        //  $(this).parent('div').remove(); //remove text box
                        // $('#InputsChan').remove(); //remove text box
                        var e = $(this).parent().parent().parent();;
                        e.remove();
                         x--; //decrement textbox
                     
                        //  $("#AddMoreFileIdWhatsapp").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxlookingsince').html("Add field");
                 }
             return false;
        });

        
        var MaxInputs20  = 20; //maximum extra input boxes allowed
         var InputsWrapper20   = $("#display_inputs_wing_master"); //Input boxes wrapper ID
         var AddButton20       = $("#AddMoreFileBoxlookingsince"); //Add button ID
         
         var x = InputsWrapper20.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added

        $(AddButton20).click(function (e) {  
            // alert();       
            //max input box allowed
            if(x <= MaxInputs20) {
                FieldCount++; //text box added ncrement
                //add input box  
                $(InputsWrapper20).append('<div class="row"><div class="col l1"><div class="input-field col l1 m4 s12 display_search"><div class="AddMoreFileIdlookingsince" > <a href="#" id="AddMoreFileBoxlookingsince" style="color: red !important"> +</a></div><a href="/add-wing-details" id="add_location" class=" modal-trigger" style="color: red !important"> Edit</a></div></div><div class="col l11"> <a href="/add-wing-details" id="add_location" class=" modal-trigger" style="color: red !important"><div class="input-field col l1 m4 s12" id="InputsWrapper2"> <label class="active">Wing Name: <span class="red-text">*</span></label> <input type="text" class="validate" name="project_land_parcel" value="B1" placeholder="Enter" ></div> </a><div class="input-field col l1 m4 s12" id="InputsWrapper2"> <label class="active">Total Floor: <span class="red-text">*</span></label> <input type="text" class="validate" name="project_land_parcel" placeholder="Enter" ></div><div class="input-field col l1 m4 s12" id="InputsWrapper2"> <label class="active">Habitable Floor <span class="red-text">*</span></label> <input type="text" class="validate" name="project_land_parcel" placeholder="Enter" ></div><div class="input-field col l1 m4 s12" id="InputsWrapper2"> <label class="active">Unit per Floor: <span class="red-text">*</span></label> <input type="text" class="validate" name="project_land_parcel" placeholder="Enter" ></div><div class="input-field col l1 m4 s12" id="InputsWrapper2"> <label class="active">Refugee unit: <span class="red-text">*</span></label> <input type="text" class="validate" name="project_land_parcel" placeholder="Enter" ></div><div class="input-field col l1 m4 s12" id="InputsWrapper2"> <label class="active">Total Unit: <span class="red-text">*</span></label> <input type="text" class="validate" name="project_land_parcel" placeholder="Enter" ></div><div class="input-field col l2 m4 s12" id="display_search"> <label class="active" style="font-size: 10px !important;">Floor Plan: <span class="red-text">*</span></label> <input type="file" name="upload_photos" style="padding-top: 14px;"></div><div class="input-field col l2 m4 s12" id="InputsWrapper2"> <label class="active" style="font-size: 10px !important;" >Name Board : <span class="red-text">*</span></label> <input type="file" name="upload_photos" style="padding-top: 14px;"></div><div class="input-field col l1 m4 s12" id="InputsWrapper2"> <label class="active" >Parking Floor : <span class="red-text">*</span></label> <input type="text" class="validate" name="project_land_parcel" placeholder="Enter" ></div><div class="input-field col l1 m4 s12" id="InputsWrapper2"> <label class="active">Comment: <span class="red-text">*</span></label> <input type="text" class="validate" name="project_land_parcel" placeholder="Enter" ></div></div></div>');            
                // $('#alterWhatsapp1').select2();
                // $('#alterWhatsapp1').select2({minimumResultsForSearch: -1});
                x++; //text box increment
                
                $("#AddMoreFileIdlookingsince").show();
                
                $('AddMoreFileBoxlookingsince').html("Add field");
                
                // Delete the "add"-link if there is 3 fields.
                if(x == 2) {
                    //  $("#AddMoreFileIdlookingsince").hide();
                    $("#lineBreak").html("<br>");
                }
    
            }
            return false;
    });



        $("body").on("click",".removeclassChallenge", function(e){ //user click on remove text
                 if( x > 1 ) {
                        //  $(this).parent('div').remove(); //remove text box
                        // $('#InputsChan').remove(); //remove text box
                        var e = $(this).parent().parent().parent();;
                        e.remove();
                         x--; //decrement textbox
                     
                        //  $("#AddMoreFileIdWhatsapp").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxlookingsince').html("Add field");
                 }
             return false;
        });

    // ///////////////////////////////////////////////////

     //$('select').material_select();
     var MaxInputs       = 10; //maximum extra input boxes allowed
         var InputsWrapperC   = $("#display_inputs_CompanyMasterH"); //Input boxes wrapper ID
         var AddButtonC       = $("#AddMoreFileBoxCompanyMasterH"); //Add button ID
         
         var x = InputsWrapperC.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added
         
         //on add input button click
         $(AddButtonC).click(function (e) {
         
                 //max input box allowed
                 if(x <= MaxInputs) {
                     FieldCount++; //text box added ncrement
                     //add input box
                     var a = FieldCount++ ;
                     console.log(a)

                     $.ajax({
                        type:'GET',
                        url: '/append_company_hierarchy_form',
                        data: {aa:a},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html = response;
                            $(InputsWrapperC).append(html);
                            $('#name_initial_'+a).select2({minimumResultsForSearch: -1});
                            $('#country_code_'+a).select2({minimumResultsForSearch: -1});
                            $('#country_code_w_'+a).select2({minimumResultsForSearch: -1});
                            $('#reporting_to_'+a).select2({minimumResultsForSearch: -1});
                            $('#designation_'+a).select2({minimumResultsForSearch: -1});                          
                            

                        },
                        error: function(response){
                            console.log(response);
                                // $('#image-input-error').text(response.responseJSON.errors.file);
                        }
                    });

                    //  cus_name_sel_
                     
         
                    // $('#country_code656').select2();
                   
                     x++; //text box increment
                     
                     $("#AddMoreCompanyMasterH").show();
                     
                     $('AddMoreFileBoxCompanyMasterH').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x == 2) {
                         $("#AddMoreCompanyMasterH").hide();
                         $("#lineBreak").html("<br>");
                     }
         
                 }
                 return false;
         });
     
          
         $("body").on("click","removeclassCompany", function(e){ //user click on remove text
                      if( x > 1 ) {
                    //   alert(1);
                        //  $(this).parent('div').remove(); //remove text box
                        var e = $(this).parent().parent();
                        e.remove();
                         x--; //decrement textbox
                     
                         $("#AddMoreCompanyMasterH").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxCompanyMasterH').html("Add field");
                 }
             return false;
        
         }); 


         /// Society Key Members

         var MaxInputs       = 10; //maximum extra input boxes allowed
         var InputsWrapper   = $("#display_inputs_SocietyMasterH"); //Input boxes wrapper ID
         var AddButton       = $("#AddMoreFileBoxSocietyMasterH"); //Add button ID
         
         var x = InputsWrapper.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added
         
         //on add input button click
         $(AddButton).click(function (e) {

         
                 //max input box allowed
                 if(x <= MaxInputs) {
                     FieldCount++; //text box added ncrement
                     //add input box
                     var a = FieldCount++ ;
                     console.log(a)

                     $.ajax({
                        type:'GET',
                        url: '/append_society_hierarchy_form',
                        data: {aa:a},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html = response;
                            $(InputsWrapper).append(html);
                            $('#name_initial_'+a).select2({minimumResultsForSearch: -1});
                            $('#country_code_'+a).select2({minimumResultsForSearch: -1});
                            $('#country_code_w_'+a).select2({minimumResultsForSearch: -1});
                            $('#reporting_to_'+a).select2({minimumResultsForSearch: -1});
                            $('#designation_'+a).select2({minimumResultsForSearch: -1});                          
                            

                        },
                        error: function(response){
                            console.log(response);
                                // $('#image-input-error').text(response.responseJSON.errors.file);
                        }
                    });

                    //  cus_name_sel_
                     
         
                    // $('#country_code656').select2();
                   
                     x++; //text box increment
                     
                     $("#AddMoreSocietyMasterH").show();
                     
                     $('AddMoreFileBoxSocietyMasterH').html("Add field");
                     
                     // Delete the "add"-link if there is 3 fields.
                     if(x == 2) {
                         $("#AddMoreSocietyMasterH").hide();
                         $("#lineBreak").html("<br>");
                     }
         
                 }
                 return false;
         });
          
         $("body").on("click",".removeclassSociety", function(e){ //user click on remove text
                 if( x > 1 ) {
                        //  $(this).parent('div').remove(); //remove text box
                        var e = $(this).parent().parent();
                        e.remove();
                         x--; //decrement textbox
                     
                         $("#AddMoreSocietyMasterH").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxSocietyMasterH').html("Add field");
                 }
             return false;
         }); 

         var MaxInputs21  = 10; //maximum extra input boxes allowed
         var InputsWrapper21   = $("#display_inputs_Config_sample"); //Input boxes wrapper ID
         var AddButton21       = $("#AddMoreFileBoxConfig_sample"); //Add button ID
         
         var x21 = InputsWrapper21.length; //initlal text box count
         var FieldCount=1; //to keep track of text box added

            $(AddButton21).click(function (e) {         
                //max input box allowed
                if(x21 <= MaxInputs21) {
                    var a = FieldCount++ ;
                    //add input box  

                    $.ajax({
                        type:'GET',
                        url: '/append_config_sample',
                        data: {aa:a},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html = response;
                            $(InputsWrapper21).append(html);
                            // $('#name_initial_'+a).select2({minimumResultsForSearch: -1});
                            // $('#country_code_'+a).select2({minimumResultsForSearch: -1});
                            // $('#country_code_w_'+a).select2({minimumResultsForSearch: -1});
                            // $('#reporting_to_'+a).select2({minimumResultsForSearch: -1});
                            // $('#designation_'+a).select2({minimumResultsForSearch: -1});                          
                            

                        },
                        error: function(response){
                            console.log(response);
                                // $('#image-input-error').text(response.responseJSON.errors.file);
                        }
                    });

                    
                    // $(InputsWrapper21).append('<tr><td><select class="validate select2 browser-default" id="seller-category" data-placeholder="Company name, Branch name" name="seller-category"><option value="option 1" >Select</option><option value="option 1">Bedroom 1</option><option value="option 1">Bedroom 2</option><option value="option 1">Hall</option></select><label for="seller-category" class="active">Room</label></td><td><input type="text" class="validate" name="lead_assign1" id="lead_assign1" style="margin-left: -21px;"></td><td><input type="text" class="validate" name="lead_assign" id="lead_assign" style="margin-left: -10px;"></td><td><select class="validate select2 browser-default" id="seller-category" data-placeholder="Company name, Branch name" name="seller-category"><option value="option 1" >Select</option><option value="option 1">West</option><option value="option 1">North West</option><option value="option 1">North East</option></select><label for="seller-category" class="active">Direction</label></td><td><a href="#" style="color: red !important" class="removeclass21">-</a></td></tr>');            
                    // $('#alterWhatsapp1').select2();
                    // $('#alterWhatsapp1').select2({minimumResultsForSearch: -1});
                    x21++; //text box increment
                    
                    $("#AddMoreFileIdConfig_sample").show();
                    
                    $('AddMoreFileBoxConfig_sample').html("Add field");
                    
                    // Delete the "add"-link if there is 3 fields.
                    if(x21 == 2) {
                        //  $("#AddMoreFileIdConfig_sample").hide();
                        $("#lineBreak").html("<br>");
                    }
        
                }
                return false;
        });



        $("body").on("click",".removeclassConfig", function(e){ //user click on remove text
                 if( x21 > 1 ) {
                        //  $(this).parent('div').remove(); //remove text box
                        // $('#InputsChan').remove(); //remove text box
                        var e = $(this).parent().parent();
                        e.remove();
                         x21--; //decrement textbox
                     
                        //  $("#AddMoreFileIdWhatsapp").show();
                     
                         $("#lineBreak").html("");
                     
                         // Adds the "add" link again when a field is removed.
                         $('AddMoreFileBoxConfig_sample').html("Add field");
                 }
             return false;
        });

         


         
         </script>

        <script type="text/javascript">
             $(function() {
                $( "#expected_closure_date" ).datepicker
                ({  
                    minDate: new Date(), 
                    // format: 'mm-dd-yyyy'; 
                }); 
              });
        </script>
         <script type="text/javascript">
             $(function() {
                $( "#date_of_visit" ).datepicker({  maxDate: new Date() }); 
              });
        </script>
        
         <script type="text/javascript">
             $(function() {
                $( "#shifing_date1" ).datepicker({  minDate: new Date() }); 
              });
        </script>
       <!--  show only current not seen past date and furure dates -->
        <!--  <script type="text/javascript">
            var dateToday = new Date(); 
            var lastDay = new Date();  
             $(function () {
                 $("#expected_closure_date").datepicker({ 
                     minDate: dateToday,
                     maxDate: lastDay, 
                 });
             });
        </script> -->
      <!-- END PAGE LEVEL JS-->
   </body>
</html>