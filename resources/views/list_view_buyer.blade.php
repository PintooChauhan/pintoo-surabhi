
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
    
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/4.1.0/css/fixedColumns.dataTables.min.css">

<style>
  select
{
    display: block !important;
}

</style>

      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">
    
        @php
        $check_s_add = 0;
        $check_s_edit = 0;
        $check_s_delete = 0;
        @endphp

        @if(isset($access_check) && !empty($access_check) )
          @foreach($access_check as $ackey=>$acvalue)
            @php
              $check_s_add = $acvalue->s_add;
              $check_s_edit = $acvalue->s_edit;
              $check_s_delete = $acvalue->s_delete;
            @endphp
          @endforeach
        @endif

@if(!empty($check_s_add) )
  @if($check_s_add==1)
  <div class="row">

      <!-- buyer page button -->
      <div class="col s2" style="float: right;margin-left:-12px">
        <button type="button" class="btn btn-primary btn-md" style="margin-left: -12px; ">
          <a href="/buyer"  class="waves-effect waves-light  " style="color: white !important"> +  Add Buyer</a>
        </button>
      </div>
      <!-- buyer page button -->  
    </div>
  @endif
@endif
  
  <!-- Filter Start -->
  {{-- <x-filter_buyer-layout></x-filter_buyer-layout> --}}
  {{-- <x-filter-buyer-layout></x-filter-buyer-layout> --}}
  {{-- <x-filter_buyer-layout></x-filter_buyer-layout> --}}
  {{-- <x-filter1></x-filter1> --}}
  {{-- <x-filter.filter_buyer></x-filter.filter_buyer> --}}
  <x-filter1></x-filter1>
  <!-- Filter End --> 

  <div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">

                <div class="row">
                    <div class="col s12">
                        <div class="tableFixHead">
                            <table id="scroll-vert-hor21" class="display nowrap table-hover">
                            <thead>
                                <tr class="pin">
                                    <th>Days</th>
                                    <th>Lead</th>
                                    <th>Name</th>
                                    <th>Cont. No.</th>
                                    <th>Configuration</th>
                                    <th>Unit</th>
                                    <th>Budget</th>          
                                                                       
                                    <th>Sub Location</th>   
                                    <th>Unit Status</th>                
                                     
                                    <th>Area</th>      
                                    <th>Bldg. Age</th>      
                                    <th>Finalization Month</th>                                       
                                    <th>Looking Since</th>      
                                    <th>Possession</th>      
                                    <th>Residence Address</th>
                                    <!-- <th>FUP Date  </th>       -->
                                    <th>Stage - Intensity</th>
                                    <!-- <th>F2F Done</th> -->
                                    <th>Lead Source</th>
                                    <?php 
                                      if( in_array($role,$roles) ){ ?>
                                        <th>Assigned Name</th>
                                 
                                    @if(!empty($check_s_delete) )
                                      @if($check_s_delete==1)
                                      <th>Action</th>
                                      @endif
                                    @endif

                                    <?php } ?>


                                  
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($allData as $value)
                                @php
                                if($value->buyer_tenant_id <= 100){
                                        
                                        $id = str_pad($value->buyer_tenant_id,3, '0', STR_PAD_LEFT);
                                    }else{
                                        $id = $value->buyer_tenant_id;
                                    }
                                @endphp
                                  <?php 
                                    $loc  = array();
                                    $decodeEle1 = json_decode($value->sublocation_id) ;
                                    if(!empty($decodeEle1)){
                                      if(!empty($decodeEle1[0])){
                                      $get1 = DB::select("select sub_location_name from dd_sub_location where sub_location_id in(".$decodeEle1[0].")  ");
                                      foreach($get1  as $key =>  $val1){
                                          // echo ).', ';
                                          array_push($loc,ucwords($val1->sub_location_name));
                                      } 
                                    }
                                  }


                                  $lead_source_name = $value->lead_source_name;
                                  $finalName = '';
                                  $sub1 = substr($lead_source_name, 0, 25);
                                  if(strlen($sub1) < 25){
                                    $ct = 25- strlen($sub1);
                                 
                                    if(!empty($sub1)){
                                      $finalName =  $sub1.str_repeat('&nbsp;', $ct);
                                    }else{
                                      $finalName =  str_repeat('&nbsp;', 41);
                                    }
                                     
                                    
                                  }elseif(strlen($lead_source_name) > 25){
                                      $finalName =  substr($lead_source_name, 0, 25);
                                    
                                  }else{
                                      $finalName = str_repeat('&nbsp;', 41);
                                    
                                    }


                                    $customer_res_address = $value->customer_res_address;
                                    $finalNameAdd = '';
                                    $sub2 = substr($customer_res_address, 0, 35);

                                    if(strlen($customer_res_address) > 35){
                                      $finalNameAdd =  $sub2;
                                    }else{
                                      $finalNameAdd =  $customer_res_address;
                                    }
                                  
                                        $unt1 = array();
                                        if( $value->unit_type_id == 'residential' ){
                                                $decodeEle1 = json_decode($value->units) ;
                                                if(!empty($decodeEle1)){
                                                  if(!empty($decodeEle1[0])){
                                                  $get1 = DB::select("select unit_type_residential_name from dd_unit_type_residential where unit_type_residential_id in(".$decodeEle1[0].")  ");
                                                  foreach($get1 as $val1){
                                                    array_push($unt1,ucwords($val1->unit_type_residential_name));
                                                  //  echo ucwords($val1->unit_type_residential_name).', ';
                                                  } 
                                                }
                                              }
                                            }else{
                                              $decodeEle1 = json_decode($value->units) ;
                                                if(!empty($decodeEle1)){
                                                  if(!empty($decodeEle1[0])){
                                                  $get1 = DB::select("select unit_type_commercial_name from dd_unit_type_commercial where unit_type_commercial_id in(".$decodeEle1[0].")  ");
                                                  foreach($get1 as $val1){
                                                    // echo ucwords($val1->unit_type_commercial_name).', ';
                                                    array_push($unt1,ucwords($val1->unit_type_commercial_name));
                                                  } 
                                                }
                                              }
                                            }

                                            $conf = array();
                                            if( $value->unit_type_id == 'residential' ){
                                              $decodeEleConf = json_decode($value->configuration_id) ;
                                              if(!empty($decodeEleConf)){
                                                if(!empty($decodeEleConf[0])){
                                                $getConf = DB::select("select configuration_name from dd_configuration where configuration_id in(".$decodeEleConf[0].")  ");
                                                foreach($getConf as $valConf){
                                                  array_push($conf,ucwords($valConf->configuration_name));
                                                  // echo ucwords($val->configuration_name).', ';
                                                } 
                                              }
                                            }
                                          }

                                      
                                         ?>

                                  
                                
                                <tr >
                                <td title="<?= date_format(date_create($value->c_date),"d-m-Y H:i"); ?>">  <?php $d = date('Y-m-d');
                                      $s = $value->c_date;
                                      $date = strtotime($s);
                                      $date1=date_create($d);
                                      $date2=date_create(date('Y-m-d', $date));
                                      $diff=date_diff($date1,$date2);
                                      echo $diff->format("%a");
                                      ?>    </td>
                                    <td><?php
                                            if( $value->lead_by_name == 'property consultant' ){
                                              echo "PC";
                                            }else{
                                              echo "SR";
                                            }
                                      ?></td>
                                    
                                    <td title="<?= $lead_source_name; ?>">
                                      @if($check_s_edit==1)
                                      
                                      <a target="_blank" href="/buyer?buyer_tenant_id={{$value->buyer_tenant_id}}">  <?php  echo ucfirst($finalName); ?></a>
                                        
                                      @else
                                      <a href="javascript:void(0);">  <?php  echo ucfirst($finalName); ?></a>
                                      @endif
                                    </td>

                                    <td>{{ $value->contact_no }}</td>
                                 
                                 
                                    <td title="<?php print_r( implode(', ',$conf) ); ?>"> <?php
                                          if(!empty($conf)) {
                                            $subconf = substr(implode(', ',$conf), 0, 10);
                                            if(strlen($subconf) < 10){
                                              echo  $subconf;
                                            }else{
                                              echo  $subconf.'...';
                                            }
                                          }
                                         ?>
                                    </td>
                                    <td title="<?php print_r( implode(', ',$unt1) ); ?>"> 
                                      <?php 
                                            if(!empty($unt1)) {
                                              $subunit1 = substr(implode(', ',$unt1), 0, 10);
                                              if(strlen($subunit1) < 10){
                                                echo  $subunit1;
                                              }else{
                                                echo  $subunit1.'...';
                                              }
                                            }
                                          ?>
                                    </td>
                                    <td>{{$value->min_budget}} - {{$value->max_budget}}</td>
                                   
                                  
                                    
                            
                                    <td title="<?php print_r(implode(', ',$loc));?>">
                                          <?php 
                                           if(!empty($loc)) {
                                            $sub = substr(implode(', ',$loc), 0, 35);
                                            if(strlen($sub) < 35){
                                              echo  $sub;
                                            }else{
                                              echo  $sub.'...';
                                            }
                                          }
                                          ?>
                                    </td>

                                    <td>
                                      @if($value->unit_status == 'under_construction')
                                          UC
                                      @elseif($value->unit_status == 'under_construction_and_rtmi' )
                                           UC & RTMI 
                                      @elseif($value->unit_status == 'rtmi')
                                          RTMI
                                      @endif
                                      </td>
                                    <td>{{$value->min_carpet_area}} - {{$value->max_carpet_area}} </td>
                                    <td>@if(!empty($value->building_age)) {{$value->building_age}} Yrs @endif</td>                                   
                                   
                                    <td><?php 
                                        $decodeEle3 = json_decode($value->finalization_month) ;
                                        if(!empty($decodeEle3)){
                                          // print_r($decodeEle3 );
                                          if(!empty($decodeEle3[0])){
                                         
                                            print_r(ucwords($decodeEle3[0]));
                                          
                                        }
                                      }
                                      ?>
                                    <td>{{ucwords($value->looking_since)}}</td>
                                    <td>@if(!empty($value->possision_year) && $value->possision_year != 'null') {{str_replace('year','',$value->possision_year)}} Yrs @endif</td> 
                                   
                                          
                                   </td>
                                    <td title="<?= $customer_res_address; ?>"><?php echo $finalNameAdd; ?></td>
                                    <!-- <td></td> -->
                                    <td title="{{ucwords($value->lead_stage)}} , {{ucwords($value->lead_intesity_id)}} , {{ucwords($value->lead_analysis)}} ">{{ucwords($value->lead_stage)}}</td>
                                    <!-- <td>  </td> -->
                                    <td>   {{ucwords($value->lead_source_name1)}}  </td>
                                    <?php 
                                      if( in_array($role,$roles) ){ ?>
                                        <td>   {{ucwords($value->lead_assigned_name)}}  </td>
                                      
                                      @if(!empty($check_s_delete) )
                                        @if($check_s_delete==1)
                                        <td>
                                          <a href="/deleteBuyer?buyer_tenant_id={{$value->buyer_tenant_id}}" onclick="return confirm('Are you sure you want to delete this buyer?');">Delete</a>
  
                                        </td>
                                        @endif
                                      @endif

                                    <?php } ?>
                               
                                </tr>
                                @endforeach
 

                            </tbody>
                            
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
 
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/4.1.0/js/dataTables.fixedColumns.min.js"></script>
    <!-- END PAGE LEVEL JS-->

<script>

  $( document ).ready(function() {
    $("#filter_form").submit(function(e) {
      
      e.preventDefault();
      //  debugger;
      // alert("filter");
      var check = 'check';
      var lead_campaign_name = $("#lead_campaign_name").val();
      var lead_assigned_name = $("#lead_assigned_name").val();
      var unit_type = $("#unit_type").val();
      var door_facing_direction = $("#door_facing_direction").val();
      var floor_choice = $("#floor_choice").val();
      var purpose = $("#purpose").val();
      var own_contribution = $("#own_contribution").val();
      var contribution_source = $("#contribution_source").val();
      var oc_time_period = $("#oc_time_period").val();
      var loan_status = $("#loan_status").val();
      var looking_since = $("#looking_since").val();
      var building_type = $("#building_type").val();
      var parking_type = $("#parking_type").val();
      var property_seen = $("#property_seen").val();
      var payment_plan = $("#payment_plan").val();
      var expected_closure_date = $("#expected_closure_date_buyer").val();
      window.location.href = "{{asset('buyer-list-view')}}"+"?check="+check+"&lead_campaign_name="+lead_campaign_name+"&lead_assigned_name="+lead_assigned_name+"&unit_type="+unit_type+"&door_facing_direction="+door_facing_direction+"&floor_choice="+floor_choice+"&purpose="+purpose+"&own_contribution="+own_contribution+"&contribution_source="+contribution_source+"&oc_time_period="+oc_time_period+"&loan_status="+loan_status+"&looking_since="+looking_since+"&building_type="+building_type+"&parking_type="+parking_type+"&property_seen="+property_seen+"&payment_plan="+payment_plan+"&expected_closure_date="+expected_closure_date;

    });

  });

</script>