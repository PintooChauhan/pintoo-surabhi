<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
            
               <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">ADD NEW USER</span><hr>  </div>

         

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >
        <form method="POST" action="/register">
            @csrf
                <div class="row">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">name: <span class="red-text">*</span></label>
                        <input type="text" id="name" class="validate" name="name" :value="old('name')"    placeholder="Enter"  >                             
                               
           
                        </div>
                        

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Email: <span class="red-text">*</span></label>
                            <input id="email" class="validate"  type="email" name="email" :value="old('email')"    placeholder="Enter"  >                             
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Password : <span class="red-text">*</span></label>
                            <input id="password"  class="validate"  type="password"  name="password"  required autocomplete="new-password"  placeholder="Enter">                             
                        </div>  

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Confirm Password : <span class="red-text">*</span></label>
                            <input id="password_confirmation"  class="validate"  type="password"  name="password_confirmation"  required autocomplete="new-password"  placeholder="Enter">                             
                        </div>               
                       
                </div>

                <div class="row">
                         <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="role" name="role" class="validate select2 browser-default">
                                <option value="" disabled selected>Select Role</option>
                                    <option value="admin">Admin</option>                                    
                                    <option value="branch_manager" >Branch Manager</option>
                                    <option value="executive" >Executive</option>
                                </select>
                                <label class="active">User Role</label>
                        </div>

                </div>
                
                <hr>
                <div class="row">
                    <div class="col l3 m4 s12">
                            <input type="submit" value="Register" >
                    </div>
                </div>
        </form>


        </div>
           
         
            

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     