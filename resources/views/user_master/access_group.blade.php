<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}
table.bordered td{
    padding: 9px 30px 6px 9px !important
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
            
               <!-- <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">ADD NEW USER</span><hr>  </div> -->

            <div class="collapsible-body"  id='budget_loan' style="display:block" >

            <div class="row">
                <div class="input-field col l4 m4 s12" id="InputsWrapper2">
                <label  class="active">Access Level Name : <span class="red-text">*</span></label>
                           <input type="text" class="validate" name="access_level"    placeholder="Enter"  >                             
                </div>
                <!-- <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-large  waves-effect waves-light" onClick="alert('In Progress')" type="button" name="action" style="line-height: 43px !important;height: 43px !important">Search</button>                        

                    </div>  
                    <div class="input-field col l2 m2 s6 display_search">
                    <button class="btn-large  waves-effect waves-light green darken-1" onClick="add_industry_form()" type="button" name="action" style="line-height: 43px !important;height: 43px !important">Add Access Group</button>                        

                    </div>  
                    <div class="input-field col l2 m2 s6 display_search">
                    <a href="/user-form"  class="btn-large  waves-effect waves-light green darken-1" style="line-height: 43px !important;height: 43px !important"> Add User</a> 

                    </div>   -->
            </div>
            <br>
                <div class="row">
                    <table class="bordered">
                        <thead>
                        <tr style="background-color:green;color:white">
                            <th>Section Name</th>
                            <th width="8%">View</th>
                            <th width="8%">Add</th>
                            <th width="8%">Edit</th>
                            <th width="8%">Delete</th>

                            <th>Branch Name</th>
                            <th width="8%">View</th>
                            <th width="8%">Add</th>
                            <th width="8%">Edit</th>
                            <th width="8%">Delete</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>User Master</td>
                            <td>
                                <select  id="view_section" name="view_section" class="validate select2 browser-default">
                                    <option value="0" >No</option>
                                    <option value="1" >Yes</option>                                                                        
                                </select>
                                <label class="active">Branch</label>
                            </td>
                            <td>
                                <select  id="add_section" name="add_section" class="validate select2 browser-default">
                                    <option value="1" >Yes</option>                                    
                                    <option value="0" >No</option>
                                </select>
                                <label class="active">Branch</label>
                            </td>
                            <td>
                                <select  id="edit_section" name="edit_section" class="validate select2 browser-default">
                                    <option value="1" >Yes</option>                                    
                                    <option value="0" >No</option>
                                </select>
                                <label class="active">Branch</label>
                            </td>
                            <td>
                                <select  id="delete_section" name="delete_section" class="validate select2 browser-default">
                                    <option value="1" >Yes</option>                                    
                                    <option value="0" >No</option>
                                </select>
                                <label class="active">Branch</label>
                            </td>

                            <td>Own Branch</td>
                            
                            <td>
                                <select  id="view_branch" name="view_branch" class="validate select2 browser-default">
                                    <option value="0" >No</option>
                                    <option value="1" >Yes</option>                                                                        
                                </select>
                                <label class="active">Branch</label>
                            </td>
                            <td>
                                <select  id="add_branch" name="add_branch" class="validate select2 browser-default">
                                    <option value="1" >Yes</option>                                    
                                    <option value="0" >No</option>
                                </select>
                                <label class="active">Branch</label>
                            </td>
                            <td>
                                <select  id="edit_branch" name="edit_branch" class="validate select2 browser-default">
                                    <option value="1" >Yes</option>                                    
                                    <option value="0" >No</option>
                                </select>
                                <label class="active">Branch</label>
                            </td>
                            <td>
                                <select  id="delete_branch" name="delete_branch" class="validate select2 browser-default">
                                    <option value="1" >Yes</option>                                    
                                    <option value="0" >No</option>
                                </select>
                                <label class="active">Branch</label>
                            </td>
                        
                        </tr>
                        </tbody>
                    </table>
  
                </div>

                <br>

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect green waves-light" onClick="alert('In Progress')" type="button" name="action">Save</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <!-- <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Cancel</button>                         -->
                        <a href="/users-list" class="waves-effect waves-green btn-small" style="background-color: red;">Cancel</a>

                    </div>    
                </div> 

            </div>


             
     
         
            

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     