<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/data-tables/css/jquery.dataTables.min.css')}}">
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="app-assets/vendors/data-tables/css/select.dataTables.min.css"> -->
  
      <!-- <script src="app-assets/js/scripts/data-tables.js"></script> -->
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.0/css/fixedColumns.dataTables.min.css"> -->
      <!-- END PAGE LEVEL JS-->






      <style>


div.dataTables_wrapper {
    width: 100% !important;
    margin: 0 auto;
}
select {
    display: initial !important; ;
}

/*::-webkit-scrollbar {
  display: none;
}*/
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.dataTables_filter input {
    border: 1px solid #aaa;
    border-radius: 3px;
    padding: 0px !important;
}
 


.dataTables_scrollBody{
    height: auto !important;
}


</style>
<style>
        #loader {
            border: 12px solid #f3f3f3;
            border-radius: 50%;
            border-top: 12px solid #444444;
            width: 70px;
            height: 70px;
            animation: spin 1s linear infinite;
        }
          
        @keyframes spin {
            100% {
                transform: rotate(360deg);
            }
        }
          
        .center {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }
    </style>

<script src="{{ asset('app-assets/js/custom/buyer.js') }}"></script>
      <!-- BEGIN: Page Main class="main-full"-->
  
      <div id="main" class="main-full" style="min-height: auto">
         <div class="row">
         <div id="loader" class="center"></div>
         <input type="hidden" value="@if(isset($buyerId) && !empty($buyerId)) {{$buyerId['buyer_tenant_id']}} @endif" id="buyer_tenant_id">
         <input type="hidden"  id="customer_id" value="@if(isset($getCustomerData) && !empty($getCustomerData)) {{$getCustomerData[0]->id}} @endif">
           
               <x-leadinfor1-layout :buyerId="$buyerId"  :getBuyerData="$getBuyerData" :getCustomerData="$getCustomerData" :getLeadSourceData="$getLeadSourceData"></x-leadinfor1-layout>
                <x-requirementr1-layout :getBuyerData="$getBuyerData" :getSpecificChoiceData="$getSpecificChoiceData"></x-requirementr1-layout>
                <x-requirementr2-layout :getBuyerData="$getBuyerData" :getPropertySeenData="$getPropertySeenData"  :getCustomerData="$getCustomerData" :getSpecificChoiceData="$getSpecificChoiceData"></x-requirementr2-layout>
                <x-requirementr3-layout :getBuyerData="$getBuyerData" :getSpecificChoiceData="$getSpecificChoiceData"></x-requirementr3-layout>
                
                <x-requirementr5-layout ></x-requirementr5-layout>
            <hr>                   
            <x-requirementr6-layout :getBuyerData="$getBuyerData" :getmemberData="$getmemberData" :getChallengesData="$getChallengesData"></x-requirementr6-layout>
            

            <!-- <a href="#" onClick="saveBuyer()">Save</a> -->
            <x-all_modals-layout></x-all_modals-layout>
            <x-all_modals-layout></x-all_modals-layout>
             <hr>

             <div class="row">
                 <div class="input-field col l3 m4 s12" id="InputsWrapper2">

                    <div class="row" >
                        <div class="input-field col l2 m2 s6 display_search">
                        <div class="preloader-wrapper small active" id="loader5" style="display:none">
                            <div class="spinner-layer spinner-green-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="row" id="submitMain">
                        <div class="col l4 m4">
                            <a href="javascript:void(0)" onClick="saveBuyer()" class=" save_challeges_solution btn-small  waves-effect waves-light green darken-1 " id="save_challeges_solution">Save</a>
                        </div>
                        
                    </div>
                </div> 
             </div>
             <!-- Code by avinash -->
 </div>
            
  
 
                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->
        <script>
        document.onreadystatechange = function() {
            if (document.readyState !== "complete") {
                document.querySelector(
                  "body").style.visibility = "hidden";
                document.querySelector(
                  "#loader").style.visibility = "visible";
            } else {
                document.querySelector(
                  "#loader").style.display = "none";
                document.querySelector(
                  "body").style.visibility = "visible";
            }
        };
    </script>

        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
    

 <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <!-- <script src="app-assets/js/vendors.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('app-assets/vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <!-- <script src="app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script> -->
    <!-- <script src="app-assets/vendors/data-tables/js/dataTables.select.min.js"></script> -->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <!-- <script src="app-assets/js/plugins.js"></script> -->
    <!-- <script src="app-assets/js/search.js"></script> -->
    <!-- <script src="app-assets/js/custom/custom-script.js"></script> -->
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{ asset('app-assets/js/scripts/data-tables.js')}}"></script>
    <!-- END PAGE LEVEL JS-->