$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// $('#add_company_form').submit(function(e) {
//    e.preventDefault();
//    let formData = new FormData(this);
//    $('#image-input-error').text('');

//    $.ajax({
//       type:'POST',
//       url: '/add-company-master',
//        data: formData,
//        contentType: false,
//        processData: false,
//        success: (response) => {
//         console.log(response); //return

//          if (response.success=='done') {
//           $(".print-error-msg").css('display','none');
//           console.log(response);
//            this.reset();
//            window.location.href = 'http://surabhi.microlan.in/project-master/?gr_id='+response.lastID;
//           //  alert('Image has been uploaded successfully');
//           //  location.reload();
//          }else{
//           printErrorMsg(response.error)
//          }
//        },
//        error: function(response){
//           console.log(response);
//             // $('#image-input-error').text(response.responseJSON.errors.file);
//        }
//    });

//    function printErrorMsg (msg) {
//     $(".print-error-msg").find("ul").html('');
//     $(".print-error-msg").css('display','block');
//     $.each( msg, function( key, value ) {
//         $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
//     });
//   }
// });

// project Master
$('#project_master_form').submit(function(e) {
  // alert();
  $(this).find('input[type=checkbox]:not(:checked)').prop('checked', true).val(0);//.attr('disabled');
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');
  var group_name = $('#group_name').val();
  if(group_name == null){
    $('#direct_error').html('Group Name is required.');
    return false;
  }
  $.ajax({
     type:'POST',
     url: '/add-project-master',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
        
       // console.log('http://surabhi.microlan.in/society-master/?pid='+response.lastID);
       console.log(response); //return

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
        
          this.reset();
          // window.location.href = 'http://surabhi.microlan.in/society-master/?project_id='+response.lastID;
          window.location.href = "/company-master";
          // alert('Image has been uploaded successfully');
          // location.reload();
        }else{
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});

//society master
$('#society_master_form').submit(function(e) {    
  
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');

  $.ajax({
     type:'POST',
     url: '/add-society-master',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       console.log(response);// return;
        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
         this.reset();
        //  window.location.href = 'http://surabhi.microlan.in/wing-master/?society_id='+response.lastID;
        window.location.href = "/company-master";
        //  console.log(response);
        //   this.reset();
        //   alert('Image has been uploaded successfully');
        //   location.reload();
        }else{
        //  console.log(response.error);return

         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});



//wing master
$('#wing_master_form').submit(function(e) {    
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');

  $.ajax({
     type:'POST',
     url: '/add-wing-master',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       console.log(response);//return

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
         console.log(response);
          this.reset();
          window.location.href = 'http://surabhi.microlan.in/unit-master/?society_id='+response.lastID;
          //alert('Image has been uploaded successfully');
          // location.reload();
        }else{
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});


//update in op_unit master
$('#add_unit_info_form').submit(function(e) {    
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');

  $.ajax({
     type:'POST',
     url: '/add_unit_info',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       console.log(response);//return

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
         console.log(response);
          // this.reset();
          // alert('Information updated successfully');
          // // window.close();
          // location.reload();
        }else{
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});


//unit master
$('#add_unit_master_form').submit(function(e) {    
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');

  $.ajax({
     type:'POST',
     url: '/add-unit-master',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       console.log(response);//return

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
         console.log(response);
          this.reset();
          alert('Information updated successfully');
         // window.close();
         location.reload();
         
        }else{
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});

//update forms
// Company  Master update
$('#update_company_form').submit(function(e) {
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');
  $('#loader1').css('display','block');
  $('#submitButton').css('display','none');

  $.ajax({
     type:'POST',
     url: '/update-company-master',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       console.log(response);// return

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
         console.log(response);
         location.reload();

          // if (confirm('Are you sure you want to continue!')) {
          //   window.location.href = 'http://surabhi.microlan.in/project-master/?gr_id='+response.lastID;
          //   console.log('Thing was saved to the database.');
          // } else {
          //   window.location.href = 'http://surabhi.microlan.in/company-master';
          
          // }
        }else{
          $('#loader1').css('display','none');
          $('#submitButton').css('display','block');
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});



// project Master update
$('#project_master_form_update').submit(function(e) {
  // alert(1)
  $(this).find('input[type=checkbox]:not(:checked)').prop('checked', true).val(0);//.attr('disabled');
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');
  $('#loader1').css('display','block');
  $('#submitButton').css('display','none');

  $.ajax({
     type:'POST',
     url: '/update-project-master',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       /// console.log('http://surabhi.microlan.in/society-master/?project_id='+response.lastID);
       console.log(response); //return

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');

            // if (confirm('Are you sure you want to continue!')) {
            //   window.location.href = 'http://surabhi.microlan.in/society-master/?project_id='+response.lastID;//http://surabhi.microlan.in/
            //   console.log('Thing was saved to the database.');
            // } else {
            //   window.location.href = 'http://surabhi.microlan.in/company-master';         
            // }
            
            window.location.href = "/company-master";

        }else{
          $('#loader1').css('display','none');
          $('#submitButton').css('display','block');
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});


//update society master
$('#society_master_form_update').submit(function(e) {    
  
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');
  $('#loader1').css('display','block');
  $('#submitButton').css('display','none');

  $.ajax({
     type:'POST',
     url: '/update-society-master',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       console.log(response);//return

       

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
         location.reload();
        // if (confirm('Are you sure you want to continue!')) {
        //   window.location.href = 'http://surabhi.microlan.in/wing-master/?society_id='+response.lastID;
        //   console.log('Thing was saved to the database.');
        // } else {
        //   window.location.href = 'http://surabhi.microlan.in/company-master';
        //   // Do nothing!
        //   //console.log('Thing was not saved to the database.');
        // }
          
     
        }else{
          $('#loader1').css('display','none');
          $('#submitButton').css('display','block');
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});


//update wing master
$('#wing_master_form_update').submit(function(e) {    
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');
  $('#loader1').css('display','block');
  $('#submitButton').css('display','none');

  $.ajax({
     type:'POST',
     url: '/update-wing-master',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       console.log(response);//return

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
         console.log(response);
          
            // if (confirm('Are you sure you want to continue!')) {
            //   window.location.href = 'http://surabhi.microlan.in/unit-master/?society_id='+response.lastID;
            //   console.log('Thing was saved to the database.');
            // } else {
            //   window.location.href = 'http://surabhi.microlan.in/company-master';            
            // }

            location.reload();

        }else{
          $('#loader1').css('display','none');
          $('#submitButton').css('display','block');
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});




//unit master series
$('#update_unit_master_series_form').submit(function(e) {    
  e.preventDefault();
  let formData = new FormData(this);
  $('#image-input-error').text('');

  $.ajax({
     type:'POST',
     url: '/update_series',
      data: formData,
      contentType: false,
      processData: false,
      success: (response) => {
       console.log(response);//return

        if (response.success=='done') {
         $(".print-error-msg").css('display','none');
         console.log(response);
          // this.reset();
          alert('Information updated successfully for selected series');
        //  window.close();
        location.reload();
        }else{
         printErrorMsg(response.error)
        }
      },
      error: function(response){
         console.log(response);
           // $('#image-input-error').text(response.responseJSON.errors.file);
      }
  });

  function printErrorMsg (msg) {
   $(".print-error-msg").find("ul").html('');
   $(".print-error-msg").css('display','block');
   $.each( msg, function( key, value ) {
       $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
   });
 }
});