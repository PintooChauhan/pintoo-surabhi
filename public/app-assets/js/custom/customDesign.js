var a = ['','One ','Two ','Three ','Four ', 'Five ','Six ','Seven ','Eight ','Nine ','Ten ','Eleven ','Twelve ','Thirteen ','Fourteen ','Fifteen ','Sixteen ','Seventeen ','Eighteen ','Nineteen '];
var b = ['', '', 'Twenty','Thirty','Forty','Fifty', 'Sixty','Seventy','Eighty','Ninety'];

function inWords (num) {
    // console.log('num'+num);
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
}


async function convert_min_budget(maxBudSel=null,OwnCoBudSel=null){

    

    var minimum_budget_amt =  $('.minimum_budget_amt').val();

    $('#minimum_budget_amt').val(minimum_budget_amt);
    // console.log(minimum_budget_amt)
    document.getElementById('minimum_budget_amt_words').innerHTML = inWords(minimum_budget_amt);
    // document.getElementById('minimum_budget').value=minimum_budget_amt;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // $('#brokerage_fees').val(ab);

    if(minimum_budget_amt) {
        $.ajax({
            url: '/getMaximumBudget/',
            type: "GET",
            data : {minimum_budget_amt:minimum_budget_amt},
            // dataType: "json",
            success:function(data) {
                //console.log(data); //return;
              if(data){
                $('.maximum_budget_amt').empty();
                $('.maximum_budget_amt').focus;
                $('.maximum_budget_amt').append('<option value=""> Select</option>'); 
                $.each(data, function(key, value){
                $('select[name="maximum_budget_amt"]').append('<option value="'+ value.maximum_budget +'">' + value.maximum_budget_in_short+ '</option>');
                 });

                
                
                 if(typeof maxBudSel == 'undefined' || maxBudSel != null){ 
                    console.log('maxBudSel ' + maxBudSel)
                    $('.maximum_budget_amt').val(maxBudSel).trigger('click');
                    convert_max_budget(OwnCoBudSel);
                 }

                 
            }else{
                $('.maximum_budget_amt').empty();
            }
          }
        });
    }else{
      $('.maximum_budget_amt').empty();
    }
}



function convert_max_budget(OwnCoBudSel=null){
    var minimum_budget_amt = $('.minimum_budget_amt').val();
    var maximum_budget_amt = $('.maximum_budget_amt').val();
    //console.log(minimum_budget_amt);
    //console.log(maximum_budget_amt);
    $('#maximum_budget_amt').val(maximum_budget_amt);
    if( parseInt(minimum_budget_amt) > parseInt(maximum_budget_amt) ){
        $('#maximum_budget_error').html('Maximum budget should be greater than or equal to minimum budget');
        $('#maximum_budget_amt').val('').trigger('change');
    }else{
        $('#maximum_budget_error').html('');
    }

    // 7500000 > 10000000
    // console.log(maximum_budget_amt)
    document.getElementById('maximum_budget_words').innerHTML = inWords(maximum_budget_amt);
    // document.getElementById('maximum_budget').value=maximum_budget_amt;
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    // console.log(maximum_budget_amt);
    if( maximum_budget_amt ) {
        $.ajax({
            url: '/getOwnContribution/',
            type: "GET",
            data : { maximum_budget_amt:maximum_budget_amt },            
            success:function(data) {
               // console.log(data); //return;
              if(data){
                $('#own_contribution_amt').empty();
                $('#own_contribution_amt').focus;
                $('#own_contribution_amt').append('<option value=""> Select</option>'); 
                $.each(data, function(key, value){
                $('select[name="own_contribution_amt"]').append('<option value="'+ value.own_contribution +'">' + value.own_contribution_in_short+ '</option>');
               
                 });
                 if(OwnCoBudSel !='' || typeof OwnCoBudSel == 'undefined' || OwnCoBudSel != null){ 
                    console.log('OwnCoBudSel '+OwnCoBudSel)
                    $('.own_contribution_amt').val(OwnCoBudSel).trigger('change');
                 }
            }else{
                $('#own_contribution_amt').empty();
            }
          }
        });
    }else{
      $('#own_contribution_amt').empty();
    }

}


function convert_own_contribution_amt(){
    var maximum_budget_amt =  $('.maximum_budget_amt').val();
    var own_contribution_amt =  $('.own_contribution_amt').val();
    // alert(own_contribution_amt);
    var minimum_budget_amt =  $('.minimum_budget_amt').val();
    $('#own_contribution_amt').val(own_contribution_amt);

  
    document.getElementById('own_contribution_amount_words').innerHTML = inWords(own_contribution_amt);
    // document.getElementById('own_contribution_amount').value=own_contribution_amt;
    var cal = parseInt(maximum_budget_amt) - parseInt(own_contribution_amt);
    $('#loan_amt').html( cal);
    $('#loan_amount').html( cal);
    $('#loan_amount_min').html( minimum_budget_amt);
    
    // document.getElementById('loan_amount_min_amount_words').innerHTML = inWords(cal);

    // $('#loan_amount_min_amount_words').html(inWords(minimum_budget_amt) );
    $('#loan_amount_max').html( maximum_budget_amt);
    $('#loan_amount_own').html( own_contribution_amt);
    
    
    if(isNaN(cal) == false){
        document.getElementById('loan_amt_in_words').innerHTML = inWords(cal);
    }
    // document.getElementById('loan_amt_in_words1').innerHTML = inWords(cal);
    


    if( parseInt(maximum_budget_amt) == parseInt(own_contribution_amt) ){
        $('#home_loan_li').css('pointer-events','none');
        $("#col_home_loan").css('background-color','#f3c5c5'); 
    }else{
        $('#home_loan_li').css('pointer-events','');
        $("#col_home_loan").css('background-color',''); 
    }
    return;
    var minimum_budget = $('#minimum_budget').val();    

    var res = minimum_budget;

    if(minimum_budget !=''){
        if(res==own_contribution_amt){
            $('#home_loan_li').css('pointer-events','none');
            $("#col_home_loan").css('background-color','#f3c5c5'); 
        }else{
            $('#home_loan_li').css('pointer-events','');
            $("#col_home_loan").css('background-color','white'); 
        }
    }
  


}



function convert_min_pack_budget(){
    var ab =  document.getElementById('minimumm_budget_pack_amt').value;
   document.getElementById('minimum_budget_pack_words').innerHTML = inWords(ab);
   document.getElementById('minimumm_package_budget').value=ab;
}


// function numFormatter(num) {
//     if(num > 999 && num < 100000){
//         return (num/1000).toFixed(0) + 'K'; // convert to K for number from > 1000 < 1 million 
//     }else if(num >= 100000 && num < 1000000){
//         return (num/100000).toFixed(0) + 'L'; // convert to M for number from > 1 million 
//     }else if(num >= 1000000){
//         return (num/1000000).toFixed(0) + 'Cr'; // convert to M for number from > 1 million 
//     }else if(num < 900){
//         return num; // if value < 1000, nothing to do
//     }
// }
function nFormatter(num, digits) {
    var si = [
      { value: 1E18, symbol: "E" },
      { value: 1E15, symbol: "P" },
      { value: 1E12, symbol: "T" },
      { value: 1E9,  symbol: "G" },
      { value: 1E7,  symbol: "Cr" },
      { value: 1E5,  symbol: "L" },
      { value: 1E3,  symbol: "k" }
    ], i;
    for (i = 0; i < si.length; i++) {
      if (num >= si[i].value) {
        return (num / si[i].value).toFixed(digits).replace(/\.?0+$/, "") + si[i].symbol;
      }
    }
    return num;
  }


// document.getElementById('own_contribution_amount').focusout = function () {
//     var ab =  $('#own_contribution_amount').val();
//     var minimum_budget = $('#minimum_budget').val();   
//     var minimum_budget1 = minimum_budget;
//     var ab1 = ab;

//     if(minimum_budget1 !=''){
//         if(minimum_budget1==ab1){
//             $('#home_loan_li').css('pointer-events','none');
//             $("#col_home_loan").css('background-color','#f3c5c5'); 
//         }else{
//             $('#home_loan_li').css('pointer-events','');
//             $("#col_home_loan").css('background-color','white'); 
//         }
//     }

// }

function convert_max_pack_budget(){
    
   var ab =  document.getElementById('maximumm_pack_budget_amt').value;
   document.getElementById('maximumm_package_budget_words').innerHTML = inWords(ab);
   document.getElementById('maximumm_package_budget').value=ab;

}

function convert_rent_amt(){
    var ab =  document.getElementById('rent_amt').value;
    document.getElementById('rent_amt_words').innerHTML = inWords(ab);
    document.getElementById('rent_amount').value=ab;
}


// document.getElementById('minimum_budget').focusout = function () {
//     var min1 = document.getElementById('minimum_budget').value;
//     var min = min1;

//     for (i = 0; i < document.getElementById("minimum_budget_amt").length; ++i){
//         if (document.getElementById("minimum_budget_amt").options[i].value == min){
//              document.getElementById("minimum_budget_amt").options[i].selected =  true;           

//             document.getElementById('minimum_budget_amt').getElementsByTagName('option')[i].selected = true;

//             return;   
            
//         }
        
//     }

//     var option = document.createElement("option");
//     option.text = nFormatter(min,2);
//     option.value = min;
//     option.selected = true;
//     var select = document.getElementById("minimum_budget_amt");
//     select.appendChild(option);   
    
//     console.log(nFormatter(min,2));
// }

document.getElementById('expected_price1').onkeyup = function () {       
    var str = document.getElementById('expected_price1').value;
    var res = str;
    document.getElementById('expected_price_words').innerHTML = inWords(res);    
    // $('#brokerage_fees').val(document.getElementById('minimum_budget').value);
};

document.getElementById('final_price').onkeyup = function () {       
    var str = document.getElementById('final_price').value;
    var res = str;
    document.getElementById('final_price_words').innerHTML = inWords(res);    
    // $('#brokerage_fees').val(document.getElementById('minimum_budget').value);
};


document.getElementById('expected_package_amount').onkeyup = function () {       
    var str = document.getElementById('expected_package_amount').value;
    var res = str;
    document.getElementById('expected_package_amount_words').innerHTML = inWords(res);    
    // $('#brokerage_fees').val(document.getElementById('minimum_budget').value);
};

document.getElementById('final_package_amount').onkeyup = function () {       
    var str = document.getElementById('final_package_amount').value;
    var res = str;
    document.getElementById('final_package_amount_words').innerHTML = inWords(res);    
    // $('#brokerage_fees').val(document.getElementById('minimum_budget').value);
};

document.getElementById('rent_escalation').onkeyup = function () {       
    var str = document.getElementById('rent_escalation').value;
    var res = str;
    document.getElementById('rent_escalation_words').innerHTML = inWords(res);    
    // $('#brokerage_fees').val(document.getElementById('minimum_budget').value);
};

document.getElementById('minimum_budget').onkeyup = function () {
    var str = document.getElementById('minimum_budget').value;
    var res = str;
    document.getElementById('minimum_budget_amt_words').innerHTML = inWords(res);
}
// document.getElementById('minimumm_package_budget').onkeyup = function () {
//     document.getElementById('minimum_budget_pack_words').innerHTML = inWords(document.getElementById('minimumm_package_budget').value);
// };

document.getElementById('maximum_budget').onkeyup = function () {
    var str = document.getElementById('maximum_budget').value;
    var res = str;
    document.getElementById('maximum_budget_words').innerHTML = inWords(res);

    // var cal = (res * 2)/100;
    // $('#brokerage_fees').val(cal);
    // document.getElementById('brokerage_fees_words').innerHTML = inWords(cal);
    // $('#brokerage_percent').val("2");
    // $('#brokerage_percent').trigger('change');
};

document.getElementById('own_contribution_amount').onkeyup = function () {
    var str = document.getElementById('own_contribution_amount').value;
    var res = str;
    document.getElementById('own_contribution_amount_words').innerHTML = inWords(res);
};

// document.getElementById('fetch_maximum_loan_amount_words').innerHTML = inWords(ab);

document.getElementById('fetch_maximum_loan_amount').onkeyup = function () {
    var str = document.getElementById('fetch_maximum_loan_amount').value;
    var res = str;
    document.getElementById('fetch_maximum_loan_amount_words').innerHTML = inWords(res);
};

document.getElementById('brokerage_fees').onkeyup = function () {
    var str = document.getElementById('brokerage_fees').value;
    var res = str;
    document.getElementById('brokerage_fees_words').innerHTML = inWords(res);
    
    // var brokerage_percent = $('#brokerage_percent').val();
    // var cal = (res * 100)/brokerage_percent;
    // //alert(cal);
    // $('#maximum_budget').val("₹ "+cal);
    //  document.getElementById('maximum_budget_words').innerHTML = inWords(cal);

};






document.getElementById('rent_amount').onkeyup = function () {
    var str = document.getElementById('rent_amount').value;
    var res = str;
    
    document.getElementById('rent_amt_words').innerHTML = inWords(res);
};


function apply_css(skip,attr, val=''){   
    var id =returnColArray();           
    id.forEach(function(entry) {
        
        if(entry==skip){
            // alert(11)
            $('#'+skip).css(attr,'orange','!important');
            // $('#'+skip).attr('style', 'background-color: orange !important');
        }else{            
            if($('#'+entry).css('pointer-events') == 'none'){

            }else{
                $('#'+entry).css(attr,val);
            }
            
            action_to_hide();            
            
        }        
    });
}

function hide_n_show(skip){
    var id = collapsible_body_ids();
    collapsible_body_ids().forEach(function(entry) {
        // console.log(id);
        if(entry==skip){
            // alert(skip);
            var x = document.getElementById(skip);  
            $('#'+skip).css('background-color','rgb(234 233 230)');
            // alert(x)          
            if (x.style.display === "none") {
                // alert(1)
                
                x.style.display = "block";
            } else {
                // alert(2)
                // alert(skip);
                // $('#col_lead_info').removeAttr("style");
                // $('#col_lead_info').css('background-color','white',"!important");
                x.style.display = "none";
                $('#col_'+skip).css('background-color','white');
                // alert(x.style.backgroundColor);
                // alert(skip);
                // $('#col_'+skip).css('background-color','white');
                // $('#col_lead_info').css('background-color','white',"!important");
                // x.style.backgroundColor = "lightblue";
            }
        }else{          
            // alert(entry)
            $('#'+entry).hide();
        }
        
    });
}

function returnColArray(){
    var a = Array('col_lead_info','col_customer_name','col_category','col_location1','col_unit','col_budget_loan','col_purpose','col_home_loan','col_looking_since','col_property_seen','col_possession_year','col_unit_amenities','col_society_amenities','col_society_amenities_unit','col_parking','col_floor_choice','col_building_age','col_specific_choice','col_finalization','col_address','col_members','col_nature_of_business','col_brokerage_fee','col_challenges','col_lead_analysis','col_key_member','col_building_name_and_structure','col_unit_info','col_unit_status1','col_unit_ame','col_parking_seller','col_price_seller','col_finalization_seller','col_f2f_address_seller','col_washroom_and_client','col_marketing','col_images_and_video','col_brokerage_fee_seller','col_challenges_seller','col_lead_analysis_seller','col_key_membe_sellerr','col_license_period','col_tanant_and_shifing_charges','col_shifting_date','col_tenant_type_and_details','col_notice_period','col_expected_closure','col_pros_cons');
    return a;
}

function collapsible_body_ids(){
    var b = Array('lead_info','customer_name','category','location1','unit','budget_loan','purpose','home_loan','looking_since','property_seen','possession_year','unit_amenities','society_amenities','society_amenities_unit','parking','floor_choice','building_age','specific_choice','finalization','address','members','nature_of_business','brokerage_fee','challenges','lead_analysis','key_member','building_name_and_structure','unit_info','unit_status1','unit_ame','parking_seller','price_seller','finalization_seller','f2f_address_seller','washroom_and_client','marketing','images_and_video','brokerage_fee_seller','challenges_seller','lead_analysis_seller','key_member_seller','license_period','tanant_and_shifing_charges','shifting_date','tenant_type_and_details','notice_period','expected_closure','pros_cons');
    return b;
}

function lead_info(){       
    apply_css('col_lead_info','background-color',''); 
    hide_n_show("lead_info");          
    

}
function customer_name(){
    apply_css('col_customer_name','background-color',''); 
    hide_n_show("customer_name");    
}       

function category(){    
    apply_css('col_category','background-color','');   
    hide_n_show("category");    
}

function location1(){         
    apply_css('col_location1','background-color','');   
    hide_n_show("location1");    
}

function unit(){        
    apply_css('col_unit','background-color','');          
    hide_n_show("unit");

    //ConfigSel  configuration
   

  
}

function budget_loan(){    
    apply_css('col_budget_loan','background-color','');
    hide_n_show("budget_loan");

    var minBudSel = $('#minBudSel').val();
    var maxBudSel = $('#maxBudSel').val();
    var OwnCoBudSel = $('#OwnCoBudSel').val();

    // alert();
// console.log(minBudSel);
    if(typeof minBudSel != 'undefined' ) {
    if(minBudSel !='' || typeof minBudSel != 'undefined' || minBudSel != null){
        var delayInMilliseconds = 1000;    
        setTimeout(function() {
            $('.minimum_budget_amt').val(minBudSel).trigger('change');
            convert_min_budget(maxBudSel,OwnCoBudSel)
          }, delayInMilliseconds);       
        
        // convert_max_budget(OwnCoBudSel);
        }
    
    }

    

   
    

}

function purpose(){         
    apply_css('col_purpose','background-color','');
    hide_n_show("purpose");
}

function home_loan(){    
    var lbl = document.getElementById("loan_amt"); 
    lbl.setAttribute("class", "active");
   // var maximum_loan_amount =  document.getElementById('maximum_loan_amount').value;
    // alert(maximum_loan_amount);
    //document.getElementById('fetch_maximum_loan_amount').value = maximum_loan_amount;
    // document.getElementById('fetch_maximum_loan_amount_words').innerHTML = inWords(document.getElementById('maximum_loan_amount').value);
    
    apply_css('col_home_loan','background-color','');
    hide_n_show("home_loan");    
    // var maximum_budget_amt =  $('.maximum_budget_amt').val();
    // var own_contribution_amt =  $('.own_contribution_amt').val();
    // //
    var minimum_budget_amt =  $('#minimum_budget_amt').val();
    document.getElementById('loan_amount_min_amount_words').innerHTML = inWords(minimum_budget_amt);

    var maximum_budget_amt =  $('#maximum_budget_amt').val();
    document.getElementById('loan_amount_max_amount_words').innerHTML = inWords(maximum_budget_amt);

    var own_contribution_amt =  $('#own_contribution_amt').val();
    document.getElementById('loan_amount_own_amount_words').innerHTML = inWords(own_contribution_amt);

    var cal = parseInt(maximum_budget_amt) - parseInt(own_contribution_amt)
    document.getElementById('loan_amount_total_amount_words').innerHTML = inWords(cal);
    // $('#own_contribution_amt').val(own_contribution_amt);

  
    // document.getElementById('own_contribution_amount_words').innerHTML = inWords(own_contribution_amt);
    // // document.getElementById('own_contribution_amount').value=own_contribution_amt;
    // var cal = parseInt(maximum_budget_amt) - parseInt(own_contribution_amt);
   
    
   
    

    document.getElementById('loan_amt_in_words').innerHTML = inWords(cal);
    
}



function change_loan_status_window(){
    var ab =  document.getElementById('loan_status').value;
    console.log(ab);
    var x = document.getElementById('loan_status_sanctioned');
    var x1 = document.getElementById('loan_status_sanctioned1');
    var y = document.getElementById('loan_status_need_assist');
    if(ab == '0'){
        x.style.display = "none";
        x1.style.display = "none";
        y.style.display = "none";

      
    }else if(ab=='sanctioned' || ab=='in_process'){      

        var selected = [];
          for (var option of document.getElementById('preferred_bank').options) {
            if (option.selected) {
              selected.push(option.value);
            }
          }
        $('#bank_name').val(selected);
        $('#bank_name').trigger('change');

        x.style.display = "block";
        x1.style.display = "block";
        y.style.display = "none";
    }else if(ab=='need_assiatance'){
        y.style.display = "block";
        x.style.display = "none";
        x1.style.display = "none";
    }  

}


function looking_since(){      
    apply_css('col_looking_since','background-color','');
    hide_n_show('looking_since');
    // $(window).scrollTop($('#matching_properties').position().top; 
}


function property_seen(){      
    apply_css('col_property_seen','background-color','');
    hide_n_show('property_seen');

}


function property_seen_details_hide_show(){
    var ab =  document.getElementById('visited_by').value;
    if(ab=='resale_pc'){
        $('#pc_info').css('display','');
        $('#calender_span').css('display','');
        $('#seller_info').css('display','none');
        $('#tat_within').css('display','none');
        $('#tat_after').css('display','none');
    }else if(ab=="resale_direct"){
        $('#pc_info').css('display','none');
        $('#calender_span').css('display','');
        $('#seller_info').css('display','');
        $('#tat_within').css('display','none');
        $('#tat_after').css('display','none');
    }else if(ab=='uc_pc'){
        $('#pc_info').css('display','');
        $('#tat1').css('display','');
        $('#tat2').css('display','');
        $('#calender_span').css('display','none');
        $('#seller_info').css('display','none');
        $('#tat_within').css('display','');
        $('#tat_after').css('display','');
    }else if(ab='uc_rdirect'){
        $('#pc_info').css('display','none');
        $('#tat1').css('display','none');
        $('#tat2').css('display','none');
        $('#calender_span').css('display','');
        $('#seller_info').css('display','none');
        $('#tat_within').css('display','none');
        $('#tat_after').css('display','none');
    }
}

function property_seen_hide_show(){
    var ab =  document.getElementById('property_seen1').value;
    if(ab=='yes'){
        $('#p1').css('display','block');
        $('#p2').css('display','block');
        $('#p3').css('display','block');
        $('#p4').css('display','block');
        $('#p5').css('display','block');
        $('#p6').css('display','block');
        $('#p7').css('display','block');
     
    }else{
        $('#p1').css('display','none');
        $('#p2').css('display','none');
        $('#p3').css('display','none');
        $('#p4').css('display','none');
        $('#p5').css('display','none');
        $('#p6').css('display','none');
        $('#p7').css('display','none');
    }

}

function possession_year(){     
    apply_css('col_possession_year','background-color','');
    hide_n_show("possession_year");     

}

function unit_amenities(){    
    apply_css('col_unit_amenities','background-color','');
    hide_n_show("unit_amenities");     

}

function society_amenities(){    
    apply_css('col_society_amenities','background-color','');
    hide_n_show("society_amenities");  
}

function society_amenities_unit(){    
    apply_css('col_society_amenities_unit','background-color','');
    hide_n_show("society_amenities_unit");  
}

function parking(){    
    apply_css('col_parking','background-color','');
    hide_n_show("parking");     
}

function floor_choice(){    
    apply_css('col_floor_choice','background-color','');
    hide_n_show("floor_choice");     
}

function building_age(){    
    apply_css('col_building_age','background-color','');
    $('#col_building_age').css('background-color','orange','!important');
    hide_n_show("building_age");     
    
}


function specific_choice(){       
    apply_css('col_specific_choice','background-color','');
    hide_n_show("specific_choice");  
}

function finalization(){
    apply_css('col_finalization','background-color','');
    hide_n_show("finalization");     
}

function address(){    
    apply_css('col_address','background-color','');
    hide_n_show("address");     
}




function show_rent_amt(){
    var ab =  $('#current_residence_status').val();
    $('#crs').html("");
    $('#crs').html( ab.toUpperCase());
    $('#crs1').html("");
    $('#crs1').html( ab.toUpperCase());
    // alert(ab);
    if(ab == ''){
        $('.rntamt').css('display','none');
    }else if(ab=='rented'){
        $('.rntamt').css('display','');
    }else if(ab=='owned'){
        $('.rntamt').css('display','none');
    }
    
}

function members(){    
    apply_css('col_members','background-color','');
    hide_n_show("members");     
}


function nature_of_business(){    
    apply_css('col_nature_of_business','background-color','');
    hide_n_show("nature_of_business");     
}

function brokerage_fee(){    

    // var ab =  document.getElementById('maximum_budget_amt').value;
    // var cal = (ab * 2)/100;
    // $('#brokerage_fees').val(cal);
    //     document.getElementById('brokerage_fees_words').innerHTML = inWords(cal);
    
    // // $('#brokerage_percent').select2("val", '2');

    // // var x = calculate_brokerage();
    // // if(x==0){
    // //     $('#brokerage_percent').val("2");
    // //     $('#brokerage_percent').trigger('change');
    // // }else{
    // //     alert(x)
    // // }
    
    
    apply_css('col_brokerage_fee','background-color','');
    $('#col_brokerage_fee').css('background-color','orange','!important');
    hide_n_show("brokerage_fee");
}

function calculate_brokerage(){
    var brokerage_percent = $('#brokerage_percent').val();

        var brokerage_fees = document.getElementById('brokerage_fees').value;

            var res = brokerage_fees;

            var cal = (res * brokerage_percent)/100;
            $('#brokerage_fees').val(cal);

            var str = document.getElementById('brokerage_fees').value;
            var res = str;
            document.getElementById('brokerage_fees_words').innerHTML = inWords(res);
     

    // alert(brokerage_percent);

    
    

}

function expected_closure(){    
    apply_css('col_expected_closure','background-color','');
    hide_n_show("expected_closure");     
}

function pros_cons(){    
    apply_css('col_pros_cons','background-color','');
    hide_n_show("pros_cons");     
}





function challenges(){    
    apply_css('col_challenges','background-color','');
    hide_n_show("challenges");     
}

function lead_analysis(){       
    apply_css('col_lead_analysis','background-color','');
    hide_n_show("lead_analysis");  
    var shifing_date1 = $('#shifing_date1').val();
    $('#sht1').html(shifing_date1);
// alert(finalization_month_ar());
    $('#fip1').html(finalization_month_ar());
    
}

function key_member(){    
    apply_css('col_key_member','background-color','');
    hide_n_show("key_member");     
}

function budget_range(){
    
    

    $('#col_matching_properties').css('background-color','white');     

    var minimum_budget = $('#minimum_budget').val();
    var maximum_budget = $('#maximum_budget').val();
    var own_contribution_amount =  $('#own_contribution_amount').val();

    if(minimum_budget !='' && maximum_budget !='' && own_contribution_amount !=''){
         $('#append_budget_range_table').empty();
        $('#col_budget_range').css('background-color','lightblue');
        $.ajax(  {
            url:"/calculate_budget_break_up",
            type:"GET",
            data:                     
                {minimum_budget:minimum_budget,maximum_budget:maximum_budget,own_contribution_amount:own_contribution_amount} ,
            
            success: function(data) {
                // alert(url);
               // console.log(data);
                $('#append_budget_range_table').css('display','');                
                $('#append_budget_range_table').append(data);
            }
        });
    }else{
        $('#append_budget_range_table').remove();
        $('#col_budget_range').css('background-color','');
        alert('Min. Budget or Max. Budget or OCA is empty');
    }
}

function matching_properties(){

    var x = document.getElementById("matching_properties_table");   

    if (x.style.display === "none") {
        x.style.display = "block";
        $('#append_budget_range_table').css('display','none');
        $('#col_matching_properties').css('background-color','lightblue');
        $('#col_budget_range').css('background-color','');
        $('#matching_properties_table').css('display','');
        $('#budget_range_table').css('display','none');  
    } else {
        x.style.display = "none";        
        $('#col_matching_properties').css('background-color','white');        
    }

    
    
}

function action_to_hide(){
    var x = $('#under_constrction').val();   
    if(x=='resale' || x=='rtmi'){
        $("#possession_year").prop('disabled', true); 
        $("#payment_plan").prop('disabled', true); 
        // $('#posession_yer').css('display','none');
        // $("#payment_pln").css('display','none'); 
        // $('#nature_of_business_li').css('pointer-events','none');
        // $("#col_nature_of_business").css('background-color','#f3c5c5'); 
        // $('#possession_year_li').css('pointer-events','');
        // $("#col_possession_year").css('background-color',''); 
        // $("#no_of_tw_parking_show").css('display','none'); 
        $('#building_age_li').css('pointer-events','');
        $("#col_building_age").css('background-color','white'); 
        $('#brokerage_fee_li').css('pointer-events','');
        $("#col_brokerage_fee").css('background-color','white');         
    }else if(x=='under_construction'){
        $('#building_age_li').css('pointer-events','none');
        $("#col_building_age").css('background-color','#f3c5c5'); 

        $('#brokerage_fee_li').css('pointer-events','none');
        $("#col_brokerage_fee").css('background-color','#f3c5c5'); 

        $("#possession_year").prop('disabled', false); 
        $("#payment_plan").prop('disabled', false); 
        // $('#posession_yer').css('display','');
        // $("#payment_pln").css('display',''); 

        
        
        // $("#no_of_tw_parking_show").css('display',''); 
        $('#possession_year_li').css('pointer-events','');
        $("#col_possession_year").css('background-color',''); 
        
    }
}




function hideShowCampaign(){
    var x = $('#lead_source_name').val();
    if(x==1 || x==2 || x==3 || x==4 || x==5 || x==6 || x==7 ){
        $("#lead_campaign_name").prop('disabled', false);
        
    }else{
        $("#lead_campaign_name").val('').trigger('change');
        $("#lead_campaign_name").prop('disabled', true);
    }
}





function copyMobileNoUnit(){
    var checkBox = document.getElementById("copywhatsapp1");
    var mobile_number = $('#mobile_number1').val();
    // alert(mobile_number)
    if (checkBox.checked == true){
        if(mobile_number != ''){
            $('#wap_number1').val(mobile_number);   
        }else{
            alert("Enter mobile number first");
            document.getElementById("copywhatsapp1").checked = false;
        }        
      } else {
        $('#wap_number').val('');
      }
}




function building_name_and_structure(){       
    apply_css('col_building_name_and_structure','background-color',''); 
    hide_n_show("building_name_and_structure");          
}

function unit_info(){       
    apply_css('col_unit_info','background-color',''); 
    hide_n_show("unit_info");          
}

function unit_status1(){      
    // alert() ;
    apply_css('col_unit_status1','background-color',''); 
    hide_n_show("unit_status1");          
}

function unit_inspection_status_hide_show(){
    var ab =  document.getElementById('unit_inspection_status').value;
    if( ab=='vacant' ){
        $('#if_cancant').css('display','');
        $('#if_self_occupied').css('display','none');
        $('#if_tenant_occupied').css('display','none');
    }else if( ab=='self_occupied' ){
        $('#if_cancant').css('display','none');
        $('#if_self_occupied').css('display','');
        $('#if_tenant_occupied').css('display','none');   
    }else if( ab=='tenant_occupied' ){
        $('#if_cancant').css('display','none');
        $('#if_self_occupied').css('display','none');
        $('#if_tenant_occupied').css('display','');
    }
}

function unit_ame(){
    apply_css('col_unit_ame','background-color',''); 
    hide_n_show("unit_ame");   
}



function price_seller(){
    apply_css('col_price_seller','background-color',''); 
    hide_n_show("price_seller");   

    var minRentAmt = $('#minRentAmt').val();
    var maxRentAmt = $('#maxRentAmt').val();
    
    var minDepositAmt = $('#minDepositAmt').val();
    var maxDepositAmt = $('#maxDepositAmt').val();

    
    if(minRentAmt !='' || typeof minRentAmt == 'undefined' || minRentAmt != null){
        document.getElementById('minumum_rent_words').innerHTML = inWords(minRentAmt);
        var delayInMilliseconds = 1000;    
        setTimeout(function() {
            $('.minumum_rent').val(minRentAmt).trigger('change');
            getMaxRent(maxRentAmt);
          }, delayInMilliseconds);    
    }

    if(minDepositAmt !='' || typeof minDepositAmt == 'undefined' || minDepositAmt != null){
        document.getElementById('minimum_deposit_words').innerHTML = inWords(minDepositAmt);
        var delayInMilliseconds = 1000;    
        setTimeout(function() {
            $('.minimum_deposit').val(minDepositAmt).trigger('change');
            getMaxDeposit(maxDepositAmt);
          }, delayInMilliseconds);    
    }


}

function convert_expected_price(){
    var ab =  document.getElementById('expected_price_amt').value;      
    // alert(ab)
    document.getElementById('expected_price_words').innerHTML = inWords(ab);
    // document.getElementById('expected_price1').value=ab;
    // $('#brokerage_fees').val(ab);
}

function convert_final_price(){
    var ab =  document.getElementById('final_price_amt1').value;     
    // alert(ab) 
    document.getElementById('final_price_words').innerHTML = inWords(ab);
    // document.getElementById('final_price').value=ab;
    // $('#brokerage_fees').val(ab);
}

function convert_expected_package_amt(){
    // alert();
    var ab =  document.getElementById('expected_package_amt').value;      
    document.getElementById('expected_package_amount_words').innerHTML = inWords(ab);
    document.getElementById('expected_package_amount').value=ab;
    // $('#brokerage_fees').val(ab);
}

function convert_final_package_amt(){
    var ab =  $('.maximum_deposit').val();
    document.getElementById('maximum_deposit_words').innerHTML = inWords(ab);
    // document.getElementById('final_package_amount').value=ab;
    $('#maximum_deposit').val(ab);
    // getMaxDeposit();
}

function convert_maximum_rent(){
    var ab =  $('.maximum_rent').val();   
    // alert(ab)        
    document.getElementById('maximum_rent_words').innerHTML = inWords(ab);
    $('#maximum_rent').val(ab)
}

function finalization_seller(){
    apply_css('col_finalization_seller','background-color',''); 
    hide_n_show("finalization_seller");   
}

function f2f_address_seller(){
    apply_css('col_f2f_address_seller','background-color',''); 
    hide_n_show("f2f_address_seller");   
}
function washroom_and_client(){
    apply_css('col_washroom_and_client','background-color',''); 
    hide_n_show("washroom_and_client");   
}

function marketing(){
    apply_css('col_marketing','background-color',''); 
    hide_n_show("marketing");   
}


function images_and_video(){
    apply_css('col_images_and_video','background-color',''); 
    hide_n_show("images_and_video");   
}

function brokerage_fee_seller(){
    apply_css('col_brokerage_fee_seller','background-color',''); 
    hide_n_show("brokerage_fee_seller");   
}

function challenges_seller(){
    apply_css('col_challenges_seller','background-color',''); 
    hide_n_show("challenges_seller");   
}

function lead_analysis_seller(){
    apply_css('col_lead_analysis_seller','background-color',''); 
    hide_n_show("lead_analysis_seller");   
}

function key_member_seller(){
    apply_css('col_key_member_seller','background-color',''); 
    hide_n_show("key_member_seller");   
}



// owner

function hide_show_lead2(){
    var x = $('#leadby').val();   
    console.log(x);
    // alert(x);leadty1pe5
    if(x==2){
        $("#pc_company_name").prop('disabled', true);
        $("#pc_excecutive_name").prop('disabled', true); 
        $('#add_pc_name').css('pointer-events','none');
        $('#add_pc_exce_name').css('pointer-events','none'); 
        $("#leadty1pe5").prop('disabled', false);       
    }else if(x==3){
        $("#leadty1pe5").prop('disabled', true);
        $("#pc_company_name").prop('disabled', false);
        $("#pc_excecutive_name").prop('disabled', false);
        $('#add_pc_name').css('pointer-events','');
        $('#add_pc_exce_name').css('pointer-events','');
    }else{
        $("#leadty1pe5").prop('disabled', false);       
        $("#pc_company_name").prop('disabled', false);
        $("#pc_excecutive_name").prop('disabled', false);
        $('#add_pc_name').css('pointer-events','');
        $('#add_pc_exce_name').css('pointer-events','');
    }
    }


function license_period(){
    apply_css('col_license_period','background-color',''); 
    hide_n_show("license_period");   
}

function tanant_and_shifing_charges(){
    apply_css('col_tanant_and_shifing_charges','background-color',''); 
    hide_n_show("tanant_and_shifing_charges");   
}

function shifting_date(){
    apply_css('col_shifting_date','background-color',''); 
    hide_n_show("shifting_date");   
}

function tenant_type_and_details(){
    apply_css('col_tenant_type_and_details','background-color',''); 
    hide_n_show("tenant_type_and_details");   
}

function hideShowTenantType(){
    var x = $('#tenant_type').val();   
    if(x=='bachelor'){
        $('#no_of_members_div').css('display','');
        $('#family_size_div').css('display','none');
        $('#working_members_div').css('display','none');
    }else{
        $('#no_of_members_div').css('display','none');
        $('#family_size_div').css('display','');
        $('#working_members_div').css('display','');
    }
}

function notice_period(){
    apply_css('col_notice_period','background-color',''); 
    hide_n_show("notice_period");   
}

function hideShowNoticePeriod(){
    var x = $('#notice_period_served').val(); 
    // alert(x);

    if(x=='no'){
        $('#notice_period_reason_div').css('display','');
        $('#notice_period_date_div').css('display','none');
       
    }else if(x=='yes'){
        $('#notice_period_reason_div').css('display','none');
        $('#notice_period_date_div').css('display','');        
    }else{
        
        $('#notice_period_date_div').css('display','none');
        $('#notice_period_reason_div').css('display','none');
    }
}


function society_floor_rice(){

    var x = document.getElementById("society_floor_rice_table");   
    // alert('floor '+x.style.display );

    if (x.style.display === "none") {
        x.style.display = "block";
        $('#society_brokarage_fee_table').css('display','none');
        $('#col_society_brokarage_fee').css('background-color','white');
       
        $('#col_society_floor_rice').css('background-color','lightblue');
      
        $('#society_floor_rice_table').css('display','');
      
    } else {
        x.style.display = "none";        
        $('#col_society_floor_rice').css('background-color','white');        
    }
    
}

function society_brokarage_fee(){

    var x = document.getElementById("society_brokarage_fee_table");   
    // alert('brake '+x.style.display );

    if (x.style.display === "none") {
        x.style.display = "block";
        $('#society_floor_rice_table').css('display','none');
        $('#col_society_floor_rice').css('background-color','white');
        $('#col_society_brokarage_fee').css('background-color','lightblue');
      
        $('#society_brokarage_fee_table').css('display','');
      
    } else {
        x.style.display = "none";        
        $('#col_society_brokarage_fee').css('background-color','white');        
    }
    
}





function copyMobileNoUnit3(){
    var checkBox = document.getElementById("copywhatsapp3");
    var mobile_number = $('#mobile_number3').val();
    // alert(mobile_number)
    if (checkBox.checked == true){
        if(mobile_number != ''){
            $('#wap_number3').val(mobile_number);   
        }else{
            alert("Enter mobile number first");
            document.getElementById("copywhatsapp3").checked = false;
        }        
      } else {
        $('#wap_number3').val('');
      }
}

function copyMobileNoUnit4(){
    var checkBox = document.getElementById("copywhatsapp4");
    var mobile_number = $('#mobile_number4').val();
    // alert(mobile_number)
    if (checkBox.checked == true){
        if(mobile_number != ''){
            $('#wap_number4').val(mobile_number);   
        }else{
            alert("Enter mobile number first");
            document.getElementById("copywhatsapp4").checked = false;
        }        
      } else {
        $('#wap_number4').val('');
      }
}

function removeDivSecMo(){
   $('#secondaryMobile').remove();
   $('.plusButton1').css('display','');
}

function removeDivSecWaMo(){
    $('.secondaryWaMobile').remove();
    $('.plusButton2').css('display','');
 }

 function removeDivSecEmail(){
    $('.secondaryEmail').remove();
    $('.plusButton3').css('display','');
 }


 function onlyNumberKey(evt) {
          
    // Only ASCII character in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}