



function execute(url,form,err){
    var url = url;
    var form = form;
    var err = err;

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(url);
            console.log(data); //return;
            if($.isEmptyObject(data.error)){
                alert(data.success);
                location.reload();
            }else{
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
}



function pc_company_name_btn(){    
    var url = 'addPcCompanyName';
    var form = 'pc_company_name_form';
    var err = 'print-error-msg_pc_company_name';


    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(url);
            // console.log(data); return;
            if($.isEmptyObject(data.error)){
                alert(data.success);
                var a = data.a;
                var option = a.option;
                var value = a.value; 
    
                var newOption= new Option(option,value, true, false);
                // Append it to the select
                $("#pc_company_name").append(newOption);
                $('#pc_company_name_form')[0].reset();
                $('#modal1').modal('close');
            }else{
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }

    // execute(url,form,err);
}

function pc_excecutive_name_btn(){    
    var url = 'addPcExcecutiveName';
    var form = 'pc_executive_name_form';
    var err = 'print-error-msg_pc_executive_name';
    execute(url,form,err);
}

function add_unit_information_btn(){    
    var url = 'addUnitInformation';
    var form = 'add_unit_information';
    var err = 'print-error-msg_add_unit_information';
    execute(url,form,err);
}


// function add_unit_amenity(){
//     var url = 'addUnitAmenity';
//     var form = 'add_unit_amenity_form';
//     var err = 'print-error-msg_add_unit_amenity';
//     execute(url,form,err);
// }

function add_building_amenity(){
    var url = 'addBuildingAmenity';
    var form = 'add_building_amenity';
    var err = 'print-error-msg_add_building_amenity';
    execute(url,form,err);
}



function add_location_btn(){
    var url = 'addLocation';
    var form = 'add_location_form';
    var err = 'print-error-msg_add_location';
    execute(url,form,err);
}


function add_building_name_form(){
    var url = 'addBuildingName';
    var form = 'add_building_name_form';
    var err = 'print-error-msg_add_building_name';
    execute(url,form,err);
}


function add_wing_name_form(){
    var url = 'addWingName';
    var form = 'add_wing_name_form';
    var err = 'print-error-msg_add_wing_name';
    execute(url,form,err);
}

function add_suffix_form(){
    var url = 'addSuffixName';
    var form = 'add_suffix_form';
    var err = 'print-error-msg_add_suffix';
    execute(url,form,err);
}

function add_unit_info(){
    var url = 'add_unit_info';
    var form = 'add_unit_info_form';
    var err = 'print-error-msg_add_unit_info';
    execute(url,form,err);
}
