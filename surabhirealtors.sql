-- MySQL dump 10.14  Distrib 5.5.68-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: surabhirealtors
-- ------------------------------------------------------
-- Server version	5.5.68-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dd_area_range`
--

DROP TABLE IF EXISTS `dd_area_range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_area_range` (
  `area_range_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_range_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`area_range_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_area_range`
--

LOCK TABLES `dd_area_range` WRITE;
/*!40000 ALTER TABLE `dd_area_range` DISABLE KEYS */;
/*!40000 ALTER TABLE `dd_area_range` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_building_owner`
--

DROP TABLE IF EXISTS `dd_building_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_building_owner` (
  `dd_building_owner_id` int(11) NOT NULL AUTO_INCREMENT,
  `building_owner` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dd_building_owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_building_owner`
--

LOCK TABLES `dd_building_owner` WRITE;
/*!40000 ALTER TABLE `dd_building_owner` DISABLE KEYS */;
INSERT INTO `dd_building_owner` VALUES (1,'builder','2021-12-21 15:15:36'),(2,'society','2021-12-21 15:15:46'),(3,'builder & society','2021-12-21 15:15:36');
/*!40000 ALTER TABLE `dd_building_owner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_building_type`
--

DROP TABLE IF EXISTS `dd_building_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_building_type` (
  `building_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `building_type_name` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`building_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_building_type`
--

LOCK TABLES `dd_building_type` WRITE;
/*!40000 ALTER TABLE `dd_building_type` DISABLE KEYS */;
INSERT INTO `dd_building_type` VALUES (1,'registered','2021-07-13 12:00:51'),(2,'unregistered','2022-02-03 06:26:43');
/*!40000 ALTER TABLE `dd_building_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_category`
--

DROP TABLE IF EXISTS `dd_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_category`
--

LOCK TABLES `dd_category` WRITE;
/*!40000 ALTER TABLE `dd_category` DISABLE KEYS */;
INSERT INTO `dd_category` VALUES (1,'residential','2021-03-09 04:54:21'),(2,'commercial','2021-03-09 04:54:25'),(7,'residential & commercial','2021-03-09 04:54:25');
/*!40000 ALTER TABLE `dd_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_category_type`
--

DROP TABLE IF EXISTS `dd_category_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_category_type` (
  `category_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_type_name` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_category_type`
--

LOCK TABLES `dd_category_type` WRITE;
/*!40000 ALTER TABLE `dd_category_type` DISABLE KEYS */;
INSERT INTO `dd_category_type` VALUES (1,'test','2021-06-09 02:42:06');
/*!40000 ALTER TABLE `dd_category_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_city`
--

DROP TABLE IF EXISTS `dd_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`city_id`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `dd_city_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `dd_state` (`dd_state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_city`
--

LOCK TABLES `dd_city` WRITE;
/*!40000 ALTER TABLE `dd_city` DISABLE KEYS */;
INSERT INTO `dd_city` VALUES (1,1,'thane','2021-05-26 08:26:03');
/*!40000 ALTER TABLE `dd_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_configuration`
--

DROP TABLE IF EXISTS `dd_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_configuration` (
  `configuration_id` int(11) NOT NULL AUTO_INCREMENT,
  `configuration_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`configuration_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_configuration`
--

LOCK TABLES `dd_configuration` WRITE;
/*!40000 ALTER TABLE `dd_configuration` DISABLE KEYS */;
INSERT INTO `dd_configuration` VALUES (19,'1 rk','2021-12-02 07:27:24'),(20,'1 bhk','2021-12-02 07:27:30'),(21,'1.5 bhk','2021-12-02 07:28:06'),(22,'2 bhk','2021-12-02 07:29:02'),(23,'2.5 bhk','2021-12-02 07:29:09'),(24,'3 bhk','2021-12-02 07:29:14'),(25,'3.5 bhk','2021-12-02 07:29:20'),(26,'4 bhk','2021-12-02 07:29:25'),(27,'4.5 bhk','2021-12-02 07:29:32'),(28,'5 bhk','2021-12-02 07:29:37'),(29,'5.5 bhk','2021-12-02 07:29:42'),(30,'6 bhk','2021-12-02 07:29:47');
/*!40000 ALTER TABLE `dd_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_configuration_size`
--

DROP TABLE IF EXISTS `dd_configuration_size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_configuration_size` (
  `configuration_size_id` int(11) NOT NULL AUTO_INCREMENT,
  `configuration_size` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`configuration_size_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_configuration_size`
--

LOCK TABLES `dd_configuration_size` WRITE;
/*!40000 ALTER TABLE `dd_configuration_size` DISABLE KEYS */;
INSERT INTO `dd_configuration_size` VALUES (1,'optima','2021-07-13 07:06:55'),(2,'ultima','2021-07-13 07:07:04'),(3,'prima','2021-12-02 07:32:31');
/*!40000 ALTER TABLE `dd_configuration_size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_construction_stage`
--

DROP TABLE IF EXISTS `dd_construction_stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_construction_stage` (
  `construction_stage_id` int(11) NOT NULL AUTO_INCREMENT,
  `construction_stage` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`construction_stage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_construction_stage`
--

LOCK TABLES `dd_construction_stage` WRITE;
/*!40000 ALTER TABLE `dd_construction_stage` DISABLE KEYS */;
INSERT INTO `dd_construction_stage` VALUES (1,'excavation','2022-01-12 15:47:34'),(2,'foundation','2022-01-12 15:47:40'),(3,'plinth beam','2022-01-12 15:47:46'),(4,'slab','2022-01-12 15:47:58'),(6,'top slab','2022-01-12 15:48:28'),(7,'exterior finishes','2022-01-12 15:48:29'),(8,'fit out','2022-01-12 15:48:34'),(9,'floor 1','2022-02-09 06:47:07'),(10,'floor 2','2022-02-09 06:47:07'),(11,'floor 3','2022-02-09 06:47:07'),(12,'floor 4','2022-02-09 06:47:07'),(13,'floor 5','2022-02-09 06:47:07'),(14,'floor 6','2022-02-09 06:47:07'),(15,'floor 7','2022-02-09 06:47:07'),(16,'floor 8','2022-02-09 06:47:07'),(17,'floor 9','2022-02-09 06:47:07'),(18,'floor 10','2022-02-09 06:47:07'),(19,'floor 11','2022-02-09 06:47:07'),(20,'floor 12','2022-02-09 06:47:07'),(21,'floor 13','2022-02-09 06:47:07'),(22,'floor 14','2022-02-09 06:47:07'),(23,'floor 15','2022-02-09 06:47:07'),(24,'floor 16','2022-02-09 06:47:07'),(25,'floor 17','2022-02-09 06:47:07'),(26,'floor 18','2022-02-09 06:47:07'),(27,'floor 19','2022-02-09 06:47:07'),(28,'floor 20','2022-02-09 06:47:07'),(29,'floor 21','2022-02-09 06:47:07'),(30,'floor 22','2022-02-09 06:47:07'),(31,'floor 23','2022-02-09 06:47:07'),(32,'floor 24','2022-02-09 06:47:07'),(33,'floor 25','2022-02-09 06:47:07'),(34,'floor 26','2022-02-09 06:47:07'),(35,'floor 27','2022-02-09 06:47:07'),(36,'floor 28','2022-02-09 06:47:07'),(37,'floor 29','2022-02-09 06:47:07'),(38,'floor 30','2022-02-09 06:47:07'),(39,'floor 31','2022-02-09 06:47:07'),(40,'floor 32','2022-02-09 06:47:07'),(41,'floor 33','2022-02-09 06:47:07'),(42,'floor 34','2022-02-09 06:47:07'),(43,'floor 35','2022-02-09 06:47:07'),(44,'floor 36','2022-02-09 06:47:07'),(45,'floor 37','2022-02-09 06:47:07'),(46,'floor 38','2022-02-09 06:47:07'),(47,'floor 39','2022-02-09 06:47:07'),(48,'floor 40','2022-02-09 06:47:07'),(49,'floor 41','2022-02-09 06:47:07'),(50,'floor 42','2022-02-09 06:47:07'),(51,'floor 43','2022-02-09 06:47:07'),(52,'floor 44','2022-02-09 06:47:07'),(53,'floor 45','2022-02-09 06:47:07'),(54,'floor 46','2022-02-09 06:47:07'),(55,'floor 47','2022-02-09 06:47:07'),(56,'floor 48','2022-02-09 06:47:07'),(57,'floor 49','2022-02-09 06:47:07'),(58,'floor 50','2022-02-09 06:47:07'),(59,'floor 51','2022-02-09 06:47:07'),(60,'floor 52','2022-02-09 06:47:07'),(61,'floor 53','2022-02-09 06:47:07'),(62,'floor 54','2022-02-09 06:47:07'),(63,'floor 55','2022-02-09 06:47:07'),(64,'floor 56','2022-02-09 06:47:07'),(65,'floor 57','2022-02-09 06:47:07'),(66,'floor 58','2022-02-09 06:47:07'),(67,'floor 59','2022-02-09 06:47:07'),(68,'floor 60','2022-02-09 06:47:07'),(69,'floor 61','2022-02-09 06:47:07'),(70,'floor 62','2022-02-09 06:47:07'),(71,'floor 63','2022-02-09 06:47:07'),(72,'floor 64','2022-02-09 06:47:07'),(73,'floor 65','2022-02-09 06:47:07'),(74,'floor 66','2022-02-09 06:47:07'),(75,'floor 67','2022-02-09 06:47:07'),(76,'floor 68','2022-02-09 06:47:07'),(77,'floor 69','2022-02-09 06:47:07'),(78,'floor 70','2022-02-09 06:47:07'),(79,'floor 71','2022-02-09 06:47:07'),(80,'floor 72','2022-02-09 06:47:07'),(81,'floor 73','2022-02-09 06:47:07'),(82,'floor 74','2022-02-09 06:47:07'),(83,'floor 75','2022-02-09 06:47:07'),(84,'floor 76','2022-02-09 06:47:07'),(85,'floor 77','2022-02-09 06:47:07'),(86,'floor 78','2022-02-09 06:47:07'),(87,'floor 79','2022-02-09 06:47:07'),(88,'floor 80','2022-02-09 06:47:07'),(89,'floor 81','2022-02-09 06:47:07'),(90,'floor 82','2022-02-09 06:47:07'),(91,'floor 83','2022-02-09 06:47:07'),(92,'floor 84','2022-02-09 06:47:07'),(93,'floor 85','2022-02-09 06:47:07'),(94,'floor 86','2022-02-09 06:47:07'),(95,'floor 87','2022-02-09 06:47:07'),(96,'floor 88','2022-02-09 06:47:07'),(97,'floor 89','2022-02-09 06:47:07'),(98,'floor 90','2022-02-09 06:47:07'),(99,'floor 91','2022-02-09 06:47:07'),(100,'floor 92','2022-02-09 06:47:07'),(101,'floor 93','2022-02-09 06:47:07'),(102,'floor 94','2022-02-09 06:47:07'),(103,'floor 95','2022-02-09 06:47:07'),(104,'floor 96','2022-02-09 06:47:07'),(105,'floor 97','2022-02-09 06:47:07'),(106,'floor 98','2022-02-09 06:47:07'),(107,'floor 99','2022-02-09 06:47:07'),(108,'floor 100','2022-02-09 06:47:07');
/*!40000 ALTER TABLE `dd_construction_stage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_construction_technology`
--

DROP TABLE IF EXISTS `dd_construction_technology`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_construction_technology` (
  `construction_technology_id` int(11) NOT NULL AUTO_INCREMENT,
  `construction_technology_name` varchar(100) NOT NULL,
  `createdby` bigint(20) DEFAULT NULL,
  `modifiedby` bigint(20) DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`construction_technology_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_construction_technology`
--

LOCK TABLES `dd_construction_technology` WRITE;
/*!40000 ALTER TABLE `dd_construction_technology` DISABLE KEYS */;
INSERT INTO `dd_construction_technology` VALUES (1,'mivan',0,0,'2021-06-09 02:37:50'),(2,'brick',NULL,NULL,'2021-12-02 08:46:47'),(3,'cement block',NULL,NULL,'2021-12-02 08:46:54'),(4,'safe, sustainable, green & fast',NULL,NULL,'2022-01-17 13:30:05'),(5,'tunnel foam',NULL,NULL,'2021-12-02 08:47:14');
/*!40000 ALTER TABLE `dd_construction_technology` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_country`
--

DROP TABLE IF EXISTS `dd_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_country` (
  `country_code_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) DEFAULT NULL,
  `country_code` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`country_code_id`),
  KEY `country_code_id` (`country_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_country`
--

LOCK TABLES `dd_country` WRITE;
/*!40000 ALTER TABLE `dd_country` DISABLE KEYS */;
INSERT INTO `dd_country` VALUES (1,'India','+91','2021-07-06 08:35:54'),(2,NULL,'+93','2021-12-02 09:19:03'),(3,NULL,'+355','2021-12-02 09:19:08'),(4,NULL,'+213','2021-12-02 09:19:14'),(5,NULL,'+1-684','2021-12-02 09:20:10'),(6,NULL,'+376','2021-12-02 09:20:19'),(7,NULL,'+244','2021-12-02 09:20:25'),(8,NULL,'+1-264','2021-12-02 09:20:32'),(9,NULL,'+672','2021-12-02 09:20:36'),(10,NULL,'+1-268','2021-12-02 09:20:42'),(11,NULL,'+54','2021-12-02 09:20:46'),(12,NULL,'+374','2021-12-02 09:20:52'),(13,NULL,'+297','2021-12-02 09:20:56'),(14,NULL,'+61','2021-12-02 09:21:00'),(15,NULL,'+43','2021-12-02 09:21:05'),(16,NULL,'+994','2021-12-02 09:21:14'),(17,NULL,'+1-242','2021-12-02 09:21:18'),(18,NULL,'+973','2021-12-02 09:21:34'),(19,NULL,'+880','2021-12-02 09:21:40'),(20,NULL,'+1-246','2021-12-02 09:21:45'),(21,NULL,'+375','2021-12-02 09:21:50'),(22,NULL,'+32','2021-12-02 09:22:02'),(23,NULL,'+501','2021-12-02 09:22:06'),(24,NULL,'+229','2021-12-02 09:22:12'),(25,NULL,'+1-441','2021-12-02 09:47:47'),(26,NULL,'+975','2021-12-02 09:47:47'),(27,NULL,'+591','2021-12-02 09:47:47'),(28,NULL,'+387','2021-12-02 09:47:47'),(29,NULL,'+267','2021-12-02 09:47:47'),(30,NULL,'+55','2021-12-02 09:47:47'),(31,NULL,'+246','2021-12-02 09:47:47'),(32,NULL,'+1-284','2021-12-02 09:47:47'),(33,NULL,'+673','2021-12-02 09:47:47'),(34,NULL,'+359','2021-12-02 09:47:47'),(35,NULL,'+226','2021-12-02 09:47:47'),(36,NULL,'+257','2021-12-02 09:47:47'),(37,NULL,'+855','2021-12-02 09:47:47'),(38,NULL,'+237','2021-12-02 09:47:47'),(39,NULL,'+1','2021-12-02 09:47:47'),(40,NULL,'+238','2021-12-02 09:47:47'),(41,NULL,'+1-345','2021-12-02 09:47:47'),(42,NULL,'+236','2021-12-02 09:47:47'),(43,NULL,'+235','2021-12-02 09:47:47'),(44,NULL,'+56','2021-12-02 09:47:47'),(45,NULL,'+86','2021-12-02 09:47:47'),(46,NULL,'+61','2021-12-02 09:47:47'),(47,NULL,'+61','2021-12-02 09:47:47'),(48,NULL,'+57','2021-12-02 09:47:47'),(49,NULL,'+269','2021-12-02 09:47:47'),(50,NULL,'+682','2021-12-02 09:47:47'),(51,NULL,'+506','2021-12-02 09:47:47'),(52,NULL,'+385','2021-12-02 09:47:47'),(53,NULL,'+53','2021-12-02 09:47:47'),(54,NULL,'+599','2021-12-02 09:47:47'),(55,NULL,'+357','2021-12-02 09:47:47'),(56,NULL,'+420','2021-12-02 09:47:47'),(57,NULL,'+243','2021-12-02 09:47:47'),(58,NULL,'+45','2021-12-02 09:47:47'),(59,NULL,'+253','2021-12-02 09:47:47'),(60,NULL,'+1-767','2021-12-02 09:47:47'),(61,NULL,'+1-809, 1-829, 1-849','2021-12-02 09:47:47'),(62,NULL,'+670','2021-12-02 09:47:47'),(63,NULL,'+593','2021-12-02 09:47:47'),(64,NULL,'+20','2021-12-02 09:47:47'),(65,NULL,'+503','2021-12-02 09:47:47'),(66,NULL,'+240','2021-12-02 09:47:47'),(67,NULL,'+291','2021-12-02 09:47:47'),(68,NULL,'+372','2021-12-02 09:47:47'),(69,NULL,'+251','2021-12-02 09:47:47'),(70,NULL,'+500','2021-12-02 09:47:47'),(71,NULL,'+298','2021-12-02 09:47:47'),(72,NULL,'+679','2021-12-02 09:47:47'),(73,NULL,'+358','2021-12-02 09:47:47'),(74,NULL,'+33','2021-12-02 09:47:47'),(75,NULL,'+689','2021-12-02 09:47:47'),(76,NULL,'+241','2021-12-02 09:47:47'),(77,NULL,'+220','2021-12-02 09:47:47'),(78,NULL,'+995','2021-12-02 09:47:47'),(79,NULL,'+49','2021-12-02 09:47:47'),(80,NULL,'+233','2021-12-02 09:47:47'),(81,NULL,'+350','2021-12-02 09:47:47'),(82,NULL,'+30','2021-12-02 09:47:47'),(83,NULL,'+299','2021-12-02 09:47:47'),(84,NULL,'+1-473','2021-12-02 09:47:47'),(85,NULL,'+1-671','2021-12-02 09:47:47'),(86,NULL,'+502','2021-12-02 09:47:47'),(87,NULL,'+44-1481','2021-12-02 09:47:47'),(88,NULL,'+224','2021-12-02 09:47:47'),(89,NULL,'+245','2021-12-02 09:47:47'),(90,NULL,'+592','2021-12-02 09:47:47'),(91,NULL,'+509','2021-12-02 09:47:47'),(92,NULL,'+504','2021-12-02 09:47:47'),(93,NULL,'+852','2021-12-02 09:47:47'),(94,NULL,'+36','2021-12-02 09:47:47'),(95,NULL,'+354','2021-12-02 09:47:47'),(96,NULL,'+91','2021-12-02 09:47:47'),(97,NULL,'+62','2021-12-02 09:47:47'),(98,NULL,'+98','2021-12-02 09:47:47'),(99,NULL,'+964','2021-12-02 09:47:47'),(100,NULL,'+353','2021-12-02 09:47:47'),(101,NULL,'+44-1624','2021-12-02 09:47:47'),(102,NULL,'+972','2021-12-02 09:47:47'),(103,NULL,'+39','2021-12-02 09:47:47'),(104,NULL,'+225','2021-12-02 09:47:47'),(105,NULL,'+1-876','2021-12-02 09:47:47'),(106,NULL,'+81','2021-12-02 09:47:47'),(107,NULL,'+44-1534','2021-12-02 09:47:47'),(108,NULL,'+962','2021-12-02 09:47:47'),(109,NULL,'+7','2021-12-02 09:47:47'),(110,NULL,'+254','2021-12-02 09:47:47'),(111,NULL,'+686','2021-12-02 09:47:47'),(112,NULL,'+383','2021-12-02 09:47:47'),(113,NULL,'+965','2021-12-02 09:47:47'),(114,NULL,'+996','2021-12-02 09:47:47'),(115,NULL,'+856','2021-12-02 09:47:47'),(116,NULL,'+371','2021-12-02 09:47:48'),(117,NULL,'+961','2021-12-02 09:47:48'),(118,NULL,'+266','2021-12-02 09:47:48'),(119,NULL,'+231','2021-12-02 09:47:48'),(120,NULL,'+218','2021-12-02 09:47:48'),(121,NULL,'+423','2021-12-02 09:47:48'),(122,NULL,'+370','2021-12-02 09:47:48'),(123,NULL,'+352','2021-12-02 09:47:48'),(124,NULL,'+853','2021-12-02 09:47:48'),(125,NULL,'+389','2021-12-02 09:47:48'),(126,NULL,'+261','2021-12-02 09:47:48'),(127,NULL,'+265','2021-12-02 09:47:48'),(128,NULL,'+60','2021-12-02 09:47:48'),(129,NULL,'+960','2021-12-02 09:47:48'),(130,NULL,'+223','2021-12-02 09:47:48'),(131,NULL,'+356','2021-12-02 09:47:48'),(132,NULL,'+692','2021-12-02 09:47:48'),(133,NULL,'+222','2021-12-02 09:47:48'),(134,NULL,'+230','2021-12-02 09:47:48'),(135,NULL,'+262','2021-12-02 09:47:48'),(136,NULL,'+52','2021-12-02 09:47:48'),(137,NULL,'+691','2021-12-02 09:47:48'),(138,NULL,'+373','2021-12-02 09:47:48'),(139,NULL,'+377','2021-12-02 09:47:48'),(140,NULL,'+976','2021-12-02 09:47:48'),(141,NULL,'+382','2021-12-02 09:47:48'),(142,NULL,'+1-664','2021-12-02 09:47:48'),(143,NULL,'+212','2021-12-02 09:47:48'),(144,NULL,'+258','2021-12-02 09:47:48'),(145,NULL,'+95','2021-12-02 09:47:48'),(146,NULL,'+264','2021-12-02 09:47:48'),(147,NULL,'+674','2021-12-02 09:47:48'),(148,NULL,'+977','2021-12-02 09:47:48'),(149,NULL,'+31','2021-12-02 09:47:48'),(150,NULL,'+599','2021-12-02 09:47:48'),(151,NULL,'+687','2021-12-02 09:47:48'),(152,NULL,'+64','2021-12-02 09:47:48'),(153,NULL,'+505','2021-12-02 09:47:48'),(154,NULL,'+227','2021-12-02 09:47:48'),(155,NULL,'+234','2021-12-02 09:47:48'),(156,NULL,'+683','2021-12-02 09:47:48'),(157,NULL,'+850','2021-12-02 09:47:48'),(158,NULL,'+1-670','2021-12-02 09:47:48'),(159,NULL,'+47','2021-12-02 09:47:48'),(160,NULL,'+968','2021-12-02 09:47:48'),(161,NULL,'+92','2021-12-02 09:47:48'),(162,NULL,'+680','2021-12-02 09:47:48'),(163,NULL,'+970','2021-12-02 09:47:48'),(164,NULL,'+507','2021-12-02 09:47:48'),(165,NULL,'+675','2021-12-02 09:47:48'),(166,NULL,'+595','2021-12-02 09:47:48'),(167,NULL,'+51','2021-12-02 09:47:48'),(168,NULL,'+63','2021-12-02 09:47:48'),(169,NULL,'+64','2021-12-02 09:47:48'),(170,NULL,'+48','2021-12-02 09:47:48'),(171,NULL,'+351','2021-12-02 09:47:48'),(172,NULL,'+1-787, 1-939','2021-12-02 09:47:48'),(173,NULL,'+974','2021-12-02 09:47:48'),(174,NULL,'+242','2021-12-02 09:47:48'),(175,NULL,'+262','2021-12-02 09:47:48'),(176,NULL,'+40','2021-12-02 09:47:48'),(177,NULL,'+7','2021-12-02 09:47:48'),(178,NULL,'+250','2021-12-02 09:47:48'),(179,NULL,'+590','2021-12-02 09:47:48'),(180,NULL,'+290','2021-12-02 09:47:48'),(181,NULL,'+1-869','2021-12-02 09:47:48'),(182,NULL,'+1-758','2021-12-02 09:47:48'),(183,NULL,'+590','2021-12-02 09:47:48'),(184,NULL,'+508','2021-12-02 09:47:48'),(185,NULL,'+1-784','2021-12-02 09:47:48'),(186,NULL,'+685','2021-12-02 09:47:48'),(187,NULL,'+378','2021-12-02 09:47:48'),(188,NULL,'+239','2021-12-02 09:47:48'),(189,NULL,'+966','2021-12-02 09:47:48'),(190,NULL,'+221','2021-12-02 09:47:48'),(191,NULL,'+381','2021-12-02 09:47:48'),(192,NULL,'+248','2021-12-02 09:47:48'),(193,NULL,'+232','2021-12-02 09:47:48'),(194,NULL,'+65','2021-12-02 09:47:48'),(195,NULL,'+1-721','2021-12-02 09:47:48'),(196,NULL,'+421','2021-12-02 09:47:48'),(197,NULL,'+386','2021-12-02 09:47:48'),(198,NULL,'+677','2021-12-02 09:47:48'),(199,NULL,'+252','2021-12-02 09:47:48'),(200,NULL,'+27','2021-12-02 09:47:48'),(201,NULL,'+82','2021-12-02 09:47:48'),(202,NULL,'+211','2021-12-02 09:47:48'),(203,NULL,'+34','2021-12-02 09:47:48'),(204,NULL,'+94','2021-12-02 09:47:48'),(205,NULL,'+249','2021-12-02 09:47:48'),(206,NULL,'+597','2021-12-02 09:47:48'),(207,NULL,'+47','2021-12-02 09:47:48'),(208,NULL,'+268','2021-12-02 09:47:48'),(209,NULL,'+46','2021-12-02 09:47:48'),(210,NULL,'+41','2021-12-02 09:47:48'),(211,NULL,'+963','2021-12-02 09:47:48'),(212,NULL,'+886','2021-12-02 09:47:48'),(213,NULL,'+992','2021-12-02 09:47:48'),(214,NULL,'+255','2021-12-02 09:47:48'),(215,NULL,'+66','2021-12-02 09:47:48'),(216,NULL,'+228','2021-12-02 09:47:48'),(217,NULL,'+690','2021-12-02 09:47:48'),(218,NULL,'+676','2021-12-02 09:47:48'),(219,NULL,'+1-868','2021-12-02 09:47:48'),(220,NULL,'+216','2021-12-02 09:47:48'),(221,NULL,'+90','2021-12-02 09:47:48'),(222,NULL,'+993','2021-12-02 09:47:48'),(223,NULL,'+1-649','2021-12-02 09:47:48'),(224,NULL,'+688','2021-12-02 09:47:48'),(225,NULL,'+1-340','2021-12-02 09:47:48'),(226,NULL,'+256','2021-12-02 09:47:48'),(227,NULL,'+380','2021-12-02 09:47:48'),(228,NULL,'+971','2021-12-02 09:47:48'),(229,NULL,'+44','2021-12-02 09:47:48'),(230,NULL,'+1','2021-12-02 09:47:48'),(231,NULL,'+598','2021-12-02 09:47:48'),(232,NULL,'+998','2021-12-02 09:47:48'),(233,NULL,'+678','2021-12-02 09:47:48'),(234,NULL,'+379','2021-12-02 09:47:48'),(235,NULL,'+58','2021-12-02 09:47:48'),(236,NULL,'+84','2021-12-02 09:47:48'),(237,NULL,'+681','2021-12-02 09:47:48'),(238,NULL,'+212','2021-12-02 09:47:48'),(239,NULL,'+967','2021-12-02 09:47:48'),(240,NULL,'+260','2021-12-02 09:47:48'),(241,NULL,'+263','2021-12-02 09:47:48');
/*!40000 ALTER TABLE `dd_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_days`
--

DROP TABLE IF EXISTS `dd_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_days` (
  `day_id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`day_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_days`
--

LOCK TABLES `dd_days` WRITE;
/*!40000 ALTER TABLE `dd_days` DISABLE KEYS */;
INSERT INTO `dd_days` VALUES (1,'All Days','2022-01-10 11:40:27'),(2,'Sunday','2022-01-10 11:40:27'),(3,'Monday','2022-01-10 11:40:27'),(4,'Tuesday','2022-01-10 11:40:27'),(5,'Wednesday','2022-01-10 11:40:27'),(6,'Thursday','2022-01-10 11:40:27'),(7,'Friday','2022-01-10 11:40:27'),(8,'Saturday','2022-01-10 11:40:27');
/*!40000 ALTER TABLE `dd_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_designation`
--

DROP TABLE IF EXISTS `dd_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_designation` (
  `designation_id` int(11) NOT NULL AUTO_INCREMENT,
  `designation_name` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`designation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_designation`
--

LOCK TABLES `dd_designation` WRITE;
/*!40000 ALTER TABLE `dd_designation` DISABLE KEYS */;
INSERT INTO `dd_designation` VALUES (37,'director','2021-12-02 09:50:04'),(38,'vp','2021-12-02 09:52:10'),(39,'avp','2021-12-02 09:52:10'),(40,'dvp','2021-12-02 09:52:10'),(41,'gm','2021-12-02 09:52:10'),(42,'agm','2021-12-02 09:52:10'),(43,'dgm','2021-12-02 09:52:10'),(44,'sr manager','2021-12-02 09:52:10'),(45,'manager','2021-12-02 09:52:10'),(46,'am','2021-12-02 09:52:10'),(47,'dm','2021-12-02 09:52:10'),(48,'sr. executive','2021-12-02 09:52:10'),(49,'executive','2021-12-02 09:52:10'),(50,'asst executive','2021-12-02 09:52:10'),(51,'dy executive','2021-12-02 09:52:10'),(52,'sr. officer','2021-12-02 09:52:10'),(53,'officer','2021-12-02 09:52:10'),(54,'asst officer','2021-12-02 09:52:10'),(55,'dy. officer, ','2021-12-02 09:52:10'),(56,'supervisor','2021-12-02 09:52:10'),(57,'sr.assistant','2021-12-02 09:52:10'),(58,'supervisor','2021-12-02 09:52:10'),(59,'Chairman','2022-01-23 08:01:42'),(60,'Vice Chairman & Managing Director','2022-01-23 08:05:06'),(61,'Vice Chairman & Managing Director','2022-01-23 08:05:08'),(62,'Executive Director','2022-01-23 10:18:48'),(63,'test','2022-01-31 14:24:56'),(64,'ABC','2022-02-02 09:42:35');
/*!40000 ALTER TABLE `dd_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_direction`
--

DROP TABLE IF EXISTS `dd_direction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_direction` (
  `direction_id` int(11) NOT NULL AUTO_INCREMENT,
  `direction` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`direction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_direction`
--

LOCK TABLES `dd_direction` WRITE;
/*!40000 ALTER TABLE `dd_direction` DISABLE KEYS */;
INSERT INTO `dd_direction` VALUES (1,'north','2021-07-19 02:39:03'),(2,'east','2021-07-19 02:39:10'),(3,'west','2021-07-19 02:39:17'),(4,'south','2021-07-19 02:39:24'),(5,'north east','2021-07-19 02:39:33'),(6,'north west','2021-07-19 02:39:45'),(7,'south east','2021-07-19 02:39:54'),(8,'south west','2021-07-19 02:40:02'),(9,'no south','2021-07-19 02:40:10'),(10,'no north','2021-07-19 02:40:23'),(11,'no east','2021-07-19 02:40:27'),(12,'no west','2021-07-19 02:40:34'),(13,'any','2021-07-19 02:40:38');
/*!40000 ALTER TABLE `dd_direction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_elevation`
--

DROP TABLE IF EXISTS `dd_elevation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_elevation` (
  `dd_elevation_id` int(11) NOT NULL AUTO_INCREMENT,
  `elevation_name` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dd_elevation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_elevation`
--

LOCK TABLES `dd_elevation` WRITE;
/*!40000 ALTER TABLE `dd_elevation` DISABLE KEYS */;
INSERT INTO `dd_elevation` VALUES (15,'ground','2022-02-01 09:30:48'),(16,'stilt','2022-02-01 09:30:48'),(17,'podium','2022-02-01 09:30:48'),(18,'podium parking 1 ','2022-02-01 09:30:48'),(19,'podium parking 2','2022-02-01 09:30:48'),(20,'podium parking 3','2022-02-01 09:30:48'),(21,'podium parking 4','2022-02-01 09:30:48'),(22,'podium parking 5','2022-02-01 09:30:48'),(23,'podium parking 6','2022-02-01 09:30:48'),(24,'podium parking 7','2022-02-01 09:30:48'),(25,'podium parking 8','2022-02-01 09:30:48'),(26,'podium parking 9','2022-02-01 09:30:48'),(27,'podium parking 10','2022-02-01 09:30:48'),(28,'basement 1','2022-02-01 09:30:48'),(29,'basement 2','2022-02-01 09:30:48'),(30,'basement 3','2022-02-01 09:30:48'),(31,'basement 4','2022-02-01 09:30:48'),(32,'basement 5','2022-02-01 09:30:48'),(33,'basement 6','2022-02-01 09:30:48'),(34,'basement 7','2022-02-01 09:30:48'),(35,'basement 8','2022-02-01 09:30:48'),(36,'basement 9','2022-02-01 09:30:48'),(37,'basement 10','2022-02-01 09:30:48'),(38,'amenities level 1','2022-02-01 09:30:48'),(39,'amenities level 2','2022-02-01 09:30:48'),(40,'amenities level 3','2022-02-01 09:30:48'),(41,'amenities level 4','2022-02-01 09:30:48'),(42,'amenities level 5','2022-02-01 09:30:48'),(43,'floor 1','2022-02-01 09:30:48'),(44,'floor 2','2022-02-01 09:30:48'),(45,'floor 3','2022-02-01 09:30:48'),(46,'floor 4','2022-02-01 09:30:48'),(47,'floor 5','2022-02-01 09:30:48'),(48,'floor 6','2022-02-01 09:30:48'),(49,'floor 7','2022-02-01 09:30:48'),(50,'floor 8','2022-02-01 09:30:48'),(51,'floor 9','2022-02-01 09:30:48'),(52,'floor 10','2022-02-01 09:30:48'),(53,'floor 11','2022-02-01 09:30:48'),(54,'floor 12','2022-02-01 09:30:48'),(55,'floor 13','2022-02-01 09:30:48'),(56,'floor 14','2022-02-01 09:30:48'),(57,'floor 15','2022-02-01 09:30:48'),(58,'floor 16','2022-02-01 09:30:48'),(59,'floor 17','2022-02-01 09:30:48'),(60,'floor 18','2022-02-01 09:30:48'),(61,'floor 19','2022-02-01 09:30:48'),(62,'floor 20','2022-02-01 09:30:48'),(63,'floor 21','2022-02-01 09:30:48'),(64,'floor 22','2022-02-01 09:30:48'),(65,'floor 23','2022-02-01 09:30:48'),(66,'floor 24','2022-02-01 09:30:48'),(67,'floor 25','2022-02-01 09:30:48'),(68,'floor 26','2022-02-01 09:30:48'),(69,'floor 27','2022-02-01 09:30:48'),(70,'floor 28','2022-02-01 09:30:48'),(71,'floor 29','2022-02-01 09:30:48'),(72,'floor 30','2022-02-01 09:30:48'),(73,'floor 31','2022-02-01 09:30:48'),(74,'floor 32','2022-02-01 09:30:48'),(75,'floor 33','2022-02-01 09:30:48'),(76,'floor 34','2022-02-01 09:30:48'),(77,'floor 35','2022-02-01 09:30:48'),(78,'floor 36','2022-02-01 09:30:48'),(79,'floor 37','2022-02-01 09:30:48'),(80,'floor 38','2022-02-01 09:30:48'),(81,'floor 39','2022-02-01 09:30:48'),(82,'floor 40','2022-02-01 09:30:48'),(83,'floor 41','2022-02-01 09:30:48'),(84,'floor 42','2022-02-01 09:30:48'),(85,'floor 43','2022-02-01 09:30:48'),(86,'floor 44','2022-02-01 09:30:48'),(87,'floor 45','2022-02-01 09:30:48'),(88,'floor 46','2022-02-01 09:30:48'),(89,'floor 47','2022-02-01 09:30:48'),(90,'floor 48','2022-02-01 09:30:48'),(91,'floor 49','2022-02-01 09:30:48'),(92,'floor 50','2022-02-01 09:30:48'),(93,'floor 51','2022-02-01 09:30:48'),(94,'floor 52','2022-02-01 09:30:48'),(95,'floor 53','2022-02-01 09:30:48'),(96,'floor 54','2022-02-01 09:30:48'),(97,'floor 55','2022-02-01 09:30:48'),(98,'floor 56','2022-02-01 09:30:48'),(99,'floor 57','2022-02-01 09:30:48'),(100,'floor 58','2022-02-01 09:30:48'),(101,'floor 59','2022-02-01 09:30:48'),(102,'floor 60','2022-02-01 09:30:48'),(103,'floor 61','2022-02-01 09:30:48'),(104,'floor 62','2022-02-01 09:30:48'),(105,'floor 63','2022-02-01 09:30:48'),(106,'floor 64','2022-02-01 09:30:48'),(107,'floor 65','2022-02-01 09:30:48'),(108,'floor 66','2022-02-01 09:30:48'),(109,'floor 67','2022-02-01 09:30:48'),(110,'floor 68','2022-02-01 09:30:48'),(111,'floor 69','2022-02-01 09:30:48'),(112,'floor 70','2022-02-01 09:30:48'),(113,'floor 71','2022-02-01 09:30:48'),(114,'floor 72','2022-02-01 09:30:48'),(115,'floor 73','2022-02-01 09:30:48'),(116,'floor 74','2022-02-01 09:30:48'),(117,'floor 75','2022-02-01 09:30:48'),(118,'floor 76','2022-02-01 09:30:48'),(119,'floor 77','2022-02-01 09:30:48'),(120,'floor 78','2022-02-01 09:30:48'),(121,'floor 79','2022-02-01 09:30:48'),(122,'floor 80','2022-02-01 09:30:48'),(123,'floor 81','2022-02-01 09:30:48'),(124,'floor 82','2022-02-01 09:30:48'),(125,'floor 83','2022-02-01 09:30:48'),(126,'floor 84','2022-02-01 09:30:48'),(127,'floor 85','2022-02-01 09:30:48'),(128,'floor 86','2022-02-01 09:30:48'),(129,'floor 87','2022-02-01 09:30:48'),(130,'floor 88','2022-02-01 09:30:48'),(131,'floor 89','2022-02-01 09:30:48'),(132,'floor 90','2022-02-01 09:30:48'),(133,'floor 91','2022-02-01 09:30:48'),(134,'floor 92','2022-02-01 09:30:48'),(135,'floor 93','2022-02-01 09:30:48'),(136,'floor 94','2022-02-01 09:30:48'),(137,'floor 95','2022-02-01 09:30:48'),(138,'floor 96','2022-02-01 09:30:48'),(139,'floor 97','2022-02-01 09:30:48'),(140,'floor 98','2022-02-01 09:30:48'),(141,'floor 99','2022-02-01 09:30:48'),(142,'floor 100','2022-02-01 09:30:48');
/*!40000 ALTER TABLE `dd_elevation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_funded_by`
--

DROP TABLE IF EXISTS `dd_funded_by`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_funded_by` (
  `funded_by_id` int(11) NOT NULL AUTO_INCREMENT,
  `funded_by_name` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`funded_by_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_funded_by`
--

LOCK TABLES `dd_funded_by` WRITE;
/*!40000 ALTER TABLE `dd_funded_by` DISABLE KEYS */;
INSERT INTO `dd_funded_by` VALUES (1,'allahabad bank','2021-12-02 08:56:18'),(2,'andhra bank','2021-12-02 08:56:18'),(3,'axis bank','2021-12-02 08:56:18'),(4,'bank of india','2021-12-02 08:56:18'),(5,'bank of maharashtra','2021-12-02 08:56:18'),(6,'canara bank','2021-12-02 08:56:18'),(7,'central bank of india','2021-12-02 08:56:26'),(8,'city union bank','2021-12-02 08:56:33'),(9,'corporation bank','2021-12-02 08:56:40'),(10,'deutsche bank','2021-12-02 08:56:48'),(11,'dhanlaxmi bank','2021-12-02 08:56:54'),(12,'federal bank','2021-12-02 08:57:01'),(13,'icici bank','2021-12-02 08:57:08'),(15,'idbi bank','2021-12-02 08:57:32'),(16,'indian bank','2021-12-02 08:57:36'),(17,'indian overseas bank','2021-12-02 08:57:43'),(18,'indusind bank','2021-12-02 08:57:57'),(19,'ing vysya bank','2021-12-02 08:58:03'),(20,'jammu and kashmir bank','2021-12-02 08:58:26'),(21,'karnataka bank ltd','2021-12-02 08:58:31'),(22,'karur vysya bank','2021-12-02 08:58:38'),(23,'kotak bank','2021-12-02 08:58:46'),(24,'laxmi vilas bank','2021-12-02 08:58:56'),(25,'punjab national bank','2021-12-02 08:59:26'),(26,'punjab & sind bank','2021-12-02 08:59:31'),(27,'shamrao vitthal co-operative bank','2021-12-02 08:59:37'),(28,'south indian bank','2021-12-02 08:59:44'),(29,'state bank of bikaner & jaipur','2021-12-02 09:00:23'),(30,'state bank of hyderabad','2021-12-02 09:00:28'),(31,'state bank of india','2021-12-02 09:00:34'),(32,'state bank of mysore','2021-12-02 09:00:39'),(33,'state bank of patiala','2021-12-02 09:00:45'),(34,'state bank of travancore','2021-12-02 09:00:50'),(35,'syndicate bank','2021-12-02 09:00:55'),(36,'uco bank','2021-12-02 09:01:02'),(37,'union bank of india','2021-12-02 09:01:11'),(38,'united bank of india','2021-12-02 09:01:16'),(39,'vijaya bank','2021-12-02 09:01:22'),(40,'yes bank ltd','2021-12-02 09:01:28');
/*!40000 ALTER TABLE `dd_funded_by` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_initials`
--

DROP TABLE IF EXISTS `dd_initials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_initials` (
  `initials_id` int(11) NOT NULL AUTO_INCREMENT,
  `initials_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`initials_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_initials`
--

LOCK TABLES `dd_initials` WRITE;
/*!40000 ALTER TABLE `dd_initials` DISABLE KEYS */;
INSERT INTO `dd_initials` VALUES (1,'mr.','2021-05-12 06:21:27'),(2,'mrs.','2021-05-12 06:21:27'),(3,'m/s','2021-05-12 06:21:27'),(4,'miss','2021-05-12 06:21:27'),(5,'dr.','2021-05-12 06:21:27'),(6,'adv.','2021-05-12 06:21:27'),(7,'pro.','2021-05-12 06:21:27');
/*!40000 ALTER TABLE `dd_initials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_lead_by`
--

DROP TABLE IF EXISTS `dd_lead_by`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_lead_by` (
  `lead_by_id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_by_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lead_by_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_lead_by`
--

LOCK TABLES `dd_lead_by` WRITE;
/*!40000 ALTER TABLE `dd_lead_by` DISABLE KEYS */;
INSERT INTO `dd_lead_by` VALUES (1,'surabhi realtor','2021-03-10 07:17:51'),(2,'property consultant','2021-03-09 10:06:38'),(3,'builder','2021-04-27 08:08:05');
/*!40000 ALTER TABLE `dd_lead_by` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_lead_campaign`
--

DROP TABLE IF EXISTS `dd_lead_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_lead_campaign` (
  `lead_campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_campaign_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lead_campaign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_lead_campaign`
--

LOCK TABLES `dd_lead_campaign` WRITE;
/*!40000 ALTER TABLE `dd_lead_campaign` DISABLE KEYS */;
INSERT INTO `dd_lead_campaign` VALUES (1,'project listing','2021-03-09 10:19:10'),(2,'normal listing ','2021-05-11 05:51:47'),(3,'omni','2021-03-09 10:19:20'),(4,'email mktg / social media','2021-03-09 10:19:24'),(5,'premium listing','2021-03-09 10:19:31'),(6,'platinum listing','2021-03-09 10:19:34'),(7,'certified listing','2021-03-09 10:19:39');
/*!40000 ALTER TABLE `dd_lead_campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_lead_source`
--

DROP TABLE IF EXISTS `dd_lead_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_lead_source` (
  `lead_source_id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_source_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lead_source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_lead_source`
--

LOCK TABLES `dd_lead_source` WRITE;
/*!40000 ALTER TABLE `dd_lead_source` DISABLE KEYS */;
INSERT INTO `dd_lead_source` VALUES (1,'99 acres','2021-03-09 10:16:52'),(2,'magic bricks','2021-03-09 10:16:52'),(3,'common floor','2021-03-09 10:16:52'),(4,'housing','2021-03-09 10:16:52'),(5,'sulekha','2021-03-09 10:16:52'),(6,'quikr','2021-03-09 10:16:52'),(7,'olx','2021-03-09 10:16:52'),(8,'call - in bound','2021-03-09 10:16:52'),(9,'call - out bound','2021-03-09 10:16:52'),(10,'facebook','2021-03-09 10:16:52'),(11,'google','2021-03-09 10:16:52'),(12,'gmb','2021-03-09 10:16:52'),(13,'linkedin','2021-03-09 10:16:52'),(14,'sms','2021-03-09 10:16:52'),(15,'email -inbound','2021-03-09 10:16:52'),(16,'email out bound','2021-03-09 10:16:52'),(17,'whatsapp','2021-03-09 10:16:52'),(18,'instagram','2021-03-09 10:16:52'),(19,'youtube','2021-03-09 10:16:52'),(20,'telegram','2021-03-09 10:16:52'),(21,'walk in','2021-03-09 10:16:52'),(22,'referencer','2021-03-09 10:16:52'),(23,'website','2021-03-09 10:16:52');
/*!40000 ALTER TABLE `dd_lead_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_lead_stage`
--

DROP TABLE IF EXISTS `dd_lead_stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_lead_stage` (
  `lead_stage_id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_stage_name` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lead_stage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_lead_stage`
--

LOCK TABLES `dd_lead_stage` WRITE;
/*!40000 ALTER TABLE `dd_lead_stage` DISABLE KEYS */;
INSERT INTO `dd_lead_stage` VALUES (1,'new','2021-05-13 04:06:47'),(2,'assigned','2021-05-13 04:06:47'),(3,'follow up - information','2021-05-13 04:06:47'),(4,'follow up appointment - site visit','2021-05-13 04:06:47'),(5,'follow up appointment - f2f','2021-05-13 04:06:47'),(6,'follow up appointment - online','2021-05-13 04:06:47'),(7,'site visit completed','2021-05-13 04:06:47'),(8,'f2f completed','2021-05-13 04:06:47'),(9,'online completed','2021-05-13 04:06:47'),(10,'follow up - post site visit','2021-05-13 04:06:47'),(11,'follow up - post f2f','2021-05-13 04:06:47'),(12,'follow up - post online','2021-05-13 04:06:47'),(13,'follow up for revisit','2021-05-13 04:06:47'),(14,'follow up - f2f again','2021-05-13 04:06:47'),(15,'follow up for negotation','2021-05-13 04:06:47'),(16,'follow up for closure meeting','2021-05-13 04:06:47'),(17,'closed – won – match property','2021-05-13 04:06:47'),(18,'closed – lost - reason','2021-05-13 04:06:47'),(19,'reinstate','2021-05-13 04:06:47'),(20,'active','2021-05-13 04:06:47'),(21,'inactive','2021-05-13 04:06:47');
/*!40000 ALTER TABLE `dd_lead_stage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_lead_type`
--

DROP TABLE IF EXISTS `dd_lead_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_lead_type` (
  `lead_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_type_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lead_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_lead_type`
--

LOCK TABLES `dd_lead_type` WRITE;
/*!40000 ALTER TABLE `dd_lead_type` DISABLE KEYS */;
INSERT INTO `dd_lead_type` VALUES (1,'buyer','2021-03-09 10:05:15'),(2,'tenant','2021-03-09 10:05:21'),(3,'owner','2021-05-12 06:16:28'),(4,'seller','2021-05-12 06:16:33');
/*!40000 ALTER TABLE `dd_lead_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_location`
--

DROP TABLE IF EXISTS `dd_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) DEFAULT NULL,
  `location_name` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`location_id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `dd_location_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `dd_city` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_location`
--

LOCK TABLES `dd_location` WRITE;
/*!40000 ALTER TABLE `dd_location` DISABLE KEYS */;
INSERT INTO `dd_location` VALUES (1,1,'thane  (east)','2021-05-12 09:13:29'),(2,1,'thane  (west)','2021-05-12 09:13:35'),(3,1,'pokharan road no.1','2021-05-12 09:13:39'),(4,1,'pokharan road no.2','2021-05-12 09:13:45'),(5,1,'dhokali','2021-05-12 09:13:51'),(6,1,'kolshet','2021-05-12 09:14:02'),(7,1,'kapurbawdi','2021-05-12 09:14:09'),(8,1,'ghodbhunder road','2021-05-12 09:14:15');
/*!40000 ALTER TABLE `dd_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_parking`
--

DROP TABLE IF EXISTS `dd_parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_parking` (
  `parking_id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`parking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_parking`
--

LOCK TABLES `dd_parking` WRITE;
/*!40000 ALTER TABLE `dd_parking` DISABLE KEYS */;
INSERT INTO `dd_parking` VALUES (21,'alloted','2021-12-02 08:48:16'),(22,'basement','2021-12-02 08:48:22'),(23,'covered ','2021-12-02 08:48:29'),(24,'first come first served','2021-12-02 08:48:35'),(25,'hydraulic','2021-12-02 08:48:40'),(26,'lower basement','2021-12-02 08:48:48'),(27,'multistorey','2021-12-02 08:48:54'),(28,'open','2021-12-02 08:48:59'),(29,'outside building','2021-12-02 08:49:04'),(30,'podium','2021-12-02 08:49:11'),(31,'puzzle','2021-12-02 08:49:16'),(32,'reserved','2021-12-02 08:49:21'),(33,'stack','2021-12-02 08:49:26'),(34,'stilt','2021-12-02 08:49:32'),(35,'tandem','2021-12-02 08:49:38'),(36,'upper basement','2021-12-02 08:49:46'),(37,'visitor parking','2021-12-02 08:49:52');
/*!40000 ALTER TABLE `dd_parking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_payment_type`
--

DROP TABLE IF EXISTS `dd_payment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_payment_type` (
  `payment_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_name` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_payment_type`
--

LOCK TABLES `dd_payment_type` WRITE;
/*!40000 ALTER TABLE `dd_payment_type` DISABLE KEYS */;
INSERT INTO `dd_payment_type` VALUES (1,'clp','2021-12-02 09:04:02'),(2,'builder subvention','2021-12-02 09:04:02'),(3,'bank subvention','2021-12-02 09:04:02'),(4,'10 : 90','2021-12-02 09:04:02'),(5,'20 : 80','2021-12-02 09:04:02'),(6,'30 : 70','2021-12-02 09:04:02'),(7,'40 : 60','2021-12-02 09:04:09'),(8,'50 : 50','2021-12-02 09:04:14'),(9,'10 : 15 : 75','2021-12-02 09:04:31'),(10,'25 : 25 : 25 : 25','2021-12-02 09:04:41');
/*!40000 ALTER TABLE `dd_payment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_preferred_tenant`
--

DROP TABLE IF EXISTS `dd_preferred_tenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_preferred_tenant` (
  `preferred_tenant_id` int(11) NOT NULL AUTO_INCREMENT,
  `preferred_tenant` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`preferred_tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_preferred_tenant`
--

LOCK TABLES `dd_preferred_tenant` WRITE;
/*!40000 ALTER TABLE `dd_preferred_tenant` DISABLE KEYS */;
INSERT INTO `dd_preferred_tenant` VALUES (1,'family','2021-07-13 08:14:17'),(2,'bachelor','2021-07-13 08:14:26'),(3,'spinster','2022-01-10 11:35:17'),(4,'any','2022-01-18 12:46:49');
/*!40000 ALTER TABLE `dd_preferred_tenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_project_amenities`
--

DROP TABLE IF EXISTS `dd_project_amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_project_amenities` (
  `project_amenities_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amenities` varchar(100) NOT NULL,
  `description` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_amenities_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_project_amenities`
--

LOCK TABLES `dd_project_amenities` WRITE;
/*!40000 ALTER TABLE `dd_project_amenities` DISABLE KEYS */;
INSERT INTO `dd_project_amenities` VALUES (1,'gym','','2021-07-09 05:19:40'),(2,'swimming pool','','2021-07-09 05:20:03'),(4,'businss center','','2021-07-09 05:20:23'),(6,'Swimming Pool',NULL,'2021-11-11 16:31:09'),(7,'Club House',NULL,'2021-11-20 11:15:05'),(8,'Lawn Tennis',NULL,'2021-11-20 11:33:35'),(9,'Amphitheatre',NULL,'2021-11-20 11:33:54'),(10,'Indoor Games',NULL,'2021-11-20 11:59:38'),(12,'Garden',NULL,'2021-11-20 12:01:57'),(13,'Club house',NULL,'2021-11-20 14:17:20'),(15,'Temple',NULL,'2021-11-20 16:57:05'),(16,'Park',NULL,'2021-11-21 10:53:27'),(17,'Intercom facility',NULL,'2021-11-21 10:53:41'),(18,'Wi-Fi',NULL,'2021-11-21 10:54:05'),(19,'Jogging track',NULL,'2021-11-21 10:54:14'),(20,'Amphitheatre',NULL,'2021-11-21 10:54:55'),(21,'Rain water harvesting',NULL,'2021-11-21 10:55:23'),(22,'Library',NULL,'2021-11-21 10:55:54'),(24,'park',NULL,'2021-11-23 07:02:54'),(26,'Amenity',NULL,'2021-11-26 12:06:26'),(27,'Vastu',NULL,'2021-11-27 08:02:33'),(30,'Badminton court',NULL,'2021-12-05 13:12:19'),(31,'Children play area',NULL,'2021-12-06 04:00:11'),(32,'Podium Garden',NULL,'2021-12-06 04:02:13'),(33,'Party Lawn',NULL,'2021-12-06 05:05:43'),(38,'Bank attached',NULL,'2022-01-11 07:08:20'),(39,'Flooring',NULL,'2022-01-23 10:26:23'),(40,'Kitchen',NULL,'2022-01-23 10:27:50'),(41,'Sanitary',NULL,'2022-01-23 10:35:04'),(42,'Electrical',NULL,'2022-01-23 10:38:31'),(43,'Doors',NULL,'2022-01-23 10:39:00'),(44,'Windows',NULL,'2022-01-23 10:39:08'),(45,'Painting',NULL,'2022-01-23 10:39:18'),(46,'Security',NULL,'2022-01-23 10:39:33'),(47,'Squash Court',NULL,'2022-01-27 13:00:57'),(48,'Landscaped Garden',NULL,'2022-01-27 13:02:30');
/*!40000 ALTER TABLE `dd_project_amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_rating`
--

DROP TABLE IF EXISTS `dd_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_rating` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_rating`
--

LOCK TABLES `dd_rating` WRITE;
/*!40000 ALTER TABLE `dd_rating` DISABLE KEYS */;
INSERT INTO `dd_rating` VALUES (1,1,'2021-07-11 06:30:21'),(2,2,'2021-07-11 06:30:22'),(3,3,'2021-07-11 06:30:24'),(4,4,'2021-07-11 06:30:25'),(5,5,'2021-07-11 06:30:28');
/*!40000 ALTER TABLE `dd_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_room`
--

DROP TABLE IF EXISTS `dd_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_room`
--

LOCK TABLES `dd_room` WRITE;
/*!40000 ALTER TABLE `dd_room` DISABLE KEYS */;
INSERT INTO `dd_room` VALUES (17,'room','2021-12-02 07:38:12'),(18,'bedroom 1','2021-12-02 07:38:21'),(19,'bedroom 2','2021-12-02 07:38:31'),(20,'bedroom 3','2021-12-02 07:38:38'),(21,'bedroom 4','2021-12-02 07:38:44'),(22,'bedroom 5','2021-12-02 07:38:54'),(23,'bedroom 6','2021-12-02 07:38:59'),(24,'fb - bed 1','2021-12-02 07:39:08'),(25,'fb - bed 2','2021-12-02 07:39:14'),(26,'fb - bed 3','2021-12-02 07:39:20'),(27,'fb - bed 4','2021-12-02 07:39:26'),(28,'fb - bed 5','2021-12-02 07:39:32'),(29,'fb - bed 6','2021-12-02 07:39:39'),(30,'balcony - bed 1','2021-12-02 07:39:48'),(31,'balcony - bed 2','2021-12-02 07:39:56'),(32,'balcony - bed 3','2021-12-02 07:40:04'),(33,'balcony - bed 4','2021-12-02 07:40:11'),(34,'balcony - bed 5','2021-12-02 07:40:16'),(35,'balcony - bed 6','2021-12-02 07:40:28'),(36,'washroom - bed 1','2021-12-02 07:40:40'),(37,'washroom - bed 2','2021-12-02 07:40:47'),(38,'washroom - bed 3','2021-12-02 07:40:55'),(39,'washroom - bed 4','2021-12-02 07:40:59'),(40,'washroom - bed 5','2021-12-02 07:41:05'),(41,'washroom - bed 6','2021-12-02 07:41:10'),(42,'common washroom','2021-12-02 07:41:15'),(43,'kitchen - l shape','2021-12-02 07:41:20'),(44,'kitchen -single platform','2021-12-02 07:41:25'),(45,'kitchen - parallel platform','2021-12-02 07:41:31'),(46,'hall','2021-12-02 07:41:37'),(47,'hall with dinning space- l shape','2021-12-02 07:41:45'),(48,'common balcony','2021-12-02 07:41:51'),(49,'walk in wardrobe','2021-12-02 07:41:56'),(50,'pooja room','2021-12-02 07:42:02'),(51,'duct','2021-12-02 07:42:07'),(52,'Living & Dining','2022-02-08 07:18:33'),(53,'Bedroom CB','2022-02-08 07:20:28'),(54,'Kitchen','2022-02-08 07:22:32'),(55,'Kitchen Duct','2022-02-08 07:23:01'),(56,'Living Room','2022-02-10 09:04:17'),(57,'Dining','2022-02-10 10:03:57'),(58,'Common WC','2022-02-10 10:06:19'),(59,'Common Bedroom','2022-02-10 10:07:06'),(60,'Master Bedroom','2022-02-10 10:09:11'),(61,'Bedroom','2022-02-10 10:19:04'),(62,'Bedroom WC','2022-02-10 10:19:57'),(63,'Master Bedroom CB','2022-02-10 11:04:34'),(64,'Master Bedroom WC','2022-02-10 11:05:05'),(65,'Living & Dinning C B','2022-02-10 12:12:23'),(66,'Living & Dinning Deck','2022-02-10 12:12:54'),(67,'Kitchen Utility','2022-02-10 12:19:07'),(68,'Servant WC','2022-02-10 12:20:26'),(69,'Master Bedroom Dress','2022-02-10 13:16:54'),(70,'Common W C ( P TOI)','2022-02-10 13:19:23'),(71,'NEW','2022-02-10 13:32:55'),(72,'Common CB','2022-02-10 13:41:24'),(75,'Prasanna','2022-02-11 06:33:05'),(76,'Entrance Lobby','2022-02-11 11:34:30'),(77,'Study Room','2022-02-11 11:35:23'),(78,'Study Room Cupboard','2022-02-11 11:35:46'),(79,'Bedroom Cupboard','2022-02-11 11:36:19'),(80,'Attached Washroom','2022-02-11 11:36:59'),(81,'Passage','2022-02-11 11:37:49'),(82,'Enclosed','2022-02-11 11:38:09'),(83,'Enclosed Balcony','2022-02-11 11:38:38'),(84,'Cupboard','2022-02-11 12:11:05'),(85,'Enclosed Balcony (Living Room)','2022-02-11 12:14:03'),(86,'Enclosed Balcony ( Master Bedroom)','2022-02-11 12:14:58'),(87,'Living','2022-02-11 12:53:05'),(88,'Dinning','2022-02-11 12:56:51');
/*!40000 ALTER TABLE `dd_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_state`
--

DROP TABLE IF EXISTS `dd_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_state` (
  `dd_state_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code_id` int(11) NOT NULL,
  `state_name` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dd_state_id`),
  KEY `country_code_id` (`country_code_id`),
  CONSTRAINT `dd_state_ibfk_1` FOREIGN KEY (`country_code_id`) REFERENCES `dd_country` (`country_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_state`
--

LOCK TABLES `dd_state` WRITE;
/*!40000 ALTER TABLE `dd_state` DISABLE KEYS */;
INSERT INTO `dd_state` VALUES (1,1,'maharashtra','2021-05-26 08:25:50');
/*!40000 ALTER TABLE `dd_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_std_designation`
--

DROP TABLE IF EXISTS `dd_std_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_std_designation` (
  `designation_id` int(11) NOT NULL AUTO_INCREMENT,
  `designation_name` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`designation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_std_designation`
--

LOCK TABLES `dd_std_designation` WRITE;
/*!40000 ALTER TABLE `dd_std_designation` DISABLE KEYS */;
INSERT INTO `dd_std_designation` VALUES (1,'chairman','2022-01-11 05:58:49'),(2,'secretary','2022-01-11 05:59:10'),(3,'treasurer','2022-01-11 05:59:34'),(4,'supervisor','2022-01-11 05:59:45'),(5,'security personnel','2022-01-11 06:00:34'),(6,'society manager','2022-01-11 06:00:05'),(7,'society personnel','2022-01-11 06:00:27');
/*!40000 ALTER TABLE `dd_std_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_sub_location`
--

DROP TABLE IF EXISTS `dd_sub_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_sub_location` (
  `sub_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `sub_location_name` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sub_location_id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `dd_sub_location_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `dd_location` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_sub_location`
--

LOCK TABLES `dd_sub_location` WRITE;
/*!40000 ALTER TABLE `dd_sub_location` DISABLE KEYS */;
INSERT INTO `dd_sub_location` VALUES (1,1,'kopri','2021-05-12 09:16:16'),(2,1,'hari om nagar','2021-05-12 09:16:29'),(3,2,'naupada','2021-05-12 09:16:52'),(4,2,'ram maruti marg','2021-05-12 09:17:02'),(5,2,'brahman society','2021-05-12 09:17:09'),(6,2,'hariniwas','2021-05-12 09:17:15'),(7,2,'malhar','2021-05-12 09:17:22'),(8,2,'teen petrol pump','2021-05-12 09:17:29'),(9,2,'tembhi naka','2021-05-12 09:17:42'),(10,2,'panchpakhdi','2021-05-12 09:17:48'),(11,2,'uthalsar','2021-05-12 09:17:54'),(12,2,'makmali talao','2021-05-12 09:18:02'),(13,2,'charai','2021-05-12 09:18:09'),(14,2,'khopat','2021-05-12 09:18:15'),(15,2,'castle mill','2021-05-12 09:18:22'),(16,2,'vrindavan','2021-05-12 09:18:31'),(17,2,'teen haath naka','2021-05-12 09:18:37'),(18,2,'louis wadi','2021-05-12 09:18:42'),(19,2,'kisan nagar','2021-05-12 09:18:49'),(20,3,'vartak nagar','2021-05-12 09:19:10'),(21,4,'pawar nagar','2021-05-12 09:19:23'),(22,4,'vasant vihar','2021-05-12 09:19:29'),(23,4,'upvan','2021-05-12 09:19:37'),(24,5,'dhokali','2021-05-12 09:20:06'),(25,6,'kolshet','2021-05-12 09:20:16'),(26,7,'kapurbawdi','2021-05-12 09:20:50'),(27,8,'tulsidham','2021-05-12 09:21:04'),(28,8,'manpada','2021-05-12 09:21:13'),(29,8,'brahmand','2021-05-12 09:21:21'),(30,8,'patlipada','2021-05-12 09:21:30'),(31,8,'hiranandani estate','2021-05-12 09:21:39'),(32,8,'waghbil','2021-05-12 09:21:46'),(33,8,'anandnagar','2021-05-12 09:21:53'),(34,8,'kasarvadavali','2021-05-12 09:21:59');
/*!40000 ALTER TABLE `dd_sub_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_unit`
--

DROP TABLE IF EXISTS `dd_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_unit`
--

LOCK TABLES `dd_unit` WRITE;
/*!40000 ALTER TABLE `dd_unit` DISABLE KEYS */;
INSERT INTO `dd_unit` VALUES (1,'sq.ft','2021-03-09 13:52:38'),(2,'sq.mt','2021-03-09 13:52:45'),(3,'acres','2021-03-09 13:52:53');
/*!40000 ALTER TABLE `dd_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_unit_amenities`
--

DROP TABLE IF EXISTS `dd_unit_amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_unit_amenities` (
  `unit_amenities_id` int(11) NOT NULL AUTO_INCREMENT,
  `amenities` varchar(200) DEFAULT NULL,
  `description` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`unit_amenities_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_unit_amenities`
--

LOCK TABLES `dd_unit_amenities` WRITE;
/*!40000 ALTER TABLE `dd_unit_amenities` DISABLE KEYS */;
/*!40000 ALTER TABLE `dd_unit_amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_unit_status`
--

DROP TABLE IF EXISTS `dd_unit_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_unit_status` (
  `unit_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_status` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`unit_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_unit_status`
--

LOCK TABLES `dd_unit_status` WRITE;
/*!40000 ALTER TABLE `dd_unit_status` DISABLE KEYS */;
INSERT INTO `dd_unit_status` VALUES (1,'rtmi','2021-07-11 06:29:28'),(2,'uc','2021-07-11 06:29:31'),(3,'rtmi & uc','2021-07-11 06:29:28');
/*!40000 ALTER TABLE `dd_unit_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_unit_type_commercial`
--

DROP TABLE IF EXISTS `dd_unit_type_commercial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_unit_type_commercial` (
  `unit_type_commercial_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_type_commercial_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`unit_type_commercial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_unit_type_commercial`
--

LOCK TABLES `dd_unit_type_commercial` WRITE;
/*!40000 ALTER TABLE `dd_unit_type_commercial` DISABLE KEYS */;
INSERT INTO `dd_unit_type_commercial` VALUES (1,'office - corporate','2022-01-13 09:59:42'),(2,'office - mall','2022-01-13 09:59:48'),(3,'office - residential','2022-01-13 09:59:54'),(4,'office - it park','2022-01-13 10:00:00'),(5,'shop - mall','2022-01-13 10:00:08'),(6,'shop - residential','2022-01-13 10:00:17'),(7,'shop - market','2022-01-13 10:00:24'),(8,'larger stores','2022-01-13 10:00:31'),(9,'showroom','2022-01-13 10:00:36'),(10,'godown','2022-01-13 10:00:51'),(11,'ware house','2022-01-13 10:00:58'),(12,'unit','2022-01-13 10:01:04'),(13,'industrial gala','2022-01-13 10:01:11'),(14,'flat','2022-01-13 10:01:17'),(15,'row house','2022-01-13 10:01:23'),(16,'bungalow','2022-01-13 10:01:28'),(17,'duplex','2022-01-13 10:01:33'),(18,'villa','2022-01-13 10:01:38'),(19,'pent house','2022-01-13 10:01:44'),(20,'independent house','2022-01-13 10:01:50'),(21,'any','2022-01-13 10:01:57');
/*!40000 ALTER TABLE `dd_unit_type_commercial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dd_unit_type_residential`
--

DROP TABLE IF EXISTS `dd_unit_type_residential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dd_unit_type_residential` (
  `unit_type_residential_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_type_residential_name` varchar(150) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`unit_type_residential_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dd_unit_type_residential`
--

LOCK TABLES `dd_unit_type_residential` WRITE;
/*!40000 ALTER TABLE `dd_unit_type_residential` DISABLE KEYS */;
INSERT INTO `dd_unit_type_residential` VALUES (1,'flat','2022-01-13 10:02:20'),(2,'row house','2022-01-13 10:03:05'),(3,'bungalow','2022-01-13 10:02:32'),(4,'duplex','2022-01-13 10:02:37'),(5,'villa','2022-01-13 10:02:42'),(6,'pent house','2022-01-13 10:03:05'),(7,'independent house','2022-01-13 10:02:53'),(8,'any','2022-01-13 10:02:56');
/*!40000 ALTER TABLE `dd_unit_type_residential` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_company`
--

DROP TABLE IF EXISTS `log_op_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_company` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) DEFAULT NULL,
  `group_name` varchar(150) DEFAULT NULL,
  `regd_office_address` text,
  `office_phone_extension` varchar(10) DEFAULT NULL,
  `office_phone` varchar(50) DEFAULT NULL,
  `office_email_id` varchar(50) DEFAULT NULL,
  `web_site_url` varchar(50) DEFAULT NULL,
  `tat_period` varchar(50) DEFAULT NULL,
  `physical_tat_period` varchar(50) DEFAULT NULL,
  `builder_invoice_format` text,
  `builder_proforma_format` text,
  `builder_payment_process` text,
  `images` text,
  `cp_empanelment_agreement` text,
  `company_rating` int(11) DEFAULT NULL,
  `cp_code` varchar(50) DEFAULT NULL,
  `comments` text,
  `key_member` varchar(50) DEFAULT NULL,
  `help_desk_email` varchar(50) DEFAULT NULL,
  `payment_tat_period` varchar(50) DEFAULT NULL,
  `status` char(1) DEFAULT '1' COMMENT '1= Active , 0=Inactive',
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_date` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_company`
--

LOCK TABLES `log_op_company` WRITE;
/*!40000 ALTER TABLE `log_op_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_company_awards_recongnition`
--

DROP TABLE IF EXISTS `log_op_company_awards_recongnition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_company_awards_recongnition` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL,
  `awards_reco_name` varchar(100) DEFAULT NULL,
  `awards_reco_image` text,
  `awards_reco_year` varchar(100) DEFAULT NULL,
  `awards_reco_desc` text,
  `company_id` bigint(20) NOT NULL,
  `status` char(1) DEFAULT '1' COMMENT '1= Active , 0=Inactive',
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `updated_date` varchar(100) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_company_awards_recongnition`
--

LOCK TABLES `log_op_company_awards_recongnition` WRITE;
/*!40000 ALTER TABLE `log_op_company_awards_recongnition` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_company_awards_recongnition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_company_images`
--

DROP TABLE IF EXISTS `log_op_company_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_company_images` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_images_id` bigint(20) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `createdby` bigint(20) DEFAULT NULL,
  `modifiedby` bigint(20) DEFAULT NULL,
  `modified_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_company_images`
--

LOCK TABLES `log_op_company_images` WRITE;
/*!40000 ALTER TABLE `log_op_company_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_company_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_company_pro_cons`
--

DROP TABLE IF EXISTS `log_op_company_pro_cons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_company_pro_cons` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_pro_cons_id` int(11) DEFAULT NULL,
  `company_pros` longtext,
  `company_cons` longtext,
  `description` longtext,
  `company_id` bigint(20) NOT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_company_pro_cons`
--

LOCK TABLES `log_op_company_pro_cons` WRITE;
/*!40000 ALTER TABLE `log_op_company_pro_cons` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_company_pro_cons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_employee`
--

DROP TABLE IF EXISTS `log_op_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_employee` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_name` varchar(100) NOT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_employee`
--

LOCK TABLES `log_op_employee` WRITE;
/*!40000 ALTER TABLE `log_op_employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_floor_plan`
--

DROP TABLE IF EXISTS `log_op_floor_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_floor_plan` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `floor_plan_id` bigint(20) DEFAULT NULL,
  `wing_id` bigint(20) NOT NULL,
  `floor_plan` varchar(255) NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `modifiedby` bigint(20) NOT NULL,
  `modified_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_floor_plan`
--

LOCK TABLES `log_op_floor_plan` WRITE;
/*!40000 ALTER TABLE `log_op_floor_plan` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_floor_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_parking`
--

DROP TABLE IF EXISTS `log_op_parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_parking` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `op_parking_id` int(11) DEFAULT NULL,
  `society_id` bigint(20) DEFAULT NULL,
  `parking_id` int(11) DEFAULT NULL,
  `createdby` bigint(20) DEFAULT NULL,
  `modifiedby` bigint(20) DEFAULT NULL,
  `modified_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_parking`
--

LOCK TABLES `log_op_parking` WRITE;
/*!40000 ALTER TABLE `log_op_parking` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_parking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_project`
--

DROP TABLE IF EXISTS `log_op_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_project` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) DEFAULT NULL,
  `group_id` bigint(20) NOT NULL,
  `project_complex_name` varchar(100) DEFAULT NULL,
  `category_id` varchar(50) DEFAULT NULL,
  `unit_status_of_complex` int(11) DEFAULT NULL,
  `sub_location_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `complex_rating` int(11) DEFAULT NULL,
  `complex_description` longtext,
  `complex_land_parcel` varchar(255) DEFAULT NULL,
  `complex_open_space` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `sales_office_address` longtext,
  `e_brochure` varchar(255) DEFAULT NULL,
  `video_links` varchar(255) DEFAULT NULL,
  `gst_no` varchar(255) DEFAULT NULL,
  `gst_certificate` varchar(255) DEFAULT NULL,
  `comments` longtext,
  `status` char(1) DEFAULT '1' COMMENT '1= Active , 0=Inactive',
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_project`
--

LOCK TABLES `log_op_project` WRITE;
/*!40000 ALTER TABLE `log_op_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_project_amenities`
--

DROP TABLE IF EXISTS `log_op_project_amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_project_amenities` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_amenities_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) NOT NULL,
  `amenities_id` bigint(20) NOT NULL,
  `description` longtext,
  `is_available` char(1) NOT NULL DEFAULT 'Y',
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_project_amenities`
--

LOCK TABLES `log_op_project_amenities` WRITE;
/*!40000 ALTER TABLE `log_op_project_amenities` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_project_amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_project_hirarchy`
--

DROP TABLE IF EXISTS `log_op_project_hirarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_project_hirarchy` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_hirarchy_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) NOT NULL,
  `name_initial` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `whatsapp_number` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `reports_to` int(11) DEFAULT NULL,
  `country_code_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `comments` text,
  `moved_to` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_project_hirarchy`
--

LOCK TABLES `log_op_project_hirarchy` WRITE;
/*!40000 ALTER TABLE `log_op_project_hirarchy` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_project_hirarchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_project_photo`
--

DROP TABLE IF EXISTS `log_op_project_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_project_photo` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_photo_id` int(11) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_project_photo`
--

LOCK TABLES `log_op_project_photo` WRITE;
/*!40000 ALTER TABLE `log_op_project_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_project_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society`
--

DROP TABLE IF EXISTS `log_op_society`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `building_code` varchar(255) DEFAULT NULL,
  `building_status` varchar(255) DEFAULT NULL,
  `building_owner` varchar(255) DEFAULT NULL,
  `building_name` varchar(255) DEFAULT NULL,
  `unit_status` varchar(255) DEFAULT NULL,
  `no_of_wings` varchar(255) DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `society_status_id` int(11) DEFAULT NULL,
  `building_type_id` varchar(255) DEFAULT NULL,
  `lattitde` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `location_id` varchar(255) DEFAULT NULL,
  `sub_location_id` varchar(255) DEFAULT NULL,
  `address_1` longtext,
  `address_2` longtext,
  `city_id` varchar(255) DEFAULT NULL,
  `state_id` varchar(255) DEFAULT NULL,
  `rera_certificate` varchar(255) DEFAULT NULL,
  `conveyance_deed` varchar(255) DEFAULT NULL,
  `conveyance_deed_document` varchar(255) DEFAULT NULL,
  `maintenance_amount` varchar(100) DEFAULT NULL,
  `pin_code` int(11) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_plan_comment` text,
  `offers` text,
  `floor_rise` varchar(255) DEFAULT NULL,
  `parking` varchar(255) DEFAULT NULL,
  `brokerage_fees` double DEFAULT NULL,
  `building_constructed_area` double DEFAULT NULL,
  `building_land_parcel_area` int(11) DEFAULT NULL,
  `building_launch_date` varchar(255) DEFAULT NULL,
  `rera_completion_year` varchar(255) DEFAULT NULL,
  `builder_completion_year` varchar(255) DEFAULT NULL,
  `society_details` longtext,
  `weekly_off` varchar(255) DEFAULT NULL,
  `office_timing` varchar(255) DEFAULT NULL,
  `building_construction_photos` varchar(255) DEFAULT NULL,
  `oc_certificate` varchar(255) DEFAULT NULL,
  `oc_reciving_date` varchar(255) DEFAULT NULL,
  `registration_certificate` varchar(255) DEFAULT NULL,
  `ebrouchure` varchar(255) DEFAULT NULL,
  `pros` longtext,
  `cons` longtext,
  `comment` longtext,
  `building_video_links` varchar(255) DEFAULT NULL,
  `building_config_details` varchar(255) DEFAULT NULL,
  `brokarage_comments` longtext,
  `brokarage_from_date` varchar(255) DEFAULT NULL,
  `brokarage_to_date` varchar(255) DEFAULT NULL,
  `unit_type` varchar(255) DEFAULT NULL,
  `funded_by` varchar(255) DEFAULT NULL,
  `construction_technology` varchar(255) DEFAULT NULL,
  `shifting_charges` varchar(255) DEFAULT NULL,
  `prefered_tenant` varchar(255) DEFAULT NULL,
  `building_description` longtext,
  `building_construction_area` varchar(255) DEFAULT NULL,
  `building_open_space` varchar(255) DEFAULT NULL,
  `gst_certificate` varchar(255) DEFAULT NULL,
  `possession_year` varchar(255) DEFAULT NULL,
  `building_year_as_per_oc` varchar(255) DEFAULT NULL,
  `building_year_as_per_possession` varchar(255) DEFAULT NULL,
  `building_rating` varchar(50) DEFAULT NULL,
  `society_office_address` longtext,
  `working_days_and_time` varchar(255) DEFAULT NULL,
  `occupation_certi_dated` varchar(255) DEFAULT NULL,
  `total_flat_available` int(11) DEFAULT NULL,
  `indian_green_building` varchar(255) DEFAULT NULL,
  `account_manager` varbinary(50) DEFAULT NULL,
  `status` char(1) DEFAULT '1' COMMENT '1= Active , 0=Inactive',
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society`
--

LOCK TABLES `log_op_society` WRITE;
/*!40000 ALTER TABLE `log_op_society` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_account_manager`
--

DROP TABLE IF EXISTS `log_op_society_account_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_account_manager` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_account_manager_id` int(11) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_account_manager`
--

LOCK TABLES `log_op_society_account_manager` WRITE;
/*!40000 ALTER TABLE `log_op_society_account_manager` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_account_manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_amnities`
--

DROP TABLE IF EXISTS `log_op_society_amnities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_amnities` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_amnities_id` bigint(20) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `amenities_id` bigint(20) NOT NULL,
  `description` longtext,
  `is_available` char(1) NOT NULL DEFAULT 'Y',
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_amnities`
--

LOCK TABLES `log_op_society_amnities` WRITE;
/*!40000 ALTER TABLE `log_op_society_amnities` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_amnities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_brokerage_fee`
--

DROP TABLE IF EXISTS `log_op_society_brokerage_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_brokerage_fee` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_brokerage_fee_id` int(11) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `zero_to_two` varchar(255) DEFAULT NULL,
  `three_to_five` varchar(255) DEFAULT NULL,
  `six_to_ten` varchar(255) DEFAULT NULL,
  `above_eleven` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_brokerage_fee`
--

LOCK TABLES `log_op_society_brokerage_fee` WRITE;
/*!40000 ALTER TABLE `log_op_society_brokerage_fee` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_brokerage_fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_configuration`
--

DROP TABLE IF EXISTS `log_op_society_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_configuration` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_confiuration_id` bigint(20) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `configuration_id` int(11) DEFAULT NULL,
  `suffix` int(11) DEFAULT NULL COMMENT 'configuration size',
  `sample_flat_available` char(1) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `total_falt_available` varchar(255) DEFAULT NULL,
  `flat_available` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_configuration`
--

LOCK TABLES `log_op_society_configuration` WRITE;
/*!40000 ALTER TABLE `log_op_society_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_floor_rise`
--

DROP TABLE IF EXISTS `log_op_society_floor_rise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_floor_rise` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_floor_rise_id` int(11) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `floor_slab_from` varchar(255) DEFAULT NULL,
  `floor_slab_to` varchar(255) DEFAULT NULL,
  `configuration` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `rs_per_sq_ft` varchar(255) DEFAULT NULL,
  `box_price` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_floor_rise`
--

LOCK TABLES `log_op_society_floor_rise` WRITE;
/*!40000 ALTER TABLE `log_op_society_floor_rise` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_floor_rise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_keymembers`
--

DROP TABLE IF EXISTS `log_op_society_keymembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_keymembers` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_keymembers_id` bigint(20) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `name_initial` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `country_code_id` int(11) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `whatsapp_number` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `residence_address` longtext,
  `dateupdated` date DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_keymembers`
--

LOCK TABLES `log_op_society_keymembers` WRITE;
/*!40000 ALTER TABLE `log_op_society_keymembers` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_keymembers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_photos`
--

DROP TABLE IF EXISTS `log_op_society_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_photos` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_photos_id` bigint(20) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_photos`
--

LOCK TABLES `log_op_society_photos` WRITE;
/*!40000 ALTER TABLE `log_op_society_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_pros_cons`
--

DROP TABLE IF EXISTS `log_op_society_pros_cons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_pros_cons` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `op_society_pros_cons_id` int(11) DEFAULT NULL,
  `pros_of_building` longtext,
  `cons_of_building` longtext,
  `society_id` bigint(20) NOT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_pros_cons`
--

LOCK TABLES `log_op_society_pros_cons` WRITE;
/*!40000 ALTER TABLE `log_op_society_pros_cons` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_pros_cons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_society_unit_amnities`
--

DROP TABLE IF EXISTS `log_op_society_unit_amnities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_society_unit_amnities` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_unit_amnities_id` int(11) DEFAULT NULL,
  `society_id` varchar(10) DEFAULT NULL,
  `amenities_id` varchar(10) DEFAULT NULL,
  `description` text,
  `is_available` char(1) NOT NULL DEFAULT 'Y',
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_society_unit_amnities`
--

LOCK TABLES `log_op_society_unit_amnities` WRITE;
/*!40000 ALTER TABLE `log_op_society_unit_amnities` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_society_unit_amnities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_unit`
--

DROP TABLE IF EXISTS `log_op_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_unit` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_id` bigint(20) DEFAULT NULL,
  `society_id` bigint(20) DEFAULT NULL,
  `unit_code` int(11) DEFAULT NULL,
  `custom_unit_code` varchar(255) DEFAULT NULL,
  `wing_id` bigint(20) DEFAULT NULL,
  `refugeeflat` char(5) DEFAULT NULL,
  `direction_id` varchar(10) DEFAULT NULL,
  `dfd` int(11) DEFAULT NULL,
  `unit_type` varchar(50) DEFAULT NULL,
  `unit_type_id` varchar(50) DEFAULT NULL,
  `hall_size` int(11) DEFAULT NULL,
  `balcony` varchar(10) DEFAULT NULL,
  `parking_id` int(11) DEFAULT NULL,
  `floor_no` int(11) DEFAULT NULL,
  `configuration_id` int(11) DEFAULT NULL,
  `configuration_size` int(11) DEFAULT NULL,
  `dryarea_bathroom` int(11) DEFAULT NULL,
  `no_of_bedrooms_with_sizes` int(11) DEFAULT NULL,
  `no_of_bathrooms` int(11) DEFAULT NULL,
  `kitchen_size` int(11) DEFAULT NULL,
  `dryarea_kitchen` int(11) DEFAULT NULL,
  `parking_info` varchar(100) DEFAULT NULL,
  `unit_available` char(1) DEFAULT NULL,
  `refugee_unit` char(1) DEFAULT 'f',
  `unit_view` varchar(255) DEFAULT NULL,
  `show_unit` varchar(255) DEFAULT NULL,
  `dining_area` int(11) DEFAULT NULL,
  `built_up_area` varchar(255) DEFAULT NULL,
  `comment` longtext,
  `build_up_area` varchar(255) DEFAULT NULL,
  `carpet_area` varchar(255) DEFAULT NULL,
  `rera_carpet_area` varchar(255) DEFAULT NULL,
  `mofa_carpet_area` varchar(255) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_unit`
--

LOCK TABLES `log_op_unit` WRITE;
/*!40000 ALTER TABLE `log_op_unit` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_unit_bedroom`
--

DROP TABLE IF EXISTS `log_op_unit_bedroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_unit_bedroom` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bedroom_id` bigint(20) DEFAULT NULL,
  `unit_id` bigint(20) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `dimension` varchar(255) DEFAULT NULL,
  `direction_id` int(11) DEFAULT NULL,
  `comment` longtext,
  `area` varchar(1) DEFAULT NULL,
  `selfcontained` char(1) DEFAULT NULL,
  `balcony` char(1) DEFAULT NULL,
  `balcony_area` varchar(1) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_unit_bedroom`
--

LOCK TABLES `log_op_unit_bedroom` WRITE;
/*!40000 ALTER TABLE `log_op_unit_bedroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_unit_bedroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_unit_bedroom_log`
--

DROP TABLE IF EXISTS `log_op_unit_bedroom_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_unit_bedroom_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `op_unit_bedroom_log_id` bigint(20) DEFAULT NULL,
  `bedroom_id` varchar(50) NOT NULL,
  `unit_id` varchar(50) DEFAULT NULL,
  `room_id` varchar(50) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `dimension` varchar(255) DEFAULT NULL,
  `direction_id` varchar(50) DEFAULT NULL,
  `comment` longtext,
  `area` varchar(50) DEFAULT NULL,
  `selfcontained` varchar(10) DEFAULT NULL,
  `balcony` varchar(50) DEFAULT NULL,
  `balcony_area` varchar(50) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_unit_bedroom_log`
--

LOCK TABLES `log_op_unit_bedroom_log` WRITE;
/*!40000 ALTER TABLE `log_op_unit_bedroom_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_unit_bedroom_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_unit_configuration`
--

DROP TABLE IF EXISTS `log_op_unit_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_unit_configuration` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_configuration_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) NOT NULL,
  `configuration_id` bigint(20) NOT NULL,
  `suffix` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `total_flats` int(11) NOT NULL,
  `available_flats` int(11) NOT NULL,
  `sample_flat` varchar(2) NOT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_unit_configuration`
--

LOCK TABLES `log_op_unit_configuration` WRITE;
/*!40000 ALTER TABLE `log_op_unit_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_unit_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_unit_photos`
--

DROP TABLE IF EXISTS `log_op_unit_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_unit_photos` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_photos_id` bigint(20) DEFAULT NULL,
  `unit_id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_unit_photos`
--

LOCK TABLES `log_op_unit_photos` WRITE;
/*!40000 ALTER TABLE `log_op_unit_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_unit_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_op_wing`
--

DROP TABLE IF EXISTS `log_op_wing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_op_wing` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wing_id` bigint(20) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `wing_name` varchar(100) NOT NULL,
  `total_floors` int(11) NOT NULL,
  `habitable_floors` int(11) NOT NULL,
  `total_units` int(11) DEFAULT NULL,
  `no_of_lifts` int(11) NOT NULL,
  `floor_plan` text,
  `name_board` text,
  `units_per_floor` int(11) DEFAULT NULL,
  `refugee_units` int(11) DEFAULT NULL,
  `comments` longtext,
  `status` char(1) DEFAULT '1' COMMENT '1= Active , 0=Inactive',
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_op_wing`
--

LOCK TABLES `log_op_wing` WRITE;
/*!40000 ALTER TABLE `log_op_wing` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_op_wing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_project_pro_cons`
--

DROP TABLE IF EXISTS `log_project_pro_cons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_project_pro_cons` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_pro_cons_id` bigint(20) DEFAULT NULL,
  `pros` longtext,
  `cons` longtext,
  `project_id` bigint(20) NOT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_project_pro_cons`
--

LOCK TABLES `log_project_pro_cons` WRITE;
/*!40000 ALTER TABLE `log_project_pro_cons` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_project_pro_cons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_society_construction_stage`
--

DROP TABLE IF EXISTS `log_society_construction_stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_society_construction_stage` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_construction_stage_id` int(11) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `building_construction_stage` varchar(255) DEFAULT NULL,
  `building_construction_stage_date` varchar(255) DEFAULT NULL,
  `construction_stage_images` varchar(255) DEFAULT NULL,
  `comments_bc` longtext,
  `created_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_society_construction_stage`
--

LOCK TABLES `log_society_construction_stage` WRITE;
/*!40000 ALTER TABLE `log_society_construction_stage` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_society_construction_stage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_society_working_days_time`
--

DROP TABLE IF EXISTS `log_society_working_days_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_society_working_days_time` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_working_days_time_id` int(11) DEFAULT NULL,
  `society_id` bigint(20) NOT NULL,
  `day_id` int(11) DEFAULT NULL,
  `office_timing` varchar(255) DEFAULT NULL,
  `office_timing_to` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_society_working_days_time`
--

LOCK TABLES `log_society_working_days_time` WRITE;
/*!40000 ALTER TABLE `log_society_working_days_time` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_society_working_days_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_building_construction_photos`
--

DROP TABLE IF EXISTS `op_building_construction_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_building_construction_photos` (
  `building_construction_photos_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `modifiedby` bigint(20) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`building_construction_photos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_building_construction_photos`
--

LOCK TABLES `op_building_construction_photos` WRITE;
/*!40000 ALTER TABLE `op_building_construction_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_building_construction_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_company`
--

DROP TABLE IF EXISTS `op_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_company` (
  `company_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  `regd_office_address` text,
  `office_phone_extension` varchar(10) DEFAULT NULL,
  `office_phone` varchar(255) DEFAULT NULL,
  `office_email_id` varchar(255) DEFAULT NULL,
  `web_site_url` varchar(255) DEFAULT NULL,
  `tat_period` varchar(255) DEFAULT NULL,
  `physical_tat_period` varchar(255) DEFAULT NULL,
  `builder_invoice_format` varchar(255) DEFAULT NULL,
  `builder_proforma_format` varchar(255) DEFAULT NULL,
  `builder_payment_process` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `cp_empanelment_agreement` varchar(255) DEFAULT NULL,
  `company_rating` int(11) DEFAULT NULL,
  `cp_code` varchar(255) DEFAULT NULL,
  `comments` longtext,
  `key_member` varchar(255) DEFAULT NULL,
  `help_desk_email` varchar(255) DEFAULT NULL,
  `payment_tat_period` varchar(50) DEFAULT NULL,
  `status` char(1) DEFAULT '1' COMMENT '	1= Active , 0=Inactive',
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_company`
--

LOCK TABLES `op_company` WRITE;
/*!40000 ALTER TABLE `op_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_company_awards_recongnition`
--

DROP TABLE IF EXISTS `op_company_awards_recongnition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_company_awards_recongnition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `awards_reco_name` varchar(100) DEFAULT NULL,
  `awards_reco_image` text,
  `awards_reco_year` varchar(255) DEFAULT NULL,
  `awards_reco_desc` longtext,
  `company_id` bigint(20) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `op_company_awards_recongnition_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `op_company` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_company_awards_recongnition`
--

LOCK TABLES `op_company_awards_recongnition` WRITE;
/*!40000 ALTER TABLE `op_company_awards_recongnition` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_company_awards_recongnition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_company_hirarchy`
--

DROP TABLE IF EXISTS `op_company_hirarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_company_hirarchy` (
  `company_hirarchy_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name_initial` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `whatsapp_number` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `reports_to` bigint(20) DEFAULT NULL,
  `country_code_id` varchar(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `comments` text,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`company_hirarchy_id`),
  KEY `company_id` (`company_id`),
  KEY `reports_to` (`reports_to`),
  KEY `designation_id` (`designation_id`),
  KEY `country_code_id` (`country_code_id`),
  CONSTRAINT `op_company_hirarchy_ibfk_8` FOREIGN KEY (`company_id`) REFERENCES `op_company` (`company_id`),
  CONSTRAINT `op_company_hirarchy_ibfk_9` FOREIGN KEY (`designation_id`) REFERENCES `dd_designation` (`designation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_company_hirarchy`
--

LOCK TABLES `op_company_hirarchy` WRITE;
/*!40000 ALTER TABLE `op_company_hirarchy` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_company_hirarchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_company_images`
--

DROP TABLE IF EXISTS `op_company_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_company_images` (
  `company_images_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `modifiedby` bigint(20) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`company_images_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `op_company_images_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `op_company` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_company_images`
--

LOCK TABLES `op_company_images` WRITE;
/*!40000 ALTER TABLE `op_company_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_company_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_company_pro_cons`
--

DROP TABLE IF EXISTS `op_company_pro_cons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_company_pro_cons` (
  `company_pro_cons_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_pros` longtext,
  `company_cons` longtext,
  `description` longtext,
  `company_id` bigint(20) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`company_pro_cons_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `op_company_pro_cons_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `op_company` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_company_pro_cons`
--

LOCK TABLES `op_company_pro_cons` WRITE;
/*!40000 ALTER TABLE `op_company_pro_cons` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_company_pro_cons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_employee`
--

DROP TABLE IF EXISTS `op_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_employee`
--

LOCK TABLES `op_employee` WRITE;
/*!40000 ALTER TABLE `op_employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_floor_plan`
--

DROP TABLE IF EXISTS `op_floor_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_floor_plan` (
  `floor_plan_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wing_id` bigint(20) NOT NULL,
  `floor_plan` varchar(255) NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `modifiedby` bigint(20) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`floor_plan_id`),
  KEY `wing_id` (`wing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_floor_plan`
--

LOCK TABLES `op_floor_plan` WRITE;
/*!40000 ALTER TABLE `op_floor_plan` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_floor_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_parking`
--

DROP TABLE IF EXISTS `op_parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_parking` (
  `op_parking_id` int(11) NOT NULL,
  `society_id` bigint(20) NOT NULL,
  `parking_id` int(11) NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `modifiedby` bigint(20) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_parking`
--

LOCK TABLES `op_parking` WRITE;
/*!40000 ALTER TABLE `op_parking` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_parking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_project`
--

DROP TABLE IF EXISTS `op_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_project` (
  `project_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `project_complex_name` varchar(100) DEFAULT NULL,
  `category_id` varchar(50) DEFAULT NULL,
  `unit_status_of_complex` int(11) DEFAULT NULL,
  `sub_location_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `complex_rating` int(11) DEFAULT NULL,
  `complex_description` longtext,
  `complex_land_parcel` varchar(255) DEFAULT NULL,
  `complex_open_space` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `sales_office_address` longtext,
  `e_brochure` varchar(255) DEFAULT NULL,
  `video_links` varchar(255) DEFAULT NULL,
  `gst_no` varchar(255) DEFAULT NULL,
  `gst_certificate` varchar(255) DEFAULT NULL,
  `comments` longtext,
  `status` char(1) DEFAULT '1' COMMENT '1= Active , 0=Inactive',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_id`),
  KEY `category_type_id` (`unit_status_of_complex`),
  KEY `company_id` (`company_id`),
  KEY `group_id` (`group_id`),
  KEY `category_id` (`category_id`),
  KEY `city_id` (`city_id`),
  KEY `sub_location_id` (`sub_location_id`),
  KEY `location_id` (`location_id`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `op_project_ibfk_12` FOREIGN KEY (`city_id`) REFERENCES `dd_city` (`city_id`),
  CONSTRAINT `op_project_ibfk_13` FOREIGN KEY (`sub_location_id`) REFERENCES `dd_sub_location` (`sub_location_id`),
  CONSTRAINT `op_project_ibfk_14` FOREIGN KEY (`location_id`) REFERENCES `dd_location` (`location_id`),
  CONSTRAINT `op_project_ibfk_15` FOREIGN KEY (`state_id`) REFERENCES `dd_state` (`dd_state_id`),
  CONSTRAINT `op_project_ibfk_6` FOREIGN KEY (`company_id`) REFERENCES `op_company` (`company_id`),
  CONSTRAINT `op_project_ibfk_8` FOREIGN KEY (`group_id`) REFERENCES `op_company` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_project`
--

LOCK TABLES `op_project` WRITE;
/*!40000 ALTER TABLE `op_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_project_amenities`
--

DROP TABLE IF EXISTS `op_project_amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_project_amenities` (
  `project_amenities_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) NOT NULL,
  `amenities_id` bigint(20) NOT NULL,
  `description` longtext,
  `is_available` char(1) NOT NULL DEFAULT 'Y',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_amenities_id`),
  KEY `project_id` (`project_id`),
  KEY `amenities_id` (`amenities_id`),
  CONSTRAINT `op_project_amenities_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `op_project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_project_amenities`
--

LOCK TABLES `op_project_amenities` WRITE;
/*!40000 ALTER TABLE `op_project_amenities` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_project_amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_project_hirarchy`
--

DROP TABLE IF EXISTS `op_project_hirarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_project_hirarchy` (
  `project_hirarchy_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) NOT NULL,
  `name_initial` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `whatsapp_number` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `reports_to` int(11) DEFAULT NULL,
  `country_code_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `comments` text,
  `moved_to` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_hirarchy_id`),
  KEY `project_id` (`project_id`),
  KEY `designation_id` (`designation_id`),
  KEY `reports_to` (`reports_to`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `op_project_hirarchy_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `op_project` (`project_id`),
  CONSTRAINT `op_project_hirarchy_ibfk_2` FOREIGN KEY (`designation_id`) REFERENCES `dd_designation` (`designation_id`),
  CONSTRAINT `op_project_hirarchy_ibfk_4` FOREIGN KEY (`company_id`) REFERENCES `op_company` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_project_hirarchy`
--

LOCK TABLES `op_project_hirarchy` WRITE;
/*!40000 ALTER TABLE `op_project_hirarchy` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_project_hirarchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_project_photo`
--

DROP TABLE IF EXISTS `op_project_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_project_photo` (
  `project_photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_photo_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `op_project_photo_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `op_project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_project_photo`
--

LOCK TABLES `op_project_photo` WRITE;
/*!40000 ALTER TABLE `op_project_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_project_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society`
--

DROP TABLE IF EXISTS `op_society`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society` (
  `society_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) DEFAULT NULL,
  `building_code` varchar(255) DEFAULT NULL,
  `building_status` varchar(255) DEFAULT NULL,
  `building_owner` varchar(255) DEFAULT NULL,
  `building_name` varchar(255) DEFAULT NULL,
  `unit_status` varchar(255) DEFAULT NULL,
  `no_of_wings` varchar(255) DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `society_status_id` int(11) DEFAULT NULL,
  `building_type_id` varchar(255) DEFAULT NULL,
  `lattitde` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `location_id` varchar(255) DEFAULT NULL,
  `sub_location_id` varchar(255) DEFAULT NULL,
  `address_1` longtext,
  `address_2` longtext,
  `city_id` varchar(255) DEFAULT NULL,
  `state_id` varchar(255) DEFAULT NULL,
  `rera_certificate` varchar(255) DEFAULT NULL,
  `conveyance_deed` varchar(255) DEFAULT NULL,
  `conveyance_deed_document` varchar(255) DEFAULT NULL,
  `maintenance_amount` varchar(100) DEFAULT NULL,
  `pin_code` int(11) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_plan_comment` text,
  `offers` text,
  `floor_rise` varchar(255) DEFAULT NULL,
  `parking` varchar(255) DEFAULT NULL,
  `brokerage_fees` double DEFAULT NULL,
  `building_constructed_area` double DEFAULT NULL,
  `building_land_parcel_area` int(11) DEFAULT NULL,
  `building_launch_date` varchar(255) DEFAULT NULL,
  `rera_completion_year` varchar(255) DEFAULT NULL,
  `builder_completion_year` varchar(255) DEFAULT NULL,
  `society_details` longtext,
  `weekly_off` varchar(255) DEFAULT NULL,
  `office_timing` varchar(255) DEFAULT NULL,
  `building_construction_photos` varchar(255) DEFAULT NULL,
  `oc_certificate` varchar(255) DEFAULT NULL,
  `oc_reciving_date` varchar(255) DEFAULT NULL,
  `registration_certificate` varchar(255) DEFAULT NULL,
  `ebrouchure` varchar(255) DEFAULT NULL,
  `pros` longtext,
  `cons` longtext,
  `comment` longtext,
  `building_video_links` varchar(255) DEFAULT NULL,
  `building_config_details` varchar(255) DEFAULT NULL,
  `brokarage_comments` longtext,
  `brokarage_from_date` varchar(255) DEFAULT NULL,
  `brokarage_to_date` varchar(255) DEFAULT NULL,
  `unit_type` varchar(255) DEFAULT NULL,
  `funded_by` varchar(255) DEFAULT NULL,
  `construction_technology` varchar(255) DEFAULT NULL,
  `shifting_charges` varchar(255) DEFAULT NULL,
  `prefered_tenant` varchar(255) DEFAULT NULL,
  `building_description` longtext,
  `building_construction_area` varchar(255) DEFAULT NULL,
  `building_open_space` varchar(255) DEFAULT NULL,
  `gst_certificate` varchar(255) DEFAULT NULL,
  `possession_year` varchar(255) DEFAULT NULL,
  `building_year_as_per_oc` varchar(255) DEFAULT NULL,
  `building_year_as_per_possession` varchar(255) DEFAULT NULL,
  `building_rating` varchar(50) DEFAULT NULL,
  `society_office_address` longtext,
  `working_days_and_time` varchar(255) DEFAULT NULL,
  `occupation_certi_dated` varchar(255) DEFAULT NULL,
  `total_flat_available` int(11) DEFAULT NULL,
  `indian_green_building` varchar(255) DEFAULT NULL,
  `account_manager` varbinary(50) DEFAULT NULL,
  `status` char(1) DEFAULT '1' COMMENT '1= Active , 0=Inactive',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_id`),
  KEY `category_id` (`category_id`(191)),
  KEY `location_id` (`location_id`(191)),
  KEY `sub_location_id` (`sub_location_id`(191)),
  KEY `city_id` (`city_id`(191)),
  KEY `state_id` (`state_id`(191)),
  KEY `payment_type_id` (`payment_type`(191)),
  KEY `building_type_id` (`building_type_id`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society`
--

LOCK TABLES `op_society` WRITE;
/*!40000 ALTER TABLE `op_society` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_account_manager`
--

DROP TABLE IF EXISTS `op_society_account_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_account_manager` (
  `society_account_manager_id` int(11) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_account_manager_id`),
  KEY `society_id` (`society_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `op_society_account_manager_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`),
  CONSTRAINT `op_society_account_manager_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `op_employee` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_account_manager`
--

LOCK TABLES `op_society_account_manager` WRITE;
/*!40000 ALTER TABLE `op_society_account_manager` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_account_manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_amnities`
--

DROP TABLE IF EXISTS `op_society_amnities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_amnities` (
  `society_amnities_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `amenities_id` bigint(20) NOT NULL,
  `description` longtext,
  `is_available` char(1) NOT NULL DEFAULT 'Y',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_amnities_id`),
  KEY `society_id` (`society_id`),
  CONSTRAINT `op_society_amnities_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_amnities`
--

LOCK TABLES `op_society_amnities` WRITE;
/*!40000 ALTER TABLE `op_society_amnities` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_amnities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_brokerage_fee`
--

DROP TABLE IF EXISTS `op_society_brokerage_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_brokerage_fee` (
  `society_brokerage_fee_id` int(11) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `zero_to_two` varchar(255) DEFAULT NULL,
  `three_to_five` varchar(255) DEFAULT NULL,
  `six_to_ten` varchar(255) DEFAULT NULL,
  `above_eleven` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_brokerage_fee_id`),
  KEY `society_id` (`society_id`),
  CONSTRAINT `op_society_brokerage_fee_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_brokerage_fee`
--

LOCK TABLES `op_society_brokerage_fee` WRITE;
/*!40000 ALTER TABLE `op_society_brokerage_fee` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_brokerage_fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_configuration`
--

DROP TABLE IF EXISTS `op_society_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_configuration` (
  `society_confiuration_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `configuration_id` int(11) DEFAULT NULL,
  `suffix` int(11) DEFAULT NULL COMMENT 'configuration size',
  `sample_flat_available` char(1) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `total_falt_available` varchar(255) DEFAULT NULL,
  `flat_available` varchar(255) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_confiuration_id`),
  KEY `society_id` (`society_id`),
  KEY `configuration_id` (`configuration_id`),
  KEY `suffix` (`suffix`),
  CONSTRAINT `op_society_configuration_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`),
  CONSTRAINT `op_society_configuration_ibfk_3` FOREIGN KEY (`configuration_id`) REFERENCES `dd_configuration` (`configuration_id`),
  CONSTRAINT `op_society_configuration_ibfk_4` FOREIGN KEY (`suffix`) REFERENCES `dd_configuration_size` (`configuration_size_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_configuration`
--

LOCK TABLES `op_society_configuration` WRITE;
/*!40000 ALTER TABLE `op_society_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_floor_rise`
--

DROP TABLE IF EXISTS `op_society_floor_rise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_floor_rise` (
  `society_floor_rise_id` int(11) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `floor_slab_from` varchar(255) DEFAULT NULL,
  `floor_slab_to` varchar(255) DEFAULT NULL,
  `configuration` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `rs_per_sq_ft` varchar(255) DEFAULT NULL,
  `box_price` varchar(255) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_floor_rise_id`),
  KEY `society_id` (`society_id`),
  CONSTRAINT `op_society_floor_rise_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_floor_rise`
--

LOCK TABLES `op_society_floor_rise` WRITE;
/*!40000 ALTER TABLE `op_society_floor_rise` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_floor_rise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_keymembers`
--

DROP TABLE IF EXISTS `op_society_keymembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_keymembers` (
  `society_keymembers_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `name_initial` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `country_code_id` int(11) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `whatsapp_number` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `residence_address` longtext,
  `dateupdated` date DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_keymembers_id`),
  KEY `society_id` (`society_id`),
  KEY `country_code_id` (`country_code_id`),
  KEY `designation_id` (`designation_id`),
  CONSTRAINT `op_society_keymembers_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`),
  CONSTRAINT `op_society_keymembers_ibfk_3` FOREIGN KEY (`country_code_id`) REFERENCES `dd_country` (`country_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_keymembers`
--

LOCK TABLES `op_society_keymembers` WRITE;
/*!40000 ALTER TABLE `op_society_keymembers` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_keymembers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_photos`
--

DROP TABLE IF EXISTS `op_society_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_photos` (
  `society_photos_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_photos_id`),
  KEY `society_id` (`society_id`),
  CONSTRAINT `op_society_photos_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_photos`
--

LOCK TABLES `op_society_photos` WRITE;
/*!40000 ALTER TABLE `op_society_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_pros_cons`
--

DROP TABLE IF EXISTS `op_society_pros_cons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_pros_cons` (
  `op_society_pros_cons_id` int(11) NOT NULL AUTO_INCREMENT,
  `pros_of_building` longtext,
  `cons_of_building` longtext,
  `society_id` bigint(20) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`op_society_pros_cons_id`),
  KEY `society_id` (`society_id`),
  CONSTRAINT `op_society_pros_cons_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_pros_cons`
--

LOCK TABLES `op_society_pros_cons` WRITE;
/*!40000 ALTER TABLE `op_society_pros_cons` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_pros_cons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_society_unit_amnities`
--

DROP TABLE IF EXISTS `op_society_unit_amnities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_society_unit_amnities` (
  `society_unit_amnities_id` int(11) NOT NULL AUTO_INCREMENT,
  `society_id` varchar(10) DEFAULT NULL,
  `amenities_id` varchar(10) DEFAULT NULL,
  `description` text,
  `is_available` char(1) NOT NULL DEFAULT 'Y',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_unit_amnities_id`),
  KEY `is_available` (`is_available`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_society_unit_amnities`
--

LOCK TABLES `op_society_unit_amnities` WRITE;
/*!40000 ALTER TABLE `op_society_unit_amnities` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_society_unit_amnities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_unit`
--

DROP TABLE IF EXISTS `op_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_unit` (
  `unit_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) DEFAULT NULL,
  `unit_code` int(11) DEFAULT NULL,
  `custom_unit_code` varchar(255) DEFAULT NULL,
  `wing_id` bigint(20) DEFAULT NULL,
  `refugeeflat` char(5) DEFAULT NULL,
  `direction_id` varchar(10) DEFAULT NULL,
  `dfd` int(11) DEFAULT NULL,
  `unit_type` varchar(50) DEFAULT NULL,
  `unit_type_id` varchar(50) DEFAULT NULL,
  `hall_size` int(11) DEFAULT NULL,
  `balcony` varchar(10) DEFAULT NULL,
  `parking_id` int(11) DEFAULT NULL,
  `floor_no` int(11) DEFAULT NULL,
  `configuration_id` int(11) DEFAULT NULL,
  `configuration_size` int(11) DEFAULT NULL,
  `dryarea_bathroom` int(11) DEFAULT NULL,
  `no_of_bedrooms_with_sizes` int(11) DEFAULT NULL,
  `no_of_bathrooms` int(11) DEFAULT NULL,
  `kitchen_size` int(11) DEFAULT NULL,
  `dryarea_kitchen` int(11) DEFAULT NULL,
  `parking_info` varchar(100) DEFAULT NULL,
  `unit_available` char(1) DEFAULT NULL,
  `refugee_unit` char(1) DEFAULT 'f',
  `unit_view` varchar(255) DEFAULT NULL,
  `show_unit` varchar(255) DEFAULT NULL,
  `dining_area` int(11) DEFAULT NULL,
  `built_up_area` varchar(255) DEFAULT NULL,
  `comment` longtext,
  `build_up_area` varchar(255) DEFAULT NULL,
  `carpet_area` varchar(255) DEFAULT NULL,
  `rera_carpet_area` varchar(255) DEFAULT NULL,
  `mofa_carpet_area` varchar(255) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`unit_id`),
  KEY `society_id` (`society_id`),
  KEY `parking_id` (`parking_id`),
  KEY `configuration_id` (`configuration_id`),
  CONSTRAINT `op_unit_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_unit`
--

LOCK TABLES `op_unit` WRITE;
/*!40000 ALTER TABLE `op_unit` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_unit_bedroom`
--

DROP TABLE IF EXISTS `op_unit_bedroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_unit_bedroom` (
  `bedroom_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_id` bigint(20) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `dimension` varchar(255) DEFAULT NULL,
  `direction_id` int(11) DEFAULT NULL,
  `comment` longtext,
  `area` varchar(1) DEFAULT NULL,
  `selfcontained` char(1) DEFAULT NULL,
  `balcony` char(1) DEFAULT NULL,
  `balcony_area` varchar(1) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`bedroom_id`),
  KEY `unit_id` (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_unit_bedroom`
--

LOCK TABLES `op_unit_bedroom` WRITE;
/*!40000 ALTER TABLE `op_unit_bedroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_unit_bedroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_unit_bedroom_log`
--

DROP TABLE IF EXISTS `op_unit_bedroom_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_unit_bedroom_log` (
  `op_unit_bedroom_log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bedroom_id` varchar(50) NOT NULL,
  `unit_id` varchar(50) NOT NULL,
  `room_id` varchar(50) NOT NULL,
  `view` varchar(255) NOT NULL,
  `dimension` varchar(255) NOT NULL,
  `direction_id` varchar(50) NOT NULL,
  `comment` longtext NOT NULL,
  `area` varchar(50) NOT NULL,
  `selfcontained` varchar(10) NOT NULL,
  `balcony` varchar(50) NOT NULL,
  `balcony_area` varchar(50) NOT NULL,
  `date` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`op_unit_bedroom_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_unit_bedroom_log`
--

LOCK TABLES `op_unit_bedroom_log` WRITE;
/*!40000 ALTER TABLE `op_unit_bedroom_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_unit_bedroom_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_unit_configuration`
--

DROP TABLE IF EXISTS `op_unit_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_unit_configuration` (
  `unit_configuration_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) NOT NULL,
  `configuration_id` bigint(20) NOT NULL,
  `suffix` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `total_flats` int(11) NOT NULL,
  `available_flats` int(11) NOT NULL,
  `sample_flat` varchar(2) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`unit_configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_unit_configuration`
--

LOCK TABLES `op_unit_configuration` WRITE;
/*!40000 ALTER TABLE `op_unit_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_unit_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_unit_photos`
--

DROP TABLE IF EXISTS `op_unit_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_unit_photos` (
  `unit_photos_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`unit_photos_id`),
  KEY `unit_id` (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_unit_photos`
--

LOCK TABLES `op_unit_photos` WRITE;
/*!40000 ALTER TABLE `op_unit_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_unit_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `op_wing`
--

DROP TABLE IF EXISTS `op_wing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `op_wing` (
  `wing_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `wing_name` varchar(100) NOT NULL,
  `total_floors` int(11) NOT NULL,
  `habitable_floors` int(11) NOT NULL,
  `total_units` int(11) DEFAULT NULL,
  `no_of_lifts` int(11) NOT NULL,
  `floor_plan` text,
  `name_board` text,
  `units_per_floor` int(11) DEFAULT NULL,
  `refugee_units` int(11) DEFAULT NULL,
  `comments` longtext,
  `status` char(1) DEFAULT '1' COMMENT '1= Active , 0=Inactive',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`wing_id`),
  KEY `society_id` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `op_wing`
--

LOCK TABLES `op_wing` WRITE;
/*!40000 ALTER TABLE `op_wing` DISABLE KEYS */;
/*!40000 ALTER TABLE `op_wing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_pro_cons`
--

DROP TABLE IF EXISTS `project_pro_cons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_pro_cons` (
  `project_pro_cons_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pros` longtext,
  `cons` longtext,
  `project_id` bigint(20) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`project_pro_cons_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `project_pro_cons_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `op_project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_pro_cons`
--

LOCK TABLES `project_pro_cons` WRITE;
/*!40000 ALTER TABLE `project_pro_cons` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_pro_cons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `society_construction_stage`
--

DROP TABLE IF EXISTS `society_construction_stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `society_construction_stage` (
  `society_construction_stage_id` int(11) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `building_construction_stage` varchar(255) DEFAULT NULL,
  `building_construction_stage_date` varchar(255) DEFAULT NULL,
  `construction_stage_images` varchar(255) DEFAULT NULL,
  `comments_bc` longtext,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_construction_stage_id`),
  KEY `society_id` (`society_id`),
  CONSTRAINT `society_construction_stage_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `society_construction_stage`
--

LOCK TABLES `society_construction_stage` WRITE;
/*!40000 ALTER TABLE `society_construction_stage` DISABLE KEYS */;
/*!40000 ALTER TABLE `society_construction_stage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `society_working_days_time`
--

DROP TABLE IF EXISTS `society_working_days_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `society_working_days_time` (
  `society_working_days_time_id` int(11) NOT NULL AUTO_INCREMENT,
  `society_id` bigint(20) NOT NULL,
  `day_id` int(11) DEFAULT NULL,
  `office_timing` varchar(255) DEFAULT NULL,
  `office_timing_to` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`society_working_days_time_id`),
  KEY `society_id` (`society_id`),
  CONSTRAINT `society_working_days_time_ibfk_1` FOREIGN KEY (`society_id`) REFERENCES `op_society` (`society_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `society_working_days_time`
--

LOCK TABLES `society_working_days_time` WRITE;
/*!40000 ALTER TABLE `society_working_days_time` DISABLE KEYS */;
/*!40000 ALTER TABLE `society_working_days_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-28  4:38:35
