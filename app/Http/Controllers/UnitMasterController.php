<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Traits\DatabaseTrait;

class UnitMasterController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = $this->adminRoles();
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        } 

        if(isset( $request->society_id) && !empty( $request->society_id)){             
            $getData =   DB::select("select s.building_name , w.* from op_wing as w join op_society as s on s.society_id=w.society_id where w.society_id  = ".$request->society_id." "); 
        }else{
            $getData = array();
        }      
        $unit = $this->getAllData('d','unit');
        $room = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');
        return view('unit_master/unit_master',['unit'=>$unit,'getData'=>$getData,'room'=>$room,'direction'=>$direction]);
    }

    public function add_unit_master(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'unit_types' => 'required',        
        ]
     );
     if ($validator->passes()) {

            $data['unit_type'] = $request->unit_types;
            $unit_type_id = '';            
            if(isset($request->unit_type_com) && !empty($request->unit_type_com) && $request->unit_types =='commercial' ){
                $unit_type_id = $request->unit_type_com;
            }

            if(isset($request->unit_type_res) && !empty($request->unit_type_res && $request->unit_types =='residencial')){
                $unit_type_id = $request->unit_type_res;
            }
            
            $data['unit_type_id'] = $unit_type_id;            
            $data['custom_unit_code'] = $request->custom_unit_code;
            $data['configuration_id'] = $request->configuration;
            $data['configuration_size'] = $request->configuration_size;
            

            $build_up_area = $request->build_up_area;
            $carpet_area = $request->carpet_area;
            $rera_carpet_area = $request->rera_carpet_area;
            $mofa_carpet_area = $request->mofa_carpet_area;
            $data['rera_carpet_area'] = $rera_carpet_area;
            $data['mofa_carpet_area'] = $mofa_carpet_area;
            $data['build_up_area'] = $build_up_area;
            $data['carpet_area'] = $carpet_area;        

            $data['direction_id'] = $request->direction_id;         
            $data['balcony'] = $request->balcony;            
            $data['unit_view'] = $request->unit_view;
            $data['show_unit'] = $request->show_unit;
            $data['user_id'] = $this->getUserId();
            
            $refugee_unit =  $request->refugee_unit;

            if($refugee_unit =='f'){
                $unit_available = 't';               
                $getcheck = DB::select("select unit_id from op_unit where unit_id = ".$request->unit_id." and refugee_unit ='t' ");
                if(!empty($getcheck)){
                    DB::statement("UPDATE op_wing SET refugee_units = refugee_units -1 ,total_units = total_units + 1 ,user_id = ".$this->getUserId()."  where wing_id =".$request->wing_id." ");
                }

            }else{
                $unit_available = 'f';              
                DB::statement("UPDATE op_wing SET  refugee_units = refugee_units + 1 ,total_units = total_units - 1 ,user_id = ".$this->getUserId()." where wing_id =".$request->wing_id." ");

            }
            $data['unit_available'] = $unit_available;
            $data['refugee_unit'] = $refugee_unit;
            $data['status'] = $request->status;
            $property_no = $request->property_no;
            $floor_no = $request->floor_no;
            $unit_id = $request->unit_id;
            $bedroom_id = $request->bedroom_id;           
            
            DB::table('op_unit')->where('unit_id',$unit_id)->update($data);       

        return response()->json(['success'=>'done']);
    }
    return response()->json(['error'=>$validator->errors()->all()]);

    }

    public function update_series(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'unit_types' => 'required',       
            ]
        );
        if ($validator->passes()) {

            $data['unit_type'] = $request->unit_types;                    
            
            if(isset($request->unit_type_com) && !empty($request->unit_type_com) && $request->unit_types =='commercial' ){
                $unit_type_id = $request->unit_type_com;
                $data['unit_type_id'] = $unit_type_id;    
            }

            if(isset($request->unit_type_res) && !empty($request->unit_type_res && $request->unit_types =='residencial')){
                $unit_type_id = $request->unit_type_res;
                $data['unit_type_id'] = $unit_type_id;    
            }          

            $data['configuration_id'] = $request->configuration;
            $data['configuration_size'] = $request->configuration_size;
            $data['build_up_area'] = $request->build_up_area;
            $data['carpet_area'] = $request->carpet_area;
            $data['rera_carpet_area'] = $request->rera_carpet_area;
            $data['mofa_carpet_area'] = $request->mofa_carpet_area;
            $data['direction_id'] = $request->direction_id;         
            $data['balcony'] = $request->balcony;            
            $data['unit_view'] = $request->unit_view;
            $data['show_unit'] = $request->show_unit;            
            $data['refugee_unit'] = $request->refugee_unit;
            $data['status'] = $request->status;
            $data['user_id'] = $this->getUserId();
            
                $floor_no = $request->floor_no;
                $unit_no = $request->unit_no;   
                $wing_id = $request->wing_id;

                $refugee_unit =  $request->refugee_unit;

                if($refugee_unit =='f'){
                    $unit_available = 't';
                    // DB::statement("UPDATE op_wing SET refugee_units = refugee_units + 1 ,total_units = total_units - 1 where wing_id =".$request->wing_id." ");
                    if($unit_no=='ALL' && $floor_no=='ALL' ){
                        DB::statement("UPDATE op_wing SET total_units = refugee_units + total_units, refugee_units = 0 ,user_id = ".$this->getUserId()." where wing_id =".$request->wing_id." ");

                    }else{
                        $explode = explode(',',$unit_no);
                        $getcheck = DB::select("select unit_id from op_unit where unit_code in (".$request->unit_no.") and refugee_unit ='t' and wing_id =".$request->wing_id." ");
                       
                        if(!empty($getcheck)){
                            DB::statement("UPDATE op_wing SET refugee_units = refugee_units - ".count($explode)." ,total_units = total_units + ".count($explode)." ,user_id = ".$this->getUserId()." where wing_id =".$request->wing_id." ");
                         
                        }
                    }
                    

                }else{
                    $unit_available = 'f';
                    //update in op_wing
                    if($unit_no=='ALL' && $floor_no=='ALL' ){
                        // echo "in if";die();
                        DB::statement("UPDATE op_wing SET refugee_units = refugee_units + total_units ,total_units = 0 ,user_id = ".$this->getUserId()." where wing_id =".$request->wing_id." ");
                    }else{
                        $cnt = count(explode(',',$unit_no));
                        DB::statement("UPDATE op_wing SET refugee_units = refugee_units + ".$cnt." ,total_units = total_units - ".$cnt." ,user_id = ".$this->getUserId()." where wing_id =".$request->wing_id." ");
                    }

                    
                    
    
                }
            
                $data['unit_available'] = $unit_available;              

                if($unit_no=='ALL' && $floor_no=='ALL' ){                  
                    DB::table('op_unit')->where('wing_id',$wing_id)->update($data);
                    $get = DB::table('op_unit')->where('wing_id',$wing_id)->get();                    
                }else{
                    $Funit_no = explode(',',$unit_no);                    
                    foreach($Funit_no as $val){                    
                        if($val < 100){                           
                            $unit_code = $val * 100;
                        }
                        else{
                            $unit_code = $val;
                        }                        
                        DB::table('op_unit')->where('wing_id',$wing_id)->where('unit_code',$unit_code)->update($data);
                        $get =  DB::select("select * from op_unit where wing_id=".$wing_id." and unit_code =".$val." ");//->where('wing_id',$wing_id)->where('unit_code',$val)->get();                
                        
                    }
                }   
               
            return response()->json(['success'=>'done']);

        }
        return response()->json(['error'=>$validator->errors()->all()]);

    }

    
 

    public function append_unit_structure(Request $request)
    {   

        

        if(isset( $request->wingId) && !empty($request->wingId)){           
            $wing_id = $request->wingId;
            $db = env('operationDB');
            $wingData =   DB::connection($db)->select("select wing_id,society_id,wing_name,total_floors,habitable_floors,total_units,units_per_floor from op_wing  where wing_id  = ".$wing_id." "); 
            $unitData = DB::select("SELECT * FROM `op_unit`  where wing_id=".$wing_id." ORDER BY `floor_no`");
            $getMinFloor = DB::select("SELECT min(floor_no) as  minfloor  FROM `op_unit`  where wing_id=".$wing_id." ");
            $getMaxFlat = DB::select("select max(no_of_flat) as  maxFlat  FROM op_wing_structure  where wing_id=".$wing_id." ");
            $minfloor = $getMinFloor[0]->minfloor;
            $maxFlat =   $getMaxFlat[0]->maxFlat;
            // print_r(  $maxFlat); die();
            // $aa = array();
            // foreach ($unitData as $key => $value) {
            //     array_push($arr,$value->unit_code);
                
            // }           
            $society_id = $wingData[0]->society_id;
            $getSociety = DB::select("select building_name from op_society where society_id =".$society_id." ");
            $building_name = $getSociety[0]->building_name;
            $wing_id = $wingData[0]->wing_id;
            $wing_name = $wingData[0]->wing_name;           
            $total_units = $wingData[0]->total_units;
            $total_floors = $wingData[0]->total_floors;
            $units_per_floor = $wingData[0]->units_per_floor;
            $habitable_floors = $wingData[0]->habitable_floors;           
            
        }else{
            $wing_id = $wing_name = $total_floors =  $total_units =  $units_per_floor = $habitable_floors = $maxFlat = '';
            $unitData = array();
        }    
        $html1 ='<span>Building Name : '.$building_name.' </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span>  Wing :&nbsp;'.$wing_name.'</span> <span style="margin-left: 13%;color:lightslategrey">( Red: Uninhabitable Floors&nbsp;&nbsp;&nbsp;&nbsp;Pink : Refugee Unit&nbsp;&nbsp;&nbsp;&nbsp;Blue : Inhabitable Floors    ) </span>';echo "\n";
        echo $html1;
        $html ='';
        
        for($ii=  $minfloor; $ii<=$total_floors; $ii++ ){

            $floorNo = $ii ;
            if($floorNo >= $habitable_floors){
               // echo 1;
                $color = "orange";
                $bcolor = "";
                $readonly = '';
            }else{
              //  echo 0;
                $color = "red";
                $bcolor = "red";
                $readonly = 'style="pointer-events : none !important; cursor: default !important;"';
            }

            if($floorNo >= $habitable_floors){
                $c = '#000000';
                $bcolor ="";
                $Refugee = '';
            }else{
                $c = 'red';
                $bcolor ="red";
                $Refugee = 'Refugee Unit';
            }
            
            $aa = array();
        foreach ($unitData as $key2 => $value2) {
            if($value2->floor_no == $ii){
            // if($ii != '13'){
                array_push($aa,$value2->unit_code);
            // }
        }
            // break;
        }
        foreach ($unitData as $key => $value) {
           
            // if($ii != '13'){
        
            $html .= '<div class="row" id="unitMasterView">
                        <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                            <input type="text" value="Floor '.$ii.'('.count($aa).') " style="text-align: center;border: solid 1px;color: '.$color.';font-weight: 700;pointer-events : none !important; cursor: default !important;" >
                        </div>';

            // $html .= '
            // <div class="input-field col l1 m4 s12" id="InputsWrapper2">
            //     <input type="text" value="Total :'.count($aa).' " style="text-align: center;border: solid 1px;color: '.$color.';font-weight: 700;" '.$readonly.'>
            // </div>';                        
        // }
            // $html .= '<div class="input-field col l1 m4 s12" id="InputsWrapper2">
            //          <a href="/unit_master_update_info/?property_no=100&amp;wing_id=33 " target="_blank" style="pointer-events : none !important; cursor: default !important;">
            //              <input type="text" value="'.$value->unit_code.'" style="text-align: center;border-color:red;color:red" title="Refugee Unit">               
            //          </a>
            //      </div>';

      
            break;
        // }   
        
    }
    
    foreach ($unitData as $key => $value1) {
        
        if($value1->floor_no == $ii){
            // array_push($arr,$value->unit_code);
            // if($ii != '13'){          
                
                    $html .= '<div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <a href="/unit_master_update_info/?property_no='.$value1->unit_code.'&wing_id='.$wing_id.' "  target="_blank" '.$readonly.' >
                            <input type="text" value="'.$value1->unit_code.'" style="text-align: center;border-color:'.$bcolor.';color:'.$c.'"  title="'.$Refugee.'" >               
                        </a>
                        </div>';
                // }
        }
        
    }
       
    // if($ii != '13'){

        $html .='  </div>';
    // }
       
    }

    for($iii=  $habitable_floors; $iii<=$total_floors; $iii++ ){   
       
        $html .= '<div class="row">
        <div class="input-field col l1 m4 s12" id="InputsWrapper2">                   
                  <a class="waves-effect waves-light btn-small orange " style="padding-left: 35px;padding-right: 35px;" href="javascript:void(0)" onClick=CheckBeforeProceed("/updateSeries?society_id='.$society_id.'&wing_id='.$wing_id.'")  target="_blank">Edit</a> 
               
               </div> ';
             for($i2=1; $i2<=$maxFlat; $i2++ ){ 
                
               $html .= '<div class="input-field col l1 m4 s12" id="InputsWrapper2">
                <a class="waves-effect waves-light btn-small" style="padding-left: 35px;padding-right: 35px;" href="javascript:void(0)" onClick=CheckBeforeProceed("/updateSeries?series='.$i2.'&society_id='.$society_id.'&total_floors='.$total_floors.'&units_per_floor='.$units_per_floor.'&wing_id='.$wing_id.'")   >Edit</a>

               </div>';
               
            }
         
           $html .="</div>";
           break;
            
          
         
        }
          
        // break;
        
    // }
    echo $html;
    die();

//     <div class="input-field col l2 m4 s12" id="InputsWrapper2" style="top: 10px;">
                            
//     <span style="color:#6b6f82;">Total Unit : '.count($aa).'</span>
// </div> 

            if($habitable_floors <= -1){
                $start1 = $habitable_floors;               
            }else{
                $start1 = 0;              
            }
            $end = 0;
           
        $html1 ='<span>Building Name : '.$building_name.' </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span>  Wing :&nbsp;'.$wing_name.'</span> <span style="margin-left: 13%;color:lightslategrey">( Red: Uninhabitable Floors&nbsp;&nbsp;&nbsp;&nbsp;Pink : Refugee Unit&nbsp;&nbsp;&nbsp;&nbsp;Blue : Inhabitable Floors    ) </span>';
        echo $html1;
        for($ii=  $start1; $ii<=$total_floors; $ii++ ){
            // if($habitable_floors == 0){
              //-1;
            //    echo $floorNo ."   ". $habitable_floors;
            // }else{
                // $floorNo = $ii ;//+ $habitable_floors;
            // }-1 >= -1
            $floorNo = $ii ;
            if($floorNo >= $habitable_floors){
               // echo 1;
                $color = "orange";
                $bcolor = "";
                $readonly = "";
            }else{
              //  echo 0;
                $color = "red";
                $bcolor = "red";
                $readonly = 'style="pointer-events : none !important; cursor: default !important;"';
            }

            if($floorNo >= $habitable_floors){
                            $c = '#000000';
                            $bcolor ="";
                            $Refugee = '';
                        }else{
                            $c = 'red';
                            $bcolor ="red";
                            $Refugee = 'Refugee Unit';
                        }
            
            $html ='';
        $html .= '
        <div class="row" id="unitMasterView">
                <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                    <input type="text"   value="Floor '.$floorNo.'"  style="text-align: center;border: solid 1px;color: '.$color.';font-weight: 700;"   >
                
                </div>';

                
                
                for($i=1; $i<=$units_per_floor; $i++ ){ 
                    // if($habitable_floors == 0){
                    //     $un = 100 * $ii + $i ;//-1;
                    //     // echo '<pre>'; print_r( "ii ". $ii . "  i ". $i );
                    //     // echo '<pre>'; print_r( $un);
                    // }else{
                    //     $un = 100 * $ii +$i;// * $habitable_floors -1;                        
                    // }
                    $un = 100 * $ii +$i;

                    if($un < 100){
                        $un1 = $un;
                        $un = 100 *$i;
                    }else{
                        $un1 = $un;
                        $un = $un;
                    }

                    
                    
                    $un = str_pad($un,3, '0', STR_PAD_LEFT);
                    $un1 = str_pad($un1,3, '0', STR_PAD_LEFT);

                    // echo "<pre>"; print_r($un);
                    // echo "<pre>"; print_r($un1);
                   

                    if(in_array($un,$arr)){
                        //#ef4dae
                        $c = '#ef4dae';
                        $bcolor = '#ef4dae';
                        $Refugee = 'Refugee Unit';
                    }else{
                        if($floorNo >= $habitable_floors){
                            $c = '#000000';
                            $bcolor ="";
                            $Refugee = '';
                        }else{
                            $c = 'red';
                            $bcolor ="red";
                            $Refugee = 'Refugee Unit';
                        }
                       
                    }
                 
                //  $ar = json_encode($un.'#'.$wing_id);
                $html .='<div class="input-field col l1 m4 s12" id="InputsWrapper2">
                    <a href="/unit_master_update_info/?property_no='.$un.'&wing_id='.$wing_id.' "  target="_blank" '.$readonly.' >
                        <input type="text"   value="'.$un1.'"  style="text-align: center;border-color:'.$bcolor.';color:'.$c.'"  title="'.$Refugee.'" >               
                </a></div>';
               
                }
                if($floorNo >= $habitable_floors){
                    $html .='
                            <div class="input-field col l2 m4 s12" id="InputsWrapper2" style="top: 10px;">
                            
                                <span style="color:#6b6f82;">Total Unit : '.$units_per_floor.'</span>
                            </div> ';

                  
                 }else{
                //    //  echo 0;
                //      $color = "red";
                //      $bcolor = "red";
                //      $readonly = 'style="pointer-events : none !important; cursor: default !important;"';
                 }

                 $html .= '  </div>';
               
       
        echo  $html;
        // break;
        }
        
        if( $total_floors > 0 ){
        for($ii2=1; $ii2<=$total_floors + 1; $ii2++ ){
         $html = '<div class="row">
         <div class="input-field col l1 m4 s12" id="InputsWrapper2">                   
                   <a class="waves-effect waves-light btn-small orange " style="padding-left: 35px;padding-right: 35px;" href="javascript:void(0)" onClick=CheckBeforeProceed("/updateSeries?society_id='.$society_id.'&wing_id='.$wing_id.'")  target="_blank">Edit</a> 
                
                </div>';
              for($i2=1; $i2<=$units_per_floor; $i2++ ){ 
                 
                $html .= '<div class="input-field col l1 m4 s12" id="InputsWrapper2">
                 <a class="waves-effect waves-light btn-small" style="padding-left: 35px;padding-right: 35px;" href="javascript:void(0)" onClick=CheckBeforeProceed("/updateSeries?series='.$i2.'&society_id='.$society_id.'&total_floors='.$total_floors.'&units_per_floor='.$units_per_floor.'&wing_id='.$wing_id.'")   >Edit</a>

                </div>';
                
             }
              
            $html .= '<div class="input-field col l2 m4 s12" id="InputsWrapper2" style="top: 10px;">             
             <span>Total Unit : '.$total_units.'</span>
                </div> 
            </div>';
            echo  $html;
            break;
           
        }
    }
       
    }

    public function updateSeries(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = $this->adminRoles();
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
        
        $series = $request->series;
        $society_id = $request->society_id;
        $total_floors = $request->total_floors;
        $units_per_floor = $request->units_per_floor;
        $wing_id = $request->wing_id;

        $getWing = DB::select("select  os.building_name, ow.wing_name,ow.comments,ow.habitable_floors   from op_wing as ow
        join op_society as os on os.society_id = ow.society_id
         where ow.wing_id = ".$wing_id." ");
         $getUnits = DB::select("select unit_code from op_unit  where wing_id = ".$wing_id." ");
         $allUnits = array();

         foreach ($getUnits as $key1 => $value1) {
            array_push($allUnits,$value1->unit_code);
         }
        //  echo "<pre>"; print_r($allUnits); die();
        $building_name =  $getWing[0]->building_name;
        $wingName = $getWing[0]->wing_name;
        $wingComment = $getWing[0]->comments;
        $habitable_floors = $getWing[0]->habitable_floors;

        if(isset($series) && !empty($series) && isset($society_id) && !empty($society_id) && isset($total_floors) && !empty($total_floors)  && isset($wing_id) && !empty($wing_id)){
            $property_no = array();
            $FProperty_no = array();
            $floor_no = array();
            for($ii2=abs($habitable_floors); $ii2<=$total_floors   ; $ii2++ ){
                if($ii2 != '13'){
                    $un = 100 * $ii2 + $series ;
                    
                    // $un = str_pad($un,3, '0', STR_PAD_LEFT);
                    if($un < 100){
                        $un = $un * 100;
                        // echo "<pre>"; print_r('if '.$un);
                    }else{
                        $un = $un;
                        // echo "<pre>"; print_r('else '.$un);
                    }
                    
                    if(in_array($un,$allUnits)){
                        array_push( $FProperty_no , $un);
                        array_push( $property_no , $un);
                        array_push( $floor_no , $ii2);     
                    }
                

                    
                   
                }       
            }
            // echo $habitable_floors;
            // echo "<pre>"; print_r(($floor_no)); die();
            if( $habitable_floors <= 0 ){ 
                // echo 1;
                $l = $property_no[0];
                $s = $l /100;
                $p = str_pad($s,3, '0', STR_PAD_LEFT);
                unset($property_no[0]);
                array_push($property_no,$p);
                sort($property_no);
           }
            $Fproperty_no = implode(",",$property_no);   
            $Ffloor_no = implode(",",$floor_no);

           

            // echo "<pre>"; print_r(($FProperty_no)); die();
        }else{
            $Fproperty_no = 'ALL';
            $Ffloor_no = 'ALL';
        }
        // echo "<pre>"; print_r( $Ffloor_no);
        // die();

        $getRegStatus = DB::select("select building_status from op_society where society_id =".$society_id." ");
        if(!empty($getRegStatus)){
            $building_status = $getRegStatus[0]->building_status;
        }else{
            $building_status = '';
        }

        $unit_id = array();
        if(!empty($property_no)){
            foreach ($FProperty_no as $key => $value) {
                $getUnitCode = DB::select("select * from op_unit where unit_code =".$value." and wing_id = ".$wing_id."   ");
                if(!empty($getUnitCode)){
                    array_push($unit_id,$getUnitCode[0]->unit_id);
                }
            }
            $Funit_id = implode(",",$unit_id);
            if(!empty($getUnitCode)){
            $getUnitCode = $getUnitCode[0];
            }
        }else{
            $Funit_id = 'ALL';
            $getUnitCode = '';  
            $FProperty_no = '';
        }
     
        $room = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');       
        $configuration = $this->getAllData('d','configuration');
        $configuration_size = $this->getAllData('d','configuration_size');        
        $parking = $this->getAllData('d','parking');
        $unit = $this->getAllData('d','unit');
        $dd_unit_type_residential = $this->getAllData('d','unit_type_residential');
        $dd_unit_type_commercial = $this->getAllData('d','unit_type_commercial');
        $data = array('property_no'=>$Fproperty_no,'floor_no'=>$Ffloor_no,'wing_id'=>$wing_id,'society_id'=>$society_id,'room'=>$room,'direction'=>$direction,'direction1'=>$direction,'dir'=>$direction,'configuration'=>$configuration,'parking'=>$parking,'unit'=>$unit,'wingName'=>$wingName,'wingComment'=>$wingComment,'configuration_size'=>$configuration_size,'building_status'=>$building_status,'unit_id'=>$Funit_id,'building_name'=>$building_name,
        'dd_unit_type_residential'=>$dd_unit_type_residential,'dd_unit_type_commercial'=>$dd_unit_type_commercial,'getUnitCode'=>$getUnitCode ,'FProperty_no'=>$FProperty_no);        
        
        return view('unit_master/unit_master_update_info_series',$data);   
    }

    
    public function append_unit_structure_side_view(Request $request)
    {       
        $param1 = $request->params1; // wing _id  // floor
        $param2 = $request->params2;
        $room = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');
        $configuration = $this->getAllData('d','configuration');
        $html = '<input type="hidden" value="'.$param1.'"> <input type="hidden" value="'.$param2.'">
        <div class="row" style="margin-right: 0rem !important">
            <div class="input-field col l6 m6 s12 ">
                <select class="validate select2  browser-default"  id="unit_available"  data-placeholder="Select" name="unit_available">
                    <option value="" disabled selected>Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <label for="property_seen" class="active">Unit Available ?</label>
            </div>

            <div class="input-field col l6 m6 s12 ">
                <select class=" validate select2  browser-default"  id="refugee_unit"  data-placeholder="Select" name="refugee_unit" onchange="hide_all_info()">
                    <option value="" disabled selected>Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <label for="property_seen1" class="active">Refugee Unit</label>
            </div>
        </div>
        
        <div id="not_refugee">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Property No.: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="property_no" id="property_no"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Floor No.: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="floor_no" id="floor_no" value="'.$param1.'" readonly placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">RERA Carpet Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="rera_carpet_area" id="rera_carpet_area"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">MOFA Carpet Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="mofa_carpet_area" id="mofa_carpet_area"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important">     
                <div class="input-field col l6 m6 s12 display_search">
                        <select class="validate select2 browser-default" id="direction" data-placeholder="Company name, Branch name" name="direction">
                        <option value="option 1" >Select</option>';
                            foreach($direction as $direction){
                            $html .=' <option value="'.$direction->direction_id .'">'. ucfirst($direction->direction) .'</option>';
                            }
                    $html .='</select>
                    <label class="active">Door Facing Direction</label>
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                    <select class="validate select2 browser-default" id="configuration" data-placeholder="Company name, Branch name" name="configuration">
                        <option value="option 1" >Select</option>';
                            foreach($configuration as $configuration){
                            $html .=' <option value="'.$configuration->configuration_id .'">'. ucfirst($configuration->configuration_name) .'</option>';
                            }
                    $html .='</select>
                    <label class="active">Configuration</label>               
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important">                
                <div class="input-field col l6 m6 s12 ">
                <select class="select2  browser-default" onChange="" id="unit_type"  data-placeholder="Select" name="unit_types">
                    <option value="" disabled selected>Select</option>
                    <option value="yes">Residencial</option>
                    <option value="no">Commercial</option>
                </select>
                <label for="property_seen" class="active">Unit Type</label>
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Build Up Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="build_up_area" id="build_up_area"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important">                
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">No.of Bedrooms with Sizes: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="no_of_bedrooms_with_sizes" id="no_of_bedrooms_with_sizes"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>


                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">No.of Bathrooms: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="no_of_bathrooms" id="no_of_bathrooms"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important"> 
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Hall Size: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="hall_size" id="hall_size"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Kitchen Size: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="kitchen_size" id="kitchen_size"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important"> 
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Parking : <span class="red-text">*</span></label>
                <input type="text" class="validate" name="parking" id="parking"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Parking Info: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="parking_info" id="parking_info"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important"> 
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">    Unit View: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_view" id="unit_view"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="comment" id="comment"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>    
        </div>

            </div>
            <div id="not_refugee1">
            <br>
            <span>ROOM | DIMENTION | DIRECTION</span> <a href="javascript:void(0)" onClick="panel_opening()" > V</a> 
            <br>
            </div>
            ';

    
        echo $html;
    }

    public function viewDientionDirection(Request $request)
    {   
        $increment = $request->aa;
        $room = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');
        $html = '<div class="row"><br><input type="hidden" name="bedroom_id[]" value="0">
        <div class="col l2 m2">
            <select class="validate select2 browser-default" id="room_'.$increment.'"  name="room[]">
            <option value="" >Select</option>';
            foreach($room as $room){
            $html .='<option value="'. $room->room_id .'"> '. ucfirst($room->room_name) .' </option>';
            }
            $html .='</select>    
        </div>
        <div class="col l2 m2">
            <input type="text" class="validate" name="view[]" id="view" style="margin-left: -10px;">
        </div>
        <div class="col l2 m2">
            <input type="text" class="validate" name="dimention[]" id="dimention" style="margin-left: -10px;">
        </div>
        <div class="col l2 m2">
        <select class="validate select2 browser-default" id="direction_'.$increment.'" data-placeholder="Company name, Branch name" name="direction[]">
        <option value="" >Select</option>';
            foreach($direction as $direction){
                $html .='<option value=" '.$direction->direction_id .'">'. ucfirst($direction->direction) .'</option>';
            }
          $html .='</select>
        </div>
        <div class="col l2 m2">
        <a href="#" style="color: red !important;font-size: 23px" class="removeclassViewDirection waves-effect waves-light">-</a>
        </div> 
    </div> ';

        
        echo $html;
    }

    public function unit_master_update_info(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = $this->adminRoles();
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }

        $property_no = $request->property_no; // property_no  // floor
        $wing_id = $request->wing_id;
        if(!empty($wing_id) && !empty($property_no)){
            $getWing = DB::select("select os.building_name ,ow.wing_id,ow.wing_name,ow.comments,ow.total_floors  from op_wing as ow 
            join op_society as os on os.society_id = ow.society_id
            where ow.wing_id = ".$wing_id." ");
            $building_name = $getWing[0]->building_name;
            $wing_id = $getWing[0]->wing_id;
            $wingName = $getWing[0]->wing_name;
            $wingComment = $getWing[0]->comments;
            $wingtotal_floors = $getWing[0]->total_floors;
            $CheckData = DB::select("select * from op_unit  where unit_code=".$property_no." and wing_id=".$wing_id." ");
        }else{
            $CheckData = array();
        }      
        
     
        if(!empty($CheckData)){
            $unit_id = $CheckData[0]->unit_id;
            $custom_unit_code = $CheckData[0]->custom_unit_code;
            $unit_type = $CheckData[0]->unit_type;
            $unit_type_id = $CheckData[0]->unit_type_id;
            $configuration_id = $CheckData[0]->configuration_id;
            $build_up_area = $CheckData[0]->build_up_area;
            $carpet_area = $CheckData[0]->carpet_area;
            $rera_carpet_area = $CheckData[0]->rera_carpet_area;
            $mofa_carpet_area = $CheckData[0]->mofa_carpet_area;
            $configuration_size1 = $CheckData[0]->configuration_size;            
            $direction_id = $CheckData[0]->direction_id;
            $balcony = $CheckData[0]->balcony;
            $status = $CheckData[0]->status;
            $show_unit = $CheckData[0]->show_unit;
            $floor_no = $CheckData[0]->floor_no;
            $kitchen_size = $CheckData[0]->kitchen_size;
            $parking_id = $CheckData[0]->parking_id;
            $parking_info = $CheckData[0]->parking_info;
            $unit_view = $CheckData[0]->unit_view;
            $comment = $CheckData[0]->comment;
            $unit_available = $CheckData[0]->unit_available;
            $refugee_unit = $CheckData[0]->refugee_unit;
            $unit_code = $CheckData[0]->unit_code;
            $society_id = $CheckData[0]->society_id;

            //get building registration status from society
            $getRegStatus = DB::select("select building_status from op_society where society_id =".$society_id." ");
            if(!empty($getRegStatus)){
                $building_status = $getRegStatus[0]->building_status;
            }else{
                $building_status = '';
            }

            $getUnitBedroomData = DB::select("select ub.*,r.room_name, (select direction from  dd_direction  where direction_id = ub.direction_id ) as direction from op_unit_bedroom as ub  join dd_room as r on r.room_id = ub.room_id where ub.unit_id =".$unit_id." order by ub.bedroom_id desc ");
        }else{
            $unit_id = $unit_type = $carpet_area = $rera_carpet_area = $mofa_carpet_area = $direction_id =  $configuration_id ='';
            $unit_type = $build_up_area = $configuration_size1 = $balcony = $status = $floor_no = $show_unit = '';            
            $kitchen_size = $parking_id = $parking_info = $unit_view =  $comment = $unit_available = $refugee_unit = $custom_unit_code = $unit_code = $building_status = $unit_type_id = '';            
            $getUnitBedroomData = array();
        }
        // 

        $room1 = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');
        $unit = $this->getAllData('d','unit');    
        $configuration = $this->getAllData('d','configuration');
        $configuration_size = $this->getAllData('d','configuration_size');        
        $parking = $this->getAllData('d','parking');
        $dd_unit_type_residential = $this->getAllData('d','unit_type_residential');
        $dd_unit_type_commercial = $this->getAllData('d','unit_type_commercial');
        
        $data = array( 'building_name'=>$building_name, 'wing_id'=>$wing_id , 'unit'=>$unit,'unit_id'=>$unit_id,'property_no'=>$property_no,'wing_id'=>$wing_id,'room'=>$room1,'room1'=>$room1,'direction'=>$direction,'direction1'=>$direction,'configuration'=>$configuration,'parking'=>$parking, 'floor_no'=>$floor_no ,'rera_carpet_area'=>$rera_carpet_area, 'mofa_carpet_area'=>$mofa_carpet_area, 'direction_id'=>$direction_id, 'configuration_id'=>$configuration_id,'unit_type'=>$unit_type, 'build_up_area'=>$build_up_area, 'configuration_size'=>$configuration_size, 'balcony'=>$balcony,'status'=>$status,'kitchen_size'=>$kitchen_size, 'parking_id'=>$parking_id ,'parking_info'=>$parking_info,  'unit_view'=>$unit_view, 'comment'=>$comment,'unit_available'=>$unit_available,'refugee_unit'=>$refugee_unit,'getUnitBedroomData'=>$getUnitBedroomData,'wingName'=>$wingName,'wingComment'=>$wingComment,'configuration_size'=>$configuration_size,'configuration_size11'=>$configuration_size1,'show_unit'=>$show_unit,'carpet_area'=>$carpet_area,'custom_unit_code'=>$custom_unit_code,'unit_code'=>$unit_code,'building_status'=>$building_status,
    'wingtotal_floors'=>$wingtotal_floors,'dd_unit_type_commercial'=>$dd_unit_type_commercial,'dd_unit_type_residential'=>$dd_unit_type_residential, 'unit_type_id'=>$unit_type_id);
        
        return view('unit_master/unit_master_update_info',$data);
    }

    public function save_unit(Request $request)
    {
       
        $form_type = $request->form_type;
        if($form_type=='single_unit'){
            $data['unit_id'] = $request->unit_id;
            $data['room_id'] = $request->room;
            $data['view'] = $request->view;
            $data['dimension'] = $request->dimension;
            $data['direction_id'] = $request->direction;
            $data['comment'] = $request->comment;
            $data['user_id'] = $this->getUserId();       
            DB::table('op_unit_bedroom')->insert($data); 
            $data['bedroom_id'] = DB::getPdo()->lastInsertId();
            $getDate = DB::select("select created_date from op_unit_bedroom where bedroom_id = ".DB::getPdo()->lastInsertId()."");
            $data['updated_date'] = $getDate[0]->created_date;            

            if(isset($request->room) && !empty($request->room)){
                $getRoom = DB::select("select room_name from dd_room where room_id = ".$request->room."");
                $data['room_name']= $getRoom[0]->room_name;
            }else{
                $data['room_name']= '';
            }

            if(isset($request->direction) && !empty($request->direction)){
                $getDirection = DB::select("select direction from dd_direction where direction_id = ".$request->direction."");
                $data['direction_name']= $getDirection[0]->direction;
            }else{
                $data['direction_name']= '';
            }
            return response()->json($data);   
        }    

        if($form_type == "multiple_unit"){
            // 
            DB::enableQueryLog();
            $unit_id = $request->unit_id;
            $bedroorm_id = $request->bedroorm_id;
                // echo "<pre>"; print_r($unit_id); die();
                $society_id = $request->society_id1;
                $wing_id = $request->wing_id1;
                $old_unit_id = $request->old_unit_id;
                $old_unit_idU = explode(",",$old_unit_id);

                /*if(isset($bedroorm_id) && !empty($bedroorm_id)){
                    DB::table('op_unit_bedroom')->whereNotIn('unit_id',$old_unit_idU)->delete();   
                }else{
                    DB::table('op_unit_bedroom')->whereIn('unit_id',$old_unit_idU)->delete();   
                }*/
                
                     

                $explodeU = explode(",",$unit_id);
                // echo "<pre>"; print_r($explodeU); die();
                $bedroom_id = array();
                foreach ($explodeU as $key => $value) {
                    // echo "<pre>"; print_r($value);
                    $data = array();
                    $data['unit_id'] = $value; //$request->unit_id;
                    $data['room_id'] = $request->room;
                    $data['view'] = $request->view;
                    $data['dimension'] = $request->dimension;
                    $data['direction_id'] = $request->direction;
                    $data['comment'] = $request->comment;
                    $data['user_id'] = $this->getUserId();       
                    
                    DB::table('op_unit_bedroom')->insert($data); 
                    $a =DB::getPdo()->lastInsertId();

                    array_push($bedroom_id,DB::getPdo()->lastInsertId());
                    $getDate = DB::select("select created_date from op_unit_bedroom where bedroom_id = ".DB::getPdo()->lastInsertId()."");
                    $data['updated_date'] = $getDate[0]->created_date;            

                    if(isset($request->room) && !empty($request->room)){
                        $getRoom = DB::select("select room_name from dd_room where room_id = ".$request->room."");
                        $data['room_name']= $getRoom[0]->room_name;
                    }else{
                        $data['room_name']= '';
                    }

                    if(isset($request->direction) && !empty($request->direction)){
                        $getDirection = DB::select("select direction from dd_direction where direction_id = ".$request->direction."");
                        $data['direction_name']= $getDirection[0]->direction;
                    }else{
                        $data['direction_name']= '';
                    }                    
                    
                }
                $data['bedrom_id'] = $a;
                // $data['allUnitId'] = json_encode($explodeU);
                $data['bedroomId'] = json_encode($bedroom_id);
                return response()->json($data); 

           
        }

        if($form_type == 'multiple_unit_all'){
            // echo"<pre>"; print_r($request->all()); die();
            $society_id = $request->society_id1;
            $wing_id = $request->wing_id1;
            $bedroorm_id = $request->bedroorm_id;
            $old_unit_id = $request->old_unit_id;
            $bedroorm_idU = explode(",",$bedroorm_id);
                if(isset($bedroorm_id) && !empty($bedroorm_id)){
                    DB::table('op_unit_bedroom')->whereNotIn('bedroom_id',$bedroorm_idU)->delete();   
                }else{
                    $getUnitData = DB::select("select unit_id from op_unit where society_id =".$society_id." and wing_id =".$wing_id." ");
                    foreach ($getUnitData as $key => $value) {

                        DB::table('op_unit_bedroom')->where('unit_id',$value->unit_id)->delete();         
                    }
                }

            $wing_id = $request->wing_id;
            $getRecords = DB::select("select unit_id from op_unit where wing_id = ".$wing_id." ");
            // echo "<pre>"; print_r($getRecords);
            $bedroom_id = array();
            foreach ($getRecords as $key => $value) {
                $data['unit_id'] = $value->unit_id; //$request->unit_id;
                $data['room_id'] = $request->room;
                $data['view'] = $request->view;
                $data['dimension'] = $request->dimension;
                $data['direction_id'] = $request->direction;
                $data['comment'] = $request->comment;    
                $data['user_id'] = $this->getUserId();                   
                DB::table('op_unit_bedroom')->insert($data);
                $a =DB::getPdo()->lastInsertId();
                array_push($bedroom_id,DB::getPdo()->lastInsertId());

                $getDate = DB::select("select created_date from op_unit_bedroom where bedroom_id = ".DB::getPdo()->lastInsertId()."");
                $data1['updated_date'] = $getDate[0]->created_date;                 
                $data1['view'] = $request->view;
                $data1['dimension'] = $request->dimension;
                $data1['comment'] = $request->comment; 

                if(isset($request->room) && !empty($request->room)){
                    $getRoom = DB::select("select room_name from dd_room where room_id = ".$request->room."");
                    $data1['room_name']= $getRoom[0]->room_name;
                }else{
                    $data1['room_name']= '';
                }
    
                if(isset($request->direction) && !empty($request->direction)){
                    $getDirection = DB::select("select direction from dd_direction where direction_id = ".$request->direction."");
                    $data1['direction_name']= $getDirection[0]->direction;
                }else{
                    $data1['direction_name']= '';
                }

                
            }

            $data1['bedrom_id'] = $a;
            // $data['allUnitId'] = json_encode($explodeU);
            $data1['bedroomId'] = json_encode($bedroom_id);

            return response()->json($data1); 
        }
    }    

    public function getDataUnitData(Request $request)
    {
        

        $form_type = $request->form_type;
        if($form_type=='single_record'){
            $bedroom_id = $request->bedroom_id;        
            if(isset($bedroom_id) && !empty($bedroom_id)){
                $getData = DB::select("select * from op_unit_bedroom where bedroom_id	 =".$bedroom_id	." ");
                return $getData;
            }
        }

        if($form_type=='multiple_record'){
            $bedroom_id = $request->bedroom_id;        
            $FbedroomId = json_decode($bedroom_id);
            $res = array_reverse($FbedroomId);
            $bedroom_id = $res[0];
            // echo"<pre>"; print_r($bedroom_id); die();
            if(isset($bedroom_id) && !empty($bedroom_id)){
                $getData = DB::select("select * from op_unit_bedroom where bedroom_id	 =".$bedroom_id	." ");
                return $getData;
            }
        }

        
    }

    public function updateTableUnitDetails(Request $request)
    {
      
        $form_type = $request->form_type;
        if($form_type=='single_record'){
            $bedroom_id = $request->bedroom_id;
            if(isset($bedroom_id) && !empty($bedroom_id)){
                $checkData = DB::select("select bedroom_id from op_unit_bedroom where  bedroom_id =".$bedroom_id." ");
                if($checkData){
                    $data['room_id'] = $request->room;
                    $data['view'] = $request->view;
                    $data['dimension'] = $request->dimension;
                    $data['direction_id'] = $request->direction;
                    $data['comment'] = $request->comment;
                    $data['user_id'] = $this->getUserId();       
                    DB::table('op_unit_bedroom')->where('bedroom_id',$bedroom_id)->update($data);
                    $getDate = DB::select("select created_date from op_unit_bedroom where bedroom_id = ".$bedroom_id."");
                    $data['updated_date'] = $getDate[0]->created_date; 

                    if(isset($request->room) && !empty($request->room)){
                        $getRoom = DB::select("select room_name from dd_room where room_id = ".$request->room."");
                        $data['room_name']= $getRoom[0]->room_name;
                    }else{
                        $data['room_name']= '';
                    }
        
                    if(isset($request->direction) && !empty($request->direction)){
                        $getDirection = DB::select("select direction from dd_direction where direction_id = ".$request->direction."");
                        $data['direction_name']= $getDirection[0]->direction;
                    }else{
                        $data['direction_name']= '';
                    }
                    
                    return response()->json(['success'=>'Record updated successfully.','bedroom_id'=>$bedroom_id,'data'=>$data]);   


                }

            }
        }

        if($form_type=='multiple_record'){
            
            $bedroom_id = $request->bedroom_id;
            $explodeB = explode(",",$bedroom_id);

            foreach ($explodeB as $key => $value) {
                $data['room_id'] = $request->room;
                $data['view'] = $request->view;
                $data['dimension'] = $request->dimension;
                $data['direction_id'] = $request->direction;
                $data['comment'] = $request->comment;
                $data['user_id'] = $this->getUserId();       
                DB::table('op_unit_bedroom')->where('bedroom_id',$value)->update($data);
                $getDate = DB::select("select created_date from op_unit_bedroom where bedroom_id = ".$value."");
                $data1['updated_date'] = $getDate[0]->created_date;                 
                $data1['view'] = $request->view;
                $data1['dimension'] = $request->dimension;
                $data1['comment'] = $request->comment; 
                $data1['bedroom_id'] = $value;    

                if(isset($request->room) && !empty($request->room)){
                    $getRoom = DB::select("select room_name from dd_room where room_id = ".$request->room."");
                    $data1['room_name']= $getRoom[0]->room_name;
                }else{
                    $data1['room_name']= '';
                }
    
                if(isset($request->direction) && !empty($request->direction)){
                    $getDirection = DB::select("select direction from dd_direction where direction_id = ".$request->direction."");
                    $data1['direction_name']= $getDirection[0]->direction;
                }else{
                    $data1['direction_name']= '';
                }
            }
            // 
            return response()->json(['success'=>'Record updated successfully.','bedroom_id'=>$value,'data'=>$data1]);   
        }

      
    }

    public function deleteUnitFormRecords(Request $request)
    {        
       
            $form_type = $request->form_type;
            if($form_type=='single_record'){
                $bedroom_id = $request->bedroom_id;
                $check = DB::select("select * from op_unit_bedroom where bedroom_id =".$bedroom_id." ");            
                if($check){
                    DB::table('op_unit_bedroom')->where('bedroom_id',$bedroom_id)->delete(); 
                    return response()->json(['success'=>'Record deleted successfully.','bedroom_id'=>$bedroom_id]);   
                }else{
                    return response()->json(['error'=>'Record does not exist']);
                } 
            }

            if($form_type == "multiple_record"){
                $bedroom_id = $request->bedroom_id;
                $FbedroomId = json_decode($bedroom_id);
                   
                foreach ($FbedroomId as $key => $value) {
                    DB::table('op_unit_bedroom')->where('bedroom_id',$value)->delete(); 
                }
                return response()->json(['success'=>'Record deleted successfully.','bedroom_id'=>$value]);   
            }
    }


    
    public function getUnitConfigDetails(Request $request)
    {        
        $wingId = $request->wingId;
        $getData = DB::select("select s.building_name,w.wing_name,u.unit_code,c.configuration_name, u.carpet_area,u.build_up_area, cs.configuration_size,u.balcony,d.direction,u.unit_view,u.unit_type,u.status,u.rera_carpet_area,u.mofa_carpet_area
        from op_unit as u 
        join dd_configuration as c on c.configuration_id=u.configuration_id
        join dd_configuration_size as cs on cs.configuration_size_id=u.configuration_size
        join op_society as s on s.society_id=u.society_id
        join op_wing as w on w.wing_id=u.wing_id
        join dd_direction as d on d.direction_id=u.direction_id
        where w.wing_id= ".$wingId."  group by s.building_name,w.wing_name,u.unit_code,c.configuration_name, u.carpet_area,u.build_up_area, cs.configuration_size,u.balcony,d.direction,u.unit_view,u.unit_type,u.status,u.rera_carpet_area,u.mofa_carpet_area order by u.unit_code asc");


        $tid1 = $tid2 = $tid3 = $tid4 = $tid5 = array();
            $kid1 = $kid2 = $kid3 = $kid4 = $kid5 = array();
            foreach ($getData as $key => $value) {
                
                if(in_array($value->configuration_name,$tid1) == false){
                    array_push($tid1,$value->configuration_name);
                    array_push($kid1,$key);
                }

                if( in_array($value->carpet_area,$tid2) == false ){
                    array_push($tid2,$value->carpet_area);
                    if(!empty($value->carpet_area)){
                    
                        array_push($kid2,$key);
                    }  
                }
                
                if( in_array($value->build_up_area,$tid3) == false ){
                    array_push($tid3,$value->build_up_area);
                    if(!empty($value->build_up_area)){
                        array_push($kid3,$key);
                    }   
                }

                if( in_array($value->rera_carpet_area,$tid4) == false ){
                    array_push($tid4,$value->rera_carpet_area);
                    if(!empty($value->rera_carpet_area)){
                        array_push($kid4,$key);
                    }   
                }
                
                if( in_array($value->mofa_carpet_area,$tid5) == false ){
                    array_push($tid5,$value->mofa_carpet_area);
                    if(!empty($value->mofa_carpet_area)){
                        array_push($kid5,$key);
                    }   
                }

            }    
           
            $finalA = array_unique(array_merge($kid1,$kid2,$kid3,$kid4,$kid5));            

            $filterData = array();
            foreach ($getData as $key1 => $value1) {
                if(in_array($key1,$finalA)){
                        
                        array_push($filterData,$value1);
                }
            }        

        if(!empty($filterData)){
        $html = '';
        $html .= 'Building Name : '.$filterData[0]->building_name.'  &nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;  Wing Name : '.$filterData[0]->wing_name.'
                <table class="bordered" >
                    <thead>
                    <tr style="color: white;background-color: #ffa500d4;">              
                        <th>Unit No</th>
                        <th>Configuration</th>
                        <th>Area</th>
                        <th>Configuration Size</th>
                        <th>Balcony</th>
                        <th>Direction</th>
                        <th>Unit View</th>
                        <th>Unit Type</th>
                        <th>Unit Status</th>
                    </tr>
                </thead>
                 <tbody>';
                

        foreach ($filterData as $key => $value) {
            $html .= "<tr>
                <td>".$value->unit_code."</td>
                <td>".$value->configuration_name."</td>
                <td>";
                 if( !empty($value->carpet_area) ){
                    $html .=  'Carpet Area :'. $value->carpet_area;
                        }
                        $html .=   "<br>";
                        if( !empty($value->build_up_area	) ){
                            $html .=  "Build Up Area :".  $value->build_up_area;
                        }

                        // if( !empty($value->rera_carpet_area) ){
                        //    $html .=  "Rera Carpet Area :". $value->rera_carpet_area;
                        // }
                             $html .=  "<br>";
                        if( !empty($value->mofa_carpet_area) ){
                           $html .=  "Rera Carpet Area : ". $value->mofa_carpet_area;
                        }

                $html .=    "</td>
                <td>".ucwords($value->configuration_size)."</td>
                <td>";
                        if($value->balcony=='t'){
                            $html .= "Yes";
                        }else{
                            $html .= "No";
                        }
                $html .= "</td>
                <td>". ucwords($value->direction) ."</td>
                <td>".$value->unit_view."</td>
                <td>". ucwords($value->unit_type)."</td>
                <td>".$value->status."</td>
            <tr>";
            
        }

        $html .='</tbody>   
                </table>';

        echo  $html;        
    }else{
        echo  $html = "No data found";        
    }
    }

    public function add_configuration(Request $request)
    {
        // echo "<pre>"; print_r($_POST);
        $validator = Validator::make($request->all(), [
            'add_configuration' => 'required'                    
        ],
        [
            'add_configuration.required' => 'Configuration size is required',
            
        ]);  
        if ($validator->passes()) {

            $add_configuration = $request->add_configuration;

            $checkExist = DB::select("select configuration_size from dd_configuration_size where configuration_size ='".trim($add_configuration)."' ");
            if(!empty($checkExist)){
                //return already exist
                $ary = array("Configuration size is already exist");
                return response()->json(['error'=>$ary]);
            }else{
                DB::table('dd_configuration_size')->insert(array('configuration_size'=>$add_configuration));
                $lastInsertId = DB::getPdo()->lastInsertId();  
                $a = array('value'=>$lastInsertId,'option'=>$add_configuration);

                return response()->json(['success'=>'Added new records.','a'=>$a]);	
            }
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    public function add_room(Request $request)
    {      
        $validator = Validator::make($request->all(), [
            'add_room' => 'required'                    
        ],
        [
            'add_room.required' => 'Configuration size is required',
            
        ]);  
        if ($validator->passes()) {

            $add_room = $request->add_room;

            $checkExist = DB::select("select room_name from dd_room where room_name ='".trim($add_room)."' ");
            if(!empty($checkExist)){
               
                $ary = array("Room is already exist");
                return response()->json(['error'=>$ary]);
            }else{
                DB::table('dd_room')->insert(array('room_name'=>$add_room));
                $lastInsertId = DB::getPdo()->lastInsertId();  
                $a = array('value'=>$lastInsertId,'option'=>$add_room);

                return response()->json(['success'=>'Added new records.','a'=>$a]);	
            }
        }
        return response()->json(['error'=>$validator->errors()]);
    }

}
