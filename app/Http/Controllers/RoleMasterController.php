<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;
use DateTime;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\AccessGroupHelper;
use Illuminate\Support\Facades\Crypt;

class RoleMasterController extends Controller
{
    // Role View

    public function role_master()
    {
        $user = Auth::user();  
        // echo "<pre>"; print_r($user); die();
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }


        $role = $this->getRole();
        $roles = array('super_admin');
        // if(!in_array($role,$roles)){
        //     Auth::logout();
        //     return redirect('login');
        // }

        /************* Role Check Start  *********************/
        $condition_name = 'roles';
        $page_action = 's_view';
        $action_status = 1;
        $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

        $data['access_check'] = $access_check;

        /************* Role Check End  *********************/

        if(!empty($access_check)){

            $role_name = $user->role;
            $role_id = $user->role_id;

            if($role_name=='developer'){
                $data['getRoleData'] = DB::select("SELECT id,role_name,status,updated_at FROM `roles` WHERE del_status='1' ");
            }elseif($role_name=='super_admin'){
                $data['getRoleData'] = DB::select("SELECT id,role_name,status,updated_at FROM `roles` WHERE session_name<>'developer' AND session_name<>'super_admin' AND del_status='1' ");
            }elseif($role_name=='admin'){
                $data['getRoleData'] = DB::select("SELECT id,role_name,status,updated_at FROM `roles` WHERE session_name<>'developer' AND session_name<>'super_admin' AND session_name<>'admin' AND del_status='1' ");
            }elseif($role_name=='executive'){
                $data['getRoleData'] = DB::select("SELECT id,role_name,status,updated_at FROM `roles` WHERE session_name<>'developer' AND session_name<>'super_admin' AND session_name<>'admin' AND session_name<>'executive' AND del_status='1' ");
            }else{
                $data['getRoleData'] = [];
            }

            return view('role_master/role_list',$data);

        }else{
            return Redirect()->back()->with('error','You do not have access to this page.');
        }
    }

    // Add User View
    public function add_role_access_level(Request $request)
    {
        // return $request;
        
        $user = Auth::user();  
        // echo "<pre>"; print_r($user); die();
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }

        $role = $this->getRole();
        $roles = array('super_admin');
        // if(!in_array($role,$roles)){
        //     Auth::logout();
        //     return redirect('login');
        // }

        /************* Role Check Start  *********************/
        $condition_name = 'roles';
        $page_action = 's_add';

        $action_status = 1;
        $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

        /************* Role Check End  *********************/
        $role_name = $user->role;

        if($role_name=='super_admin' || $role_name=='admin'|| $role_name=='executive'){
            if($request->role_id=='4'){
                return redirect('role_master');
            }
        }

        if(!empty($access_check)){
            $role_id = $request->role_id;

            // if(Crypt::decryptString($request->role_id)){
            //     $role_id = Crypt::decryptString($request->role_id);
            // }else{
            //     return Redirect()->back()->with('error','You do not have access to this page');
            // }
            

            $data['getRoleData'] = DB::select("SELECT id, role_name, session_name FROM `roles` WHERE id='$role_id' AND del_status='1' ");
                $data['getRoleMainAccessData'] = DB::select("SELECT main_access_level_id, page_name, condition_name FROM `role_main_access_level` WHERE del_status='1' ");
            
            $data['sub_access_data'] = [];
            foreach($data['getRoleMainAccessData'] as $rmkey=>$rmvalue){
                $main_access_level_id = $rmvalue->main_access_level_id;
                $page_name = $rmvalue->page_name;
                $condition_name = $rmvalue->condition_name;
                $sub_access_data = DB::select("SELECT sub_access_level_id, main_access_level_id, role_id, s_view, s_add, s_edit, s_delete FROM `role_sub_access_level` WHERE main_access_level_id='$main_access_level_id' AND role_id='$role_id'");
                if(count($sub_access_data)>0){
                    $data['sub_access_data'][$rmkey] = $sub_access_data;
                }
            }

            return view('role_master/add_role_view',$data);

        }else{
            return Redirect()->back()->with('error','You do not have access to this page');
        }
    }

// save Role Access Group

    public function saveRoleAccessGroup(Request $request)
    {
        // return $request;
        
        // return count($buyer_array_data);/
        // return $buyer_array_data[0];
        // return public_path('');

        $user = Auth::user();  
        // echo "<pre>"; print_r($user); die();
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }

        $role = $this->getRole();
        $roles = array('super_admin');
        // if(!in_array($role,$roles)){
        //     Auth::logout();
        //     return redirect('login');
        // }
            

        $role_id = $request->role_id;
        $access_level_group = $request->access_level_group;
        $roleAccessData['role_id'] = $role_id;

        if( isset($request->access_level_group) ){

            if($request->access_level_group!=null){

                $access_array_data =  explode(",",$request->access_level_group);
                $access_array_count =  count($access_array_data);

                for($i=0;$i < $access_array_count; $i++){
                        $access_array_data2 = explode("--",$access_array_data[$i]);
                    $main_access_level_id = $access_array_data2[1];
                    $action = $access_array_data2[2];
                    $status = $access_array_data2[3];
                    $roleAccessData['main_access_level_id'] = $main_access_level_id;
                        $roleAccessData['s_'.$action] = $status;
                    // return $roleAccessData;
                    $checkExists = DB::table('role_sub_access_level')
                    ->where('main_access_level_id',$main_access_level_id)
                    ->where('role_id',$role_id)
                    ->get();

                    if(count($checkExists)>0){  }else{
                        DB::table('role_sub_access_level')
                        ->insert($roleAccessData);
                    }

                    DB::table('role_sub_access_level')->where('role_id',$role_id)->where('main_access_level_id',$main_access_level_id)
                    ->update($roleAccessData);
                

                }
                return response()->json(['success'=>'Role Access Group Added Successfully']);

            }else{
                DB::table('role_sub_access_level')->where('role_id',$role_id)
                ->update(['s_view'=>0,'s_add'=>0,'s_edit'=>0,'s_delete'=>0]);

                return response()->json(['success'=>'Role Access Group Added Successfully']);
            }
        
        }else{
            return response()->json(['success'=>'Data does not exists']);
        }

        
    }

// dashboard view
    // Add User View
    public function dashboard(Request $request)
    {
        // return $request;
        
        $user = Auth::user();  
        // echo "<pre>"; print_r($user); die();
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }

        /************* Role Check Start  *********************/
        $condition_name = 'roles';
        $page_action = 's_add';

        $action_status = 1;
        $condition_name_buyer = 'buyer';
        $condition_name_tenant = 'tenant';
        $condition_name_property_consultant = 'property_consultant';
        $condition_name_building_master = 'building_master';
        $condition_name_users = 'users';
        $condition_name_roles = 'roles';
        $access_check_buyer = AccessGroupHelper::globalAccessGroupVar($condition_name_buyer,$page_action,$action_status);
        $access_check_tenant = AccessGroupHelper::globalAccessGroupVar($condition_name_tenant,$page_action,$action_status);
        $access_check_property_consultant = AccessGroupHelper::globalAccessGroupVar($condition_name_property_consultant,$page_action,$action_status);
        $access_check_building_master = AccessGroupHelper::globalAccessGroupVar($condition_name_building_master,$page_action,$action_status);
        $access_check_users = AccessGroupHelper::globalAccessGroupVar($condition_name_users,$page_action,$action_status);
        $access_check_roles = AccessGroupHelper::globalAccessGroupVar($condition_name_roles,$page_action,$action_status);

        $data['access_check_buyer'] = $access_check_buyer;
        $data['access_check_tenant'] = $access_check_tenant;
        $data['access_check_property_consultant'] = $access_check_property_consultant;
        $data['access_check_building_master'] = $access_check_building_master;
        $data['access_check_users'] = $access_check_users;
        $data['access_check_roles'] = $access_check_roles;
        /************* Role Check End  *********************/

        // return view('dashboard1');
        return view('dashboard1',$data);
    }



}
