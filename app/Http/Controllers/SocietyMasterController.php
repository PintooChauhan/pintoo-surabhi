<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Traits\DatabaseTrait;

class SocietyMasterController extends Controller
{
    use DatabaseTrait;
    public function index(Request $request)
    {   
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = $this->adminRoles();
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }

        if(isset( $request->project_id) && !empty( $request->project_id)){
            $project_id = $request->project_id;
            $project_amenities =   DB::select("select pm.*,am.amenities  from op_project_amenities as pm join dd_project_amenities as am on am.project_amenities_id = pm.amenities_id  where pm.project_id = ".$project_id." "); 
            $getProjectData = DB::select("select * from op_society where project_id =".$project_id." ");
            

            $getProjectSublocation = DB::select("select sub_location_id,sales_office_address from op_project where project_id =".$project_id." ");
            if(!empty($getProjectSublocation)){
                $sub_location_idP = $getProjectSublocation[0]->sub_location_id;
                $sales_office_addressP = $getProjectSublocation[0]->sales_office_address;
            }else{
                $sub_location_idP = '';
                $sales_office_addressP = '';
            }
           

        }elseif(isset( $request->society_id) && !empty( $request->society_id)){       
            $society_id = $request->society_id;
            $getProjectData = DB::select("select * from op_society where society_id =".$society_id." ");  
            $project_amenities = array();            
            $project_id ='';            
        }else{
            
            $project_id =  $request->p_id;
            $getProjectData = array();
            $getProjectSublocation = DB::select("select sub_location_id,sales_office_address from op_project where project_id =".$project_id." ");
            if(!empty($getProjectSublocation)){
                
                $sub_location_idP = $getProjectSublocation[0]->sub_location_id;
                $sales_office_addressP = $getProjectSublocation[0]->sales_office_address;
            }else{
               
                $sub_location_idP = '';
                $sales_office_addressP = '';
            }
            $project_amenities =   DB::select("select pm.*,am.amenities  from op_project_amenities as pm join dd_project_amenities as am on am.project_amenities_id = pm.amenities_id  where pm.project_id = ".$project_id." "); 
        }
      
        if($getProjectData){  
           
            $society_id = $getProjectData[0]->society_id;
            $project_id = $getProjectData[0]->project_id;
            $building_status = $getProjectData[0]->building_status;
            $building_code = $getProjectData[0]->building_code;
            $building_owner = $getProjectData[0]->building_owner;
            $building_name = $getProjectData[0]->building_name;
            $unit_status = $getProjectData[0]->unit_status;
            $no_of_wings = $getProjectData[0]->no_of_wings;
            $category_id = $getProjectData[0]->category_id;
            $society_status_id = $getProjectData[0]->society_status_id;
            $building_type_id = $getProjectData[0]->building_type_id;
            $lattitde = $getProjectData[0]->lattitde;
            $longitude = $getProjectData[0]->longitude;
            $location_id = $getProjectData[0]->location_id;
            $sub_location_id = $getProjectData[0]->sub_location_id;
            $address_1 = $getProjectData[0]->address_1;
            $address_2 = $getProjectData[0]->address_2;
            $city_id = $getProjectData[0]->city_id;
            $state_id = $getProjectData[0]->state_id;
            $rera_certificate = $getProjectData[0]->rera_certificate;
            $conveyance_deed = $getProjectData[0]->conveyance_deed;
            $conveyance_deed_document = $getProjectData[0]->conveyance_deed_document;
            $maintenance_amount = $getProjectData[0]->maintenance_amount;
            $pin_code = $getProjectData[0]->pin_code;
            $payment_type1 = $getProjectData[0]->payment_type;
            $offers = $getProjectData[0]->offers;
            $floor_rise = $getProjectData[0]->floor_rise;
            $parking = $getProjectData[0]->parking;
            $brokerage_fees = $getProjectData[0]->brokerage_fees;
            $building_constructed_area = $getProjectData[0]->building_constructed_area;           
            $building_land_parcel_area = $getProjectData[0]->building_land_parcel_area;
            $building_launch_date = $getProjectData[0]->building_launch_date;
            $rera_completion_year = $getProjectData[0]->rera_completion_year;
            $builder_completion_year = $getProjectData[0]->builder_completion_year;
            $society_details = $getProjectData[0]->society_details;
            $weekly_off = $getProjectData[0]->weekly_off;
            $office_timing = $getProjectData[0]->office_timing;
            $building_construction_photos = $getProjectData[0]->building_construction_photos;
            $oc_certificate = $getProjectData[0]->oc_certificate;
            $oc_reciving_date = $getProjectData[0]->oc_reciving_date;
            $registration_certificate = $getProjectData[0]->registration_certificate;
            $ebrouchure = $getProjectData[0]->ebrouchure;
            $pros = $getProjectData[0]->pros;
            $cons = $getProjectData[0]->cons;
            $comment = $getProjectData[0]->comment;
            $building_video_links = $getProjectData[0]->building_video_links;
            $building_config_details = $getProjectData[0]->building_config_details;
            $brokarage_comments = $getProjectData[0]->brokarage_comments;
            $brokarage_from_date = $getProjectData[0]->brokarage_from_date;
            $brokarage_to_date = $getProjectData[0]->brokarage_to_date;
            $unit_type = $getProjectData[0]->unit_type;
            $funded_by = $getProjectData[0]->funded_by;
            $construction_technology = $getProjectData[0]->construction_technology;
            $shifting_charges = $getProjectData[0]->shifting_charges;
            $prefered_tenant = $getProjectData[0]->prefered_tenant;
            $building_description = $getProjectData[0]->building_description;
            $building_construction_area = $getProjectData[0]->building_construction_area;
            $building_open_space = $getProjectData[0]->building_open_space;
            $gst_certificate = $getProjectData[0]->gst_certificate;
            $possession_year = $getProjectData[0]->possession_year;
            $building_year_as_per_possession = $getProjectData[0]->building_year_as_per_possession;
            $building_year_as_per_oc = $getProjectData[0]->building_year_as_per_oc;
            $building_rating = $getProjectData[0]->building_rating;
            $shifting_charges = $getProjectData[0]->shifting_charges;
            $prefered_tenant = $getProjectData[0]->prefered_tenant;
            $society_office_address = $getProjectData[0]->society_office_address;
            $working_days_and_time = $getProjectData[0]->working_days_and_time;
            $office_timing = $getProjectData[0]->office_timing;
            $occupation_certi_dated = $getProjectData[0]->occupation_certi_dated;
            $total_flat_available = $getProjectData[0]->total_flat_available;
            $maintenance_amount = $getProjectData[0]->maintenance_amount;
            $indian_green_building = $getProjectData[0]->indian_green_building;

            $project_amenities =   DB::select("select pm.*,am.amenities  from op_project_amenities as pm join dd_project_amenities as am on am.project_amenities_id = pm.amenities_id  where pm.project_id = ".$project_id." "); 
            $aid = array();
            foreach($project_amenities as $val){
              array_push($aid,$val->amenities_id);
            }
            $im = implode(",",$aid);
            if(!empty($im)){
                $project_amenities_society = DB::select("select  * from dd_project_amenities where project_amenities_id not in (".$im.") ");
            }else{
                $project_amenities_society = array();
            }

            $society_amnities = DB::select("select pm.*,am.amenities  from op_society_amnities as pm join dd_project_amenities as am on am.project_amenities_id = pm.amenities_id  where pm.society_id=".$society_id." ");
            $unit_amnities = DB::select("select pm.*,am.amenities  from op_society_unit_amnities as pm join dd_unit_amenities as am on am.unit_amenities_id = pm.amenities_id  where pm.society_id=".$society_id." ");

            $getCntUnit = DB::select("select sum(total_units) as unitCount from op_wing where society_id=".$society_id."  and status='1'");
            $unitCount = $getCntUnit[0]->unitCount;
            
            $getProjectSublocation = DB::select("select sub_location_id,sales_office_address from op_project where project_id =".$project_id." ");
            if(!empty($getProjectSublocation)){
                $sub_location_idP = $getProjectSublocation[0]->sub_location_id;
                $sales_office_addressP = $getProjectSublocation[0]->sales_office_address;
            }else{
                $sub_location_idP = '';
                $sales_office_addressP = '';
            }
            
            $checkUnitData = DB::select("select unit_id  from op_unit where society_id=".$society_id." ");
            if(!empty($checkUnitData)){
                // unit_view
                $unitData = DB::select("select s.building_status,s.building_name,w.wing_name,u.unit_id,u.unit_code,
                (select configuration_name from dd_configuration where  configuration_id=u.configuration_id ) as configuration_name ,
                (select configuration_size from dd_configuration_size  where  configuration_size_id=u.configuration_size ) as configuration_size  ,
                u.carpet_area,u.build_up_area, u.floor_no,
                u.balcony,
                (select direction from dd_direction where direction_id=u.direction_id ) as direction,
                u.unit_view,u.show_unit,u.unit_type,u.status,u.rera_carpet_area,u.mofa_carpet_area
                                from op_unit as u                
                                join op_society as s on s.society_id=u.society_id
                                join op_wing as w on w.wing_id=u.wing_id
                                where u.society_id= ".$society_id." 
                                group by s.building_status, u.configuration_id,u.carpet_area,u.rera_carpet_area,u.mofa_carpet_area,s.building_name,w.wing_name,u.unit_id,u.unit_code,
                                u.build_up_area,u.balcony,u.unit_view,u.show_unit,u.unit_type,u.status,configuration_name,configuration_size,direction,u.floor_no  order by w.wing_name,u.unit_code asc ");
            }else{
                $unitData = array();
            }
            
           
            $tid1 = $tid2 = $tid3 = $tid4 = $tid5 = array();
            $kid1 = $kid2 = $kid3 = $kid4 = $kid5 = array();
            foreach ($unitData as $key => $value) {
                
                if(in_array($value->configuration_name,$tid1) == false){
                    array_push($tid1,$value->configuration_name);
                    if(!empty($value->configuration_name)){                      
                        array_push($kid1,$key);
                    }  
                  
                }

                if( in_array($value->carpet_area,$tid2) == false ){
                    array_push($tid2,$value->carpet_area);
                    if(!empty($value->carpet_area)){
                    
                        array_push($kid2,$key);
                    }  
                }
                
                if( in_array($value->build_up_area,$tid3) == false ){
                    array_push($tid3,$value->build_up_area);
                    if(!empty($value->build_up_area)){
                        array_push($kid3,$key);
                    }   
                }

                if( in_array($value->rera_carpet_area,$tid4) == false ){
                    array_push($tid4,$value->rera_carpet_area);
                    if(!empty($value->rera_carpet_area)){
                        array_push($kid4,$key);
                    }   
                }
                
                if( in_array($value->mofa_carpet_area,$tid5) == false ){
                    array_push($tid5,$value->mofa_carpet_area);
                    if(!empty($value->mofa_carpet_area)){
                        array_push($kid5,$key);
                    }   
                }

            }              

            $finalA = array_unique(array_merge($kid1,$kid2,$kid3,$kid4,$kid5));            
           
           
            $filterData = array();
            foreach ($unitData as $key1 => $value1) {
                if(in_array($key1,$finalA)){
                        
                        array_push($filterData,$value1);
                }
            }
         
            $getAccountManagerData = DB::select("select * from op_society_account_manager where society_id = ".$society_id."");    
            if(!empty($getAccountManagerData)){
                $employee_id = $getAccountManagerData[0]->employee_id;                   
            }else{
                $employee_id = '';                    
            }  
            
            $getSocietyConfigData = DB::select("select * from op_society_configuration where society_id = ".$society_id."");    
            if(!empty($getSocietyConfigData)){
                $getSocietyConfigData = $getSocietyConfigData;    
            }else{
                $getSocietyConfigData = array();
            }  

            $getConstructionStage = DB::select("select ( select dcs.construction_stage from dd_construction_stage as dcs where dcs.construction_stage_id = ss.building_construction_stage   ) as construction_stage ,ss.* from society_construction_stage as ss 
            where society_id = ".$society_id."");    
            if(!empty($getConstructionStage)){
                $getConstructionStage = $getConstructionStage;    
            }else{
                $getConstructionStage = array();
            }           

            $getWorkingDaysTime = DB::select("select w.*,d.day from society_working_days_time as w join dd_days as d on d.day_id = w.day_id where w.society_id = ".$society_id."");    
            if(!empty($getWorkingDaysTime)){
                $getWorkingDaysTime = $getWorkingDaysTime;    
            }else{
                $getWorkingDaysTime = array();
            }

            $getFloorRiseData = DB::select("select * from op_society_floor_rise where society_id = ".$society_id."");    
            if(!empty($getFloorRiseData)){
                $getFloorRiseData = $getFloorRiseData;    
            }else{
                $getFloorRiseData = array();
            }
            
            $getBrokerageFeeData = DB::select("select * from op_society_brokerage_fee where society_id = ".$society_id."");    
            if(!empty($getBrokerageFeeData)){
                $getBrokerageFeeData = $getBrokerageFeeData;    
            }else{
                $getBrokerageFeeData = array();
            }

            $getKeyMembersData = DB::select("select (select designation_name from  dd_designation where designation_id= s.designation_id) as designation_name,di.initials_name,s.* from op_society_keymembers as s 
            left join dd_initials as di on di.initials_id = s.name_initial
            where s.society_id =".$society_id."");    
            if(!empty($getKeyMembersData)){
                $getKeyMembersData = $getKeyMembersData;    
            }else{
                $getKeyMembersData = array();
            }          

            $getSocietyPhotosData = DB::select("select * from op_society_photos where society_id = ".$society_id."");    
            if(!empty($getSocietyPhotosData)){
                $getSocietyPhotosData = $getSocietyPhotosData;    
            }else{
                $getSocietyPhotosData = array();
            }

            $getProjectProsConsData = DB::select("SELECT * FROM op_society_pros_cons where society_id=".$society_id." order by op_society_pros_cons_id desc ");
            if(!empty($getProjectProsConsData)){
                $getProjectProsConsData =  $getProjectProsConsData;
            }else{
                $getProjectProsConsData = array();
            }       

        }else{

            if(!empty($request->p_id)){
                $project_id = $request->p_id ;
            }else{
                $project_id = $project_id ;
            }
            
            $brokarage_to_date = $payment_type1 ="";            
            $society_id = $building_status = $building_code = $building_owner = $building_name = $unit_status = $total_flat_available= '';
            $no_of_wings = $category_id = $society_status_id = $building_type_id = $lattitde = $longitude = $maintenance_amount = '';
            $location_id = $sub_location_id = $address_1 = $address_2 = $city_id = $state_id = $rera_certificate = '';
            $conveyance_deed = $conveyance_deed_document = $maintenance_amount = $pin_code = $payment_type = '';
            $offers = $floor_rise = $parking = $brokerage_fees = $construction_stage = $construction_stage_date = '';
            $building_constructed_area = $building_land_parcel_area = $building_launch_date = $rera_completion_year = $builder_completion_year = '';
            $society_details = $weekly_off = $office_timing = $building_construction_photos = $oc_certificate = '';
            $oc_reciving_date = $registration_certificate = $ebrouchure = $pros = $cons = $comment = $building_video_links =''; 
            $building_config_details = $brokarage_comments = $brokarage_from_date = $brokarage_from_date =$unit_type = $funded_by = $construction_technology = '';
            $shifting_charges = $prefered_tenant = $building_description = $building_construction_area = '';
            $building_open_space = $gst_certificate =  $possession_year = $building_year_as_per_possession = $building_year_as_per_oc = $building_rating = '';
            $employee_id = $shifting_charges = $prefered_tenant = $society_office_address = $working_days_and_time = $office_timing = $occupation_certi_dated= $unitCount = $indian_green_building = '';
           
            $society_amnities = $unit_amnities =  array();
            $sub_location_idP = $sub_location_idP;
            $sales_office_addressP = $sales_office_addressP;
            $getSocietyConfigData = array(); $unitData = array(); $getWorkingDaysTime = array();
            $getFloorRiseData = array(); $getBrokerageFeeData = array(); $getKeyMembersData = array();
            $getSocietyPhotosData = array(); $getProjectProsConsData = array(); $getConstructionStage = array();
            $project_amenities_society = DB::select("select  * from dd_project_amenities  ");
            $filterData = array();
        }
        
       
        $sub_location =  $this->getAllData('d','sub_location');
        $location =  $this->getAllData('d','location');
        $city = $this->getAllData('d','city');
        $state = $this->getAllData('d','state'); 
        $unit = $this->getAllData('d','unit');
        $unit_status1 = $this->getAllData('d','unit_status');
        $category = $this->getAllData('d','category');
        $parking1 = $this->getAllData('d','parking');   
        $rating = $this->getAllData('d','rating');      
        $configuration = $this->getAllData('d','configuration');  
        $configuration2 = $this->getAllData('d','configuration');  
        $configuration_size = $this->getAllData('d','configuration_size');  
        $construction_technology1 = $this->getAllData('d','construction_technology');
        $preferred_tenant = $this->getAllData('d','preferred_tenant');
        $building_type = $this->getAllData('d','building_type');
        $payment_type = $this->getAllData('d','payment_type');
        $initials = $this->getAllData('d','initials');
        $country_code = $this->getAllData('d','country');
        $designation = $this->getAllData('d','std_designation');
        $employee1 = $this->getAllData('o','employee');
        $funded_by1 = $this->getAllData('d','funded_by');
        $days = $this->getAllData('d','days');
        $dd_building_owner = $this->getAllData('d','building_owner');
        $dd_elevation = $this->getAllData('d','elevation');
        $dd_construction_stage = $this->getAllData('d','construction_stage');
        $dd_unit_amenities = $this->getAllData('d','unit_amenities');        
        
        $allamenities = DB::select("select * from dd_project_amenities order by amenities asc"); 
        $getAllProjects = DB::select("select project_id, project_complex_name from op_project where status =1  and project_complex_name is not null");
            
                    $company_hirarchy =  array();       
                    $dataProjectArray = array( 'society_id'=>$society_id , 'project_id'=>$project_id, 'building_code'=>$building_code, 'building_owner'=>$building_owner, 'building_name'=>$building_name, 'unit_status'=>$unit_status,
                'no_of_wings'=> $no_of_wings , 'category_id'=>$category_id,  'society_status_id'=>$society_status_id,  'building_type_id'=>$building_type_id, 'lattitde'=>$lattitde, 'longitude'=>$longitude,
                    'location_id'=>$location_id, 'sub_location_id'=>$sub_location_id, 'address_1'=>$address_1, 'address_2'=>$address_2 , 'city_id'=> $city_id, 'state_id'=>$state_id,  'rera_certificate'=>$rera_certificate,
                    'conveyance_deed'=>$conveyance_deed, 'building_constructed_area'=>$building_constructed_area ,'conveyance_deed_document'=>$conveyance_deed_document, 'maintenance_amount'=>$maintenance_amount, 'pin_code'=>$pin_code, 'payment_type'=>$payment_type,'payment_type1'=>$payment_type1, 
                    'offers'=>$offers, 'floor_rise'=>$floor_rise,'parking2'=>$parking1 ,'parking1'=>$parking1, 'brokerage_fees'=>$brokerage_fees, 
                    'building_land_parcel_area'=>$building_land_parcel_area, 'building_launch_date'=>$building_launch_date, 'rera_completion_year'=>$rera_completion_year, 'builder_completion_year'=>$builder_completion_year,
                    'society_details'=>$society_details, 'weekly_off'=>$weekly_off, 'office_timing'=>$office_timing, 'building_construction_photos'=>$building_construction_photos, 'oc_certificate'=>$oc_certificate,
                    'oc_reciving_date'=>$oc_reciving_date, 'registration_certificate'=>$registration_certificate, 'ebrouchure'=>$ebrouchure, 'pros'=>$pros, 'cons'=>$cons, 'comment'=>$comment, 'building_video_links'=>$building_video_links,
                    'building_config_details'=>$building_config_details, 'brokarage_comments'=>$brokarage_comments,'brokarage_to_date'=>$brokarage_to_date,'brokarage_from_date'=>$brokarage_from_date ,'unit_type'=>$unit_type, 'funded_by'=>$funded_by, 'construction_technology1'=>$construction_technology1,
                    'shifting_charges'=>$shifting_charges, 'prefered_tenant'=>$prefered_tenant, 'building_description'=>$building_description, 'building_construction_area'=>$building_construction_area,
                    'building_open_space'=>$building_open_space, 'gst_certificate'=>$gst_certificate, 'possession_year'=> $possession_year,'building_year_as_per_possession'=>$building_year_as_per_possession, 'building_year_as_per_oc'=>$building_year_as_per_oc, 'building_rating'=>$building_rating,
                    'locationR'=>$location,'sub_locationR'=>$sub_location,'cityR'=>$city,'stateR'=>$state,'unit'=>$unit,'project_amenities'=>$project_amenities,'unit_status1'=>$unit_status1,'category'=>$category,
                    'parking'=>$parking,'rating'=>$rating,'configuration'=>$configuration,'configuration2'=>$configuration2,'configuration_size'=>$configuration_size,'configuration_size2'=>$configuration_size,'construction_technology'=>$construction_technology,'preferred_tenant'=>$preferred_tenant,
                    'building_type'=>$building_type,'building_type1'=>$building_type,'payment_type'=>$payment_type,'initials'=>$initials,'country_code'=>$country_code,'designation'=>$designation,'company_hirarchy'=>$company_hirarchy,'employee1'=>$employee1     
                    ,'project_amenities_society'=>$project_amenities_society,'employee_id'=>$employee_id,'getSocietyConfigData'=>$getSocietyConfigData,
                    'getFloorRiseData'=>$getFloorRiseData,'getBrokerageFeeData'=>$getBrokerageFeeData,'getKeyMembersData'=>$getKeyMembersData,
                    'shifting_charges'=>$shifting_charges,'prefered_tenant'=>$prefered_tenant,'society_office_address'=>$society_office_address, 'working_days_and_time'=>$working_days_and_time, 'office_timing'=>$office_timing, 'occupation_certi_dated'=>$occupation_certi_dated,
                    'getSocietyPhotosData'=>$getSocietyPhotosData,'getProjectData'=>$getProjectData,'getProjectProsConsData'=>$getProjectProsConsData,
                    'getConstructionStage'=>$getConstructionStage,'total_flat_available'=>$total_flat_available, 'maintenance_amount'=>$maintenance_amount,'funded_by1'=>$funded_by1,
                    'unitData'=>$unitData,'days'=>$days,'getWorkingDaysTime'=>$getWorkingDaysTime,'allamenities'=>$allamenities,'allamenities1'=>$allamenities,'building_status'=>$building_status,
                    'sub_location_idP'=>$sub_location_idP,'sales_office_addressP'=>$sales_office_addressP, 'getAllProjects'=>$getAllProjects,'filterData'=>$filterData,'society_amnities'=>$society_amnities,'dd_building_owner'=>$dd_building_owner,'dd_elevation2'=>$dd_elevation,'dd_elevation1'=>$dd_elevation,'dd_construction_stage'=>$dd_construction_stage,'dd_construction_stage1'=>$dd_construction_stage,
                    'unitCount'=>$unitCount,'indian_green_building'=>$indian_green_building,'dd_unit_amenities'=>$dd_unit_amenities
                    ,'unit_amnities'=>$unit_amnities) ;           
                
        return view('society_master/society_master',$dataProjectArray);
    }

    public function add_society_master(Request $request)
    {
 
       
        $form_type = $request->form_type;
        $project_id = $request->project_id;
        $society_id = $request->society_id;
        
        if( $form_type == 'pros_cons' ){
            $data['pros_of_building'] = $request->pros_of_building;
            $data['cons_of_building'] = $request->cons_of_building; 
            $data['user_id'] = $this->getUserId();       

            if( !empty( $society_id ) ){
                // return 'in if';
                $data['society_id'] = $society_id;
                DB::table('op_society_pros_cons')->insert($data);
                $data['op_society_pros_cons_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from op_society_pros_cons where op_society_pros_cons_id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date;               
            }else{
            //    return 'in else';
                DB::table('op_society')->insert(['project_id'=> $project_id,'user_id'=>$this->getUserId() ]);            
                $data['society_id'] = DB::getPdo()->lastInsertId();
                DB::table('op_society_pros_cons')->insert($data);   
                $data['op_society_pros_cons_id'] = DB::getPdo()->lastInsertId();  
                $getDate = DB::select("select updated_date from op_society_pros_cons where op_society_pros_cons_id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date;            
            }
            return response()->json($data);  
        }

        date_default_timezone_set("Asia/Calcutta");      

        if( $form_type == 'office_timing' ){
            $data['day_id'] = $request->working_day;
            $data['office_timing'] = $request->office_timing;   
            $data['office_timing_to'] = $request->office_timing_to;   
            $data['created_date'] = date('Y-m-d H:i:s');
            $data['user_id'] = $this->getUserId();  

            if( !empty( $society_id ) ){
                $data['society_id'] = $society_id;
                DB::table('society_working_days_time')->insert($data);
                $society_working_days_time_id =  DB::getPdo()->lastInsertId();
                $data['society_working_days_time_id'] =  $society_working_days_time_id;
                $getDate = DB::select("select updated_date from society_working_days_time where society_working_days_time_id = ".$society_working_days_time_id."");
                $data['updated_date'] = $getDate[0]->updated_date;
                $getDay = DB::select("select d.day from society_working_days_time as w join dd_days as d on d.day_id = w.day_id where society_working_days_time_id = ".$society_working_days_time_id."");
                // print_r($getDay); die();
                $data['day'] = $getDay[0]->day;
            }else{
                
                DB::table('op_society')->insert(['project_id'=> $project_id,'user_id'=>$this->getUserId() ]);                
                $data['society_id'] = DB::getPdo()->lastInsertId();
                DB::table('society_working_days_time')->insert($data);   
                $society_working_days_time_id =  DB::getPdo()->lastInsertId();
                $data['society_working_days_time_id'] = $society_working_days_time_id;  
                $getDate = DB::select("select updated_date from society_working_days_time where society_working_days_time_id = ".$society_working_days_time_id."");
                $data['updated_date'] = $getDate[0]->updated_date;   
                $getDay = DB::select("select d.day from society_working_days_time as w join dd_days as d on d.day_id = w.day_id where society_working_days_time_id = ".$society_working_days_time_id."");
                $data['day'] = $getDay[0]->day;         
            }
            return response()->json($data);  
        }

        
        if( $form_type == 'hierarchy' ){
            $data['name_initial'] = $request->name_initial;
            $data['name'] = $request->name_h;
            $data['country_code_id'] = $request->country_code;
            $data['mobile_number'] = $request->mobile_number_h;
            $data['whatsapp_number'] = $request->whatsapp_number_h;
            $data['email_id'] = $request->primary_email_h;
            $data['designation_id'] = $request->designation_h;
            $data['residence_address'] = $request->residence_address_h;
            $data['society_id'] = $request->society_id;
            $data['user_id'] = $this->getUserId();  

            if(isset($society_id) && !empty($society_id)){
                DB::table('op_society_keymembers')->insert($data); 
                $society_keymembers_id = DB::getPdo()->lastInsertId();
                $data['society_keymembers_id'] = $society_keymembers_id;
                $getDate = DB::select("select designation_id,updated_date from op_society_keymembers where society_keymembers_id = ".$society_keymembers_id."");
                if(isset($getDate[0]->designation_id) && !empty($getDate[0]->designation_id)){
                $designation_id = $getDate[0]->designation_id;
                $getDesignation = DB::select("select designation_name from dd_std_designation where designation_id=".$designation_id." ");
                $data['designation_name'] = $getDesignation[0]->designation_name;  
                }else{
                    $data['designation_name'] ="";
                }
                if(!empty($request->name_initial)){
                    $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                    $data['initials_name'] = $Initial[0]->initials_name;
                }else{
                    $data['initials_name'] = '';
                }
                $data['updated_date'] = $getDate[0]->updated_date;  
                return response()->json($data);  
            }else{
                DB::table('op_society')->insert(['project_id'=>$project_id,'user_id'=>$this->getUserId()]);            
                $society_id = DB::getPdo()->lastInsertId();
                $data['society_id'] = $society_id;        
                DB::table('op_society_keymembers')->insert($data); 
                $society_keymembers_id = DB::getPdo()->lastInsertId();
                $data['society_keymembers_id'] = $society_keymembers_id;
                $getDate = DB::select("select designation_id,updated_date from op_society_keymembers where society_keymembers_id = ".$society_keymembers_id."");
                $designation_id = $getDate[0]->designation_id;
                $getDesignation = DB::select("select designation_name from dd_std_designation where designation_id=".$designation_id." ");
                $data['updated_date'] = $getDate[0]->updated_date;  
                $data['designation_name'] = $getDesignation[0]->designation_name;  
                if(!empty($request->name_initial)){
                    $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                    $data['initials_name'] = $Initial[0]->initials_name;
                }else{
                    $data['initials_name'] = '';
                }
                return response()->json($data);   
            }
        }

        if( $form_type == 'other_form' ){
       
        $building_status = $request->building_status;
        if($building_status == 1) { 
            $validator = Validator::make($request->all(), [
                    'building_name' => 'required',                   
                    'building_status' => 'required',                    
                    'sub_location_r' => 'required',                    
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'building_name' => 'required',               
                'building_status' => 'required',
                'sub_location_uc' => 'required',               
            ]);
        }       

                if ($validator->passes()) {
                    $data = array(); 
                    $society_id = $request->society_id;
                    $data['building_name'] = $request->building_name;
                    $data['building_status'] = $request->building_status;
                    $data['building_owner'] = $request->building_owner;
                    $data['unit_status'] = json_encode($request->unit_status);
                    $data['user_id'] = $this->getUserId();  
                    if($building_status == 1) { // registered
                    
                        $data['category_id'] = json_encode($request->building_category_r);
                        $data['building_type_id'] = json_encode($request->building_type_r);
                        $data['construction_technology'] = $request->construction_technology_r;
                        $data['parking'] = json_encode($request->parking_r);
                        $data['maintenance_amount'] = $request->maintenance_amount_r;
                        $building_land_parcel_area_unit_r = $request->building_land_parcel_area_unit_r;
                        $data['building_land_parcel_area'] = $request->building_land_parcel_area_r;                    
                        $data['building_open_space'] = $request->building_open_space_r;
                        $building_constructed_area_unit_r = $request->building_constructed_area_unit_r;
                        $data['building_constructed_area'] = $request->building_constructed_area_r;
                        $data['possession_year'] = $request->possession_year_r;
                        $data['building_year_as_per_possession'] = $request->building_year_possession_year_r;
                        $data['occupation_certi_dated'] = $request->occupation_certi_dated_r;
                        $data['building_year_as_per_oc'] = $request->building_year_oc_dated_r;
                        $data['building_description'] = $request->building_description_r;
                        $data['lattitde'] = $request->society_geo_location_lat_r;
                        $data['longitude'] = $request->society_geo_location_long_r;

                        $data['sub_location_id'] = $request->sub_location_r;
                        $data['location_id'] = $request->location_r;                       
                        $data['city_id'] = $request->city_r;
                        $data['state_id'] = $request->state_r;

                        $data['society_office_address'] = $request->building_office_address;
                        $data['building_rating'] = $request->building_rating_r;
                        $data['indian_green_building'] = $request->indian_green_building_r;
                        $data['shifting_charges'] = $request->shifting_charges_r;
                        $data['prefered_tenant'] = $request->preferred_tenant_r;
                        $data['comment'] = $request->comment_r;
                        $data['account_manager'] = $request->account_manager_r;
                        $data['building_code'] = $request->building_code_r;
                        $data['building_video_links'] = $request->building_video_links_r;

                        $is_availableR = $request->is_availableR;
                        $explode_is_availableR =  explode(",",$is_availableR);
                        $description_ameR = $request->description_ameR;
                        $explode_description_ameR =  explode(",",$description_ameR);
                        // print_r($explode_description_ameR); die();
                      

                        if(isset($is_availableR) && !empty($is_availableR)){
                            foreach($explode_is_availableR as $key=>$val){
                                // echo "<pre>"; print_r(key$key);
                                // echo "<pre>"; print_r($val);
                                $amData = array('description'=>$explode_description_ameR[$key]);
                                DB::table('op_society_amnities')->where('society_amnities_id',$val)->update($amData);
                            }
                        }


                        if($request->hasfile('oc_certificate_r'))
                        {
                            $array2 = array();
                            foreach($request->file('oc_certificate_r') as $file)
                            {
                                $rand2 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand2);
                                array_push($array2,$rand2);                        
                            }

                            $getOcCert = DB::select("select oc_certificate from op_society where society_id= ".$society_id." ");
                            if(  isset($getOcCert) &&  !empty($getOcCert) ){
                                
                                $existingOcCert = json_decode($getOcCert[0]->oc_certificate);                       
                                if(!empty($existingOcCert)){
                                    $finalArrayOcCert = array_merge($existingOcCert,$array2);
                                }else{
                                    $finalArrayOcCert = $array2;
                                }
                                
                            }else{
                                $finalArrayOcCert = $array2;
                            }   
                            $data['oc_certificate'] = json_encode($finalArrayOcCert);
                        }

                        if($request->hasfile('registration_certificate_r'))
                        {
                            $array3 = array();
                            foreach($request->file('registration_certificate_r') as $file)
                            {
                                $rand3 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand3);
                                array_push($array3,$rand3);                        
                            }
                            $data['registration_certificate'] = json_encode($array3);
                        }

                        if($request->hasfile('conveyance_deed_r'))
                        {
                            $array4 = array();
                            foreach($request->file('conveyance_deed_r') as $file)
                            {
                                $rand4 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand4);
                                array_push($array4,$rand4);                        
                            }
                            $data['conveyance_deed'] = json_encode($array4);
                        }

                        if( isset($society_id) && !empty($society_id) ){
                            DB::table('op_society')->where('society_id',$society_id)->update($data); 
                            $last_id =$society_id; //DB::getPDO()->lastInsertId();
                            $data['last_id'] = $last_id;
                            $ProjectLocation = array('sub_location_id'=>$request->sub_location_r,'location_id'=>$request->location_r,
                            'city_id'=>$request->city_r,'state_id'=>$request->state_r);
                            DB::table('op_project')->where('project_id',$project_id)->update($ProjectLocation);

                        }else{
                            $checkBuildingName = DB::select("select building_name from op_society where project_id =".$request->project_id." and building_name ='".trim($request->building_name)."' and status='1'");
                                // echo "<pre>"; print_r($checkBuildingName); die();
                                if(!empty($checkBuildingName)){                
                                    $ary = array("Building Name is already exist");
                                    return response()->json(['error'=>$ary]);
                                }
                            $data['project_id'] = $project_id;
                            DB::table('op_society')->insert($data); 
                            $last_id = DB::getPDO()->lastInsertId();
                            $data['last_id'] = $last_id;
                        }                       


                        if($request->hasfile('building_images_r'))
                        {
                            foreach($request->file('building_images_r') as $file)
                            {
                                $rand1 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand1);
                                $photoData1 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand1,'meta_key'=>'building_images');
                                DB::table('op_society_photos')->insert($photoData1);      
                            }
                        }

                        if($request->hasfile('building_video_r'))
                        {
                            foreach($request->file('building_video_r') as $file)
                            {
                                $rand8 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand8);
                                $photoData8 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand8,'meta_key'=>'building_video');
                                DB::table('op_society_photos')->insert($photoData8);      
                            }
                        }
                    }else{  // UC
                        $data['category_id'] = json_encode($request->building_category_uc);
                        $data['building_type_id'] = json_encode($request->building_type_uc);
                        $data['building_config_details'] = $request->building_configuration_details_uc;
                        $data['construction_technology'] = $request->construction_technology_uc;
                        $data['parking'] = json_encode($request->parking_uc);//
                        $data['funded_by'] = json_encode($request->funded_by_uc);
                        $data['payment_type'] = json_encode($request->payment_plan_uc);
                        $data['payment_plan_comment'] = $request->payment_plan_comment_uc;
                        $data['brokarage_comments'] = $request->brokarage_comment_uc;
                        $data['brokarage_from_date'] = $request->brokarage_from_date_uc;
                        $data['brokarage_to_date'] = $request->brokarage_to_date_uc;                 
                        $data['building_description'] = $request->building_description_uc;
                        $data['maintenance_amount'] = $request->maintenance_amount_uc;
                        $data['lattitde'] = $request->society_geo_location_lat_uc;
                        $data['longitude'] = $request->society_geo_location_long_uc;
                        $data['location_id'] = $request->location_uc;
                        $data['sub_location_id'] = $request->sub_location_uc;
                        $data['city_id'] = $request->city_uc;
                        $data['state_id'] = $request->state_uc;
                        $data['society_office_address'] = $request->building_office_address_uc;
                        $data['building_land_parcel_area'] = $request->building_land_parcel_area_uc;
                        $data['building_open_space'] = $request->building_open_space_uc;
                        $data['building_constructed_area'] = $request->building_constructed_area_uc;
                        $data['building_rating'] = $request->building_rating_uc;
                        $data['indian_green_building'] = $request->indian_green_building_uc;
                        $data['building_launch_date'] = $request->building_lauch_date_uc;
                        $data['builder_completion_year'] = $request->builder_completion_year_uc;
                        $data['rera_completion_year'] = $request->rera_completion_year_uc;
                        $data['building_video_links'] = $request->building_video_links_uc;
                        $data['comment'] = $request->comment_uc;
                        $data['total_flat_available'] = $request->total_flat_available_uc;
                        $data['account_manager'] = $request->account_manager_uc;
                        $data['building_code'] = $request->building_code_uc;

                        $is_availableUC = $request->is_availableUC;
                        $explode_is_availableUC =  explode(",",$is_availableUC);
                        $description_ameUC = $request->description_ameUC;
                        $explode_description_ameUC =  explode(",",$description_ameUC);
                        // print_r($request->all()); die();
                      

                        if(isset($is_availableUC) && !empty($is_availableUC)){
                            foreach($explode_is_availableUC as $key=>$val){
                                // echo "<pre>"; print_r(key$key);
                                // echo "<pre>"; print_r($val);
                                $amData = array('description'=>$explode_description_ameUC[$key]);
                                DB::table('op_society_amnities')->where('society_amnities_id',$val)->update($amData);
                            }
                        }
                        if( isset($society_id) && !empty($society_id) ){
                            DB::table('op_society')->where('society_id',$society_id)->update($data); 
                            $last_id =$society_id; //DB::getPDO()->lastInsertId();
                            $data['last_id'] = $last_id;
                            $ProjectLocation = array('sub_location_id'=>$request->sub_location_uc,'location_id'=>$request->location_uc,
                            'city_id'=>$request->city_uc,'state_id'=>$request->state_uc);
                            DB::table('op_project')->where('project_id',$project_id)->update($ProjectLocation);
                        }else{
                            $data['project_id'] = $project_id;
                            DB::table('op_society')->insert($data); 
                            $last_id = DB::getPDO()->lastInsertId();
                            $data['last_id'] =$last_id;
                            $society_id = $last_id;
                        } 

                        // print_r($society_id); die();
                        // if(isset($request->account_manager_uc) && !empty($request->account_manager_uc) && $request->account_manager_uc != null){
                        //     $dataAM = array('society_id'=>$last_id,'employee_id'=>$request->account_manager_uc);
                        //     $checkAccountManager = DB::select("select * from op_society_account_manager where society_id =".$society_id." ");
                        //     if($checkAccountManager){
                        //         DB::table('op_society_account_manager')->where('society_id',$society_id)->update($dataAM);   
                        //     }else{
                        //         DB::table('op_society_account_manager')->insert($dataAM);   
                        //     }
                        // }

                        if($request->hasfile('rera_certificate_uc'))
                        {
                            foreach($request->file('rera_certificate_uc') as $file)
                            {
                                $rand8 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand8);
                                $photoData8 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand8,'meta_key'=>'rera_certificate');
                                DB::table('op_society_photos')->insert($photoData8);      
                            }
                        }

                        if($request->hasfile('flat_images_uc'))
                        {
                            foreach($request->file('flat_images_uc') as $file)
                            {
                                $rand9 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand9);
                                $photoData9 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand9,'meta_key'=>'flat_images');
                                DB::table('op_society_photos')->insert($photoData9);      
                            }
                        }

                        if($request->hasfile('building_images_uc'))
                        {
                            foreach($request->file('building_images_uc') as $file)
                            {
                                $rand9 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand9);
                                $photoData9 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand9,'meta_key'=>'building_images');
                                DB::table('op_society_photos')->insert($photoData9);      
                            }
                        }
                        
                        if($request->hasfile('building_video_uc'))
                        {
                            foreach($request->file('building_video_uc') as $file)
                            {
                                $rand10 = mt_rand(99,9999999).".".$file->extension();
                                $file->move(public_path('images'), $rand10);
                                $photoData10 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand10,'meta_key'=>'building_video');
                                DB::table('op_society_photos')->insert($photoData10);      
                            }
                        }
                        // echo "<pre>"; print_r($society_id);die();

                        $data1['zero_to_two'] = $request->zero_to_two_uc;
                        $data1['three_to_five'] = $request->three_to_five_uc;
                        $data1['six_to_ten'] = $request->six_to_ten_uc;
                        $data1['above_eleven'] = $request->above_eleven_uc;

                        // echo "<pre>"; print_r($data1); die();

                        $checkBrokerageFree = DB::select("select * from op_society_brokerage_fee where society_id =". $society_id." ");

                        // echo "<pre>"; print_r($checkBrokerageFree); die();

                        if($checkBrokerageFree){                          
                            DB::table('op_society_brokerage_fee')->where('society_id',$society_id)->update($data1); 
                        }else{
                           $data1['society_id'] = $society_id;
                           DB::table('op_society_brokerage_fee')->insert($data1); 
                        }


                        $floor_slab_from_uc = $request->floor_slab_from_uc;
                        $floor_slab_from = explode(",",$floor_slab_from_uc);
                        $floor_slab_to_uc = $request->floor_slab_to_uc;
                        $floor_slab_to = explode(",",$floor_slab_to_uc);
                        $configuration_uc = $request->configuration_uc;
                        $configuration = explode(",",$configuration_uc);
                        $area_uc = $request->area_uc;
                        $area = explode(",",$area_uc);
                        $rs_per_sq_ft_uc = $request->rs_per_sq_ft_uc;
                        $rs_per_sq_ft = explode(",",$rs_per_sq_ft_uc);
                        $box_price_uc = $request->box_price_uc;
                        $box_price = explode(",",$box_price_uc);
                        $society_floor_rise_id_uc = $request->society_floor_rise_id_uc;
                        $society_floor_rise_id = explode(",",$society_floor_rise_id_uc);

                        $cnt = count($floor_slab_from);
                        for ($i=0; $i<$cnt ; $i++) {  
                            $array  = array('society_id'=>$society_id,'floor_slab_from'=>$floor_slab_from[$i],'floor_slab_to'=>$floor_slab_to[$i],
                            'configuration'=>$configuration[$i],'area'=>$area[$i],'rs_per_sq_ft'=>$rs_per_sq_ft[$i],'box_price'=>$box_price[$i]);
                            if($society_floor_rise_id[$i] == 0 ){
                                if($floor_slab_from[$i] != 0){
                                    DB::table('op_society_floor_rise')->insert($array); 
                                }
                            }else{
                                DB::table('op_society_floor_rise')->where('society_floor_rise_id',$society_floor_rise_id[$i])->update($array); 
                            }
                        }
                        
                    }
                    
                    return response()->json(['success'=>'done','lastID'=>$last_id]);

                    
                }
                return response()->json(['error'=>$validator->errors()->all()]);


        }

        if( $form_type == 'construction_stage' ){
            // echo "<pre>"; print_r($request->all()); die();
            $data['building_construction_stage'] = json_encode($request->building_construction_stage);
            $data['building_construction_stage_date'] = $request->building_construction_stage_date;
            $data['comments_bc'] = $request->comments_bc;
            $data['user_id'] = $this->getUserId();  
            $society_id = $request->society_id;
            $project_id = $request->project_id;
            $array2 = array();
            if($request->hasfile('construction_stage_images'))
            {
                
                foreach($request->file('construction_stage_images') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('images'), $rand2);
                    array_push($array2,$rand2);                        
                }
                $data['construction_stage_images'] = json_encode($array2);
               
            }

                if( !empty( $society_id ) ){
                    $data['society_id'] = $society_id;
                    DB::table('society_construction_stage')->insert($data);
                    $data['society_construction_stage_id'] = DB::getPdo()->lastInsertId();
                    $data['construction_stage_images1'] = ($array2);
                }else{                    
                    DB::table('op_society')->insert(['project_id'=> $project_id,'user_id'=>$this->getUserId() ]);            
                    $data['society_id'] = DB::getPdo()->lastInsertId();
                    DB::table('society_construction_stage')->insert($data);   
                    $data['society_construction_stage_id'] = DB::getPdo()->lastInsertId();              
                    $data['construction_stage_images1'] = ($array2);
                }

                $getDate = DB::select("select created_date from society_construction_stage where society_construction_stage_id = ".DB::getPdo()->lastInsertId()." ");
                $data['created_date'] = $getDate[0]->created_date;

                $getConstructionStageN = DB::select("select construction_stage  from dd_construction_stage where construction_stage_id in (".$request->building_construction_stage[0].") ");
                $csArr = array();
                if(!empty($getConstructionStageN)){
                    foreach ($getConstructionStageN as $key => $value) {
                        array_push($csArr,ucfirst($value->construction_stage ));      
                    }                    
                    $data['building_construction_stage'] =($csArr);
                }
                
                return response()->json($data);  
        }   
    }

    public function append_society_hierarchy_form(Request $request)
    {        
        $cnt  = $request->aa;
        $initials = $this->getAllData('d','initials');
        $designation = $this->getAllData('d','designation');
        $country_code = $this->getAllData('d','country');
        $db = env('operationDB');
        $company_hirarchy =   DB::connection($db)->select("select company_hirarchy_id,name  from op_company_hirarchy");
        $html = '<div class="row"><div class="col l11"><div class="row"><div class="input-field col l3 m4 s12" id="InputsWrapper2"><label for="cus_name active" class="dopr_down_holder_label active">Name <span class="red-text">*</span></label><div  class="sub_val no-search"><select  id="name_initial_'.$cnt.'" name="name_initial[]" class="select2 browser-default">
        ';
            foreach($initials as $ini){
                $html .= '<option value="'.$ini->initials_id.'">'.ucfirst($ini->initials_name).'</option>';
            }
        // <option value="1">Mr.</option><option value="2">Miss.</option><option value="3">Mrs.</option>
        $html .='</select></div><input type="text" class="validate mobile_number" name="name_h[]" id="name_h"  placeholder="Enter"  ></div><div class="input-field col l3 m4 s12" id="InputsWrapper"><label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label><div  class="sub_val no-search"><select  id="country_code_'.$cnt.'" name="country_code[]" class="select2 browser-default">';
            foreach($country_code as $cou){
                $html .= '<option value="'.$cou->country_code_id.'">'.ucfirst($cou->country_code).'</option>';
            }
        // <option value="1">+91</option><option value="2">+1</option>
        $html .= '</select></div><input type="text" class="validate mobile_number" name="mobile_number_h[]" id="mobile_number"  placeholder="Enter" ></div>';
        // $html .='<div class="input-field col l3 m4 s12" id="InputsWrapper2"><div  class="sub_val no-search"><select  id="country_code_w_'.$cnt.'" name="country_code_w[]" class="select2 browser-default">';
        //         foreach($country_code as $cou){
        //             $html .= '<option value="'.$cou->country_code_id.'">'.ucfirst($cou->country_code).'</option>';
        //         }
        // $html .= '</select></div><label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label> <input type="text" class="validate mobile_number" name="wap_number[]" id="wap_number"  placeholder="Enter" ><div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " ><input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto"></div></div>';
        $html .= '<div class="input-field col l3 m4 s12" id="InputsWrapper3"><label for="primary_email active" class="active">Email Address: <span class="red-text">*</span></label><input type="email" class="validate" name="primary_email_h[]" id="primary_email"  placeholder="Enter" ></div>';
        $html .=' <div class="input-field col l3 m4 s12 display_search"><select class="select2  browser-default designation_h"  data-placeholder="Select" id="designation_'.$cnt.'" name="designation_h[]">';
            foreach($designation as $des){
                $html .= '<option value="'.$des->designation_id.'">'.ucfirst($des->designation_name).'</option>';
            }
        $html .='</select><label for="designation" class="active">Designation </label><div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" ><a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a></div></div></div></div>';    
        // $html .='<div class="input-field col l3 m4 s12 display_search"><select class="select2  browser-default"  data-placeholder="Select" id="reporting_to_'.$cnt.'" name="reporting_to_h[]"><option value=""></option>';
        //     foreach($company_hirarchy as $comp){
        //         $html .= '<option value="'.$comp->company_hirarchy_id.'">'.ucfirst($comp->name).'</option>';
        //     }
        // $html .='</select><label for="reporting_to" class="active">Reporting to </label></div><div class="input-field col l3 m4 s12" id="InputsWrapper2"><label  class="active">Comments: <span class="red-text">*</span></label>
        // <input type="text" class="validate" name="comments_h[]" id="comments_h[]"  placeholder="Enter" ></div><div class="input-field col l3 m4 s12 display_search"><label  class="active">Upload Photo : </label><input type="file" name="photo_h[]"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;"></div></div></div>';    
        $html .= ' <div class="col l1"><a href="javascript:void(0)" style="color: red !important;font-size: 23px" class="removeclassCompany waves-effect waves-light">-</a><a href="javascript:void(0)" id="add_location" class="modal-trigger" title="Action"  style="color: grey !important;font-size: 23px;"> <i class="material-icons  dp48">group</i></a><a href="#modal10" id="add_location" title="View Flow Chart" class="modal-trigger" style="color: grey !important;font-size: 23px;"> <i class="material-icons  dp48">remove_red_eye</i></a></div>';
        return $html;
    }


    public function append_config_sample(Request $request)
    {
        // print_r();
        $increment = $request->aa;
        $configuration = $this->getAllData('d','configuration');  
        $configuration_size = $this->getAllData('d','configuration_size');

        $html = '<tr>
        <td style="width: 21%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
               <select  id="sample_flat_available_'.$increment.'" name="sample_flat_available[]" class="validate select2 browser-default">
                    <option value="1" >Yes</option>                                    
                    <option value="0" >No</option>
                </select>
            <div>
        </td>
        <td style="width: 12.2%;">  
             <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
               <select  name="configuration[]" id="configuration_'.$increment.'" class="validate select2 browser-default">
                   ';
                  foreach($configuration as $configuration){

                    $html .='<option value="'.$configuration->configuration_id.'"> '. ucwords($configuration->configuration_name).'</option>';                                    
                  }
                  
            $html .='</select>
            <div>
        </td>                        
        <td style="width: 9%;">
                <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
                    <select   name="configuration_size[]" id="configuration_size'.$increment.'" class="validate select2 browser-default">
                        ';
                        foreach($configuration_size as $configuration_size){
                        $html .='<option value="'.$configuration_size->configuration_size_id.'">'. ucwords($configuration_size->configuration_size).'</option>';
                        }
                $html .='</select>
               <div>
        </td>
        <td style="width: 12.7%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                <input type="text" class="validation" name="area[]"   placeholder="Enter" >
            </div>
        </td>
        <td style="width: 12.7%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                <input type="text" class="validation" name="total_falt_available[]"   placeholder="Enter" >
            </div>
        </td>  
        <td style="width: 12.7%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                <input type="text" class="validation" name="flat_available[]"   placeholder="Enter" >
            </div>
        </td>                        
        <td></td>
        <td style="width: 9%;">
        
        <a href="javascript:void(0)" style="color: red !important;font-size: 23px" class="removeclassConfig waves-effect waves-light">-</a>
                        <!-- <a href="javascript:void(0)" id="hap" class="clonable-button-add" title="Add" style="color: red !important"> <i class="material-icons  dp48">add</i></a> 
                        <a href="javascript:void(0)" class="clonable-button-close" title="Remove" style="color: red !important; display: none;"> <i class="material-icons  dp48">remove</i></a>  -->
        </td>
        </tr>';
        echo $html;    }

    public function update_society_master(Request $request)
    {
      
        $validator = Validator::make($request->all(), [
            'building_name' => 'required',
            'unit_status' => 'required',
            'building_status' => 'required',
            'building_category' => 'required',
            'society_geo_location_lat' => 'required',
            'society_geo_location_long' => 'required',
            'location' => 'required',
            'sub_location' => 'required',
            'city' => 'required',
            'state' => 'required',
            'parking' => 'required',
                       
            
        ]
     );
     if ($validator->passes()) {
            $society_id = $request->society_id;
            $data = array(); 
            $data['project_id'] = $request->project_id;
            $data['building_owner'] = $request->building_owner;
            $data['society_status_id'] = 1;// $request->building_status;
            $data['unit_status'] = $request->unit_status;
            $data['building_description'] = $request->building_description;
            $data['building_name'] = $request->building_name;
            $data['building_code'] = $request->building_code;
            $data['category_id'] = $request->building_category;
            $data['building_land_parcel_area'] = $request->building_land_parcel_area;
            //building_land_parcel_area_unit] => sq.ft
            $data['building_construction_area'] = $request->building_constructed_area;
            //building_constructed_area_unit] => sq.ft
            $data['building_open_space'] = $request->building_open_space;
            //building_open_space_unit] => sq.ft
            $data['building_type_id'] = $request->building_type;
            $data['parking'] = json_encode($request->parking);

            $data['sub_location_id'] = $request->sub_location;    
            $data['location_id'] = $request->location;         
            $data['city_id'] = $request->city;
            $data['state_id'] = $request->state;

            $data['construction_technology'] = $request->construction_technology;
            $data['lattitde'] = $request->society_geo_location_lat;
            $data['longitude'] = $request->society_geo_location_long;
            $data['maintenance_amount'] = $request->maintenance_amount;
            $data['building_rating'] = $request->building_rating;
            $data['pros'] = $request->pros_of_building;
            $data['cons'] = $request->cons_of_building;

            $data['shifting_charges'] = $request->shifting_charges;
            $data['prefered_tenant'] = $request->prefered_tenant;
            $data['society_office_address'] = $request->society_office_address;
            $data['working_days_and_time'] = $request->working_days_and_time;
            $data['office_timing'] = $request->office_timing;
            $data['office_timing_to'] = $request->office_timing_to;
            $data['occupation_certi_dated'] = $request->occupation_certi_dated;
            $data['possession_year'] = $request->possession_year;
            
            $data['payment_type'] = json_encode($request->payment_plan);
            $data['brokarage_comments'] = $request->brokarage_comments;
            $data['funded_by'] = $request->funded_by;
            $data['construction_stage'] = $request->building_construction_stage;
            $data['construction_stage_date'] = $request->building_construction_stage_date;
            $data['building_launch_date'] = $request->buulding_lauch_date;
            $data['builder_completion_year'] = $request->builder_completion_year;
            $data['rera_completion_year'] = $request->rera_completion_year;
            $data['comment'] = $request->comment;
            $data['building_video_links'] = $request->building_video_links;
            $data['building_config_details'] = $request->building_config_details;
            $data['user_id'] = $this->getUserId();

            // for single image upload
            if( $request->hasfile('oc_certificate') )
            { 
                $rand1 = mt_rand(99,9999999).".".$request->oc_certificate->extension();         
                $request->oc_certificate->move(public_path('images'), $rand1);
                $data['oc_certificate'] = $rand1;
            }
    
            if( $request->hasfile('registration_certificate') )
            { 
                $rand2 = mt_rand(99,9999999).".".$request->registration_certificate->extension();         
                $request->registration_certificate->move(public_path('images'), $rand2);
                $data['registration_certificate'] = $rand2;
            }
    
            if( $request->hasfile('conveyance_deed') )
            { 
                $rand3 = mt_rand(99,9999999).".".$request->conveyance_deed->extension();         
                $request->conveyance_deed->move(public_path('images'), $rand3);
                $data['conveyance_deed'] = $rand3;
            }
            
            if( $request->hasfile('rera_certificate') )
            { 
                $rand4 = mt_rand(99,9999999).".".$request->rera_certificate->extension();         
                $request->rera_certificate->move(public_path('images'), $rand4);
                $data['rera_certificate'] = $rand4;
            }
            // echo "/n"; print_r($data);exit();
            DB::table('op_society')->where('society_id',$society_id)->update($data);
            
            $society_keymembers_id = $request->society_keymembers_id;
            $name_initial = $request->name_initial;
            $name_h = $request->name_h;
            $country_code = $request->country_code;
            $mobile_number_h = $request->mobile_number_h;
            $country_code_w = $request->country_code_w;           
            $primary_email_h = $request->primary_email_h;
            $designation_h = $request->designation_h;           
        
            $cnt = count($name_initial);
            
            for ($i=0; $i<$cnt ; $i++) {                            
                $array = array('society_id'=>$society_id);        
                if(!empty($name_h[$i])){
                    $name = $name_h[$i]; 
                    $a = array('name'=>$name) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($country_code[$i])){
                    $country_code_id = $country_code[$i]; 
                    $a = array('country_code_id'=>$country_code_id) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($mobile_number_h[$i])){
                    $mobile_number = $mobile_number_h[$i]; 
                    $a = array('mobile_number'=>$mobile_number) ;
                    $array=   array_merge($array,$a);                   
                }       
        
                if(!empty($primary_email_h[$i])){
                    $email_id = $primary_email_h[$i]; 
                    $a = array('email_id'=>$email_id) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($designation_h[$i])){
                    $designation_id = $designation_h[$i]; 
                    $a = array('designation_id'=>$designation_id) ;
                    $array=   array_merge($array,$a);                   
                }        
       
                // DB::table('op_society_keymembers')->insert($array); 
                // echo "<pre>"; print_r($array);
                
                if($society_keymembers_id[$i] == 0 ){
                    // echo "/n"; print_r($company_hei_id[$i]);
                    // 
                    if(!empty($array['name'])){
                        DB::table('op_society_keymembers')->insert($array);                
                    }
                    
                }else{           
                    // echo "/n"; print_r($company_hei_id[$i]);
                    // echo "<pre>"; print_r($array);
                    DB::table('op_society_keymembers')->where('society_keymembers_id',$society_keymembers_id[$i])->update($array);
                } 
            }

              // for multiple image upload
              if($request->hasfile('construction_stage_images'))
              {
                  foreach($request->file('construction_stage_images') as $file)
                  {
                      $rand5 = mt_rand(99,9999999).".".$file->extension();
                      $file->move(public_path('images'), $rand5);
                      $photoData5 = array('society_id'=>$society_id,'type'=>'photo','url'=>$rand5,'meta_key'=>'construction_stage_images');
                      DB::table('op_society_photos')->insert($photoData5);      
                  }
              }

              if($request->hasfile('flat_images'))
              {
                  foreach($request->file('flat_images') as $file)
                  {
                      $rand6 = mt_rand(99,9999999).".".$file->extension();
                      $file->move(public_path('images'), $rand6);
                      $photoData6 = array('society_id'=>$society_id,'type'=>'photo','url'=>$rand6,'meta_key'=>'flat_images');
                      DB::table('op_society_photos')->insert($photoData6);      
                  }
              }

              if($request->hasfile('building_images'))
              {
                  foreach($request->file('building_images') as $file)
                  {
                      $rand7 = mt_rand(99,9999999).".".$file->extension();
                      $file->move(public_path('images'), $rand7);
                      $photoData7 = array('society_id'=>$society_id,'type'=>'photo','url'=>$rand7,'meta_key'=>'building_images');
                      DB::table('op_society_photos')->insert($photoData7);      
                  }
              }

              if($request->hasfile('building_video'))
              {
                  foreach($request->file('building_video') as $file)
                  {
                      $rand8 = mt_rand(99,9999999).".".$file->extension();
                      $file->move(public_path('images'), $rand8);
                      $photoData8 = array('society_id'=>$society_id,'type'=>'photo','url'=>$rand8,'meta_key'=>'building_video');
                      DB::table('op_society_photos')->insert($photoData8);      
                  }
              }
              //stoed all imgaes in sepaarte table with name

        $society_floor_rise_id = $request->society_floor_rise_id;      
        $floor_slab_to = $request->floor_slab_to;
        $floor_slab_from = $request->floor_slab_from;
        $configuration1 = $request->configuration1;
        $rs_per_sq_ft = $request->rs_per_sq_ft;
        $box_price = $request->box_price;
        
        $cnt = count($floor_slab_to);
        for ($iii=0; $iii<$cnt ; $iii++) {  
            $array3 = array('society_id'=>$society_id);

            // $floor_slab_to1 = $floor_slab_to[$i];
            // $floor_slab_from1 = $floor_slab_from[$i];
            // $configuration11 = $configuration1[$i];
            // $rs_per_sq_ft1 = $rs_per_sq_ft[$i];
            // $box_price1 = $box_price[$i];

            if(!empty($floor_slab_to[$iii])){
                $floor_slab_to1 = $floor_slab_to[$iii]; 
                $a = array('floor_slab_to'=>$floor_slab_to1) ;
                $array3=   array_merge($array3,$a); 
            }

            if(!empty($floor_slab_from[$iii])){
                $floor_slab_from1 = $floor_slab_from[$iii]; 
                $a = array('floor_slab_from'=>$floor_slab_from1) ;
                $array3=   array_merge($array3,$a); 
            }

            if(!empty($configuration1[$iii])){
                $configuration11 = $configuration1[$iii]; 
                $a = array('configuration'=>trim($configuration11)) ;
                $array3=   array_merge($array3,$a); 
            }

            if(!empty($rs_per_sq_ft[$iii])){
                $rs_per_sq_ft1 = $rs_per_sq_ft[$iii]; 
                $a = array('rs_per_sq_ft'=>$rs_per_sq_ft1) ;
                $array3=   array_merge($array3,$a); 
            }

            if(!empty($box_price[$iii])){
                $box_price1 = $box_price[$iii]; 
                $a = array('box_price'=>$box_price1) ;
                $array3=   array_merge($array3,$a); 
            }   
            // echo "/n"; print_r($iii);

            if($society_floor_rise_id[$iii] == 0 ){   
                               
                if(!empty($array3['configuration'])){
                    DB::table('op_society_floor_rise')->insert($array3);                
                }                    
            }else{                               
                DB::table('op_society_floor_rise')->where('society_floor_rise_id',$society_floor_rise_id[$iii])->update($array3);
            } 

            // DB::table('op_society_floor_rise')->insert($array);
        }
            
            $society_confiuration_id = $request->society_confiuration_id;
            $sample_flat_available = $request->sample_flat_available;
            $configuration = $request->configuration;
            $configuration_size = $request->configuration_size;
            $area = $request->area;
            $total_falt_available = $request->total_falt_available;
            $flat_available = $request->flat_available;

            $cnt1 = count($sample_flat_available);
            for ($ii=0; $ii<$cnt1 ; $ii++) {  
                // $sample_flat_available1 = $sample_flat_available[$ii];
                // $configuration1 = $configuration[$ii];
                // $configuration_size1 = $configuration_size[$ii];
                // $area1 = $area[$ii];
                // $total_falt_available1 = $total_falt_available[$ii];
                // $flat_available1 = $flat_available[$ii];
                $array1 = array('society_id'=>$society_id);

                if(!empty($sample_flat_available[$ii])){
                    $sample_flat_available1 = $sample_flat_available[$ii]; 
                    $a1 = array('sample_flat_available'=>$sample_flat_available1) ;
                    $array1 = array_merge($array1,$a1);                   
                }

                if(!empty($configuration[$ii])){
                    $configuration1 = $configuration[$ii]; 
                    $a1 = array('configuration_id'=>$configuration1) ;
                    $array1 = array_merge($array1,$a1);                   
                }

                if(!empty($configuration_size[$ii])){
                    $configuration_size1 = $configuration_size[$ii]; 
                    $a1 = array('suffix'=>$configuration_size1) ;
                    $array1 = array_merge($array1,$a1);                   
                }

                if(!empty($area[$ii])){
                    $area1 = $area[$ii]; 
                    $a1 = array('area'=>$area1) ;
                    $array1 = array_merge($array1,$a1);                   
                }
                
                if(!empty($total_falt_available[$ii])){
                    $total_falt_available1 = $total_falt_available[$ii]; 
                    $a1 = array('total_falt_available'=>$total_falt_available1) ;
                    $array1 = array_merge($array1,$a1);                   
                }

                if(!empty($flat_available[$ii])){
                    $flat_available1 = $flat_available[$ii]; 
                    $a1 = array('flat_available'=>$flat_available1) ;
                    $array1 = array_merge($array1,$a1);                   
                }
                //echo "/n"; print_r($ii);
                if($society_confiuration_id[$ii] == 0 ){                   
                    if(!empty($array1['area'])){
                        DB::table('op_society_configuration')->insert($array1);                
                    }                    
                }else{                               
                    DB::table('op_society_configuration')->where('society_confiuration_id',$society_confiuration_id[$ii])->update($array1);
                } 

                // DB::table('op_society_configuration')->insert($array);    
                //     break;
            }
        
            // exit();
              return response()->json(['success'=>'done','lastID'=>$society_id]);
       
        }

        return response()->json(['error'=>$validator->errors()->all()]); 

    }

    public function getDataSociety(Request $request)
    {        
        $form_type = $request->form_type;
        if($form_type == 'pros_cons'){
            $op_society_pros_cons_id = $request->op_society_pros_cons_id;
            $getData = DB::select("select * from op_society_pros_cons where op_society_pros_cons_id =".$op_society_pros_cons_id." ");
            return $getData;
        }

        $form_type = $request->form_type;
        if($form_type == 'office_timing'){
            $society_working_days_time_id = $request->society_working_days_time_id;
            $getData = DB::select("select * from society_working_days_time where society_working_days_time_id =".$society_working_days_time_id." ");
            return $getData;
        }

        if($form_type == 'hierarchy'){
            $society_keymembers_id = $request->society_keymembers_id;
            $getData = DB::select("SELECT h.*,(select designation_name from dd_std_designation where designation_id= h.designation_id ) as designation_name  FROM op_society_keymembers as h  where h.society_keymembers_id=".$society_keymembers_id."  ");
            return $getData;
        }
        if($form_type =="construction_stage"){
            $society_construction_stage_id = $request->society_construction_stage_id;
            $getData = DB::select("select * from society_construction_stage where society_construction_stage_id=".$society_construction_stage_id."  ");
            return $getData;
        }
    }

    public function updateSocietyFormRecords(Request $request)
    {        
        $form_type = $request->form_type;
        if($form_type == 'pros_cons'){
             $op_society_pros_cons_id = $request->op_society_pros_cons_id;
             $checkData = DB::select("select * from op_society_pros_cons where op_society_pros_cons_id =".$op_society_pros_cons_id." ");
             if($checkData){
                 $data['pros_of_building'] = $request->pros_of_building;
                 $data['cons_of_building'] = $request->cons_of_building;
                 $data['user_id'] = $this->getUserId();
                 DB::table('op_society_pros_cons')->where('op_society_pros_cons_id',$op_society_pros_cons_id)->update($data); 
                 $getDate = DB::select("select updated_date from op_society_pros_cons where op_society_pros_cons_id = ".$op_society_pros_cons_id."");
                 $data['updated_date'] = $getDate[0]->updated_date; 
                 return response()->json(['success'=>'Record updated successfully.','op_society_pros_cons_id'=>$op_society_pros_cons_id,'data'=>$data]);   
 
             }else{
                 return response()->json(['error'=>'Record does not exist']);
             }                 
        }

        if($form_type == 'office_timing'){
            $society_working_days_time_id = $request->society_working_days_time_id;
            $checkData = DB::select("select * from society_working_days_time where society_working_days_time_id =".$society_working_days_time_id." ");
            if($checkData){
                $data['day_id'] = $request->working_days_and_time;
                $data['office_timing'] = $request->office_timing;
                $data['office_timing_to'] = $request->office_timing_to;
                $data['user_id'] = $this->getUserId();
                DB::table('society_working_days_time')->where('society_working_days_time_id',$society_working_days_time_id)->update($data); 
                $getDate = DB::select("select updated_date from society_working_days_time where society_working_days_time_id = ".$society_working_days_time_id."");
                $data['updated_date'] = $getDate[0]->updated_date; 
                $getDay = DB::select("select d.day from society_working_days_time as w join dd_days as d on d.day_id = w.day_id where society_working_days_time_id = ".$society_working_days_time_id."");
                $data['day'] = $getDay[0]->day;
                return response()->json(['success'=>'Record updated successfully.','society_working_days_time_id'=>$society_working_days_time_id,'data'=>$data]);   

            }else{
                return response()->json(['error'=>'Record does not exist']);
            }                 
        }

        if($form_type == 'hierarchy'){
          
            $society_keymembers_id = $request->society_keymembers_id;
            $checkData = DB::select("select society_keymembers_id from op_society_keymembers where society_keymembers_id =".$society_keymembers_id." ");
            // echo "<pre>"; print_r($checkData); exit();
            if($checkData){
                $data['name_initial'] = $request->name_initial;  
                $data['name'] = $request->name_h;
                $data['country_code_id'] = $request->country_code;
                $data['mobile_number'] = $request->mobile_number_h;
                $data['whatsapp_number'] = $request->whatsapp_number;
                $data['email_id'] = $request->primary_email_h;
                $data['designation_id'] = $request->designation_h;
                $data['residence_address'] = $request->residence_address_h;
                $data['user_id'] = $this->getUserId();                 
                    
                    DB::table('op_society_keymembers')->where('society_keymembers_id',$society_keymembers_id)->update($data); 
                    if(isset($request->designation_h) && !empty($request->designation_h)){
                    $designationID = DB::select("select designation_name from dd_std_designation where designation_id=".$request->designation_h." ");
                        if(isset($designationID[0]->designation_name) && !empty($designationID[0]->designation_name)){
                            $data['designation_name'] = $designationID[0]->designation_name;   
                        }else{
                            $data['designation_name'] = '';
                        }
                    }else{
                        $data['designation_name'] = '';
                    }
                    $getDate = DB::select("select updated_date from op_society_keymembers where society_keymembers_id = ".$society_keymembers_id."");
                    if(isset($getDate[0]->updated_date) && !empty($getDate[0]->updated_date)){
                        $data['updated_date'] = $getDate[0]->updated_date;            
                    }else{
                        $data['updated_date'] = '';
                    }  

                    if(!empty($request->name_initial)){
                        $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                        $data['initials_name'] = $Initial[0]->initials_name;
                    }else{
                        $data['initials_name'] = '';
                    }
                        
                    return response()->json(['success'=>'Record updated successfully.','society_keymembers_id'=>$society_keymembers_id,'data'=>$data]);   
    
                }else{
                        return response()->json(['error'=>'Record does not exist']);
                } 
            
        }

        if($form_type == 'construction_stage'){
            $society_construction_stage_id = $request->society_construction_stage_id;
            $checkData = DB::select("select society_construction_stage_id,	construction_stage_images from society_construction_stage where society_construction_stage_id =".$society_construction_stage_id." ");
            if($checkData){
            $data['building_construction_stage'] = $request->building_construction_stage;
            $data['building_construction_stage_date'] = $request->building_construction_stage_date;
            $data['comments_bc'] = $request->comments_bc;
            $data['user_id'] = $this->getUserId();
                $data1 = array();
                if($request->hasfile('construction_stage_images'))
                {
                    $array = array();
                    foreach($request->file('construction_stage_images') as $file)
                    {
                        $rand2 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('construction_stage_images'), $rand2);
                        array_push($array,$rand2);
                        
                    }
                    if(isset($checkData[0]->construction_stage_images) && !empty($checkData[0]->construction_stage_images)){
                        $a2 = json_decode($checkData[0]->construction_stage_images);
                        // echo "1";
                        $m = array_merge($array,$a2);
                        $f = json_encode($m);
                        $data['construction_stage_images'] = $f;
                        $data1['img'] = $m;
                    }else{
                        // echo "2";
                        $data['construction_stage_images'] = json_encode($array);
                        $data1['img'] = $array;                        
                    }                    
                }
                // echo "<pre>"; print_r($data1); die();

                DB::table('society_construction_stage')->where('society_construction_stage_id',$society_construction_stage_id)->update($data); 
                
                $getDate = DB::select("select created_date from society_construction_stage where society_construction_stage_id = ".$society_construction_stage_id." ");
                $data['created_date'] = $getDate[0]->created_date;

                $getConstructionStageN = DB::select("select construction_stage  from dd_construction_stage where construction_stage_id= ".$request->building_construction_stage." ");
                if(!empty($getConstructionStageN)){
                    $data['building_construction_stage'] = $getConstructionStageN[0]->construction_stage;
                }

                return response()->json(['success'=>'Record updated successfully.','society_construction_stage_id'=>$society_construction_stage_id,'data'=>$data,'data1'=>$data1]);   


            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }
        
    }

    public function deleteSocietyFormRecords(Request $request)
    {
       
        $form_type = $request->form_type;
        if($form_type=='pros_cons'){
            $op_society_pros_cons_id = $request->op_society_pros_cons_id;
            $check = DB::select("select * from op_society_pros_cons where op_society_pros_cons_id =".$op_society_pros_cons_id." ");
           
            if($check){
                DB::table('op_society_pros_cons')->where('op_society_pros_cons_id',$op_society_pros_cons_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','op_society_pros_cons_id'=>$op_society_pros_cons_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }


        if($form_type=='office_timing'){
            $society_working_days_time_id = $request->society_working_days_time_id;
            $check = DB::select("select * from society_working_days_time where society_working_days_time_id =".$society_working_days_time_id." ");
            // echo "<pre>"; print_r($check); exit();
            if($check){
                DB::table('society_working_days_time')->where('society_working_days_time_id',$society_working_days_time_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','society_working_days_time_id'=>$society_working_days_time_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }

        if($form_type=='hirarchy'){
            $society_keymembers_id = $request->society_keymembers_id;
            $check = DB::select("select * from op_society_keymembers where society_keymembers_id =".$society_keymembers_id." ");
            // echo "<pre>"; print_r($check); exit();
            if($check){
                DB::table('op_society_keymembers')->where('society_keymembers_id',$society_keymembers_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','society_keymembers_id'=>$society_keymembers_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        } 
        
        if($form_type=='constructionStage'){
            $society_construction_stage_id = $request->society_construction_stage_id;
            $check = DB::select("select * from society_construction_stage where society_construction_stage_id =".$society_construction_stage_id." ");
            // echo "<pre>"; print_r($check); exit();
            if($check){
                DB::table('society_construction_stage')->where('society_construction_stage_id',$society_construction_stage_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','society_construction_stage_id'=>$society_construction_stage_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        } 
      
        if($form_type=="society"){
           
            $society_id = $request->society_id;

            $checkInSociety = DB::select("select society_id from op_society where society_id = ".$society_id."");
            if(!empty($checkInSociety)){

                foreach ($checkInSociety as $key1 => $value1) {
                    $society_id = $value1->society_id;
                    $this->softDelete('op_society','society_id',$society_id);                            
                    
                    $checkInWing = DB::select("select wing_id from op_wing where society_id = ".$society_id."");
                    // echo "<pre>"; echo "checkInSociety"; print_r($checkInWing);
                    if(!empty($checkInWing)){
                         foreach ($checkInWing as $key2 => $value2) {
                             $wing_id = $value2->wing_id;
                             $this->softDelete('op_wing','wing_id',$wing_id);
                             $checkInUnit = DB::select("select unit_id from op_unit where wing_id = ".$wing_id."");
                            
                         }
                    }else{
                        echo "<pre>"; echo "delete from op_wing and return";
                    }
                }
                
            }
            return 1;


        }
    }

    public function softDelete($table,$column,$value)
    {   $data = array('status'=>0,'user_id'=>$this->getUserId());
        DB::table($table)->where($column,$value)->update($data); 

    }

    public function search_society_list(Request $request)
    {
        
        $search = $request->search;  
        $data = DB::table('op_society')
        ->selectRaw('op_society.*')//,op_wing.wing_name                                                    
        //  ->where('op_company.group_name','!=','')
          ->where('op_society.building_name', 'like', "%".$search."%") 
         ->where('op_society.status','=','1')
         ->orderBy('society_id', 'DESC')   
         ->paginate(15)->appends(['search'=>$search]);

         return view('society_master/society_master_list',['data'=>$data]);
    }

    public function updateUnitAmenity(Request $request)
    {
        // echo "<pre>"; print_r($request->all());

        $society_unit_amnities_id = $request->society_unit_amnities_id;
        $exp = explode(",",$society_unit_amnities_id);
        $exp = array_unique($exp);
        $imp= implode(',',$exp);
        $society_id  = $request->society_id;
        $form_type = $request->form_type;

        if($form_type == 'uuc'){
            $getData = DB::select("select society_unit_amnities_id from op_society_unit_amnities where society_id= ".$society_id."  and society_unit_amnities_id not in(".$imp.") ");     
            echo "<pre>"; print_r($getData); die();
            if(!empty($getData)){
                foreach( $getData as $val1){           
                    DB::table('op_society_amnities')->where('society_amnities_id',$val1->society_amnities_id)->delete();
                }


                $checkData = DB::select("select society_unit_amnities_id,amenities_id from op_society_unit_amnities where society_id= ".$society_id." ");
                // echo "<pre>"; print_r($checkData); die();

                $getData = array();
                $getAmeData = array();
                foreach ($checkData as $key => $value) {
                    array_push($getData,$value->amenities_id);
                    array_push($getAmeData,$value->society_unit_amnities_id);
                }

                $result=array_diff($exp,$getData);

                

                $lastIds = array();
                    foreach ($result as $key1 => $value1) {
                    

                        $data1['society_id'] = $society_id;
                        $data1['amenities_id'] = $value1;

                        DB::table('op_society_unit_amnities')->insert($data1);
                        $lid= DB::getPdo()->lastInsertId();

                        array_push($lastIds,$lid);
                    }

                    $finalArray = array_merge($getAmeData,$lastIds);


                    $implode = implode(',',$finalArray);
                    
                    $getAmeId = DB::select("select opa.society_unit_amnities_id,opa.amenities_id,pa.amenities,opa.description   from op_society_unit_amnities as opa    join dd_unit_amenities as pa on pa.unit_amenities_id= opa.amenities_id   where opa.society_id=".$society_id." and opa.society_unit_amnities_id in (".$implode.") order by pa.amenities asc");
                    


                    if( $form_type == 'reg' ){

                        foreach ($getAmeId  as $key2 => $value2) {
                            $html = "";
                            $html ='<div class="col l4 m4 s12">
                            <div class="col l6 m s12 input-field col l3 m4 s12 display_search">'.ucwords($value2->amenities).'       
                            </div>
                            <div class="col l3 m3 s12" style="display:none">
                                <label><input type="checkbox" name="is_availableR[]" id="is_availableR" value="'.$value2->society_amnities_id.'" checked ><span>Available</span></label> updateAmenityDesc
                            </div>
                            <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                                <label  class="active">Description: </label>
                                    <input type="text" class="description_ameR validate" name="description_ameR[]" id="description_ameR"  placeholder="Enter" style="height: 36px;" value="'.$value2->description.'" >
                            </div>
                            </div>';
                            print_r($html);
                        }
                    }elseif( $form_type == 'uuc'  ) {
                        foreach ($getAmeId  as $key2 => $value2) {
                            $html = "";
                            $html ='<div class="col l4 m4 s12">
                            <div class="col l6 m s12 input-field col l3 m4 s12 display_search">'.ucwords($value2->amenities).'       
                            </div>
                            <div class="col l3 m3 s12" style="display:none">
                                <label><input type="checkbox" name="is_unit_availableUc[]" id="is_unit_availableUc" value="'.$value2->society_amnities_id.'" checked ><span>Available</span></label> updateAmenityDesc
                            </div>
                            <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                                <label  class="active">Description: </label>
                                    <input type="text" class="description_ameUc validate" name="description_ameUc[]" id="description_ameUc"  placeholder="Enter" style="height: 36px;" value="'.$value2->description.'" >
                            </div>
                            </div>';
                            print_r($html);
                        }
                    }   
       
        
            }
        }
        
    }



    public function updateAmenity(Request $request)
    {        
    //    echo "<pre>"; print_r($request->all()); die();
        $project_amenities_id = $request->project_amenities_id;
        $exp = explode(",",$project_amenities_id);
        $exp = array_unique($exp);
        $imp= implode(',',$exp);
        $society_id  = $request->society_id;
        $form_type = $request->form_type;

        $getData = DB::select("select society_amnities_id from op_society_amnities where society_id= ".$society_id."  and amenities_id not in(".$imp.") ");

        // echo "<pre>"; print_r($getData); die();

        foreach( $getData as $val1){           
            DB::table('op_society_amnities')->where('society_amnities_id',$val1->society_amnities_id)->delete();
        }

       $checkData = DB::select("select society_amnities_id,amenities_id from op_society_amnities where society_id= ".$society_id." ");

       $getData = array();
       $getAmeData = array();
       foreach ($checkData as $key => $value) {
           array_push($getData,$value->amenities_id);
           array_push($getAmeData,$value->society_amnities_id);
       }

       $result=array_diff($exp,$getData);

       $lastIds = array();
        foreach ($result as $key1 => $value1) {
           

            $data1['society_id'] = $society_id;
            $data1['amenities_id'] = $value1;
            $data1['user_id'] = $this->getUserId();

            DB::table('op_society_amnities')->insert($data1);
            $lid= DB::getPdo()->lastInsertId();

            array_push($lastIds,$lid);
        }

        $finalArray = array_merge($getAmeData,$lastIds);


        $implode = implode(',',$finalArray);

        $getAmeId = DB::select("select opa.society_amnities_id,opa.amenities_id,pa.amenities,opa.description from op_society_amnities as opa 
        join dd_project_amenities as pa on pa.project_amenities_id = opa.amenities_id
        where opa.society_id=".$society_id." and opa.society_amnities_id in (".$implode.") order by pa.amenities asc");


        if( $form_type == 'reg' ){

            foreach ($getAmeId  as $key2 => $value2) {
                $html = "";
                $html ='<div class="col l4 m4 s12">
                <div class="col l6 m s12 input-field col l3 m4 s12 display_search">'.ucwords($value2->amenities).'       
                </div>
                <div class="col l3 m3 s12" style="display:none">
                    <label><input type="checkbox" name="is_availableR[]" id="is_availableR" value="'.$value2->society_amnities_id.'" checked ><span>Available</span></label> updateAmenityDesc
                </div>
                <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                    <label  class="active">Description: </label>
                        <input type="text" class="description_ameR validate" name="description_ameR[]" id="description_ameR"  placeholder="Enter" style="height: 36px;" value="'.$value2->description.'" >
                </div>
                </div>';
                print_r($html);
            }
        }elseif( $form_type == 'uc'  ) {
            foreach ($getAmeId  as $key2 => $value2) {
                $html = "";
                $html ='<div class="col l4 m4 s12">
                <div class="col l6 m s12 input-field col l3 m4 s12 display_search">'.ucwords($value2->amenities).'       
                </div>
                <div class="col l3 m3 s12" style="display:none">
                    <label><input type="checkbox" name="is_availableUC[]" id="is_availableUC" value="'.$value2->society_amnities_id.'" checked ><span>Available</span></label> updateAmenityDesc
                </div>
                <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                    <label  class="active">Description: </label>
                        <input type="text" class="description_ameUC validate" name="description_ameUC[]" id="description_ameUC"  placeholder="Enter" style="height: 36px;" value="'.$value2->description.'" >
                </div>
                </div>';
                print_r($html);
            }
        }   
        
    }

    public function createEmptySociety(Request $request)
    {
        // echo "<pre>"; print_r( $request->all() );
        if(isset($request->project_id) && !empty($request->project_id)){
            $data1['project_id'] =  $request->project_id;
            $data1['user_id'] = $this->getUserId();
            DB::table('op_society')->insert($data1); 
            return DB::getPdo()->lastInsertId();
        }else{
            return 0;
        }
    }


        // society list
        public function society_list(Request $request)
        {
            $data = DB::table('op_society')
            ->selectRaw('op_society.*')//,op_wing.wing_name                                                    
             ->where('op_society.building_name','!=','')
             //  ->where('op_project.group_name', 'like', "%".$search."%") 
             ->where('op_society.status','=','1')
             ->orderBy('society_id', 'DESC')        
             ->paginate(15);  
             
             return view('society_master/society_master_list',['data'=>$data]);
        }

        public function getSublocationId(Request $request)
        {           

            $getProjectSublocation = DB::select("select sub_location_id from op_project where project_id =".$request->project_id." ");
            if(!empty($getProjectSublocation)){
                $sub_location_idP = $getProjectSublocation[0]->sub_location_id;
            }else{
                $sub_location_idP = '';
            }

            return $sub_location_idP;
        }


        
    public function delImageS(Request $request)
    {
      
        $society_photos_id = $request->society_photos_id;
        if(!empty( $society_photos_id )){
            DB::table('op_society_photos')->where('society_photos_id',$society_photos_id)->delete(); 
        }
        
    }

    public function delImageConst(Request $request)
    {
        $id = $request->id;
        $image = $request->image;
        $getImgData = DB::select("select construction_stage_images from society_construction_stage where society_construction_stage_id=".$id." ");       

        $construction_stage_images = json_decode($getImgData[0]->construction_stage_images);
        $final_array= array_diff($construction_stage_images, array($image));
        $enc = json_encode(array_values($final_array));
        DB::table('society_construction_stage')->where('society_construction_stage_id',$id)->update(array('construction_stage_images'=>$enc));   
    }

    public function delImageRS(Request $request)
    {       

        $image = $request->image;
        $type = $request->type;
        $society_id = $request->society_id;

        if(  $type == 'oc' ){
            $getCPA = DB::select('select  oc_certificate from op_society where society_id ='.$society_id.' ');
            $oc_certificate = json_decode($getCPA[0]->oc_certificate);
            $final_array= array_diff($oc_certificate, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_society')->where('society_id',$society_id)->update(array('oc_certificate'=>$enc,'user_id'=>$this->getUserId()));   
        }

        if(  $type == 'rc' ){
            $getCPA = DB::select('select  registration_certificate from op_society where society_id ='.$society_id.' ');
            $registration_certificate = json_decode($getCPA[0]->registration_certificate);
            $final_array= array_diff($registration_certificate, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_society')->where('society_id',$society_id)->update(array('registration_certificate'=>$enc,'user_id'=>$this->getUserId()));   
        }

        if(  $type == 'cd' ){
            $getCPA = DB::select('select  conveyance_deed from op_society where society_id ='.$society_id.' ');
            $conveyance_deed = json_decode($getCPA[0]->conveyance_deed);
            $final_array= array_diff($conveyance_deed, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_society')->where('society_id',$society_id)->update(array('conveyance_deed'=>$enc,'user_id'=>$this->getUserId()));   
        }

    }
}
