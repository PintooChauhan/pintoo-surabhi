<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Traits\DatabaseTrait;

class PCMasterController extends Controller
{
    use DatabaseTrait;
    public function pc_master_list(){

        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }
        

        /************* Role Check Start  *********************/
        $condition_name = 'property_consultant';
        $page_action = 's_view';
        $action_status = 1;
        $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

        $data['access_check'] = $access_check;

        /************* Role Check End  *********************/

        if(!empty($access_check)){

            $user = Auth::user();         
            //    echo "<pre>"; print_r( $user); die();
            if(empty($user)){
                Auth::logout();
                return redirect('login');
            }


            $role = $this->getRole();
            $roles = $this->adminRoles();
            if(!in_array($role,$roles)){
                Auth::logout();
                return redirect('login');
            }
            
            $data['getPcData'] = DB::select("  select pc.property_consultant_id, 
            pc.company_name,          
            (select count(pc_branch_id) from tbl_pc_branch where property_consultant_id = pc.property_consultant_id and branch_name != '' ) as branch_cnt,
            (select count(pc_employees_id) from tbl_pc_employees where property_consultant_id = pc.property_consultant_id and employee_name!= '' ) as employee_cnt,        
            (select sub_location_name from dd_sub_location where sub_location_id = pc.sub_location) as sub_location1 , 
            (select location_name from dd_location where location_id = pc.location ) as location1 , 
            (select city_name from dd_city where city_id= pc.city) as city1,
            pc.office_address,
            pc.rera_number,
            pc.no_of_years,
            pc.created_date,
            (select name from users where id = pc.user_id) as usernanme
            from tbl_property_consultant as pc
            where pc.company_name !='' and pc.del_status='1' group by pc.property_consultant_id, pc.company_name, 
            sub_location1,location1,city1,office_address,no_of_years,rera_number,created_date,usernanme order by
            pc.company_name asc");
            $data['mobile_number'] = '';
            return view('property_consultant/pc_list_view_new',$data);
            
        }else{
            return Redirect()->back()->with('error','You do not have access to this page.');
        }
    }

    public function pc_master(Request $request)
    {
        $user = Auth::user();  
        // echo "<pre>"; print_r($user); die();
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }

        /************* Role Check Start  *********************/
        $condition_name = 'property_consultant';
        if(isset($property_consultant_id) && !empty($property_consultant_id)){
            $page_action = 's_edit';
        }else{
            $page_action = 's_add';
        }

        $action_status = 1;
        $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

        /************* Role Check End  *********************/

        if(!empty($access_check)){

            $role = $this->getRole();
            $roles = array('super_admin');
            if(!in_array($role,$roles)){
                Auth::logout();
                return redirect('login');
            }
            $property_consultant_id = $request->pc_id;
            $data['sub_location'] = $this->getAllData('d','sub_location');
            $data['location'] =  $this->getAllData('d','location');
            $data['city'] = $this->getAllData('d','city');
            $data['state'] = $this->getAllData('d','state'); 
            $data['social_site'] = $this->getAllData('d','social_site');
            $data['rating'] = $this->getAllData('d','rating');  
            $data['property_consultant_id'] = $property_consultant_id;
            $data['country'] = $this->getAllData('d','country');


            if(isset($property_consultant_id) && !empty($property_consultant_id)){
                $data['getSinglePc'] = DB::select("select * from tbl_property_consultant where property_consultant_id =".$property_consultant_id." ");
                
                $data['getSocialData'] = DB::select("select pc.property_consultant_social_media_id, pc.property_consultant_id, pc.url,pc.created_date,
                (select social_site from dd_social_site where social_site_id = pc.social_site) as social_site 
                from tbl_property_consultant_social_media as pc where pc.property_consultant_id =".$property_consultant_id." ");
            }else{
                $data['getSinglePc'] = array();
                $data['getReviewData'] = array();
                $data['getSocialData'] = array();
            }

            return view('property_consultant/pc_master',$data);

        }else{
            return Redirect()->back()->with('error','You do not have access to this page');
        }
    }

    public function add_pc(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
      
        $property_consultant_id = $request->pc_id;
        $data['sub_location'] = $this->getAllData('d','sub_location');
        $data['location'] =  $this->getAllData('d','location');
        $data['city'] = $this->getAllData('d','city');
        $data['state'] = $this->getAllData('d','state'); 
        $data['rating'] = $this->getAllData('d','rating'); 
        $data['property_consultant_id'] = $property_consultant_id;

        if(isset($property_consultant_id) && !empty($property_consultant_id)){
            $data['getSinglePc'] = DB::select("select * from tbl_property_consultant where property_consultant_id =".$property_consultant_id." ");
        }else{
            $data['getSinglePc'] = array();
        }

       
       
        $data['getPcData'] = DB::select("select pc.*, (select sub_location_name from dd_sub_location where sub_location_id = pc.sub_location) as sub_location1 
        , (select location_name from dd_location where location_id = pc.location ) as location1 
        , (select city_name from dd_city where city_id= pc.city) as city1
        , (select state_name from dd_state  where dd_state_id= pc.state) as state1
        from tbl_property_consultant as pc order by property_consultant_id desc");
        return view('property_consultant/add_new_pc',$data);
    }

    public function savePc(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
        $user_id = $this->getUserId();
        $property_consultant_id = $request->property_consultant_id;
        $data['company_name'] = $request->company_name;
        $data['firm_type'] = $request->firm_type;
        $data['rera_number'] = $request->rera_number;
        $data['office_address'] = $request->reg_office_address;
        $data['web_site'] = $request->web_site;
        $data['office_number'] = $request->office_number;
        $data['email_id'] = $request->office_email_id;
        $data['sub_location'] = $request->sub_location;
        $data['location'] = $request->location;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        
        $data['about_company'] = $request->about_company;
        $data['about_owner'] = $request->about_owner;
        $data['inception_year'] = $request->inception_year;
        $data['no_of_years'] = $request->no_of_years;
        $data['comment'] = $request->comments;
        $data['user_id'] = $user_id;
        $data['lattitde_longitude'] = $request->lattitde_longitude;
        $data['reg_office_address'] = $request->reg_office_address;
        

        if( isset($property_consultant_id) && !empty($property_consultant_id) ){
            // update tbl_customer
         DB::table('tbl_property_consultant')->where('property_consultant_id',$property_consultant_id)->update($data);      
        }else{
            DB::table('tbl_property_consultant')->insert($data);
            $data['property_consultant_id']  =DB::getPdo()->lastInsertId();
        }

    }

    public function saveData(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
        $user_id = $this->getUserId();
        
       $property_consultant_id = $request->property_consultant_id;
       if(empty($property_consultant_id)){
        $data  =array('company_name'=>'');
        DB::table('tbl_property_consultant')->insert($data);
        $property_consultant_id = DB::getPdo()->lastInsertId();
       }else{
        $property_consultant_id = $property_consultant_id;
       }
       $data['property_consultant_id']  = $property_consultant_id;
       $form_type = $request->form_type;

       if($form_type=='review'){
        // echo "<pre>"; print_r(date('Y-m-d H:i')); die();
        $data1['pc_branch_id'] = $request->pc_branch_id;
        $data1['parameters'] = $request->parameters;
        $data1['review'] = $request->review;
        $data1['rating'] = $request->rating;
        $data1['user_id'] = $user_id;
        $data1['created_date'] =  date('Y-m-d H:i');
        DB::table('tbl_property_consultant_review')->insert($data1);
        $property_consultant_review_id = DB::getPdo()->lastInsertId();
        $getDate = DB::select("select created_date from tbl_property_consultant_review where property_consultant_review_id = ".$property_consultant_review_id."");
        $data1['updated_date'] = date_format(date_create($getDate[0]->created_date),"d-m-Y H:i");
        $data1['username'] = $this->getName();
        $data1['property_consultant_review_id'] = $property_consultant_review_id;

        return response()->json($data1);  
        }
        if($form_type=='social'){
            $data['url'] = $request->social_url;
            $data['social_site'] = $request->social_site;
            $data['user_id'] = $user_id;
            DB::table('tbl_property_consultant_social_media')->insert($data);
            $property_consultant_social_media_id = DB::getPdo()->lastInsertId();
            $getDate = DB::select("select created_date from tbl_property_consultant_social_media where property_consultant_social_media_id = ".$property_consultant_social_media_id."");
            $data['updated_date'] = $getDate[0]->created_date;
             if(!empty($request->social_site)){
                $getData = DB::select("select social_site from dd_social_site where social_site_id = ".$request->social_site."");
                $data['social_site'] = ucfirst($getData[0]->social_site);
             }
            $data['property_consultant_social_media_id'] = $property_consultant_social_media_id;
        }
   

       return response()->json($data);  
    }

    public function getData(Request $request)
    {
        $form_type = $request->form_type;
        if($form_type == 'review'){
            $property_consultant_review_id = $request->property_consultant_review_id;
            $getData = DB::select("select * from tbl_property_consultant_review where property_consultant_review_id =".$property_consultant_review_id."  ");

        }

        if($form_type == 'social'){
            $property_consultant_social_media_id = $request->property_consultant_social_media_id;
            $getData = DB::select("select * from tbl_property_consultant_social_media where property_consultant_social_media_id =".$property_consultant_social_media_id."  ");

        }

        return $getData;   
    }

    public function updateTableRecords(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }   

        $form_type = $request->form_type;
        $user_id = $this->getUserId();
        if($form_type == 'review'){
            $property_consultant_review_id = $request->property_consultant_review_id;
            $check = DB::select("select property_consultant_review_id  from tbl_property_consultant_review where property_consultant_review_id = ".$property_consultant_review_id." ");
            if($check){

                $data['parameters'] = $request->parameters;
                $data['review'] = $request->review;
                $data['rating'] = $request->rating;
                $data['user_id'] = $user_id;
                $data['created_date'] =  date('Y-m-d H:i');
                DB::table('tbl_property_consultant_review')->where('property_consultant_review_id',$property_consultant_review_id)->update($data); 
                $getDate = DB::select("select created_date from tbl_property_consultant_review where property_consultant_review_id = ".$property_consultant_review_id."");
                $data['updated_date'] = date_format(date_create($getDate[0]->created_date),"d-m-Y H:i");
                $data['username'] = $this->getName();
                
                return response()->json(['success'=>'Record updated successfully.','property_consultant_review_id'=>$property_consultant_review_id,'data'=>$data]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }

        
        if($form_type == 'social'){
            $property_consultant_social_media_id = $request->property_consultant_social_media_id;
            $check = DB::select("select property_consultant_social_media_id  from tbl_property_consultant_social_media where property_consultant_social_media_id = ".$property_consultant_social_media_id." ");
            if($check){

                $data['url'] = $request->social_url;
                $data['social_site'] = $request->social_site;
               

                DB::table('tbl_property_consultant_social_media')->where('property_consultant_social_media_id',$property_consultant_social_media_id)->update($data); 
                $getDate = DB::select("select created_date from tbl_property_consultant_social_media where property_consultant_social_media_id = ".$property_consultant_social_media_id."");
                if(!empty($request->social_site)){
                    $getData = DB::select("select social_site from dd_social_site where social_site_id = ".$request->social_site."");
                    $data['social_site'] = ucfirst($getData[0]->social_site);
                 }
                $data['updated_date'] = $getDate[0]->created_date;
   
                
                return response()->json(['success'=>'Record updated successfully.','property_consultant_social_media_id'=>$property_consultant_social_media_id,'data'=>$data]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 

        }

    }


    public function branch_information(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
        $pc_branch_id = $request->bi_id;
        $property_consultant_id = $request->pc_id;
        $data['property_consultant_id'] =  $property_consultant_id;
        $data['sub_location'] = $this->getAllData('d','sub_location');
        $data['location'] =  $this->getAllData('d','location');
        $data['city'] = $this->getAllData('d','city');
        $data['state'] = $this->getAllData('d','state'); 
        $data['rating'] = $this->getAllData('d','rating'); 
        $data['unit_type'] = $this->getAllData('d','unit_type'); 
        $data['unit_status'] = $this->getAllData('d','unit_status'); 
        $data['lead_source'] = DB::select("select * from dd_lead_source where lead_source_name not in('call - in bound','call - out bound','sms','email -inbound','email out bound','walk in') ");
        $data['lead_campaign'] = $this->getAllData('d','lead_campaign');
        $data['deal_type'] = $this->getAllData('d','deal_type');
        $data['complex'] = DB::select('select project_id,project_complex_name from op_project where status="1" ');
        // select (select project_id from op_project where project_id = os.project_id) as project_id,
        // (select project_complex_name from op_project where project_id = os.project_id) as project_complex_name ,
        // os.building_name,os.society_id from op_society as os where os.status="1"
        $data['getPcData'] = DB::select('select pb.*,(select name from users where id= pb.account_owner) as sr_account_owner,
        (select project_complex_name from op_project where project_id = pb.focused_complex ) as project_complex_name, 
        (select sub_location_name from dd_sub_location where sub_location_id = pb.sub_location)  as sub_location_name
         from tbl_pc_branch as pb where pb.property_consultant_id = '.$property_consultant_id.'  and pb.del_status="1" ');

        
        $data['pc_branch_id'] = $pc_branch_id;
        $data['company'] = DB::select("select property_consultant_id,company_name from tbl_property_consultant where company_name !='' and property_consultant_id =".$property_consultant_id." ");
        $data['users'] = DB::select("select id ,name from users ");
         if(isset($pc_branch_id) && !empty($pc_branch_id)){
            $data['getSinglePc'] = DB::select("select * from tbl_pc_branch where pc_branch_id =".$pc_branch_id." ");
            //  $data['getReviewData'] = array();
         $data['getReviewData'] = DB::select("select *,
         (select name from users where id = user_id) as usernanme
          from tbl_property_consultant_review where pc_branch_id =".$pc_branch_id." ");

        }else{
            $data['getSinglePc'] = array();
            $data['getReviewData'] = array();
        }
        // echo "<pre>"; print_r($data['company'] ); die();

        return view('property_consultant/branch_information',$data);
    }

    public function saveBranch(Request $request)
    {

        // echo "<pre>"; print_r($request->all()); die();
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }

        $pc_branch_id = $request->pc_branch_id;
        $data['branch_name'] = $request->branch_name;
        $data['property_consultant_id'] = $request->company_name;
        $data['latitude_and_longitude'] = $request->latitude_and_longitude;
        $data['address'] = $request->address;
        $data['focused_sub_location'] = json_encode($request->focused_sub_location);
        $data['focused_location'] = json_encode($request->focused_location);
        $data['focused_complex'] = json_encode($request->focused_complex);
        $data['focused_unit_type'] = json_encode($request->focused_unit_type);
        $data['sub_location'] = $request->sub_location;
        $data['location'] = $request->location;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['focused_unit_status'] = json_encode($request->focused_unit_status);
        $data['account_owner'] = $request->account_owner;

        $data['office_status'] = $request->office_status;
        $data['ownership_status'] = $request->ownership_status;
        $data['campaign_id'] = json_encode($request->campaign_id);
        $data['lead_source_name'] = json_encode($request->lead_source_name);
        $data['postal_address'] = $request->postal_address;
        $data['deal_type'] = json_encode($request->deal_type);


        if( isset($pc_branch_id) && !empty($pc_branch_id) ){
            // update tbl_customer
         DB::table('tbl_pc_branch')->where('pc_branch_id',$pc_branch_id)->update($data);      
        }else{
            DB::table('tbl_pc_branch')->insert($data);
            $pc_branch_id  =DB::getPdo()->lastInsertId();
        }
    }

    public function employee_information(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
        $pc_employees_id = $request->pce_id;
        $property_consultant_id = $request->pc_id;
        $data['property_consultant_id'] =  $property_consultant_id;
        $data['country'] = $this->getAllData('d','country');
        $data['designation'] = $this->getAllData('d','designation1');
        // $data['rating'] = $this->getAllData('d','rating');
        $data['branch'] = DB::select("select pb.pc_branch_id,pb.branch_name, pc.company_name,
        (select sub_location_name from dd_sub_location where sub_location_id = pb.sub_location ) as sub_location_name
        from tbl_pc_branch as pb
       join tbl_property_consultant  as pc on pc.property_consultant_id=pb.property_consultant_id
        where pb.sub_location !='' and pb.del_status='1'  and pc.property_consultant_id ='".$property_consultant_id."' ");
        $data['location'] =  $this->getAllData('d','location');
        $data['city'] = $this->getAllData('d','city');
        $data['state'] = $this->getAllData('d','state'); 
        $data['rating'] = $this->getAllData('d','rating'); 
        $data['pc_employees_id'] = $pc_employees_id;
        $data['initials'] = $this->getAllData('d','initials');
        $data['company'] = DB::select("select property_consultant_id,company_name from tbl_property_consultant where company_name !='' and property_consultant_id =".$property_consultant_id." ");
        if(isset($pc_employees_id) && !empty($pc_employees_id)){
            $data['getSinglePc'] = DB::select("select * from tbl_pc_employees where pc_employees_id =".$pc_employees_id." ");
            $data['getReviewData'] = DB::select("select *, (select name from users where id = user_id) as usernanme from tbl_pc_employees_review where pc_employees_id =".$pc_employees_id." ");
        }else{
            $data['getSinglePc'] = array();
            $data['getReviewData'] = array();
        }

        // echo "<pre>"; print_r($data['company']); die();


        $data['getPcData'] = DB::select("select pe.* ,(select country_code from dd_country where country_code_id= pe.primary_country_code_id) as primary_country_code,
         (select sub_location_name from dd_sub_location where sub_location_id = (select sub_location from tbl_pc_branch where pc_branch_id = pe.pc_branch_id )) as sub_location_name,
        (select country_code from dd_country where country_code_id= pe.secondary_country_code_id) as secondary_country_code,
        (select country_code from dd_country where country_code_id= pe.primary_wa_countryc_code) as primary_wa_countryc_code1,
        (select country_code from dd_country where country_code_id= pe.secondary_wa_countryc_code) as secondary_wa_countryc_code1,
        (select branch_name from tbl_pc_branch where pc_branch_id = pe.pc_branch_id) as branch_name ,
        (select designation from dd_designation1 where dd_designation1_id = pe.designation ) as designation1,
        (select focused_unit_type from tbl_pc_branch where pc_branch_id= pe.pc_branch_id) as focused_unit_type,
        (select focused_unit_status from tbl_pc_branch where pc_branch_id= pe.pc_branch_id) as focused_unit_status,
        (select focused_location from tbl_pc_branch where pc_branch_id= pe.pc_branch_id) as focused_location ,
        (select focused_sub_location from tbl_pc_branch where pc_branch_id= pe.pc_branch_id) as focused_sub_location,
        (select focused_complex from tbl_pc_branch where pc_branch_id= pe.pc_branch_id) as focused_complex
        from tbl_pc_employees as pe where pe.property_consultant_id=".$property_consultant_id." and del_status='1' ");
        return view('property_consultant/employee_information',$data);
    }

    public function saveEmployee(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die();
       
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }

        if(isset($request->primary_email)){
            $validator = Validator::make($request->all(), [
                'primary_email' => 'email',                
            ],            
            );
            if ($validator->fails()) {
                       $data['msg'] = 'Enter valid email';
                $data['error'] = 'failed';  
                return $data;
            }
        }
        // die();
        $pc_employees_id = $request->pc_employees_id;
        
        if(isset($request->moved_to) && !empty($request->moved_to)){
            $getPC_id = DB::select("select property_consultant_id from tbl_pc_branch where pc_branch_id =".$request->moved_to." ");
            $property_consultant_id = $getPC_id[0]->property_consultant_id;
            $data['property_consultant_id'] = $property_consultant_id;
            $data['pc_branch_id'] = $request->moved_to;
            echo '111';
        }else{
            $data['property_consultant_id'] = $request->property_consultant_id;
            $data['pc_branch_id'] = $request->branch;
        }
       // echo "<pre>"; print_r( $data); die();
        $data['employee_name'] = $request->employee_name;
        $data['approachable'] = $request->approachable;
        $data['primary_country_code_id'] = $request->primary_country_code_id;
        $data['primary_mobile_number'] = $request->primary_mobile_number;
        $data['secondary_country_code_id'] = $request->secondary_country_code_id;
        $data['seccondary_mobile_number'] = $request->seccondary_mobile_number;
        $data['primary_wa_countryc_code'] = $request->primary_wa_countryc_code;
        $data['primary_wa_number'] = $request->primary_wa_number;
        $data['secondary_wa_countryc_code'] = $request->secondary_wa_countryc_code;
        $data['secondary_wa_number'] = $request->secondary_wa_number;
        $data['designation'] = $request->designation;
        $data['primary_email'] = $request->primary_email;
        $data['secondary_email'] = $request->secondary_email;
        $data['date_of_birth'] = $request->date_of_birth;
        $data['employee_rating'] = $request->rating;
        $data['comment'] = $request->comment;
        $data['status'] = $request->status;
        $data['about_him'] = $request->about_him;
        $data['residence_address'] = $request->residence_address;
        $data['moved_to'] = $request->moved_to;
        // echo "<pre>"; print_r($data);die();

        if( $request->hasfile('photo') )
        { 
            $rand1 = mt_rand(99,9999999).".".$request->photo->extension();                    
            $request->photo->move(public_path('emp_photo'), $rand1);
            $data['photo'] = $rand1;
        }else{
            $data['photo'] = '';
        }
        // echo "<pre>"; print_r($data); die();

        if( isset($pc_employees_id) && !empty($pc_employees_id) ){
            // update tbl_customer
         DB::table('tbl_pc_employees')->where('pc_employees_id',$pc_employees_id)->update($data);      
        }else{
            DB::table('tbl_pc_employees')->insert($data);
            $pc_employees_id  =DB::getPdo()->lastInsertId();
        }
    }

    public function saveDataReview(Request $request)
    {
    //    echo "<pre>"; print_r($request->all()); die();
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
       $user_id = $this->getUserId();
       $pc_employees_id = $request->pc_employees_id;
       if(empty($pc_employees_id)){
        $data1  =array('employee_name'=>'');
        DB::table('tbl_pc_employees')->insert($data1);
        $pc_employees_id = DB::getPdo()->lastInsertId();
       }else{
        $pc_employees_id = $pc_employees_id;
       }

       $data['pc_employees_id']  = $pc_employees_id;
      
       $form_type = $request->form_type;

       if($form_type=='review'){
        $data['parameters'] = $request->parameters;
        $data['rating'] = $request->rating;
        $data['review'] = $request->review;
        $data['user_id'] = $user_id;
        
        DB::table('tbl_pc_employees_review')->insert($data);
        $pc_employees_review_id = DB::getPdo()->lastInsertId();
        $getDate = DB::select("select created_date from tbl_pc_employees_review where pc_employees_review_id = ".$pc_employees_review_id."");
        $data['updated_date'] = $getDate[0]->created_date;
        $data['pc_employees_review_id'] = $pc_employees_review_id;
        $data['username'] = $this->getName();
        }
       
       return response()->json($data);  
    }

    public function getDataReview(Request $request)
    {
        $form_type = $request->form_type;
        if($form_type == 'review'){
            $pc_employees_review_id = $request->pc_employees_review_id;
            $getData = DB::select("select * from tbl_pc_employees_review where pc_employees_review_id =".$pc_employees_review_id."  ");

        }

        return $getData;   
    }

    public function updateTableRecordsReview(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }

        $form_type = $request->form_type;
        if($form_type == 'review'){
            $pc_employees_review_id = $request->pc_employees_review_id;
            $check = DB::select("select pc_employees_review_id  from tbl_pc_employees_review where pc_employees_review_id = ".$pc_employees_review_id." ");
            if($check){

                $data['parameters'] = $request->parameters;
                $data['rating'] = $request->rating;
                $data['review'] = $request->review;
               

                DB::table('tbl_pc_employees_review')->where('pc_employees_review_id',$pc_employees_review_id)->update($data); 
                $getDate = DB::select("select created_date from tbl_pc_employees_review where pc_employees_review_id = ".$pc_employees_review_id."");
                $data['updated_date'] = $getDate[0]->created_date;
                $data['username'] = $this->getName();
   
                
                return response()->json(['success'=>'Record updated successfully.','pc_employees_review_id'=>$pc_employees_review_id,'data'=>$data]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }

    }


    public function getFocusedSublocation(Request $request)
    {
        // echo "<pre>"; print_r(implode(',',$request->locationIds)); die;
        
        if(isset($request->locationIds) && !empty($request->locationIds)){
            $data['fetSublocation'] = DB::select(" select * from dd_sub_location where location_id in(".implode(',',$request->locationIds).") ");
            // $data['fetComplex'] = DB::select(" select project_id,project_complex_name from op_project where location_id in(".implode(',',$request->locationIds).") ");
           
        }else{
            $data['fetSublocation'] = array();
            // $data['fetComplex'] = array();
        }
        
        return response()->json($data);
    }

    
    public function getFocusedComplex(Request $request)
    {
        
        
        if(isset($request->sublocationIds) && !empty($request->sublocationIds)){
           // $fetComplex = DB::select(" select project_id,project_complex_name from op_project where sub_location_id in(".implode(',',$request->sublocationIds).") ");
           $fetComplex = DB::select("select project_id, '' as society_id , '' as building_name,project_complex_name
           from op_project where sub_location_id in (".implode(',',$request->sublocationIds).")  and  status=1   
           union 
           select op.project_id,os.society_id,os.building_name,op.project_complex_name
           from op_project as op
           join  op_society as os on os.project_id = op.project_id
           where op.status=1   and  os.status=1  and os.sub_location_id in(".implode(',',$request->sublocationIds).")
           order by project_complex_name  asc");
            
        }else{
            $fetComplex = array();
        }
        return response()->json($fetComplex);
    }


    public function deleteCompletly(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); //die();
        $form_type = $request->form_type;

        if($form_type=='reviewPC'){
            $property_consultant_review_id = $request->property_consultant_review_id;
            $check = DB::select("select property_consultant_review_id from tbl_property_consultant_review where property_consultant_review_id =".$property_consultant_review_id."");
            if($check){
                $property_consultant_review_id = $check[0]->property_consultant_review_id;
                DB::table('tbl_property_consultant_review')->where('property_consultant_review_id',$property_consultant_review_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','property_consultant_review_id'=>$property_consultant_review_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }

        if($form_type=='socialPC'){
            
            $property_consultant_social_media_id = $request->property_consultant_social_media_id;
           
            $check = DB::select("select property_consultant_social_media_id from tbl_property_consultant_social_media where property_consultant_social_media_id =".$property_consultant_social_media_id."");
            // print_r($check); die();
            if($check){
                $property_consultant_social_media_id = $check[0]->property_consultant_social_media_id;
                DB::table('tbl_property_consultant_social_media')->where('property_consultant_social_media_id',$property_consultant_social_media_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','property_consultant_social_media_id'=>$property_consultant_social_media_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }

        if($form_type=='reviewBPC'){
            $pc_employees_review_id = $request->pc_employees_review_id;
            $check = DB::select("select pc_employees_review_id from tbl_pc_employees_review where pc_employees_review_id =".$pc_employees_review_id."");
            if($check){
                $pc_employees_review_id = $check[0]->pc_employees_review_id;
                DB::table('tbl_pc_employees_review')->where('pc_employees_review_id',$pc_employees_review_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','pc_employees_review_id'=>$pc_employees_review_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }
    }

    public function softDelete($table,$column,$value)
    {   $data = array('del_status'=>0,'user_id'=>$this->getUserId());
        DB::table($table)->where($column,$value)->update($data); 

    }

    public function deletePartially(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die();
        $form_type = $request->form_type;
        
        if($form_type == 'PC'){
            $property_consultant_id = $request->property_consultant_id;
            $check = DB::select("select property_consultant_id from tbl_property_consultant where property_consultant_id =".$property_consultant_id."");
            if($check){
                $this->softDelete('tbl_property_consultant','property_consultant_id',$property_consultant_id);
                return response()->json(['success'=>'Record deleted successfully.']);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }

        if($form_type == 'PC_branch'){
            $pc_branch_id = $request->pc_branch_id;
            $check = DB::select("select pc_branch_id from tbl_pc_branch where pc_branch_id =".$pc_branch_id."");
            if($check){
                $this->softDelete('tbl_pc_branch','pc_branch_id',$pc_branch_id);
                return response()->json(['success'=>'Record deleted successfully.']);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }

        if($form_type == 'PC_Emp'){
            $pc_employees_id = $request->pc_employees_id;
            $check = DB::select("select pc_employees_id from tbl_pc_employees where pc_employees_id =".$pc_employees_id."");
            if($check){
                $this->softDelete('tbl_pc_employees','pc_employees_id',$pc_employees_id);
                return response()->json(['success'=>'Record deleted successfully.']);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }
    }

    public function seach_by_mobile(Request $request)
    {
       
       
       if( !empty($request->mobile_number) ){
        // echo 1; die();
        $mobile_number = $request->mobile_number;

        $get_pcid = DB::select("select property_consultant_id from tbl_pc_employees where primary_mobile_number='".$mobile_number."' or seccondary_mobile_number = '".$mobile_number."'");
        // echo "<pre>"; print_r($get_pcid); die
        if(!empty($get_pcid)){
            $property_consultant_id = $get_pcid[0]->property_consultant_id;
            $data['getPcData'] = DB::select("  select pc.property_consultant_id, 
                pc.company_name,          
                (select count(pc_branch_id) from tbl_pc_branch where property_consultant_id = pc.property_consultant_id and branch_name != '' ) as branch_cnt,
                (select count(pc_employees_id) from tbl_pc_employees where property_consultant_id = pc.property_consultant_id and employee_name!= '' ) as employee_cnt,        
                (select sub_location_name from dd_sub_location where sub_location_id = pc.sub_location) as sub_location1 , 
                (select location_name from dd_location where location_id = pc.location ) as location1 , 
                (select city_name from dd_city where city_id= pc.city) as city1,
                pc.office_address,
                pc.rera_number,
                pc.no_of_years,
                pc.created_date,
                (select name from users where id = pc.user_id) as usernanme
                from tbl_property_consultant as pc
                where pc.company_name !='' and pc.del_status='1' and property_consultant_id ='".$property_consultant_id."'  group by pc.property_consultant_id, pc.company_name, 
                sub_location1,location1,city1,office_address,no_of_years,rera_number,created_date,usernanme order by
                pc.company_name asc");
                $data['mobile_number'] = $mobile_number;
                

        }else{
            $data['getPcData'] = array();
        }
        return view('property_consultant/pc_list_view_new',$data);
        
       }
       
    }

}