<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use App\Traits\DatabaseTrait;

class ProjectMasterController extends Controller
{
    use DatabaseTrait;
    public function index(Request $request)
    {
        // echo $request->ip();
        // server ip

        // echo \Request::ip();
        // // server ip

        // echo \request()->ip();
        // server ip

        // echo $this->getIp(); //see the method below
        // clent ip

        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = $this->adminRoles();
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }

       
        if( isset($request->gr_id) && !empty($request->gr_id) || isset($request->project_id) && !empty($request->project_id) || isset($request->c_id) && !empty($request->c_id) ){
            $group_id = $request->gr_id;
            $project_id = $request->project_id; 
            if(!empty($group_id)){
                $getProjectData = DB::select("SELECT * FROM op_project WHERE group_id=".$group_id." ");
            }elseif(!empty($project_id)){
                $getProjectData = DB::select("SELECT * FROM op_project WHERE project_id=".$project_id." ");
            }else{
                $getProjectData = array();
            }
            
            if(!empty($getProjectData)){
                $project_id = $getProjectData[0]->project_id;
                $group_id = $getProjectData[0]->group_id;
                $project_complex_name = $getProjectData[0]->project_complex_name;
                $category_id = $getProjectData[0]->category_id;
                $unit_status_of_complex = $getProjectData[0]->unit_status_of_complex;
                $sub_location_id = $getProjectData[0]->sub_location_id;
                $location_id = $getProjectData[0]->location_id;
                $company_id = $getProjectData[0]->company_id;
                $city_id = $getProjectData[0]->city_id;
                $state_id = $getProjectData[0]->state_id;
                $complex_rating = $getProjectData[0]->complex_rating;
                $complex_description = $getProjectData[0]->complex_description;
                $complex_land_parcel = $getProjectData[0]->complex_land_parcel;
                $company_name = $getProjectData[0]->company_name;
                $complex_open_space = $getProjectData[0]->complex_open_space;
                $sales_office_address =$getProjectData[0]->sales_office_address;
                $e_brochure = $getProjectData[0]->e_brochure;
                $video_links = $getProjectData[0]->video_links;
                $gst_no = $getProjectData[0]->gst_no;
                $gst_certificate = $getProjectData[0]->gst_certificate;
                $comments =$getProjectData[0]->comments;

                $getSociety = DB::select("select society_id from op_society where project_id=".$project_id." and status='1' and building_name is not null ");
                if(!empty($getSociety)){
                    $sid = array();
                    foreach ($getSociety as $key => $value) {
                        array_push($sid,$value->society_id);
                    }
                }
                if(!empty($sid)){
                
                $checkUnitData = DB::select("select unit_id  from op_unit where society_id in(".implode(',',$sid).") ");
                // echo"<pre>"; print_r($checkUnitData); die();

                if(!empty($checkUnitData)){
                    // unit_view
                    $unitData = DB::select("select s.building_status,s.building_name,w.wing_name,u.unit_id,u.unit_code,
                    (select configuration_name from dd_configuration where  configuration_id=u.configuration_id ) as configuration_name ,
                    (select configuration_size from dd_configuration_size  where  configuration_size_id=u.configuration_size ) as configuration_size  ,
                    u.carpet_area,u.build_up_area, u.floor_no,
                    u.balcony,(select direction from dd_direction where direction_id=u.direction_id ) as direction,
                    u.unit_view,u.show_unit,u.unit_type,u.status,u.rera_carpet_area,u.mofa_carpet_area
                                    from op_unit as u                
                                    join op_society as s on s.society_id=u.society_id
                                    join op_wing as w on w.wing_id=u.wing_id
                                    
                                    where u.society_id in (".implode(',',$sid).")
                                    group by s.building_status, u.configuration_id,u.carpet_area,u.rera_carpet_area,u.mofa_carpet_area,s.building_name,w.wing_name,u.unit_id,u.unit_code,
                                    u.build_up_area,u.balcony,u.unit_view,u.show_unit,u.unit_type,u.status,u.floor_no,configuration_name,configuration_size,direction");
                }else{
                    $unitData = array();
                }
                }else{
                    $unitData = array();
                }
                
                // print_r($unitData); die();
                
                $tid1 = $tid2 = $tid3 = $tid4 = $tid5 = array();
                $kid1 = $kid2 = $kid3 = $kid4 = $kid5 = array();
                foreach ($unitData as $key => $value) {
                    
                    if(in_array($value->configuration_name,$tid1) == false){
                        array_push($tid1,$value->configuration_name);
                        if(!empty($value->configuration_name)){
                            array_push($kid1,$key);
                        }
                    }
    
                    if( in_array($value->carpet_area,$tid2) == false ){
                        array_push($tid2,$value->carpet_area);
                        if(!empty($value->carpet_area)){
                        
                            array_push($kid2,$key);
                        }  
                    }
                    
                    if( in_array($value->build_up_area,$tid3) == false ){
                        array_push($tid3,$value->build_up_area);
                        if(!empty($value->build_up_area)){
                            array_push($kid3,$key);
                        }   
                    }
    
                    if( in_array($value->rera_carpet_area,$tid4) == false ){
                        array_push($tid4,$value->rera_carpet_area);
                        if(!empty($value->rera_carpet_area)){
                            array_push($kid4,$key);
                        }   
                    }
                    
                    if( in_array($value->mofa_carpet_area,$tid5) == false ){
                        array_push($tid5,$value->mofa_carpet_area);
                        if(!empty($value->mofa_carpet_area)){
                            array_push($kid5,$key);
                        }   
                    }
    
                }    
               
                $finalA = array_unique(array_merge($kid1,$kid2,$kid3,$kid4,$kid5));            
    
                $filterData = array();
                foreach ($unitData as $key1 => $value1) {
                    if(in_array($key1,$finalA)){
                            
                            array_push($filterData,$value1);
                    }
                }
    
                // echo "<pre>"; print_r($filterData); die();
    

                $getProjectAmenitiesData = DB::select("SELECT opa.*,pa.amenities FROM op_project_amenities as opa 
                left join dd_project_amenities as pa on pa.project_amenities_id=opa.amenities_id
                WHERE opa.project_id=".$project_id." "); 

                if(!empty($getProjectAmenitiesData)){
                    $getProjectAmenitiesData = $getProjectAmenitiesData;
                }else{
                    $getProjectAmenitiesData = array();
                }

                $getProjectPhotoData = DB::select("SELECT * FROM op_project_photo where project_id=".$project_id." ");
                if(!empty($getProjectPhotoData)){
                    $getProjectPhotoData = $getProjectPhotoData;
                }else{
                    $getProjectPhotoData = array();
                }

                // $getProjecthirarchyData = DB::select("SELECT h.*,d.designation_name FROM op_project_hirarchy as h  join dd_designation as d on d.designation_id=h.designation_id where h.project_id=".$project_id." ");
                // $getProjecthirarchyData = DB::select("SELECT h.*,(select designation_name from dd_designation where h.designation_id= designation_id) as designation_name,(select name from op_project_hirarchy where reports_to = h.reports_to ) as reporting_to FROM op_project_hirarchy as h 
                // where h.project_id=".$project_id." ");
                $getProjecthirarchyData = DB::select("SELECT  h.*,di.initials_name,(select designation_name from  dd_designation where designation_id= h.designation_id ) as designation_name,och.name as reporting_to   FROM op_project_hirarchy as h
                left join op_project_hirarchy as och on h.reports_to = och.project_hirarchy_id
                left join dd_initials as di on di.initials_id = h.name_initial
                      where h.project_id= ".$project_id."  order by h.project_hirarchy_id desc ");
                if(!empty($getProjecthirarchyData)){
                    $getProjecthirarchyData = $getProjecthirarchyData;
                }else{
                    $getProjecthirarchyData = array();
                }

                // echo '<pre>'; print_r($getProjecthirarchyData); die();

                $getProjectProsConsData = DB::select("SELECT * FROM project_pro_cons where project_id=".$project_id." ");
                if(!empty($getProjectProsConsData)){
                    $getProjectProsConsData =  $getProjectProsConsData;
                }else{
                    $getProjectProsConsData = array();
                }        

                $company_hirarchy =  DB::select("select * from op_project_hirarchy where project_id=".$project_id."  ");

                $getSociety = DB::select("select society_id from op_society where  project_id = ".$project_id." ");
                if(!empty($getSociety)){
                    $sid = array();
                    foreach ($getSociety as $key => $value) {
                        array_push( $sid,$value->society_id);
                    }
                    // echo "<pre>"; print_r( $sid  );
                    //echo "<pre>"; print_r( explode(',',$sid)  ); 
                    // die();
                    $getCntUnit = DB::select("select sum(total_units) as unitCount from op_wing where society_id in (".implode(',',$sid).") and status='1' ");
                    $unitCount = $getCntUnit[0]->unitCount;


                }else{
                    $unitCount = '';
                }            
            }else{
                // echo 1111; exit();
                if(!empty($request->c_id)){
                    $group_id = $request->c_id;
                    $company_id = $request->c_id;
                }else{
                    $group_id = '';
                    $company_id = $request->gr_id;
                }

               $project_id = '';
               
                $project_complex_name = $category_id = $unit_status_of_complex =  $sub_location_id = $location_id = '';
                    $city_id = $state_id = $complex_rating = $complex_description = $complex_land_parcel = '';
                    $complex_open_space = $sales_office_address = $e_brochure = $video_links = $gst_no = $gst_certificate = '';
                    $comments = $company_name = $unitCount ='';
                    $getProjectAmenitiesData = array();
                    $getProjectPhotoData  = array();
                    $getProjecthirarchyData = array();      
                    $company_hirarchy =array();
                    $getProjectProsConsData =  $filterData = array();
                
            }
        
        }else{
            // echo 1111; exit();
           $project_id = $group_id = '';
           $company_id = $request->gr_id;
            $project_complex_name = $category_id = $unit_status_of_complex =  $sub_location_id = $location_id = '';
                 $city_id = $state_id = $complex_rating = $complex_description = $complex_land_parcel = '';
                $complex_open_space = $sales_office_address = $e_brochure = $video_links = $gst_no = $gst_certificate = '';
                $comments = $company_name = $unitCount ='';
                $getProjectAmenitiesData = array();
                $getProjectPhotoData  = array();
                $getProjecthirarchyData = array();      
                $company_hirarchy =array();
                $getProjectProsConsData =  $filterData = array();            
            
        }   
        
        $sub_location =  $this->getAllData('d','sub_location');
        $location =  $this->getAllData('d','location');
        $city = $this->getAllData('d','city');
        $state = $this->getAllData('d','state'); 
        $category = $this->getAllData('d','category');
        $category_type = $this->getAllData('d','category_type');
        $construction_technology = $this->getAllData('d','construction_technology');
        $unit = $this->getAllData('d','unit');
        $select = array('company_id','group_name');
        $where = "group_name !='' ";
        $group_name = $this->getAllData('o','company',$select,$where);
        $project_amenities = $this->getAllData('d','project_amenities');
        $rating = $this->getAllData('d','rating');
        $unit_status = $this->getAllData('d','unit_status');
        $initials = $this->getAllData('d','initials');
        $country_code = $this->getAllData('d','country');
        $designation = $this->getAllData('d','designation');
        $allamenities = DB::select("select * from dd_project_amenities order by amenities asc"); //$this->getAllData('d','project_amenities');
     

        $dataArray = array('project_id'=>$project_id,'group_id'=>$group_id,'project_complex_name'=>$project_complex_name,'category_id'=>$category_id,
                'unit_status_of_complex'=>$unit_status_of_complex,'sub_location_id'=>$sub_location_id,
                'location_id'=>$location_id,'company_id'=>$company_id,'city_id'=>$city_id,
                'state_id'=>$state_id,'complex_rating'=>$complex_rating,'complex_description'=>$complex_description,
                'complex_land_parcel'=>$complex_land_parcel,'complex_open_space'=>$complex_open_space,
                'sales_office_address'=>$sales_office_address,'e_brochure'=>$e_brochure,
                'video_links'=>$video_links,'gst_no'=>$gst_no,'gst_certificate'=>$gst_certificate,
                'comments'=>$comments,'company_name'=>$company_name,
                'location'=>$location,'sub_location'=>$sub_location,'city'=>$city,'state'=>$state,'category'=>$category,'category_type'=>$category_type,'construction_technology'=>$construction_technology,'group_name'=>$group_name,'unit'=>$unit,'project_amenities'=>$project_amenities,'rating'=>$rating,'unit_status'=>$unit_status,'initials'=>$initials,'country_code'=>$country_code,'designation'=>$designation,'company_hirarchy'=>$company_hirarchy,'company_hirarchy1'=>$company_hirarchy,
                'getProjectAmenitiesData'=>$getProjectAmenitiesData,'getProjectPhotoData'=>$getProjectPhotoData,'getProjectPhotoData1'=>$getProjectPhotoData,
                'getProjecthirarchyData'=>$getProjecthirarchyData,'getProjectProsConsData'=>$getProjectProsConsData,'allamenities'=>$allamenities,'getProjectAmenitiesData1'=>$getProjectAmenitiesData,'filterData'=>$filterData,
            'unitCount'=>$unitCount     );
      

        return view('project_master/project_master',$dataArray);
    }

    public function findCityWithStateID(Request $request)
    {    
      $city_id = $request->city_id;
      $getCity = DB::select("select * from dd_city where city_id =".$city_id."  ");
      $getState = DB::select("select * from dd_state where dd_state_id =".$getCity[0]->state_id."  ");
      return response()->json($getState);
    }

    public function findlocationWithcityID(Request $request)
    {       
        $location_id = $request->location_id;
        $getLocation = DB::select("select * from dd_location where location_id =".$location_id."  ");
        $getCity = DB::select("select * from dd_city where city_id =".$getLocation[0]->city_id."  ");       
        return response()->json($getCity);
    }

    public function findsublocationWithlocationID(Request $request)    {
        
        $SublocationID = $request->SublocationID;
        $getSubLocation = DB::select("select * from dd_sub_location where sub_location_id =".$SublocationID."  ");
        $getLocation = DB::select("select * from dd_location where location_id =".$getSubLocation[0]->location_id."  ");
        return response()->json($getLocation);
    }
    

    public function add_project_master(Request $request)
    {
       
       $form_type = $request->form_type;
       $company_id = $request->company_id;
       $project_id = $request->project_id;
        if($form_type == 'pros_cons'){   
            if(isset($project_id) && !empty($project_id)){
               
                $data = array();
                $data['pros'] = $request->pros_of_complex;
                $data['cons'] = $request->cons_of_complex;
                $data['project_id'] = $project_id; 
                $data['user_id'] = $this->getUserId();
                DB::table('project_pro_cons')->insert($data); 
                $data['project_pro_cons_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from project_pro_cons where project_pro_cons_id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date;
                return response()->json($data);              

            }else{                
                $data = array();
                $data['pros'] = $request->pros_of_complex;
                $data['cons'] = $request->cons_of_complex;
                $data['user_id'] = $this->getUserId();
                DB::table('op_project')->insert(['group_id'=>$company_id,'company_id'=>$company_id,'user_id'=>$this->getUserId()]);            
                $project_id = DB::getPdo()->lastInsertId();
                $data['project_id'] = $project_id;                        
                DB::table('project_pro_cons')->insert($data); 
                $data['project_pro_cons_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from project_pro_cons where project_pro_cons_id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date;                
                return response()->json($data);   
            }

        }

        if($form_type == 'hierarchy'){         
            $data['user_type'] = $request->user_type;
            $data['company_name'] = $request->company_name1;
            $data['name_initial'] = $request->name_initial;
            $data['name'] = $request->name_h;
            $data['country_code_id'] = $request->country_code;
            $data['mobile_number'] = $request->mobile_number_h;
            $data['whatsapp_number'] = $request->wap_number;
            $data['email_id'] = $request->primary_email_h;
            $data['designation_id'] = $request->designation_h;
            $data['reports_to'] = $request->reporting_to_h;
            $data['moved_to'] = $request->moved_to_h;
            $data['project_id'] = $request->project_id;
            $data['user_id'] = $this->getUserId();
                if( $request->hasfile('photo_h') )
                { 
                    $rand1 = mt_rand(99,9999999).".".$request->photo_h->extension();                    
                    $request->photo_h->move(public_path('photo_h'), $rand1);
                    $data['photo'] = $rand1;
                }else{
                    $data['photo'] = '';
                }

            if(isset($project_id) && !empty($project_id)){
                DB::table('op_project_hirarchy')->insert($data); 
                $data['project_hirarchy_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from op_project_hirarchy where project_hirarchy_id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date; 
                if(!empty($request->designation_h)){
                    $designationID = DB::select("select designation_name from dd_designation where designation_id=".$request->designation_h." ");
                    $data['designation_name'] = $designationID[0]->designation_name;
                }else{
                    $data['designation_name'] = "";
                }
                if(isset($request->reporting_to_h) && !empty($request->reporting_to_h)){
                $reportsTo = DB::select("select name from op_project_hirarchy where project_hirarchy_id=".$request->reporting_to_h." ");
                $data['reports_name'] = $reportsTo[0]->name;
                }else{
                    $data['reports_name'] = '';
                }
                if(!empty($request->name_initial)){
                    $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                    $data['initials_name'] = $Initial[0]->initials_name;
                }else{
                    $data['initials_name'] = '';
                }
                return response()->json($data);  
            }else{                 
                DB::table('op_project')->insert(['group_id'=>$company_id,'company_id'=>$company_id,'user_id'=>$this->getUserId()]);            
                $project_id = DB::getPdo()->lastInsertId();
                $data['project_id'] = $project_id;        
                DB::table('op_project_hirarchy')->insert($data); 
                $data['project_hirarchy_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from op_project_hirarchy where project_hirarchy_id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date; 
                if(!empty($request->designation_h)){
                    $designationID = DB::select("select designation_name from dd_designation where designation_id=".$request->designation_h." ");
                    $data['designation_name'] = $designationID[0]->designation_name;
                }else{
                    $data['designation_name'] = "";
                }
                if(isset($request->reporting_to_h) && !empty($request->reporting_to_h)){
                    $reportsTo = DB::select("select name from op_project_hirarchy where project_hirarchy_id=".$request->reporting_to_h." ");
                    $data['reports_name'] = $reportsTo[0]->name;
                }else{
                    $data['reports_name'] = '';
                }

                if(!empty($request->name_initial)){
                    $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                    $data['initials_name'] = $Initial[0]->initials_name;
                }else{
                    $data['initials_name'] = '';
                }

                return response()->json($data);   
            }
        }

        $validator = Validator::make($request->all(), [
            'group_name' => 'required',
            'complex_name' => 'required',
            'sub_location' => 'required'            
         ]);

         $complex_name = $request->complex_name;
            
            if($request->company_id2 == ""){
                $company_id = $request->group_name;
            }else{
                $company_id = $request->company_id2;
            }
            
            $checkComplex = DB::select("select project_id from op_project where group_id =".$company_id." and  project_complex_name ='".trim($complex_name)."' and status='1'   ");
            if(!empty($checkComplex)){                
                $ary = array("Complex Name is already exist");
                return response()->json(['error'=>$ary]);
            }

        
        if ($validator->passes()) {
            

            $data = array();   
            $data['group_id'] = $request->group_name;
            $data['project_complex_name'] = trim($request->complex_name);
            $data['company_name'] = $request->company_name_final;
            $data['location_id'] = $request->location;
            $data['sub_location_id'] = $request->sub_location;
            $data['city_id'] = $request->city;
            $data['state_id'] = $request->state;
            $data['sales_office_address'] = $request->sales_office_address;            
            $data['unit_status_of_complex'] = $request->unit_status_of_complex;
            $data['complex_land_parcel'] = $request->complex_land_parcel;
            $data['complex_open_space'] = $request->complex_open_space;
            $data['complex_rating'] = $request->project_rating;
            $data['complex_description'] = $request->complex_description;
            $data['category_id'] = json_encode($request->category);
            $data['video_links'] = $request->video_link;   
            $data['comments'] = $request->comments;
            $data['user_id'] = $this->getUserId();
            $data['company_id'] = $request->group_name;
            
            $project_id= $request->project_id;
            if( $request->hasfile('gst_certificate') )
            { 
                $array_gst_certificate = array();
                foreach($request->file('gst_certificate') as $file)
                {
                    $rand1 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $rand1);
                    array_push($array_gst_certificate,$rand1);                    
                }

                if(isset($project_id) && !empty($project_id)){
                    $getGstCert = DB::select("select gst_certificate from op_project where project_id= ".$project_id." ");
                    if(  isset($getGstCert) &&  !empty($getGstCert) ){
                        
                        $existingGstCert = json_decode($getGstCert[0]->gst_certificate);                       
                        if(!empty($existingGstCert)){
                            $finalArrayGstCert = array_merge($existingGstCert,$array_gst_certificate);
                        }else{
                            $finalArrayGstCert = $array_gst_certificate;
                        }
                        
                    }else{
                        $finalArrayGstCert = $array_gst_certificate;
                    }   
                }else{
                    $finalArrayGstCert = $array_gst_certificate;
                }
                $data['gst_certificate'] = json_encode($finalArrayGstCert);     
            }

            if( $request->hasfile('e_brochure') )
            { 
                $array_e_brochure = array();
                foreach($request->file('e_brochure') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $rand2);
                    array_push($array_e_brochure,$rand2);                    
                }
                $data['e_brochure'] = json_encode($array_e_brochure);     
            } 
            if(isset($request->project_id2) && !empty($request->project_id2)){                
                DB::table('op_project')->where('project_id',$request->project_id2)->update($data); 
                $project_id = $request->project_id2;
            }else{
                DB::table('op_project')->insert($data);
                $project_id = DB::getPdo()->lastInsertId();
            }
            

            
            if( $request->hasfile('upload_video') )
            { 
                foreach($request->file('upload_video') as $file)
                {                    
                    $randV = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $randV);
                    $videoData = array('project_id'=>$project_id,'type'=>'video','url'=>$randV,'meta_key'=>'upload_video');
                    DB::table('op_project_photo')->insert($videoData);            
                }               
            }

            if($request->hasfile('upload_photos'))
            {
                $array = array();
                foreach($request->file('upload_photos') as $file)
                {
                    $randP = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $randP);
                    $photoData = array('project_id'=>$project_id,'type'=>'photo','url'=>$randP,'meta_key'=>'upload_photos');
                    DB::table('op_project_photo')->insert($photoData);                     
                }               
            }
                if(isset($request->is_available) && !empty($request->is_available))
                {

                    $is_available = $request->is_available;
                    $description_ame = $request->description_ame;
                    $cnt = count($is_available);
                    // echo "<pre>"; print_r($is_available); 
                        for ($i=0; $i<$cnt ; $i++) { 
                            $is_available1 = $is_available[$i];
                                $description_ame1 = $description_ame[$i];
                            $array = array();

                            // echo "<pre>"; print_r( $is_available1 ); 
                
                            if(!empty($is_available1)){
                                // $is_available1 = $is_available[$i]; 
                                $is_available2 = $is_available[$i] == 0 ? 'N': 'Y';
                                    $a = array('description'=>$description_ame1) ;
                                    $array=   array_merge($array,$a);                       
                                // }
                                    // echo "<pre>"; print_r($array); 
                                DB::table('op_project_amenities')->where('project_amenities_id',$is_available1)->update($array); 
                            }   
                        }         
            }

            
            return response()->json(['success'=>'done','lastID'=>$project_id]); 
         }
         return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function update_project_master(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); exit();

            $validator = Validator::make($request->all(), [
                'group_name' => 'required',
                'complex_name' => 'required',                
                'sub_location' => 'required'      
                
            ]
        );

        $complex_name = $request->complex_name;
        $project_id = $request->project_id;
        $getCheck = DB::select("select count(project_id) as cnt from op_project where project_complex_name ='".$complex_name."' ");        
        if(!empty($getCheck)){
            if( $getCheck[0]->cnt == 1 ){               
                $getCheck1 = DB::select("select count(project_id) as cnt from op_project where project_complex_name ='".$complex_name."' and project_id='".$project_id."' ");
                if(!empty($getCheck1)){
                    if( $getCheck1[0]->cnt == 1 ){ 
                    }else{
                        $ary = array("Complex Name is already exist");
                        return response()->json(['error'=>$ary]);
                    }
                }
            }
        }
        
            
        if ($validator->passes()) {

            $data = array();            
            $data['group_id'] = $request->group_name;
            $data['project_complex_name'] = $request->complex_name;
            $data['company_name'] = $request->company_name_final;
            $data['location_id'] = $request->location;
            $data['sub_location_id'] = $request->sub_location;
            $data['city_id'] = $request->city;
            $data['state_id'] = $request->state;
            $data['sales_office_address'] = $request->sales_office_address;
            $data['unit_status_of_complex'] = $request->unit_status_of_complex;
            $data['complex_land_parcel'] = $request->complex_land_parcel;
            $data['complex_open_space'] = $request->complex_open_space;
            if($request->project_rating != ""){
                $data['complex_rating'] = $request->project_rating;
            }else{
                $data['complex_rating'] = NULL;
            }           
            $data['complex_description'] = $request->complex_description;
            $data['category_id'] = json_encode($request->category);
            $data['video_links'] = $request->video_link;   
            $data['comments'] = $request->comments;
            $data['company_id'] = $request->group_name;
            $project_id = $request->project_id;
            $data['user_id'] = $this->getUserId();

            $is_available = $request->is_available;
            $description_ame = $request->description_ame;
            if(isset($is_available) && !empty($is_available)){
                foreach($is_available as $key=>$val){                  
                    $amData = array('description'=>$description_ame[$key]);
                    DB::table('op_project_amenities')->where('project_amenities_id',$val)->update($amData);
                }
            }      
            
            if( $request->hasfile('gst_certificate') )
            { 
                $array_gst_certificate = array();
                foreach($request->file('gst_certificate') as $file)
                {
                    $rand1 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $rand1);
                    array_push($array_gst_certificate,$rand1);                    
                }
                    $getGstCert = DB::select("select gst_certificate from op_project where project_id= ".$project_id." ");
                    if(  isset($getGstCert) &&  !empty($getGstCert) ){                        
                        $existingGstCert = json_decode($getGstCert[0]->gst_certificate);                       
                        if(!empty($existingGstCert)){
                            $finalArrayGstCert = array_merge($existingGstCert,$array_gst_certificate);
                        }else{
                            $finalArrayGstCert = $array_gst_certificate;
                        }
                        
                    }else{
                        $finalArrayGstCert = $array_gst_certificate;
                    }   
                $data['gst_certificate'] = json_encode($finalArrayGstCert);     
            }
            
            if( $request->hasfile('upload_video') )
            { 
                foreach($request->file('upload_video') as $file)
                {                    
                    $randV = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $randV);
                    $videoData = array('project_id'=>$project_id,'type'=>'video','url'=>$randV,'meta_key'=>'upload_video');
                    DB::table('op_project_photo')->insert($videoData);            
                }               
            }

            if($request->hasfile('upload_photos'))
            {
                $array = array();
                foreach($request->file('upload_photos') as $file)
                {
                    $randP = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $randP);
                    $photoData = array('project_id'=>$project_id,'type'=>'photo','url'=>$randP,'meta_key'=>'upload_photos');
                    DB::table('op_project_photo')->insert($photoData);                     
                }               
            }
           
            if( $request->hasfile('e_brochure') )
            { 
                $array_e_brochure = array();
                foreach($request->file('e_brochure') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $rand2);
                    array_push($array_e_brochure,$rand2);                    
                }

                $getEb = DB::select("select e_brochure from op_project where project_id= ".$project_id." ");
                    if(  isset($getEb) &&  !empty($getEb) ){                        
                        $existingEb = json_decode($getEb[0]->e_brochure);                       
                        if(!empty($existingEb)){
                            $finalArrayEb = array_merge($existingEb,$array_e_brochure);
                        }else{
                            $finalArrayEb = $array_e_brochure;
                        }
                        
                    }else{
                        $finalArrayEb = $array_e_brochure;
                    }   
                $data['e_brochure'] = json_encode($finalArrayEb);     
            }         
           
            if(isset($project_id) && !empty($project_id)){
                DB::table('op_project')->where('project_id',$request->project_id)->update($data);
            }else{
                DB::table('op_project')->insert($data);
            }
            return response()->json(['success'=>'done','lastID'=>$project_id]);    
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function getDataProjet(Request $request)
    {       
        $form_type = $request->form_type;
        if($form_type == 'pros_cons'){
            $project_pro_cons_id = $request->project_pro_cons_id;
            $getData = DB::select("select * from project_pro_cons where project_pro_cons_id =".$project_pro_cons_id." ");
            return $getData;
        }
        if($form_type == 'hierarchy'){
            
            $project_hirarchy_id = $request->project_hirarchy_id;           
            $getData = DB::select(" SELECT h.*, (select designation_name from  dd_designation where designation_id= h.designation_id ) as designation_name FROM op_project_hirarchy as h  where h.project_hirarchy_id = ".$project_hirarchy_id." ");
            return $getData;
        }
    }

    public function updateProjectFormRecords(Request $request)
    {
        
       $form_type = $request->form_type;
       if($form_type == 'pros_cons'){
            $project_pro_cons_id = $request->project_pro_cons_id;
            $checkData = DB::select("select * from project_pro_cons where project_pro_cons_id =".$project_pro_cons_id." ");
            if($checkData){
                $data['pros'] = $request->pros_of_complex;
                $data['cons'] = $request->cons_of_complex;
                $data['user_id'] = $this->getUserId();
                DB::table('project_pro_cons')->where('project_pro_cons_id',$project_pro_cons_id)->update($data);
                $getDate = DB::select("select updated_date from project_pro_cons where project_pro_cons_id = ".$project_pro_cons_id."");
                $data['updated_date'] = $getDate[0]->updated_date; 
                return response()->json(['success'=>'Record updated successfully.','project_pro_cons_id'=>$project_pro_cons_id,'data'=>$data]);   

            }else{
                return response()->json(['error'=>'Record does not exist']);
            }                 
       }
       if($form_type == 'hierarchy'){        
        $project_hirarchy_id = $request->project_hirarchy_id;
        $checkData = DB::select("select project_hirarchy_id,photo from op_project_hirarchy where project_hirarchy_id =".$project_hirarchy_id." ");        
        if($checkData){

            $data['user_type'] = $request->user_type;
            $data['company_name'] = $request->company_name1;
            $data['name_initial'] = $request->name_initial;
            $data['name'] = $request->name_h;
            $data['country_code_id'] = $request->country_code;
            $data['mobile_number'] = $request->mobile_number_h;
            $data['whatsapp_number'] = $request->wap_number;
            $data['email_id'] = $request->primary_email_h;
            $data['designation_id'] = $request->designation_h;
            $data['reports_to'] = $request->reporting_to_h;
            $data['moved_to'] = $request->moved_to_h;
            $data['user_id'] = $this->getUserId();
                if( $request->hasfile('photo_h') )
                {                     
                    $rand1 = mt_rand(99,9999999).".".$request->photo_h->extension();                    
                    $request->photo_h->move(public_path('photo_h'), $rand1);
                    $data['photo'] = $rand1;
                }else{                  
                    $data['photo'] = $checkData[0]->photo;
                }               
                DB::table('op_project_hirarchy')->where('project_hirarchy_id',$project_hirarchy_id)->update($data); 
                if(!empty($request->name_initial)){
                    $initials_name = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                    $data['initials_name'] = ucwords($initials_name[0]->initials_name);
                }else{
                    $data['initials_name'] = "";
                }

                if(!empty($request->designation_h)){
                    $designationID = DB::select("select designation_name from dd_designation where designation_id=".$request->designation_h." ");
                    $data['designation_name'] = $designationID[0]->designation_name;
                }else{
                    $data['designation_name'] = "";
                }
                if(!empty($request->reporting_to_h)){
                    $reportsTo = DB::select("select name from op_project_hirarchy where project_hirarchy_id=".$request->reporting_to_h." ");
                    $data['reports_name'] = $reportsTo[0]->name;
                }else{
                    $data['reports_name'] = "";
                }

                if(!empty($request->name_initial)){
                    $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                    $data['initials_name'] = $Initial[0]->initials_name;
                }else{
                    $data['initials_name'] = '';
                }

                $getDate = DB::select("select updated_date from op_project_hirarchy where project_hirarchy_id = ".$project_hirarchy_id."");
                $data['updated_date'] = $getDate[0]->updated_date; 
                return response()->json(['success'=>'Record updated successfully.','project_hirarchy_id'=>$project_hirarchy_id,'data'=>$data]);   

        }else{
                return response()->json(['error'=>'Record does not exist']);
        } 
        
       }
    }

    public function deleteProjectFormRecords(Request $request)
    {
        $form_type = $request->form_type;
        if($form_type=='pros_cons'){
            $project_pro_cons_id = $request->project_pro_cons_id;
            $check = DB::select("select * from project_pro_cons where project_pro_cons_id =".$project_pro_cons_id." ");            
            if($check){
                DB::table('project_pro_cons')->where('project_pro_cons_id',$project_pro_cons_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','project_pro_cons_id'=>$project_pro_cons_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }
        if($form_type=='hirarchy'){
            $project_hirarchy_id = $request->project_hirarchy_id;
            $check = DB::select("select * from op_project_hirarchy where project_hirarchy_id =".$project_hirarchy_id." ");
            
            if($check){
                DB::table('op_project_hirarchy')->where('project_hirarchy_id',$project_hirarchy_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','project_hirarchy_id'=>$project_hirarchy_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }
        if($form_type=="project"){           
            $project_id = $request->project_id;
            $checkInProject = DB::select("select project_id from op_project where project_id = ".$project_id."");                
            
            if(!empty($checkInProject)){
                // delete all sun project tables
                foreach ($checkInProject as $key => $value) {
                    $project_id = $value->project_id; 
                    $this->softDelete('op_project','project_id',$project_id);
                    // $this->softDelete('op_project_amenities','project_id',$project_id);
                    // $this->softDelete('op_project_hirarchy','project_id',$project_id);
                    // $this->softDelete('op_project_photo','project_id',$project_id);
                    // $this->softDelete('project_pro_cons','project_id',$project_id);                       
                    
                    $checkInSociety = DB::select("select society_id from op_society where project_id = ".$project_id."");
                    if(!empty($checkInSociety)){
                        foreach ($checkInSociety as $key1 => $value1) {
                            $society_id = $value1->society_id;
                            $this->softDelete('op_society','society_id',$society_id);
                            // $this->softDelete('op_society_account_manager','society_id',$society_id);
                            // $this->softDelete('op_society_amnities','society_id',$society_id);
                            // $this->softDelete('op_society_brokerage_fee','society_id',$society_id);
                            // $this->softDelete('op_society_floor_rise','society_id',$society_id);
                            // $this->softDelete('op_society_keymembers','society_id',$society_id);
                            // $this->softDelete('op_society_photos','society_id',$society_id);
                            // $this->softDelete('op_society_pros_cons','society_id',$society_id);
                            // $this->softDelete('society_construction_stage','society_id',$society_id);
                            // $this->softDelete('society_working_days_time','society_id',$society_id);
                            
                            $checkInWing = DB::select("select wing_id from op_wing where society_id = ".$society_id."");
                            // echo "<pre>"; echo "checkInSociety"; print_r($checkInWing);
                            if(!empty($checkInWing)){
                                 foreach ($checkInWing as $key2 => $value2) {
                                     $wing_id = $value2->wing_id;
                                     $this->softDelete('op_wing','wing_id',$wing_id);
                                     $checkInUnit = DB::select("select unit_id from op_unit where wing_id = ".$wing_id."");
                                    //  echo "<pre>"; echo "checkInUnit "; print_r($checkInUnit);
                                    // if(!empty($checkInUnit)){
                                    //     foreach ($checkInUnit as $key3 => $value3) {
                                    //         $unit_id = $value3->unit_id;
                                            //    $this->softDelete('op_unit','unit_id',$unit_id);

                                    //     }
                                    // }
                                 }
                            }else{
                                echo "<pre>"; echo "delete from op_wing and return";
                            }
                        }

                    }else{
                        echo "<pre>"; echo "delete from op_society and return";
                    }
                  

                }                  
            }
            return 1;

        }


    }


    public function softDelete($table,$column,$value)
    {   $data = array('status'=>0,'user_id'=>$this->getUserId());
        DB::table($table)->where($column,$value)->update($data); 

    }    
      // project list
      public function project_list(Request $request)
      {
          $data = DB::table('op_project')
          ->selectRaw('op_project.*')//,op_wing.wing_name                                                    
           ->where('op_project.project_complex_name','!=','')
           //  ->where('op_project.group_name', 'like', "%".$search."%") 
           ->where('op_project.status','=','1')
           ->orderBy('project_id', 'DESC')        
           ->paginate(15);   
           return view('project_master/project_list',['data'=>$data]);
      }


    public function search_project_list(Request $request)
    {
        $search = $request->search;  
        $data = DB::table('op_project')
        ->selectRaw('op_project.*')//,op_wing.wing_name                                                    
        //  ->where('op_company.group_name','!=','')
          ->where('op_project.project_complex_name', 'like', "%".$search."%") 
         ->where('op_project.status','=','1')
         ->orderBy('project_id', 'DESC')   
         ->paginate(5)->appends(['search'=>$search]);
         return view('project_master/project_list',['data'=>$data]);
    }

    public function updateProjectAmenity(Request $request)
    {      

        $project_amenities_id = $request->project_amenities_id;
        $exp = explode(",",$project_amenities_id);
        $exp = array_unique($exp);
        $imp= implode(',',$exp);   
        $project_id  = $request->project_id;
        $getData = DB::select("select project_amenities_id from op_project_amenities where project_id= ".$project_id."  and amenities_id not in(".$imp.") ");

        foreach( $getData as $val1){           
            DB::table('op_project_amenities')->where('project_amenities_id',$val1->project_amenities_id)->delete();
        }

        $checkData = DB::select("select project_amenities_id,amenities_id from op_project_amenities where project_id= ".$project_id." ");
        $getData = array();
        $getAmeData = array();
        foreach ($checkData as $key => $value) {
            array_push($getData,$value->amenities_id);
            array_push($getAmeData,$value->project_amenities_id);
        }

        $result=array_diff($exp,$getData);
        $lastIds = array();
        foreach ($result as $key1 => $value1) {         

            $data1['project_id'] = $project_id;
            $data1['amenities_id'] = $value1;
            $data['user_id'] = $this->getUserId();
            DB::table('op_project_amenities')->insert($data1);
            $lid= DB::getPdo()->lastInsertId();
            array_push($lastIds,$lid);
        }

        $finalArray = array_merge($getAmeData,$lastIds);
        $implode = implode(',',$finalArray);
        $getAmeId = DB::select("select opa.project_amenities_id,opa.amenities_id,pa.amenities,opa.description from op_project_amenities as opa 
        join dd_project_amenities as pa on pa.project_amenities_id = opa.amenities_id
        where opa.project_id=".$project_id." and opa.project_amenities_id in (".$implode.") ");

        foreach ($getAmeId  as $key2 => $value2) {
            $html = "";
            $html ='<div class="col l4 m4 s12">
            <div class="col l6 m s12 input-field col l3 m4 s12 display_search">'.ucwords($value2->amenities).'       
            </div>
            <div class="col l3 m3 s12" style="display:none">
                <label><input type="checkbox" name="is_available[]" id="is_available" value="'.$value2->project_amenities_id.'" checked ><span>Available</span></label> updateAmenityDesc
            </div>
            <div class="col l6 m6 s12 input-field col l3 m4 s12 display_search" >
                <label  class="active">Description: </label>
                    <input type="text" class="validate" name="description_ame[]"  placeholder="Enter" style="height: 36px;" value="'.$value2->description.'" >
            </div>
            </div>';
            print_r($html);
        }      
        
    }
    

    public function addProjectAmenity(Request $request)
    {        
        $add_new_amenity = $request->add_new_amenity;
        $check = DB::select("select amenities  from  dd_project_amenities  where  amenities = '".trim($add_new_amenity)."' ");
        if(!empty($check)){
            return 0;
        }else{
            $data['amenities'] = trim($add_new_amenity);
            // $data['user_id'] = $this->getUserId();
            DB::table('dd_project_amenities')->insert($data);
            return 1;
        }
        
    }

    public function flow_chart_project(Request $request)
    {        
        $str = $request->a;
        $ar = str_replace('_',',',$str);
        $aa = explode(",",$ar);
        $project_hirarchy_id = $aa[0];
        $user_type = $aa[1];             
        $data= DB::select("select och.reports_to,och.name as reports_to_name, oh.name,d.designation_name from op_project_hirarchy as oh
        left join op_project_hirarchy as och on och.reports_to= oh.project_hirarchy_id
        left join dd_designation as d on d.designation_id = och.designation_id 
        where oh.project_id=".$request->project_id." and och.user_type='".$user_type."' " );
     
        $intAr = array();
        foreach($data as $value1){            
        //    array_push($cid,$value->company_hirarchy_id);       
                // echo "<pre>"; print_r($value);
                // [{'v':'Mike', 'f':'Mike<div style="color:red; font-style:italic">President</div>'},
                // '', '']
                // array({{'v:'.$value1->reports_to_name,'f':'<span style="color:red>'.$value1->name.'</span>',''},'','');
                // $d = array("v"=>$value1->reports_to_name ,"f"=>"$value1->reports_to_name<div style='color:red; font-style:italic'>$value1->designation_name</div>");
                // $tempAr = array($d,$value1->name,'');
                $tempAr = array($value1->reports_to_name,$value1->name,'');
                
                // echo "<pre>"; print_r($tempAr);
                 array_push($intAr,$tempAr);
        }
        // echo "<pre>"; print_r($tempAr);
        echo json_encode($intAr);     
     
    }

    public function createEmptyProject(Request $request)
    {
       
        if(isset($request->company_id) && !empty($request->company_id)){
            $data1['group_id'] =  $request->company_id;
            $data['user_id'] = $this->getUserId();
            DB::table('op_project')->insert($data1); 
            return DB::getPdo()->lastInsertId();
        }else{
            return 0;
        }
    }

    public function delImageP(Request $request)
    {
       
        $project_photo_id = $request->project_photo_id;
        if(!empty( $project_photo_id )){
            DB::table('op_project_photo')->where('project_photo_id',$project_photo_id)->delete(); 
        }
        
    }

    public function delImageB(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die();
        $image = $request->image;
        $type = $request->type;
        $project_id = $request->project_id;
        if($type=='eb'){
            $getCPA = DB::select('select  e_brochure from op_project where project_id ='.$project_id.' ');
            $e_brochure = json_decode($getCPA[0]->e_brochure);
            $final_array= array_diff($e_brochure, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_project')->where('project_id',$project_id)->update(array('e_brochure'=>$enc,'user_id'=>$this->getUserId()));  
        }

        if($type=='gs'){
            $getCPA = DB::select('select  gst_certificate from op_project where project_id ='.$project_id.' ');
            $gst_certificate = json_decode($getCPA[0]->gst_certificate);
            $final_array= array_diff($gst_certificate, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_project')->where('project_id',$project_id)->update(array('gst_certificate'=>$enc,'user_id'=>$this->getUserId()));   
        }
    }



}
