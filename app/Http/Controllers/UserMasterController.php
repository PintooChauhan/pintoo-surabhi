<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;
use DateTime;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserMasterController extends Controller
{
    
    public function index(Request $request)
    {
        return view('user_master/new_user_list');
    }

    public function access_group()
    {
        return view('user_master/access_group');
    }

    public function new_user()
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
        return view('user_master/add_new_user');
    }

    public function store(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die();
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role'=>$request->role
        ]);

        // event(new Registered($user));

        // return redirect(RouteServiceProvider::HOME);
        return redirect('/user-form');
    }

// user View
public function userView()
{
    $user = Auth::user();
    if(empty($user)){
        Auth::logout();
        return redirect('login');
    }

    /************* Role Check Start  *********************/

    $condition_name = 'users';
    $page_action = 's_view';
    $action_status = 1;
    $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

    $data['access_check'] = $access_check;

    /************* Role Check End  *********************/

    if(!empty($access_check)){
        
        $data['getUserData'] = DB::select("SELECT id,name,email,role,status,created_at,updated_at FROM `users` WHERE role<>'developer' AND del_status='1' ");
        $data['mobile_number'] = '';
        return view('user_master1/user_view',$data);

    }else{
        return Redirect()->back()->with('error','You do not have access to this page.');
    }

}

// Add User View
public function addUser(Request $request)
{
    // return $request;
    $user = Auth::user();  
    // echo "<pre>"; print_r($user); die();
    if(empty($user)){
        Auth::logout();
        return redirect('login');
    }
    
    /************* Role Check Start  *********************/
    $condition_name = 'users';
    if(isset($request->buyer_tenant_id) && !empty($request->buyer_tenant_id)){
        $page_action = 's_edit';
    }else{
        $page_action = 's_add';
    }

    $action_status = 1;
    $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

    /************* Role Check End  *********************/

    if(!empty($access_check)){

        $role = $this->getRole();
        $roles = array('super_admin');
        // if(!in_array($role,$roles)){
        //     Auth::logout();
        //     return redirect('login');
        // }

        $user_id = $request->user_id;
        $data['sub_location'] = $this->getAllData('d','sub_location');
        $data['location'] =  $this->getAllData('d','location');
        $data['city'] = $this->getAllData('d','city');
        $data['state'] = $this->getAllData('d','state'); 
        $data['social_site'] = $this->getAllData('d','social_site');
        $data['rating'] = $this->getAllData('d','rating');  
        $data['user_id'] = $user_id;
        $data['country'] = $this->getAllData('d','country');

        $data['branch'] = DB::select("SELECT pc_branch_id, branch_name FROM `tbl_pc_branch` WHERE branch_name IS NOT NULL AND del_status ='1' ORDER BY branch_name ASC");
        $data['designation'] = DB::select("SELECT dd_designation1_id, designation FROM `dd_designation1`  ORDER BY designation ASC");
        $data['role'] = DB::select("SELECT id,role_name,session_name FROM `roles` WHERE role_name<>'Developer' AND status='1' AND del_status='1' ORDER BY role_name ASC");

        if(isset($user_id) && !empty($user_id)){
            // return 'if';
            // $data['getSingleUser'] = DB::select("SELECT id, user_id, dob, phone_number, branch_id, designation_id, basic_salary, photo, resumes, 
            // temporary_address, permanent_address, description, comment, internal_comment, status, (SELECT name,email,role,role_id FROM users WHERE users.id=user_details.user_id) FROM `user_details` WHERE user_details.user_id ='$user_id' AND del_status='1'");

            // $data['getSingleUser'] = DB::select("SELECT ud.id, ud.user_id, ud.dob, ud.phone_number, ud.branch_id, ud.designation_id, ud.pc_employees_id, ud.basic_salary, ud.photo, resumes, 
            // ud.temporary_address, ud.permanent_address, ud.description, ud.comment, ud.internal_comment, ud.status, u.name, u.email, u.role, u.role_id FROM `user_details` AS ud, `users` AS u WHERE ud.user_id='$user_id' AND ud.del_status='1' AND ud.user_id=u.id;");
            
            $data['getSingleUser'] = DB::select("SELECT u.id, ud.user_id, ud.dob, ud.phone_number, ud.branch_id, ud.designation_id, ud.pc_employees_id, ud.basic_salary, ud.photo, ud.resumes, 
            ud.temporary_address, ud.permanent_address, ud.description, ud.comment, ud.internal_comment, ud.status, u.name, u.email, u.role, u.role_id FROM `users` AS u LEFT JOIN `user_details` AS ud ON u.id=ud.user_id WHERE u.id='$user_id' AND u.del_status='1' ");
            
            // return $data['getSingleUser'];
            $data['reporting_manager_data'] = array();
            foreach($data['getSingleUser'] as $key=>$value){
                $branch_id = $value->branch_id;
                
                if($branch_id!=''){
                    $data['reporting_manager_data'] = DB::select("SELECT pc_employees_id,employee_name FROM `tbl_pc_employees` WHERE pc_branch_id ='$branch_id' AND del_status='1'");
                }
            }
            
            
            // $getReportingManagerData = DB::select("SELECT pc_employees_id,employee_name FROM `tbl_pc_employees` WHERE pc_branch_id ='$branch_id' AND del_status='1'");

            // $data['getSocialData'] = DB::select("select pc.property_consultant_social_media_id, pc.property_consultant_id, pc.url,pc.created_date,
            // (select social_site from dd_social_site where social_site_id = pc.social_site) as social_site 
            //  from tbl_property_consultant_social_media as pc where pc.property_consultant_id =".$property_consultant_id." ");
        }else{
            
            $data['getSingleUser'] = array();
            // $data['getReviewData'] = array();
            // $data['getSocialData'] = array();
            $data['reporting_manager_data'] = array();
        }

        // return $data;

        return view('user_master1/add_user_view',$data);

    }else{
        return Redirect()->back()->with('error','You do not have access to this page');
    }
}

// save User

public function saveUser(Request $request)
{
    // return $request;

    // return public_path('');

    $user = Auth::user();  
    // echo "<pre>"; print_r($user); die();
    if(empty($user)){
        Auth::logout();
        return redirect('login');
    }

    $role = $this->getRole();
    $roles = array('developer','super_admin');

    // if(!in_array($role,$roles)){
    //     Auth::logout();
    //     return redirect('login');
    // }

    if(isset($request->user_id) && !empty($request->user_id))
    {
    //  $validator = $request->validate([
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id',
            'phone_number' =>'required',
            'role_id' =>'required',
        ]);
    }else{
    //  $validator = $request->validate([
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            // 'password' => 'required|string|confirmed|min:8',
            // 'password' => 'required|confirmed|min:8',
            // 'confirm_password' => 'required|same:password|min:8',
            'password' => 'min:8|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required|min:8',
            // 'password' => 'required|string|min:8',
            'phone_number' =>'required',
            'role_id' =>'required',
        ]);
    }


    // return 'error';
    if ($validator->passes()) {
        // return 'if';
        $user_id = $request->user_id;
        $userData = array();
        $userData['user_id'] = $request->user_id;
        // $userData['name'] = $request->name;
        // return $request->dob;
        if(isset($request->dob) && !empty($request->dob))
        {
            $userData['dob'] = DateTime::createFromFormat('d/m/Y', $request->dob)->format('Y-m-d');
        }
        
        $userData['phone_number'] = $request->phone_number;
        $userData['branch_id'] = $request->branch_id;
        $userData['designation_id'] = $request->designation_id;
        $userData['pc_employees_id'] = $request->pc_employees_id;
        $userData['basic_salary'] = $request->basic_salary;
        // $userData['photo'] = $request->photo;
        // $userData['resumes'] = $request->resumes;
        $userData['temporary_address'] = $request->temporary_address;
        $userData['permanent_address'] = $request->permanent_address;
        $userData['description'] = $request->description;
        $userData['comment'] = $request->comment;
        $userData['internal_comment'] = $request->internal_comment;

        if( $request->hasfile('photo') )
        { 
            $rand1 = time().mt_rand(99,9999999).".".$request->photo->extension(); 
            $request->photo->move(public_path('users/photos'), $rand1);
            $userData['photo'] = $rand1;
        }

        if($request->hasfile('resumes'))
        {
            $array = array();
            foreach($request->file('resumes') as $file)
            {
                $rand2 = mt_rand(99,9999999).".".$file->extension();
                $file->move(public_path('users/resumes'), $rand2);
               array_push($array,$rand2);
                
            }
            $userData['resumes'] = json_encode($array);
        }

        // $userData['role_id'] = $request->internal_comment;
        // return $userData;
        if( isset($user_id) && !empty($user_id) ){
            // return 'if';
            // update tbl_customer
            $user = User::where('id',$request->user_id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'role'=>$request->role_name,
                'role_id'=>$request->role_id
            ]);

            DB::table('user_details')->where('user_id',$user_id)->update($userData);  
            return response()->json(['success'=>'User Updated Successfully']);    
        }else{
            // return 'else';
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role'=>$request->role_name,
                'role_id'=>$request->role_id
            ]);
            // dd($user);
            $user_id  =DB::getPdo()->lastInsertId();
            $userData['user_id'] = $user_id;
            DB::table('user_details')->insert($userData);
            
            return response()->json(['success'=>'User Added Successfully']);
        }

    }
    else{
        // return 'else';
        return response()->json(['error'=>$validator->errors()]);
    }

    
}

public function delUserImage(Request $request)
{
    // return $request;
    $doc = $request->image;
    $type = $request->type;
    $user_id = $request->user_id;

    if($type == 'img'){

        $result = DB::table('user_details')->where('user_id',$user_id)->get();
        if(count($result)>0){
            if($doc!=''){
                // $imgPath = asset('users/photos/'.$doc);
                $imgPath = 'public/users/photos/'.$doc;
                // $imgPath = asset('users/photos/'.$doc);
                
                if(unlink($imgPath)){
                    $result1 = DB::table('user_details')->where('user_id',$user_id)->update(['photo'=>'']);
                    if($result1){
                        return response()->json(['success'=>'Image deleted Successfully']);
                    }else{
                        return response()->json(['error'=>'Record not exists']);
                    }
                }else{
                    return response()->json(['error'=>'Record not exists']);
                }

            }
        }
    }

    if($type == 'resume'){
        $result = DB::select("SELECT  resumes FROM user_details WHERE user_id='$user_id'");

        if(count($result)>0){

            $resumes = json_decode($result[0]->resumes);
            // $resumes = $getResume[0]->resumes;
            $final_array= array_diff($resumes, array($doc));
            // $enc = json_encode(array_values($final_array));
            $enc = array_values($final_array);

            if($doc!=''){
            // $resumePath = asset('users/resumes/'.$doc);
            $resumePath = 'public/users/resumes/'.$doc;

            if(unlink($resumePath)){
                $result1 = DB::table('user_details')->where('user_id',$user_id)->update(array('resumes'=>$enc));
                if($result1){
                    return response()->json(['success'=>'Resume deleted Successfully']);
                }else{
                    return response()->json(['error'=>'Record not exists']);
                }
            }else{
                return response()->json(['error'=>'Record not exists']);
            }

            }
        }
    }

}

public function statusUser(Request $request)
{
    // return $request;
    // echo "<pre>"; print_r($request->all()); die();
    $form_type = $request->form_type;
    
    if($form_type == 'status_user'){
        $user_id = $request->user_id;
        $check = DB::select("SELECT status from `users` WHERE id ='$user_id'");
        if($check){
            foreach($check as $key=>$value){
                $status = $value->status;
            }
            if($status==0){
                $status='1';
            }
            else{
                $status='0';
            }

            $user = User::where('id',$request->user_id)
            ->where('del_status',1)
            ->update(['status' =>$status]);

            DB::table('user_details')
            ->where('user_id',$user_id)
            ->where('del_status',1)
            ->update(['status'=>$status]);

            return response()->json(['success'=>'Status Change successfully.']);   
        }else{
            return response()->json(['error'=>'Record does not exist']);
        }
    }
}


public function deleteUser(Request $request)
{
    // return $request;
    // echo "<pre>"; print_r($request->all()); die();
    $form_type = $request->form_type;
    
    if($form_type == 'delete_user'){
        $user_id = $request->user_id;
        $check = DB::select("SELECT id from `users` WHERE id ='$user_id'");
        if($check){
            $user = User::where('id',$request->user_id)->update(['del_status' =>0]);
            DB::table('user_details')->where('user_id',$user_id)->update(['del_status'=>'0']);
            return response()->json(['success'=>'Record deleted successfully.']);   
        }else{
            return response()->json(['error'=>'Record does not exist']);
        }
    }
}



// Update User Password
public function updatePasswordUser(Request $request)
{
    // return $request;
    // echo "<pre>"; print_r($request->all()); die();
    

    $validator = Validator::make($request->all(),[
        'new_password' => 'min:8|required_with:confirm_new_password|same:confirm_new_password',
        'confirm_new_password' => 'required|min:8',
    ]);

    if ($validator->passes()){

        $user_id = $request->user_id;
        if(isset($request->user_id) && !empty($request->user_id)){

            $check = DB::select("SELECT password from `users` WHERE id ='$user_id' and del_status='1'");


            $user_id = $request->user_id;
            $new_password = $request->new_password;
            if($check){
                $user = User::where('id',$request->user_id)->update(['password' =>$new_password]);
                return response()->json(['success'=>'Password Change successfully.']);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }else{
            return response()->json(['error'=>'Record does not exist']);
        }

    }else{
        return response()->json(['error'=>$validator->errors()]);
    }

}

// get Reporting Manager Data
public function getUserReportingManagerData(Request $request)
{    
    // return $request;
   $branch_id = $request->branch_id;
   $getReportingManagerData = DB::select("SELECT pc_employees_id,employee_name FROM `tbl_pc_employees` WHERE pc_branch_id ='$branch_id' AND del_status='1'");
  return response()->json($getReportingManagerData);
}

}
