<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AccessGroupHelper extends Controller
{
    public static function globalAccessGroupVar($condition_name,$page_action,$action_status)
    {
        $user_session_data = Auth::user();
        $access_check = '';
        if(!empty($user_session_data)){
            $user_role_id = $user_session_data->role_id;
    
            // $condition_name = 'buyer';
            $buyer_check = DB::SELECT("select main_access_level_id ,condition_name FROM `role_main_access_level` WHERE condition_name='$condition_name' AND del_status='1' LIMIT 1");
            if(!empty($buyer_check)){
                foreach($buyer_check as $bckey=>$bcvalue){
                    $u_main_access_level_id = $bcvalue->main_access_level_id;
                }
                // $action_name = '1';
                $access_check = DB::SELECT("select sub_access_level_id, main_access_level_id, role_id, s_view, s_add, s_edit, s_delete
                FROM `role_sub_access_level` WHERE main_access_level_id='$u_main_access_level_id' AND role_id='$user_role_id' AND $page_action='$action_status'  LIMIT 1");
                return $access_check;
            }
        }
    }
}