<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use App\Traits\DatabaseTrait;

class CompanyMasterController extends Controller
{
    use DatabaseTrait;
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function index(Request $request)
    {

        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = $this->adminRoles();
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
       
            $data = DB::select("select opc.company_id, opc.group_name, opc.web_site_url,opc.tat_period,
            opp.project_id,opp.status as p_status,opp.project_complex_name,
            ops.society_id,ops.building_name,ops.building_type_id,ops.rera_completion_year,ops.unit_status as soc_unit_status,ops.category_id as soc_category_id,
            (select count(*) from op_wing where society_id = ops.society_id  and ops.status =1 ) as wingCnt,
            (select count(*) from op_society where project_id = opp.project_id and status =1  and opp.status =1 and building_name is not null ) as societyCnt,
            dus.unit_status,
            dsl.sub_location_name,
            dl.location_name
            from op_company as opc
            left join op_project as opp on opp.group_id = opc.company_id
            left join op_society as ops on ops.project_id= opp.project_id
            left   join dd_unit_status as dus on dus.unit_status_id= opp.unit_status_of_complex
            left  join dd_sub_location as dsl on dsl.sub_location_id= opp.sub_location_id
            left join dd_location as dl on dl.location_id= opp.location_id
                where opc.group_name is not null 
                    and opc.status ='1'  and (opp.status='1'  or opp.status is null) and (ops.status is null or ops.status = '1' ) order by opc.company_id desc
                ");     
         
         $data = $this->arrayPaginator($data, $request);      
        return view('company_master/company_master',['data'=>$data,'role'=>$role]);
    }

    public function arrayPaginator($array, $request)
    {
        
        $page = $request->get('page', 1);
        $perPage = 20;
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function searchData(Request $request)
    {
        
        $user = Auth::user();    
           
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin','admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }

        $search = $request->search;         

            $data = DB::select("select opc.company_id, opc.group_name, opc.web_site_url,opc.tat_period,
            opp.project_id,opp.status as p_status,opp.project_complex_name,
            ops.society_id,ops.building_name,ops.building_type_id,ops.rera_completion_year,ops.unit_status as soc_unit_status,ops.category_id as soc_category_id,
            (select count(*) from op_wing where society_id = ops.society_id  and ops.status =1 ) as wingCnt,
            (select count(*) from op_society where project_id = opp.project_id and status =1  and opp.status =1 and building_name is not null ) as societyCnt,
            dus.unit_status,
            dsl.sub_location_name,
            dl.location_name
            from op_company as opc
            left join op_project as opp on opp.group_id = opc.company_id
            left join op_society as ops on ops.project_id= opp.project_id
            left   join dd_unit_status as dus on dus.unit_status_id= opp.unit_status_of_complex
            left  join dd_sub_location as dsl on dsl.sub_location_id= opp.sub_location_id
            left join dd_location as dl on dl.location_id= opp.location_id
            where opc.group_name is not null 
            and opc.status ='1'  and (opp.status='1'  or opp.status is null) and (opc.group_name like '%".$search."%' or opp.project_complex_name like '%".$search."%' or ops.building_name like '%".$search."%' ) and (ops.status is null or ops.status = '1' ) order by opc.company_id desc
                ");
             $data = $this->arrayPaginator($data, $request);
  
         return view('company_master/company_master',['data'=>$data]);
    }    

    public function add_company_master_form(Request $request)
    {
        $user = Auth::user();    
           
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin','admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }

        if(isset($request->company_id) && !empty($request->company_id)){
            $company_id = $request->company_id;
            $getCompanyData = DB::select("select * from op_company where company_id = ".$company_id." ");
            if(!empty($getCompanyData)){
                $company_id = $getCompanyData[0]->company_id;
                $group_name = $getCompanyData[0]->group_name;
                $regd_office_address = $getCompanyData[0]->regd_office_address;
                $office_phone_extension = $getCompanyData[0]->office_phone_extension;
                $office_phone = $getCompanyData[0]->office_phone;
                $office_email_id = $getCompanyData[0]->office_email_id;
                $web_site_url = $getCompanyData[0]->web_site_url;
                $physical_tat_period = $getCompanyData[0]->physical_tat_period;
                $tat_period = $getCompanyData[0]->tat_period;
                $builder_invoice_format = $getCompanyData[0]->builder_invoice_format;
                $builder_proforma_format = $getCompanyData[0]->builder_proforma_format;
                $builder_payment_process = $getCompanyData[0]->builder_payment_process;
                $images = $getCompanyData[0]->images;
                $cp_empanelment_agreement = $getCompanyData[0]->cp_empanelment_agreement;
                $company_rating = $getCompanyData[0]->company_rating;
                $cp_code = $getCompanyData[0]->cp_code;
                $comments = $getCompanyData[0]->comments;
                $key_member = $getCompanyData[0]->key_member;
                $help_desk_email = $getCompanyData[0]->help_desk_email;
                $payment_tat_period = $getCompanyData[0]->payment_tat_period;

                // $getHirarchyData = DB::select("SELECT h.*,d.designation_name FROM op_company_hirarchy as h  join dd_designation as d on d.designation_id=h.designation_id where h.company_id=".$company_id." ");
                // $getHirarchyData = DB::select("SELECT h.*,(select designation_name from  dd_designation where designation_id= h.designation_id ) as designation_name,(select name from op_company_hirarchy where reports_to = h.reports_to ) as reporting_to           FROM op_company_hirarchy as h where h.company_id= ".$company_id." ");
                $getHirarchyData = DB::select("SELECT  h.*,di.initials_name,(select designation_name from  dd_designation where designation_id= h.designation_id ) as designation_name,och.name as reporting_to   FROM op_company_hirarchy as h
                left join op_company_hirarchy as och on h.reports_to = och.company_hirarchy_id
                left join dd_initials as di on di.initials_id = h.name_initial
                      where h.company_id= ".$company_id."  order by h.company_hirarchy_id desc ");
                if(!empty($getHirarchyData)){
                    $getHirarchyData = $getHirarchyData;
                }else{
                    $getHirarchyData = array();
                }

                // echo '<pre>'; print_r($getHirarchyData); die();

                $getAwardData = DB::select("select * from op_company_awards_recongnition where company_id =".$company_id. "");  
                if(!empty($getAwardData)){
                    $getAwardData = $getAwardData;
                }else{
                    $getAwardData = array();
                }

                $getProsConsData = DB::select("select * from op_company_pro_cons where company_id =".$company_id. "");  
                if(!empty($getProsConsData)){
                    $getProsConsData = $getProsConsData;
                    // $company_pros = $getProsConsData[0]->company_pros;
                    // $company_cons = $getProsConsData[0]->company_cons;
                }else{
                    $getProsConsData = array();
                    // $company_pros = '';
                    // $company_cons = '';
                }

                $company_hirarchy =  DB::select("select * from op_company_hirarchy where company_id=".$company_id."  ");
               
            }            

        }else{
            $company_id = $group_name =  $regd_office_address = $office_phone_extension = $office_phone ='';
            $office_email_id = $web_site_url = $tat_period = $builder_invoice_format = $builder_proforma_format = '';
            $builder_payment_process = $images = $cp_empanelment_agreement = $company_rating = $cp_code = '';
            $comments = $key_member = $help_desk_email = $company_pros = $company_cons = $physical_tat_period = $payment_tat_period = '';

            $getHirarchyData = array();
            $getAwardData = array();
            $getProsConsData = array();
            $company_hirarchy =  array();
            
           
        }

      

        $initials = $this->getAllData('d','initials');
        $designation = $this->getAllData('d','designation');
        $country_code = $this->getAllData('d','country');      
            $dataArray = array('initials'=>$initials,'designation'=>$designation,'country_code'=>$country_code,'company_hirarchy'=>$company_hirarchy,'company_id'=>$company_id, 'group_name'=>$group_name,  'regd_office_address'=>$regd_office_address, 
            'office_phone_extension'=>$office_phone_extension,   'office_phone'=> $office_phone, 
            'office_email_id'=> $office_email_id, 'web_site_url'=> $web_site_url,'physical_tat_period'=>$physical_tat_period ,'tat_period'=>$tat_period, 
            'builder_invoice_format'=>$builder_invoice_format, 'builder_proforma_format'=> $builder_proforma_format, 
            'builder_payment_process'=> $builder_payment_process, 'images'=>$images, 'cp_empanelment_agreement'=>$cp_empanelment_agreement, 
            'company_rating'=>$company_rating, 'cp_code'=>$cp_code, 'comments'=>$comments, 'key_member'=> $key_member, 
            'help_desk_email'=>$help_desk_email,'getHirarchyData'=>$getHirarchyData,'getAwardData'=>$getAwardData,'getProsConsData'=>$getProsConsData,'payment_tat_period'=>$payment_tat_period
        ); 
        return view('company_master/add_company_master',$dataArray);
    }

    public function add_company_master(Request $request)
    {
        $user = Auth::user();    
           
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = array('super_admin','admin');
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
        
        $form_type = $request->form_type;
        $company_id1 = $request->company_id1;
        if($form_type == 'award_reco'){            
            $data = array();
            $data['awards_reco_name'] = $request->awards_reco_name;
            $data['awards_reco_year'] = $request->awards_reco_year;
            $data['awards_reco_desc'] = $request->awards_reco_desc;
            $data['user_id'] = $this->getUserId();
            $array = array();
            if($request->hasfile('awards_reco_image'))
            {
                
                foreach($request->file('awards_reco_image') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('awards_reco_image'), $rand2);
                     array_push($array,$rand2);                    
                }
                $data['awards_reco_image'] = json_encode($array);
                
            }else{
                $data['awards_reco_image'] = '';
            }

            if(isset($company_id1) && !empty($company_id1)){            
                $data['company_id'] = $company_id1;        
                DB::table('op_company_awards_recongnition')->insert($data); 
                $data['award_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from op_company_awards_recongnition where id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date;
                $data['awards_reco_image1'] = $array;
                return response()->json($data);   
            }else{   
                DB::table('op_company')->insert(['group_name'=>null]);            
                $company_id = DB::getPdo()->lastInsertId();
                $data['company_id'] = $company_id;        
                DB::table('op_company_awards_recongnition')->insert($data); 
                $data['award_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from op_company_awards_recongnition where id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date;
                $data['awards_reco_image1'] = $array;
                return response()->json($data);   
            }            
        }

        if($form_type == 'pros_cons'){   
            if(isset($company_id1) && !empty($company_id1)){
                $data = array();
                $data['company_pros'] = $request->company_pros;
                $data['company_cons'] = $request->company_cons;
                $data['company_id'] = $company_id1; 
                $data['user_id'] = $this->getUserId();
                DB::table('op_company_pro_cons')->insert($data); 
                $data['company_pro_cons_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from op_company_pro_cons where company_pro_cons_id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date;
                return response()->json($data);   

            }else{ 
                $data = array();
                $data['company_pros'] = $request->company_pros;
                $data['company_cons'] = $request->company_cons;
                $data['user_id'] = $this->getUserId();
                DB::table('op_company')->insert(['group_name'=>null,'user_id'=>$this->getUserId()]);            
                $company_id = DB::getPdo()->lastInsertId();
                $data['company_id'] = $company_id;                   
                DB::table('op_company_pro_cons')->insert($data); 
                $data['company_pro_cons_id'] = DB::getPdo()->lastInsertId();
                $getDate = DB::select("select updated_date from op_company_pro_cons where company_pro_cons_id = ".DB::getPdo()->lastInsertId()."");
                $data['updated_date'] = $getDate[0]->updated_date;
                return response()->json($data);   
            }

        } 

        if($form_type == 'hierarchy'){ 
                $data = array();
                $data['name_initial'] = $request->name_initial;
                $data['name'] = $request->name_h;
                $data['country_code_id'] = $request->country_code;
                $data['mobile_number'] = $request->mobile_number_h;
                $data['whatsapp_number'] = $request->wap_number;
                $data['email_id'] = $request->primary_email_h;
                $data['designation_id'] = $request->designation_h;
                $data['reports_to'] = $request->reporting_to_h;
                $data['comments'] = $request->comments_h;
                $data['user_id'] = $this->getUserId();

                if( $request->hasfile('photo_h') )
                { 
                    $rand1 = mt_rand(99,9999999).".".$request->photo_h->extension();                    
                    $request->photo_h->move(public_path('photo_h'), $rand1);
                    $data['photo'] = $rand1;
                }else{
                    $data['photo'] = '';
                }

               

                if(isset($company_id1) && !empty($company_id1)){                
                    $data['company_id'] = $company_id1; 
                    DB::table('op_company_hirarchy')->insert($data); 
                    $data['company_hirarchy_id'] = DB::getPdo()->lastInsertId();   
                    $getDate = DB::select("select updated_date from op_company_hirarchy where company_hirarchy_id = ".DB::getPdo()->lastInsertId()."");
                    $data['updated_date'] = $getDate[0]->updated_date;  
                    if(isset($request->designation_h) && !empty($request->designation_h)){
                        $designationID = DB::select("select designation_name from dd_designation where designation_id=".$request->designation_h." ");
                        $data['designation_name'] = $designationID[0]->designation_name;
                    }
                    if(!empty($request->reporting_to_h)){                        
                        $reportsTo = DB::select("select name from op_company_hirarchy  where company_hirarchy_id=".$request->reporting_to_h." ");
                        $data['reports_name'] = $reportsTo[0]->name;        
                    }else{
                        $data['reports_name'] = "";
                    }

                    if(!empty($request->name_initial)){
                        $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                        $data['initials_name'] = $Initial[0]->initials_name;
                    }else{
                        $data['initials_name'] = '';
                    }
                    return response()->json($data);   

                }else{ 
                    DB::table('op_company')->insert(['group_name'=>null]);            
                    $company_id = DB::getPdo()->lastInsertId();
                    $data['company_id'] = $company_id;        
                    DB::table('op_company_hirarchy')->insert($data); 
                    $data['company_hirarchy_id'] = DB::getPdo()->lastInsertId();
                    $getDate = DB::select("select updated_date from op_company_hirarchy where company_hirarchy_id = ".DB::getPdo()->lastInsertId()."");
                    $data['updated_date'] = $getDate[0]->updated_date;  
                    if(isset($request->designation_h) && !empty($request->designation_h)){
                        $designationID = DB::select("select designation_name from dd_designation where designation_id=".$request->designation_h." ");
                        $data['designation_name'] = $designationID[0]->designation_name;
                    }
                    if(!empty($request->reporting_to_h)){                        
                        $reportsTo = DB::select("select name from op_company_hirarchy  where company_hirarchy_id=".$request->reporting_to_h." ");
                        $data['reports_name'] = $reportsTo[0]->name;        
                    }else{
                        $data['reports_name'] = "";
                    }
                    

                    if(!empty($request->name_initial)){
                        $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial." ");
                        $data['initials_name'] = $Initial[0]->initials_name;
                    }else{
                        $data['initials_name'] = '';
                    }

                    return response()->json($data);   
                }
        }

        if($form_type == 'other_form'){
            $validator = Validator::make($request->all(), [
                'group_name' => 'required',
                // 'reg_office_address' => 'required',
                // 'tat_period' => 'required',                
            ],
            [
                'group_name.required' => 'Group name is required',
                // 'reg_office_address.required' => 'Reg. office address is required',
                // 'tat_period.required' => 'TAT period is required',               
    
            ]);
            if ($validator->passes()) {

                $data = array();
                $data['group_name'] = $request->group_name;
                $data['key_member'] = $request->key_member;
                $data['regd_office_address'] = $request->reg_office_address;
                // $data['office_phone_extension'] = $request->office_phone_extension;
                $data['office_phone'] = $request->office_phone;
                $data['office_email_id'] = $request->office_email_id;
                $data['web_site_url'] = $request->web_site_url;
                if($request->company_rating != 'null'){
                    $data['company_rating'] = $request->company_rating;
                }
                $data['cp_code'] = $request->cp_code;
                $data['physical_tat_period'] = $request->physical_tat_period;
                $data['tat_period'] = $request->tat_period;
                $data['help_desk_email'] = $request->help_desk_email;
                $data['comments'] = $request->comments;
                $data['payment_tat_period'] = $request->payment_tat_period;
                $data['user_id'] = $this->getUserId();

                if($request->hasfile('cp_empanelment_agreement'))
                {
                    $array1 = array();
                    foreach($request->file('cp_empanelment_agreement') as $file)
                    {
                        $rand1 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('cp_empanelment_agreement'), $rand1);
                        array_push($array1,$rand1);                        
                    }

                    $getCp = DB::select("select cp_empanelment_agreement from op_company where company_id= ".$company_id1." ");
                    if(  isset($getCp) &&  !empty($getCp) ){                        
                        $existingCp = json_decode($getCp[0]->cp_empanelment_agreement);                       
                        if(!empty($existingCp)){
                            $finalArrayCp = array_merge($existingCp,$array1);
                        }else{
                            $finalArrayCp = $array1;
                        }
                        
                    }else{
                        $finalArrayCp = $array1;
                    }

                    $data['cp_empanelment_agreement'] = json_encode($finalArrayCp);
                }
                // else{
                //     $data['cp_empanelment_agreement'] = '';
                // }

                if($request->hasfile('images'))
                {
                    $array2 = array();
                    foreach($request->file('images') as $file)
                    {
                        $rand2 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('images'), $rand2);
                        array_push($array2,$rand2);                        
                    }
                    $getImages = DB::select("select images from op_company where company_id= ".$company_id1." ");
                    if(  isset($getImages) &&  !empty($getImages) ){
                        
                        $existingImages = json_decode($getImages[0]->images);                       
                        if(!empty($existingImages)){
                            $finalArrayImg = array_merge($existingImages,$array2);
                        }else{
                            $finalArrayImg = $array2;
                        }
                        
                    }else{
                        $finalArrayImg = $array2;
                    }                   
                    $data['images'] = json_encode($finalArrayImg);

                }
                // else{
                //     $data['images'] = '';
                // }
                
                if($request->hasfile('builder_invoice_format'))
                {
                    $array3 = array();
                    foreach($request->file('builder_invoice_format') as $file)
                    {
                        $rand3 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('builder_invoice_format'), $rand3);
                        array_push($array3,$rand3);                        
                    }


                    $getBuilderInv = DB::select("select builder_invoice_format from op_company where company_id= ".$company_id1." ");
                    if(  isset($getBuilderInv) &&  !empty($getBuilderInv) ){
                        
                        $existingBuilderInv = json_decode($getBuilderInv[0]->builder_invoice_format);                       
                        if(!empty($existingBuilderInv)){
                            $finalArrayBuilderInv = array_merge($existingBuilderInv,$array3);
                        }else{
                            $finalArrayBuilderInv = $array3;
                        }
                        
                    }else{
                        $finalArrayBuilderInv = $array3;
                    }                   

                    $data['builder_invoice_format'] = json_encode($finalArrayBuilderInv);
                }
              

                if($request->hasfile('builder_payment_process'))
                {
                    $array5 = array();
                    foreach($request->file('builder_payment_process') as $file)
                    {
                        $rand5 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('builder_payment_process'), $rand5);
                        array_push($array5,$rand5);                        
                    }

                    $getBuildePayment = DB::select("select builder_payment_process from op_company where company_id= ".$company_id1." ");
                    if(  isset($getBuildePayment) &&  !empty($getBuildePayment) ){                        
                        $existingBuilderPayment = json_decode($getBuildePayment[0]->builder_payment_process);                       
                        if(!empty($existingBuilderPayment)){
                            $finalArrayBuilderPayment = array_merge($existingBuilderPayment,$array5);
                        }else{
                            $finalArrayBuilderPayment = $array5;
                        }
                        
                    }else{
                        $finalArrayBuilderPayment = $array5;
                    }

                    $data['builder_payment_process'] = json_encode($finalArrayBuilderPayment);
                }
                // else{
                //     $data['builder_payment_process'] = '';
                // }

                if(isset($company_id1) && !empty($company_id1)){                
                    DB::table('op_company')->where('company_id',$company_id1)->update($data);                     
                    return response()->json(['success'=>'Record updated successfully.','company_id'=>$company_id1]);   

                }else{                     
                    DB::table('op_company')->insert($data); 
                    return response()->json(['success'=>'Record inserted successfully.','company_id'=>DB::getPdo()->lastInsertId()]);
                    
                }
            }
                return response()->json(['error'=>$validator->errors()->all()]);             
        }
    }

    // user id is pending
    function addDesignationCompanyMaster(Request $request){     
        // echo "<pre>"; print_r($request->all()); exit();
        $validator = Validator::make($request->all(), [
            'add_designation' => 'required'                    
        ],
        [
            'add_designation.required' => 'Designation is required',
            
        ]);        

        if ($validator->passes()) {
            $data = array('designation_name'=>$request->add_designation);
            DB::table('dd_designation')->insert($data); 
            $lastInsertId = DB::getPdo()->lastInsertId();  
            $a = array('value'=>$lastInsertId,'option'=>$request->add_designation);

            return response()->json(['success'=>'Added new records.','a'=>$a]);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    // not used need to check and delete this
    public function update_company_master_form(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); exit();
        $validator = Validator::make($request->all(), [
            'group_name' => 'required',        
        ],
        [
            'group_name.required' => 'Group name is required',
        ]);

        if ($validator->passes()) {
            $data = array();
            $data['group_name'] = $request->group_name;
            $data['regd_office_address'] = $request->reg_office_address;
            $data['office_phone_extension'] = $request->office_phone_extension;
            $data['office_phone'] = $request->office_phone;
            $data['office_email_id'] = $request->office_email_id;
            $data['web_site_url'] = $request->web_site_url;
            $data['company_rating'] = $request->company_rating;            
            // $data['recognition'] = $request->recognition;
            $data['cp_code'] = $request->cp_code;
            $data['tat_period'] = $request->tat_period;
            $data2['awards_reco_name'] = $request->awards_reco_name;          

            $data['key_member'] = $request->key_member;
            $data['help_desk_email'] = $request->help_desk_email;
            $data['comments'] = $request->comments;
            $data1['company_pros'] = $request->company_pros;
            $data1['company_cons'] = $request->company_cons;
            $data['user_id'] = $this->getUserId();
            $data1['user_id'] = $this->getUserId();
           

            $join ="";
            
            if($request->hasfile('images'))
            {
                $array = array();
                foreach($request->file('images') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('images'), $rand2);
                   array_push($array,$rand2);
                    
                }
                $data['images'] = json_encode($array);
            }            
           
            if( $request->hasfile('builder_invoice_format') )
            { 
                $rand1 = mt_rand(99,9999999).".".$request->builder_invoice_format->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->builder_invoice_format->move(public_path('images'), $rand1);
                $data['builder_invoice_format'] = $rand1;
            }

            if( $request->hasfile('builder_invoice_process') )
            { 
                $rand3 = mt_rand(99,9999999).".".$request->builder_invoice_process->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->builder_invoice_process->move(public_path('images'), $rand3);
                $data['builder_invoice_process'] = $rand3;
            }

            if( $request->hasfile('builder_proforma_format') )
            { 
                $rand4 = mt_rand(99,9999999).".".$request->builder_proforma_format->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->builder_proforma_format->move(public_path('images'), $rand4);
                $data['builder_proforma_format'] = $rand4;
            }

            if( $request->hasfile('builder_payment_process') )
            { 
                $rand5 = mt_rand(99,9999999).".".$request->builder_payment_process->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->builder_payment_process->move(public_path('images'), $rand5);
                $data['builder_payment_process'] = $rand5;
            }

            if( $request->hasfile('cp_empanelment_agreement') )
            { 
                $rand6 = mt_rand(99,9999999).".".$request->cp_empanelment_agreement->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->cp_empanelment_agreement->move(public_path('images'), $rand6);
                $data['cp_empanelment_agreement'] = $rand6;
            }

            DB::table('op_company')->where('company_id',$request->company_id)->update($data);

            DB::table('op_company_pro_cons')->where('company_id',$request->company_id)->update($data1); 

            if($request->hasfile('awards_reco_image'))
            {
                $arrayI = array();
                foreach($request->file('awards_reco_image') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('images'), $rand2);
                   array_push($arrayI,$rand2);
                    
                }
               
            }  
            // $data2['company_id'] = $company_id;
            $awards_reco_name = $request->awards_reco_name;
            $cnt2 = count($awards_reco_name);
            
          //  print_r($arrayI);//exit();
            for ($ii=0; $ii < $cnt2; $ii++) {  
                $awards_reco_name1 = $awards_reco_name[$ii];
                // $awards_reco_image1 = $arrayI[$ii];
                $array2 = array('company_id'=>$request->company_id);

                if(!empty($awards_reco_name1[$ii])){
                     $awards_reco_name2 = $awards_reco_name[$ii]; 
                    $a2 = array('awards_reco_name'=>$awards_reco_name2) ;
                    $array2=   array_merge($array2,$a2);                   
                }

                if(!empty($arrayI[$ii])){
                    $awards_reco_image2 = $arrayI[$ii]; 
                    $a2 = array('awards_reco_image'=>$awards_reco_image2) ;
                    $array2=   array_merge($array2,$a2);                   
                }else{
                    $a2 = array('awards_reco_image'=>'') ;
                    $array2=   array_merge($array2,$a2); 
                }
                
                // echo "<pre>";   print_r($array2);///
                DB::table('op_company_awards_recongnition')->insert($array2); 
            }

            $company_hei_id = $request->company_hei_id;
            $name_initial = $request->name_initial;
            $name_h = $request->name_h;
            $country_code = $request->country_code;
            $mobile_number_h = $request->mobile_number_h;
            $country_code_w = $request->country_code_w;
            $wap_number = $request->wap_number;
            $primary_email_h = $request->primary_email_h;
            $designation_h = $request->designation_h;
            $reporting_to_h = $request->reporting_to_h;
            $comments_h = $request->comments_h;

            if($request->hasfile('photo_h'))
            {
                $arrayH = array();
                foreach($request->file('photo_h') as $file)
                {
                    $rand_h = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('images'), $rand_h);
                        array_push($arrayH,$rand_h);
                    
                }
                // $data2['awards_reco_image'] = ($array);
            } 
    
            $cnt = count($name_initial);
            // echo "<pre>"; print_r($company_hei_id[$i); //exit();
            for ($i=0; $i<$cnt ; $i++) {  
                
                $array = array('company_id'=>$request->company_id);

                if(!empty($name_h[$i])){
                    $name = $name_h[$i]; 
                    $a = array('name'=>$name) ;
                    $array=   array_merge($array,$a);                   
                }else{
                    $a = array('name'=>'') ;
                    $array=   array_merge($array,$a);                   
                }
    
                if(!empty($country_code[$i])){
                    $country_code_id = $country_code[$i]; 
                    $a = array('country_code_id'=>$country_code_id) ;
                    $array=   array_merge($array,$a);                   
                }
    
                if(!empty($mobile_number_h[$i])){
                    $mobile_number = $mobile_number_h[$i]; 
                    $a = array('mobile_number'=>$mobile_number) ;
                    $array=   array_merge($array,$a);                   
                }
    
                if(!empty($wap_number[$i])){
                    $whatsapp_number = $wap_number[$i]; 
                    $a = array('whatsapp_number'=>$whatsapp_number) ;
                    $array=   array_merge($array,$a);                   
                }
    
                if(!empty($primary_email_h[$i])){
                    $email_id = $primary_email_h[$i]; 
                    $a = array('email_id'=>$email_id) ;
                    $array=   array_merge($array,$a);                   
                }
    
                if(!empty($designation_h[$i])){
                    $designation_id = $designation_h[$i]; 
                    $a = array('designation_id'=>$designation_id) ;
                    $array=   array_merge($array,$a);                   
                }
    
                if(!empty($reporting_to_h[$i])){
                    $reports_to = $reporting_to_h[$i]; 
                    $a = array('reports_to'=>$reports_to) ;
                    $array=   array_merge($array,$a);                   
                }
    
                if(!empty($comments_h[$i])){
                    $comments = $comments_h[$i]; 
                    $a = array('comments'=>$comments) ;
                    $array=   array_merge($array,$a);                   
                }
    
                if(!empty($arrayH[$i])){
                    $photo = $arrayH[$i]; 
                    $aH = array('photo'=>$photo) ;
                    $array=   array_merge($array,$aH);                   
                }else{
                    $aH = array('photo'=>'') ;
                    $array=   array_merge($array,$aH); 
                }

                if($company_hei_id[$i] == 0 ){
                    // echo "/n"; print_r($company_hei_id[$i]);
                    // echo "<pre>"; print_r();
                    if(!empty($array['name'])){
                        DB::table('op_company_hirarchy')->insert($array);                
                    }
                    
                }else{           
                    // echo "/n"; print_r($company_hei_id[$i]);
                    // echo "<pre>"; print_r($array);
                    DB::table('op_company_hirarchy')->where('company_hirarchy_id',$company_hei_id[$i])->update($array);
                }       

                
                // echo "<pre>"; print_r($array);
                    
            }
            // exit();
         return response()->json(['success'=>'done','lastID'=>$request->company_id]);
        } 
        // echo "<pre>"; print_r($validator->errors()->all());
        // exit();
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function getData(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die(); 
        $form_type = $request->form_type;
        if($form_type == 'award_reco'){
            $award_id = $request->award_id;
            $getData = DB::select("select * from op_company_awards_recongnition where id =".$award_id." ");          
            return $getData;
        }

        if($form_type == 'pros_cons'){
            $company_pro_cons_id = $request->company_pro_cons_id;
            $getData = DB::select("select * from op_company_pro_cons where company_pro_cons_id =".$company_pro_cons_id." ");
            return $getData;
        }

        if($form_type == 'hierarchy'){
            $company_hirarchy_id = $request->company_hirarchy_id;
            $getData = DB::select("select * from op_company_hirarchy where company_hirarchy_id =".$company_hirarchy_id." ");
            return $getData;
        }
    }

    public function updateTableFormRecords(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die();
        $form_type= $request->form_type;
        if($form_type=='award_reco'){
            $award_id = $request->award_id;
            $check = DB::select("select id,awards_reco_image from op_company_awards_recongnition where id =".$award_id."");
        
            if($check){
                $data['awards_reco_name'] = $request->awards_reco_name;
                $data['awards_reco_year'] = $request->awards_reco_year;
                $data['awards_reco_desc'] = $request->awards_reco_desc;
                $data['user_id'] = $this->getUserId();
                $award_id = $request->award_id;
                $data1 = array();
                if($request->hasfile('awards_reco_image'))
                {
                    $array = array();
                    foreach($request->file('awards_reco_image') as $file)
                    {
                        $rand2 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('awards_reco_image'), $rand2);
                        array_push($array,$rand2);
                        
                    }
                    if(isset($check[0]->awards_reco_image) && !empty($check[0]->awards_reco_image)){
                        $a2 = json_decode($check[0]->awards_reco_image);
                        $m = array_merge($array,$a2);
                        $f = json_encode($m);
                        $data['awards_reco_image'] = $f;
                        $data1['img'] = $m;
                    }else{
                        $data['awards_reco_image'] = json_encode($array);
                        $data1['img'] = $array;                        
                    }                    
                }
                DB::table('op_company_awards_recongnition')->where('id',$award_id)->update($data); 
                $getDate = DB::select("select updated_date from op_company_awards_recongnition where id = ".$award_id."");
                $data['updated_date'] = $getDate[0]->updated_date;
                return response()->json(['success'=>'Record updated successfully.','award_id'=>$award_id,'data'=>$data,'data1'=>$data1]);   


            }else{
                return response()->json(['error'=>'Record does not exist']);
            }    
        }

        if($form_type=='pros_cons'){
            $company_pro_cons_id = $request->company_pro_cons_id;
            $check = DB::select("select company_pro_cons_id  from op_company_pro_cons where company_pro_cons_id = ".$company_pro_cons_id." ");
            if($check){
                $data['company_pros'] = $request->company_pros;
                $data['company_cons'] = $request->company_cons;
                $data['user_id'] = $this->getUserId();
                DB::table('op_company_pro_cons')->where('company_pro_cons_id',$company_pro_cons_id)->update($data); 
                $data['company_pro_cons_id'] = $company_pro_cons_id;
                $getDate = DB::select("select updated_date from op_company_pro_cons where company_pro_cons_id = ".$company_pro_cons_id."");
                $data['updated_date'] = $getDate[0]->updated_date;
                return response()->json(['success'=>'Record updated successfully.','company_pro_cons_id'=>$company_pro_cons_id,'data'=>$data]);   

            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }

        if($form_type=='hierarchy'){

            $validator = Validator::make($request->all(), [
                // 'primary_email_h1' => 'required|email',
            ]
           );

        if ($validator->passes()) {

            $company_hirarchy_id = $request->company_hirarchy_id1;
            $check = DB::select("select company_hirarchy_id,photo  from op_company_hirarchy where company_hirarchy_id = ".$company_hirarchy_id." ");
            if($check){
                $data['name_initial'] = $request->name_initial1;
                $data['name'] = $request->name_h1;
                $data['country_code_id'] = $request->country_code1;
                $data['mobile_number'] = $request->mobile_number_h1;
                $data['whatsapp_number'] = $request->wap_number1;
                $data['email_id'] = $request->primary_email_h1;
                $data['designation_id'] = $request->designation_h1;
                $data['reports_to'] = $request->reporting_to_h1;
                $data['photo'] = $request->photo_h1;
                $data['comments'] = $request->comments_h1;
                $data['user_id'] = $this->getUserId();

                if( $request->hasfile('photo_h1') )
                { 
                    $rand1 = mt_rand(99,9999999).".".$request->photo_h1->extension();                    
                    $request->photo_h1->move(public_path('photo_h'), $rand1);
                    $data['photo'] = $rand1;
                }else{
                    $data['photo'] = $check[0]->photo;
                }

                DB::table('op_company_hirarchy')->where('company_hirarchy_id',$company_hirarchy_id)->update($data); 
                $data['company_hirarchy_id'] = $company_hirarchy_id;
                $getDate = DB::select("select updated_date from op_company_hirarchy where company_hirarchy_id = ".$company_hirarchy_id."");
                $data['updated_date'] = $getDate[0]->updated_date;
                if(!empty($request->designation_h1)){
                    $designationID = DB::select("select designation_name from dd_designation where designation_id=".$request->designation_h1." ");
                    $data['designation_name'] = $designationID[0]->designation_name;
                }else{
                    $data['designation_name'] = '';
                }
                
                if(!empty($request->reporting_to_h1)){
                    $reportsTo = DB::select("select name from op_company_hirarchy where company_hirarchy_id=".$request->reporting_to_h1." ");
                    $data['reports_name'] = $reportsTo[0]->name;
                }else{
                    $data['reports_name'] = '';
                }

                if(!empty($request->name_initial1)){
                    $Initial = DB::select("select initials_name from dd_initials where initials_id=".$request->name_initial1." ");
                    $data['initials_name'] = $Initial[0]->initials_name;
                }else{
                    $data['initials_name'] = '';
                }
                
                return response()->json(['success'=>'Record updated successfully.','company_hirarchy_id'=>$company_hirarchy_id,'data'=>$data]);   

            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }else{
            return response()->json(['error'=>'Invalid Email']);
        }

        }
        
    }

    public function deleteTableFormRecords(request $request)
    {
        // echo "<pre>"; print_r($request->all()); exit();
        $form_type= $request->form_type;
        if($form_type=='award_reco'){
            $award_id = $request->award_id;
            $check = DB::select("select id from op_company_awards_recongnition where id =".$award_id."");
            if($check){
                $id = $check[0]->id;
                DB::table('op_company_awards_recongnition')->where('id',$award_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','award_id'=>$award_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
        }
        
        if($form_type=='pros_cons'){
            $company_pro_cons_id = $request->company_pro_cons_id;
            $check = DB::select("select company_pro_cons_id from op_company_pro_cons where company_pro_cons_id =".$company_pro_cons_id."");
            if($check){
                $company_pro_cons_id = $check[0]->company_pro_cons_id;
                DB::table('op_company_pro_cons')->where('company_pro_cons_id',$company_pro_cons_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','company_pro_cons_id'=>$company_pro_cons_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }

        if($form_type=='hirarchy'){
            $company_hirarchy_id = $request->company_hirarchy_id;
            $check = DB::select("select company_hirarchy_id from op_company_hirarchy where company_hirarchy_id =".$company_hirarchy_id."");
            if($check){
                $company_hirarchy_id = $check[0]->company_hirarchy_id;
                DB::table('op_company_hirarchy')->where('company_hirarchy_id',$company_hirarchy_id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','company_hirarchy_id'=>$company_hirarchy_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }

        if($form_type=='company'){
            // echo "<pre>"; print_r($request->all());
            $company_id = $request->company_id;
            $checkCompany = DB::select('select company_id from op_company where company_id ='.$company_id.' ');
            $this->softDelete('op_company','company_id',$company_id);                 
            // echo "<pre>"; print_r($checkCompany);
            if(!empty($checkCompany)){
                // delete all sub company tables           
                // $this->softDelete('op_company_awards_recongnition','company_id',$company_id);
                // $this->softDelete('op_company_hirarchy','company_id',$company_id);
                // $this->softDelete('op_company_pro_cons','company_id',$company_id);
                $checkInProject = DB::select("select project_id from op_project where group_id = ".$company_id."");                
                // echo "<pre>"; echo "checkInProject"; print_r($checkInProject);
                if(!empty($checkInProject)){
                    // delete all sun project tables
                    foreach ($checkInProject as $key => $value) {
                        $project_id = $value->project_id; 
                        $this->softDelete('op_project','project_id',$project_id);
                        // $this->softDelete('op_project_amenities','project_id',$project_id);
                        // $this->softDelete('op_project_hirarchy','project_id',$project_id);
                        // $this->softDelete('op_project_photo','project_id',$project_id);
                        // $this->softDelete('project_pro_cons','project_id',$project_id);                       
                        
                        $checkInSociety = DB::select("select society_id from op_society where project_id = ".$project_id."");
                        if(!empty($checkInSociety)){
                            foreach ($checkInSociety as $key1 => $value1) {
                                $society_id = $value1->society_id;
                                $this->softDelete('op_society','society_id',$society_id);
                                // $this->softDelete('op_society_account_manager','society_id',$society_id);
                                // $this->softDelete('op_society_amnities','society_id',$society_id);
                                // $this->softDelete('op_society_brokerage_fee','society_id',$society_id);
                                // $this->softDelete('op_society_floor_rise','society_id',$society_id);
                                // $this->softDelete('op_society_keymembers','society_id',$society_id);
                                // $this->softDelete('op_society_photos','society_id',$society_id);
                                // $this->softDelete('op_society_pros_cons','society_id',$society_id);
                                // $this->softDelete('society_construction_stage','society_id',$society_id);
                                // $this->softDelete('society_working_days_time','society_id',$society_id);
                                
                                $checkInWing = DB::select("select wing_id from op_wing where society_id = ".$society_id."");
                                // echo "<pre>"; echo "checkInSociety"; print_r($checkInWing);
                                if(!empty($checkInWing)){
                                     foreach ($checkInWing as $key2 => $value2) {
                                         $wing_id = $value2->wing_id;
                                         $this->softDelete('op_wing','wing_id',$wing_id);
                                         $checkInUnit = DB::select("select unit_id from op_unit where wing_id = ".$wing_id."");
                                        //  echo "<pre>"; echo "checkInUnit "; print_r($checkInUnit);
                                        // if(!empty($checkInUnit)){
                                        //     foreach ($checkInUnit as $key3 => $value3) {
                                        //         $unit_id = $value3->unit_id;
                                                //    $this->softDelete('op_unit','unit_id',$unit_id);

                                        //     }
                                        // }
                                     }
                                }else{
                                    echo "<pre>"; echo "delete from op_wing and return";
                                }
                            }

                        }else{
                            echo "<pre>"; echo "delete from op_society and return";
                        }
                      

                    }                  
                }
            }  
        }
    }

    public function softDelete($table,$column,$value)
    {   $data = array('status'=>0,'user_id'=>$this->getUserId());
        DB::table($table)->where($column,$value)->update($data); 

    }

    public function Convert($array)
    {
        $finalArray = array();
        foreach ($array as $key => $value) {
            // echo "<pre>"; print_r($value);
            array_push($finalArray,$value->unit_id);
        }
        return $finalArray;
    }

    // company list
    public function company_list(Request $request)
    {
        // $data = DB::table('op_company')
        // ->selectRaw('op_company.*')//,op_wing.wing_name                                                    
        //  ->where('op_company.group_name','!=','')
        //  //  ->where('op_company.group_name', 'like', "%".$search."%") 
        //  ->where('op_company.status','=','1')
        //  ->orderBy('company_id', 'DESC')   ;     
        //  ->paginate(15);   
        $data = DB::select("select opc.company_id, opc.group_name, opc.web_site_url,opc.tat_period,
        opp.project_id,opp.status as p_status,opp.project_complex_name,
        ops.society_id,ops.building_name,ops.building_type_id,ops.rera_completion_year,ops.unit_status as soc_unit_status,ops.category_id as soc_category_id,
        (select count(*) from op_wing where society_id = ops.society_id  and ops.status =1 ) as wingCnt,
        (select count(*) from op_society where project_id = opp.project_id and status =1  and opp.status =1 and building_name is not null ) as societyCnt,
        dus.unit_status,
        dsl.sub_location_name,
        dl.location_name
        from op_company as opc
        left join op_project as opp on opp.group_id = opc.company_id
        left join op_society as ops on ops.project_id= opp.project_id
        left   join dd_unit_status as dus on dus.unit_status_id= opp.unit_status_of_complex
        left  join dd_sub_location as dsl on dsl.sub_location_id= opp.sub_location_id
        left join dd_location as dl on dl.location_id= opp.location_id
            where opc.group_name is not null 
                and opc.status ='1'  and (opp.status='1'  or opp.status is null) and (ops.status is null or ops.status = '1' ) order by opc.company_id desc
            ");     
            return json_encode($data);

            // echo "<pre>"; print_r( ); die();


         return view('company_master/company_list',['data'=>$data]);
    }

    public function search_company_list(Request $request)
    {
        $search = $request->search;  
        $data = DB::table('op_company')
        ->selectRaw('op_company.*')//,op_wing.wing_name                                                    
        //  ->where('op_company.group_name','!=','')
          ->where('op_company.group_name', 'like', "%".$search."%") 
         ->where('op_company.status','=','1')
         
        //  ->orWhere('op_project.project_complex_name','like', "%".$search."%")                                        
         ->orderBy('company_id', 'DESC')   
         ->paginate(5)->appends(['search'=>$search]);
                        
        //  ->paginate(5);   

            


         return view('company_master/company_list',['data'=>$data]);
    }

    public function flow_chart_company(Request $request)
    {        
   
             
        $data= DB::select("select och.reports_to,och.name as reports_to_name, oh.name, d.designation_name  from op_company_hirarchy  as oh
        left join op_company_hirarchy  as och on och.reports_to= oh.company_hirarchy_id
        left join dd_designation as d on d.designation_id = och.designation_id 
        where oh.company_id =".$request->company_id."  " );
     
        $intAr = array();
        foreach($data as $value1){            
        //    array_push($cid,$value->company_hirarchy_id);       
                // echo "<pre>"; print_r($value);
                // [{'v':'Mike', 'f':'Mike<div style="color:red; font-style:italic">President</div>'},
                // '', '']
                // array({{'v:'.$value1->reports_to_name,'f':'<span style="color:red>'.$value1->name.'</span>',''},'','');
                // $d = array("v"=>$value1->reports_to_name ,"f"=>"$value1->reports_to_name<div style='color:red; font-style:italic'>$value1->designation_name</div>");
                // $tempAr = array($d,$value1->name,'');
                $tempAr = array($value1->reports_to_name,$value1->name,'');
                
                // echo "<pre>"; print_r($tempAr);
                 array_push($intAr,$tempAr);
        }
        // echo "<pre>"; print_r($tempAr);
        echo json_encode($intAr);     
     
    }
    
    public function delImage(Request $request)
    {
        
        $image = $request->image;
        $type = $request->type;
        $company_id = $request->company_id;

        if(  $type == 'cp' ){
            $getCPA = DB::select('select  cp_empanelment_agreement from op_company where company_id ='.$company_id.' ');
            $cp_empanelment_agreement = json_decode($getCPA[0]->cp_empanelment_agreement);
            $final_array= array_diff($cp_empanelment_agreement, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_company')->where('company_id',$company_id)->update(array('cp_empanelment_agreement'=>$enc,'user_id'=>$this->getUserId()));    
        }

        if(  $type == 'bif' ){
            $getCPA = DB::select('select  builder_invoice_format from op_company where company_id ='.$company_id.' ');
            $builder_invoice_format = json_decode($getCPA[0]->builder_invoice_format);
            $final_array= array_diff($builder_invoice_format, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_company')->where('company_id',$company_id)->update(array('builder_invoice_format'=>$enc,'user_id'=>$this->getUserId()));   
        }

        if(  $type == 'bpf' ){
            $getCPA = DB::select('select  builder_proforma_format from op_company where company_id ='.$company_id.' ');
            $builder_proforma_format = json_decode($getCPA[0]->builder_proforma_format);
            $final_array= array_diff($builder_proforma_format, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_company')->where('company_id',$company_id)->update(array('builder_proforma_format'=>$enc,'user_id'=>$this->getUserId()));   
        }

        if(  $type == 'bpp' ){
            $getCPA = DB::select('select  builder_payment_process from op_company where company_id ='.$company_id.' ');
            $builder_payment_process = json_decode($getCPA[0]->builder_payment_process);
            $final_array= array_diff($builder_payment_process, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_company')->where('company_id',$company_id)->update(array('builder_payment_process'=>$enc,'user_id'=>$this->getUserId()));   
        }

        if(  $type == 'img' ){
            $getCPA = DB::select('select  images from op_company where company_id ='.$company_id.' ');
            $images = json_decode($getCPA[0]->images);
            $final_array= array_diff($images, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_company')->where('company_id',$company_id)->update(array('images'=>$enc,'user_id'=>$this->getUserId()));   
        }       
        
        if(  $type == 'awards' ){
            $getCPA = DB::select('select  awards_reco_image from op_company_awards_recongnition where awards_reco_name ='.$image.' ');
            $awards_reco_image = json_decode($getCPA[0]->awards_reco_image);
            $final_array= array_diff($awards_reco_image, array($image));
            $enc = json_encode(array_values($final_array));
            DB::table('op_company')->where('company_id',$company_id)->update(array('awards_reco_image'=>$enc,'user_id'=>$this->getUserId()));   
        }

        
    }

    public function delImageHei(Request $request)
    {
        // echo "<pre>"; print_r($request->all());
        $company_hirarchy_id = $request->company_hirarchy_id;
        // $company_id = $request->company_id;
        DB::table('op_company_hirarchy')->where('company_hirarchy_id',$company_hirarchy_id)->update(array('photo'=>'','user_id'=>$this->getUserId()));   
    }

    public function delImageAward(Request $request)
    {
        // echo "<pre>"; print_r($request->all());
        $id = $request->id;
        $image = $request->image;

        $getImgData = DB::select("select awards_reco_image from op_company_awards_recongnition where id=".$id." ");
        // $awards_reco_image = $getImgData[0]->awards_reco_image;

        $awards_reco_image = json_decode($getImgData[0]->awards_reco_image);
        $final_array= array_diff($awards_reco_image, array($image));
        $enc = json_encode(array_values($final_array));
        DB::table('op_company_awards_recongnition')->where('id',$id)->update(array('awards_reco_image'=>$enc,'user_id'=>$this->getUserId()));   

    }
        
}




    

       