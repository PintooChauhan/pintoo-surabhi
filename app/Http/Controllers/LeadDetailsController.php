<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;
use App\Traits\DatabaseTrait;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AccessGroupHelper;

class LeadDetailsController extends Controller
{
   use DatabaseTrait;

   public function buyerListView(Request $request)
   {
        // return $request;

        $user = Auth::user();         
        //    echo "<pre>"; print_r( $user); die();
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }

        /************* Role Check Start  *********************/
        $condition_name = 'buyer';
        $page_action = 's_view';
        $action_status = 1;
        $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

        $data['access_check'] = $access_check;

        /************* Role Check End  *********************/

        if(!empty($access_check)){

            $role = $this->getRole();      
            $user_id = $this->getUserId();
            $roles = $this->adminRoles();
            
            $data['role'] = $role;
            $data['roles'] = $roles;

            $j='';
            $j1 = '';
            if( !in_array($role,$roles) ){
                $j .= 'and lb.lead_assigned_id='.$user_id;
                $j1 .= " and ( bt.lead_stage not in(17,18) or bt.lead_stage is null )";
            }

            if(!isset($request->check) && empty($request->check)){

                $data['allData'] = DB::select("select bt.f2f_done,bt.c_date as c_date , bt.unit_type_id ,bt.buyer_tenant_id,bt.units,(select lead_by_name from dd_lead_by where lead_by_id = (select lead_by from tbl_lead where buyer_tenant_id = bt.buyer_tenant_id) ) as lead_by_name,(select customer_name from tbl_customer where id= bt.customer_id) as lead_source_name,(select lead_stage_name from dd_lead_stage where lead_stage_id = bt.lead_stage)  as lead_stage,
                (select primary_mobile_number from tbl_customer where id= bt.customer_id) as contact_no,
                bt.configuration_id,
                (select minimum_budget_in_short from dd_minimum_budget as minb where minb.minimum_budget = bt.min_budget) as min_budget,
            (select maximum_budget_in_short from dd_maximum_budget as maxb where maxb.maximum_budget= bt.max_budget) as max_budget
            ,bt.sublocation_id,bt.location_id,bt.min_carpet_area,bt.max_carpet_area,bt.unit_status,
                bt.finalization_month,bt.lead_intesity_id,bt.lead_intesity_id ,bt.lead_analysis,
                (select possession_year from dd_possession_year where dd_possession_year_id =  bt.possision_year)  as possision_year,
                (select customer_res_address from tbl_customer where id= bt.customer_id)  as customer_res_address,
                (select looking_since from dd_looking_since as look where look.looking_since_id= bt.looking_since) as looking_since,
                (select building_age from dd_building_age as bg where bg.dd_building_age_id= bt.building_age) as building_age,
                (select name from users where id=lb.lead_assigned_id) as lead_assigned_name,
                (select lead_source_name from dd_lead_source where lead_source_id= (select lead_source_name from tbl_lead where buyer_tenant_id = bt.buyer_tenant_id) ) as lead_source_name1 
                from tbl_buyer_tenant as bt 
                join tbl_lead as lb on lb.buyer_tenant_id  = bt.buyer_tenant_id  
            where bt.type='buyer'  and bt.status=1 ".$j." ".$j1."
            order by bt.buyer_tenant_id desc");
            

            }else{
                // return $request;
                $campaign_id = $request->lead_campaign_name;
                $lead_assigned_id = $request->lead_assigned_name;
                $unit_type_id = $request->unit_type;
                $door_facing_direction = $request->door_facing_direction;
                $floor_choice = $request->floor_choice;
                $purpose = $request->purpose;
                $own_contribution_amount = $request->own_contribution;
                $contribution_source = $request->contribution_source; // multi
                $oc_time_period = $request->oc_time_period;
                $loan_status_id = $request->loan_status;
                $looking_since = $request->looking_since;
                $building_type_id = $request->building_type;
                $parking_type_id = $request->parking_type;
                $property_seen = $request->property_seen;
                $payment_plan_id = $request->payment_plan;
                $expected_closure_date = $request->expected_closure_date;
            
            $campaign_id_cod = "";
            $lead_assigned_id_cod = "";
            $unit_type_id_cod = "";
            $door_facing_direction_cod = "";
            $floor_choice_cod = "";
            $purpose_cod = "";
            $own_contribution_amount_cod = "";
            $contribution_source_cod = "";
            $oc_time_period_cod = "";
            $loan_status_id_cod = "";
            $looking_since_cod = "";
            $building_type_id_cod = "";
            $parking_type_id_cod = "";
            $property_seen_join_cod = "";
            $property_seen_cod = "";
            $payment_plan_id_cod = "";
            $expected_closure_date_cod = "";

            if(!empty($campaign_id)){
                $campaign_id_cod = " AND lb.campaign_id='$campaign_id'";
            }

            if(!empty($lead_assigned_id)){
                $lead_assigned_id_cod = " AND lb.lead_assigned_id='$lead_assigned_id'";
            }
            
            if(!empty($unit_type_id)){
                
                $unit_type_id_cod = " AND bt.unit_type_id='$unit_type_id'";
            }

            if(!empty($door_facing_direction)){
                
                // $door_facing_direction_arr = explode(",",$door_facing_direction);
                $door_facing_direction_cod = " OR bt.door_direction_id='[$door_facing_direction]'";
            }

            if(!empty($floor_choice)){
               
                $floor_choice_cod = " OR bt.floor_choice='[$floor_choice]'";
            }
            
            if(!empty($purpose)){
               
                $purpose_cod = " OR bt.purpose_id='[$purpose]'";
            }

            if(!empty($own_contribution_amount)){
                
                $own_contribution_amount_cod = " AND bt.own_contribution_amount='$own_contribution_amount'";
            }
            
            if(!empty($contribution_source)){
                
                $contribution_source_cod = " AND bt.oc_source_id='[$contribution_source]'";
            }
            
            if(!empty($oc_time_period)){
                
                $oc_time_period_cod = " AND bt.oc_time_period='[$oc_time_period]'";
            }

            if(!empty($loan_status_id)){
                
                $loan_status_id_cod = " AND bt.loan_status_id='[$loan_status_id]'";
            }

            if(!empty($looking_since)){
                
                $looking_since_cod = " AND bt.looking_since='$looking_since'";
            }

            if(!empty($building_type_id)){
                
                $building_type_id_cod = " AND bt.building_type_id='[$building_type_id]'";
            }
            
            if(!empty($parking_type_id)){
                
                $parking_type_id_cod = " AND bt.parking_type_id='[$parking_type_id]'";
            }
            
            if(!empty($property_seen)){
                
                $property_seen_join_cod = " join tbl_property_seen as psb on psb.buyer_tenant_id = bt.buyer_tenant_id";
                $property_seen_cod = " AND psb.property_visited_by='[$property_seen]'";
            }

            if(!empty($payment_plan_id)){
                
                $payment_plan_id_cod = " AND bt.payment_plan_id='[$payment_plan_id]'";
            }
            
            if(!empty($expected_closure_date)){
                
                $expected_closure_date_cod = " AND bt.expected_closure_date='$expected_closure_date'";
            }

            $data['allData'] = DB::select("select bt.f2f_done,bt.c_date as c_date , bt.unit_type_id ,bt.buyer_tenant_id,bt.units,(select lead_by_name from dd_lead_by where lead_by_id = (select lead_by from tbl_lead where buyer_tenant_id = bt.buyer_tenant_id) ) as lead_by_name,(select customer_name from tbl_customer where id= bt.customer_id) as lead_source_name,(select lead_stage_name from dd_lead_stage where lead_stage_id = bt.lead_stage)  as lead_stage,
            (select primary_mobile_number from tbl_customer where id= bt.customer_id) as contact_no,
            bt.configuration_id,
            (select minimum_budget_in_short from dd_minimum_budget as minb where minb.minimum_budget = bt.min_budget) as min_budget,
        (select maximum_budget_in_short from dd_maximum_budget as maxb where maxb.maximum_budget= bt.max_budget) as max_budget
        ,bt.sublocation_id,bt.location_id,bt.min_carpet_area,bt.max_carpet_area,bt.unit_status,
            bt.finalization_month,bt.lead_intesity_id,bt.lead_intesity_id ,bt.lead_analysis,
            (select possession_year from dd_possession_year where dd_possession_year_id =  bt.possision_year)  as possision_year,
            (select customer_res_address from tbl_customer where id= bt.customer_id)  as customer_res_address,
            (select looking_since from dd_looking_since as look where look.looking_since_id= bt.looking_since) as looking_since,
            (select building_age from dd_building_age as bg where bg.dd_building_age_id= bt.building_age) as building_age,
            (select name from users where id=lb.lead_assigned_id) as lead_assigned_name,
            (select lead_source_name from dd_lead_source where lead_source_id= (select lead_source_name from tbl_lead where buyer_tenant_id = bt.buyer_tenant_id) ) as lead_source_name1 
            from tbl_buyer_tenant as bt 
            join tbl_lead as lb on lb.buyer_tenant_id  = bt.buyer_tenant_id 
            ".$property_seen_join_cod."
        where bt.type='buyer'  and bt.status=1 ".$j." ".$j1." ".$campaign_id_cod." ".$lead_assigned_id_cod." ".$unit_type_id_cod." 
        ".$door_facing_direction_cod." ".$floor_choice_cod." ".$purpose_cod." ".$own_contribution_amount_cod." 
        ".$contribution_source_cod." ".$oc_time_period_cod." ".$loan_status_id_cod." ".$looking_since_cod." 
        ".$building_type_id_cod." ".$parking_type_id_cod." ".$property_seen_cod." ".$payment_plan_id_cod." 
        ".$expected_closure_date_cod." order by bt.buyer_tenant_id desc");

            }

            // return $data;
            return view('list_view_buyer',$data);
        }else{
            return Redirect()->back()->with('error','You do not have access to this page.');
        }

   }
    

    public function lead_details(Request $request)
    {
       $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }


        /************* Role Check Start  *********************/
        $condition_name = 'buyer';
        if(isset($request->buyer_tenant_id) && !empty($request->buyer_tenant_id)){
            $page_action = 's_edit';
        }else{
            $page_action = 's_add';
        }

        $action_status = 1;
        $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

        /************* Role Check End  *********************/

        if(!empty($access_check)){

            $role = $this->getRole();
            


            if(isset($request->buyer_tenant_id) && !empty($request->buyer_tenant_id)){
                // echo "<pre>"; print_r($request->buyer_tenant_id); die();
                $data['buyerId'] = array('buyer_tenant_id'=>$request->buyer_tenant_id);

                $getBuyerData = DB::select(" select * from tbl_buyer_tenant where buyer_tenant_id =".$request->buyer_tenant_id."");
                $data['getBuyerData'] = $getBuyerData;
                if(!empty($getBuyerData)){
                    $data['getCustomerData'] = DB::select(" select * from tbl_customer where id =".$getBuyerData[0]->customer_id."");
                }else{
                    $data['getCustomerData'] = array();
                }            

                $getmemberData = DB::select(" select * from tbl_key_members where buyer_tenant_id =".$request->buyer_tenant_id."");
                if(!empty($getmemberData)){
                    $data['getmemberData'] = $getmemberData;
                }else{
                    $data['getmemberData'] = array();
                }

                $getPropertySeenData = DB::select("select ps.*
                from tbl_property_seen as ps where ps.buyer_tenant_id = ".$request->buyer_tenant_id."");
                if(!empty($getPropertySeenData)){
                    $data['getPropertySeenData'] = $getPropertySeenData;
                }else{
                    $data['getPropertySeenData'] = array();
                }

                $getChallengesData = DB::select("select * from tbl_challanges where buyer_tenant_id =".$request->buyer_tenant_id."");
                if(!empty($getChallengesData)){
                    $data['getChallengesData'] = $getChallengesData;
                }else{
                    $data['getChallengesData'] = array();
                }

                $getSpecificChoiceData = DB::select("select * from tbl_specific_choice where buyer_tenant_id =".$request->buyer_tenant_id."");
                // echo "<pre>"; print_r($getSpecificChoiceData); die;
                if(!empty($getSpecificChoiceData)){
                    $data['getSpecificChoiceData'] = $getSpecificChoiceData;
                }else{
                    $data['getSpecificChoiceData'] = array();
                }

                $getLeadSourceData = DB::select("select * from tbl_lead where buyer_tenant_id =".$request->buyer_tenant_id."");
                if(!empty($getLeadSourceData)){
                    $data['getLeadSourceData'] = $getLeadSourceData;
                }else{
                    $data['getLeadSourceData'] = array();
                }

            }else{
                $data['buyerId'] = array();
                $data['getBuyerData'] = array();
                $data['getCustomerData'] = array();
                $data['getmemberData'] = array();
                $data['getPropertySeenData'] = array();
                $data['getChallengesData'] = array();
                $data['getSpecificChoiceData'] = array();
                $data['getLeadSourceData'] = array();
            }   
            // echo "<pre>"; print_r(   $data['getCustomerData']  );die();
            
            return view('lead_details3',$data);
        }else{
            return Redirect()->back()->with('error','You do not have access to this page');
        }
    }

    public function getMinCarpetArea(Request $request)
    {
        $configuration = $request->configId;

        if(!empty( $configuration) && isset( $configuration)){
            $getData = DB::select("select * from dd_minimum_carpet where configuration_id >= ".min( $configuration)."  order by minimum_carpet asc ");
        }else{
            $getData = array();
        }

        return response()->json($getData);
        
    }

    public function getMaximumBudget(Request $request)
    {       
        $minimum_budget_amt = $request->minimum_budget_amt;
        if(!empty($minimum_budget_amt)){
            $getMaxBudget = DB::select("select * from dd_maximum_budget where maximum_budget >= '".$minimum_budget_amt."' order by maximum_budget asc ");
            return response()->json($getMaxBudget);
        }
    }

    public function getOwnContribution(Request $request)
    {       
        $maximum_budget_amt = $request->maximum_budget_amt;
        if(!empty($maximum_budget_amt)){
            $getOwnContri = DB::select("select * from dd_own_contribution where own_contribution <= '".$maximum_budget_amt."' order by own_contribution asc");
            return response()->json($getOwnContri);
        }
    }

    public function getUnitTypes(Request $request)
    {
        $unit_type = $request->unit_type;
        if(!empty($unit_type)){
            if($unit_type == 'commercial'){
                $getUnits = DB::select("select * from dd_unit_type_commercial  ");
            }elseif($unit_type == 'residential'){
                $getUnits = DB::select("select * from dd_unit_type_residential ");
            }            
            return response()->json($getUnits);
        }
    }

    

    public function saveBuyer(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        
        $user_id = $this->getUserId();
        $buyer_tenant_id = $request->buyer_tenant_id;
        $customer_id = $request->customer_id;
        // below is  in table :     tbl_customer
          $customerData['intials'] = $request->cus_name_initial;
          $customerData['customer_name'] = ucwords($request->customer_name1);
          $customerData['primary_country_code_id'] = $request->mobile_conuntry_code;
          $customerData['primary_mobile_number'] = $request->mobile_number_primary;
          $customerData['primary_wa_countryc_code'] = $request->country_code_wa;
          $customerData['primary_wa_number'] = $request->whatsapp_number_primary;
          $customerData['primary_email'] = $request->primary_email;

          $customerData['secondary_country_code_id'] = $request->secondary_country_code_id;
          $customerData['seccondary_mobile_number'] = $request->seccondary_mobile_number;
          $customerData['secondary_wa_countryc_code'] = $request->secondary_wa_countryc_code;
          $customerData['secondary_wa_number'] = $request->secondary_wa_number;
          $customerData['secondary_email'] = $request->secondary_email;

          $customerData['customer_res_address'] = $request->current_residence_address;
          $customerData['customer_city'] = $request->current_residence_city;
          $customerData['customer_state'] = $request->current_residence_state;
          $customerData['customer_pin_code'] = $request->current_residence_pincode;
          $customerData['customer_company_name_address'] = $request->company_name;        
          $customerData['company_city'] = $request->company_city;
          $customerData['company_state'] = $request->company_state;
          $customerData['company_pincode'] = $request->company_pincode;
          $customerData['customer_designation_id'] = $request->designation;
          $customerData['industry']  = $request->industry_name;
          $customerData['information']  = $request->information;
          $customerData['rent_amount']  = $request->rent_amount;
          $customerData['user_id']  = $user_id;
          
          if( isset($customer_id) && !empty($customer_id) ){
            // update tbl_customer
            DB::table('tbl_customer')->where('id',$customer_id)->update($customerData);      
          }else{
              
              DB::table('tbl_customer')->insert($customerData);
              $customer_id  =DB::getPdo()->lastInsertId();
          }
          


        // Unit tab
        $data['customer_id'] = $customer_id;
        $data['unit_type_id'] = $request->unit_type;//json encode
        
        if(isset($request->units) && !empty($request->units)) {
            $data['units'] = json_encode($request->units);// json_encode
        }

        if(isset($request->configuration) && !empty($request->configuration)) {
            $data['configuration_id'] = json_encode($request->configuration);// json_encode
        }




        $data['unit_status'] = $request->unit_status;
        $data['possision_year'] = $request->possession_year;
        $data['possision_year'] = $request->possession_year;
        $data['payment_plan_id'] = json_encode($request->payment_plan);

        if( isset($request->minimum_carpet_area) && !empty($request->minimum_carpet_area)  ){
            $data['min_carpet_area'] = $request->minimum_carpet_area;
        }
        //$data['minimum_carpet_area_unit']  = $request->minimum_carpet_area_unit;
        if( isset($request->maximum_carpet_area) && !empty($request->maximum_carpet_area)  ){
            $data['max_carpet_area'] = $request->maximum_carpet_area;
        }
        //$data['maximum_carpet_area_unit']  = $request->maximum_carpet_area_unit;
        $data['location_id'] = json_encode($request->location11);
        $data['sublocation_id'] = json_encode($request->sub_location1);
        //Budget Tab
        $data['min_budget'] = $request->minimum_budget;
        $data['max_budget'] = $request->maximum_budget;
        $data['own_contribution_amount'] = $request->own_contribution_amount;
        $data['oc_source_id'] =json_encode( $request->own_contribution_source);
        $data['oc_time_period'] = $request->oc_time_period;
        $data['Other_financial_details'] = $request->other_financial_details;

        //Purpose Tab
        $data['purpose_id'] = json_encode($request->purpose);//json encode

        //Home loan Tab
        $data['preferred_bank_id'] = json_encode($request->preferred_bank);//json_encode
        $data['loan_status_id'] = $request->loan_status;
        $data['Loan_stage_Id'] = $request->loan_stage;
        $data['home_loan_comment'] = $request->home_loan_comment; // Field not in table
        $data['need_assistance'] = $request->need_assistance;

        //Finalization Tab
        $data['finalization_month'] = json_encode($request->finalization_month);
        $data['reason_for_finalization'] = $request->reason_for_finalization;

        //Building & Unit Preference Tab
        $data['furnishing_status'] = $request->furnishing_status;
        $data['building_age'] = $request->building_age;
        $data['floor_choice'] = json_encode($request->floor_choice);
        $data['door_direction_id'] = json_encode($request->door_facing_direction);
        $data['bathroom_id'] = json_encode($request->bathroom);
        $data['balcony_id'] = json_encode($request->balcony);
        

        // Parking Tab
        $data['parking_type_id'] = json_encode($request->parking_type);// json_encode
        $data['no_of_two_wheeler_parking'] = $request->no_of_tw_parking;
        $data['no_of_four_wheeler_parking'] = $request->no_of_fw_parking; 

  

        //F2F & Address Tab
        $data['current_residential_status'] = $request->current_residence_status;
        //$data['unit_type_id']  = $request->rent_amount;

       

        //Family Members Tab
        $data['familly_size'] = $request->family_size;        
        $data['working_members'] = $request->working_members;
        $data['member_details'] = $request->members_details;

        //Nature of Business & Washroom Tab
        $data['nature_of_business_id'] = $request->nature_of_bussiness;
        $data['washroom_type_id'] = json_encode($request->washroom); // json_encode

        //Brokerage fees tab
        $data['brokerage_fees'] = $request->brokerage_fees;
        $data['brokerage_fees_comment']  = $request->brokerage_fees_comment; // no field in Db

        //Lead Analysis & Intensity Tab   
        $data['lead_stage']  = $request->lead_stage;    // no field in Db    
        $data['lead_analysis'] = $request->lead_analysis;
        $data['lead_intesity_id'] = $request->lead_intensity;
        $data['expected_closure_date'] = $request->expected_closure_date;
        $data['describe_in_method'] = $request->describe_in_5wh;
        $data['looking_since'] = $request->looking_since;

        
        $data['f2f_done'] = $request->f2f_done;
        $data['user_id']  = $user_id;
       
      

        if(isset($buyer_tenant_id) && !empty($buyer_tenant_id) ){
            // update tbl_customer
            DB::table('tbl_buyer_tenant')->where('buyer_tenant_id',$buyer_tenant_id)->update($data);      
          }else{
            //   die('prasanna');
            $data['type'] ='buyer';
            $data['c_date'] = date('Y-m-d H:i');
            DB::table('tbl_buyer_tenant')->insert($data);
            $buyer_tenant_id  =DB::getPdo()->lastInsertId();
            $specific_choice_data1['buyer_tenant_id'] = $buyer_tenant_id;
            $specific_choice_data1['user_id']  = $user_id;
            DB::table('tbl_specific_choice')->insert($specific_choice_data1);

            $key_member_data1['buyer_tenant_id'] = $buyer_tenant_id;
            $key_member_data1['user_id']  = $user_id;
            DB::table('tbl_key_members')->insert($key_member_data1);

            $leadData1['buyer_tenant_id'] = $buyer_tenant_id;
            $leadData1['user_id'] = $user_id;
            DB::table('tbl_lead')->insert($leadData1);
          }

       
            //Lead Source Tab
            $leadData['lead_by'] = $request->leadby;
            $leadData['lead_by_pc_company'] = json_encode($request->pc_company_name);
            $leadData['lead_source_name'] = $request->lead_source_name;
            $leadData['campaign_id'] = $request->lead_campaign_name;
            $leadData['lead_reached_from'] = $request->lead_reached_from;
            $leadData['lead_assigned_id'] = $request->lead_assigned_name;
            $leadData['user_id'] = $user_id;
        //   echo "<pre>"; print_r($leadData);die();
            if(isset($buyer_tenant_id) && !empty($buyer_tenant_id) ){
                DB::table('tbl_lead')->where('buyer_tenant_id',$buyer_tenant_id)->update($leadData);      
            }else{
                $leadData['buyer_tenant_id'] = $buyer_tenant_id;
                DB::table('tbl_lead')->insert($leadData);
            }

        //Specific Choice Tab  new table :tbl_specific_choice
       
        $specific_choice_data['complex_name'] = json_encode($request->project_name);
        $specific_choice_data['building_name'] = json_encode($request->building_name);
        // $specific_choice_data['sub_location'] = json_encode($request->sub_location);
        // $specific_choice_data['location'] = json_encode($request->location);
        $specific_choice_data['building_amenities'] = json_encode($request->society_amnities1);
        $specific_choice_data['unit_amenities'] = json_encode($request->unit_amenities);
        $specific_choice_data['comment'] = $request->specific_choice_comment;
        $specific_choice_data['user_id'] = $user_id;

        if(isset($buyer_tenant_id) && !empty($buyer_tenant_id) ){
            DB::table('tbl_specific_choice')->where('buyer_tenant_id',$buyer_tenant_id)->update($specific_choice_data);      
        }else{
            $specific_choice_data['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_specific_choice')->insert($specific_choice_data);
        }
    
        //Key members Tab . Table : tbl_key_members
        
        $key_member_data['decision_initial'] = $request->decision_initial;
        $key_member_data['decision_maker_name'] = $request->decision_maker_name;
        $key_member_data['relation_with_buyer'] = json_encode($request->relation_with_buyer);
        $key_member_data['decision_in_5wh'] = $request->decision_in_5wh;
        $key_member_data['financer_initial'] = $request->financer_initial;
        $key_member_data['financer_name'] = $request->financer_name;
        $key_member_data['relation_with_buyer_f'] = json_encode($request->relation_with_buyer_f);
        $key_member_data['financer_in_5wh'] = $request->financer_in_5wh;
        $key_member_data['influencer_initial'] = $request->influencer_initial;
        $key_member_data['influencer_name'] = $request->influencer_name;
        $key_member_data['relation_with_buyer_i'] = json_encode($request->relation_with_buyer_i);
        $key_member_data['influencer_in_5wh'] = $request->influencer_in_5wh;
        $key_member_data['user_id'] = $user_id;
        if(isset($buyer_tenant_id) && !empty($buyer_tenant_id) ){
            // update tbl_customer
            DB::table('tbl_key_members')->where('buyer_tenant_id',$buyer_tenant_id)->update($key_member_data);      
        }else{
            $key_member_data['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_key_members')->insert($key_member_data);

        }

    }

    public function lookingSince(Request $request)
    {
        // generete customer id and buyer_tenant id and set in hidden fields
        //    echo "<pre>"; print_r($request->all()); die();
        
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
   
        $user_id = $this->getUserId();

      
        $data['property_seen'] = $request->property_seen1;
        $data['unit_information'] = $request->unit_information;
        $data['likes'] = $request->unit_information_like;
        $data['dislike'] = $request->unit_information_dislike;
        $data['property_visited_by'] = $request->visited_by;
        $data['date_of_visit'] = $request->date_of_visit;
        $data['physical_virtual'] = $request->physical_virtual;
        $data['no_of_days'] = $request->no_of_days;        
        $data['user_id'] = $user_id;   

        $buyer_tenant_id = $request->buyer_tenant_id;
        if(empty($buyer_tenant_id)){
            // insert here
            $emptyData = array('created_date'=>date("Y-m-d"),'user_id'=>$user_id);
            
            DB::table('tbl_customer')->insert($emptyData);
            $customer_id =   DB::getPdo()->lastInsertId();
           // $data['c_date'] = date('Y-m-d H:i');
            $emptyData1 = array('customer_id'=>$customer_id,'type'=>'buyer','user_id'=>$user_id,'c_date'=>date('Y-m-d H:i'));
            DB::table('tbl_buyer_tenant')->insert($emptyData1);
            $buyer_tenant_id  =DB::getPdo()->lastInsertId();

            $specific_choice_data1['buyer_tenant_id'] = $buyer_tenant_id;
            $specific_choice_data1['user_id'] = $user_id;   
            DB::table('tbl_specific_choice')->insert($specific_choice_data1);

            $key_member_data1['buyer_tenant_id'] = $buyer_tenant_id;
            $key_member_data1['user_id'] = $user_id;   
            DB::table('tbl_key_members')->insert($key_member_data1);

            $leadData1['buyer_tenant_id'] = $buyer_tenant_id;
            $leadData1['user_id'] = $user_id;   
            DB::table('tbl_lead')->insert($leadData1);

         //   echo"<pre>"; print_r($buyer_tenant_id);
            $data['buyer_tenant_id'] = $buyer_tenant_id;

            DB::table('tbl_property_seen')->insert($data);
            $data['property_seen_id']  =DB::getPdo()->lastInsertId();
            $getDate = DB::select("select created_date from tbl_property_seen where property_seen_id = ".DB::getPdo()->lastInsertId()."");
            $data['updated_date'] = $getDate[0]->created_date;
            $data['customer_id'] = $customer_id;
            if( isset($request->unit_information) && !empty($request->unit_information)){
                // $getData = DB::select("select building_name from op_society where society_id = ".$request->unit_information."");
                // $data['unit_information'] = $getData[0]->building_name;

                  $ex = explode("-", $request->unit_information);
                    $pr  = $ex[0];
                    $prn = DB::select("select project_complex_name from op_project where project_id=".$pr." ");
                    $fprn = $prn[0]->project_complex_name;
                    $fbun = '';
                    if(isset( $ex[1]) && !empty( $ex[1])){
                    $bu  = $ex[1];
                    $bun = DB::select("select building_name from op_society where society_id=".$bu." ");
                      $fbun = '-'. $bun[0]->building_name;
                    }

               // $getData = DB::select("select building_name from op_society where society_id = ".$request->unit_information."");
                $data['unit_information'] = $data['unit_information'] = $fprn.$fbun;
            }
            return response()->json($data);  
        }else{
            // update here
            $customer_id =  $request->customer_id;
            $buyer_tenant_id  = $request->buyer_tenant_id;
            $data['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_property_seen')->insert($data);
            $data['property_seen_id']  =DB::getPdo()->lastInsertId();
            $getDate = DB::select("select created_date from tbl_property_seen where property_seen_id = ".DB::getPdo()->lastInsertId()."");
            $data['updated_date'] = $getDate[0]->created_date;
            if(isset($request->unit_information) && !empty($request->unit_information)){

                 $ex = explode("-", $request->unit_information);
                    $pr  = $ex[0];
                    $prn = DB::select("select project_complex_name from op_project where project_id=".$pr." ");
                    $fprn = $prn[0]->project_complex_name;
                    $fbun = '';
                    if(isset( $ex[1]) && !empty( $ex[1])){
                    $bu  = $ex[1];
                    $bun = DB::select("select building_name from op_society where society_id=".$bu." ");
                      $fbun = '-'. $bun[0]->building_name;
                    }

               // $getData = DB::select("select building_name from op_society where society_id = ".$request->unit_information."");
                $data['unit_information'] = $data['unit_information'] = $fprn.$fbun;
            }
            return response()->json($data);  
            // $data['property_seen1'] = $request->property_seen1;
            // $data['unit_information'] = $request->unit_information;
            
        }     
    

    }

    public function getData1(Request $request)
    {
        $form_type = $request->form_type;
        if($form_type == 'property_seen'){
            $property_seen_id = $request->property_seen_id;
            $getData = DB::select("select * from tbl_property_seen where property_seen_id =".$property_seen_id."  ");

        }

        if($form_type == 'challenges'){
            $challanges_id = $request->challanges_id;
            $getData = DB::select("select * from tbl_challanges where challanges_id =".$challanges_id."  ");

        }

        return $getData;   
    }

    public function updateTableRecords(Request $request){

        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
   
        $user_id = $this->getUserId();

        $form_type = $request->form_type;
        if($form_type == 'property_seen'){
            $property_seen_id = $request->property_seen_id;
            $check = DB::select("select property_seen_id  from tbl_property_seen where property_seen_id = ".$property_seen_id." ");
            if($check){

                $data['looking_since'] = $request->looking_since;
                $data['property_seen'] = $request->property_seen1;
                $data['unit_information'] = $request->unit_information;
                $data['likes'] = $request->unit_information_like;
                $data['dislike'] = $request->unit_information_dislike;
                $data['property_visited_by'] = $request->visited_by;
                $data['date_of_visit'] = $request->date_of_visit;
                $data['physical_virtual'] = $request->physical_virtual;
                $data['no_of_days'] = $request->no_of_days;
                $data['user_id'] = $user_id;


                DB::table('tbl_property_seen')->where('property_seen_id',$property_seen_id)->update($data); 
                $getDate = DB::select("select created_date from tbl_property_seen where property_seen_id = ".$property_seen_id."");
                $data['updated_date'] = $getDate[0]->created_date;
                if(isset($request->unit_information) && !empty($request->unit_information)){

                    $ex = explode("-", $request->unit_information);
                    $pr  = $ex[0];
                    $prn = DB::select("select project_complex_name from op_project where project_id=".$pr." ");
                    $fprn = $prn[0]->project_complex_name;
                    $fbun = '';
                    if(isset( $ex[1]) && !empty( $ex[1])){
                    $bu  = $ex[1];
                    $bun = DB::select("select building_name from op_society where society_id=".$bu." ");
                      $fbun = '-'. $bun[0]->building_name;
                    }

                    // $getData = DB::select("select building_name from op_society where society_id = ".$request->unit_information."");

                    $data['unit_information'] = $fprn.$fbun;
                }
                
                return response()->json(['success'=>'Record updated successfully.','property_seen_id'=>$property_seen_id,'data'=>$data]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }

        if($form_type=='challenges'){
            $challanges_id = $request->challanges_id;
            $check = DB::select("select challanges_id  from tbl_challanges where challanges_id = ".$challanges_id." ");
            if($check){
                $data['challenges'] = $request->challenges;
                $data['solution'] = $request->solutions;
                $data['user_id'] = $user_id;
                $data['c_date'] = date('Y-m-d H:i:s');

                DB::table('tbl_challanges')->where('challanges_id',$challanges_id)->update($data); 
                $getDate1 = DB::select("select c_date from tbl_challanges where challanges_id = ".$challanges_id."");
                $data['updated_date'] = $getDate1[0]->c_date;
                
                
                return response()->json(['success'=>'Record updated successfully.','challanges_id'=>$challanges_id,'data'=>$data]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            } 
        }
    }

    public function deleteTableRecords(request $request)
    {
         // echo "<pre>"; print_r($request->all()); die();

         $form_type = $request->form_type;
         if($form_type == 'property_seen'){
             $property_seen_id = $request->property_seen_id;
             $check = DB::select("select property_seen_id  from tbl_property_seen where property_seen_id = ".$property_seen_id." ");
            if($check){
                $id = $check[0]->property_seen_id;
                DB::table('tbl_property_seen')->where('property_seen_id',$id)->delete(); 
                return response()->json(['success'=>'Record deleted successfully.','property_seen_id'=>$property_seen_id]);   
            }else{
                return response()->json(['error'=>'Record does not exist']);
            }
         }

         if($form_type == 'challenges'){
            $challanges_id = $request->challanges_id;
            $check = DB::select("select challanges_id  from tbl_challanges where challanges_id = ".$challanges_id." ");
           if($check){
               $id = $check[0]->challanges_id;
               DB::table('tbl_challanges')->where('challanges_id',$id)->delete(); 
               return response()->json(['success'=>'Record deleted successfully.','challanges_id'=>$challanges_id]);   
           }else{
               return response()->json(['error'=>'Record does not exist']);
           }
        }
    }

    public function challenges_and_solutions(Request $request)
    {
        // generete customer id and buyer_tenant id and set in hidden fields
        //    echo "<pre>"; print_r($request->all());// die();

        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
   
        $user_id = $this->getUserId();

        
        $data['challenges'] = $request->challenges;
        $data['solution'] = $request->solutions;
        $data['user_id'] = $user_id;
      

        $buyer_tenant_id = $request->buyer_tenant_id;
        if(empty($buyer_tenant_id)){
            // insert here
            $emptyData = array('created_date'=>date("Y-m-d"),'user_id'=>$user_id);
            DB::table('tbl_customer')->insert($emptyData);
            $customer_id =   DB::getPdo()->lastInsertId();
           // echo"<pre>"; print_r($customer_id); 
            $emptyData1 = array('customer_id'=>$customer_id,'type'=>'buyer','user_id'=>$user_id,'c_date'=>date('Y-m-d H:i'));
            DB::table('tbl_buyer_tenant')->insert($emptyData1);
            $buyer_tenant_id  =DB::getPdo()->lastInsertId();
            
            $specific_choice_data1['buyer_tenant_id'] = $buyer_tenant_id;
            $specific_choice_data1['user_id'] = $user_id;
            DB::table('tbl_specific_choice')->insert($specific_choice_data1);

            $key_member_data1['buyer_tenant_id'] = $buyer_tenant_id;
            $key_member_data1['user_id'] = $user_id;
            DB::table('tbl_key_members')->insert($key_member_data1);

            $leadData1['buyer_tenant_id'] = $buyer_tenant_id;
            $leadData1['user_id'] = $user_id;
            DB::table('tbl_lead')->insert($leadData1);

            $data['buyer_tenant_id'] = $buyer_tenant_id;

            DB::table('tbl_challanges')->insert($data);
            $data['challanges_id']  =DB::getPdo()->lastInsertId();
            $data['customer_id'] = $customer_id;
            return response()->json($data);  
        }else{
            // update here
            $customer_id =  $request->customer_id;
            $buyer_tenant_id  = $request->buyer_tenant_id;
            $data['buyer_tenant_id'] = $buyer_tenant_id;
            $data['c_date'] = date('Y-m-d H:i:s');
            DB::table('tbl_challanges')->insert($data);
            $data['challanges_id']  =DB::getPdo()->lastInsertId();
            $getDate = DB::select("select c_date from tbl_challanges where challanges_id = ".DB::getPdo()->lastInsertId()."");
            $data['updated_date'] = $getDate[0]->c_date;
            return response()->json($data);  
            // $data['property_seen1'] = $request->property_seen1;
            // $data['unit_information'] = $request->unit_information;
            
        }
      
    

    }

    function addPcCompanyName(Request $request){
        // echo "<pre>"; print_r($request->all()); die();

        $validator = Validator::make($request->all(), [
            'add_pc_company_name' => 'required',           
        ],
        [
            'add_pc_company_name.required' => 'Company name is required',           
        ]);        

        if ($validator->passes()) {

            $data['company_name'] = $request->add_pc_company_name;
            $data['sub_location'] = $request->add_pc_sub_location_m1;
            $data['location'] = $request->add_pc_location_m1;
            $data['owner_name'] = $request->add_pc_owner_name;
            $data['owner_name_mobile_mo'] = $request->add_pc_mobile_no;
            $data['owner_name_email'] = $request->add_pc_owner_email;
            $data['excecutive_name'] = $request->add_pc_excecutive_name;
            $data['excecutive_mobile_no'] = $request->add_pc_excecutive_mobile_no;
            $data['excecutive_email'] = $request->add_pc_excecutive_email;
            DB::table('dd_property_consultant')->insert($data); 
            $lastInsertId = DB::getPdo()->lastInsertId();  

            if(isset($request->add_pc_location_m1)){
                $getSubLocation  = DB::select("select location_name from dd_location where location_id= ".$request->add_pc_location_m1." ");
                $location_name = $getSubLocation[0]->location_name;
            }else{
                $location_name = "";
            }
            $st = $request->add_pc_company_name.",".ucwords($location_name).",".$request->add_pc_owner_name.",".$request->add_pc_mobile_no;
            $a = array('value'=>$lastInsertId,'option'=>$st);

            // return response()->json($data);           

            return response()->json(['success'=>'Added new records.','a'=>$a]);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }



    public function findlocationWithsublocationID(Request $request)    {
        
        $SublocationID = $request->SublocationID;
        $getSubLocation = DB::select("select * from dd_sub_location where sub_location_id =".$SublocationID."  ");
        $getLocation = DB::select("select * from dd_location where location_id =".$getSubLocation[0]->location_id."  ");
        return response()->json($getLocation);
    }

    public function getMaxCarpetArea(Request $request)
    {
        $minCarpetArea = $request->minCarpetArea;
        // echo "<pre>"; print_r($minCarpetArea);
        $getData = DB::select("select * from dd_maximum_carpet where maximum_carpet >=".$minCarpetArea." order by maximum_carpet asc ");
        // echo "<pre>"; print_r($getData);
        return response()->json($getData);
    }
    
  

    function addPcExcecutiveName(Request $request){

        $validator = Validator::make($request->all(), [            
            'add_exe_pc_employee_name' => 'required',
            'add_exe_pc_owner_mobile' => 'required',
            'add_exe_pc_whatsapp_no'=> 'required',
            'add_exe_pc_employee_email'=> 'required'
        ],
        [           
            'add_exe_pc_employee_name.required' => 'PC Employee name is required',
            'add_exe_pc_owner_mobile.required' => 'PC Owner mobile number is required',
            'add_exe_pc_whatsapp_no.required' => 'PC Whatsapp number is required',
            'add_exe_pc_employee_email.required' => 'PC Employee Email is required'
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new record.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addUnitInformation(Request $request){

        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'building' => 'required',
            'complex' => 'required',
            'add_sub_location' => 'required',
            'add_location' => 'required',
            'add_city' => 'required',
            'state_name' => 'required'            
        ],
        [
            'building.required' => 'Building name is required',
            'complex.required' => 'Complex name is required',
            'add_sub_location.required' => 'Sub loaction  is required',
            'add_location.required' => 'Location  is required',
            'add_city.required' => 'City is required',
            'state_name.required' => 'State is required'
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }


    function addUnitAmenity(Request $request){

        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'unit_amenity' => 'required'                    
        ],
        [
            'unit_amenity.required' => 'Amenity name is required',
            
        ]);        

        if ($validator->passes()) {

            $check = DB::select("select unit_amenities_id from dd_unit_amenities where amenities='".trim($request->unit_amenity)."' ");
            if(!empty($check)){
                return response()->json(['error'=>'0']);
            }else{
                $data['amenities'] = $request->unit_amenity;            
                DB::table('dd_unit_amenities')->insert($data);
                $unit_amenities_id =  DB::getPdo()->lastInsertId();
                // $data['unit_amenities_id']  =$unit_amenities_id
                $html ='<div id="ck1-button"><label><input type="checkbox" onClick="addUnitAmenity('.$unit_amenities_id.')" value="'.$unit_amenities_id.'" id="society_amnities1" ><span style="padding: 0px 10px 0px 10px;">'.$request->unit_amenity.'</span></label></div>';
                return response()->json(['success'=>'Added new records.','resp'=>$html]);
            }
        
			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addBuildingAmenity(Request $request){

        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'add_building_amenity' => 'required'                    
        ],
        [
            'add_building_amenity.required' => 'Amenity name is required',
            
        ]);        

        if ($validator->passes()) {
            $check = DB::select("select project_amenities_id from dd_project_amenities where amenities='".trim($request->add_building_amenity)."' ");
            if(!empty($check)){
                return response()->json(['error'=>'0']);
            }else{
                $data['amenities'] = $request->add_building_amenity;
                DB::table('dd_project_amenities')->insert($data);
                $project_amenities_id =  DB::getPdo()->lastInsertId();
                $html ='<div id="ck-button"><label><input type="checkbox" onClick="addBuildingAmenity('.$project_amenities_id.')" value="'.$project_amenities_id.'" id="society_amnities1" ><span style="padding: 0px 10px 0px 10px;">'.$request->add_building_amenity.'</span></label></div>';
                return response()->json(['success'=>'Added new records.','resp'=>$html]);	
            }
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addLocation(Request $request){
        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'add_state' => 'required',
            'add_city' => 'required',
            'add_location' => 'required',
            'add_sub_location' => 'required',           
        ],
        [
            'add_state.required' => 'State is required',
            'add_city.required' => 'City is required',
            'add_location.required' => 'Location is required',
            'add_sub_location.required' => 'Sub Location is required',
            
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new record.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }
  

    public function calculate_budget_break_up(Request $request)
    {
        // echo "<pre>"; print_r($request->all());
        $minimum_budget = $request->minimum_budget;
        $maximum_budget = $request->maximum_budget;
        $own_contribution_amount = $request->own_contribution_amount;
        $min = $minimum_budget;
        $max = $maximum_budget;
        $OCA = $own_contribution_amount;

        $TF = 13100;   //transfer fee
        $LF = 11000;   // legal charges

        $minPercent = ($min * 5 )/100;
        // echo "<pre>"; print_r($minPercent    );
        // if($minPercent <= $OCA){
        //     echo "valid";
        // }else{
        //     echo "Your OCA must be 5% of basic amount"; //exit();
        // }

        

        

        $loop = $max-$min  ;
        $strl = strlen($loop) - 2;
        $value = '1E'.($strl);
        $divide_and_multiply = (int)$value;
        // print_r($divide_and_multiply);
        // exit();


        $finalLoop = ($loop/$divide_and_multiply) + 1;



        // 
        $plus = array('000000');
        $val = array();

        for($i=0; $i <= $finalLoop ; $i++){
            array_push($val,$min + $plus[$i]);
            array_push($plus,$divide_and_multiply*$i);
        }

        $tableGeneratedFrom = array_unique($val);

        return view('requirement/budget_breakup_table',['tableGeneratedFrom'=>$tableGeneratedFrom,'OCA'=>$OCA,'TF'=>$TF,'LF'=>$LF,'minimum_budget'=>$minimum_budget,'maximum_budget'=>$maximum_budget,'own_contribution_amount'=>$own_contribution_amount]);

        // foreach($tableGeneratedFrom as $tblgen)
        // {
                
        //     $basic_amount = $tblgen; // sts  end 70l   interval 1lakh
        //     echo "<pre>"; echo "Basic Amount: "; print_r($basic_amount);
        //     $OCA =   $OCA; // own contribution amount
        //     echo "<pre>"; echo "OCA: "; print_r($OCA);
        // //     echo "<pre>"; echo "OCA in per "; print_r(($OCA*5)/100);

        //     $SDR = ($basic_amount * 4) /100;  //stamp duty and registration  4 %
        //     echo "<pre>"; echo "SDR: "; print_r($SDR);

        //     $TF = $TF;   //transfer fee
        //     echo "<pre>"; echo "TF: "; print_r($TF);
        //     $LF = $LF;   // legal charges
        //     echo "<pre>"; echo "LF: "; print_r($LF);
        //     $consultancyFee = ($basic_amount * 1) /100;  // 1%  brakerage fee
        //     echo "<pre>"; echo "consultancyFee: "; print_r($consultancyFee);
            
        //     if($OCA >     3000000){
        //         $registrationFee = 30000;
        //     }else{
        //         $registrationFee = $OCA / 100;
        //     }
        //     echo "<pre>"; echo "registrationFee: "; print_r($registrationFee);

        //     $otherChanges = $SDR + $TF + $LF + $consultancyFee + $registrationFee;
        //     echo "<pre>"; echo "otherChanges: "; print_r($otherChanges);

        //     $downPayment = $OCA - $otherChanges;
        //     echo "<pre>"; echo "downPayment: "; print_r($downPayment);

        // //    echo "<pre>"; echo "downPayment in per: "; print_r(($downPayment*5)/100);

        //     $loanAmount = $basic_amount - $downPayment;
        //     echo "<pre>"; echo "loanAmount: "; print_r($loanAmount);

        //     $packageAmount = ($loanAmount +  $downPayment + $otherChanges ) - $consultancyFee;
        //     echo "<pre>"; echo "packageAmount: "; print_r($packageAmount);
        
        //     $completePackage = $loanAmount +  $downPayment + $otherChanges;
        //     echo "<pre>"; echo "completePackage: "; print_r($completePackage);
        //     echo "<pre>";
        //     echo "-----------------------------";


        // }     

    }

    public function download_budget_break_up(Request $request)
    {
        $minimum_budget = $request->minimum_budget;
        $maximum_budget = $request->maximum_budget;
        $own_contribution_amount = $request->own_contribution_amount;
        $min = $minimum_budget;
        $max = $maximum_budget;
        $OCA = $own_contribution_amount;

        $TF = 13100;   //transfer fee
        $LF = 11000;   // legal charges

        $minPercent = ($min * 5 )/100;
        // echo "<pre>"; print_r($minPercent    );
        // if($minPercent <= $OCA){
        //     echo "valid";
        // }else{
        //     echo "Your OCA must be 5% of basic amount"; //exit();
        // }

        

        

        $loop = $max-$min  ;
        $strl = strlen($loop) - 2;
        $value = '1E'.($strl);
        $divide_and_multiply = (int)$value;
        // print_r($divide_and_multiply);
        // exit();


        $finalLoop = ($loop/$divide_and_multiply) + 1;



        // 
        $plus = array('000000');
        $val = array();

        for($i=0; $i <= $finalLoop ; $i++){
            array_push($val,$min + $plus[$i]);
            array_push($plus,$divide_and_multiply*$i);
        }

        $tableGeneratedFrom = array_unique($val);

        // print_r($data); exit();

            $pdf = PDF::loadView('requirement/download_budget_breakup',['data'=>$tableGeneratedFrom,'OCA'=>$OCA,'TF'=>$TF,'LF'=>$LF]);
      	    return $pdf->download('Budget-break-up.pdf');
    }



    function addResCity(Request $request){     

        $validator = Validator::make($request->all(), [
            'add_res_city' => 'required'                    
        ],
        [
            'add_res_city.required' => 'City is required',
            
        ]);        

        if ($validator->passes()) {
            $data['city_name'] = $request->add_res_city;
            $data['state_id'] = $request->add_res_state;
            DB::table('dd_city')->insert($data);
            $data['city_id']  =DB::getPdo()->lastInsertId();

            return response()->json(['success'=>'Added new records.','data'=>$data]);	
        }
        return response()->json(['error'=>$validator->errors()]);
    }


    function addCompanyCity(Request $request){     
        // echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all(), [
            'add_company_city' => 'required'                    
        ],
        [
            'add_company_city.required' => 'City is required',
            
        ]);        

        if ($validator->passes()) {

            $data['city_name'] = $request->add_company_city;
            $data['state_id'] = $request->add_company_state;
            DB::table('dd_city')->insert($data);
            $data['city_id']  =DB::getPdo()->lastInsertId();

            return response()->json(['success'=>'Added new records.','data'=>$data]);		
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addDesignation(Request $request){     
        // echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all(), [
            'add_designation' => 'required'                    
        ],
        [
            'add_designation.required' => 'Designation is required',
            
        ]);        

        if ($validator->passes()) {

             $data['designation_name']= $request->add_designation;

            DB::table('dd_designation')->insert($data);
            $data['designation_id']  =DB::getPdo()->lastInsertId();

            return response()->json(['success'=>'Added new records.','data'=>$data]);	
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addIndustry(Request $request){     
        // echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all(), [
            'add_industry_name' => 'required'                    
        ],
        [
            'add_industry_name.required' => 'Industry Name is required',
            
        ]);        

        if ($validator->passes()) {
            $data['industry']= $request->add_industry_name;
            $data['industry_type']= $request->add_industry_type;

            DB::table('dd_industry')->insert($data);
            $data['dd_industry_id']  =DB::getPdo()->lastInsertId();

            return response()->json(['success'=>'Added new records.','data'=>$data]);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    public function deleteBuyer(Request $request)
    {

        $buyer_tenant_id = $request->buyer_tenant_id;
        $data = array('status'=>0);
        DB::table('tbl_buyer_tenant')->where('buyer_tenant_id',$buyer_tenant_id)->update($data);     
        return redirect('buyer-list-view'); 

    }

     public function deleteTenant(Request $request)
    {

        $buyer_tenant_id = $request->buyer_tenant_id;
        $data = array('status'=>0);
        DB::table('tbl_buyer_tenant')->where('buyer_tenant_id',$buyer_tenant_id)->update($data);     
        return redirect('tenant-list-view'); 

    }

}
