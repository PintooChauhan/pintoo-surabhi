<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Traits\DatabaseTrait;

class WingMasterController extends Controller
{
    use DatabaseTrait;
    public function index(Request $request)
    {   
        
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = $this->adminRoles();
        if(!in_array($role,$roles)){
            Auth::logout();
            return redirect('login');
        }
        
        if(isset( $request->society_id) && !empty( $request->society_id)){
            $buildingData =   DB::select("select building_name,society_id,building_type_id from op_society  where society_id  = ".$request->society_id." "); 
            $Finalbuilding_name = $buildingData[0]->building_name;
            $Finalsociety_id = $buildingData[0]->society_id;
            $Finalbuilding_type_id = $buildingData[0]->building_type_id;
            $society_id = $request->society_id;           

            $wingData =   DB::select("select * from op_wing  where society_id  = ".$society_id." order by wing_id asc"); 
            
            if(!empty($Finalbuilding_type_id)){
                $decode = json_decode($Finalbuilding_type_id);
               
                if(!empty($decode[0])){
                    $ex = explode(",",$decode[0]);
                    $getElevation = DB::select("select * from dd_elevation where dd_elevation_id in (".implode(',',$ex).") ");
                }else{
                    $getElevation = array();
                }
               
            }else{
                $getElevation = array();
            }
           
        }else{            
            $Finalbuilding_name =  $Finalbuilding_type_id = '';
            $Finalsociety_id = '';
            $wingData = $getElevation = array();
        }
        
        return view('wing_master/wing_master',['Finalbuilding_name'=>$Finalbuilding_name,'Finalsociety_id'=>$Finalsociety_id,'wingData'=>$wingData,'society_id'=>$request->society_id,'getElevation'=>$getElevation]);
    }

    public function add_wing_details(Request $request)
    {      
            
        if(isset( $request->wing_id) && !empty( $request->wing_id)){
            $wing_id = $request->wing_id;
            $db = env('operationDB');
            $wingData =   DB::connection($db)->select("select wing_id,society_id,wing_name,total_floors,total_units,units_per_floor from op_wing  where wing_id  = ".$wing_id." "); 
            $society_id = $wingData[0]->society_id;
            $wing_id = $wingData[0]->wing_id;
            $wing_name = $wingData[0]->wing_name;
            $total_floors = $wingData[0]->total_floors;
            $total_units = $wingData[0]->total_units;
            $units_per_floor = $wingData[0]->units_per_floor;
            
        }else{
            $wing_id = $wing_name = $total_floors =  $total_units =  $units_per_floor ='';
        }   
        $unit = $this->getAllData('d','unit');
        $configuration = $this->getAllData('d','configuration'); 
        return view('wing_master/wing_master1',['unit'=>$unit,'configuration'=>$configuration, 'society_id'=>$society_id,'wing_id'=>$wing_id,'wing_name'=>$wing_name,'total_floors'=>$total_floors,'total_units'=>$total_units,'units_per_floor'=>$units_per_floor]);
    }

    public function add_wing_master(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'wing_name' => 'required',
            // 'total_floor' => 'required',
            'haitable_floor' => 'required',
            'lifts'=>'required',            
        ],
        [
            'wing_name.required' => 'Wing name is required',
            // 'total_floor.required' => 'Total floor is required',
            'haitable_floor.required' => 'Haitable floor is required' ,
            'lifts.required' => 'Lifts is required'        
        ]
        );

        $checkWing = DB::select("select wing_name from op_wing where wing_name='".$request->wing_name."' and society_id	=".$request->society_id." ");
        if(!empty($checkWing)){       
        $ary = array("Wing name is already exist");
        return response()->json(['error'=>$ary]);
        }
        if ($validator->passes()) {
            $data = array();    
            $society_id = $request->society_id;
            $wing_name = $request->wing_name;
            $total_floors = $request->total_floor;
            $habitable_floors = $request->haitable_floor;
            $units_per_floor = $request->unit_per_floor;            
            $total_units = $request->total_unit;
            $refugee_units = $request->refugee_unit;
            $no_of_lifts = $request->lifts;
            $comments = $request->comments;
            
            $array2 = array();
            if($request->hasfile('floor_plan'))
            {                
                foreach($request->file('floor_plan') as $file)
                {
                    $rand1 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('floor_plan'), $rand1);
                   array_push($array2,$rand1);                    
                }              
            } 

            $array1 = array();
            if($request->hasfile('name_board'))
            {                
                foreach($request->file('name_board') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('name_board'), $rand2);
                   array_push($array1,$rand2);                    
                }              

            }
           
            $lastWingIds = array();   
            $society_id1 = $society_id;                 
            $array = array('society_id'=>$society_id1,'wing_name'=>$wing_name,'total_floors'=>$total_floors,'habitable_floors'=>$habitable_floors,
            'units_per_floor'=>$units_per_floor,'total_units'=>$total_units,'refugee_units'=>$refugee_units,
            'no_of_lifts'=>$no_of_lifts,'comments'=>$comments,'floor_plan'=>json_encode($array2),'name_board'=>json_encode($array1),'user_id'=>$this->getUserId());
            DB::table('op_wing')->insert($array); 
           
            $last_id = DB::getPDO()->lastInsertId();
    
         
            $html = "<div class='row' id='wing_".$last_id."'>
            <input type='hidden' name='wing_id' value='".$last_id."' > 
            <div class='col l11'>
                <div class='input-field col l1 m4 s12' id='InputsWrapper2'>
                    <label  class='labelClass active'>Wing Name</label>
                    <input type='text' class='validate' name='wing_name' id='wing_name_".$last_id."' value='".$wing_name."'  placeholder='Enter' >                
                </div>

                <div class='input-field col l1 m4 s12' id='InputsWrapper2'>
                    <label  class='labelClass active'>Total Floor</label>
                    <input type='text' class='validate' name='total_floor' id='total_floor_".$last_id."'  value='".$total_floors."'   placeholder='Enter' onchange='calculateTotalUnits(".$last_id.")' >
                </div>

                <div class='input-field col l1 m4 s12' id='InputsWrapper2'>
                    <label  class='labelClass active'>Habitable From </label>
                    <input type='text' class='validate' name='haitable_floor' id='haitable_floor_".$last_id."'   value='".$habitable_floors."' placeholder='Enter' onchange='calculateTotalUnits(".$last_id.")' >
                </div>
               
                <div class='input-field col l1 m4 s12' id='InputsWrapper2'>
                    <label  class='labelClass active'>Refugee unit</label>
                    <input type='text' class='validate' name='refugee_unit' id='refugee_unit_".$last_id."'   value='".$refugee_units."'  placeholder='Enter' onchange='calculateTotalUnits(".$last_id.")'  >                            
                </div>
                <div class='input-field col l1 m4 s12' id='InputsWrapper2'>
                    <label  class='labelClass active'>Total Unit</label>
                    <input type='text' class='validate' name='total_unit' id='total_unit_".$last_id."' value='".$total_units."' readonly  placeholder='Enter' >
                </div>
                <div class='input-field col l1 m4 s12' id='InputsWrapper2'>
                    <label  class='active' > No. of Lifts </label>
                    <input type='text' class='validate' name='lifts' id='lifts_".$last_id."' value='".$no_of_lifts."'  placeholder='Enter' >                               
                </div>
                <div class='input-field col l1 m4 s12' id='InputsWrapper2'>
                    <label  class='labelClass active'>Comment </label>
                    <input type='text' class='validate' name='comments' id='comments_".$last_id."' value='".$comments."'   placeholder='Enter' >                    
                </div>
                <div class='input-field col l1 m4 s12' id='InputsWrapper2'>
                    <label  class='labelClass active'>Lobby Size</label>
                    <input type='text' class='validate' name='unit_per_floor' id='unit_per_floor_".$last_id."' value='".$units_per_floor."'   placeholder='Enter' onchange='calculateTotalUnits(".$last_id.")' >
                </div>

                <div class='input-field col l2 m4 s12' id='display_search'>
                    <label  class='active' style='font-size: 10px !important;'>Floor Plan </label>
                    <input type='file' name='floor_plan' id='floor_plan_".$last_id."' multiple='true'  style='padding-top: 14px;' onChange=uploadImgF(".$last_id.")>";
                   $ii=0; foreach($array2 as $value2){
                    $x2 = pathinfo($value2, PATHINFO_FILENAME);
                        $html .= "<span class='".$x2."'><a href='floor_plan/".$value2."'  target='_blank' > Floor Plan ".$ii++."</a>  &nbsp; <a href='javascript:void(0)' style='color: red;' onClick=delImageW('".$value2."#fp#".$last_id."') >X</a> &nbsp; , </span>";
                       
                    }
               $html .= "
                        <div class='preloader-wrapper small active' id='loaderFimg_".$last_id."' style='display:none'>
                                <div class='spinner-layer spinner-green-only'>
                                    <div class='circle-clipper left'>
                                        <div class='circle'></div>
                                    </div>
                                    <div class='gap-patch'>
                                        <div class='circle'></div>
                                    </div>
                                    <div class='circle-clipper right'>
                                        <div class='circle'></div>
                                    </div>
                                
                                </div>
                            </div>
               </div>
                <div class='input-field col l2 m4 s12' id='InputsWrapper2'>
                    <label  class='active' style='font-size: 10px !important;' >Name Board  </label>
                    <input type='file' name='name_board' id='name_board_".$last_id."' multiple='true' style='padding-top: 14px;' onChange=uploadImgN(".$last_id.")> ";                                      
                   $jj=0; foreach($array1 as $value1){
                    $x = pathinfo($value1, PATHINFO_FILENAME);
                        $html .= " <span class='".$x."'><a href='name_board/".$value1."'  target='_blank' > Name Board ".$jj++."</a> &nbsp; <a href='javascript:void(0)' style='color: red;' onClick=delImageW('".$value1."#nb#".$last_id."') >X</a> &nbsp; , </span>";
                        
                    }
                $html .="  <div class='preloader-wrapper small active' id='loaderNimg_".$last_id."' style='display:none'>
                                <div class='spinner-layer spinner-green-only'>
                                    <div class='circle-clipper left'>
                                        <div class='circle'></div>
                                    </div>
                                    <div class='gap-patch'>
                                        <div class='circle'></div>
                                    </div>
                                    <div class='circle-clipper right'>
                                        <div class='circle'></div>
                                    </div>
                                
                                </div>
                            </div>
                </div>
            </div>
                <div class='col l1'>
                <div class='row' >
                    <div class='input-field col l2 m2 s6 display_search'>
                        <div class='preloader-wrapper small active' id='loaderDel_".$last_id."' style='display:none'>
                            <div class='spinner-layer spinner-green-only'>
                                <div class='circle-clipper left'>
                                    <div class='circle'></div>
                                </div>
                                <div class='gap-patch'>
                                    <div class='circle'></div>
                                </div>
                                <div class='circle-clipper right'>
                                    <div class='circle'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
                        
                <div class='row' id='submitDel_".$last_id."'>
                    <div class='row'   >
                        <div class='col l3 m3'  >
                            <a href='javascript:void(0)' title='Copy' onClick='replicaWing(".$last_id.")'><i class='material-icons dp48'>add_to_photos</i></a>
                        </div> 
                        
                        <div class='col l3 m3'  >
                            <a href='javascript:void(0)' title='Delete' onClick='removeWing(".$last_id.")' style='color:red'><i class='material-icons dp48'>remove</i></a>
                        </div>

                        <div class='col l3 m3'>
                            <a href='javascript:void(0)' title='Setup Wing Structure' onClick='setupStructure(".$last_id.")'><i class='material-icons dp48'>refresh</i></a>
                        </div>
                    </div>
                    
                    <div class='row'>              
                        <div class='col l3 m3'>
                            <a href='javascript:void(0)' title='View Building Structure' onClick='unitView(".$last_id.")'><i class='material-icons dp48'>location_city</i></a>
                        </div>
                    </div>                                    
                </div>






                    
                </div>
            </div><br>";
            return $html;
        }  
        return response()->json(['error'=>$validator->errors()->all()]);
    }

   public function add_unit_info(Request $request)
   {  

       $validator = Validator::make($request->all(), [
        'rera_carpet_area' => 'required',
        'unit_type' => 'required',
        'parking' => 'required',
        'parking_info' => 'required',
        'configuration' => 'required',
        'no_of_bathrooms' => 'required',
        ],
        [
            'rera_carpet_area.required' => 'Rera carpet area is required',
            'unit_type.required' => 'Unit type is required',
            'parking.required' => 'Parking is required',
            'parking_info.required' => 'Parking info is required',
            'configuration.required' => 'Configuration is required',
            'no_of_bathrooms.required' => 'No of bathrooms is required'           
        ]);

        if ($validator->passes()) {

            $floor_no = $request->floor_no;        

            $wing_id = $request->wing_id;
            $society_id   = $request->society_id;
            $unit_available = $request->unit_available;
            $refugee_unit = $request->refugee_unit;     
            $property_no = $request->property_no;
            $floor_no = $request->floor_no;
            $rera_carpet_area = $request->rera_carpet_area;
            $mofa_carpet_area = $request->mofa_carpet_area;
            $door_facing_direction = $request->door_facing_direction;
            $configuration = $request->configuration;
            $unit_type = $request->unit_type;
            $build_up_area = $request->build_up_area;
            $no_of_bedrooms = $request->no_of_bedrooms;
            $no_of_bathrooms = $request->no_of_bathrooms;
            $hall_size = $request->hall_size;
            $kitchen_size = $request->kitchen_size;
            $parking = $request->parking;
            $parking_info = $request->parking_info;
            $unit_view = $request->unit_view;
            $comment = $request->comment;
          
           $data = array('refugeeflat'=>$refugee_unit,'reracarpet_area'=>$rera_carpet_area,'mofa_carpet_area'=>$mofa_carpet_area,'dfd'=>$door_facing_direction,'unit_type_id'=>0,
            'hallsize'=>$hall_size,'parking_id'=>$parking,'floor_no'=>$floor_no,'configuration_id'=>$configuration,'built_up_area'=>$build_up_area,
            'no_of_bathrooms'=>$no_of_bathrooms,'kitchen_size'=>$kitchen_size,'parking_info'=>$parking_info, 'unit_available'=>1,'unit_view'=>$unit_view
            ,'user_id'=>$this->getUserId());

            DB::table('op_unit')
              ->where('wing_id', $wing_id)
            //   ->where('society_id', $society_id)
              ->where('unit_code',$floor_no)
              ->update($data);

              exit();

            return response()->json(['success'=>'done']);

        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function add_unit_master_data(Request $request)
    {        

        $html = '<div id="modal4" class="modal" style="width:84% !important">
        <div class="modal-content">
            <h5 style="text-align: center"><b>UNIT INFORMATION</b></h5>
            <hr>
        
            <form method="post" id="add_unit_info_form">@csrf
                <input type="text" value="" name="society_id">
                <input type="text" value="" name="wing_id">
            <div class="row" style="margin-right: 0rem !important">

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default" id="unit_available"  data-placeholder="Select" name="unit_available">
                        <option value="" disabled selected>Select</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <label for="property_seen" class="active">Unit Available ?</label>
                </div>

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default"  id="refugee_unit" onChange="hide_all_info()"  data-placeholder="Select" name="refugee_unit">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <label for="property_seen1" class="active">Refugee Unit</label>
                </div>
            </div>
            <div id="not_refugee">
                <div class="row" style="margin-right: 0rem !important" >  
            
                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Property No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="property_no" id="property_no"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Floor No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="floor_no" id="floor_no"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                            

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="numeric" class="input_select_size" name="rera_carpet_area" id="rera_carpet_area" required placeholder="Enter" >
                                <label>RERA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="rera_carpet_area_unit" name="rera_carpet_area_unit" class="select2 browser-default ">
                               
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="numeric" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area" required placeholder="Enter"  >
                                <label>MOFA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="mofa_carpet_area_unit" name="mofa_carpet_area_unit" class="select2 browser-default ">
                                
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Door Facing Direction: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="door_facing_direction" id="door_facing_direction"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Configuration: <span class="red-text">*</span></label>
                
                    <select  name="configuration" class="validate select2 browser-default">
                                  
                                </select>
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                
                    <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default"  id="re"  data-placeholder="Select" name="unit_type">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Residencial</option>
                        <option value="no">Commercial</option>
                    </select>
                    <label for="property_seen" class="active">Unit Type</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Build Up Area: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="build_up_area" id="build_up_area"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">No.of Bedrooms with Sizes: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="no_of_bedrooms" id="no_of_bedrooms"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>


                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">No.of Bathrooms: <span class="red-text">*</span></label>
                    <input type="numeric" class="validate" name="no_of_bathrooms" id="no_of_bathrooms"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
            
                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Hall Size: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="hall_size" id="hall_size"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Kitchen Size: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="kitchen_size" id="kitchen_size"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Parking : <span class="red-text">*</span></label>
                    <input type="numeric" class="validate" name="parking" id="parking"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Parking Info: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="parking_info" id="parking_info"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
            

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">    Unit View: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="unit_view" id="unit_view"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="comment" id="comment"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                </div>             
            </div>           
            <div class="alert alert-danger print-error-msg_add_unit_info" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style="color:red"></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light"  type="submit" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            
            
                
            </div>
      
    </div> 
         ';
         return $html;
    }

    public function update_wing_master(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'wing_name' => 'required',
            'total_floor' => 'required',
            'haitable_floor' => 'required',
            'lifts'=>'required',
            
            ]
            ,
            [
                'wing_name.required' => 'Wing name is required',
                'total_floor.required' => 'Total floor is required',
                'haitable_floor.required' => 'Haitable floor is required' ,
                'lifts.required' => 'Lifts is required'         
            ]
        );

        if ($validator->passes()) {
            $data = array();    
            $wing_id = $request->wing_id;
            $society_id = $request->society_id;
            $wing_name = $request->wing_name;
            $total_floors = $request->total_floor;
            $habitable_floors = $request->haitable_floor;
            $units_per_floor = $request->unit_per_floor;            
            $total_units = $request->total_unit;
            $refugee_units = $request->refugee_unit;
            $no_of_lifts = $request->lifts;
            $comments = $request->comments;
            
            $array = array();
            if($request->hasfile('floor_plan'))
            {                
                foreach($request->file('floor_plan') as $file)
                {
                    $rand1 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('floor_plan'), $rand1);
                   array_push($array,$rand1);                    
                }
                
            } 

            $array1 = array();
            if($request->hasfile('name_board'))
            {                
                foreach($request->file('name_board') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('name_board'), $rand2);
                   array_push($array1,$rand2);                    
                }      

            }
          
            $lastWingIds = array();   
            $society_id1 = $society_id;
            $array = array('wing_name'=>$wing_name,'total_floors'=>$total_floors,'habitable_floors'=>$habitable_floors,
            'units_per_floor'=>$units_per_floor,'total_units'=>$total_units,'refugee_units'=>$refugee_units,
            'no_of_lifts'=>$no_of_lifts,'comments'=>$comments,'floor_plan'=>json_encode($array),'name_board'=>json_encode($array1),'user_id'=>$this->getUserId());    
            DB::table('op_wing')->where('wing_id',$wing_id)->update($array); 
        //     $last_id = $wing_id;           

        //     DB::table('op_unit')->where('wing_id',$wing_id)->delete(); //die();

        //     $total_floors1 = $total_floors;
        //     $units_per_floor1 = $units_per_floor; 
            
        //     for ($iii=0; $iii <= $units_per_floor1 ; $iii++) { 
        //         for ($i=1; $i < $total_floors1 +1  ; $i++) { 
        //            $start = 100;// - $habitable_floors;
                  
        //         //    if($habitable_floors <= 0){
        //         //       if($habitable_floors == 0) {
        //         //           //echo "<pre>"; echo " in zero";
        //         //           $s = 000;
        //         //       }elseif($habitable_floors == '-1'){
        //         //           // echo "<pre>"; echo " in -1";
        //         //           $s = 100;
        //         //       }elseif($habitable_floors == '-2'){
        //         //           // echo "<pre>"; echo " in -2";
        //         //           $s = 200;
        //         //       }elseif($habitable_floors == '-3'){
        //         //           $s = 300;
        //         //       }                             
        //         //       $un = $start * $i +$iii - $s ;// + $habitable_floors;                       
                      
        //         //    }else{                             
        //         //       $un = $start * $i +$iii ;// + $habitable_floors;
        //         //    }

        //         $un = $start * $i +$iii ;// + $habitable_floors;
        //         $arr = array('society_id'=>$society_id,'unit_code'=>$un,'wing_id'=>$last_id,'floor_no'=>$i,'user_id'=>$this->getUserId());                    
        //         DB::table('op_unit')->insert($arr);
        //       }
              
        //   }

        //     if($units_per_floor > $total_floors){                    
        //         if($total_floors == '0'){
        //             $a = 1;
                   
        //         }else{
        //             $a= 0;
                    
        //         }
        //         $t= $units_per_floor + $a;   
        //         // echo "<pre>"; print_r($t); 
        //         for($j=1; $j < $t ; $j++){
        //             $st = 100 * ($total_floors + $j);
        //             // echo "<pre>"; print_r($st); 
        //             $arr = array('society_id'=>$society_id,'unit_code'=>$st,'wing_id'=>$last_id,'floor_no'=>1,'user_id'=>$this->getUserId());
        //             // echo "<pre>"; print_r($arr); 
        //             DB::table('op_unit')->insert($arr);              
        //         }
        //         // echo 'done';
        //     }else{
        //         // echo 00; die();
        //         // $g =  $units_per_floor * 100;
        //        // $c  = $total_floors - $units_per_floor;   
        //     //    echo "<pre>"; print_r($units_per_floor);
        //     //    echo "<pre>"; print_r($total_floors); 
        //             for($l= $units_per_floor +1; $l <=$total_floors; $l++){
        //                 // echo "<pre>"; print_r($l*100);
        //                 DB::table('op_unit')
        //                 ->where('wing_id',$last_id)
        //                 ->where('unit_code',$l*100)
        //                 ->delete(); 
        //             }
                    
                    
                    
        
        //     }
        //     // echo 'out of if';
        //     for($k=1; $k <= $units_per_floor; $k++){
        //         $a = 100 * $k;
        //        // echo "<pre>"; echo $a;
        //         $array = array('floor_no'=>0,'user_id'=>$this->getUserId());
        //         DB::table('op_unit')
        //         ->where('unit_code',$a)
        //         ->where('wing_id',$last_id)
        //         ->update($array); 
        //     }
        //     // echo 11111;

            return response()->json(['success'=>'done','lastID'=>$society_id1]);

        }    
        return response()->json(['error'=>$validator->errors()->all()]);

    }

    public function replicaWing(Request $request)
    {
        $wingID = $request->wingID;
        $checkWing = DB::select("select * from op_wing where wing_id=".$wingID." ");
        if($checkWing){
            $data['wing_name'] = $checkWing[0]->wing_name;
            $data['total_floors'] = $checkWing[0]->total_floors;
            $data['habitable_floors'] = $checkWing[0]->habitable_floors;
            $data['units_per_floor'] = $checkWing[0]->units_per_floor;
            $data['refugee_units'] = $checkWing[0]->refugee_units;
            $data['total_units'] = $checkWing[0]->total_units;
            $data['no_of_lifts'] = $checkWing[0]->no_of_lifts;
            $data['comments'] = $checkWing[0]->comments;
            $data['user_id'] = $this->getUserId();       
        }else{
            return 0;
        }
        echo json_encode($data);
    }

    public function removeWing(Request $request)
    {
        // echo "<pre>"; print_r($request->all());
        $wingID = $request->wingID;
        $checkWing = DB::select("select wing_id from op_wing where wing_id=".$wingID." ");
        if(!empty($checkWing)){
            DB::table('op_unit')->where('wing_id',$wingID)->delete(); 
            sleep(2);
            DB::table('op_wing')->where('wing_id',$wingID)->delete(); 
            return  $wingID;
        }else{
            return 0;
        }

    }

    public function getUnitConfigDetails1(Request $request)
    {        
        $wingId = $request->wingId;
        $getData = DB::select("select 
        (select s.building_name from op_society as s where s.society_id=u.society_id ) as building_name,
        (select s.building_status from op_society as s where s.society_id=u.society_id ) as building_status,
        w.wing_name,w.habitable_floors,
        u.unit_code,u.floor_no, u.carpet_area,u.build_up_area,u.balcony,u.unit_view,u.unit_type,u.status,u.rera_carpet_area,u.mofa_carpet_area,u.custom_unit_code,u.show_unit,
        (select c.configuration_name from dd_configuration  as c where c.configuration_id=u.configuration_id) as configuration_name,
        (select cs.configuration_size from dd_configuration_size as cs where cs.configuration_size_id=u.configuration_size) as configuration_size,
        (select d.direction from dd_direction as d where d.direction_id=u.direction_id) as direction 
            from op_unit as u
        join op_wing as w on w.wing_id=u.wing_id
        where w.wing_id= ".$wingId." order by u.floor_no asc ");
    
        if(!empty($getData)){
        $html = '';
        $html .= 'Building Name : '.$getData[0]->building_name.'  &nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;  Wing Name : '.$getData[0]->wing_name.'
                <table class="bordered" >
                    <thead>
                    <tr style="color: white;background-color: #ffa500d4;">              
                        <th>Unit No</th>
                        <th>Configuration</th>
                        <th>Area</th>
                        <th>Configuration Size</th>
                        <th>Balcony</th>
                        <th>Direction</th>
                        <th>Unit View</th>
                        <th>Unit Type</th>';
                        if( $getData[0]->building_status == '2' ){
                            $html .= '<th>Show Unit</th>';
                        }

                       $html .= '<th>Unit Status</th>
                    </tr>
                </thead>
                 <tbody>';
                
                $habitable_floors = $getData[0]->habitable_floors;

        foreach ($getData as $key => $value) {
            if( $value->floor_no >= $habitable_floors){
                if($value->floor_no <=0 ){
                    $unit_code = $value->unit_code /100;
                        $unit_code = str_pad($unit_code,3, '0', STR_PAD_LEFT);;
                }else{
                    $unit_code  = $value->unit_code;
                }
            $html .= "<tr>
                <td>";
                if( !empty($value->custom_unit_code) ){
                    $html .= $value->custom_unit_code;
                }else{
                    $html .= $unit_code ;
                }

                
                $html .= "</td>
                <td>".$value->configuration_name."</td>
                <td>";
                 if( !empty($value->carpet_area) ){
                    $html .=  'Carpet Area :'. $value->carpet_area;
                        }
                        $html .=   "<br>";
                        if( !empty($value->build_up_area	) ){
                            $html .=  "Build Up Area :".  $value->build_up_area;
                        }

                        // if( !empty($value->rera_carpet_area) ){
                        //    $html .=  "Rera Carpet Area :". $value->rera_carpet_area;
                        // }
                             $html .=  "<br>";
                        if( !empty($value->mofa_carpet_area) ){
                           $html .=  "Rera Carpet Area : ". $value->mofa_carpet_area;
                        }

                $html .=    "</td>
                <td>".ucwords($value->configuration_size)."</td>
                <td>";
                        if($value->balcony=='t'){
                            $html .= "Yes";
                        }else{
                            $html .= "No";
                        }
                $html .= "</td>
                <td>". ucwords($value->direction) ."</td>
                <td>".$value->unit_view."</td>
                <td>". ucwords($value->unit_type)."</td>";
                    if( $getData[0]->building_status == '2' ){
                        $html .= "<td>";
                        if( $value->show_unit == 'y'){
                           $html .= 'Yes';
                        }else{
                            $html .= 'No';
                        }
                        $html .="</td>";
                    }
                $html .= "<td>".$value->status."</td>
            <tr>";
                }
        }

        $html .='</tbody>   
                </table>';

        echo  $html;        
        }else{
            echo  $html = "No data found";        
        }
    }


    public function delImageW(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die();
        $image = $request->image;
        $type = $request->type;
        $wing_id = $request->wing_id;

        if(  $type == 'fp' ){
            $getCPA = DB::select('select  floor_plan from op_wing where wing_id ='.$wing_id.' ');
            $floor_plan = json_decode($getCPA[0]->floor_plan);
            $final_array= array_diff($floor_plan, array($image));
            $enc = json_encode(array_values($final_array));
            // echo "<pre>"; print_r( $getCPA);
            DB::table('op_wing')->where('wing_id',$wing_id)->update(array('floor_plan'=>$enc,'user_id'=>$this->getUserId()));        
        }

        if(  $type == 'nb' ){
            $getCPA = DB::select('select  name_board from op_wing where wing_id ='.$wing_id.' ');
            $name_board = json_decode($getCPA[0]->name_board);
            $final_array= array_diff($name_board, array($image));
            $enc = json_encode(array_values($final_array));
            echo "<pre>"; print_r( $enc);
            DB::table('op_wing')->where('wing_id',$wing_id)->update(array('name_board'=>$enc,'user_id'=>$this->getUserId()));   
        }
    }
    
    public function uploadImg(Request $request)
    {

        $wing_id = $request->wing_id;
        $getData = DB::select("select floor_plan,name_board from op_wing where wing_id=".$wing_id." ");
        if($request->hasfile('floor_plan'))
            {                
                $array2 = array();
                if(isset($getData[0]->floor_plan) && !empty($getData[0]->floor_plan)){
                   $floor_plan =  json_decode($getData[0]->floor_plan);
                   foreach ($floor_plan as $key => $value) {
                    array_push($array2,$value);
                   }
                }           
               
                foreach($request->file('floor_plan') as $file)
                {
                    $rand1 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('floor_plan'), $rand1);
                   array_push($array2,$rand1);
                    
                }
                $data['floor_plan'] = json_encode($array2);
                $data['user_id'] = $this->getUserId();  
                DB::table('op_wing')->where('wing_id',$wing_id)->update($data); 

                
            } 

            if($request->hasfile('name_board'))
            {
                $array2 = array();
                if(isset($getData[0]->name_board) && !empty($getData[0]->name_board)){
                    $name_board =  json_decode($getData[0]->name_board);
                    foreach ($name_board as $key1 => $value1) {
                     array_push($array2,$value1);
                    }
                 }

                foreach($request->file('name_board') as $file)
                {
                    $rand1 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('name_board'), $rand1);
                   array_push($array2,$rand1);
                    
                }
                $data['name_board'] = json_encode($array2);
                $data['user_id'] = $this->getUserId();  
                DB::table('op_wing')->where('wing_id',$wing_id)->update($data); 
            } 
            $data['wing_id'] = $wing_id;
            return $data;
                
    }

    public function generate_structure(Request $request)
    {
        
        $wingId = $request->wingId;
        $getDataStructure = DB::select("SELECT * from op_wing_structure where wing_id = ".$wingId." ");

        // echo "<pre>"; print_r($getDataStructure); die();
        // 
        $getData = DB::select("SELECT wing_id,society_id,habitable_floors from op_wing where wing_id = ".$wingId." ");
        // $wing_id = $getData[0]->wing_id;
        $society_id = $getData[0]->society_id;
        $habitable_floors = $getData[0]->habitable_floors;
        return view('wing_master/generate_structure',['habitable_floors'=>$habitable_floors,'wingId'=>$wingId,'getDataStructure'=>$getDataStructure,'society_id'=>$society_id]);
    }

    public function storeStructure(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die();
        $wing_id = $request->wing_id;
        $society_id = $request->society_id;
        $floor_from = $request->floor_from;
        $floor_to   = $request->floor_to;
        $no_of_flat = $request->no_of_flat;
        $wing_structure_id = $request->wing_structure_id;
        $removeIds = explode(",",$request->removeIds); 
        $uni = array();
        

        foreach ( $floor_from as $key1 => $value1) {
            $data2['wing_id'] = $wing_id;
            $data2['floor_from'] = $value1;
            $data2['floor_to'] = $floor_to[$key1];
            $data2['no_of_flat'] = $no_of_flat[$key1];
            $data2['user_id'] = $this->getUserId();

            if(isset($wing_structure_id[$key1])){
                // echo 'in if';
                DB::table('op_wing_structure')->where('op_wing_structure_id',$wing_structure_id[$key1])->update($data2);
            
            }else{
                // echo 'in else';
                DB::table('op_wing_structure')->insert($data2);     
            }
            
        }

        if(isset($removeIds) && !empty($removeIds)){
            DB::table('op_wing_structure')->whereIn('op_wing_structure_id',$removeIds)->delete();  
        }
        // die();
        foreach ( $floor_from as $key => $value) {
            // echo  $key;
            for ($i=$value; $i <= $floor_to[$key] ; $i++) { 
                // if($i != '13'){
                for ($ii=1; $ii <=$no_of_flat[$key] ; $ii++) { 
                    if($i <= 0){
                        if($i >= 0){
                            // if($i != '13'){
                            $unit_code = 100 * $ii;
                            // }
                        }else{
                            // if($i != '13'){
                                $unit_code = -1 * (100 *  abs($i) +$ii) ;
                            // }
                        }
                       
                    }else{
                        // if($i != '13'){
                        $unit_code = 100 * $i + $ii;
                        // }
                    }
                 
                    array_push($uni,$unit_code);
                    $check = DB::select("select unit_id from op_unit where society_id=".$society_id." and unit_code =".$unit_code." and wing_id =".$wing_id." ");
                    if(!empty( $check)){
                       
                        $arr1 = array('floor_no'=>$i,'user_id'=>$this->getUserId());      
                        // echo "<pre>"; print_r($arr1);
                        DB::table('op_unit')->where('society_id',$society_id)->where('wing_id',$wing_id )->where('unit_code',$unit_code)->update($arr1);
                    }else{
                        // echo "<pre>"; print_r($unit_code);
                        $arr = array('society_id'=>$society_id,'unit_code'=>$unit_code,'wing_id'=>$wing_id ,'floor_no'=>$i,'user_id'=>$this->getUserId());                   
                        // echo "<pre>"; print_r($arr);
                        DB::table('op_unit')->insert($arr);  
                        
                        // array_push($ins,$last_id);  
                    }
                }
            // }
        }
        }
        // die();
        
       

        if(!empty($uni)){
            $getData = DB::select("select unit_id from op_unit where society_id= ".$society_id." and unit_code not in (".implode(',',$uni).")  ");
            if(!empty($getData)){
                foreach ($getData as $key => $value) {
                    DB::table('op_unit')->where('unit_id',$value->unit_id)->delete();  
                }
            }   
        }

        $getCntData = DB::select("select count(unit_id) as unitCount from op_unit where society_id= ".$society_id." and wing_id =".$wing_id."  ");
        $unitCount = $getCntData[0]->unitCount;
        $arr3 = array('total_units'=>$unitCount);
        DB::table('op_wing')->where('society_id',$society_id)->where('wing_id',$wing_id )->update($arr3);
        return redirect()->back();
        
    }
}
