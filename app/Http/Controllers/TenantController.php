<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;
use Illuminate\Support\Facades\Auth;


class TenantController extends Controller
{

    public function tenantListView()
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }

        /************* Role Check Start  *********************/
        $condition_name = 'tenant';
        $page_action = 's_view';
        $action_status = 1;
        $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

        $data['access_check'] = $access_check;

        /************* Role Check End  *********************/
        if(!empty($access_check)){

            $role = $this->getRole();      
            $user_id = $this->getUserId();
            $roles = $this->adminRoles();
            $data['role'] = $role;
            $data['roles'] = $roles;
        
            $j='';
            $j1 ='';
            if( !in_array($role,$roles) ){
                $j .= 'and lb.lead_assigned_id='.$user_id;
                $j1 .= " and ( bt.lead_stage not in(17,18) or bt.lead_stage is null )";
            }

            $data['allData'] = DB::select("select bt.f2f_done ,bt.c_date as c_date , bt.unit_type_id ,bt.buyer_tenant_id,bt.units,(select lead_by_name from dd_lead_by where lead_by_id = (select lead_by from tbl_lead where buyer_tenant_id = bt.buyer_tenant_id) ) as lead_by_name,(select customer_name from tbl_customer where id= bt.customer_id) as lead_source_name,(select lead_stage_name from dd_lead_stage where lead_stage_id = bt.lead_stage)  as lead_stage,
            (select primary_mobile_number from tbl_customer where id= bt.customer_id) as contact_no,
            bt.configuration_id,
            (select minimum_budget_in_short from dd_minimum_budget as minb where minb.minimum_budget = bt.min_budget) as min_budget,
        (select maximum_budget_in_short from dd_maximum_budget as maxb where maxb.maximum_budget= bt.max_budget) as max_budget
        ,bt.sublocation_id,bt.location_id,bt.min_carpet_area,bt.max_carpet_area,bt.unit_status,
            bt.possision_year,bt.finalization_month,bt.lead_intesity_id,bt.lead_intesity_id ,bt.lead_analysis,
            (select minimum_rent_in_words from dd_minimum_rent as minr where minr.minimum_rent = bt.min_rent_amount) as min_rent_amount,
            (select maximum_rent_in_words from dd_maximum_rent as maxr where maxr.maximum_rent = bt.max_rent_amount) as max_rent_amount,
            (select minimum_deposit_in_words from dd_minimum_deposit as mind where mind.minimum_deposit= bt.min_deposit_amount) as min_deposit_amount,
            (select maximum_deposit_in_words from dd_maximum_deposit as maxd where maxd.maximum_deposit= bt.max_deposit_amount) as max_deposit_amount,
            bt.shifting_date,bt.furnishing_status,
            (select customer_res_address from tbl_customer where id= bt.customer_id)  as customer_res_address,
            (select looking_since from dd_looking_since as look where look.looking_since_id= bt.looking_since) as looking_since,
            (select building_age from dd_building_age as bg where bg.dd_building_age_id= bt.building_age) as building_age,
            (select name from users where id=lb.lead_assigned_id) as lead_assigned_name,
            (select lead_source_name from dd_lead_source where lead_source_id= (select lead_source_name from tbl_lead where buyer_tenant_id = bt.buyer_tenant_id) ) as lead_source_name1
            from tbl_buyer_tenant as bt  join tbl_lead as lb on lb.buyer_tenant_id  = bt.buyer_tenant_id   where bt.type='tenant' and bt.status=1 ".$j." ".$j1."  order by bt.buyer_tenant_id desc");
            
            return view('list_view_tenant',$data);

        }else{
            return Redirect()->back()->with('error','You do not have access to this page.');
        }
    }

    public function index(Request $request)
    {
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }

        /************* Role Check Start  *********************/
        $condition_name = 'tenant';

        if(isset($request->buyer_tenant_id) && !empty($request->buyer_tenant_id)){
            $page_action = 's_edit';
        }else{
            $page_action = 's_add';
        }

        $action_status = 1;
        $access_check = AccessGroupHelper::globalAccessGroupVar($condition_name,$page_action,$action_status);

        $data['access_check'] = $access_check;

        /************* Role Check End  *********************/
        if(!empty($access_check)){

            $role = $this->getRole();
            $roles = $this->adminRoles();
        

            if(isset($request->buyer_tenant_id) && !empty($request->buyer_tenant_id)){
                // echo "<pre>"; print_r($request->buyer_tenant_id); die();
                $data['buyerId'] = array('buyer_tenant_id'=>$request->buyer_tenant_id);

                $getBuyerData = DB::select(" select * from tbl_buyer_tenant where buyer_tenant_id =".$request->buyer_tenant_id."");
                $data['getBuyerData'] = $getBuyerData;
                if(!empty($getBuyerData)){
                    $data['getCustomerData'] = DB::select(" select * from tbl_customer where id =".$getBuyerData[0]->customer_id."");
                }else{
                    $data['getCustomerData'] = array();
                }            

                $getmemberData = DB::select(" select * from tbl_key_members where buyer_tenant_id =".$request->buyer_tenant_id."");
                if(!empty($getmemberData)){
                    $data['getmemberData'] = $getmemberData;
                }else{
                    $data['getmemberData'] = array();
                }

                $getPropertySeenData = DB::select("select ps.* from tbl_property_seen as ps where ps.buyer_tenant_id  =".$request->buyer_tenant_id."");
                if(!empty($getPropertySeenData)){
                    $data['getPropertySeenData'] = $getPropertySeenData;
                }else{
                    $data['getPropertySeenData'] = array();
                }

                $getChallengesData = DB::select("select * from tbl_challanges where buyer_tenant_id =".$request->buyer_tenant_id."");
                if(!empty($getChallengesData)){
                    $data['getChallengesData'] = $getChallengesData;
                }else{
                    $data['getChallengesData'] = array();
                }

                $getSpecificChoiceData = DB::select("select * from tbl_specific_choice where buyer_tenant_id =".$request->buyer_tenant_id."");
                if(!empty($getSpecificChoiceData)){
                    $data['getSpecificChoiceData'] = $getSpecificChoiceData;
                }else{
                    $data['getSpecificChoiceData'] = array();
                }

                $getLeadSourceData = DB::select("select * from tbl_lead where buyer_tenant_id =".$request->buyer_tenant_id."");
                if(!empty($getLeadSourceData)){
                    $data['getLeadSourceData'] = $getLeadSourceData;
                }else{
                    $data['getLeadSourceData'] = array();
                }
                
            }else{
                $data['buyerId'] = array();
                $data['getBuyerData'] = array();
                $data['getCustomerData'] = array();
                $data['getmemberData'] = array();
                $data['getPropertySeenData'] = array();
                $data['getChallengesData'] = array();
                $data['getSpecificChoiceData'] = array();
                $data['getLeadSourceData'] = array();
            } 
            // echo "<pre>"; print_r( $data['getCustomerData']); die();
            return view('tenant',$data);

        }else{
            return Redirect()->back()->with('error','You do not have access to this page.');
        }

    }

    function getMaxRent(Request $request){
        $minumumRent = $request->minumumRent;
        // echo "<pre>"; print_r($minCarpetArea);
        $getData = DB::select("select * from dd_maximum_rent where maximum_rent >=".$minumumRent." order by maximum_rent asc ");
        // echo "<pre>"; print_r($getData);
        return response()->json($getData);
    }

    function getMaxDeposit(Request $request){
        $minumumDeposit = $request->minumumDeposit;
        // echo "<pre>"; print_r($minCarpetArea);
        $getData = DB::select("select * from dd_maximum_deposit where maximum_deposit >=".$minumumDeposit." order by maximum_deposit asc ");
        // echo "<pre>"; print_r($getData);
        return response()->json($getData);
    }

    public function saveTenat(Request $request)
    {

        // echo "<pre>"; print_r($request->all()); die();
        $user = Auth::user();         
        if(empty($user)){
            Auth::logout();
            return redirect('login');
        }        
        $role = $this->getRole();
        $roles = $this->adminRoles();
        // if(!in_array($role,$roles)){
        //     Auth::logout();
        //     return redirect('login');
        // }
        $user_id = $this->getUserId();
        $buyer_tenant_id = $request->buyer_tenant_id;
        $customer_id = $request->customer_id;
        // echo "<pre>"; print_r($customer_id);

         // below is  in table :     tbl_customer
         $customerData['intials'] = $request->cus_name_initial;
         $customerData['customer_name'] = ucwords($request->customer_name1);
         $customerData['primary_country_code_id'] = $request->mobile_conuntry_code;
         $customerData['primary_mobile_number'] = $request->mobile_number_primary;
         $customerData['primary_wa_countryc_code'] = $request->country_code_wa;
         $customerData['primary_wa_number'] = $request->whatsapp_number_primary;
         $customerData['primary_email'] = $request->primary_email;

         $customerData['secondary_country_code_id'] = $request->secondary_country_code_id;
         $customerData['seccondary_mobile_number'] = $request->seccondary_mobile_number;
         $customerData['secondary_wa_countryc_code'] = $request->secondary_wa_countryc_code;
         $customerData['secondary_wa_number'] = $request->secondary_wa_number;
         $customerData['secondary_email'] = $request->secondary_email;

         $customerData['customer_res_address'] = $request->current_residence_address;
         $customerData['customer_city'] = $request->current_residence_city;
         $customerData['customer_state'] = $request->current_residence_state;
         $customerData['customer_pin_code'] = $request->current_residence_pincode;
         $customerData['customer_company_name_address'] = $request->company_name;        
         $customerData['company_city'] = $request->company_city;
         $customerData['company_state'] = $request->company_state;
         $customerData['company_pincode'] = $request->company_pincode;
         $customerData['customer_designation_id'] = $request->designation;
         $customerData['industry']  = $request->industry_name;
         $customerData['information']  = $request->information;
         $customerData['rent_amount']  = $request->rent_amount;
         $customerData['user_id']  = $user_id;

        //  echo "<pre>"; print_r($customerData); die();
         if( isset($customer_id) && !empty($customer_id) ){
           // update tbl_customer
           DB::table('tbl_customer')->where('id',$customer_id)->update($customerData);      
         }else{
             DB::table('tbl_customer')->insert($customerData);
             $customer_id  =DB::getPdo()->lastInsertId();
         }
// die();
         
        // Unit tab
        $data['customer_id'] = $customer_id;
        $data['unit_type_id'] = $request->unit_type;//json encode
        
        if(isset($request->units) && !empty($request->units)) {
            $data['units'] = json_encode($request->units);// json_encode
        }

        if(isset($request->configuration) && !empty($request->configuration)) {
            $data['configuration_id'] = json_encode($request->configuration);// json_encode
        }




        $data['unit_status'] = $request->unit_status;
        $data['possision_year'] = $request->possession_year;
        $data['possision_year'] = $request->possession_year;
        $data['payment_plan_id'] = json_encode($request->payment_plan);

        if( isset($request->minimum_carpet_area) && !empty($request->minimum_carpet_area)  ){
            $data['min_carpet_area'] = $request->minimum_carpet_area;
        }
        //$data['minimum_carpet_area_unit']  = $request->minimum_carpet_area_unit;
        if( isset($request->maximum_carpet_area) && !empty($request->maximum_carpet_area)  ){
            $data['max_carpet_area'] = $request->maximum_carpet_area;
        }
        //$data['maximum_carpet_area_unit']  = $request->maximum_carpet_area_unit;
        $data['location_id'] = json_encode($request->location11);
        $data['sublocation_id'] = json_encode($request->sub_location1);
        //Budget Tab
        $data['min_budget'] = $request->minimum_budget;
        $data['max_budget'] = $request->maximum_budget;
        $data['own_contribution_amount'] = $request->own_contribution_amount;
        $data['oc_source_id'] =json_encode( $request->own_contribution_source);
        $data['oc_time_period'] = $request->oc_time_period;
        $data['Other_financial_details'] = $request->other_financial_details;

        //Purpose Tab
        $data['purpose_id'] = json_encode($request->purpose);//json encode

        //Home loan Tab
        $data['preferred_bank_id'] = json_encode($request->preferred_bank);//json_encode
        $data['loan_status_id'] = $request->loan_status;
        $data['Loan_stage_Id'] = $request->loan_stage;
        $data['home_loan_comment'] = $request->home_loan_comment; // Field not in table
        $data['need_assistance'] = $request->need_assistance;

        //Finalization Tab
        $data['finalization_month'] = json_encode($request->finalization_month);
        $data['reason_for_finalization'] = $request->reason_for_finalization;

        //Building & Unit Preference Tab
        $data['furnishing_status'] = $request->furnishing_status;
        $data['building_age'] = $request->building_age;
        $data['floor_choice'] = json_encode($request->floor_choice);
        $data['door_direction_id'] = json_encode($request->door_facing_direction);
        $data['bathroom_id'] = json_encode($request->bathroom);


        // Parking Tab
        $data['parking_type_id'] = json_encode($request->parking_type);// json_encode
        $data['no_of_two_wheeler_parking'] = $request->no_of_tw_parking;
        $data['no_of_four_wheeler_parking'] = $request->no_of_fw_parking; 

  

        //F2F & Address Tab
        $data['current_residential_status'] = $request->current_residence_status;
        //$data['unit_type_id']  = $request->rent_amount;

       

        //Family Members Tab
        $data['familly_size'] = $request->family_size;        
        $data['working_members'] = $request->working_members;
        $data['member_details'] = $request->members_details;

        //Nature of Business & Washroom Tab
        $data['nature_of_business_id'] = $request->nature_of_bussiness;
        $data['washroom_type_id'] = json_encode($request->washroom); // json_encode

        //Brokerage fees tab
        $data['brokerage_fees'] = $request->brokerage_fees;
        $data['brokerage_fees_comment']  = $request->brokerage_fees_comment; // no field in Db

        //Lead Analysis & Intensity Tab   
        $data['lead_stage']  = $request->lead_stage;    // no field in Db    
        $data['lead_analysis'] = $request->lead_analysis;
        $data['lead_intesity_id'] = $request->lead_intensity;
        $data['expected_closure_date'] = $request->expected_closure_date;
        $data['describe_in_method'] = $request->describe_in_5wh;
        $data['min_rent_amount'] = $request->minumum_rent;
        $data['max_rent_amount'] = $request->maximum_rent;
        $data['min_deposit_amount'] = $request->minimum_deposit;
        $data['max_deposit_amount'] = $request->maximum_deposit;
        $data['notice_served'] = $request->notice_served;
        $data['notice_period_date'] = $request->notice_period_date;
        $data['notice_period_reason'] = $request->notice_period_reason;
        $data['shifting_date'] = $request->shifting_date;
        $data['reason_of_shifting'] = $request->reason_of_shifting;

        $data['license_period'] = $request->license_period;
        $data['lock_in_period'] = $request->lock_in_period;
        $data['looking_since'] = $request->looking_since;
        $data['tenant_type'] = $request->tenant_type;

        $data['f2f_done'] = $request->f2f_done;
        $data['user_id']  = $user_id;

        if(isset($buyer_tenant_id) && !empty($buyer_tenant_id) ){
            // update tbl_customer
            DB::table('tbl_buyer_tenant')->where('buyer_tenant_id',$buyer_tenant_id)->update($data);      
          }else{
            //   die('prasanna');
            $data['type'] ='tenant';
            $data['c_date'] = date('Y-m-d H:i');
            DB::table('tbl_buyer_tenant')->insert($data);
            $buyer_tenant_id  =DB::getPdo()->lastInsertId();
            $specific_choice_data1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_specific_choice')->insert($specific_choice_data1);

            $key_member_data1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_key_members')->insert($key_member_data1);

            $leadData1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_lead')->insert($leadData1);
          }

          //Lead Source Tab
          $leadData['lead_by'] = $request->leadby;
          if(isset($request->property_consultant_info) && !empty($request->property_consultant_info)){
            $leadData['lead_by_pc_company'] = json_encode($request->property_consultant_info);
          }
          $leadData['lead_source_name'] = $request->lead_source_name;
          $leadData['campaign_id'] = $request->lead_campaign_name;
          $leadData['lead_reached_from'] = $request->lead_reached_from;
          $leadData['lead_assigned_id'] = $request->lead_assigned_name;
      //   echo "<pre>"; print_r($leadData);die();
          if(isset($buyer_tenant_id) && !empty($buyer_tenant_id) ){
              DB::table('tbl_lead')->where('buyer_tenant_id',$buyer_tenant_id)->update($leadData);      
          }else{
              $leadData['buyer_tenant_id'] = $buyer_tenant_id;
              DB::table('tbl_lead')->insert($leadData);
          }

           //Specific Choice Tab  new table :tbl_specific_choice
       
        $specific_choice_data['complex_name'] = json_encode($request->project_name);
        $specific_choice_data['building_name'] = json_encode($request->building_name);
        // $specific_choice_data['sub_location'] = json_encode($request->sub_location);
        // $specific_choice_data['location'] = json_encode($request->location);
        $specific_choice_data['building_amenities'] = json_encode($request->society_amnities1);
        $specific_choice_data['unit_amenities'] = json_encode($request->unit_amenities);
        $specific_choice_data['comment'] = $request->specific_choice_comment;

        if(isset($buyer_tenant_id) && !empty($buyer_tenant_id) ){
            DB::table('tbl_specific_choice')->where('buyer_tenant_id',$buyer_tenant_id)->update($specific_choice_data);      
        }else{
            $specific_choice_data['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_specific_choice')->insert($specific_choice_data);
        }

         //Key members Tab . Table : tbl_key_members
        
         $key_member_data['decision_initial'] = $request->decision_initial;
         $key_member_data['decision_maker_name'] = $request->decision_maker_name;
         $key_member_data['relation_with_buyer'] = json_encode($request->relation_with_buyer);
         $key_member_data['decision_in_5wh'] = $request->decision_in_5wh;
         $key_member_data['financer_initial'] = $request->financer_initial;
         $key_member_data['financer_name'] = $request->financer_name;
         $key_member_data['relation_with_buyer_f'] = json_encode($request->relation_with_buyer_f);
         $key_member_data['financer_in_5wh'] = $request->financer_in_5wh;
         $key_member_data['influencer_initial'] = $request->influencer_initial;
         $key_member_data['influencer_name'] = $request->influencer_name;
         $key_member_data['relation_with_buyer_i'] = json_encode($request->relation_with_buyer_i);
         $key_member_data['influencer_in_5wh'] = $request->influencer_in_5wh;
 
         if(isset($buyer_tenant_id) && !empty($buyer_tenant_id) ){
             // update tbl_customer
             DB::table('tbl_key_members')->where('buyer_tenant_id',$buyer_tenant_id)->update($key_member_data);      
         }else{
             $key_member_data['buyer_tenant_id'] = $buyer_tenant_id;
             DB::table('tbl_key_members')->insert($key_member_data);
 
         }
         
    }

    public function lookingSinceTenant(Request $request)
    {
        // generete customer id and buyer_tenant id and set in hidden fields
        //    echo "<pre>"; print_r($request->all()); die();
        
       
        $data['property_seen'] = $request->property_seen1;
        $data['unit_information'] = $request->unit_information;
        $data['likes'] = $request->unit_information_like;
        $data['dislike'] = $request->unit_information_dislike;
        $data['property_visited_by'] = $request->visited_by;
        $data['date_of_visit'] = $request->date_of_visit;
        $data['physical_virtual'] = $request->physical_virtual;
        $data['no_of_days'] = $request->no_of_days;

        $buyer_tenant_id = $request->buyer_tenant_id;
        if(empty($buyer_tenant_id)){
            // insert here
            $emptyData = array('created_date'=>date("Y-m-d"));
            DB::table('tbl_customer')->insert($emptyData);
            $customer_id =   DB::getPdo()->lastInsertId();
           // echo"<pre>"; print_r($customer_id);  
            $emptyData1 = array('customer_id'=>$customer_id,'type'=>'tenant','c_date'=>date('Y-m-d H:i'));
            DB::table('tbl_buyer_tenant')->insert($emptyData1);
            $buyer_tenant_id  =DB::getPdo()->lastInsertId();

            $specific_choice_data1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_specific_choice')->insert($specific_choice_data1);

            $key_member_data1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_key_members')->insert($key_member_data1);

            $leadData1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_lead')->insert($leadData1);

         //   echo"<pre>"; print_r($buyer_tenant_id);
            $data['buyer_tenant_id'] = $buyer_tenant_id;

            DB::table('tbl_property_seen')->insert($data);
            $data['property_seen_id']  =DB::getPdo()->lastInsertId();
            $getDate = DB::select("select created_date from tbl_property_seen where property_seen_id = ".DB::getPdo()->lastInsertId()."");
            $data['updated_date'] = $getDate[0]->created_date;
            $data['customer_id'] = $customer_id;
            if(!empty($request->unit_information)){
                // $getData = DB::select("select building_name from op_society where society_id = ".$request->unit_information."");
                // $data['unit_information'] = $getData[0]->building_name;

                 $ex = explode("-", $request->unit_information);
                    $pr  = $ex[0];
                    $prn = DB::select("select project_complex_name from op_project where project_id=".$pr." ");
                    $fprn = $prn[0]->project_complex_name;
                    $fbun = '';
                    if(isset( $ex[1]) && !empty( $ex[1])){
                    $bu  = $ex[1];
                    $bun = DB::select("select building_name from op_society where society_id=".$bu." ");
                      $fbun = '-'. $bun[0]->building_name;
                    }

                    // $getData = DB::select("select building_name from op_society where society_id = ".$request->unit_information."");

                    $data['unit_information'] = $fprn.$fbun;
            }
            return response()->json($data);  
        }else{
            // update here
            $customer_id =  $request->customer_id;
            $buyer_tenant_id  = $request->buyer_tenant_id;
            $data['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_property_seen')->insert($data);
            $data['property_seen_id']  =DB::getPdo()->lastInsertId();
            $getDate = DB::select("select created_date from tbl_property_seen where property_seen_id = ".DB::getPdo()->lastInsertId()."");
            $data['updated_date'] = $getDate[0]->created_date;
            if(!empty($request->unit_information)){
                // $getData = DB::select("select building_name from op_society where society_id = ".$request->unit_information."");
                // $data['unit_information'] = $getData[0]->building_name;

                 $ex = explode("-", $request->unit_information);
                    $pr  = $ex[0];
                    $prn = DB::select("select project_complex_name from op_project where project_id=".$pr." ");
                    $fprn = $prn[0]->project_complex_name;
                    $fbun = '';
                    if(isset( $ex[1]) && !empty( $ex[1])){
                    $bu  = $ex[1];
                    $bun = DB::select("select building_name from op_society where society_id=".$bu." ");
                      $fbun = '-'. $bun[0]->building_name;
                    }

                    // $getData = DB::select("select building_name from op_society where society_id = ".$request->unit_information."");

                    $data['unit_information'] = $fprn.$fbun;

            }
            return response()->json($data);  
            // $data['property_seen1'] = $request->property_seen1;
            // $data['unit_information'] = $request->unit_information;
            
        }     
    }

    public function challenges_and_solutions_tenant(Request $request)
    {
        // generete customer id and buyer_tenant id and set in hidden fields
        //    echo "<pre>"; print_r($request->all());// die();
        
        $data['challenges'] = $request->challenges;
        $data['solution'] = $request->solutions;
      

        $buyer_tenant_id = $request->buyer_tenant_id;
        if(empty($buyer_tenant_id)){
            // insert here
            $emptyData = array('created_date'=>date("Y-m-d"));
            DB::table('tbl_customer')->insert($emptyData);
            $customer_id =   DB::getPdo()->lastInsertId();
           // echo"<pre>"; print_r($customer_id); 
            $emptyData1 = array('customer_id'=>$customer_id,'type'=>'tenant','c_date'=>date('Y-m-d H:i'));
            DB::table('tbl_buyer_tenant')->insert($emptyData1);
            $buyer_tenant_id  =DB::getPdo()->lastInsertId();
            
            $specific_choice_data1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_specific_choice')->insert($specific_choice_data1);

            $key_member_data1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_key_members')->insert($key_member_data1);

            $leadData1['buyer_tenant_id'] = $buyer_tenant_id;
            DB::table('tbl_lead')->insert($leadData1);

            $data['buyer_tenant_id'] = $buyer_tenant_id;
            $data['c_date'] = date('Y-m-d H:i:s');
            DB::table('tbl_challanges')->insert($data);
            $data['challanges_id']  =DB::getPdo()->lastInsertId();
            $data['customer_id'] = $customer_id;
            return response()->json($data);  
        }else{
            // update here
            $customer_id =  $request->customer_id;
            $buyer_tenant_id  = $request->buyer_tenant_id;
            $data['buyer_tenant_id'] = $buyer_tenant_id;
            $data['c_date'] = date('Y-m-d H:i:s');
            DB::table('tbl_challanges')->insert($data);
            $data['challanges_id']  =DB::getPdo()->lastInsertId();
            $getDate = DB::select("select c_date from tbl_challanges where challanges_id = ".DB::getPdo()->lastInsertId()."");
            $data['updated_date'] = $getDate[0]->c_date;
            return response()->json($data);  
            // $data['property_seen1'] = $request->property_seen1;
            // $data['unit_information'] = $request->unit_information;
            
        }
      
    

    }


       
}
