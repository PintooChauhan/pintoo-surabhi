<?php

namespace App\Traits;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Auth;

trait DatabaseTrait {    
    
    public function getAllData($DBType,$table,$selectRaw=null,$where=null) {
        // 
        if($DBType=='d'){
            $db = env('operationDB');
            $table = 'dd_'.$table;   
        }elseif($DBType=='o'){
            $db = env('operationDB');
            $table = 'op_'.$table;
        }        
        if($selectRaw != null){
             $select = $selectRaw;
            
             $implode = implode(',',$select); 
        }else{
            $implode = '*';
        }
        $join='';
        if( $where != null){
            $join.="where ".$where;
            // $join.="and ".$where." !='' ";
            // return $join;
            
        }
        // return $join;

        $getData =   DB::connection($db)->select("select ".$implode." from ".$table." ".$join." ");

    //    DB::connection($db)->table($table)->selectRaw($implode)->get();
        
        return $getData;
    }

    public function getRole()
    {
        $user = Auth::user();       
        if(!empty($user)){
            return $user->role;
        }
    }

    public function getName()
    {
        $user = Auth::user();       
        if(!empty($user)){
            return $user->name;
        }
    }


    

    public function getUserId()
    {
        $user = Auth::user();       
        if(!empty($user)){
            return $user->id;
        }
    }

    public function adminRoles()
    {
        return array('developer','super_admin','admin','executive');
    }

 
}