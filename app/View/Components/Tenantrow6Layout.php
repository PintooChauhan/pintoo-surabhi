<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;

class Tenantrow6Layout extends Component
{
    use DatabaseTrait;
    public $getChallengesData;
    public $getBuyerData;
    public $getmemberData;
    public function __construct($getChallengesData,$getBuyerData,$getmemberData)
    {     
        $this->getChallengesData = $getChallengesData;
        $this->getBuyerData = $getBuyerData;
        $this->getmemberData = $getmemberData;
    }
    public function render()
    {      
        $data['lead_stage'] = $this->getAllData('d','lead_stage');
        $data['initials'] = $this->getAllData('d','initials');
        $data['relation'] = $this->getAllData('d','relation');
        $data['relationn'] = $this->getAllData('d','relation');
        return view('tenant.tenantrow6',$data);
        
    }
}
