<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;

class filter_buyer extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render()
    {
        $data['leadCampaignData'] = DB::SELECT('SELECT lead_campaign_id,lead_campaign_name FROM `dd_lead_campaign` ORDER BY lead_campaign_name ASC');
        $data['leadAssignedData'] = DB::SELECT("SELECT id ,name FROM `users` WHERE role<>'developer' AND status='1' AND del_status='1'  ORDER BY name ASC");
        $data['unitTypeData'] = DB::SELECT("SELECT unit_type_id ,unit_type FROM `dd_unit_type` ORDER BY unit_type ASC");
        $data['doorDirectionData'] = $this->getAllData('d','direction');
        $data['floorChoiceData'] = $this->getAllData('d','floor_choice');
        $data['purposeData'] = $this->getAllData('d','purpose');
        
        return view('filter.filter_buyer');
    }
}