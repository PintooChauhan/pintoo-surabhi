<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;

class Tenantrow5Layout extends Component
{
    use DatabaseTrait;
    public $getBuyerData;
    public function __construct($getBuyerData)
    {     
        $this->getBuyerData = $getBuyerData;
        
    }
    public function render()
    {     
         $data['washroom_type'] = $this->getAllData('d','washroom_type');

        return view('tenant.tenantrow5', $data);
        
    }
}
