<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;
class Tenantrow4Layout extends Component
{
    use DatabaseTrait;
    public $getSpecificChoiceData;
    public $getBuyerData;
    public $getCustomerData;
    public function __construct($getBuyerData,$getSpecificChoiceData,$getCustomerData)
    {     
        $this->getBuyerData = $getBuyerData;
        $this->getSpecificChoiceData = $getSpecificChoiceData;
        $this->getCustomerData = $getCustomerData;
    }
    public function render()
    {      
        $data['parking'] = DB::select("select * from dd_parking where parking_name !='no stack parking'");//$this->getAllData('d','parking');
        $data['licence_period'] = $this->getAllData('d','licence_period');  
        $data['lock_in_period'] = $this->getAllData('d','lock_in_period');
        $data['project'] = DB::select("select * from op_project where status='1' ");
        //$data['society'] = DB::select("select * from op_society where status='1' "); 
        $data['society'] = DB::select("select project_id, '' as society_id , '' as building_name,project_complex_name
        from op_project where  status=1   
        union 
        select op.project_id,os.society_id,os.building_name,op.project_complex_name
        from op_project as op
        join  op_society as os on os.project_id = op.project_id
        where op.status=1   and  os.status=1  
        order by project_complex_name  asc");
    
        return view('tenant.tenantrow4',$data);
        
    }
}
