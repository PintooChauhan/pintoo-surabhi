<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;

class Sellerrow2Layout extends Component
{
    use DatabaseTrait;
    public function render()
    {       
        $data['unit_status'] = DB::select("select * from dd_unit_status order by unit_status asc");
        $data['unit'] = DB::select("select * from dd_unit order by unit_name asc");
        $data['unit_type'] = DB::select("select * from dd_unit_type order by unit_type asc");
        $data['unit_type_residential'] = array();// $this->getAllData('view','unit_type_residential');
        $data['unit_type_commercial'] = array();// $this->getAllData('view','unit_type_commercial');
        $data['configuration'] = array();// $this->getAllData('view','configuration');
        $data['area_range'] = array();// $this->getAllData('view','area_range');
        $data['company'] = DB::select("select company_id,group_name from op_company where status='1' ");
        $data['complex'] = DB::select("select project_id,project_complex_name from op_project where status='1' ");
        $data['society1'] = DB::select("select society_id,building_name from op_society where status='1' and building_name !='' order by building_name  asc");
        $data['wing'] = DB::select("select wing_id,wing_name from op_wing where status='1' ");
        $data['sub_location'] = $this->getAllData('d','sub_location');
        $data['location'] = $this->getAllData('d','location');
        $data['city'] = $this->getAllData('d','city');
        $data['state'] = $this->getAllData('d','state');
        $data['final_price'] = $this->getAllData('d','final_price');
        $data['expected_price'] = $this->getAllData('d','expected_price');
        $data['loan_amount'] = $this->getAllData('d','loan_amount');
        $data['bank'] = $this->getAllData('d','bank');
        // echo "<pre>"; print_r($data); die();

        

        $data['society'] = DB::select("select oc.company_id ,oc.group_name	,op.project_id, '' as society_id , '' as building_name,op.project_complex_name 
        , (select sub_location_name from dd_sub_location where sub_location_id = op.sub_location_id) as sub_location_name
        , (select location_name from dd_location where location_id = op.location_id) as location_name
                from op_project as op 
                 join op_company as oc on oc.company_id = op.group_id
                  where  op.status=1   
                union 
                select op.group_id as company_id , oc.group_name ,op.project_id,os.society_id,os.building_name,op.project_complex_name
                , (select sub_location_name from dd_sub_location where sub_location_id = op.sub_location_id) as sub_location_name
                , (select location_name from dd_location where location_id = op.location_id) as location_name
                from op_project as op
                join  op_society as os on os.project_id = op.project_id
                 join op_company as oc on oc.company_id = op.group_id
                 where op.status=1   and  os.status=1  
                order by group_name ,project_complex_name  asc, building_name asc");

        return view('seller.sellerrow2',$data);
        
    }
}
