<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;
class Requirementr1Layout extends Component
{
    use DatabaseTrait;
   
    public $getBuyerData;
    public $getSpecificChoiceData;
    
    public function __construct($getBuyerData,$getSpecificChoiceData)
    {     
        $this->getBuyerData = $getBuyerData;
        $this->getSpecificChoiceData = $getSpecificChoiceData;
    }
    public function render()
    {      
        $data['unit'] = $this->getAllData('d','unit');       
        $data['unit_type_residential'] = $this->getAllData('d','unit_type_residential');
        $data['unit_type_commercial'] = $this->getAllData('d','unit_type_commercial');
        $data['configuration'] = $this->getAllData('d','configuration');
        $data['area_range'] = $this->getAllData('d','area_range');
        $data['possession_year'] = $this->getAllData('d','possession_year');
        $data['payment_plan'] = $this->getAllData('d','payment_plan');
        $data['minimum_carpet'] = $this->getAllData('d','minimum_carpet');
        $data['maximum_carpet'] = $this->getAllData('d','maximum_carpet');
        $data['minimum_budget'] = DB::select("select * from dd_minimum_budget order by minimum_budget asc");
        $data['maximum_budget'] = $this->getAllData('d','maximum_budget');
        $data['own_contribution'] = $this->getAllData('d','own_contribution');
        $data['own_contribution_source'] = $this->getAllData('d','own_contribution_source');
        $data['oc_time_period'] = $this->getAllData('d','oc_time_period');
        $data['purpose'] = $this->getAllData('d','purpose');
        $data['sub_location'] = $this->getAllData('d','sub_location');
        $data['location'] = DB::select("select * from dd_location order by seq asc");//$this->getAllData('d','location');
        $data['building_age'] = $this->getAllData('d','building_age');   
        $data['floor_choice'] = $this->getAllData('d','floor_choice');  
        $data['direction'] = $this->getAllData('d','direction'); 
        $data['bathroom'] = $this->getAllData('d','bathroom');
        $data['parking'] = $this->getAllData('d','parking');
        $data['bank'] = $this->getAllData('d','bank');  
       
        $data['loan_stage'] = $this->getAllData('d','loan_stage');   
        $data['balcony'] = $this->getAllData('d','balcony');   
        $data['balconyN'] = $this->getAllData('d','balcony');   
        
        $data['society'] = DB::select("select project_id, '' as society_id , '' as building_name,project_complex_name
        from op_project where  status=1   
        union 
        select op.project_id,os.society_id,os.building_name,op.project_complex_name
        from op_project as op
        join  op_society as os on os.project_id = op.project_id
        where op.status=1   and  os.status=1  
        order by project_complex_name  asc");
        // print_r($unit1); exit();
        return view('requirement.requirementr1',$data);
    }
}
