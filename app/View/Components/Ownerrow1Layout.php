<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;

class Ownerrow1Layout extends Component
{
    use DatabaseTrait;
    public function render()
    {
        $lead_by = array();// $this->getAllData('view','lead_by');
        $lead_type = array();// $this->getAllData('view','lead_type');
        $lead_source = array();// $this->getAllData('view','lead_source');
        $lead_campaign = array();// $this->getAllData('view','lead_campaign');
        
        return view('owner.ownerrow1',['lead_by'=>$lead_by,'lead_type'=>$lead_type,'lead_source'=>$lead_source,'lead_campaign'=>$lead_campaign]);
        // return view('seller.sellerrow1');
    }
}
