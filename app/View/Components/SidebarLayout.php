<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use App\Http\Controllers\AccessGroupHelper;

class SidebarLayout extends Component
{
    use DatabaseTrait;

    // public $access_check_buyer;
    // public $access_check_tenant;
    // public $access_check_property_consultant;
    // public $access_check_building_master;
    // public $access_check_users;
    // public $access_check_roles;
    
    // public function __construct($access_check_buyer,$access_check_tenant,$access_check_property_consultant,$access_check_building_master,$access_check_users,$access_check_roles)
    // {       
    //     $this->access_check_buyer = $access_check_buyer;
    //     $this->access_check_tenant = $access_check_tenant;
    //     $this->access_check_property_consultant = $access_check_property_consultant;
    //     $this->access_check_building_master = $access_check_building_master;
    //     $this->access_check_users = $access_check_users;
    //     $this->access_check_roles = $access_check_roles;
    // }

    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        $data['role'] = $this->getRole();
        $data['roles'] = $this->adminRoles();

        $page_action = 's_view';
        $action_status = 1;
        $condition_name_buyer = 'buyer';
        $condition_name_tenant = 'tenant';
        $condition_name_property_consultant = 'property_consultant';
        $condition_name_building_master = 'building_master';
        $condition_name_users = 'users';
        $condition_name_roles = 'roles';
        $access_check_buyer = AccessGroupHelper::globalAccessGroupVar($condition_name_buyer,$page_action,$action_status);
        $access_check_tenant = AccessGroupHelper::globalAccessGroupVar($condition_name_tenant,$page_action,$action_status);
        $access_check_property_consultant = AccessGroupHelper::globalAccessGroupVar($condition_name_property_consultant,$page_action,$action_status);
        $access_check_building_master = AccessGroupHelper::globalAccessGroupVar($condition_name_building_master,$page_action,$action_status);
        $access_check_users = AccessGroupHelper::globalAccessGroupVar($condition_name_users,$page_action,$action_status);
        $access_check_roles = AccessGroupHelper::globalAccessGroupVar($condition_name_roles,$page_action,$action_status);

        $data['access_check_buyer'] = $access_check_buyer;
        $data['access_check_tenant'] = $access_check_tenant;
        $data['access_check_property_consultant'] = $access_check_property_consultant;
        $data['access_check_building_master'] = $access_check_building_master;
        $data['access_check_users'] = $access_check_users;
        $data['access_check_roles'] = $access_check_roles;

        return view('layouts.sidebar',$data);
    }
}
