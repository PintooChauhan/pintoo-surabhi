<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;

class Tenantrow3Layout extends Component
{
    use DatabaseTrait;
    public $getSpecificChoiceData;
    public $getBuyerData;
    public $getCustomerData;
    public function __construct($getBuyerData,$getSpecificChoiceData,$getCustomerData)
    {     
        $this->getBuyerData = $getBuyerData;
        $this->getSpecificChoiceData = $getSpecificChoiceData;
        $this->getCustomerData = $getCustomerData;
    }
    public function render()
    {      
        $data['city'] = $this->getAllData('d','city');
        $data['state'] = $this->getAllData('d','state');
        $data['building_age'] = $this->getAllData('d','building_age');
        $data['designation'] = $this->getAllData('d','designation1');
        $data['industry'] = $this->getAllData('d','industry');
        $data['floor_choice'] = $this->getAllData('d','floor_choice');
        $data['direction'] = $this->getAllData('d','direction');
        $data['bathroom'] = $this->getAllData('d','bathroom');   
        $data['minimum_rent'] = $this->getAllData('d','minimum_rent');
        return view('tenant.tenantrow3',$data);
        
    }
}
