<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;

class Leadinfor1Layout extends Component
{
    use DatabaseTrait;
  
    public $getBuyerData;
    public $getCustomerData;
    public $buyerId;
    public $getLeadSourceData;
    
    public function __construct($getBuyerData,$getCustomerData,$buyerId,$getLeadSourceData)
    {       
        $this->getBuyerData = $getBuyerData;
        $this->getCustomerData = $getCustomerData;
        $this->buyerId = $buyerId;
        $this->getLeadSourceData = $getLeadSourceData;
    }

    public function render()
    {
        $role = $this->getRole();
        $user_id = $this->getUserId();        
        $data['lead_by'] = $this->getAllData('d','lead_by');
        $data['lead_type'] = $this->getAllData('d','lead_type');
        $data['lead_source'] = $this->getAllData('d','lead_source');
        $data['lead_campaign'] = $this->getAllData('d','lead_campaign');
        $data['property_consultant'] = DB::select("select pc.property_consultant_id,pc.company_name,pc.office_number,pc.about_owner,(select location_name from dd_location where location_id = pc.location) as location_name from tbl_property_consultant as pc where pc.company_name !=''"); //$this->getAllData('d','property_consultant');        
        $data['initials'] = $this->getAllData('d','initials');
        $data['country'] = $this->getAllData('d','country');
        $data['sub_location'] = $this->getAllData('d','sub_location');
        if($role == 'executive'){
            $data['users'] = DB::select("select * from users where id=".$user_id." ");
        }else{
            $data['users'] = DB::select("select * from users ");
        }
        $data['role'] = $role;
      
        
        return view('lead_info.leadinfor1',$data);
    }
}
