<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;
class Requirementr6Layout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    use DatabaseTrait;
    public $getBuyerData;
    public $getmemberData;
    public $getChallengesData;
    public function __construct($getBuyerData,$getmemberData,$getChallengesData)
    {     
        $this->getBuyerData = $getBuyerData;
        $this->getmemberData = $getmemberData;
        $this->getChallengesData = $getChallengesData;
    }
    public function render()
    {
        $data['lead_stage'] = DB::select("select * from dd_lead_stage order by seq asc"); //$this->getAllData('d','lead_stage');
        $data['initials'] = $this->getAllData('d','initials');
        $data['relation'] = $this->getAllData('d','relation');
        $data['relationn'] = $this->getAllData('d','relation');
        return view('requirement.requirementr6',$data);
    }
}
