<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;

class filter1 extends Component
{
    use DatabaseTrait;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data['role'] = $this->getRole();
        $data['leadCampaignData'] = DB::SELECT('SELECT lead_campaign_id,lead_campaign_name FROM `dd_lead_campaign` ORDER BY lead_campaign_name ASC');
        $data['leadAssignedData'] = DB::SELECT("SELECT id ,name FROM `users` WHERE role<>'developer' AND status='1' AND del_status='1'  ORDER BY name ASC");
        $data['unitTypeData'] = DB::SELECT("SELECT unit_type_id ,unit_type FROM `dd_unit_type` ORDER BY unit_type ASC");
        $data['doorDirectionData'] = $this->getAllData('d','direction');
        $data['floorChoiceData'] = $this->getAllData('d','floor_choice');
        $data['purposeData'] = $this->getAllData('d','purpose');
        $data['ownContributionData'] = $this->getAllData('d','own_contribution');
        $data['contributionSourceData'] = $this->getAllData('d','own_contribution_source');
        $data['ocTimePeriodData'] = $this->getAllData('d','oc_time_period');
        $data['looking_since'] = $this->getAllData('d','looking_since');
        $data['building_type'] = $this->getAllData('d','building_type');
        $data['parkingData'] = $this->getAllData('d','parking');
        $data['paymentPlanData'] = $this->getAllData('d','payment_plan');
        // return $data;
        return view('filter.filter1',$data);
    }
}
