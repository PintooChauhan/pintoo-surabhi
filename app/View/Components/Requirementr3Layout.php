<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;

class Requirementr3Layout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    use DatabaseTrait;
    public $getBuyerData;
    public $getSpecificChoiceData;
    public function __construct($getBuyerData,$getSpecificChoiceData)
    {     
        $this->getBuyerData = $getBuyerData;
        $this->getSpecificChoiceData = $getSpecificChoiceData;
    }
    public function render()
    {

        
        $data['washroom_type'] = $this->getAllData('d','washroom_type');  
        $data['sub_location'] = $this->getAllData('d','sub_location');
        $data['location'] = $this->getAllData('d','location');
       
        $data['project'] = DB::select("select * from op_project where status='1' ");
       // $data['society'] = DB::select("select * from op_society where status='1' ");
        

        

        // echo "<pre>"; print_r( $data['society_amnities']); die();
        return view('requirement.requirementr3',$data);
    }
}
