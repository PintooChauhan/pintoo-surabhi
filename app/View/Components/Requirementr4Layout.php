<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;


class Requirementr4Layout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    use DatabaseTrait;
    public $getBuyerData;
    public $getCustomerData;
    public function __construct($getBuyerData,$getCustomerData)
    {     
        $this->getBuyerData = $getBuyerData;
        $this->getCustomerData = $getCustomerData;
    }
    public function render()
    {
       
       
        $data['state'] = $this->getAllData('d','state');
       
      
        return view('requirement.requirementr4',$data);
    }
}
