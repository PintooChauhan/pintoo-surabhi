<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;
class Requirementr2Layout extends Component
{
    use DatabaseTrait;
    public $getBuyerData;
    public $getPropertySeenData;
    public $getCustomerData;
    public $getSpecificChoiceData;
    public function __construct($getBuyerData,$getPropertySeenData,$getCustomerData,$getSpecificChoiceData)
    {     
        $this->getBuyerData = $getBuyerData;
        $this->getPropertySeenData = $getPropertySeenData;
        $this->getCustomerData = $getCustomerData;
        $this->getSpecificChoiceData = $getSpecificChoiceData;
    }
    public function render()
    {
        
        $data['looking_since'] = $this->getAllData('d','looking_since');   
        //$data['society'] = DB::select("select * from op_society where status='1' ");
        $data['society'] = DB::select("select project_id, '' as society_id , '' as building_name,project_complex_name
        from op_project where  status=1   
        union 
        select op.project_id,os.society_id,os.building_name,op.project_complex_name
        from op_project as op
        join  op_society as os on os.project_id = op.project_id
        where op.status=1   and  os.status=1  
        order by project_complex_name  asc");
        $data['minimum_rent'] = $this->getAllData('d','minimum_rent');
        $data['city'] = $this->getAllData('d','city');
        $data['state'] = $this->getAllData('d','state');
        $data['designation'] = $this->getAllData('d','designation1');
        $data['industry'] = $this->getAllData('d','industry');
        $data['society_amnities'] = DB::select("select * from dd_project_amenities order by amenities asc");
        $data['unit_amenities'] = DB::select("select * from dd_unit_amenities order by amenities asc");
        return view('requirement.requirementr2',$data);
    }
}
