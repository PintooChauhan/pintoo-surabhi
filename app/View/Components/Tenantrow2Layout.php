<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
use DB;

class Tenantrow2Layout extends Component
{
    use DatabaseTrait;
    public $getBuyerData;
    public $getPropertySeenData;
    public $getSpecificChoiceData;
    public function __construct($getBuyerData,$getPropertySeenData,$getSpecificChoiceData)
    {     
        $this->getBuyerData = $getBuyerData;
        $this->getPropertySeenData = $getPropertySeenData;
        $this->getSpecificChoiceData = $getSpecificChoiceData;
    }
    public function render()
    {
        $data['unit'] =  $this->getAllData('d','unit');
        $data['unit1'] =  $this->getAllData('d','unit');
        $data['unit_type_residential'] =  $this->getAllData('d','unit_type_residential');
        $data['unit_type_commercial'] =  $this->getAllData('d','unit_type_commercial');
        $data['configuration'] =  $this->getAllData('d','configuration');
        $data['area_range'] =  $this->getAllData('d','area_range');
        $data['minimum_rent'] = $this->getAllData('d','minimum_rent');
        $data['maximum_rent'] = $this->getAllData('d','maximum_rent');
        $data['minimum_deposit'] = $this->getAllData('d','minimum_deposit');
        $data['maximum_deposit'] = $this->getAllData('d','maximum_deposit');
        $data['sub_location'] = $this->getAllData('d','sub_location');
        $data['location'] = DB::select("select * from dd_location order by seq asc");//$this->getAllData('d','location');
        $data['unit_amenities'] = DB::select("select * from dd_unit_amenities order by amenities asc");
        $data['looking_since'] = $this->getAllData('d','looking_since');   
       // $data['society'] = DB::select("select * from op_society where status='1' ");
       $data['society_amnities'] = DB::select("select * from dd_project_amenities order by amenities asc");
          $data['society'] = DB::select("select project_id, '' as society_id , '' as building_name,project_complex_name
        from op_project where  status=1   
        union 
        select op.project_id,os.society_id,os.building_name,op.project_complex_name
        from op_project as op
        join  op_society as os on os.project_id = op.project_id
        where op.status=1   and  os.status=1  
        order by project_complex_name  asc");

        return view('tenant.tenantrow2',$data);
        // return view('seller.sellerrow1');
    }
}
