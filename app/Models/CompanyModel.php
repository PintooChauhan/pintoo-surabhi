<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class CompanyModel extends Model
{
    use Sortable;
    public $sortableAs = ['group_name'];

}
