        <style>label{ color: #908a8a;      }</style>
      
            <div class="row">                                        
                    

                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="unit()">
                            <div class="collapsible-header" id="col_unit">Unit</div>
                            
                            </li>                                        
                        </ul>
                    </div>

                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="location1()">
                            <div class="collapsible-header" id='col_location1'>Location</div>
                            
                            </li>                                        
                        </ul>
                    </div>

                    
                    <div class="col l3 s12">
                            <ul class="collapsible">
                                <li onClick="budget_loan()">
                                <div class="collapsible-header" id='col_budget_loan'>Budget</div>
                                
                                </li>                                        
                            </ul>
                    </div>

                    
                

                    <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="purpose()">
                            <div class="collapsible-header" id="col_purpose">Purpose</div>
                            
                            </li>                                        
                        </ul>
                    </div>

                    
            </div>

                                                    
                                                

        <div class="row">        
            <div class="collapsible-body"  id='category' style="display:none">
                                                
                

                    




            </div>

            <div class="collapsible-body"  id='location1' style="display:none">
                                                
                <div class='row'>
                    <div class="input-field col l3 m4 s12 display_search">
                    
                        <select class="select2  browser-default" multiple="multiple" id="leadtype37" data-placeholder="Company name, Branch name" name="leadtype3">
                                <option value="option 1" selected>Thane</option>
                                <option value="option 1">Mulund</option>
                                <option value="option 1">Kalyan</option>
                            </select>
                            <label for="leadtype37  " class="active">City
                            </label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    
                        <select  id="state" name="lead_assign" class="validate select2 browser-default">
                        <option value="" disabled selected>Select</option>
                            <option value="1" selected>Maharashtra</option>
                            
                        </select>
                        <label class="active">State</label>
                    </div>
                        
                    <div class="input-field col l1 m4 s12 display_search">
                    
                        <div  class="addbtn" >
                                <a href="#modal3" id="add_location" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                        </div> 
                </div>
                </div>
                <br>
                <div class="row" >
                    <div class='col l4' >
                    <div><b> Locations</b> </div>
                    <div id="ck-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Location Thane</span>
                        </label>
                    </div>     
                    </div>
                    
                    <div class='col l8' >
                    <div><b>Sub Locations</b> </div>
                    <div id="ck-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Waghbill</span>
                        </label>
                    </div>
                    <div id="ck-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Anand Nagar</span>
                        </label>
                    </div>
                    </div>
                </div>
                <br>
                <div class="row" >
                    <div class='col l4' >
                    <div><b> Locations</b> </div>
                    <div id="ck-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Location Thane</span>
                        </label>
                    </div>     
                    </div>
                    
                    <div class='col l8' >
                    <div><b>Sub Locations</b> </div>
                    <div id="ck-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Waghbill</span>
                        </label>
                    </div>
                    <div id="ck-button">
                        <label>
                            <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Anand Nagar</span>
                        </label>
                    </div>
                    </div>
                </div>
                    

            </div>

            <div class="collapsible-body"  id='unit' style="display:none">
                                                
                    <div class="row" >
                        <div class="input-field col l3 m4 s12 ">
                                <!-- onChange="hideShowUnitType()" -->
                                <select class="select2  browser-default" multiple="multiple"  id="unit_type"   name="unit_type"  >
                               
                                <optgroup label="Residential">
                                    @foreach($unit_type_residential as $catType)     
                                <option value="r-{{$catType->unit_type_residential_id}}" @if( ($catType->unit_type_residential_name)=='flat') selected @endif >{{ ucwords($catType->unit_type_residential_name)  }}&nbsp;&nbsp;(Residential)</option>
                                @endforeach
                                </optgroup>
                                <optgroup label="Commercial">
                                    @foreach($unit_type_commercial as $catType)     
                                <option value="c-{{$catType->unit_type_commercial_id}}" >{{ ucwords($catType->unit_type_commercial_name) }}&nbsp;&nbsp;(Commercial)</option>
                                @endforeach
                                </optgroup>

                                
                                </select>
                                <label class="active">Unit Type</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select  id="under_constrction" name="under_constrction" onChange="action_to_hide()" class="validate select2 browser-default">
                            <option value="" disabled selected>Select</option>
                                <option value="resale">Resale</option>
                                <option value="under_construction">Under Construction</option>
                                <option value="rtmi">RTMI</option>                            
                            </select>
                            <label class="active">Unit Status</label>
                        </div>

                        <div class="input-field col l3 m4 s12 ">
                        
                            <select  id="configuration" name="configuration" placeholder="Select" class="select2  browser-default" multiple="multiple" >
                                @foreach($configuration as $config)
                                    <option value="{{$config->configuration_id}}">{{ ucwords($config->configuration_name)}}</option>
                                @endforeach
                            </select>
                            <label class="active">Configuration</label>
                        </div>

                     

                        <div class="input-field col l3 m4 s12 display_search" id='posession_yer'>
                        
                                <select class="select2  browser-default"  data-placeholder="Select" name="possession_year" id="possession_year">
                                    <option value="" disabled selected>Select</option>
                                    <option value="option 1">0 Year</option>
                                    <option value="option 1">1 Year</option>
                                </select>
                                <label for="possession_year" class="active">Possesion Year
                                </label>
                        </div> 

                        

                    </div>              

                    <div class="row" >
                        <!-- <div class="input-field col l3 m4 s12 display_search">
                        
                            <select  id="unit_category" name="unit_category" onChange="action_to_hide()" class="validate select2 browser-default">
                            <option value="" disabled selected>Select</option>
                                <option value="residental" checked>Residental</option>
                                <option value="commercial">Commercial</option>
                                
                            </select>
                            <label class="active">Category</label>
                        </div> -->

                        <div class="input-field col l3 m4 s12 display_search" id="payment_pln">
                    
                            <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="payment_plan" id="payment_plan">
                                <option value="option 1">CLP </option>
                                <option value="option 1">Sub vention</option>
                                <!-- <option value="option 1">Investment</option> -->
                            </select>
                            <label for="purpose" class="active">Payment Plan
                            </label>
                        </div> 
                    <!-- Start work from here Prasanna -->
                 		
                        <div class="col m3 l3 s12">
                            <div class="row">
                                <div class="input-field col l8 m6 s8" style="padding: 0 10px;">
                                    <select class="select2 browser-default">
                                        <option>Select Area</option>
                                        <option>200</option>
                                        <option>300</option>
                                        <option>400</option>
                                    </select>
                                    <label for="minimum_carper_area" class="active">Minimum carpet area                              </label>   
                                </div>
                                <div class="input-field col l4 m6 s4 mobile_view" style="padding: 0 10px;">
                                    <select class="select2 browser-default" >
                                    <option value=""> sq.ft</option>
                                    <option value="1">sq.mt</option>
                                    <option value="2">acres</option>
                                </select>
                                </div>
                            </div>
                        </div>

                        <!-- End work from here Prasanna -->
                        <div class="col m3 l3 s12">
                                <div class="row">
                                    <div class="input-field col l8 m6 s8" style="padding: 0 10px;">
                                        <select class="select2 browser-default">
                                            <option>Select Area</option>
                                            <option>200</option>
                                            <option>300</option>
                                            <option>400</option>
                                        </select>
                                        <label for="maximum_carper_area" class="active">Maxiimum carpet area</label>   
                                    </div>
                                    <div class="input-field col l4 m6 s4 mobile_view" style="padding: 0 10px;">
                                        <select class="select2 browser-default" >
                                        <option value=""> sq.ft</option>
                                        <option value="1">sq.mt</option>
                                        <option value="2">acres</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                    
                    </div>
            </div>
        

            <div class="collapsible-body"  id='possession_year' style="display:none">
                
                <div class='row'>
                    
                            
                </div>
            </div>

            <div class="collapsible-body"  id='budget_loan' style="display:none">
                    <div class="row">
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <div  class="sub_val no-search">
                                    <select  id="minimum_budget_amt" name="minimum_budget_amt" onChange="convert_min_budget()"  class="select2 browser-default">
                                        <option value="" disabled selected>Select</option>
                                        <option value="1000000">₹ 10L</option>
                                        <option value="2000000">₹20L</option>
                                        <option value="3000000">₹30L</option>
                                        
                                    </select>
                                </div>    
                                <label for="wap_number" class="dopr_down_holder_label active">Minimum Budget: <span class="red-text">*</span></label>
                                <div class="rupeeSign">    
                                    <input  type="text" class="validate mobile_number" name="minimum_budget" id="minimum_budget" required placeholder="Enter">
                                </div>
                                <span id="minimum_budget_amt_words"><br></span>                                
                                
                            </div>
                            <script>
    
    // alert(inWords(res));
    // $('#brokerage_fees').val(document.getElementById('minimum_budget').value);
};
                            </script>

                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <div  class="sub_val no-search">
                                    <select  id="maximum_budget_amt" name="maximum_budget_amt" onChange="convert_max_budget()"  class="select2 browser-default">
                                        <option value="" disabled selected>Select</option>
                                        <option value="1000000">₹ 10L</option>
                                        <option value="2000000">₹ 20L</option>
                                        <option value="3000000">₹ 30L</option>
                                        
                                    </select>
                                </div>    
                                <label for="wap_number" class="dopr_down_holder_label active">Maximum Budget: <span class="red-text">*</span></label>
                                <div class="rupeeSign">
                                    <input  type="text" class="validate mobile_number" name="maximum_budget" id="maximum_budget" required placeholder="Enter" >
                                </div>
                                <span id="maximum_budget_words"></span>                                                                               
                                
                            </div>

                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <div  class="sub_val no-search">
                                    <select   id="own_contribution_amt" name="own_contribution_amt" onChange="convert_own_contribution_amt()"   class="select2 browser-default">
                                        <option value=""  selected>Select</option>
                                        <option value="1000000">₹ 10L</option>
                                        <option value="2000000">₹20L</option>
                                        <option value="3000000">₹30L</option>
                                        
                                    </select>
                                </div>    
                                <label for="wap_number" class="dopr_down_holder_label active">Own Contribution Amount: <span class="red-text">*</span></label>
                                <div class="rupeeSign">
                                <input  type="text" class="mobile_number"  name="own_contribution_amount" id="own_contribution_amount"  placeholder="Enter"  >
                                </div>
                                <span id="own_contribution_amount_words"></span>                  
                            </div>  

                            <div class="input-field col l3 m4 s12 display_search">
                        
                                        <select class="select2  browser-default" multiple="multiple" id="own_contri_source" data-placeholder="Select" name="leadtype3">
                                            <option value="option 1" disabled >select</option>
                                            <option value="option 1">Saving</option>
                                            <option value="option 1">Investment</option>
                                        </select>
                                        <label for="own_contri_source  " class="active">Own Contribution Source
                                        </label>
                                </div>
                        
                    </div>
                    

                    <div class="row">
                        

                            <div class="input-field col l3 m4 s12 display_search">
                                    
                                    <select  id="oc_time_period" name="oc_time_period" class="validate select2 browser-default">
                                    <option value="" disabled selected>Select</option>
                                        <option value="1" >1 month</option>                                    
                                        <option value="1" >2 month</option>
                                    </select>
                                    <label class="active">OC Time Period</label>
                            </div>

                            <div class=" input-field  col l3 m4 s12">
                                <label for="lead_assign" class="active">Other Financial Details: <span class="red-text">*</span></label>
                                <!-- <textarea name="minimum_loan_amount" style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="other_financial_details"></textarea> -->
                                <input type="text" class="validate" name="other _financial_details" placeholder="Enter" >
                                
                            </div>

                            <div class="input-field col l3 m4 s12 display_search">
                                <ul class="collapsible" style="margin-top: -2px">
                                    <li onClick="budget_range()">
                                    <div class="collapsible-header"  id="col_budget_range">Budget Break up </div>
                                    
                                    </li>                                        
                                </ul>
                            </div>

                            <div class="input-field col l3 m4 s12 display_search">
                                <ul class="collapsible" style="margin-top: -2px">
                                    <li onClick="matching_properties()">
                                    <div class="collapsible-header"  id="col_matching_properties" >Matching Properties</div>
                                    
                                    </li>                                        
                                </ul>
                            </div>

                           

                    </div>
                    
                    <br>
                    
                    <div id="append_budget_range_table"></div>
                    

                    <div class='row' id='matching_properties_table' style="display:none">
                        <div class="col l8 s8 m8">
                            <table class="bordered  striped centered" id="example"  style=" background-color: white;">
                            <thead style=" background-color: lightblue;">
                            <tr>
                            <th>Society Name</th>
                            <th>Configuration</th>
                            <th>Area</th>
                            <th>Price</th>
                            <th>Lead By Details</th>
                            
                            </tr>
                            </thead>
                            <tbody>
                        
                        
                            </tbody>
                        </table>
                    
                        </div>
                    </div>

                </div>

            </div>

            <div class="collapsible-body"  id='purpose' style="display:none">
                                                
                <div class='row'>
                    <div class="input-field col l3 m4 s12 display_search">
                    
                        <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="purpose">
                                <option value="option 1">Self Occupancy</option>
                                <option value="option 1">Upgrade</option>
                                <option value="option 1">Investment</option>
                            </select>
                            <label for="purpose" class="active">Purpose
                            </label>
                    </div>                
                </div>
            </div>

        
        </div>

                                            

     <!-- Modal Location Structure -->
    <div id="modal3" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Location </h5>
        <hr>
        
        <form method="post" id="add_location_form">
                <div class="row" style="margin-right: 0rem !important">
                    <div class="input-field col l3 m3 s12 display_search">
                        <label  class="active">State.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="state_name" placeholder="Enter"  >
                    </div>

                    
                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">City: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_city"   placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Location: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_location"  placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Sub Location: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_sub_location"   placeholder="Enter" >
                    </div>
                </div> 
                
                
                <div class="alert alert-danger print-error-msg_add_location" style="display:none">
                <ul style="color:red"></ul>
                </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="add_location_btn()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
                
                
            </div>
        </form>
    </div>

    