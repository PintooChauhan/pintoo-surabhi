
        <div class="row">                                        
                           
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="home_loan()" id="home_loan_li">
                        <div class="collapsible-header" id='col_home_loan'>Home loan</div>
                        
                        </li>                                        
                    </ul>
                </div>

                <div class="col l3 s12">
                        <ul class="collapsible">
                            <li onClick="finalization()">
                            <div class="collapsible-header" id='col_finalization'>Finalization</div>
                            
                            </li>                                        
                        </ul>
                </div>
                                

                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="looking_since()">
                        <div class="collapsible-header" id="col_looking_since">Looking Since & Property Seen</div>
                        
                        </li>                                        
                    </ul>
                </div>
                
                <div class="col l3 s12">
                    <ul class="collapsible">
                        <li onClick="building_age()" id="building_age_li">
                        <div class="collapsible-header" id='col_building_age'>  Building & Unit Preference</div>
                        
                        </li>                                        
                    </ul>
                </div>
                
                    
                
        </div>

        <!-- <div class="collapsible-body"  id='purpose' style="display:none">
                                            
            <div class='row'>
                <div class="input-field col l3 m4 s12 display_search">
                
                     <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="purpose">
                            <option value="option 1">Self Occupancy</option>
                            <option value="option 1">Upgrade</option>
                            <option value="option 1">Investment</option>
                        </select>
                        <label for="purpose" class="active">Purpose
                        </label>
                </div>                
            </div>
        </div> -->

        <div class="collapsible-body"  id='home_loan' style="display:none">
                                            
            <div class='row'>
            
                <div class="input-field col l3 m4 s12 display_search">
                
                        <select class="select2  browser-default" multiple="multiple"  data-placeholder="Search" name="preferred_bank" id="preferred_bank">
                            <option value="hdfc">HDFC</option>
                            <option value="icici">ICICI</option>
                            <option value="sbi">SBI</option>
                        </select>
                        <label for="purpose" class="active">Preferred Bank
                        </label>
                </div>
                
                <div class="input-field col l3 m4 s12 display_search">
                
                        <select class="select2  browser-default"  onChange="change_loan_status_window()"  data-placeholder="Select" name="loan_status" id="loan_status">
                            <option value="" disabled selected>Select</option>
                            <option value="sanctioned">Sanctioned</option>
                            <option value="in_process">In Process</option>
                            <option value="need_assiatance">Need Assistance</option>
                        </select>
                        <label for="purpose" class="active">Loan Status
                        </label>
                </div>

                <div class=" input-field  col l3 m4 s12">
                                <label for="loan_amt" id="loan_amt" class="active">Loan Amount: </label>                           
                                <input type="text" class="validate" name="fetch_maximum_loan_amount"  id='fetch_maximum_loan_amount' placeholder="Enter" >
                            <span id="fetch_maximum_loan_amount_words"></span>
                            
                    </div>

                    <div class="input-field col l3 m4 s12 display_search" id="bank_name_h">                            
                            <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="bank_name" id="bank_name">
                                <option value="hdfc">HDFC</option>
                                <option value="icici">ICICI</option>
                                <option value="sbi">SBI</option>
                            </select>
                            <label for="purpose" class="active"> Bank Name
                            </label>
                    </div>

            </div>    

            <div class='row' id='loan_status_sanctioned' style="display:none">

            <div class="input-field col l3 m4 s12 display_search"></div>    
              
                
                <div class="input-field col l3 m4 s12 display_search">
                
                        <select class="select2  browser-default"    data-placeholder="Select" name="loan_stage" id="loan_stage">
                            <option value="" disabled selected>Select</option>
                            <option value="option1">option1</option>
                            <option value="option2">option2</option>
                            <option value="option3">option3</option>
                        </select>
                        <label for="purpose" class="active">Loan Stage
                        </label>
                </div>

                <div class=" input-field  col l3 m4 s12">
                    <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                    <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" placeholder="Enter" name="home_loan_comment" rows='2' col='4'></textarea> -->
                    <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                            
                </div>
            </div>    

            <div class='row' id='loan_status_need_assist' style="display:none">
            <div class="input-field col l3 m4 s12 display_search"></div>
                <div class="input-field col l3 m4 s12">
                    <label for="lead_assign" class="active">Need Assistance: <span class="red-text">*</span></label>
                    <!-- <textarea name="need_assistance" style="border-color: #5b73e8; padding-top: 12px"  rows='2' col='4' placeholder="We would like to inform that we have tie up with major bank and also we have sister concern company in Home loan also"></textarea> -->
                    <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                            
                </div>
            </div>

        </div>

        
       

        
        <div class="collapsible-body"  id='finalization' style="display:none">
             <div class="row">
                      
                      <div class="input-field col l3 m4 s12 display_search">                
                          <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="finalization_month">          

                             @for($i=0; $i<=5;$i++)
                                 <option value="<?php echo date('M Y',strtotime('first day of +'.$i.' month')); ?>"><?php echo date('M Y',strtotime('first day of +'.$i.' month')); ?> </option> 
                             @endfor                     
                              
                          </select>
                          <label for="purpose" class="active"> Finalization Month  </label>
                      </div>
  
                     

                      <div class="input-field col l3 m4 s12" >  
                        <label for="lead_assign" class="active">Reason for Finalization: <span class="red-text">*</span></label>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="finalization_comment" rows='2' col='4'></textarea> -->
                        <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                    </div>
                     
            </div>
        </div>

        <div class="collapsible-body"  id='looking_since' style="display:none">
                                            
            <div class='row'>
                <div class="input-field col l3 m4 s12 display_search">
                
                     <select class="select2  browser-default"   data-placeholder="Select" name="looking_since">
                            <option value="" disabled selected>Select</option>
                            <option value="option 1">1 Month</option>
                            <option value="option 1">2 Month</option>
                        </select>
                        <label for="looking_since" class="active">Looking Since
                        </label>
                </div>
                
                <div class="input-field col l3 m4 s12 ">
                                <select class="select2  browser-default" onChange="property_seen_hide_show()" id="property_seen1"  data-placeholder="Select" name="property_seen">
                                    <option value="" disabled selected>Select</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                                <label for="property_seen" class="active">Property Seen
                                </label>
                </div>
                <!-- <div class="input-field col l1 m4 s12 display_search">
                    <div id="AddMoreFileIdlookingsince" class="addbtn" >                        
                        <a href="#" id="AddMoreFileBoxlookingsince" class="" style="color: grey !important">+</a> 
                    </div> 
                </div> -->
            </div>
           
            <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
			  <div class="clonable" data-ss="0">

                <div class='row'>      

                    <div class="input-field col l3 m4 s12 display_search" id="p2" style="display:none">
                        <label for="loan_amt" id="loan_amt" class="active">Unit Information: </label>                           
                        <input type="text" class="validate" name="no_of_tw_parking" Placeholder="Search"  id='no_of_tw_parking'  > 
                            
                            <!-- <div id="AddMoreFileId3" class="addbtn" >
                            <a href="#modal4" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                            </div>  -->
                    </div>  

                    
                    <div class="input-field col l3 m4 s12 display_search"  id="p3" style="display:none">
                    
                            <select class="select2  browser-default" onChange="property_seen_details_hide_show()"  data-placeholder="Select" name="visited_by" id="visited_by">
                                <option value="" disabled selected>Select</option>
                                <option value="resale_pc">Property Consultant</option>
                                <!-- <option value="resale_direct">Resale Direct</option>
                                <option value="uc_pc">UC- Property Consultant</option>
                                <option value="uc_direct">UC- Direct</option> -->
                            </select>
                            <label for="visited_by" class="active">Property Visited By
                            </label>
                    </div> 

                    
                    
                    <div class="input-field col l3 m4 s12" id="p4" style="display:none" >                    
                        <label for="wap_number" class="active">Date of Visit: <span class="red-text">*</span></label>
                        <input type="text" class="datepicker" name="date_of_visit"  required placeholder="Calender" style="border: 1px solid #c3bf20 !important;height: 41px;">
                        <span id="calender_span" style="display:none;color: white; border: solid 1px green; background-color: green;padding: 1px 10px 2px 10px;">No of days : 10</span>
                        <span id="tat_within" style="display:none;border-radius: 3px;color: white; border: solid 1px red; background-color: red; padding: 1px 15px 2px 10px;">No of days left : 10</span>
                        <span id="tat_after" style="display:none;border-radius: 3px;color: white; border: solid 1px green; background-color: green;padding: 1px 15px 2px 10px;">No of days left : 10</span>
                    </div>  

                    <div class="input-field col l2 m4 s12" id="p5" style="display:none" >  
                        <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="property_seen_comment" rows='2' col='4'></textarea> -->
                        <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                    </div> 

                    <div class="col l1"  id="p6" style="display:none">
                            <a href="#" id="hap" class="clonable-button-add" title="Add"   style="color: grey !important"> <i class="material-icons  dp48">add</i></a> 
                            <a href="#" class="clonable-button-close" title="Remove"  style="color: grey !important"> <i class="material-icons  dp48">remove</i></a> 
                    </div>
                    
                </div>
              </div>
            </div>

      

         </div>

         <div class="collapsible-body"  id='building_age' style="display:none">
            <div class="row">
                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"  data-placeholder="Select" name="building_age">
                            <option value="" disabled selected>Select</option>
                            <option value="option 1">1 – 5 </option>
                            <option value="option 1">5 - 10</option>
                        </select>
                        <label for="building_age" class="active">Building Age  </label>
                    </div>   
                    <div class="input-field col l3 m4 s12 display_search">                
                        <select class="select2  browser-default" multiple="multiple" id="building_type" data-placeholder="Select" name="building_type">
                            <option value="option 1">West </option>
                            <option value="option 1">North East</option>
                        </select>
                        <label for="building_type" class="active">Building Type</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"  data-placeholder="Select" name="floor_choice">
                            <option value="" disabled selected>Select</option>
                            <option value="option 1">10-15</option>
                            <option value="option 1">15-20</option>
                        </select>
                        <label for="floor_choice" class="active">Floor Choice    </label>
                    </div>   

                    <div class="input-field col l3 m4 s12 display_search">                
                        <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="door_facing_direction">
                            <option value="option 1">West </option>
                            <option value="option 1">North East</option>
                        </select>
                        <label for="purpose" class="active">Door Direction</label>
                    </div>
            </div>

        </div>

        

    <!-- Modal Unit Information Structure -->
    <div id="modal4" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Unit Information </h5>
        <hr>
        
        <form method="post" id="add_unit_information">
                <div class="row" style="margin-right: 0rem !important">
                    <div class="input-field col l3 m3 s12 display_search">
                        <label  class="active">New Building.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="building" placeholder="Enter"  >
                    </div>

                    <div class="input-field col l3 m3 s12 display_search">
                        <label  class="active">Complex.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="complex" placeholder="Enter"  >
                    </div>   

                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Sub Location: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_sub_location"   placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">Location: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_location"  placeholder="Enter" >
                    </div>
                </div> 
                <div class="row" style="margin-right: 0rem !important">
                    <div class="input-field col l3 m4 s12 display_search">
                        <label for="lead_assign" class="active">City: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="add_city"   placeholder="Enter" >
                    </div>
                    <div class="input-field col l3 m3 s12 display_search">
                        <label  class="active">State.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="state_name" placeholder="Enter"  >
                    </div>                
                </div>
                    
                
                
                
                <div class="alert alert-danger print-error-msg_add_unit_information" style="display:none">
                <ul style="color:red"></ul>
                </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="add_unit_information_btn()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
                
                
            </div>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.0.slim.min.js"  crossorigin="anonymous"></script>
      <script src="{{ asset('app-assets/js/jquery.cloner.js') }}"></script>