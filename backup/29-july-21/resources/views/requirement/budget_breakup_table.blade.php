<a href="/download_budget_break_up?minimum_budget={{$minimum_budget}}&maximum_budget={{$maximum_budget}}&own_contribution_amount={{$own_contribution_amount}}">Download in PDF</a>
            <div class='row' id='budget_range_table'>
                    <div class="col l8 s8 m8">
                    <table class="bordered  striped centered" id="example"  style=" background-color: white;">
                        <thead style=" background-color: lightblue;">
                        <tr>
                        <th>Basic</th>
                        <th>Own Contribution</th>
                        <th>SDR</th>
                        <th>Down Payment</th>
                        <th>DP %</th>
                        <th>Transfer Fees</th>
                        <th>Legal Fees</th>
                        <th>Loan Amount</th>
                        <th>Loan %</th>
                        <th>Package</th>
                        <th>Complete Package</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tableGeneratedFrom as $tbldata)
                        <?php 
                        $basic_amount = $tbldata;  
                        $SDR = ($basic_amount * 4) /100;  
                        $TF = $TF;
                        $LF = $LF;                        
                        $consultancyFee = ($basic_amount * 1) /100; 
                        if($OCA >     3000000){
                            $registrationFee = 30000;
                        }else{
                            $registrationFee = $OCA / 100;
                        }
                        $otherChanges = $SDR + $TF + $LF + $consultancyFee + $registrationFee;
                        $downPayment = $OCA - $otherChanges;
                        $loanAmount = $basic_amount - $downPayment;
                        $packageAmount = ($loanAmount +  $downPayment + $otherChanges ) - $consultancyFee;
                        $completePackage = $loanAmount +  $downPayment + $otherChanges;
                        
                        $contri= 100 / $basic_amount * $OCA;
                        // echo $contri;

                        if( ($basic_amount *5)/100 < $OCA ){
                            //   echo ($basic_amount *5)/100 ; 
                        }else{
                            // echo 'Invalid';
                        }


                        ?>

                        <tr>
                            <td>{{ $basic_amount }}</td>
                            <td>{{ $OCA }}</td>
                            <td>{{ $SDR }}</td>
                            <td>{{  $downPayment }}</td>
                            <td></td>
                            <td>{{ $TF }}</td>
                            <td>{{ $LF }}</td>
                            <td>{{ $loanAmount }}</td>
                            <td>{{ (100/$basic_amount) * $loanAmount  }}</td>
                            <td>{{ $packageAmount }}</td>
                            <td> {{ $completePackage }}</td>
                        </tr>
                        @endforeach
                        
                      
                        </tbody>
                    </table>
                   
                    </div>
        </div>