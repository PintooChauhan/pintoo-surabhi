
    <div class="row">                 
            
        <div class="col l3 s12"  >
                <ul class="collapsible" >
                    <li onClick="address()" >
                    <div class="collapsible-header" id='col_address'>F2F & Address</div>
                    
                    </li>                                        
                </ul>
        </div>

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="members()">
                <div class="collapsible-header" id='col_members'> Family Members</div>
                
                </li>                                        
            </ul>
        </div>

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="nature_of_business()" id="nature_of_business_li">
                <div class="collapsible-header" id="col_nature_of_business">Nature of Business & Washroom</div>
                
                </li>                                        
            </ul>
        </div>   

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="brokerage_fee()" id="brokerage_fee_li">
                <div class="collapsible-header" id='col_brokerage_fee'>  Brokerage fees</div>
                
                </li>                                        
            </ul>
        </div>
    </div> 
       

        <!-- <div class="collapsible-body"  id='floor_choice' style="display:none">
            <div class="row">
                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"  data-placeholder="Select" name="floor_choice">
                            <option value="" disabled selected>Select</option>
                            <option value="option 1">10-15</option>
                            <option value="option 1">15-20</option>
                        </select>
                        <label for="floor_choice" class="active">Floor Choice    </label>
                    </div>   
                    <div class="input-field col l3 m4 s12 display_search">                
                        <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="door_facing_direction">
                            <option value="option 1">West </option>
                            <option value="option 1">North East</option>
                        </select>
                        <label for="purpose" class="active">Door Facing Direction</label>
                    </div>
            </div>
        </div> -->

        <!-- <div class="collapsible-body"  id='parking' style="display:none">
            <div class='row'>
                <div class="input-field col l3 m4 s12 display_search">                    
                    <select class="select2  browser-default"  multiple="multiple"  data-placeholder="Select" name="parking_type">
                        <option value="" >Allotted  </option>
                        <option value="option 1">Basement</option>
                        <option value="option 1">Podium</option>
                    </select>
                    <label for="possession_year" class="active">Parking Type </label>
                </div>

                <div class=" input-field  col l3 m4 s12">
                            <label for="loan_amt" id="loan_amt" class="active">No of FW Parking: </label>                           
                            <input type="number" class="validate" name="no_of_fw_parking"  id='no_of_fw_parking'  >
                         
                            
                </div>

                <div class=" input-field  col l3 m4 s12">
                        <label for="loan_amt" id="loan_amt" class="active">No of TW Parking - only Commercial </label>                           
                        <input type="number" class="validate" name="no_of_tw_parking"  id='no_of_tw_parking'  >
                        
                </div>
            </div>


        </div> -->


        

        

        <div class="collapsible-body"  id='address' style="display:none">
                    <div class="row">
                
                        <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">F2F Time & Date : <span class="red-text">*</span></label>
                            <input type="datetime-local"  name="f2f_time_and_date"  required placeholder="Calender" >                                                                                             
                        </div>
                        <div class="input-field col l3 m4 s12 display_search">                
                            <select class="select2  browser-default"  data-placeholder="select" onChange="show_rent_amt()" name="current_residence_status" id="current_residence_status">
                                <option value="" selected disabled>Select </option>
                                <option value="owned">Owned</option>
                                <option value="rented">Rented</option>
                            </select>
                            <label for="purpose" class="active"> Current Residence Status  </label>
                        </div>  

                        <div class="input-field col l3 m4 s12 "  >
                             <div  class="sub_val no-search">
                                <select  id="rent_amt" name="rent_amt" onChange="convert_rent_amt()"  class="select2 browser-default">
                                    <option value="" disabled selected>Select</option>
                                    <option value="10000">10K</option>
                                    <option value="20000">20K</option>
                                    <option value="30000">30K</option>                                    
                                </select>
                            </div>    
                            <label for="wap_number" class="dopr_down_holder_label active">Rent Amount: <span class="red-text">*</span></label>
                            <div class="rupeeSign">
                            <input type="text" class="validate mobile_number" name="rent_amount" id="rent_amount" required  >
                            </div>
                            <!-- <span id="rent_amt_words"></span>                                 -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col l3 m4 s12" >  
                            <label for="lead_assign" class="active">Current Residence Address: <span class="red-text">*</span></label>
                            <!-- <textarea style="border-color: #5b73e8;padding-top: 12px;margin-bottom:-5px !important" Placeholder="Enter" name="current_residence_address"  rows='2' col='4'></textarea> -->
                            <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  data-placeholder="Select" name="city">
                                <option value="" disabled selected>Select</option>
                                <option value="option 1">Thane</option>
                                <option value="option 1">Dadar</option>
                            </select>
                            <label for="possession_year" class="active">City </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                            <a href="#modal5" id="add_cra_city" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  data-placeholder="Select" name="state">
                                <!-- <option value="" disabled selected>Select</option> -->
                                <option value="option 1" selected>Maharashtra</option>
                                <option value="option 1">Dadar</option>
                            </select>
                            <label for="possession_year" class="active">State </label>
                            <!-- <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                            <a href="#" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div> -->
                        </div>

                        <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">Pincode : <span class="red-text">*</span></label>
                            <input type="text"  name="current_pincode"  required Placeholder="Enter" >                                                                                             
                        </div>
                    </div>
            <div class="row">                

                        <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">Company Name & Address : <span class="red-text">*</span></label>
                            <input type="text"  name="company_name"  required Placeholder="Enter" >                                                                                             
                        </div>


                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  data-placeholder="Select" name="city">
                                <option value="" disabled selected>Select</option>
                                <option value="option 1">Thane</option>
                                <option value="option 1">Dadar</option>
                            </select>
                            <label for="possession_year" class="active">City </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                                <a href="#modal6" id="add_com_city" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  data-placeholder="Select" name="state">
                                <!-- <option value="" disabled selected>Select</option> -->
                                <option value="option 1" selected>Maharashtra</option>
                                <option value="option 1">Dadar</option>
                            </select>
                            <label for="possession_year" class="active">State </label>
                            <!-- <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                            <a href="#" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div> -->
                        </div>

                        <div class="input-field col l3 m4 s12" >                    
                            <label for="wap_number" class="active">Pincode : <span class="red-text">*</span></label>
                            <input type="text"  name="company_pincode"  required Placeholder="Enter" >                                                                                             
                        </div>


            </div>
            <div class="row">

                       
                        
                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  data-placeholder="Select" name="designation">
                                <option value="" disabled selected>Select</option>
                                <option value="option 1" >Sales Manager</option>
                                <option value="option 1">Sales Executive</option>
                            </select>
                            <label for="possession_year" class="active">Designation </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                            <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  data-placeholder="Select" name="industry_name">
                                <option value="" disabled selected>Select</option>
                                <option value="option 1">IT</option>
                                <option value="option 1">Hospitality</option>
                            </select>
                            <label for="possession_year" class="active">Industry </label>
                            <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                            <a href="#modal10" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div>
                        </div>
                        <div class="input-field col l3 m4 s12 display_search">                    
                            <select class="select2  browser-default"  data-placeholder="Select" name="industry_name">
                                <option value="" disabled selected>Select</option>
                                <option value="option 1">IT</option>
                                <option value="option 1">Hospitality</option>
                            </select>
                            <label for="possession_year" class="active">Industry Type </label>
                            <!-- <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;"  >
                            <a href="#modal10" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                             </div> -->
                        </div>

                        

                        
                        <div class="input-field col l3 m4 s12" >  
                            <label for="lead_assign" class="active">Information: <span class="red-text">*</span></label>
                            <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="information"  rows='2' col='4'></textarea> -->
                            <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                        </div>
            </div>
           
        </div>

<div class="collapsible-body"  id='members' style="display:none">
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Family Size : <span class="red-text">*</span></label>
            <input type="number"  name="family_size"  Placeholder="Enter" >
        </div>

        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Working Members: <span class="red-text">*</span></label>
            <input type="number"  name="working_members" Placeholder="Enter" >
        </div>

        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Members details: <span class="red-text">*</span></label>
            <input type="text"  name="members_details"  Placeholder="Enter">
        </div>  
    </div>
</div>


<div class="collapsible-body"  id='nature_of_business' style="display:none">
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Nature of Business : <span class="red-text">*</span></label>
            <input type="text"  name="nature_of_bussiness"  Placeholder="Enter" >
        </div>

        <div class="input-field col l3 m4 s12 display_search">                
            <select class="select2  browser-default" multiple="multiple" id="washroom" data-placeholder="Select" name="washroom">
                <option value="option 1">Attached </option>
                <option value="option 1">Common</option>
            </select>
            <label for="building_type" class="active">Washroom </label>
        </div>
    </div>
</div>

<div class="collapsible-body"  id='brokerage_fee' style="display:none">
    <div class="row">

    <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Property Value : <span class="red-text">*</span></label>
            <input type="text"  name="property_value" placeholder="Enter" >
            <!-- <div  class="addbtn" >
                    <a href="#" id="add_location13" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  -->
        </div>
             <div class="input-field col l3 m4 s12 ">
                    <div  class="sub_val no-search">
                    <select  id="brokerage_percent" name="brokerage_percent" onChange="calculate_brokerage()"  class="select2 browser-default">
                    <option value="" disabled selected>Select</option>
                    <option value="0.1">0.1 %</option>
                    <option value="0.2">0.2 %</option>
                    <option value="0.3">0.3 %</option>
                    <option value="0.4">0.4 %</option>
                    <option value="0.5">0.5 %</option>
                    <option value="0.6">0.6 %</option>
                    <option value="0.7">0.7 %</option>
                    <option value="0.8">0.8 %</option>
                    <option value="0.9">0.9 %</option>
                    <option value="1">1 %</option>    
                    <option value="1.1">1.1 %</option>
                    <option value="1.2">1.2 %</option>
                    <option value="1.3">1.3 %</option>
                    <option value="1.4">1.4 %</option>
                    <option value="1.5">1.5 %</option>
                    <option value="1.6">1.6 %</option>
                    <option value="1.7">1.7 %</option>
                    <option value="1.8">1.8 %</option>
                    <option value="1.9">1.9 %</option>
                    <option value="2">2 %</option>    
                    <option value="2.1">2.1 %</option>
                    <option value="2.2">2.2 %</option>
                    <option value="2.3">2.3 %</option>
                    <option value="2.4">2.4 %</option>
                    <option value="2.5">2.5 %</option>
                    <option value="2.6">2.6 %</option>
                    <option value="2.7">2.7 %</option>
                    <option value="2.8">2.8 %</option>
                    <option value="2.9">2.9 %</option>
                    <option value="3">3 %</option>                             
                                     

                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Brokerage fees: <span class="red-text">*</span></label>
               <div class="rupeeSign">
                <input type="text" class="validate mobile_number" name="brokerage_fees" id="brokerage_fees" placeholder="Numeric" required  >
                <span id="brokerage_fees_words"></span>                                
            </div>
            </div>

            <div class="input-field col l3 m4 s12" >  
                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" placeholder="Enter" name="brokerage_comment"  rows='2' col='4'></textarea> -->
                <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
            </div> 
    </div>
</div>




     <!-- Modal Company address - city Structure -->
     <div id="modal5" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add City</h5>
        <hr>
        
        <form method="post" id="add_res_city_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">City: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_res_city" id="add_res_city"   placeholder="Enter">
                    <span class="add_res_city_err"></span>
                    
                </div>
                <div class="input-field col l3 m4 s12 ">                        
                    <select  id="seller_dfd" name="seller_dfd" placeholder="Select" class="select2  browser-default" >
                        <option>option1</option>
                        <option>option2</option>
                    </select>
                    <label class="active">State</label>                
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_res_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>

                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_res_city_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div>    

            </div>
        </form>
    </div>

    <!-- Modal Company address - city Structure -->
    <div id="modal6" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add City</h5>
        <hr>
        
        <form method="post" id="add_Company_city_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">City: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_company_city" id="add_company_city"   placeholder="Enter">
                    <span class="add_company_city_err"></span>
                    
                </div>
                <div class="input-field col l3 m4 s12 ">                        
                    <select  id="seller_dfd" name="seller_dfd" placeholder="Select" class="select2  browser-default" >
                        <option>option1</option>
                        <option>option2</option>
                    </select>
                    <label class="active">State</label>                
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_company_city" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_Company_city_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div>    
            </div>
        </form>
    </div>

      <!-- Modal Designation Structure -->
      <div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>


      <!-- Modal Industry Name Structure -->
      <div id="modal10" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Industry Name</h5>
        <hr>
        
        <form method="post" id="add_industry_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Industry Type: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_industry_type" id="add_industry_type"   placeholder="Enter">
                    <span class="add_industry_name_err"></span>                    
                </div>
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Industry Name: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_industry_name" id="add_industry_name"   placeholder="Enter">
                    <span class="add_industry_name_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_industry_name" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_industry_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>
