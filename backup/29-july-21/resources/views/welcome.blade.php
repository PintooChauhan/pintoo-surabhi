<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->

      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->

      <!-- BEGIN: Page Main-->
      <div id="main">
         <div class="row">
            <!--<div id="breadcrumbs-wrapper" data-image="app-assets/images/gallery/breadcrumb-bg.jpg">
               <div class="container">
                   <div class="row">
                       <div class="col s12 m6 l6">
                           <h5 class="breadcrumbs-title mt-0 mb-0"><span>Form Wizard</span></h5>
                       </div>
                       <div class="col s12 m6 l6 right-align-md">
                           <ol class="breadcrumbs mb-0">
                               <li class="breadcrumb-item"><a href="index.html">Home</a>
                               </li>
                               <li class="breadcrumb-item"><a href="#">Form</a>
                               </li>
                               <li class="breadcrumb-item active">Form Wizard
                               </li>
                           </ol>
                       </div>
                   </div>
               </div>
               </div>-->
            <div class="col s12">
               <div class="container">
                  <div class="section section-form-wizard">
                     <!--          <div class="card">
                        <div class="card-content">
                            <p class="caption mb-0">We use <a href="https://kinark.github.io/Materialize-stepper/?feedback_email=r%40m.com&feedback_password=sdasdasd#!">Stepper</a>
                                as a Form Wizard. Stepper is a fundamental part of material design
                                guidelines. It makes forms simplier and a lot of other stuffs.</p>
                        </div>
                        </div> -->
                     <!-- Horizontal Stepper -->
                     <div class="row mt-3">
                        <div class="col s12">
                           <div class="card">
                              <div class="card-content pb-0">
                                 <ul class="stepper horizontal">
                                    <li class="step active">
                                       <div class="step-title waves-effect">Lead Details</div>
                                       <div class="step-content">
                                          <form id="form" class="col s12" novalidate="novalidate">
                                             <div class="row">
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select class="select2 browser-default" id="leadby" required data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadby" class="active">Lead by</label>
                                                </div>
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select  id="leadtype" name="leadtype" class="select2 browser-default validate" data-minimum-results-for-search="Infinity" required>
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadtype" class="active">Lead type: <span class="red-text">*</span></label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <!--      <select  id="prop_con_comp" name="prop_con_comp" class="select2 browser-default" multiple="multiple" requireddata-placeholder="Choose your option:" > -->
                                                   
                                                   <select data-placeholder="Choose your option:" class="select2 browser-default"
                                                      id="prop_con_comp" data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled  >Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <div id="AddMoreFileId" class="addbtn">
                                                      <a href="#" id="AddMoreFileBox" class="">+</a> 
                                                   </div>
                                                   <label for="leadtype2" class="active">Property Consultant Company 
                                                   </label>
                                                   
                                                   <!--  <label >Property consultant company:<span class="red-text">*</span></label>  -->
                                                </div>
                                                <!--- multiple selection dropdown select2-->
                                                <div class="input-field col l3 m4 s12 display_search">
                                                   <select class="select2  browser-default" multiple="multiple" id="leadtype3" data-placeholder="Property Consultant Company" name="leadtype3">
                                                      <option value="option 1">option 1</option>
                                                      <option value="option 2">option 2</option>
                                                      <option value="option 3">option 3</option>
                                                      <option value="option 4">option 4</option>
                                                      <option value="option 5">option 5</option>
                                                      <option value="option 6">option 6</option>
                                                   </select>
                                                   <label for="leadtype3" class="active">PC Executive Name 
                                                   </label> 
                                                   
                                                </div>
                                                <!--- multiple selection dropdown select2-->
                                                <!---Dynamic content custome dropdown-->
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_sourch_name" class="active">Lead Source Name
                                                   <span class="red-text">*</span></label> 
                                                   <input type="text" id="lead_sourch_name" name="lead_sourch_name" class="validate" required placeholder="Lead Source" style="z-index:1; background:none;">
                                                   <div id="AddMoreFileId6" class="addbtn" style="margin-right:8px; display:block; z-index:0">
                                                      <i class="material-icons" style=="font-size:15px; color:#888; ">expand_more</i>
                                                   </div>
                                                   <div class="lead_source_info" id="lead_source_info" >
                                                      <div class="form-group" style="height:250px;  overflow-y:scroll">
                                                         <h4 class="card-title portal">PORTAL</h4>
                                                         <div class="form-check countable_chkboxex mb-3">
                                                            <label class="form-check-label" for="acres">
                                                            <input class="form-check-input filled-in" type="checkbox" name="project_listing" value="99 Acres" id="acres" onchange="open_acres()">
                                                            <span>99 Acres</span>
                                                            </label>           
                                                         </div>
                                                         <div class="acress_compain_names">
                                                            <div class="form-check inline-block-div mb-3">
                                                               <label class="form-check-label" for="defaultCheck1">
                                                               <input class="form-check-input filled-in" type="checkbox" value="" id="defaultCheck1">
                                                               <span>Project Listing </span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3">
                                                               <label class="form-check-label" for="nl">
                                                               <input class="form-check-input filled-in" type="checkbox" value="" id="nl">
                                                               <span> Normal Listing</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3">
                                                               <label class="form-check-label" for="om">
                                                               <input class="form-check-input filled-in" type="checkbox" value="" id="om">
                                                               <span>Omni</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3">
                                                               <label class="form-check-label" for="eml">
                                                               <input class="form-check-input filled-in" type="checkbox" value="" id="eml">
                                                               <span>Email Mktg </span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3">
                                                               <label class="form-check-label" for="some">
                                                               <input class="form-check-input filled-in" type="checkbox" value="" id="some">
                                                               <span>Social media</span>
                                                               </label>
                                                            </div>
                                                         </div>
                                                         <!-- close 99 acress info  -->
                                                         <div class="form-check countable_chkboxex mb-3">
                                                            <label class="form-check-label" for="magic_brickss">
                                                            <input class="form-check-input filled-in" type="checkbox" name="normal_listing" value="Magic Bricks" id="magic_brickss" onchange="open_magic_bricks()">
                                                            <span>Magic Bricks</span>
                                                            </label>
                                                         </div>
                                                         <div class="magic_bricks_compain_name">
                                                            <div class="form-check inline-block-div mb-3">
                                                               <label class="form-check-label" for="prls">
                                                               <input class="form-check-input filled-in" type="checkbox" value="" id="prls">
                                                               <span> Premium Listing</span>
                                                               </label>
                                                            </div>
                                                         </div>
                                                         <!-- close magic_bricks -->
                                                         <div class="form-check countable_chkboxex mb-3">
                                                            <label class="form-check-label" for="common_floor">
                                                            <input class="form-check-input filled-in" type="checkbox" name="normal_listing" value="Common floor" id="common_floor" onchange="open_commonFloor()">
                                                            <span>Common floor</span>
                                                            </label>
                                                         </div>
                                                         <div class="common_floor_compain_name">
                                                            <div class="form-check inline-block-div mb-3">
                                                               <label class="form-check-label" for="remli">
                                                               <input class="form-check-input filled-in" type="checkbox" value="" id="remli">
                                                               <span>Premium Listing</span>
                                                               </label>
                                                            </div>
                                                         </div>
                                                         <!-- close common flor compaun name  -->                                                         
                                                         <dir class="magic_bricks">
                                                            <h4 class="card-title portal">Other </h4>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="cibob">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Call - In bound / Out bound" id="cibob">
                                                               <span>Call - In bound / Out bound</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="fb">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Face Book" id="fb">
                                                               <span>Face Book</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="gadlp">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Google - Ad words / Landing Page " id="gadlp">
                                                               <span>Google - Ad words / Landing Page </span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="link">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Linkedin" id="link">
                                                               <span>Linkedin</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="sms">
                                                               <input class="form-check-input filled-in" type="checkbox" value="SMS" id="sms">
                                                               <span> SMS</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="eiob">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Email - Inbound / Out Bound" id="eiob">
                                                               <span>Email - Inbound / Out Bound </span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="wap">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Whats App" id="wap">
                                                               <span> Whats App</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="insta">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Instagram" id="insta">
                                                               <span>Instagram</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="yubt">
                                                               <input class="form-check-input filled-in" type="checkbox" value=" You tube" id="yubt">
                                                               <span>You tube</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="telg">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Telegram" id="telg">
                                                               <span>Telegram</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="walkn">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Walk in " id="walkn">
                                                               <span>Walk in </span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="tdusa">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Telephone Directory - unknown source - add" id="tdusa">
                                                               <span>Telephone Directory - unknown source - add</span>
                                                               </label>
                                                            </div>
                                                            <div class="form-check inline-block-div mb-3 countable_chkboxex">
                                                               <label class="form-check-label" for="web">
                                                               <input class="form-check-input filled-in" type="checkbox" value="Website" id="web">
                                                               <span>Website</span>
                                                               </label>
                                                            </div>
                                                         </div>
                                                         <!-- close other -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <!--- multiple selection dropdown select2-->
                                                <!--- multiple selection dropdown normal-->
                                                <div class="input-field col l3 m4 s12 no-search">
                                                   <select class="select2 browser-default" id="drop_value" name="drop_value">
                                                      <option value="square">Square</option>
                                                      <option value="rectangle">Rectangle</option>
                                                      <option value="rombo">Rombo</option>
                                                      <option value="romboid">Romboid</option>
                                                      <option value="trapeze">Trapeze</option>
                                                      <option value="traible">Triangle</option>
                                                      <option value="polygon">Polygon</option>
                                                   </select>
                                                   <label class="active">Lead Reached from
                                                   </label>
                                                </div>
                                                <!--- multiple selection dropdown  normal-->
                                                <!--- combine input-->
                                                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                                   <label for="cus_name active" class="dopr_down_holder_label active">Customer Name 
                                                   <span class="red-text">*</span></label>
                                                   <div  class="sub_val no-search">
                                                      <select  id="cus_name_sel" name="cus_name_sel" class="select2 browser-default ">
                                                         <option value="1">Mr.</option>
                                                         <option value="2">Miss.</option>
                                                         <option value="3">Mrs.</option>
                                                      </select>
                                                   </div>
                                                   <input type="text" class="validate mobile_number" name="contactNum" id="cus_name" required placeholder="Customer Name"  >
                                                </div>
                                                <!--- combine input-->
                                                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                                   <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label>
                                                   <div  class="sub_val no-search">
                                                      <select  id="country_code" name="country_code" class="select2 browser-default">
                                                         <option value="1">+91</option>
                                                         <option value="2">+1</option>
                                                         <option value="3">+3</option>
                                                      </select>
                                                   </div>
                                                   <input type="text" class="validate mobile_number" name="contactNum" id="contactNum1" required placeholder="Mobile Number" >
                                                   <div id="AddMoreFileId" class="addbtn">
                                                      <a href="#" id="AddMoreFileBox" class="">+</a> 
                                                   </div>
                                                </div>
                                                <span class="" id="display_inputs"></span>
                                                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                                   <div  class="sub_val no-search">
                                                      <select  id="country_code1" name="country_code1" class="select2 browser-default">
                                                         <option value="1">+91</option>
                                                         <option value="2">+1</option>
                                                         <option value="3">+3</option>
                                                      </select>
                                                   </div>
                                                   <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate mobile_number" name="wap_number" id="wap_number" required placeholder="Whatsapp Number" >
                                                </div>
                                                <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                                                   <label for="primary_email active" class="active">Primary Email: <span class="red-text">*</span></label>
                                                   <input type="email" class="validate" name="primary_email" id="primary_email" required placeholder="Primary Email" >
                                                   <div id="AddMoreFileId3" class="addbtn">
                                                      <a href="#" id="AddMoreFileBox3" class="">+</a> 
                                                   </div>
                                                </div>
                                                <div class="" id="display_inputs3"></div>
                                                <div class="input-field col l3 m4 s12 display_search">
                                                   <!--   <label for="lead_assign">Lead assign: <span class="red-text">*</span></label>
                                                      <input type="text" class="validate" name="lead_assign" id="lead_assign" required> -->
                                                   <select  id="lead_assign" name="lead_assign" class="validate select2 browser-default">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label class="active">Lead assign</label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_assign" class="active">Lead code: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate" name="lead_assign" id="lead_assign" required placeholder="Lead code"  >
                                                </div>
                                             </div>
                                             <div class=" ">
                                                <div class="row">
                                                   <div class="col m12 s12 mb-3 text-right">
                                                      <button class="red btn btn-reset" type="reset">
                                                      <i class="material-icons left">clear</i>Reset
                                                      </button>
                                                      <button class="btn btn-light previous-step" disabled>
                                                      <i class="material-icons left">arrow_back</i>
                                                      Prev
                                                      </button>
                                                      <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                      <i class="material-icons right">arrow_forward</i></button>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </li>
                                    <li class="step ">
                                       <div class="step-title waves-effect">Requirment Details</div>
                                       <div class="step-content">
                                          <form id="form" class="col s12" novalidate="novalidate">
                                             <div class="row">
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select class="select2 browser-default" id="leadby1" required data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadby" class="active">Heading</label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_assign12" class="active">Heading: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate" name="lead_assign23" id="lead_assign23" required placeholder="Heading"  >
                                                </div>
                                             </div>
                                             <div class=" ">
                                                <div class="row">
                                                   <div class="col m12 s12 mb-3 text-right">
                                                      <button class="red btn btn-reset" type="button">
                                                      <i class="material-icons left">clear</i>Reset
                                                      </button>
                                                      <button class="btn btn-light previous-step" disabled>
                                                      <i class="material-icons left">arrow_back</i>
                                                      Prev
                                                      </button>
                                                      <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                      <i class="material-icons right">arrow_forward</i></button>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </li>
                                    <li class="step ">
                                       <div class="step-title waves-effect">Internal Details</div>
                                       <div class="step-content">
                                          <form id="form" class="col s12" novalidate="novalidate">
                                             <div class="row">
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select class="select2 browser-default" id="leadby13" required data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadby13" class="active">Heading</label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_assign1f2" class="active">Heading: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate" name="lead_assign1f2" id="lead_assign23" required placeholder="Heading"  >
                                                </div>
                                             </div>
                                             <div class=" ">
                                                <div class="row">
                                                   <div class="col m12 s12 mb-3 text-right">
                                                      <button class="red btn btn-reset" type="button">
                                                      <i class="material-icons left">clear</i>Reset
                                                      </button>
                                                      <button class="btn btn-light previous-step" disabled>
                                                      <i class="material-icons left">arrow_back</i>
                                                      Prev
                                                      </button>
                                                      <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                      <i class="material-icons right">arrow_forward</i></button>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </li>
                                    <li class="step ">
                                       <div class="step-title waves-effect">Property Detail</div>
                                       <div class="step-content">
                                          <form id="form" class="col s12" novalidate="novalidate">
                                             <div class="row">
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select class="select2 browser-default" id="leadbyfdg" required data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadbyfdg" class="active">Heading</label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_assigner23" class="active">Heading: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate" name="lead_assigner23" id="lead_assign23" required placeholder="Heading"  >
                                                </div>
                                             </div>
                                             <div class=" ">
                                                <div class="row">
                                                   <div class="col m12 s12 mb-3 text-right">
                                                      <button class="red btn btn-reset" type="button">
                                                      <i class="material-icons left">clear</i>Reset
                                                      </button>
                                                      <button class="btn btn-light previous-step" disabled>
                                                      <i class="material-icons left">arrow_back</i>
                                                      Prev
                                                      </button>
                                                      <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                      <i class="material-icons right">arrow_forward</i></button>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </li>
                                    <li class="step ">
                                       <div class="step-title waves-effect">Property Matching</div>
                                       <div class="step-content">
                                          <form id="form" class="col s12" novalidate="novalidate">
                                             <div class="row">
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select class="select2 browser-default" id="leadby1rg" required data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadby1rg" class="active">Heading</label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_assignd12" class="active">Heading: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate" name="lead_assignd12" id="lead_assign23" required placeholder="Heading"  >
                                                </div>
                                             </div>
                                             <div class=" ">
                                                <div class="row">
                                                   <div class="col m12 s12 mb-3 text-right">
                                                      <button class="red btn btn-reset" type="button">
                                                      <i class="material-icons left">clear</i>Reset
                                                      </button>
                                                      <button class="btn btn-light previous-step" disabled>
                                                      <i class="material-icons left">arrow_back</i>
                                                      Prev
                                                      </button>
                                                      <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                      <i class="material-icons right">arrow_forward</i></button>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     