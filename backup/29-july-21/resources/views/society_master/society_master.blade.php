<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}
.card .card-content {
    padding-top: 0px;
    min-height: auto !important;
}
      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->

         

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

            

        <div class="row" style="font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD SOCIETY MASTER</span>
                </div><br>
                <form method="post" id="society_master_form">@csrf
                <input type="hidden" name="project_id" value="{{$project_id}}" >
                <div class="row">
                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Building Owner: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="building_owner"    placeholder="Enter"  >                             
                            
                    </div>

                       <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="building_status" name="building_status" onChange="hide_show_forms()" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                  @foreach($building_type as $building_type)                               
                                    <option value="{{$building_type->building_type_id}}" >{{ ucwords($building_type->building_type_name)}}</option>
                                   @endforeach                                
                                </select>
                                <label class="active">Building Status</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="unit_status" name="unit_status" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                   @foreach($unit_status as $unit_status)                               
                                    <option value="{{$unit_status->unit_status_id}}" >{{ strtoupper($unit_status->unit_status)}}</option>
                                   @endforeach
                                </select>
                                <label class="active">Unit Status</label>
                        </div>


                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                    
                     
                        <select  id="account_manager" name="account_manager" class="select2 browser-default ">
                            <option value="" disabled selected>Select</option>
                            @foreach($employee as $employee)
                            <option value="{{$employee->employee_id}}">{{ucfirst($employee->employee_name)}}</option>
                            @endforeach
                        </select>
                        <label class="active">Account Manager</label>
                            
                    </div>
                </div>

                <div class="row">
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Building  Name: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="building_name"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Building Code: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="building_code"   placeholder="Enter" >
                        </div>    

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="building_category" name="building_category" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                  @foreach($category as $category)                               
                                    <option value="{{$category->category_id}}" >{{ strtoupper($category->category_name)}}</option>
                                   @endforeach                                 
                                </select>
                                <label class="active">Building Category</label>
                        </div>

                         <div class="input-field col l3 m4 s12" id="InputsWrapper">
                            <div class="row">
                                <div class="input-field col m8 s8" style="padding: 0 10px;">
                                <input type="number" class="input_select_size" name="building_land_parcel_area" id="building_land_parcel_area"  placeholder="Enter" >
                                    <label> Building Land Parcel
                                </div>
                                <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                    <select  id="building_land_parcel_area_unit" name="building_land_parcel_area_unit" class="select2 browser-default ">
                                    @foreach($unit as $un)
                                    <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>   

                    </div>

                    <div class="row"> 
                        <div class="input-field col l3 m4 s12" id="InputsWrapper">
                            <div class="row">
                                <div class="input-field col m8 s8" style="padding: 0 10px;">
                                <input type="number" class="input_select_size" name="building_constructed_area" id="building_constructed_area"  placeholder="Enter" >
                                    <label> Building Constructed Area
                                </div>
                                <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                    <select  id="building_constructed_area_unit" name="building_constructed_area_unit" class="select2 browser-default ">
                                    @foreach($unit as $un)
                                    <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>     

                        <div class="input-field col l3 m4 s12" id="InputsWrapper">
                            <div class="row">
                                <div class="input-field col m8 s8" style="padding: 0 10px;">
                                <input type="number" class="input_select_size" name="building_open_space" id="building_open_space"  placeholder="Enter" >
                                    <label> Building Open space
                                </div>
                                <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                    <select  id="building_open_space_unit" name="building_open_space_unit" class="select2 browser-default ">
                                    @foreach($unit as $un)
                                    <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div> 

                      

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="building_type" name="building_type" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    @foreach($building_type1 as $building_type)
                                    <option value="{{ $building_type->building_type_id }}">{{ ucwords($building_type->building_type_name) }}</option>                                    
                                    @endforeach                       
                                </select>
                                <label class="active">Building Type</label>
                        </div>
                        <div class="input-field col l3 m4 s12 display_search">                                
                            <select  id="parking" name="parking[]"   multiple="multiple" class="validate select2 browser-default">
                            <!-- <option value="" disabled selected>Select</option> -->
                                  @foreach($parking as $parking)
                                    <option value="{{ $parking->parking_id  }}">{{ ucwords($parking->parking_name) }}</option>                                    
                                    @endforeach                              
                            </select>
                            <label class="active">Parking Type</label>
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        @foreach($project_amenities as $ame)            
                            <div class="col l6 m6 s12">
                                <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">{{ucwords($ame->amenities)}}</div>
                                <div class="col l3 m3 s12">
                                    <label><input type="checkbox" name="is_available[]" checked value="{{$ame->project_amenities_id}}"><span>Available</span></label>
                                </div>
                                <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">
                                    <label  class="active">Description: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="description_ame[]" value="{{$ame->description}}"   placeholder="Enter" style="height: 36px;" >
                                </div>
                            
                            </div>         
                        @endforeach
                     </div>
                     <br>
                     <div class="row">

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="location" name="location" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    @foreach($location as $location)
                                    <option value="{{$location->location_id}}">{{ ucwords($location->location_name)}}</option>
                                    @endforeach
                                </select>
                                <label class="active">Location: <span class="red-text">*</span></label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="sub_location" name="sub_location" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    @foreach($sub_location as $sub_location)
                                    <option value="{{$sub_location->sub_location_id}}">{{ ucwords($sub_location->sub_location_name)}}</option>
                                    @endforeach                                 
                                </select>
                                <label class="active">Sub Location: <span class="red-text">*</span></label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="city" name="city" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                @foreach($city as $city)
                                    <option value="{{$city->city_id}}">{{ ucwords($city->city_name)}}</option>
                                    @endforeach                                  
                                </select>
                                <label class="active">City: <span class="red-text">*</span></label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="state" name="state" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    @foreach($state as $state)
                                    <option value="{{$state->dd_state_id}}">{{ ucwords($state->state_name)}}</option>
                                    @endforeach                                  
                                </select>
                                <label class="active">State: <span class="red-text">*</span></label>
                        </div>

                        </div>
                    <div class="row">
                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                             <select  id="construction_technology" name="construction_technology" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    @foreach($construction_technology as $construction_technology)
                                    <option value="{{$construction_technology->construction_technology_id}}">{{ ucwords($construction_technology->construction_technology_name)}}</option>
                                    @endforeach                                  
                             </select>
                                <label class="active">Construction technology: <span class="red-text">*</span></label>
                        </div> 

                        <div class="col m3 l3 s12">
                                <div class="row">
                                    <div class="input-field col l6 m6 s8" style="padding: 0 10px;">
                                    <label  class="active">Building Geo Location: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="society_geo_location_lat"   placeholder="Enter LAT" >
                                    </div>
                                    <div class="input-field col l6 m6 s4 mobile_view" style="padding: 0 10px;">
                                    <label  class="active">Building Geo Location: <span class="red-text">*</span></label>
                                    <input type="text" class="validate" name="society_geo_location_long"   placeholder="Enter LONG" >
                                    </div>
                                </div>
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <label  class="active">Maintenance Amount: <span class="red-text">*</span></label>
                                <input type="number" class="validate" name="maintenance_amount"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select   name="building_rating" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                   @foreach($rating as $rating)
                                    <option value="{{ $rating->rating_id  }}">{{ $rating->rating }}</option>                                    
                                    @endforeach                                    
                                </select>
                                <label class="active">Building Rating</label>
                        </div>

                    </div>
                    <div class="row">

                       <div class="input-field col l4 m6 s12" id="InputsWrapper2">                        
                             <label  class="active">Pros of Building: <span class="red-text">*</span></label>
                            <!-- <textarea style="border-color: #5b73e8; padding-top: 12px;width:104% !important" placeholder="Enter" name="sales_office_address" rows='20' col='40'></textarea> -->
                            <input type="text" class="validate" name="pros_of_building"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l4 m6 s12" id="InputsWrapper2">                        
                             <label  class="active">Cons of Building: <span class="red-text">*</span></label>
                            <!-- <textarea style="border-color: #5b73e8; padding-top: 12px;width:104% !important" placeholder="Enter" name="sales_office_address" rows='20' col='40'></textarea> -->
                            <input type="text" class="validate" name="cons_of_building"   placeholder="Enter" >
                        </div>

                        <!-- <div class="input-field col l4 m6 s12" id="InputsWrapper2">                        
                            <label  class="active">Building Description: <span class="red-text">*</span></label>
                            <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea>
                        </div>   -->
                   </div>

                    <!-- put in below row html editor  -->
                 <div class="row">
           
           <section class="full-editor">
               <div class="row">
                   <div class="col s12">
                 
                       <div class="card">
                       <br>
                           <div class="card-content">
                           Building  Description

                              
                               <div class="row">
                                   <div class="col s12 l10 m4">
                                   <textarea style="border-color: #5b73e8; padding-top: 12px;width: 1238px; height: 46px;" placeholder="Enter" name="building_description"  ></textarea>
                                       <!-- <div id="full-wrapper" name="html_editor2">
                                           <div id="full-container" name="html_editor1">
                                               <div class="editor" name="html_editor">
                                                   
                                                   
                                               </div>
                                           </div>
                                       </div> -->
                                       
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       </div>
                
                <div id="under_construction" style="display:none">
               
                    <div class="row">                        
                        <div class="input-field col l3 m4 s12 display_search">                                 
                                <select  id="payment_plan" name="payment_plan[]"   multiple="multiple" class="validate select2 browser-default">
                                <option value="" disabled >Select</option>
                                   @foreach($payment_type as $payment_type)                            
                                    <option value="{{$payment_type->payment_type_id}}"  >{{$payment_type->payment_name}}</option>       
                                   @endforeach
                                </select>
                                <label class="active"> Payment Plan</label>
                        </div>

                        <!-- <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Floor Rise click Button: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="project_land_parcel"   placeholder="Enter" >
                        </div> -->

                        <div class="input-field col l3 m4 s12 display_search">
                                <ul class="collapsible" style="margin-top: -3px">
                                    <li onClick="society_floor_rice()">
                                    <div class="collapsible-header"  id="col_society_floor_rice" >Floor Rise</div>                                    
                                    </li>                                        
                                </ul>
                            </div>

                            
                        <div class="input-field col l3 m4 s12 display_search">
                                <ul class="collapsible" style="margin-top: -3px">
                                    <li onClick="society_brokarage_fee()">
                                    <div class="collapsible-header"  id="col_society_brokarage_fee" >Brokerage Fees</div>                                    
                                    </li>                                        
                                </ul>
                            </div>


                        
                        <!-- <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Brokerage Fees click button: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="project_land_parcel"   placeholder="Enter" >
                        </div> -->

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                            <label  class="active">Comment on Brokerage: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="brokarage_comments"   placeholder="Enter" >
                            <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="brokarage_comments" rows='20' col='40'></textarea> -->
                        </div>  
                    </div>
                    <div class="row"  id='society_brokarage_fee_table' style="display:none">
                    <div class="col l3"></div><div class="col l3"></div>
                        <div class="col l3">
                            <table class="bordered">
                                <thead>
                                <tr style="color: white;background-color: #ffa500d4;">                        
                                <th >No. of Booking</th>
                                <th>Brokerage %</th>                        
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>0 to 2
                                    </td>
                                    <td><input class="col l10" type="number" class="  validate" name="zero_to_two"   placeholder="Enter" ></td>                                                
                                </tr>
                                <tr>
                                    <td>3 to 5
                                    </td>
                                    <td><input class="col l10" type="number" class="  validate" name="three_to_five"   placeholder="Enter" ></td>                                                
                                </tr>

                                <tr>
                                    <td>6 to 10
                                    </td>
                                    <td><input class="col l10" type="number" class="  validate" name="six_to_ten"   placeholder="Enter" ></td>                                                
                                </tr>

                                <tr>
                                    <td>11 and above
                                    </td>
                                    <td><input class="col l10" type="number" class="  validate" name="above_eleven"   placeholder="Enter" ></td>                                                
                                </tr>

                                </tbody>
                            </table>
                        </div>  
                    </div>

                    <div id='society_floor_rice_table' style="display:none">        
                        <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
                            <div class="clonable" data-ss="1">      
                                <div class="row">
                                    <div class="col l3"></div>
                                    <div class="input-field col l1 m4 s12">
                                            <label  class="active">Floor slab: <span class="red-text">*</span></label>
                                        <input type="text" class="  validate" name="floor_slab_to[]"   placeholder="Enter" >                                
                                        </div> 
                                        <div class="input-field col l1 m4 s12">
                                        <label  class="active">Floor slab: <span class="red-text">*</span></label>
                                        <input type="text" class="  validate" name="floor_slab_from[]"   placeholder="Enter" >                                
                                        </div>

                                    <div class="input-field col l1 m4 s12">
                                        <label  class="active">Configuration: <span class="red-text">*</span></label>
                                        <input type="text" class=" validate" name="configuration1[]"   placeholder="Enter" >                                
                                        </div>
                                    <div class="input-field col l1 m4 s12">
                                        <label  class="active">Rs. Per Sq. ft.: <span class="red-text">*</span></label>
                                        <input type="text" class="  validate" name="rs_per_sq_ft[]"   placeholder="Enter" >                                
                                        </div>
                                    <div class="input-field col l1 m4 s12">
                                        <label  class="active">Box price: <span class="red-text">*</span></label>
                                        <input type="text" class="  validate" name="box_price[]"   placeholder="Enter" >                                
                                        </div>
                                
                                        <div class="input-field col l2 m4 s12">    
                                        <a href="javascript:void(0)" id="hap" class="clonable-button-add" title="Add"   style="color: red !important"> <i class="material-icons  dp48">add</i></a> 
                                        <a href="javascript:void(0)" class="clonable-button-close" title="Remove"  style="color: red !important"> <i class="material-icons  dp48">remove</i></a> 
                                    
                                        </div>

                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="row">                       

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Funded by: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="funded_by"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Building Construction Stage: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="building_construction_stage"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Building Construction Stage Date: <span class="red-text">*</span></label>
                            <input type="text" class="datepicker" name="building_construction_stage_date"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Construction Stage Image  : </label>
                            <input type="file" name="construction_stage_images[]" multiple="true"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">                        
                        </div>

                    </div>
                    <div class="row">                      

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Building Launch Date: <span class="red-text">*</span></label>
                            <input type="text" class="datepicker" name="buulding_lauch_date"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Builder Completion Year: <span class="red-text">*</span></label>
                            <input type="text" class="datepicker" name="builder_completion_year"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active"> Rera Completion Year: <span class="red-text">*</span></label>
                            <input type="text" class="datepicker" name="rera_completion_year"   placeholder="Enter" >
                        </div> 

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">RERA Certificate : </label>
                            <input type="file" name="rera_certificate"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                        </div>
                    </div>
                    <div class="row">                        

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Flat Images Upload : </label>
                            <input type="file" name="flat_images[]" multiple="true" id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                           <label  class="active">Comments: <span class="red-text">*</span></label>
                          <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea>
                        </div> 

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Building Images Upload : </label>
                            <input type="file" name="building_images[]" multiple="true" id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">                        
                        </div> 

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Building Video Upload : </label>
                            <input type="file" name="building_video[]" multiple="true" id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">                        
                        </div>

                    </div>
                    <div class="row">                      

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Building Video Link: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="building_video_links"   placeholder="Enter" >
                        </div> 

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="building_config_details" name="building_config_details" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    <option value="1" >option 1</option>                                    
                                    <option value="2" >option 2</option>                                   
                                </select>
                                <label class="active"> Building Configuration Details </label>
                        </div>
                    </div>

                          


                </div>
                  
               
               
                

                       
                <br>
                <div class="row">
                    <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">                        
                        <th >Configuration & Sample flat</th>
                        <th>Configuration</th>
                        <th>Suffix</th>
                        <th>Area</th>
                        <th>Total Available Flats</th>
                        <th>Flats Available</th>
                        <th>Last Updated Date</th> 
                        <th style="background-color: green !important">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                        <!-- <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
                            <div class="clonable" data-ss="1">  -->
                            <table class="bordered">   
                            <tbody> 
                        <tr>
                        <td style="width: 21%;">
                            <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
                               <select  id="sample_flat_available" name="sample_flat_available[]" class="validate select2 browser-default">
                                    <option value="1" >Yes</option>                                    
                                    <option value="0" >No</option>
                                </select>
                            <div>
                        </td>
                        <td style="width: 12.2%;">  
                             <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
                               <select  name="configuration[]" class="validate select2 browser-default">
                                    <!-- <option value="" selected disabled>Select</option> -->
                                  @foreach($configuration as $configuration)
                                    <option value="{{ $configuration->configuration_id  }}">{{ ucwords($configuration->configuration_name) }}</option>                                    
                                    @endforeach
                                </select>
                            <div>
                        </td>                        
                        <td style="width: 9%;">
                                <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
                                    <select   name="configuration_size[]" class="validate select2 browser-default">
                                        <!-- <option value="" selected disabled>Select</option> -->
                                        @foreach($configuration_size as $configuration_size)
                                        <option value="{{ $configuration_size->configuration_size_id  }}">{{ ucwords($configuration_size->configuration_size) }}</option>                                    
                                        @endforeach
                                    </select>
                               <div>
                        </td>
                        <td style="width: 12.7%;">
                            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                                <input type="text" class="validation" name="area[]"   placeholder="Enter" >
                            </div>
                        </td>
                        <td style="width: 12.7%;">
                            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                                <input type="text" class="validation" name="total_falt_available[]"   placeholder="Enter" >
                            </div>
                        </td>  
                        <td style="width: 12.7%;">
                            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                                <input type="text" class="validation" name="flat_available[]"   placeholder="Enter" >
                            </div>
                        </td>                        
                        <td></td>
                        <td style="width: 9%;">
                        <!-- <a href="#">EDIT</a> -->
                                            <span id="AddMoreFileIdConfig_sample" class="addbtn" style="position: unset">
                                            <a href="#" id="AddMoreFileBoxConfig_sample" class="" style="color: red !important">+</a> 
                                            </span>
                                        <!-- <a href="javascript:void(0)" id="hap" class="clonable-button-add" title="Add" style="color: red !important"> <i class="material-icons  dp48">add</i></a> 
                                        <a href="javascript:void(0)" class="clonable-button-close" title="Remove" style="color: red !important; display: none;"> <i class="material-icons  dp48">remove</i></a>  -->
                        </td>
                        </tr>
                        </tbody>
                        <tbody id="display_inputs_Config_sample"></tbody> 
                        
                        </table>
                        <!-- </div>
                        </div> -->
                        
  
                </div>




                <br>

                <!-- <div class="row"><lable style="margin-left: 15px;font-weight: 700;">Society Key Member</lable></div> -->
                <!-- <div class="row">
                   <div class="col l6 m6 s12">
                        <div class="row">
                            <div class="col l3 m3 s12">
                             
                            <input type="text" class="validate" name="project_land_parcel"   placeholder="Enter Amenity 1" ></div>
                            <div class="col l3 m3 s12">
                                <div id="ck1-button">
                                    <label>
                                        <input type="checkbox" value="available" ><span style="padding: 0px 10px 0px 10px;">Available</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col l3 m3 s12">
                               <select  id="add_section1" name="add_section" class="validate select2 browser-default">
                                    <option value="1" >option1</option>                                    
                                    <option value="0" >option2</option>
                                </select>
                                <label class="active"></label>
                            </div>
                           
                        </div>
                        <br>
                        <div class="row">
                            <div class="col l3 m3 s12"><input type="text" class="validate" name="project_land_parcel"   placeholder="Enter Amenity 2" ></div>
                            <div class="col l3 m3 s12">
                                <div id="ck1-button">
                                    <label>
                                        <input type="checkbox" value="available" ><span style="padding: 0px 10px 0px 10px;">Available</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col l3 m3 s12">
                               <select  id="add_section2" name="add_section" class="validate select2 browser-default">
                                    <option value="1" >option1</option>                                    
                                    <option value="0" >option2</option>
                                </select>
                                <label class="active"></label>
                            </div>
                           
                        </div>
                   </div>
                   <div class="col l6 m6 s12">
                         <div class="row">
                         <div class="col l3 m3 s12"><input type="text" class="validate" name="project_land_parcel"   placeholder="Enter Amenity 1" ></div>
                            <div class="col l3 m3 s12">
                                <div id="ck1-button">
                                    <label>
                                        <input type="checkbox" value="available" ><span style="padding: 0px 10px 0px 10px;">Available</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col l3 m3 s12">
                               <select  id="add_section3" name="add_section" class="validate select2 browser-default">
                                    <option value="1" >option1</option>                                    
                                    <option value="0" >option2</option>
                                </select>
                                <label class="active"></label>
                            </div>
                           
                        </div>
                            <br>
                        <div class="row">
                        <div class="col l3 m3 s12"><input type="text" class="validate" name="project_land_parcel"   placeholder="Enter Amenity 2" ></div>
                            <div class="col l3 m3 s12">
                                <div id="ck1-button">
                                    <label>
                                        <input type="checkbox" value="available" ><span style="padding: 0px 10px 0px 10px;">Available</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col l3 m3 s12">
                               <select  id="add_section4" name="add_section" class="validate select2 browser-default">
                                    <option value="1" >option1</option>                                    
                                    <option value="0" >option2</option>
                                </select>
                                <label class="active"></label>
                            </div>
                           
                        </div>
                   </div>
                </div>
                <br> -->
                  
            <div id="registered" style="display:none">
               <div class="row">

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Shitfing charges : <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="shifting_charges"   placeholder="Enter" >
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                          <select  id="preferred_tenant" name="preferred_tenant" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    @foreach($preferred_tenant as $preferred_tenant)
                                        <option value="{{ $preferred_tenant->preferred_tenant_id  }}">{{ $preferred_tenant->preferred_tenant }}</option>                                    
                                    @endforeach                               
                                </select>
                                <label class="active">Preferred tenant </label>                 
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                             <label  class="active">Society Office Addres: <span class="red-text">*</span></label>
                             <input type="text" name="society_office_address"  class="validate" placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                          <select  id="working_days_and_time" name="working_days_and_time" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    <option value="1" >option 1</option>                                    
                                    <option value="2" >option 2</option>                                   
                                </select>
                                <label class="active">Working days and time </label>                 
                    </div> 
                </div>
                <div class="row">
                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Office Timing: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="office_timing"   placeholder="Enter" >
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Possession year : <span class="red-text">*</span></label>
                            <input type="text" class="datepicker" name="possession_year"   placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Occupation certificate dated : <span class="red-text">*</span></label>
                            <input type="text" class="datepicker" name="occupation_certi_dated"   placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label  class="active">OC Certificte : </label>
                        <input type="file" name="oc_certificate"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col l3 m4 s12 display_search">
                        <label  class="active">Registration Certificate : </label>
                        <input type="file" name="registration_certificate"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                        
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <label  class="active">Conveyance Deed : </label>
                        <input type="file" name="conveyance_deed"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                    </div>      
                    <div class="input-field col l3 m6 s12" id="InputsWrapper2">                        
                        <label  class="active">Building Description: <span class="red-text">*</span></label>
                        <!-- <input type="file" name="building_description"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;"> -->
                        <input type="text" class="validate" name="building_description"   placeholder="Enter" >
                    </div>       
                    <div class="input-field col l3 m6 s12" id="InputsWrapper2">                        
                        <label  class="active">Commment: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="comment"   placeholder="Enter" >
                    </div>       
               </div>

               <!-- <br> -->
                <!-- <div class="row"><lable style="margin-left: 5px;font-weight: 700;">Building Key Member</lable></div>
                <div class="row">
                    <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">                        
                        <th>Designation</th>
                        <th>Name</th>
                        <th>Phone No.</th>
                        <th>Email Id</th>
                        <th>Action</th>
                        <th>Last Update Date</th>                       
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>Sales</td>
                        <td>Tushar Bhagat</td>                        
                        <td>9820677891</td>
                        <td>Alabc@yahoo.comvin</td>
                        <td></td>                        
                        <td>08th April 2021</td>
                        
                        </tr>
                        </tbody>
                    </table>
  
                </div>
            </div> -->

            <hr>
            <div class="row">
                    <!-- <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Name </th>
                        <th>Designation</th>
                        <th>Mobile No.</th>
                        <th>Email Id</th>
                        <th>Reports to</th>
                        <th style="background-color: green !important"> <a href='#' onClick="" style="color: white;; " >Add New</a></th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>Tushar Bhagat</td>
                        <td>Sales</td>
                        <td>9820677891</td>
                        <td>Alabc@yahoo.comvin</td>
                        <td>Abbas</td>                       
                       
                        </tr> 
                        </tbody>
                    </table> -->
                <!-- <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
				<div class="clonable" data-ss="1"> -->
                    <div class="row">
                        
                      <div class="col l11">
                        <div class="row">
                          <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label for="cus_name active" class="dopr_down_holder_label active">Name 
                            <span class="red-text">*</span></label>
                            <div  class="sub_val no-search">
                                <select  id="cus_name_sel" name="name_initial[]" class="select2 browser-default ">
                                 @foreach($initials as $ini)
                                    <option value="{{$ini->initials_id}}">{{ucfirst($ini->initials_name)}}</option>
                                  @endforeach
                                </select>
                            </div>
                              <input type="text" class="validate mobile_number" name="name_h[]" id="cus_name"  placeholder="Enter"  >
                          </div>

                          <div class="input-field col l3 m4 s12" id="InputsWrapper">
                              <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label>
                              <div  class="sub_val no-search">
                                  <select  id="country_code" name="country_code[]" class="select2 browser-default">
                                    @foreach($country_code as $cou)
                                       <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                    @endforeach
                                  </select>
                              </div>
                              <input type="text" class="validate mobile_number" name="mobile_number_h[]" id="mobile_number"  placeholder="Enter" >
                          </div>

                          <!-- <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <div  class="sub_val no-search">
                                <select  id="country_code1" name="country_code_w[]" class="select2 browser-default">
                                 @foreach($country_code as $cou)
                                    <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                  @endforeach
                                </select>
                            </div>
                            <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label>
                            <input type="text" class="validate mobile_number" name="wap_number[]" id="wap_number"  placeholder="Enter" >
                            <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                            </div>
                          </div>  -->

                          <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                            <label for="primary_email active" class="active">Email Address: <span class="red-text">*</span></label>
                            <input type="email" class="validate" name="primary_email_h[]" id="primary_email"  placeholder="Enter" >
                          </div>

                          <div class="input-field col l3 m4 s12 display_search">                    
                              <select class="select2  browser-default designation_h"  data-placeholder="Select" name="designation_h[]" id="designation_h">
                                  <option value="" disabled selected>Select</option>
                                  @foreach($designation as $des)
                                    <option value="{{$des->designation_id}}">{{ucfirst($des->designation_name)}}</option>
                                  @endforeach
                              </select>
                              <label for="possession_year" class="active">Designation </label>
                              <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                              <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                              </div>
                          </div>
                          
                        </div>
                         <!--  <div class="row">

                          <div class="input-field col l3 m4 s12 display_search">                    
                              <select class="select2  browser-default designation_h"  data-placeholder="Select" name="designation_h[]" id="designation_h">
                                  <option value="" disabled selected>Select</option>
                                  @foreach($designation as $des)
                                    <option value="{{$des->designation_id}}">{{ucfirst($des->designation_name)}}</option>
                                  @endforeach
                              </select>
                              <label for="possession_year" class="active">Designation </label>
                              <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                              <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                              </div>
                          </div>

                          <div class="input-field col l3 m4 s12 display_search">                    
                              <select class="select2  browser-default"  data-placeholder="Select" name="reporting_to_h[]">
                                  <option value="" disabled selected>Select</option>
                                  @foreach($company_hirarchy as $hei)
                                    <option value="{{$hei->company_hirarchy_id}}">{{ucfirst($hei->name)}}</option>
                                  @endforeach
                              </select>
                              <label for="possession_year" class="active">Reporting to </label>                           
                          </div>

                          <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                            <label  class="active">Comments: <span class="red-text">*</span></label>
                            <input type="text" name="comments_h[]"  id="input-file-now" placeholder="Enter">
                          </div>

                          <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Upload Photo : </label>
                            <input type="file" name="photo_h[]"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                          </div>
                        </div>  -->
                    
                      </div>
                      <div class="col l1">
                           <!-- <a href="#" id="hap" class="clonable-button-add" title="Add"   style="color: red !important"> <i class="material-icons  dp48">add</i></a> 
                           <a href="#" class="clonable-button-close" title="Remove"  style="color: red !important"> <i class="material-icons  dp48">remove</i></a>  -->
                              <!-- <div class="input-field col l1 m4 s12 display_search">                     -->
                              <!-- <button id="hap" class="clonable-button-add btn btn-primary" type="button">+</button>
                              <div class="col-md-1"><button type="button" style="display: block !important;" class="btn btn-danger clonable-button-close">-</button> -->
                              <div  class="AddMoreSocietyMasterH" >
                                          
                                          <a href="#" id="AddMoreFileBoxSocietyMasterH" class="waves-effect waves-light" style="color: grey !important;font-size: 23px;">+</a> 
                                          <a href="#" id="add_location" class="modal-trigger" title="Action"  style="color:  grey !important;font-size: 23px;"> <i class="material-icons  dp48">group</i></a>    
                                         <a href="#modal10" id="add_location" title="View Flow Chart" class="modal-trigger" style="color:  grey !important;font-size: 23px;"> <i class="material-icons  dp48">remove_red_eye</i></a>    
                                          </div> 
                                         
                                
                                  
                             </div>
                       
                    </div>
                <!-- </div>
                </div> -->
                <div class="" id="display_inputs_SocietyMasterH"></div>
                </div>

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action">Save</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <a href="#" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
                    </div>    
                </div> 
        </form>
        <br>
        
         
            

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
    <div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="errors" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>

    <div id="modal10" class="modal">
            <div class="modal-content">
            <h5 style="text-align: center"> Flow Chart</h5>
            
            <hr>
            
            <html>
            <head>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                google.charts.load('current', {packages:["orgchart"]});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Name');
                data.addColumn('string', 'Manager');
                data.addColumn('string', 'ToolTip');

                // For each orgchart box, provide the name, manager, and tooltip to show.
                data.addRows([
                    // [{'v':'Mike', 'f':'Mike'},
                    // '', ''],
                    // [{'v':'Jim', 'f':'Jim<div style="color:red; font-style:italic">Vice President</div>'},
                    // 'Mike', 'VP'],
                    ['Mike', '', ''],
                    ['Jim', 'Mike', ''],
                    ['Alice', 'Mike', ''],
                ['Prasanna', 'Alice', ''],
                ['Kiran', 'Prasanna', ''],
                ['ABC', 'Kiran', ''],
                ['XYZ', 'Kiran', ''],
                    ['Bob', 'Jim', 'Bob Sponge'],		  
                    ['Carol', 'Bob', ''],
                ['Prasanna', 'Carol', '']
                ]);

                // Create the chart.
                var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
                // Draw the chart, setting the allowHtml option to true for the tooltips.
                chart.draw(data, {'allowHtml':true});
                }
                </script>
                </head>
            <body>
                <div id="chart_div"></div>
            </body>
            </html>

            <br>
            <div class="row">
            <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
            </div>
            </div>
            <br>
            
    </div>

        <script src="https://code.jquery.com/jquery-3.4.0.slim.min.js"  crossorigin="anonymous"></script>
      <script src="{{ asset('app-assets/js/jquery.cloner.js') }}"></script>     


      <script>
function add_designation_form(){
    var url = 'addDesignationCompanyMaster';
    var form = 'add_designation_form';
    var err = 'print-error-msg_add_designation';
     var desig = $('#add_designation').val();
    // alert(desig);
    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:  {add_designation:desig}      ,             
            //  $('#'+form).serialize() ,
        
        success: function(data) {
            // console.log(data); return;
            var a = data.a;
            var option = a.option;
            var value = a.value; 

            var newOption= new Option(option,value, true, false);
            // Append it to the select
            $(".designation_h").append(newOption);//]].trigger('change');
            // $('#designation_h').append($('<option>').val(optionValue).text(optionText));


            // console.log(data.success);return;
            if($.isEmptyObject(data.error)){
               $('.success1').html(data.success);
               //  alert(data.success);
               //  location.reload();
            }else{
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
    // execute(url,form,err);
}
</script> 

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     