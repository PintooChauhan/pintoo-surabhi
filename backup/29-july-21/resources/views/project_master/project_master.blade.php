
<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}
.card .card-content {
    padding-top: 0px;
    min-height: auto !important;
}
      </style>
       <link href="{{ asset('app-assets/css/summernote/summernote-bs4.css') }} " rel="stylesheet">

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->

         

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

            

        <div class="row" style="font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
                    
                        <span class="userselect"> @if(isset($project_id)) UPDATE @else ADD @endif PROJECT MASTER</span>
                </div><br>
                
                @if(isset($project_id))
                    <form id="project_master_form_update" method="post"   enctype="multipart/form-data"> 
                @else
                    <form id="project_master_form" method="post"   enctype="multipart/form-data"> 
                @endif
                 @csrf
                <div class="row">
                       <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="group_name" name="group_name" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                  @foreach($group_name as $group_name)
                                    <option value="{{$group_name->company_id}}" @if(isset($group_id) && !empty($group_id) )  @if($group_name->company_id == $group_id ) selected  @endif  @endif>{{ ucwords($group_name->group_name)}} </option>
                                    @endforeach                            
                                </select>
                                <label class="active">Group Name:<span class="red-text">*</span></label>
                        </div>           

                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Complex Name: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="complex_name" @if(isset($project_complex_name)) value="{{$project_complex_name}}" @endif   placeholder="Enter"  >                             
                            
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="location" name="location" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    @foreach($location as $location)
                                    <option value="{{$location->location_id}}" @if(isset($location_id))  @if($location->location_id == $location_id)  selected @endif @endif >{{ ucwords($location->location_name)}}</option>
                                    @endforeach
                                </select>
                                <label class="active">Location: <span class="red-text">*</span></label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="sub_location" name="sub_location" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    @foreach($sub_location as $sub_location)
                                    <option value="{{$sub_location->sub_location_id}}" @if(isset($sub_location_id))  @if($sub_location->sub_location_id == $sub_location_id)  selected @endif @endif >{{ ucwords($sub_location->sub_location_name)}}</option>
                                    @endforeach                                 
                                </select>
                                <label class="active">Sub Location: <span class="red-text">*</span></label>
                        </div>
                    </div>
                    <div class="row">

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="city" name="city" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                   @foreach($city as $city)
                                    <option value="{{$city->city_id}}" @if(isset($city_id))  @if($city->city_id == $city_id)  selected @endif @endif >{{ ucwords($city->city_name)}}</option>
                                    @endforeach                                  
                                </select>
                                <label class="active">City: <span class="red-text">*</span></label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="state" name="state" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                     @foreach($state as $state)
                                    <option value="{{$state->dd_state_id}}" @if(isset($state_id))  @if($state->dd_state_id == $state_id)  selected @endif @endif >{{ ucwords($state->state_name)}}</option>
                                    @endforeach                                  
                                </select>
                                <label class="active">State: <span class="red-text">*</span></label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="category" name="category" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                     @foreach($category as $category)
                                    <option value="{{$category->category_id}}" @if(isset($category_id))  @if($category->category_id == $category_id)  selected @endif @endif >{{ ucwords($category->category_name)}}</option>
                                    @endforeach                                 
                                </select>
                                <label class="active">Category : <span class="red-text">*</span></label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="category_type" name="unit_status_of_complex" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>                                    
                                     @foreach($unit_status as $unit_status)
                                    <option value="{{$unit_status->unit_status_id}}" @if(isset($unit_status_of_complex))  @if($unit_status->unit_status_id == $unit_status_of_complex)  selected @endif @endif >{{ ucwords($unit_status->unit_status)}}</option>
                                    @endforeach                                                            
                                </select>
                                <label class="active">Unit Status of Complex :<span class="red-text">*</span></label>
                        </div>
                            

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Project Land Parcel: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="project_land_parcel"   placeholder="Enter" @if(isset($complex_land_parcel)) value="{{$complex_land_parcel}}" @endif  >
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                             <label class="active"> Company Name: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="company_name"   placeholder="Enter" @if(isset($company_name)) value="{{$company_name}}" @endif >
                        </div>


                        <div class="input-field col l6 m4 s12" id="InputsWrapper2">                        
                             <label  class="active">Sales Office Address: <span class="red-text">*</span></label>
                             <input type="text" class="validate" name="sales_office_address"   placeholder="Enter" @if(isset($sales_office_address)) value="{{$sales_office_address}}" @endif >
                            <!-- <textarea style="border-color: #5b73e8; padding-top: 12px;width:104% !important" placeholder="Enter" name="sales_office_address" rows='20' col='40'></textarea> -->
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="project_rating" name="project_rating" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                   @foreach($rating as $rating)
                                    <option value="{{$rating->rating_id}}"  @if(isset($complex_rating))  @if($rating->rating_id == $complex_rating)  selected @endif @endif >{{ ucwords($rating->rating)}}</option>
                                    @endforeach                                 
                                </select>
                                <label class="active">Project rating</label>
                        </div>

                        <!-- <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Funded by: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="funded_by"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Building Architecture: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="building_arch"   placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Landscape Architect: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="landscape_arch"   placeholder="Enter" >
                        </div> 

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">RCC Consultant: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="rcc_consultant"   placeholder="Enter" >
                        </div> 

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Solicitor / Advocate: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="solicitor"   placeholder="Enter" >
                        </div>   

                        <div class="input-field col l3 m4 s12 display_search">                                
                                <select  id="construction_tech" name="construction_tech" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    <option value="1" >option 1</option>                                    
                                    <option value="2" >option 2</option>                                   
                                </select>
                                <label class="active">Construction Technology</label>
                        </div> --> 
                    
                       


                </div>

              

                <!-- put in below row html editor  -->
                 <div class="row">
           
                        <section class="full-editor">
                            <div class="row">
                                <div class="col s12">
                              
                                    <div class="card">
                                    <br>
                                        <div class="card-content">
                                        Complex Description

                                           
                                            <div class="row">
                                                <div class="col s12">
                                               
                                                    <div id="full-wrapper" name="html_editor2">
                                                        <div id="full-container" name="html_editor1">
                                                        <textarea style="border-color: #5b73e8; padding-top: 12px;width: 1238px; height: 46px;" placeholder="Enter" name="complex_description"  > @if(isset($complex_description)) {{$complex_description}} @endif</textarea>
                                                            <!-- <textarea class="editor" > --> 
                                                            <!-- <div class="editor" name="html_editor">
                                                                
                                                                
                                                            </div> complex_description -->
                                                            <!-- </textarea> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

    <script>

    </script>


                <div class="row">

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Upload Photos : </label>
                            <input type="file" name="upload_photos[]"  id="input-file-now" class="dropify" multiple='true'  data-default-file="" style="padding-top: 14px;">
                            
                            @if(isset($getProjectPhotoData) && !empty($getProjectPhotoData))
                            @php $i=1; @endphp 
                                @foreach($getProjectPhotoData as $getProjectPhotoData)
                                     @if($getProjectPhotoData->meta_key == 'upload_photos')
                                     <a href="{{ asset('complex/'.$getProjectPhotoData->url) }}" target="_blank"> Uploaded Photo {{$i}} </a>
                                     <!-- <input type="text" value="{{$getProjectPhotoData->url}}" name="upload_photos1[]"> -->
                                     <br>
                                     @php $i++; @endphp 
                                    @endif
                                @endforeach
                            @endif
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">E Brochure : </label>
                            <input type="file" name="e_brochure[]"  id="input-file-now"  class="dropify"  multiple='true'  data-default-file="" style="padding-top: 14px;">
                            @if(isset($e_brochure) && !empty($e_brochure))
                            @php $e_brochure = json_decode($e_brochure); $i=1; @endphp 
                            @foreach($e_brochure as $e_brochure)
                                <a href="{{ asset('complex/'.$e_brochure) }}" target="_blank"> E Brochure {{$i}}</a>
                                <br>
                               @php $i++; @endphp 
                            @endforeach    
                            @endif
                            
                              
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Upload Video : </label>
                            <input type="file" name="upload_video[]"  id="input-file-now" class="dropify"  multiple='true'  data-default-file="" style="padding-top: 14px;">
                            @if(isset($getProjectPhotoData1) && !empty($getProjectPhotoData1))
                            @php $i=1; @endphp 
                                @foreach($getProjectPhotoData1 as $getProjectPhotoData1)
                                     @if($getProjectPhotoData1->meta_key == 'upload_video')
                                     <a href="{{ asset('complex/'.$getProjectPhotoData1->url) }}" target="_blank"> Uploaded Video {{$i}} </a>
                                     <!-- <input type="text" value="{{$getProjectPhotoData->url}}" name="upload_photos1[]"> -->
                                     <br>
                                     @php $i++; @endphp 
                                    @endif
                                @endforeach
                            @endif
                        </div>


                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Add Video Link: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="video_link"   placeholder="Enter"  @if(isset($video_links)) value="{{$video_links}}" @endif>
                        </div> 
                </div>

                <br><div class="row"><lable style="margin-left: 15px;font-weight: 700;">Complex Amenities</lable></div>
                <div class="row"> 
            @if(isset($getProjectAmenitiesData) && !empty($getProjectAmenitiesData))     
                @foreach($getProjectAmenitiesData as $getProjectAmenitiesData)
                    @foreach($project_amenities as $ame)     
                        @if($getProjectAmenitiesData->amenities_id == $ame->project_amenities_id) 
                            <div class="col l6 m6 s12">
                                <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">{{ucfirst($ame->amenities)}}</div>
                                <div class="col l3 m3 s12">
                                    <label><input type="checkbox" name="is_available[]" value="{{$ame->project_amenities_id}}" checked ><span>Available</span></label>
                                </div>
                                <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">
                                    <label  class="active">Description: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="description_ame[]" value="{{$getProjectAmenitiesData->description}}"   placeholder="Enter" style="height: 36px;" >
                                </div>
                            
                            </div>  
                            @else

                            <!-- <div class="col l6 m6 s12">
                                <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">{{ucfirst($ame->amenities)}}</div>
                                <div class="col l3 m3 s12">
                                    <label><input type="checkbox" name="is_available[]" value="{{$ame->project_amenities_id}}" ><span>Available</span></label>
                                </div>
                                <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">
                                    <label  class="active">Description: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="description_ame[]"   placeholder="Enter" style="height: 36px;" >
                                </div>                            
                            </div>                       -->
                          @endif    
                    @endforeach
                
                @endforeach
            @else
                 @foreach($project_amenities as $ame) 
                    <div class="col l6 m6 s12">
                        <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">{{ucfirst($ame->amenities)}}</div>
                        <div class="col l3 m3 s12">
                            <label><input type="checkbox" name="is_available[]" value="{{$ame->project_amenities_id}}" ><span>Available</span></label>
                        </div>
                        <div class="col l3 m3 s12 input-field col l3 m4 s12 display_search">
                            <label  class="active">Description: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="description_ame[]"   placeholder="Enter" style="height: 36px;" >
                        </div>
                    
                    </div> 

                 @endforeach

            @endif

                    
                </div>
                  <br>  

                <div class="row">
                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">                    
                        <label  class="active">Pros of Complex: <span class="red-text">*</span></label>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="pros_of_complex" rows='20' col='40'></textarea> -->
                        <input type="text" class="validate" name="pros_of_complex"   placeholder="Enter" >
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">                    
                        <label  class="active">Cons of Complex: <span class="red-text">*</span></label>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="cons_of_complex" rows='20' col='40'></textarea> -->
                        <input type="text" class="validate" name="cons_of_complex"   placeholder="Enter" >
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="number" class="input_select_size" name="complex_land_parcel" id="complex_land_parcel"  placeholder="Enter" @if(isset($complex_land_parcel)) value="{{$complex_land_parcel}}" @endif >
                                <label>Complex Land Parcel
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="carpet_area_unit" name="complex_land_parcel_unit" class="select2 browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="number" class="input_select_size" name="complex_open_space" id="open_space"  placeholder="Enter" @if(isset($complex_open_space)) value="{{$complex_open_space}}" @endif>
                                <label> Open space
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="carpet_area_unit" name="open_space_unit" class="select2 browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="row">                

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">GST No.: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="gst_no"    placeholder="Enter" @if(isset($gst_no)) value="{{$gst_no}}" @endif>
                    </div>
                    <div class="input-field col l3 m4 s12 display_search">
                        <label  class="active">GST certificate : </label>
                        <input type="file" name="gst_certificate"  id="input-file-now" class="dropify" multiple='true'  data-default-file="" style="padding-top: 14px;">
                    </div>
                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">  
                        <label  class="active">Comments: <span class="red-text">*</span></label>
                        <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea> -->
                        <input type="text" class="validate" name="comments"   placeholder="Enter" @if(isset($comments)) value="{{$comments}}" @endif >
                    </div>  

                </div>  
                
                <div class="row">
                    <!-- <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Name </th>
                        <th>Designation</th>
                        <th>Mobile No.</th>
                        <th>Email Id</th>
                        <th>Reports to</th>
                        <th style="background-color: green !important"> <a href='#' onClick="" style="color: white;; " >Add New</a></th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>Tushar Bhagat</td>
                        <td>Sales</td>
                        <td>9820677891</td>
                        <td>Alabc@yahoo.comvin</td>
                        <td>Abbas</td>                       
                       
                        </tr> 
                        </tbody>
                    </table> -->
                <!-- <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
				<div class="clonable" data-ss="1"> -->
                    <hr>
                    <div class="row">                        
                        <div class="col l11">
                            <div class="row">
                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <label for="cus_name active" class="dopr_down_holder_label active">Name 
                                <span class="red-text">*</span></label>
                                <div  class="sub_val no-search">
                                    <select  id="cus_name_sel" name="name_initial[]" class="select2 browser-default ">
                                    @foreach($initials as $ini)
                                        <option value="{{$ini->initials_id}}">{{ucfirst($ini->initials_name)}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <input type="text" class="validate mobile_number" name="name_h[]" id="cus_name"  placeholder="Enter"  >
                            </div>

                            <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label>
                                <div  class="sub_val no-search">
                                    <select  id="country_code" name="country_code[]" class="select2 browser-default">
                                        @foreach($country_code as $cou)
                                        <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="text" class="validate mobile_number" name="mobile_number_h[]" id="mobile_number"  placeholder="Enter" >
                            </div>

                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                <div  class="sub_val no-search">
                                    <select  id="country_code1" name="country_code_w[]" class="select2 browser-default">
                                    @foreach($country_code as $cou)
                                        <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label>
                                <input type="text" class="validate mobile_number" name="wap_number[]" id="wap_number"  placeholder="Enter" >
                                <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                    <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                                </div>
                            </div> 

                            <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                                <label for="primary_email active" class="active">Email Address: <span class="red-text">*</span></label>
                                <input type="email" class="validate" name="primary_email_h[]" id="primary_email"  placeholder="Enter" >
                            </div>
                            </div>
                            <div class="row">
                            <div class="input-field col l3 m4 s12 display_search">                    
                                <select class="select2  browser-default designation_h"  data-placeholder="Select" name="designation_h[]" id="designation_h">
                                    <option value="" disabled selected>Select</option>
                                    @foreach($designation as $des)
                                        <option value="{{$des->designation_id}}">{{ucfirst($des->designation_name)}}</option>
                                    @endforeach
                                </select>
                                <label for="possession_year" class="active">Designation </label>
                                <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                                <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                                </div>
                            </div>

                            <div class="input-field col l3 m4 s12 display_search">                    
                                <select class="select2  browser-default"  data-placeholder="Select" name="reporting_to_h[]">
                                    <option value="" disabled selected>Select</option>
                                    @foreach($company_hirarchy1 as $hei)
                                        <option value="{{$hei->project_hirarchy_id}}">{{ucfirst($hei->name)}}</option>
                                    @endforeach
                                </select>
                                <label for="possession_year" class="active">Reporting to </label>                           
                            </div>

                            <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                                <label  class="active">Comments: <span class="red-text">*</span></label>
                                <input type="text" name="comments_h[]"  id="input-file-now" placeholder="Enter">
                            </div>

                            <div class="input-field col l3 m4 s12 display_search">
                                <label  class="active">Upload Photo : </label>
                                <input type="file" name="photo_h[]"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                            </div>
                            </div>
                                
                        </div>
                        <div class="col l1">                          
                             <div  class="AddMoreCompanyMasterH" >                                          
                                <a href="#" id="AddMoreFileBoxCompanyMasterH" class="waves-effect waves-light" style="color: grey !important;font-size: 23px;">+</a> 
                            </div> 
                                <br>                       
                                  <a href="#modal10" id="add_location" title="View Flow Chart" class="modal-trigger" style="color:  grey !important;font-size: 23px;"> <i class="material-icons  dp48">remove_red_eye</i></a>    
                        </div>                      
                    </div>

                    @if(isset($getProjecthirarchyData))
                        @php $i=1; @endphp
                        @foreach($getProjecthirarchyData as $getProjecthirarchyData)
                        <div class="row">                        
                            <div class="col l11">
                                <div class="row">
                                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                    <label for="cus_name active" class="dopr_down_holder_label active">Name 
                                    <span class="red-text">*</span></label>
                                    <div  class="sub_val no-search">
                                        <select  id="name_initial_{{$i}}" name="name_initial[]" class="select2 browser-default ">
                                        @foreach($initials as $ini)
                                            <option value="{{$ini->initials_id}}">{{ucfirst($ini->initials_name)}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <input type="text" class="validate mobile_number" name="name_h[]" id="cus_name"  placeholder="Enter" @if(isset( $getProjecthirarchyData->name )) value="{{  $getProjecthirarchyData->name }}" @endif  >
                                </div>

                                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                    <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label>
                                    <div  class="sub_val no-search">
                                        <select  id="country_code_{{$i}}" name="country_code[]" class="select2 browser-default">
                                            @foreach($country_code as $cou)
                                            <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="text" class="validate mobile_number" name="mobile_number_h[]" id="mobile_number"  placeholder="Enter"  @if(isset( $getProjecthirarchyData->mobile_number )) value="{{ $getProjecthirarchyData->mobile_number }}" @endif >
                                </div>

                                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                    <div  class="sub_val no-search">
                                        <select  id="country_code1_{{$i}}" name="country_code_w[]" class="select2 browser-default">
                                        @foreach($country_code as $cou)
                                            <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label>
                                    <input type="text" class="validate mobile_number" name="wap_number[]" id="wap_number"  placeholder="Enter"  @if(isset( $getProjecthirarchyData->whatsapp_number )) value="{{ $getProjecthirarchyData->whatsapp_number }}" @endif >
                                    <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                        <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                                    </div>
                                </div> 

                                <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                                    <label for="primary_email active" class="active">Email Address: <span class="red-text">*</span></label>
                                    <input type="email" class="validate" name="primary_email_h[]" id="primary_email"  placeholder="Enter" @if(isset( $getProjecthirarchyData->email_id )) value="{{ $getProjecthirarchyData->email_id }}" @endif >
                                </div>
                                </div>
                                <div class="row">
                                <div class="input-field col l3 m4 s12 display_search">                    
                                    <select class="select2  browser-default designation_h"  data-placeholder="Select" name="designation_h[]" id="designation_h_{{$i}}">
                                        <option value="" disabled selected>Select</option>
                                        @foreach($designation as $des)
                                            <option value="{{$des->designation_id}}" @if($des->designation_id == $getProjecthirarchyData->designation_id ) selected @endif >{{ucfirst($des->designation_name)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="possession_year" class="active">Designation </label>
                                    <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                                    <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                                    </div>
                                </div>

                                <div class="input-field col l3 m4 s12 display_search">                    
                                    <select class="select2  browser-default"  data-placeholder="Select" name="reporting_to_h[]">
                                        <option value="" disabled selected>Select</option>
                                        @foreach($company_hirarchy as $hei)
                                            <option value="{{$hei->project_hirarchy_id}}"  @if($hei->project_hirarchy_id == $getProjecthirarchyData->reports_to ) selected @endif>{{ucfirst($hei->name)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="possession_year" class="active">Reporting to </label>                           
                                </div>

                                <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                                    <label  class="active">Comments: <span class="red-text">*</span></label>
                                    <input type="text" name="comments_h[]"  id="input-file-now" placeholder="Enter" @if(isset( $getProjecthirarchyData->comments )) value="{{ $getProjecthirarchyData->comments }}" @endif>
                                </div>

                                <div class="input-field col l3 m4 s12 display_search">
                                    <label  class="active">Upload Photo : </label>
                                    <input type="file" name="photo_h[]"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                                </div>
                                </div>
                                    
                            </div>
                            <div class="col l1">                          
                                <!-- <div  class="AddMoreCompanyMasterH" >                                           -->
                                    <!-- <a href="#" id="AddMoreFileBoxCompanyMasterH" class="waves-effect waves-light" style="color: grey !important;font-size: 23px;">+</a>  -->
                                    <a href="#" style="color: red !important;font-size: 23px" class="removeclassCompany waves-effect waves-light">-</a>
                                <!-- </div>  -->
                                    <br>                       
                                    <a href="#modal10" id="add_location" title="View Flow Chart" class="modal-trigger" style="color:  grey !important;font-size: 23px;"> <i class="material-icons  dp48">remove_red_eye</i></a>    
                            </div>                      
                        </div>
                        @php $i++; @endphp
                        @endforeach
                    @endif
                <!-- </div>
                </div> -->
                <div class="" id="display_inputs_CompanyMasterH"></div>
                </div>
        </div>
        




   

        <div class="row">   <div class="input-field col l6 m6 s6 display_search">
            <div class="alert alert-danger print-error-msg" style="display:none;color:red">
            <ul></ul>
            </div>
         </div> 
     </div> 

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action"> @if(isset($project_id)) Update @else Save @endif</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <a href="#" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
                    </div>    
                </div> 

        <br>
        
         
            

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->

    <div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="errors" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
    </div>

    <div id="modal10" class="modal">
            <div class="modal-content">
            <h5 style="text-align: center"> Flow Chart</h5>
            
            <hr>
            
            <html>
            <head>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                google.charts.load('current', {packages:["orgchart"]});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Name');
                data.addColumn('string', 'Manager');
                data.addColumn('string', 'ToolTip');

                // For each orgchart box, provide the name, manager, and tooltip to show.
                data.addRows([
                    // [{'v':'Mike', 'f':'Mike'},
                    // '', ''],
                    // [{'v':'Jim', 'f':'Jim<div style="color:red; font-style:italic">Vice President</div>'},
                    // 'Mike', 'VP'],
                    ['Mike', '', ''],
                    ['Jim', 'Mike', ''],
                    ['Alice', 'Mike', ''],
                ['Prasanna', 'Alice', ''],
                ['Kiran', 'Prasanna', ''],
                ['ABC', 'Kiran', ''],
                ['XYZ', 'Kiran', ''],
                    ['Bob', 'Jim', 'Bob Sponge'],		  
                    ['Carol', 'Bob', ''],
                ['Prasanna', 'Carol', '']
                ]);

                // Create the chart.
                var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
                // Draw the chart, setting the allowHtml option to true for the tooltips.
                chart.draw(data, {'allowHtml':true});
                }
                </script>
                </head>
            <body>
                <div id="chart_div"></div>
            </body>
            </html>

            <br>
            <div class="row">
            <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
            </div>
            </div>
            <br>
            
    </div>

<script>
function add_designation_form(){
    var url = 'addDesignationCompanyMaster';
    var form = 'add_designation_form';
    var err = 'print-error-msg_add_designation';
     var desig = $('#add_designation').val();
    // alert(desig);
    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:  {add_designation:desig}      ,             
            //  $('#'+form).serialize() ,
        
        success: function(data) {
            // console.log(data); return;
            var a = data.a;
            var option = a.option;
            var value = a.value; 

            var newOption= new Option(option,value, true, false);
            // Append it to the select
            $(".designation_h").append(newOption);//]].trigger('change');
            // $('#designation_h').append($('<option>').val(optionValue).text(optionText));


            // console.log(data.success);return;
            if($.isEmptyObject(data.error)){
               $('.success1').html(data.success);
               //  alert(data.success);
               //  location.reload();
            }else{
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
    // execute(url,form,err);
}
</script> 
        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     