<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
<style>
.collapsible-body{
    border-bottom: 0px !important;
}
</style>

      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->

      <!-- BEGIN: Page Main-->
      <div id="main">
         <div class="row">
            
            <div class="col s12">
               <div class="container">
                  <div class="section section-form-wizard">
                  
                     <!-- Horizontal Stepper -->
                     <div class="row mt-3">
                        <div class="col s12">
                           <div class="card">
                              <div class="card-content pb-0">
                                 <ul class="stepper horizontal">
                                    <li class="step active">
                                       <div class="step-title waves-effect">Lead Details</div>
                                       <div class="step-content">
                                        <div class="row">                                        
                                            <div class="col l3">
                                                <ul class="collapsible">
                                                    <li onClick="lead_info()">
                                                    <div class="collapsible-header" id="col_lead_info" style="background-color:orange"><i class="material-icons">filter_drama</i>Lead Info</div>
                                                    
                                                    </li>                                        
                                                </ul>
                                            </div>

                                            <div class="col l3">
                                                <ul class="collapsible">
                                                    <li onClick="customer_name()">
                                                    <div class="collapsible-header" id="col_customer_name"><i class="material-icons">person</i>Customer Info</div>
                                                    
                                                    </li>                                        
                                                </ul>
                                            </div>
                                        </div>    
                                            

                                            
                                        <div class="row">        
                                         <div class="collapsible-body"  id='lead_info' style="display:block">
                                            <div class="input-field col l3 m4 s12">
                                                <select class="select2 browser-default" id="leadby" required data-minimum-results-for-search="Infinity">
                                                    <option value="" disabled selected>Select</option>
                                                    <option value="sr" selected>SR</option>
                                                    <option value="pc">PC</option>
                                                </select>
                                                <label for="leadby" class="active">Lead by</label>
                                            </div>

                                            <div class="input-field col l3 m4 s12">
                                                <select  id="leadtype" name="leadtype" class="select2 browser-default validate" data-minimum-results-for-search="Infinity" required>
                                                    <option value="" disabled selected>Select</option>
                                                    <option value="buyer">Buyer</option>
                                                    <option value="tenant">Tenant</option>
                                                    
                                                </select>
                                                <label for="leadtype" class="active">Lead type: <span class="red-text">*</span></label>
                                            </div>

                                            <div class="input-field col l3 m4 s12 display_search">
                                                <select class="select2  browser-default" multiple="multiple" id="leadtype3" data-placeholder="Company name, Branch name" name="leadtype3">
                                                    <option value="option 1">Surabhi Realtors, Brahmand</option>
                                                    <option value="option 1">Surabhi Realtors, Majiwada</option>
                                                    <option value="option 1">Surabhi Realtors, Kalyan</option>
                                                    <option value="option 1">Surabhi Realtors, Thane</option>
                                                </select>
                                                <label for="leadtype3" class="active">PC Company Name 
                                                </label>                                             
                                            </div>

                                            <div class="input-field col l3 m4 s12 display_search">
                                                <!--   <label for="lead_assign">Lead assign: <span class="red-text">*</span></label>
                                                    <input type="text" class="validate" name="lead_assign" id="lead_assign" required> -->
                                                <select  multiple="multiple"  id="leadtype41" name="lead_assignx" class="validate select2 browser-default">
                                                    <option value="option 1">Niyaz, 9323256381</option>
                                                    <option value="option 2">Suraj, 7819326589</option>
                                                    <option value="option 3">Lucky, 8289653263</option>
                                                </select>
                                                <label id="leadtype41"  class="active">PC Executive Name </label>
                                            </div>

                                            <div class="input-field col l3 m4 s12 display_search">
                                                <select class="select2  browser-default"  id="leadtype5" data-placeholder="Select" name="leadtype5">
                                                    <option value="" disabled selected>Select</option>
                                                    <option value="option 2">Magicbricks</option>
                                                    <option value="option 3">99 acres</option>                                                     
                                                </select>
                                                <label for="leadtype5" class="active">Lead Source Name
                                                </label>                                             
                                            </div>                                            

                                            <div class="input-field col l3 m4 s12 display_search">
                                                <!--   <label for="lead_assign">Lead assign: <span class="red-text">*</span></label>
                                                    <input type="text" class="validate" name="lead_assign" id="lead_assign" required> -->
                                                <select  id="lead_assign" name="lead_assign" class="validate select2 browser-default">
                                                <option value="" disabled selected>Society, Complex, Configuration, Carpet Area</option>
                                                    <option value="1">Option 1</option>
                                                    <option value="2">Option 2</option>
                                                    <option value="3">Option 3</option>
                                                </select>
                                                <label class="active">Lead Reached From</label>
                                            </div>

                                            <div class="input-field col l3 m4 s12 display_search">
                                                <select class="select2  browser-default"  id="leadtype7" data-placeholder="Select" name="leadtype7">
                                                    <option value="" disabled selected>Select</option>
                                                    <option value="option 2">Suraj Pawar</option>
                                                    <option value="option 3">Arun Jadhav</option>
                                                    
                                                </select>
                                                <label for="leadtype7" class="active">Lead Assigned Name
                                                </label>                                             
                                            </div>

                                            <div class="input-field col l3 m4 s12 display_search">
                                                    <label for="lead_assign" class="active">Lead code: <span class="red-text">*</span></label>
                                                    <input type="text" class="validate" name="lead_assign" Readonly  placeholder="Lead code"  value="B001">
                                            </div>
                                         </div>
                                         <div class="collapsible-body" id='customer_name' style="display:none">
                                                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                                    <label for="cus_name active" class="dopr_down_holder_label active">Customer Info 
                                                   <span class="red-text">*</span></label>
                                                   <div  class="sub_val no-search">
                                                      <select  id="cus_name_sel" name="cus_name_sel" class="select2 browser-default ">
                                                         <option value="1">Mr.</option>
                                                         <option value="2">Miss.</option>
                                                         <option value="3">Mrs.</option>
                                                      </select>
                                                   </div>
                                                      <input type="text" class="validate mobile_number" name="contactNum" id="cus_name" required placeholder="Customer Name"  >
                                                </div>


                                                <div class="input-field col l3 m4 s12" id="InputsWrapper">
                                                   <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label>
                                                   <div  class="sub_val no-search">
                                                      <select  id="country_code" name="country_code" class="select2 browser-default">
                                                         <option value="1">+91</option>
                                                         <option value="2">+1</option>
                                                         <option value="3">+3</option>
                                                      </select>
                                                   </div>
                                                   <input type="text" class="validate mobile_number" name="contactNum" id="contactNum1" required placeholder="Mobile Number" >
                                                   <div id="AddMoreFileId" class="addbtn">
                                                      <a href="#" id="AddMoreFileBox" class="">+</a> 
                                                   </div>
                                                </div>
                                                <span class="" id="display_inputs"></span>

                                                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                                                   <div  class="sub_val no-search">
                                                      <select  id="country_code1" name="country_code1" class="select2 browser-default">
                                                         <option value="1">+91</option>
                                                         <option value="2">+1</option>
                                                         <option value="3">+3</option>
                                                      </select>
                                                   </div>
                                                   <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate mobile_number" name="wap_number" id="wap_number" required placeholder="Whatsapp Number" >
                                                </div> 

                                                <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                                                   <label for="primary_email active" class="active">Primary Email: <span class="red-text">*</span></label>
                                                   <input type="email" class="validate" name="primary_email" id="primary_email" required placeholder="Primary Email" >
                                                   <div id="AddMoreFileId3" class="addbtn">
                                                      <a href="#" id="AddMoreFileBox3" class="">+</a> 
                                                   </div>
                                                </div>
                                                <div class="" id="display_inputs3"></div>    
                                                
                                         </div>
                                         
                                      </div>     <br>                                   
                                      <div class="row">
                                            <div class="col m12 s12 mb-3 text-right">
                                                <button class="red btn btn-reset" type="reset">
                                                <i class="material-icons left">clear</i>Reset
                                                </button>
                                                <button class="btn btn-light previous-step" disabled>
                                                <i class="material-icons left">arrow_back</i>
                                                Prev
                                                </button>
                                                <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                <i class="material-icons right">arrow_forward</i></button>
                                            </div>
                                        </div>



                                       
                                    </li>
                                    <li class="step ">
                                       <div class="step-title waves-effect">Requirment Details</div>
                                       <div class="step-content">
                                        <div class="row">                                        
                                                <div class="col l3">
                                                    <ul class="collapsible">
                                                        <li onClick="category()">
                                                        <div class="collapsible-header" id="col_category"><i class="material-icons">filter_drama</i>Category</div>
                                                        
                                                        </li>                                        
                                                    </ul>
                                                </div>

                                                <div class="col l3">
                                                    <ul class="collapsible">
                                                        <li onClick="location1()">
                                                        <div class="collapsible-header" id='col_location1'><i class="material-icons">person</i>location</div>
                                                        
                                                        </li>                                        
                                                    </ul>
                                                </div>

                                                
                                                <div class="col l3">
                                                    <ul class="collapsible">
                                                        <li onClick="unit()">
                                                        <div class="collapsible-header"><i class="material-icons">person</i>Unit</div>
                                                        
                                                        </li>                                        
                                                    </ul>
                                                </div>

                                                
                                                <div class="col l3">
                                                    <ul class="collapsible">
                                                        <li onClick="purpose()">
                                                        <div class="collapsible-header"><i class="material-icons">person</i>Purpose</div>
                                                        
                                                        </li>                                        
                                                    </ul>
                                                </div>
                                            </div> 

                                            <div class="row">        
                                                <div class="collapsible-body"  id='category' style="display:none">
                                                                                    
                                                       <div class="row" >
                                                            <div class="input-field col l3 m4 s12 display_search">
                                                            
                                                                <select  id="category" name="lead_assign" class="validate select2 browser-default">
                                                                <option value="" disabled selected>Select</option>
                                                                    <option value="1">Option 1</option>
                                                                    <option value="2">Option 2</option>
                                                                    <option value="3">Option 3</option>
                                                                </select>
                                                                <label class="active">Category</label>
                                                            </div>

                                                            <div class="input-field col l3 m4 s12 display_search">
                                                            
                                                                <select  id="category_type" name="lead_assign" class="validate select2 browser-default">
                                                                <option value="" disabled selected>Select</option>
                                                                    <option value="1">Option 1</option>
                                                                    <option value="2">Option 2</option>
                                                                    <option value="3">Option 3</option>
                                                                </select>
                                                                <label class="active">Category Type</label>
                                                            </div>
                                                        </div>



                                                </div>

                                                <div class="collapsible-body"  id='location' style="display:none">
                                                                                    
                                                    <div class="row" >
                                                        <div class="input-field col l3 m4 s12 display_search">
                                                        
                                                            <select  id="city" name="lead_assign" class="validate select2 browser-default">
                                                            <option value="" disabled selected>Select</option>
                                                                <option value="1">Option 1</option>
                                                                <option value="2">Option 2</option>
                                                                <option value="3">Option 3</option>
                                                            </select>
                                                            <label class="active">City</label>
                                                        </div>

                                                        <div class="input-field col l3 m4 s12 display_search">
                                                        
                                                            <select  id="state" name="lead_assign" class="validate select2 browser-default">
                                                            <option value="" disabled selected>Select</option>
                                                                <option value="1">Option 1</option>
                                                                <option value="2">Option 2</option>
                                                                <option value="3">Option 3</option>
                                                            </select>
                                                            <label class="active">state</label>
                                                        </div>
                                                    </div>
                                                        <style>
div label input {
   margin-right:100px;
}
body {
    font-family:sans-serif;
}

#ck-button {
    margin:4px;
    background-color:#EFEFEF;
    border-radius:4px;
    border:1px solid #D0D0D0;
    overflow:auto;
    float:left;
}

#ck-button label {
    float:left;
    width:auto;
}

#ck-button label span {
    text-align:center;
    padding:3px 0px;
    display:block;
}

#ck-button label input {
    position:absolute;
    top:-20px;
}

#ck-button input:checked + span {
    background-color:#911;
    color:#fff;
}
[type='checkbox'] + span:not(.lever):before, [type='checkbox']:not(.filled-in) + span:not(.lever):after{
    z-index: -1 !important;
}

</style>
                                                        <div class="row" >
                                                           <div class='col l4' >
                                                           <div><b> Locations</b> </div>
                                                            <div id="ck-button">
                                                                <label>
                                                                    <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Location Thane</span>
                                                                </label>
                                                            </div>     
                                                            </div>
                                                            
                                                            <div class='col l8' >
                                                            <div><b>Sub Locations</b> </div>
                                                            <div id="ck-button">
                                                                <label>
                                                                    <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Waghbill</span>
                                                                </label>
                                                            </div>
                                                            <div id="ck-button">
                                                                <label>
                                                                    <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Anand Nagar</span>
                                                                </label>
                                                            </div>
                                                            </div>
                                                        </div>



                                                        <div class="row" >
                                                           <div class='col l4' >
                                                           <div><b> Locations</b> </div>
                                                            <div id="ck-button">
                                                                <label>
                                                                    <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Location Thane</span>
                                                                </label>
                                                            </div>     
                                                            </div>
                                                            
                                                            <div class='col l8' >
                                                            <div><b>Sub Locations</b> </div>
                                                            <div id="ck-button">
                                                                <label>
                                                                    <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Waghbill</span>
                                                                </label>
                                                            </div>
                                                            <div id="ck-button">
                                                                <label>
                                                                    <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">Anand Nagar</span>
                                                                </label>
                                                            </div>
                                                            </div>
                                                        </div>




                                                </div>
                                            </div>

                                            <div class="row">                                        
                                                <div class="col l3">
                                                    <ul class="collapsible">
                                                        <li onClick="category()">
                                                        <div class="collapsible-header" id="col_category"><i class="material-icons">filter_drama</i>Category</div>
                                                        
                                                        </li>                                        
                                                    </ul>
                                                </div>

                                                <div class="col l3">
                                                    <ul class="collapsible">
                                                        <li onClick="location1()">
                                                        <div class="collapsible-header" id='col_location1'><i class="material-icons">person</i>location</div>
                                                        
                                                        </li>                                        
                                                    </ul>
                                                </div>

                                                
                                                <div class="col l3">
                                                    <ul class="collapsible">
                                                        <li onClick="unit()">
                                                        <div class="collapsible-header"><i class="material-icons">person</i>Unit</div>
                                                        
                                                        </li>                                        
                                                    </ul>
                                                </div>

                                                
                                                <div class="col l3">
                                                    <ul class="collapsible">
                                                        <li onClick="purpose()">
                                                        <div class="collapsible-header"><i class="material-icons">person</i>Purpose</div>
                                                        
                                                        </li>                                        
                                                    </ul>
                                                </div>
                                            </div>  
                                       </div>
                                    </li>
                                    <li class="step ">
                                       <div class="step-title waves-effect">Internal Details</div>
                                       <div class="step-content">
                                          <form id="form" class="col s12" novalidate="novalidate">
                                             <div class="row">
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select class="select2 browser-default" id="leadby13" required data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadby13" class="active">Heading</label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_assign1f2" class="active">Heading: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate" name="lead_assign1f2" id="lead_assign23" required placeholder="Heading"  >
                                                </div>
                                             </div>
                                             <div class=" ">
                                                <div class="row">
                                                   <div class="col m12 s12 mb-3 text-right">
                                                      <button class="red btn btn-reset" type="button">
                                                      <i class="material-icons left">clear</i>Reset
                                                      </button>
                                                      <button class="btn btn-light previous-step" disabled>
                                                      <i class="material-icons left">arrow_back</i>
                                                      Prev
                                                      </button>
                                                      <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                      <i class="material-icons right">arrow_forward</i></button>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </li>
                                    <li class="step ">
                                       <div class="step-title waves-effect">Property Detail</div>
                                       <div class="step-content">
                                          <form id="form" class="col s12" novalidate="novalidate">
                                             <div class="row">
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select class="select2 browser-default" id="leadbyfdg" required data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadbyfdg" class="active">Heading</label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_assigner23" class="active">Heading: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate" name="lead_assigner23" id="lead_assign23" required placeholder="Heading"  >
                                                </div>
                                             </div>
                                             <div class=" ">
                                                <div class="row">
                                                   <div class="col m12 s12 mb-3 text-right">
                                                      <button class="red btn btn-reset" type="button">
                                                      <i class="material-icons left">clear</i>Reset
                                                      </button>
                                                      <button class="btn btn-light previous-step" disabled>
                                                      <i class="material-icons left">arrow_back</i>
                                                      Prev
                                                      </button>
                                                      <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                      <i class="material-icons right">arrow_forward</i></button>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </li>
                                    <li class="step ">
                                       <div class="step-title waves-effect">Property Matching</div>
                                       <div class="step-content">
                                          <form id="form" class="col s12" novalidate="novalidate">
                                             <div class="row">
                                                <!--- Single selection dropdown -->
                                                <div class="input-field col l3 m4 s12">
                                                   <select class="select2 browser-default" id="leadby1rg" required data-minimum-results-for-search="Infinity">
                                                      <option value="" disabled selected>Choose your option</option>
                                                      <option value="1">Option 1</option>
                                                      <option value="2">Option 2</option>
                                                      <option value="3">Option 3</option>
                                                   </select>
                                                   <label for="leadby1rg" class="active">Heading</label>
                                                </div>
                                                <div class="input-field col l3 m4 s12">
                                                   <label for="lead_assignd12" class="active">Heading: <span class="red-text">*</span></label>
                                                   <input type="text" class="validate" name="lead_assignd12" id="lead_assign23" required placeholder="Heading"  >
                                                </div>
                                             </div>
                                             <div class=" ">
                                                <div class="row">
                                                   <div class="col m12 s12 mb-3 text-right">
                                                      <button class="red btn btn-reset" type="button">
                                                      <i class="material-icons left">clear</i>Reset
                                                      </button>
                                                      <button class="btn btn-light previous-step" disabled>
                                                      <i class="material-icons left">arrow_back</i>
                                                      Prev
                                                      </button>
                                                      <button class="btn waves-effect waves-light" type="submit" name="action"> Next
                                                      <i class="material-icons right">arrow_forward</i></button>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- START RIGHT SIDEBAR NAV -->
               </div>
               <div class="content-overlay"></div>
            </div>
         </div>
      </div>
      <!-- END: Page Main-->



      <script>
        function lead_info(){
            $('#lead_info').show();
            $('#customer_name').hide();    
            $('#col_lead_info').css('background-color','orange');        
            $('#col_customer_name').css('background-color',''); 

//             var x = document.getElementById("lead_info");
//             alert(x);
//   if (x.style.display == "none") {
//     x.style.display = "block";
//   } else {
//     //   $('#lead_info').css('disaply','none')
//     x.style.display = "none";
//   }

        }
        function customer_name(){
            $('#customer_name').show();
            $('#lead_info').hide();
            $('#col_lead_info').css('background-color','');
            $('#col_customer_name').css('background-color','orange'); 
        }

        function category(){
            $('#category').show();
            $('#location').hide();     
            $('#col_location1').css('background-color','');
            $('#col_category').css('background-color','orange');        
        }
        
        function location1(){
            // alert(); return;
            $('#location').show();
            $('#category').hide();
            $('#col_location1').css('background-color','orange');
            $('#col_category').css('background-color','');        
        }
    </script>

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     