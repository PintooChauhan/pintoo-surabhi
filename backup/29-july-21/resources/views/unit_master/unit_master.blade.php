<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->

         <style>
            .building_name{
                padding-bottom: 5px;
                background-color: green;
                color: white;
                padding-top: 5px;
                padding-left: 5px;
            }
         </style>

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

       
            

        <div class="row " style="font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">UNIT MASTER</span>
                </div><br>

                <div class="row">
                    <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">       
                        <th >Socity name</th> 
                            <th >Wing Name</th>
                            <th>No of Floors</th>
                            <th>Parking Floors</th>
                            <th>No of units</th>
                        </tr>
                        </thead>
                        <tbody>
                           @foreach($getData as $getData) 
                           <!-- <input type="text" id="wing_id" value="{{$getData->wing_id}}"> -->
                            <tr>                      
                                <td>{{strtoupper($getData->building_name)}}</td>                        
                                <td> <a href="javascript:void(0)" onClick="unitView({{$getData->wing_id}})">{{ strtoupper($getData->wing_name) }}</a></td>
                                <td>{{$getData->total_floors}}</td>
                                <td></td>  
                                <td>{{$getData->total_units}}</td>                        
                        
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
  
                </div>
                <br>
                <script>
                    function unitView(wingId) {
                        // alert(wingId)

                        $.ajax({
                        type:'GET',
                        url: '/append_unit_structure',
                        data: {wingId:wingId},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html = response;
                            $('#unitView').css('display','block');
                            $('#unitView').html(html);
                            $('#unitSideView').html('');
                            // $('#name_initial_'+a).select2({minimumResultsForSearch: -1});
                            // $('#country_code_'+a).select2({minimumResultsForSearch: -1});
                            // $('#country_code_w_'+a).select2({minimumResultsForSearch: -1});
                            // $('#reporting_to_'+a).select2({minimumResultsForSearch: -1});
                            // $('#designation_'+a).select2({minimumResultsForSearch: -1});                          
                            

                            },
                            error: function(response){
                                console.log(response);
                                    // $('#image-input-error').text(response.responseJSON.errors.file);
                            }
                        });
                    }

                    function sideunitView(params1,params2) {
                       var params1 = params1;
                       var params2 = params2;
                       
                       $.ajax({
                        type:'GET',
                        url: '/append_unit_structure_side_view',
                        data: {params1:params1,params2:params2},
                        // contentType: false,
                        // processData: false,
                        success: (response) => {
                            console.log(response); //return
                            var html = response;
                            $('#unitSideView').html(html);
                            $("#panel_table").reset();
                            // $('#name_initial_'+a).select2({minimumResultsForSearch: -1});
                            // $('#country_code_'+a).select2({minimumResultsForSearch: -1});
                            // $('#country_code_w_'+a).select2({minimumResultsForSearch: -1});
                            // $('#reporting_to_'+a).select2({minimumResultsForSearch: -1});
                            // $('#designation_'+a).select2({minimumResultsForSearch: -1});                          
                            

                            },
                            error: function(response){
                                console.log(response);
                                    // $('#image-input-error').text(response.responseJSON.errors.file);
                            }
                        });
                       

                    }

                    function hide_all_info(){
               var refugee_unit =  $('#refugee_unit').val();
               if(refugee_unit=='yes'){
                $('#not_refugee').css('display','none');
                $('#not_refugee1').css('display','none');
               }else{
                $('#not_refugee').css('display','');
                $('#not_refugee1').css('display','');
               }

                // 
            }
                </script>
                <form method="post" id="add_unit_master_form" enctype="multipart/form-data">  @csrf
                <div class="row">
                    <div class="col l11 m11">
                        <div id="unitView" style="display:none;"></div>
                        <!-- overflow-x: hidden;overflow-y: auto; text-align: justify -->
                    </div>
                    
                </div>

               


                
                <br>
                
                <!-- <div class="row">
                    <div class=" input-field  col l3 m4 s12">
                        <label for="loan_amt" id="loan_amt" class="active">Wing Name: </label>                           
                        <input type="text" class="validate" name="wing_name"  id='wing_name'  min="0" max="10"  placeholder="Enter" >                        
                    </div>

                    <div class=" input-field  col l3 m4 s12">
                         <select class="select2  browser-default" onChange="hide_fields1()" id="unit_type"  data-placeholder="Select" name="unit_type">
                            <option value="" disabled selected>Select</option>
                            <option value="residential">Residential</option>
                            <option value="commercial">Commercial</option>
                        </select>
                        <label for="property_seen" class="active">Unit Type</label>
                    </div>

                    <div class=" input-field  col l3 m4 s12">
                        <label for="loan_amt" id="loan_amt" class="active">Flat No: </label>                           
                        <input type="text" class="validate" name="flat_no"  id='flat_no'  min="0" max="10"  placeholder="Enter" >                        
                    </div>

                    <div class=" input-field  col l3 m4 s12">
                        <label for="loan_amt" id="loan_amt" class="active">Floor No: </label>                           
                        <input type="number" class="validate" name="floor_no"  id='floor_no'  min="0" max="10"  placeholder="Enter" >                        
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="text" class="input_select_size" name="build_up_area" id="build_up_area" required placeholder="Enter" >
                                <label>Build up area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="build_up_area_unit" name="build_up_area_unit" class="select2 browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="text" class="input_select_size" name="carpet_area" id="carpet_area" required placeholder="Enter" >
                                <label> Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="carpet_area_unit" name="carpet_area_unit" class="select2 browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="text" class="input_select_size" name="rera_carpet_area" id="rera_carpet_area" required placeholder="Enter" >
                                <label>RERA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="rera_carpet_area_unit" name="rera_carpet_area_unit" class="select2 browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>

                    
                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="text" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area" required placeholder="Enter"  >
                                <label>MOFA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="mofa_carpet_area_unit" name="mofa_carpet_area_unit" class="select2 browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                        <select class="select2  browser-default"  id="configuration"  data-placeholder="Select" name="configuration">
                            <option value="" disabled selected>Select</option>
                            <option value="yes">1</option>
                            <option value="no">2</option>
                        </select>
                        <label for="property_seen" class="active">Configuration</label>
                    </div>

                    <div class=" input-field  col l3 m4 s12">
                        <select class="select2  browser-default"  id="configuration_size"  data-placeholder="Select" name="configuration_size">
                            <option value="" disabled selected>Select</option>
                            <option value="yes">1</option>
                            <option value="no">2</option>
                        </select>
                        <label for="property_seen" class="active">Configuration Size</label>
                    </div>

                     <div class=" input-field  col l3 m4 s12">
                        <select class="select2  browser-default"  id="direction"  data-placeholder="Select" name="direction">
                            <option value="" disabled selected>Select</option>
                            <option value="yes">1</option>
                            <option value="no">2</option>
                        </select>
                        <label for="property_seen" class="active">Direction</label>
                    </div>

                    <div class=" input-field  col l3 m4 s12">
                    <select class="select2  browser-default"  id="balcony"  data-placeholder="Select" name="balcony">
                            <option value="" disabled selected>Select</option>
                            <option value="yes">1</option>
                            <option value="no">2</option>
                        </select>
                        <label for="property_seen" class="active">Balcony</label>
                    </div>
                </div> -->
           
                    <div class='row' id='panel_table' style="display:none" >
                        <div class="col l4 s4 m4"></div>
                        <div class="col l8 s8 m8">
                            <table class="bordered  striped centered" id="example123"  style=" background-color: white;">
                            <thead style=" background-color: lightblue;">
                            <tr>
                            <th width="10%">Room &nbsp; &nbsp;</th>                                
                            <th width="8%">View</th>
                            <th width="6%">Dimension</th>
                            <th width="10%">Direction</th>
                            <th width="2%">Action</th>
                            
                            </tr>
                            </thead> 
                            <tbody>
                        </tbody>
                        </table>    
                            <table class="bordered">   
                            <tbody> 
                                <tr>
                                    <td style="width: 24%;">
                                        <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
                                            <select class="validate  browser-default" id="room" data-placeholder="Company name, Branch name" name="room[]">
                                                    <option value="option 1" >Select</option>
                                                    @foreach($room as $room)
                                                        <option value="{{$room->room_id}}">{{ ucfirst($room->room_name) }}</option>                                                    
                                                    @endforeach
                                                </select>
                                        <div>        
                                    </td>
                                     <td style="width: 20%;">
                                        <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                                            <input type="text" class="validate" name="view[]" id="view" style="margin-left: -10px;">
                                        </div>
                                    </td>
                                    <td style="width: 20%;">
                                        <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                                            <input type="text" class="validate" name="dimention[]" id="dimention" style="margin-left: -10px;">
                                        </div>
                                    </td>
                                    <td  style="width: 25%;">
                                        <div class="input-field col l12 m4 s12" style="padding-left: 0px;">    
                                            <select class="validate  browser-default" id="direction" data-placeholder="Company name, Branch name" name="direction[]">
                                                <option value="option 1" >Select</option>
                                                    @foreach($direction as $direction)
                                                        <option value="{{$direction->direction_id}}">{{ ucfirst($direction->direction) }}</option>                                                    
                                                    @endforeach
                                            </select>
                                        </div>
                                    </td>

                                    <td>
                                            <span id="AddMoreFileIdChallenge_panel" class="addbtn" style="position: unset">
                                            <a href="#" id="AddMoreFileBoxChallenge_Panel" class="" style="color: red !important">+</a> 
                                            </span>
                                    </td>      

                                </tr>               
                            </tbody>
                            <tbody id="display_inputs_Challenge_panel"></tbody> 
                        </table>
                    
                    </div>  


            </div>

            <div class="row">   <div class="input-field col l6 m6 s6 display_search">
                <div class="alert alert-danger print-error-msg" style="display:none;color:red">
                <ul></ul>
                </div>
            </div>  </div> 

      <div class="row">
         <div class="input-field col l2 m2 s6 display_search">
            <button class="btn-small  waves-effect waves-light green darken-1" type="submit" name="action">Save</button>                        
         </div>
         <div class="input-field col l2 m2 s6 display_search">
            <a href="#" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
         </div>
      </div>
    </form> 
                
           
           <script>
            function setModalValues(value){
                $('#floor_no').val(value);
            }
           
           </script>
          
          <div id="modal19" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Room</h5>
        <hr>
        
        <!-- <form method="post" id="add_preferried_tenant"> -->
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active"> Room: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_preferred_tenant" id="add_preferred_tenant"   placeholder="Enter">                                       
                </div>                
            </div>     
            
           
            <div class="alert alert-danger print-error-msg_add_preferried_tenant" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
                <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_preferried_tenant_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        <!-- </form> -->
</div>  
         


    <!-- <div id="modal4" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center"><b>UNIT INFORMATION</b></h5>
        <hr>
        
            <form method="post" id="add_unit_amenity">
            <div class="row" style="margin-right: 0rem !important">

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default" onChange="" id="property_seen1"  data-placeholder="Select" name="property_seen">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <label for="property_seen" class="active">Unit Available ?</label>
                </div>

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default" onChange="" id="r1e"  data-placeholder="Select" name="property_seen">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <label for="property_seen1" class="active">Refugee Unit</label>
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Property No.: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Floor No.: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="floor_no" id="floor_no"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">RERA Carpet Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">MOFA Carpet Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Door Facing Direction: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Configuration: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 ">
                <select class="select2  browser-default" onChange="" id="re"  data-placeholder="Select" name="property_seen">
                    <option value="" disabled selected>Select</option>
                    <option value="yes">Residencial</option>
                    <option value="no">Commercial</option>
                </select>
                <label for="property_seen" class="active">Unit Type</label>
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Build Up Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">No.of Bedrooms with Sizes: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>


                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">No.of Bathrooms: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Hall Size: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Kitchen Size: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Parking : <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Parking Info: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">    Unit View: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

            </div>             
           
            <div class="alert alert-danger print-error-msg_add_unit_amenity" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            
            
                
            </div>
        </form>
    </div>  -->

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->
<script>
    function hide_fields1(){
        var unit_type = $('#unit_type').val();
        
        if(unit_type=='commercial'){
            // alert(unit_type); //return;
            $('#configuration').prop('disabled', true); 
            $('#configuration_size').prop('disabled', true); 
            $('#balcony').prop('disabled', true); 

        }else{
            $('#configuration').prop('disabled', false); 
            $('#configuration_size').prop('disabled', false); 
            $('#balcony').prop('disabled', false); 
        }
    }
</script>

        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     