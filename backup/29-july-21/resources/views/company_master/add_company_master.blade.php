<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
<!-- BEGIN: SideNav-->
<x-sidebar-layout></x-sidebar-layout>
<!-- END: SideNav-->
<style>
   ::-webkit-scrollbar {
   display: none;
   }
   input:focus::placeholder {
   color: transparent;
   }
   .select2-container--default .select2-selection--multiple:before {
   content: ' ';
   display: block;
   position: absolute;
   border-color: #888 transparent transparent transparent;
   border-style: solid;
   border-width: 5px 4px 0 4px;
   height: 0;
   right: 6px;
   margin-left: -4px;
   margin-top: -2px;top: 50%;
   width: 0;cursor: pointer
   }
   .select2-container--open .select2-selection--multiple:before {
   content: ' ';
   display: block;
   position: absolute;
   border-color: transparent transparent #888 transparent;
   border-width: 0 4px 5px 4px;
   height: 0;
   right: 6px;
   margin-left: -4px;
   margin-top: -2px;top: 50%;
   width: 0;cursor: pointer
   }
</style>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- BEGIN: Page Main class="main-full"-->
<!-- <div id="container1"><div id="container2"> -->
<div id="main" class="main-full" style="min-height: auto">
<div class="row">
   <!--             
      <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
           
               <span class="userselect">ADD COMPANY MASTER</span><hr> 
       </div> -->
   <div class="collapsible-body"  id='budget_loan' style="display:block" >
      <div class="row" style="font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
         <span class="userselect">ADD COMPANY MASTER</span>
      </div>
      <br>
    <form method="post" id="add_company_form" enctype="multipart/form-data">  @csrf
      <div class="row">
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Group Name: <span class="red-text">*</span></label>
            <input type="text" class="validate" name="group_name"    placeholder="Enter"  >                             
         </div>
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Reg. Office Address: <span class="red-text">*</span></label>
            <input type="text" class="validate" name="reg_office_address"    placeholder="Enter"  >                             
         </div>
         <div class="input-field col l3 m4 s12 display_search">
            <label for="contactNum1" class="dopr_down_holder_label active">Office Phone: <span class="red-text">*</span></label>
            <div  class="sub_val no-search">
               <select  id="office_phone_extension" name="office_phone_extension" class="select2 browser-default">
                  @foreach($country_code as $cou)
                  <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                  @endforeach
               </select>
            </div>
            <input type="text" class="validate mobile_number" name="office_phone" id="office_phone"  placeholder="Enter" >
         </div>
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Office Email Id : <span class="red-text">*</span></label>
            <input type="email" class="validate" name="office_email_id"    placeholder="Enter"  >                             
         </div>
      </div>
      <div class="row">
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Web Site Address : <span class="red-text">*</span></label>
            <input type="text" class="validate" name="web_site_url"    placeholder="Enter"  >                             
         </div>
         <div class="input-field col l3 m4 s12 display_search">
            <select  id="company_rating" name="company_rating" class="select2  browser-default">
               <option value="" disabled selected>Select</option>
               <option value="1" >1</option>
               <option value="2" >2</option>
               <option value="3" >3</option>
               <option value="4" >4</option>
               <option value="5" >5</option>
            </select>
            <label class="active">Company Rating</label>
         </div>
        
         <!-- <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Recognition : <span class="red-text">*</span></label>
            <input type="text" class="validate" name="recognition"    placeholder="Enter"  >                             
         </div> -->
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">CP Code : <span class="red-text">*</span></label>
            <input type="text" class="validate" name="cp_code"    placeholder="Enter"  >                             
         </div>

         <div class="input-field col l3 m4 s12 display_search">
            <select  id="tat_period" name="tat_period" class="select2  browser-default">
               <option value="" disabled selected>Select</option>
               <option value="1" >1</option>
               <option value="2" >2</option>
               <option value="3" >3</option>
               <option value="4" >4</option>
               <option value="5" >5</option>
            </select>
            <label class="active">TAT Period(Days) </label> 
         </div>
      </div>
      
      <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
		<div class="clonable" data-ss="1">
      <div class="row">
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Awards & Recognition : <span class="red-text">*</span></label>
            <input type="text" class="validate" name="awards_reco_name[]"    placeholder="Enter"  >                             
         </div>

         <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">Photos: </label>
            <input type="file" name="awards_reco_image[]"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
         </div>

         <div class="input-field col l1 m4 s12 display_search">   
                  <a href="#" id="hap" class="clonable-button-add" title="Add"   style="color: grey !important;font-size: 23px;"> <i class="material-icons  dp48">add</i></a> 
                  <a href="#" class="clonable-button-close" title="Remove"  style="color: red !important;font-size: 23px;"> <i class="material-icons  dp48">remove</i></a>
         </div>
      </div>
      </div>
      </div>

     

      
      <div class="row">
         <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">Builder Invoice format : </label>
            <input type="file" name="builder_invoice_format"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
         </div>
         <!-- <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">Invoice Process : </label>
            <input type="file" name="builder_invoice_process"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
         </div> -->
         <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">Builder Proforma Format : </label>
            <input type="file" name="builder_proforma_format"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
         </div>
         <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">Builder Payment Process : </label>
            <input type="file" name="builder_payment_process"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
         </div>
         <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">Images : </label>
            <input type="file" name="images[]" multiple="true" id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
         </div>
         <div class="input-field col l3 m4 s12 display_search">
            <label  class="active">CP empanelment agreement : </label>
            <input type="file" name="cp_empanelment_agreement"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
         </div>
      </div>
      <div class="row">
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Company Pros: <span class="red-text">*</span></label>
            <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="company_pros" rows='20' col='40'></textarea>
         </div>
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Company Cons: <span class="red-text">*</span></label>
            <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="company_cons" rows='20' col='40'></textarea>
         </div>
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Key member Name : <span class="red-text">*</span></label>
            <input type="text" class="validate" name="key_member"    placeholder="Enter"  >                             
         </div>
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">
            <label  class="active">Help desk email : <span class="red-text">*</span></label>
            <input type="text" class="validate" name="help_desk_email"    placeholder="Enter"  >                             
         </div>
      </div>
      <div class="row">
         <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
            <label  class="active">Comments: <span class="red-text">*</span></label>
            <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea>
         </div>
      </div>

      <hr>
      <div class="row">
                    <!-- <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Name </th>
                        <th>Designation</th>
                        <th>Mobile No.</th>
                        <th>Email Id</th>
                        <th>Reports to</th>
                        <th style="background-color: green !important"> <a href='#' onClick="" style="color: white;; " >Add New</a></th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>Tushar Bhagat</td>
                        <td>Sales</td>
                        <td>9820677891</td>
                        <td>Alabc@yahoo.comvin</td>
                        <td>Abbas</td>                       
                       
                        </tr> 
                        </tbody>
                    </table> -->
                <!-- <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
				<div class="clonable" data-ss="1"> -->
                    <div class="row">
                        
                      <div class="col l11">
                        <div class="row">
                          <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label for="cus_name active" class="dopr_down_holder_label active">Name 
                            <span class="red-text">*</span></label>
                            <div  class="sub_val no-search">
                                <select  id="cus_name_sel" name="name_initial[]" class="select2 browser-default ">
                                 @foreach($initials as $ini)
                                    <option value="{{$ini->initials_id}}">{{ucfirst($ini->initials_name)}}</option>
                                  @endforeach
                                </select>
                            </div>
                              <input type="text" class="validate mobile_number" name="name_h[]" id="cus_name"  placeholder="Enter"  >
                          </div>

                          <div class="input-field col l3 m4 s12" id="InputsWrapper">
                              <label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label>
                              <div  class="sub_val no-search">
                                  <select  id="country_code" name="country_code[]" class="select2 browser-default">
                                    @foreach($country_code as $cou)
                                       <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                    @endforeach
                                  </select>
                              </div>
                              <input type="text" class="validate mobile_number" name="mobile_number_h[]" id="mobile_number"  placeholder="Enter" >
                          </div>

                          <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <div  class="sub_val no-search">
                                <select  id="country_code1" name="country_code_w[]" class="select2 browser-default">
                                 @foreach($country_code as $cou)
                                    <option value="{{$cou->country_code_id}}">{{$cou->country_code}}</option>
                                  @endforeach
                                </select>
                            </div>
                            <label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label>
                            <input type="text" class="validate mobile_number" name="wap_number[]" id="wap_number"  placeholder="Enter" >
                            <div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " >
                                <input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto">
                            </div>
                          </div> 

                          <div class="input-field col l3 m4 s12" id="InputsWrapper3">
                            <label for="primary_email active" class="active">Email Address: <span class="red-text">*</span></label>
                            <input type="email" class="validate" name="primary_email_h[]" id="primary_email"  placeholder="Enter" >
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col l3 m4 s12 display_search">                    
                              <select class="select2  browser-default designation_h"  data-placeholder="Select" name="designation_h[]" id="designation_h">
                                  <option value="" disabled selected>Select</option>
                                  @foreach($designation as $des)
                                    <option value="{{$des->designation_id}}">{{ucfirst($des->designation_name)}}</option>
                                  @endforeach
                              </select>
                              <label for="possession_year" class="active">Designation </label>
                              <div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" >
                              <a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a> 
                              </div>
                          </div>

                          <div class="input-field col l3 m4 s12 display_search">                    
                              <select class="select2  browser-default"  data-placeholder="Select" name="reporting_to_h[]">
                                  <option value="" disabled selected>Select</option>
                                  @foreach($company_hirarchy as $hei)
                                    <option value="{{$hei->company_hirarchy_id}}">{{ucfirst($hei->name)}}</option>
                                  @endforeach
                              </select>
                              <label for="possession_year" class="active">Reporting to </label>                           
                          </div>

                          <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                            <label  class="active">Comments: <span class="red-text">*</span></label>
                            <input type="text" name="comments_h[]"  id="input-file-now" placeholder="Enter">
                          </div>

                          <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Upload Photo : </label>
                            <input type="file" name="photo_h[]"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                          </div>
                        </div>
                            
                      </div>
                      <div class="col l1">
                           <!-- <a href="#" id="hap" class="clonable-button-add" title="Add"   style="color: red !important"> <i class="material-icons  dp48">add</i></a> 
                           <a href="#" class="clonable-button-close" title="Remove"  style="color: red !important"> <i class="material-icons  dp48">remove</i></a>  -->
                              <!-- <div class="input-field col l1 m4 s12 display_search">                     -->
                              <!-- <button id="hap" class="clonable-button-add btn btn-primary" type="button">+</button>
                              <div class="col-md-1"><button type="button" style="display: block !important;" class="btn btn-danger clonable-button-close">-</button> -->
                              <div  class="AddMoreCompanyMasterH" >
                                          
                                          <a href="#" id="AddMoreFileBoxCompanyMasterH" class="waves-effect waves-light" style="color: grey !important;font-size: 23px;">+</a> 
                                          </div> 
                                          <br>
                                
                                  <!-- <a href="#" id="add_location" class="modal-trigger" title="Action"  style="color:  grey !important;font-size: 23px;"> <i class="material-icons  dp48">group</i></a>     -->
                                  <a href="#modal10" id="add_location" title="View Flow Chart" class="modal-trigger" style="color:  grey !important;font-size: 23px;"> <i class="material-icons  dp48">remove_red_eye</i></a>    
                             </div>
                       
                    </div>
                <!-- </div>
                </div> -->
                <div class="" id="display_inputs_CompanyMasterH"></div>
                </div>
        </div>




      <div class="row">   <div class="input-field col l6 m6 s6 display_search">
         <div class="alert alert-danger print-error-msg" style="display:none;color:red">
           <ul></ul>
        </div>
      </div>  </div> 

      
      <div class="row">
         <div class="input-field col l2 m2 s6 display_search">
            <button class="btn-small  waves-effect waves-light green darken-1" type="submit" name="action">Save</button>                        
         </div>
         <div class="input-field col l2 m2 s6 display_search">
            <a href="/company-master" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
         </div>
      </div>
    </form>  
      <br>
   </div>
   <div class="content-overlay"></div>
</div>


<!-- Modal Designation Structure -->
<div id="modal9" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Designation</h5>
        <hr>
        
        <form method="post" id="add_designation_form">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Designation: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="add_designation" id="add_designation"   placeholder="Enter">
                    <span class="add_designation_err"></span>
                    
                </div>
            </div> 
            
           
            <div class="alert alert-danger print-error-msg_add_designation" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer" style="text-align: left;">
            <span class="errors" style='color:red'></span>
            <span class="success1" style='color:green;'></span>
            <div class="row">
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="add_designation_form()" type="button" name="action">Submit</button>                        

                    </div>    
                    
                    <div class="input-field col l3 m3 s6 display_search">
                        <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                    </div>    
                </div> 
            </div>
        </form>
</div>

<div id="modal10" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center"> Flow Chart</h5>
         
        <hr>
        
        <html>
         <head>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">
               google.charts.load('current', {packages:["orgchart"]});
               google.charts.setOnLoadCallback(drawChart);

               function drawChart() {
               var data = new google.visualization.DataTable();
               data.addColumn('string', 'Name');
               data.addColumn('string', 'Manager');
               data.addColumn('string', 'ToolTip');

               // For each orgchart box, provide the name, manager, and tooltip to show.
               data.addRows([
                  // [{'v':'Mike', 'f':'Mike'},
                  // '', ''],
                  // [{'v':'Jim', 'f':'Jim<div style="color:red; font-style:italic">Vice President</div>'},
                  // 'Mike', 'VP'],
                  ['Mike', '', ''],
                  ['Jim', 'Mike', ''],
                  ['Alice', 'Mike', ''],
               ['Prasanna', 'Alice', ''],
               ['Kiran', 'Prasanna', ''],
               ['ABC', 'Kiran', ''],
               ['XYZ', 'Kiran', ''],
                  ['Bob', 'Jim', 'Bob Sponge'],		  
                  ['Carol', 'Bob', ''],
               ['Prasanna', 'Carol', '']
               ]);

               // Create the chart.
               var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
               // Draw the chart, setting the allowHtml option to true for the tooltips.
               chart.draw(data, {'allowHtml':true});
               }
            </script>
            </head>
         <body>
            <div id="chart_div"></div>
         </body>
         </html>

        <br>
        <div class="row">
          <div class="input-field col l3 m3 s6 display_search">
              <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
          </div>
        </div>
        <br>
         
</div> 
<!-- </div>
   </div> -->
<!-- END: Page Main-->
<!-- </div>
   </div> -->
   <script src="https://code.jquery.com/jquery-3.4.0.slim.min.js"  crossorigin="anonymous"></script>
      <script src="{{ asset('app-assets/js/jquery.cloner.js') }}"></script>

<script>
function add_designation_form(){
   // alert();
    var url = 'addDesignationCompanyMaster';
    var form = 'add_designation_form';
    var err = 'print-error-msg_add_designation';

    $.ajax(  {
        url:"/"+url,
        type:"GET",
        data:                     
             $('#'+form).serialize() ,
        
        success: function(data) {
            // alert(data.success); 
            var a = data.a;
            var option = a.option;
            var value = a.value; 

            var newOption= new Option(option,value, true, false);
            // Append it to the select
            $(".designation_h").append(newOption);//.trigger('change');
            // $('#designation_h').append($('<option>').val(optionValue).text(optionText));


            // console.log(data.success);return;
            if($.isEmptyObject(data.error)){
               $('.success1').html(data.success);
               //  alert(data.success);
               //  location.reload();
            }else{
                printErrorMsg(data.error,err);
            }
        }
    });    
    
    function printErrorMsg (msg,err) {

        $("."+err).find("ul").html('');
        $("."+err).css('display','block');
        $.each( msg, function( key, value ) {
            $("."+err).find("ul").append('<li>'+value+'</li>');
        });                
    }
    // execute(url,form,err);
}
</script>      
  
<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->