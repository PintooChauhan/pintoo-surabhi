<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->
                
         

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

        <div class="row">
                    <div class="input-field col l6 m6 s12" id="InputsWrapper2">
                            <input type="text" class="validate" name="search"    placeholder="Enter Project / Builders Company Name"  >                             
                    </div>
                      <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-large waves-effect waves-light green darken-1" onClick="" type="button" name="action" style="line-height: 43px !important;height: 43px !important">Search</button>                        

                    </div>  
                   
                </div>
            <br>

                <div class="row">
                    <table class="bordered">
                        <thead>
                        <tr style="color: white;background-color: #ffa500d4;">
                        <th>Company Name </th>
                        <th>Project Name</th>
                        <th>Status</th>
                        <th>Location</th>
                        <th>Sub Location</th>
                        <th>Types of Flats</th>
                        <th>Website</th>
                        <th>TAT Period</th>
                        <!-- <th> </th> -->
                        <th style="background-color: green !important"> <a href='/add-company-master-form' style="color: white;; " >Add Company</a></th>

                        
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $data)
                          <tr>
                            <td>{{ ucwords($data->group_name) }}</td>
                            <td><a href="project-master?gr_id={{$data->company_id}}" target="_blank"> {{ ucwords($data->project_complex_name) }} </a></td>
                            <td>{{ strtoupper($data->unit_status) }}</td>
                            <td>{{ ucwords($data->location_name) }}</td>
                            <td>{{ ucwords($data->sub_location_name) }}</td>
                            <td>?</td>        
                            <td>{{$data->web_site_url}}</td>
                            <td>{{$data->tat_period}}</td>
                            <td><a href='/project-master?gr_id={{$data->company_id}}' >Add Porject</a></td>
                            <!-- <td><a href='#'  >View Company</a></td> -->
                                  <!-- /edit-company-master -->
                          </tr>
                        @endforeach
                        </tbody>
                    </table>
  
                </div>

    
       
        
        

         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


 

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     