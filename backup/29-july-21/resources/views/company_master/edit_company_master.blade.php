<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->

         

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

            

        <div class="row" style="font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">EDIT COMPANY MASTER</span>
                </div><br>
                <div class="row">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Group Name: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="company_name"    placeholder="Enter"  >                             
                            
                        </div>
                        

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Reg. Office Address: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="reg_office_address"    placeholder="Enter"  >                             
                        </div>

                    
                        <div class="input-field col l3 m4 s12 display_search">                    
                            <label for="contactNum1" class="dopr_down_holder_label active">Office Phone: <span class="red-text">*</span></label>
                            <div  class="sub_val no-search">
                                <select  id="country_code" name="country_code" class="select2 browser-default">
                                    <option value="1">+91</option>
                                    <option value="2">+1</option>
                                    <option value="3">+3</option>
                                </select>
                            </div>
                            <input type="text" class="validate mobile_number" name="phone_no" id="phone_no" required placeholder="Enter" >
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Office Email Id : <span class="red-text">*</span></label>
                            <input type="email" class="validate" name="email_id"    placeholder="Enter"  >                             
                        </div>
                       
                </div>
                

                <div class="row">

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Web Site Address : <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="web_site_address"    placeholder="Enter"  >                             
                        </div>  

                        <div class="input-field col l3 m4 s12 display_search">
                                
                                <select  id="company_rating" name="company_rating" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    <option value="1" >1</option>                                    
                                    <option value="2" >2</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                    <option value="5" >5</option>
                                </select>
                                <label class="active">Company Rating</label>
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Awards : <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="awards"    placeholder="Enter"  >                             
                        </div>  

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Recognition : <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="recognition"    placeholder="Enter"  >                             
                        </div>

                </div>

                <div class="row">

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">CP Code : <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="cp_code"    placeholder="Enter"  >                             
                    </div>  

                    <div class="input-field col l3 m4 s12 display_search">
                            
                            <select  id="oc_time_period" name="oc_time_period" class="validate select2 browser-default">
                            <option value="" disabled selected>Select</option>
                                <option value="1" >1</option>                                    
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                            </select>
                            <label class="active">TAT Period(Days) </label> 
                    </div>
                </div>

                <div class="row">

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Invoice Format : </label>
                            <input type="file" name="invoice_format"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Invoice Process : </label>
                            <input type="file" name="invoice_process"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                              
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Proforma Format : </label>
                            <input type="file" name="proforma_format"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Payment Process : </label>
                            <input type="file" name="payment_process"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                              
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Images : </label>
                            <input type="file" name="image" multiple="true" id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                              
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">CP empanelment agreement : </label>
                            <input type="file" name="invoice_format"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                        </div>  
                </div>
         

                <div class="row">

                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        
                        <label  class="active">Company Pros: <span class="red-text">*</span></label>
                        <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea>
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        
                        <label  class="active">Company Cons: <span class="red-text">*</span></label>
                        <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea>
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Key member : <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="cp_code"    placeholder="Enter"  >                             
                    </div> 

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">Help desk email : <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="cp_code"    placeholder="Enter"  >                             
                    </div> 
                </div>
                <div class="row">

                    

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">                        
                        <label  class="active">Comments: <span class="red-text">*</span></label>
                        <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea>
                    </div>  

                                    
                </div>

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light green darken-1" onClick="alert('In Progress')" type="button" name="action">Update</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <a href="/company-master" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
                    </div>    
                </div> 

        <br>
        
         
            

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     