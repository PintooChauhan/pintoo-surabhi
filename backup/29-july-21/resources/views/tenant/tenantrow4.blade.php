<div class="row">                                        
                
    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="parking()">
            <div class="collapsible-header" id='col_parking'>Parking</div>
            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="notice_period()">
            <div class="collapsible-header" id='col_notice_period'>Notice Period</div>
            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="society_amenities()">
            <div class="collapsible-header" id="col_society_amenities">Building Amenities  </div>
            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="building_age()" id="building_age_li">
            <div class="collapsible-header" id='col_building_age'> Building & Unit Preference</div>
            
            </li>                                        
        </ul>
    </div>
                                    
  

            
               

       </div> 

       <div class="collapsible-body"  id='parking' style="display:none">
           <div class='row'>
               <div class="input-field col l3 m4 s12 display_search">                    
                   <select class="select2  browser-default"  multiple="multiple"  data-placeholder="Select" name="parking_type">
                       <option value="" >Allotted  </option>
                       <option value="option 1">Basement</option>
                       <option value="option 1">Podium</option>
                   </select>
                   <label for="possession_year" class="active">Parking Type </label>
               </div>

               <div class=" input-field  col l3 m4 s12">
                           <label for="loan_amt" id="loan_amt" class="active">No of FW Parking: </label>                           
                           <input type="number" class="validate" name="no_of_fw_parking"  id='no_of_fw_parking'  min="0" max="10"  placeholder="Enter" >
                        
                           
               </div>

               <div class=" input-field  col l3 m4 s12" id='no_of_tw_parking_show'>
                       <label for="loan_amt" id="loan_amt" class="active">No of TW Parking</label>                           
                       <input type="number" class="validate" name="no_of_tw_parking"  id='no_of_tw_parking'   min="0" max="10" placeholder="Enter" >
                       
               </div>
           </div>


       </div>
     

       

     


       
       
       <div class="collapsible-body"  id='society_amenities' style="display:none">
           
            <div class='row'>        
             <div class='col l1 m1 s2'>
                        <div id="AddMoreFileId3" class="addbtn" style="position: unset !important">   
                            <a href="#modal13" id="unit_am" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                        </div>   
                    </div>     
                    <div class='col l8 m8 s12'>
                        <div id="ck-button">
                            <label>
                                <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                            </label>
                        </div>
                        <div id="ck-button">
                            <label>
                                <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                            </label>
                        </div>
                        <div id="ck-button">
                            <label>
                                <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                            </label>
                        </div>
                        <div id="ck-button">
                            <label>
                                <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                            </label>
                        </div>
                        <div id="ck-button">
                            <label>
                                <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                            </label>
                        </div>
                        <div id="ck-button">
                            <label>
                                <input type="checkbox" value="1" id='my' onClick='my()'><span style="padding: 0px 10px 0px 10px;">AMENITY</span>
                            </label>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>

        <div class="collapsible-body"  id='building_age' style="display:none">
            <div class="row">
                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"  data-placeholder="Select" name="building_age">
                            <option value="" disabled selected>Select</option>
                            <option value="option 1">1 – 5 </option>
                            <option value="option 1">5 - 10</option>
                        </select>
                        <label for="building_age" class="active">Building Age  </label>
                    </div>   
                    <div class="input-field col l3 m4 s12 display_search">                
                        <select class="select2  browser-default" multiple="multiple" id="building_type" data-placeholder="Select" name="building_type">
                            <option value="option 1">West </option>
                            <option value="option 1">North East</option>
                        </select>
                        <label for="building_type" class="active">Building Type</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">                            
                        <select class="select2  browser-default"  data-placeholder="Select" name="floor_choice">
                            <option value="" disabled selected>Select</option>
                            <option value="option 1">10-15</option>
                            <option value="option 1">15-20</option>
                        </select>
                        <label for="floor_choice" class="active">Floor Choice    </label>
                    </div>   

                    <div class="input-field col l3 m4 s12 display_search">                
                        <select class="select2  browser-default" multiple="multiple"  data-placeholder="Select" name="door_facing_direction">
                            <option value="option 1">West </option>
                            <option value="option 1">North East</option>
                        </select>
                        <label for="purpose" class="active">Door Direction</label>
                    </div>
            </div>

        </div>

   
       
       
    <div class="collapsible-body"  id='notice_period' style="display:none">
           <div class='row'>
               <div class="input-field col l3 m4 s12 display_search">                    
                   <select class="select2  browser-default" onChange="hideShowNoticePeriod()"  data-placeholder="Select" name="notice_period" id="notice_period_val">
                       <!-- <option value="" >Allotted  </option> -->
                       <option value="yes">Yes</option>
                       <option value="no">No</option>
                   </select>
                   <label  class="active">Notice Served </label>
               </div>

              <div class=" input-field  col l3 m4 s12" id="notice_period_date_div">
                <label for="wap_number" class="active">Notice Period Date <span class="red-text">*</span></label>
                <input type="text" class="datepicker" name="notice_period_date"  required placeholder="Calender" style="height: 41px;">
              </div>

              <div class="input-field col l3 m4 s12" id="notice_period_reason_div" style="display:none" >  
                    <label for="lead_assign" class="active">Reason: <span class="red-text">*</span></label>
                    <!-- <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="notice_period_reason" rows='2' col='4'></textarea> -->
                </div> 
           </div>


       </div>
   

       


    <!-- Modal Unit amenity Structure -->
    <div id="modal13" class="modal">
        <div class="modal-content">
        <h5 style="text-align: center">Add Building Amenity</h5>
        <hr>
        
        <form method="post" id="add_unit_amenity">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l4 m4 s12 display_search">
                    <label for="lead_assign" class="active">Amenity: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>
                    
                </div>
            </div>             
           
            <div class="alert alert-danger print-error-msg_add_unit_amenity" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light" onClick="add_unit_amenity()" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            
            
                
            </div>
        </form>
    </div>
