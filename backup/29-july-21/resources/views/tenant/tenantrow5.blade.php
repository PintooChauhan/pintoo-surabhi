<div class="row">                                        
                
<div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="license_period()">
            <div class="collapsible-header" id="col_license_period">License Period & Lock in Period</div>
            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="specific_choice()">
            <div class="collapsible-header" id="col_specific_choice">Specific Choice </div>
            
            </li>                                        
        </ul>
    </div>

    <div class="col l3 s12">
        <ul class="collapsible">
            <li onClick="nature_of_business()" id="nature_of_business_li">
            <div class="collapsible-header" id="col_nature_of_business">Nature of Business & Washroom</div>
            
            </li>                                        
        </ul>
    </div>   

    <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="brokerage_fee()" id="brokerage_fee_li">
                <div class="collapsible-header" id='col_brokerage_fee'> Brokerage fees</div>
                
                </li>                                        
            </ul>
        </div>   
               

       </div>
       <div class="collapsible-body"  id='license_period' style="display:none">
       <div class="row">
                      
                      <div class="input-field col l3 m4 s12 display_search">                
                          <select class="select2  browser-default"  data-placeholder="Select" name="license_period">          
                                 <option value="1">1 Year</option> 
                                 <option value="2">2 Year</option> 
                          </select>
                          <label for="purpose" class="active">License Period </label>
                      </div>                  

                      <div class="input-field col l3 m4 s12 display_search">                
                          <select class="select2  browser-default"  data-placeholder="Select" name="lock_in_period">          
                                 <option value="1">6 Month</option> 
                                 <option value="2">1 Year</option> 
                          </select>
                          <label for="purpose" class="active">Lock In Period </label>
                      </div>
  
                     
                     
            </div>
    </div>  
 

       <div class="collapsible-body"  id='specific_choice' style="display:none">
       <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
				<div class="clonable" data-ss="1">
            <div class="row">
                      
                    <div class="input-field col l3 m4 s12 display_search">                
                            <label for="loan_amt" id="loan_amt" class="active">Project Name: </label>                           
                            <input type="text" class="validate" name="project_name"  id='project_name' placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">                
                        <label for="loan_amt" id="loan_amt" class="active">Building Name: </label>                           
                        <input type="text" class="validate" name="building"  id='building' placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">                
                        <label for="loan_amt" id="loan_amt" class="active">Location: </label>                           
                        <input type="text" class="validate" name="location"  id='location' placeholder="Enter" >
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">                
                        <label for="loan_amt" id="loan_amt" class="active">Sub Location: </label>                           
                        <input type="text" class="validate" name="sub_location"  id='sub_location' placeholder="Enter" >
                    </div>
            </div>

              
                    <div class="row">
                            
                            <div class="input-field col l3 m4 s12 display_search">                
                                <label for="loan_amt" id="loan_amt" class="active">Building Amenities: </label>                           
                                <input type="text" class="validate" name="building_amenities"  id='building_amenities' placeholder="Enter" >
                            </div>
        
                            <div class="input-field col l3 m4 s12 display_search">                
                                <label for="loan_amt" id="loan_amt" class="active">Unit Amenities: </label>                           
                                <input type="text" class="validate" name="unit_amenities"  id='unit_amenities' placeholder="Enter" >
                            </div>

                            <div class="input-field col l3 m4 s12" >  
                                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                                <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" Placeholder="Enter" name="specific_choice_comment" rows='2' col='4'></textarea> -->
                                <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
                            </div>

                            <div class="input-field col l1 m1 s1" >  
                                <!-- <div  class="addbtn" >
                                        <a href="#" id="add_location1" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
                                </div>  -->
                                <!-- <a href="#" id="hap" class="clonable-button-add" title="Add"   style="color: grey !important"> <i class="material-icons  dp48">add</i></a> 
                                <a href="#" class="clonable-button-close" title="Remove"  style="color: grey !important"> <i class="material-icons  dp48">remove</i></a>  -->
                            </div>

                            
                    </div>
                </div>
              </div>  
        </div>

        
<div class="collapsible-body"  id='nature_of_business' style="display:none">
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Nature of Business : <span class="red-text">*</span></label>
            <input type="text"  name="nature_of_bussiness"  Placeholder="Enter" >
        </div>

        <div class="input-field col l3 m4 s12 display_search">                
            <select class="select2  browser-default" multiple="multiple" id="washroom" data-placeholder="Select" name="washroom">
                <option value="option 1">Attached </option>
                <option value="option 1">Common</option>
            </select>
            <label for="building_type" class="active">Washroom </label>
        </div>
    </div>
</div>

<div class="collapsible-body"  id='brokerage_fee' style="display:none">
    <div class="row">

    <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Rent amount : <span class="red-text">*</span></label>
            <input type="text"  name="rent_amount" placeholder="Enter" >
            <!-- <div  class="addbtn" >
                    <a href="#" id="add_location13" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  -->
        </div>

             <div class="input-field col l3 m4 s12 ">
                    <div  class="sub_val no-search">
                    <select  id="brokerage_percent" name="brokerage_percent" onChange="calculate_brokerage()"  class="select2 browser-default">
                    <option value="" disabled selected>Select</option>
                    <option value="0.1">0.1 %</option>
                    <option value="0.2">0.2 %</option>
                    <option value="0.3">0.3 %</option>
                    <option value="0.4">0.4 %</option>
                    <option value="0.5">0.5 %</option>
                    <option value="0.6">0.6 %</option>
                    <option value="0.7">0.7 %</option>
                    <option value="0.8">0.8 %</option>
                    <option value="0.9">0.9 %</option>
                    <option value="1">1 %</option>    
                    <option value="1.1">1.1 %</option>
                    <option value="1.2">1.2 %</option>
                    <option value="1.3">1.3 %</option>
                    <option value="1.4">1.4 %</option>
                    <option value="1.5">1.5 %</option>
                    <option value="1.6">1.6 %</option>
                    <option value="1.7">1.7 %</option>
                    <option value="1.8">1.8 %</option>
                    <option value="1.9">1.9 %</option>
                    <option value="2">2 %</option>    
                    <option value="2.1">2.1 %</option>
                    <option value="2.2">2.2 %</option>
                    <option value="2.3">2.3 %</option>
                    <option value="2.4">2.4 %</option>
                    <option value="2.5">2.5 %</option>
                    <option value="2.6">2.6 %</option>
                    <option value="2.7">2.7 %</option>
                    <option value="2.8">2.8 %</option>
                    <option value="2.9">2.9 %</option>
                    <option value="3">3 %</option>                             
                                     

                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Brokerage fees: <span class="red-text">*</span></label>
                <div class="rupeeSign">
                <input type="text" class="validate mobile_number" name="brokerage_fees" id="brokerage_fees" placeholder="Numeric" required  >
                </div>
                <span id="brokerage_fees_words"></span>                                
            </div>

            <div class="input-field col l3 m4 s12" >  
                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" placeholder="Enter" name="brokerage_comment"  rows='2' col='4'></textarea> -->
                <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
            </div> 
    </div>
</div>
