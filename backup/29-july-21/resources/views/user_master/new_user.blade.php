<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
            
               <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">ADD NEW USER</span><hr>  </div>

         

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >
                <div class="row">
                     <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        <label  class="active">First name: <span class="red-text">*</span></label>
                        <input type="text" class="validate" name="first_name"    placeholder="Enter"  >                             
                            
                        </div>
                        

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Last name: <span class="red-text">*</span></label>
                            <input type="text" class="validate" name="last_name"    placeholder="Enter"  >                             
                        </div>

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">DOB : <span class="red-text">*</span></label>
                            <input type="text" class="datepicker" name="dob"    placeholder="Calender"  >                             
                        </div>  

                        <div class="input-field col l3 m4 s12 display_search">                    
                            <label for="contactNum1" class="dopr_down_holder_label active">Phone Number: <span class="red-text">*</span></label>
                            <div  class="sub_val no-search">
                                <select  id="country_code" name="country_code" class="select2 browser-default">
                                    <option value="1">+91</option>
                                    <option value="2">+1</option>
                                    <option value="3">+3</option>
                                </select>
                            </div>
                            <input type="text" class="validate mobile_number" name="phone_no" id="phone_no" required placeholder="Enter" >
                        </div>
                       
                </div>
                

                <div class="row">

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            <label  class="active">Email Id : <span class="red-text">*</span></label>
                            <input type="email" class="validate" name="email_id"    placeholder="Enter"  >                             
                        </div>  

                        <div class="input-field col l3 m4 s12 display_search">
                                
                                <select  id="oc_time_period" name="oc_time_period" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    <option value="1" >Branch1</option>                                    
                                    <option value="1" >Branch2</option>
                                </select>
                                <label class="active">Branch</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                                
                                <select  id="designation" name="designation" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    <option value="1" >Sales</option>                                    
                                    <option value="1" >Marketing</option>
                                </select>
                                <label class="active">Designation</label>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                                
                                <select  id="reporting_manager" name="reporting_manager" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    <option value="1" >Ankit</option>                                    
                                    <option value="1" >Neha</option>
                                </select>
                                <label class="active">Reporting Manager</label>
                                <div id="add_pc_name" class="addbtn" >
                                    <a href="#modal1" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: red !important;right: -4px;"> +</a> 
                                </div>                            
                        </div>
                </div>

                <div class="row">

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            
                        <select  id="access_levl_gr" name="access_levl_gr" class="validate select2 browser-default">
                                <option value="" disabled selected>Select</option>
                                    <option value="1" >Sales</option>                                    
                                    <option value="1" >Marketing</option>
                                </select>
                                <label class="active">Access Level Group</label>
                                <div id="add_pc_name" class="addbtn" >
                                    <a href="#modal1" id="add_pc_name" class="waves-effect waves-light  modal-trigger" style="color: red !important;right: -4px;"> +</a> 
                                </div>                            
                        </div>  

                        <div class="input-field col l3 m4 s12 display_search">                                
                            <label  class="active">Basic Salary : <span class="red-text">*</span></label>
                            <input type="email" class="validate" name="basic_salary"    placeholder="Enter"  >                             
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Photo Upload : <span class="red-text">*</span></label>
                            <input type="file" name="photo"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Resume Upload : <span class="red-text">*</span></label>
                            <input type="file" name="resume"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
                              
                        </div>
                </div>
                

               
                <div class="row">

                        <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                            
                            <label  class="active">Temp Address: <span class="red-text">*</span></label>
                            <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="temp_address" rows='20' col='40'></textarea>
                        </div>  

                        <div class="input-field col l3 m4 s12 display_search">                                
                        <label  class="active">Permanent Address: <span class="red-text">*</span></label>
                            <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="permanent_address" rows='20' col='40'></textarea>
                        </div>

                        <div class="input-field col l3 m4 s12 display_search">
                            <label  class="active">Description: <span class="red-text">*</span></label>
                            <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="description" rows='20' col='40'></textarea>
                        </div>
                </div>

                <div class="row">

                    <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                        
                        <label  class="active">Comments: <span class="red-text">*</span></label>
                        <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="comments" rows='20' col='40'></textarea>
                    </div>  

                    <div class="input-field col l3 m4 s12 display_search">                                
                    <label  class="active">Internal Comments: <span class="red-text">*</span></label>
                        <textarea style="border-color: #5b73e8; padding-top: 12px" placeholder="Enter" name="internal_comments" rows='20' col='40'></textarea>
                    </div>                   
                </div>

                <div class="row">
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light" onClick="alert('In Progress')" type="button" name="action">Save</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <!-- <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Cancel</button>                         -->
                        <a href="/users-list" class="waves-effect waves-green btn-small" style="background-color: red;">Cancel</a>

                    </div>    
                </div> 

        </div>
           
         
            

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->


        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     