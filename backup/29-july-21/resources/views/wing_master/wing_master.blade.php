<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}

.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

      </style>
      
     
      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
<!--             
               <div class="container" style="font-weight: 600;text-align: center; padding-top:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD COMPANY MASTER</span><hr> 
                </div> -->

         <style>
            .building_name{
                padding-bottom: 5px;
                background-color: green;
                color: white;
                padding-top: 5px;
                padding-left: 5px;
            }
         </style>

             
        <div class="collapsible-body"  id='budget_loan' style="display:block" >

       
            

        <div class="row " style="font-weight: 600;text-align: center; padding-top:10px; padding-bottom:10px;color:white;background-color:green"> 
                    
                        <span class="userselect">ADD WING MASTER</span>
                </div><br>
                <form method="post" id="wing_master_form">@csrf
                <div class="row">
                    <div class="col l8"></div>
                    <div class="col l4">
                        <!-- <a href="#modal4" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> ADD WING</a> -->
                        <a href="/unit-master?society_id={{$society_id}}" class="btn-small  waves-effect waves-light green darken-1 modal-trigger"> UNIT MASTER</a>
                    </div>                        
                </div>
                <br>
                
               <div id="" >
                <div class="row building_name" > <lable style="font-weight: 700;">{{$Finalbuilding_name}}</lable></div>
                <div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":false}'>
				<div class="clonable" data-ss="1">
                    <div class="row">
                        <div class="col l1">
                        <a href="#" id="hap" class="clonable-button-add"  style="color: red !important"> <i class="material-icons dp48">add</i></a> 
                        <a href="#" class="clonable-button-close" style="color: red !important"> <i class="material-icons dp48">remove</i></a> 
                            <!-- <div class="input-field col l1 m4 s12 display_search">                     -->
                            <!-- <button id="hap" class="clonable-button-add btn btn-primary" type="button">+</button>
                            <div class="col-md-1"><button type="button" style="display: block !important;" class="btn btn-danger clonable-button-close">-</button> -->
                                <!-- <div  class="AddMoreFileIdlookingsince" >
                                        
                                        
                                </div>  -->
                                <!-- <a href="/add-wing-details?society_id={{$Finalsociety_id}}" id="add_location" class=" modal-trigger" style="color: red !important"> <i class="material-icons dp48">edit</i></a>     -->
                            <!-- </div>  -->
                        </div>
                        <input type="hidden" class="validate" name="society_id" value="{{$society_id}}" >
                        <div class="col l11">
                            <!-- <a href="/add-wing-details" id="add_location" class=" modal-trigger" style="color: red !important"> -->
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Wing Name: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="wing_name[]"   placeholder="Enter" >
                            </div>
                            <!-- </a> -->
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Total Floor: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="total_floor[]"   placeholder="Enter" >
                            </div>

                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Habitable Floor <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="haitable_floor[]"   placeholder="Enter" >
                            </div>
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Unit per Floor: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="unit_per_floor[]"   placeholder="Enter" >
                            </div>
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Refugee unit: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="refugee_unit[]"   placeholder="Enter" >                            
                            </div>
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Total Unit: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="total_unit[]"   placeholder="Enter" >
                            </div>
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="active" > No. of Lifts : <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="lifts[]"   placeholder="Enter" >
                            </div>
                            <div class="input-field col l2 m4 s12" id="display_search">
                                <label  class="active" style="font-size: 10px !important;">Floor Plan: <span class="red-text">*</span></label>
                                <input type="file" name="floor_plan[]" multiple="true"  style="padding-top: 14px;">
                            </div>
                            <div class="input-field col l2 m4 s12" id="InputsWrapper2">
                                <label  class="active" style="font-size: 10px !important;" >Name Board : <span class="red-text">*</span></label>
                                <input type="file" name="name_board[]" multiple="true" style="padding-top: 14px;">
                            </div>
                            
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Comment: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="comments[]"   placeholder="Enter" >
                                <!-- <textarea style="border-color: #5b73e8; " placeholder="Enter" name="comments" rows='20' col='40'></textarea> -->
                                
                            </div>
                        </div>
    
                    </div>
                    @if(isset($wingData))
                    @foreach($wingData as $wingData)
                    <div class="row">
                        <div class="col l1">
                        <a href="#" id="hap" class="clonable-button-add"  style="color: red !important"> <i class="material-icons dp48">add</i></a> 
                        <a href="#" class="clonable-button-close" style="color: red !important"> <i class="material-icons dp48">remove</i></a> 
                            <!-- <div class="input-field col l1 m4 s12 display_search">                     -->
                            <!-- <button id="hap" class="clonable-button-add btn btn-primary" type="button">+</button>
                            <div class="col-md-1"><button type="button" style="display: block !important;" class="btn btn-danger clonable-button-close">-</button> -->
                                <!-- <div  class="AddMoreFileIdlookingsince" >
                                        
                                        
                                </div>  -->
                                <!-- <a href="/add-wing-details?wing_id={{$wingData->wing_id}}" id="add_location" class=" modal-trigger" style="color: red !important"> <i class="material-icons dp48">edit</i></a>     -->
                            <!-- </div>  -->
                        </div>
                        
                        <div class="col l11">
                            <!-- <a href="/add-wing-details" id="add_location" class=" modal-trigger" style="color: red !important"> -->
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Wing Name: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="wing_name[]" value="@if(isset($wingData->wing_name)) {{$wingData->wing_name}}  @endif "  placeholder="Enter" >
                                
                            </div>
                            
                            <!-- </a> -->
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Total Floor: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="total_floor[]"  value="@if(isset($wingData->total_floors)) {{$wingData->total_floors}}  @endif "   placeholder="Enter" >
                            </div>

                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Habitable Floor <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="haitable_floor[]"   value="@if(isset($wingData->habitable_floors)) {{$wingData->habitable_floors}}  @endif " placeholder="Enter" >
                            </div>
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Unit per Floor: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="unit_per_floor[]" value="@if(isset($wingData->units_per_floor)) {{$wingData->units_per_floor}}  @endif "   placeholder="Enter" >
                            </div>
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Refugee unit: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="refugee_unit[]"  value="@if(isset($wingData->refugee_units)) {{$wingData->refugee_units}}  @endif "  placeholder="Enter" >                            
                            </div>
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Total Unit: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="total_unit[]" value="@if(isset($wingData->total_units)) {{$wingData->total_units}}  @endif "  placeholder="Enter" >
                            </div>
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="active" > No. of Lifts : <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="lifts[]" value="@if(isset($wingData->no_of_lifts)) {{$wingData->no_of_lifts}}  @endif "  placeholder="Enter" >
                            </div>
                            <div class="input-field col l2 m4 s12" id="display_search">
                                <label  class="active" style="font-size: 10px !important;">Floor Plan: <span class="red-text">*</span></label>
                                <input type="file" name="floor_plan[]" multiple="true"  style="padding-top: 14px;">
                            </div>
                            <div class="input-field col l2 m4 s12" id="InputsWrapper2">
                                <label  class="active" style="font-size: 10px !important;" >Name Board : <span class="red-text">*</span></label>
                                <input type="file" name="name_board[]" multiple="true" style="padding-top: 14px;">
                            </div>
                            
                            <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                                <label  class="labelClass">Comment: <span class="red-text">*</span></label>
                                <input type="text" class="validate" name="comments[]" value="@if(isset($wingData->comments )) {{$wingData->comments }}  @endif "   placeholder="Enter" >
                                <!-- <textarea style="border-color: #5b73e8; " placeholder="Enter" name="comments" rows='20' col='40'></textarea> -->
                                
                            </div>
                        </div>
    
                    </div>
                    @endforeach
                    @endif

                </div>
                </div>
                <!-- <div class="" id="display_inputs_wing_master"></div> -->
               </div> 

             

               <br>
               <div class="row">
               <div class="alert alert-danger print-error-msg" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
                    <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-small  waves-effect waves-light green darken-1"  type="submit" name="action">Save</button>                        

                    </div>    
                    
                    <div class="input-field col l2 m2 s6 display_search">
                        <a href="#" class="waves-effect btn-small" style="background-color: red;">Cancel</a>
                    </div>    
                </div> 
         
 
         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->
<script>
     




</script>
<script src="https://code.jquery.com/jquery-3.4.0.slim.min.js"  crossorigin="anonymous"></script>
      <script src="{{ asset('app-assets/js/jquery.cloner.js') }}"></script>
        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     