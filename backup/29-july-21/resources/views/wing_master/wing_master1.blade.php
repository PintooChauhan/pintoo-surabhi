<!-- Header Layout start -->
<x-header-layout></x-header-layout>
<!-- Header Layout end -->
      <!-- BEGIN: SideNav-->
      <x-sidebar-layout></x-sidebar-layout>
      <!-- END: SideNav-->
      <style>
::-webkit-scrollbar {
  display: none;
}
input:focus::placeholder {
  color: transparent;
}
input:hover {
    background-color: orange;
  }
.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}
table.bordered td{
    padding: 9px 30px 6px 9px !important
}

      </style>
      

      <!-- BEGIN: Page Main class="main-full"-->
      <!-- <div id="container1"><div id="container2"> -->
    <div id="main" class="main-full" style="min-height: auto">
        <div class="row">
           
           
            
               <!-- <div class="container" style="font-weight: 600;text-align: center;margin-top: 5px;"> <span class="userselect">ADD NEW USER</span><hr>  </div> -->

            <div class="collapsible-body"  id='budget_loan' style="display:block" >

            <div class="row">
                <div class="input-field col l3 m4 s12" id="InputsWrapper2">
                <input type="text"  name="access_level" value="@if(isset($wing_name)) {{$wing_name}} @endif" Readonly style="text-align: center;"  placeholder="Enter"  >
                </div>
                <!-- <div class="input-field col l2 m2 s6 display_search">
                        <button class="btn-large  waves-effect waves-light" onClick="alert('In Progress')" type="button" name="action" style="line-height: 43px !important;height: 43px !important">Search</button>                        

                    </div>  
                    <div class="input-field col l2 m2 s6 display_search">
                    <button class="btn-large  waves-effect waves-light green darken-1" onClick="add_industry_form()" type="button" name="action" style="line-height: 43px !important;height: 43px !important">Add Access Group</button>                        

                    </div>  
                    <div class="input-field col l2 m2 s6 display_search">
                    <a href="/user-form"  class="btn-large  waves-effect waves-light green darken-1" style="line-height: 43px !important;height: 43px !important"> Add User</a> 

                    </div>   -->
            </div>
            <br>
               <div class="row">
                     <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <input type="text"  name="access_level" value="Floor"  Readonly style="text-align: center;"  placeholder="Enter"  >
                    </div>
                    <input type="text" value="{{$society_id}}" id="society_id">
                <input type="text" value="{{$wing_id}}" name="wing_id">
                  
            
                    @php $total_floor= $total_floors;  $total_unit =$units_per_floor; $i=0;@endphp
                    @for($i=1; $i<=$total_unit; $i++ )
                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <input type="text"  name="access_level" value="Unit-{{$i}}" Readonly style="text-align: center;"  placeholder="Enter"  >
                    </div>
                    @endfor

                    
                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <input type="text"  name="access_level" value="Flats in Floor" Readonly style="text-align: center;"  placeholder="Enter"  >
                    </div>
    
               </div>
               @for($ii=1; $ii<=$total_floor; $ii++ )
               <div class="row">
                     <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <input type="text"  name="access_level" value="{{$ii}}"  style="text-align: center;"   >
                    </div>
                    @for($i=1; $i<=$total_unit; $i++ ) 
                    @php $un = 100 * $ii +$i; @endphp
                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <a href="#modal4" class="modal-trigger" onClick="setModalValues({{$un}})">
                            <input type="text"  name="access_level" value="{{$un}}"  style="text-align: center;"   >
                        </a>    
                    </div>
                    @endfor
<!-- 
                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <a href="#modal4" class="modal-trigger" onClick="setModalValues(1202)">
                            <input type="text"  name="access_level" value="1202"  style="text-align: center;"   >
                        </a>
                    </div>

                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <a href="#modal4" class="modal-trigger" onClick="setModalValues(1203)">
                            <input type="text"  name="access_level" value="1203"  style="text-align: center;"   >
                        </a>    
                    </div>

                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <a href="#modal4" class="modal-trigger" onClick="setModalValues(1204)">
                            <input type="text"  name="access_level" value="1204"  style="text-align: center;"   >
                        </a>
                    </div>
                        -->
                    <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                        <input type="text"  name="access_level" value="{{$units_per_floor}}"  style="text-align: center;"   >
                    </div> 
    
               </div>
               @endfor
                     <br>
               <div class="row">
                <div class="input-field col l2 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light green darken-1" onClick="" type="button" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l2 m3 s6 display_search">
                <a href="/wing-master" class="waves-effect waves-green btn-small"  style="background-color: red;">Cancel</a>
                </div>    
            </div>

               

            </div>


             
    <div id="modal4" class="modal" style="width:84% !important">
        <div class="modal-content">
            <h5 style="text-align: center"><b>UNIT INFORMATION</b></h5>
            <hr>
        
            <form method="post" id="add_unit_info_form">@csrf
                <input type="text" value="{{$society_id}}" name="society_id">
                <input type="text" value="{{$wing_id}}" name="wing_id">
            <div class="row" style="margin-right: 0rem !important">

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default" id="unit_available"  data-placeholder="Select" name="unit_available">
                        <option value="" disabled selected>Select</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <label for="property_seen" class="active">Unit Available ?</label>
                </div>

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default"  id="refugee_unit" onChange="hide_all_info()"  data-placeholder="Select" name="refugee_unit">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <label for="property_seen1" class="active">Refugee Unit</label>
                </div>
            </div>
            <div id="not_refugee">
                <div class="row" style="margin-right: 0rem !important" >  
            
                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Property No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="property_no" id="property_no"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Floor No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="floor_no" id="floor_no"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                            

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="numeric" class="input_select_size" name="rera_carpet_area" id="rera_carpet_area" required placeholder="Enter" >
                                <label>RERA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="rera_carpet_area_unit" name="rera_carpet_area_unit" class="select2 browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="numeric" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area" required placeholder="Enter"  >
                                <label>MOFA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="mofa_carpet_area_unit" name="mofa_carpet_area_unit" class="select2 browser-default ">
                                @foreach($unit as $un)
                                <option value="{{ $un->unit_name }}">{{ ucwords($un->unit_name) }}</option>                                    
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Door Facing Direction: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="door_facing_direction" id="door_facing_direction"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Configuration: <span class="red-text">*</span></label>
                    <!-- <input type="text" class="validate" name="configuration" id="configuration"   placeholder="Enter"> -->
                    <select  name="configuration" class="validate select2 browser-default">
                                  @foreach($configuration as $configuration)
                                    <option value="{{ $configuration->configuration_id  }}">{{ ucwords($configuration->configuration_name) }}</option>                                    
                                    @endforeach
                                </select>
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                
                    <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default"  id="re"  data-placeholder="Select" name="unit_type">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Residencial</option>
                        <option value="no">Commercial</option>
                    </select>
                    <label for="property_seen" class="active">Unit Type</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Build Up Area: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="build_up_area" id="build_up_area"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">No.of Bedrooms with Sizes: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="no_of_bedrooms" id="no_of_bedrooms"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>


                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">No.of Bathrooms: <span class="red-text">*</span></label>
                    <input type="numeric" class="validate" name="no_of_bathrooms" id="no_of_bathrooms"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
            
                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Hall Size: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="hall_size" id="hall_size"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Kitchen Size: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="kitchen_size" id="kitchen_size"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Parking : <span class="red-text">*</span></label>
                    <input type="numeric" class="validate" name="parking" id="parking"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Parking Info: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="parking_info" id="parking_info"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
            

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">    Unit View: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="unit_view" id="unit_view"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="comment" id="comment"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                </div>             
            </div>           
            <div class="alert alert-danger print-error-msg_add_unit_info" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style='color:red'></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light"  type="submit" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            
            
                
            </div>
        <!-- </form> -->
    </div> 
         
            

         
         
        </div>
               <div class="content-overlay"></div>
    </div>
         <!-- </div>
      </div> -->
      <!-- END: Page Main-->
      <!-- </div>
        </div> -->
        <script>
            function setModalValues(value){
                $('#floor_no').val(value);
            }

            function hide_all_info(){
               var refugee_unit =  $('#refugee_unit').val();
               if(refugee_unit=='yes'){
                $('#not_refugee').css('display','none');
               }else{
                $('#not_refugee').css('display','');
               }

                // 
            }
           
           </script>

        

<!-- Footer layout start -->
<x-footer-layout></x-footer-layout>
<!-- Footer layout End -->
     