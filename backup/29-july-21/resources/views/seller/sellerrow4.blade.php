<div class="row">                             
    
    <div class="col l3 s12"  >
            <ul class="collapsible" >
                <li onClick="images_and_video()" >
                <div class="collapsible-header" id='col_images_and_video'>Images & Video</div>
                
                </li>                                        
            </ul>
    </div>    

       <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="brokerage_fee_seller()" id="brokerage_fee_li">
                <div class="collapsible-header" id='col_brokerage_fee_seller'> Brokerage fees</div>
                
                </li>                                        
            </ul>
        </div>    
</div>



<div class="collapsible-body"  id='images_and_video' style="display:none">
    <div class="row">      

        <div class="input-field col l3 m4 s12 display_search">
        <label  class="active"> Image and Videos : <span class="red-text">*</span></label>
        <input type="file" name="image_and_video[]" multiple  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;">
        </div>

        <div class="input-field col l3 m4 s12 display_search">
        <label  class="active"> Videos link : <span class="red-text">*</span></label>
        <input type="text" name="video_links" multiple  id="input-file-now" placeholder="Enter">
        </div>
    </div>
</div>


<div class="collapsible-body"  id='brokerage_fee_seller' style="display:none">
    <div class="row">
            
    <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Property Price : <span class="red-text">*</span></label>
            <input type="text"  name="property_price" placeholder="Enter" >
            <!-- <div  class="addbtn" >
                    <a href="#" id="add_location13" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  -->
        </div>

             <div class="input-field col l3 m4 s12 ">
                    <div  class="sub_val no-search">
                    <select  id="brokerage_percent" name="brokerage_percent" onChange="calculate_brokerage()"  class="select2 browser-default">
                    <option value="" disabled selected>Select</option>
                    <option value="0.1">0.1 %</option>
                    <option value="0.2">0.2 %</option>
                    <option value="0.3">0.3 %</option>
                    <option value="0.4">0.4 %</option>
                    <option value="0.5">0.5 %</option>
                    <option value="0.6">0.6 %</option>
                    <option value="0.7">0.7 %</option>
                    <option value="0.8">0.8 %</option>
                    <option value="0.9">0.9 %</option>
                    <option value="1">1 %</option>    
                    <option value="1.1">1.1 %</option>
                    <option value="1.2">1.2 %</option>
                    <option value="1.3">1.3 %</option>
                    <option value="1.4">1.4 %</option>
                    <option value="1.5">1.5 %</option>
                    <option value="1.6">1.6 %</option>
                    <option value="1.7">1.7 %</option>
                    <option value="1.8">1.8 %</option>
                    <option value="1.9">1.9 %</option>
                    <option value="2">2 %</option>    
                    <option value="2.1">2.1 %</option>
                    <option value="2.2">2.2 %</option>
                    <option value="2.3">2.3 %</option>
                    <option value="2.4">2.4 %</option>
                    <option value="2.5">2.5 %</option>
                    <option value="2.6">2.6 %</option>
                    <option value="2.7">2.7 %</option>
                    <option value="2.8">2.8 %</option>
                    <option value="2.9">2.9 %</option>
                    <option value="3">3 %</option>                             
                                     

                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Brokerage fees: <span class="red-text">*</span></label>
                <div class="rupeeSign">
                <input type="text" class="validate mobile_number" name="brokerage_fees" id="brokerage_fees" placeholder="Numeric" required  >
                </div>
                <span id="brokerage_fees_words"></span>                                
            </div>

            <div class="input-field col l3 m4 s12" >  
                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" placeholder="Enter" name="brokerage_comment"  rows='2' col='4'></textarea> -->
                <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
            </div> 
    </div>
</div>