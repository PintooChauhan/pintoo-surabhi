<div class="row">        

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="lead_analysis_seller()">
                <div class="collapsible-header" id="col_lead_analysis_seller">Lead Analysis & Intensity </div>                
                </li>                                        
            </ul>
        </div>   

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="key_member_seller()">
                <div class="collapsible-header" id="col_key_member_seller">Key members </div>                
                </li>                                        
            </ul>
        </div>   

        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="expected_closure()" id="expected_closure_li">
                <div class="collapsible-header" id='col_expected_closure'>Expected closure</div>
                
                </li>                                        
            </ul>
        </div>

     
                        
        <div class="col l3 s12">
            <ul class="collapsible">
                <li onClick="challenges_seller()">
                <div class="collapsible-header" id="col_challenges_seller">Challenges & solutions </div>                
                </li>                                        
            </ul>
        </div>   
        
        

        
</div>


<div class="collapsible-body"  id='challenges_seller' style="display:none">
<div class="clonable-block" data-toggle="cloner" data-options='{"clearValueOnClone":true}'>
   <div class="clonable" data-ss="1">
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Challenges : <span class="red-text">*</span></label>
            <input type="text"  name="challenges"  Placeholder="Enter" >
            <!-- <div  class="addbtn" >
                    <a href="#" id="add_location13" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  -->
        </div>

        <div class="input-field col l3 m4 s12 display_search">
            <label for="lead_assign" class="active">Solutions : <span class="red-text">*</span></label>
            <input type="text"  name="solutions"  Placeholder="Enter" >
            <!-- <div  class="addbtn" >
                    <a href="#" id="add_location32" class="waves-effect waves-light  modal-trigger" style="color: grey !important"> +</a> 
            </div>  -->
        </div>

        <div class="input-field col l3 m4 s12 display_search">
            <!-- <div id="AddMoreFileIdChallenge" class="addbtn" >                        
                <a href="#" id="AddMoreFileBoxChallenge" class="" style="color: grey !important">+</a> 
            </div>  -->
            <a href="#" id="hap" class="clonable-button-add" title="Add"   style="color: grey !important"> <i class="material-icons  dp48">add</i></a> 
                          <a href="#" class="clonable-button-close" title="Remove"  style="color: grey !important"> <i class="material-icons  dp48">remove</i></a> 
        </div>        
    </div>
    </div>
</div>
    <div class="" id="display_inputs_Challenge"></div>
</div>



<div class="collapsible-body"  id='lead_analysis_seller' style="display:none">
    <div class="row">
            <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="lead_analysis">
                    <option value="" disabled selected>Select</option>
                    <option value="option 1">Need</option>
                    <option value="option 1">Desire</option>
                </select>
                <label for="possession_year" class="active">Lead analysis </label>              
            </div>

            <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="lead_intensity">
                    <option value="" disabled selected>Select</option>
                    <option value="option 1">Cold</option>
                    <option value="option 1">Warm</option>
                </select>
                <label for="possession_year" class="active">Lead Intensity </label>              
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Describe in 5W1H method : <span class="red-text">*</span></label>
                <input type="text"  name="describe_in_5wh"  Placeholder="Enter" >               
            </div>

        
    </div>
</div>

<div class="collapsible-body"  id='key_member_seller' style="display:none">
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="suffix_decision" name="suffix_decision"  class="select2 browser-default">
                        <option value="mr" selected>Mr</option>
                        <option value="2000000">Option1</option>                      
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Decisoin Maker Name: <span class="red-text">*</span></label>
                <input type="text" class="validate mobile_number" name="decision_maker_name" id="decision_maker_name" required placeholder="Enter" >      
            
        </div>    
            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Relation with Seller : <span class="red-text">*</span></label>
                <input type="text"  name="decision_name_relation"  Placeholder="Enter" >               
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Describe in 5W1H method : <span class="red-text">*</span></label>
                <input type="text"  name="decision_in_5wh"  Placeholder="Enter" >               
            </div>
    </div>
    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="suffix_financer" name="suffix_financer"  class="select2 browser-default">
                        <option value="mr" selected>Mr</option>
                        <option value="2000000">Option1</option>                      
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Financer Name: <span class="red-text">*</span></label>
                <input type="text" class="validate mobile_number" name="financer_name" id="financer_name" required placeholder="Enter" >      
            
        </div>
            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Relation with Seller : <span class="red-text">*</span></label>
                <input type="text"  name="financer_name_relation"  Placeholder="Enter" >               
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Describe in 5W1H method : <span class="red-text">*</span></label>
                <input type="text"  name="financer_in_5wh"  Placeholder="Enter" >               
            </div>
    </div>

    <div class="row">
        <div class="input-field col l3 m4 s12 display_search">
            <div  class="sub_val no-search">
                    <select  id="suffix_influencer" name="suffix_influencer"  class="select2 browser-default">
                        <option value="mr" selected>Mr</option>
                        <option value="2000000">Option1</option>                      
                        
                    </select>
                </div>    
                <label for="wap_number" class="dopr_down_holder_label active">Influencer Name: <span class="red-text">*</span></label>
                <input type="text" class="validate mobile_number" name="influencer_name" id="influencer_name" required placeholder="Enter" >      
            
        </div>
            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Relation with Seller : <span class="red-text">*</span></label>
                <input type="text"  name="influencer_name_relation"  Placeholder="Enter" >               
            </div>

            <div class="input-field col l3 m4 s12 display_search">
                <label for="lead_assign" class="active">Describe in 5W1H method : <span class="red-text">*</span></label>
                <input type="text"  name="influencer_in_5wh"  Placeholder="Enter" >               
            </div>
    </div>
</div>

<div class="collapsible-body"  id='expected_closure' style="display:none">
    <div class="row">

    <div class="input-field col l3 m4 s12 display_search">                    
                <select class="select2  browser-default"  data-placeholder="Select" name="expcted_closure">
                    <option value="" disabled selected>Select</option>
                    <option value="option 1">option</option>
                    <option value="option 1">option</option>
                </select>
                <label for="expcted_closure" class="active">Expected closure month & week </label>              
            </div>

    
            

            <div class="input-field col l3 m4 s12" >  
                <label for="lead_assign" class="active">Describe in 5W1H Method: <span class="red-text">*</span></label>
                <!-- <textarea style="border-color: #5b73e8;padding-top: 12px" placeholder="Enter" name="describe_in_5W1H_method1"  rows='2' col='4'></textarea> -->
                <input type="text" class="validate" name="minimum_loan_amount" placeholder="Enter" >
            </div> 
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.4.0.slim.min.js"  crossorigin="anonymous"></script>
      <script src="{{ asset('app-assets/js/jquery.cloner.js') }}"></script>