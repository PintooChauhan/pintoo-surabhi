<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;

class ProjectMasterController extends Controller
{
    public function index(Request $request)
    {
        // echo $request->ip();
        // server ip

        // echo \Request::ip();
        // // server ip

        // echo \request()->ip();
        // server ip

        // echo $this->getIp(); //see the method below
        // clent ip

        // exit();
        // $id = 2;
        // $where = 'location_id='.$id;
        // $select = array('location_id','location_name');
        // 
        
        // echo "<pre>"; print_r($location);
        // exit();
        if(isset($request->gr_id)){
            $group_id =$request->gr_id;
            $getProjectData = DB::select("SELECT * FROM op_project WHERE group_id=".$group_id." ");
            if(!empty($getProjectData)){
                $project_id = $getProjectData[0]->project_id;
                $group_id = $getProjectData[0]->group_id;
                $project_complex_name = $getProjectData[0]->project_complex_name;
                $category_id = $getProjectData[0]->category_id;
                $unit_status_of_complex = $getProjectData[0]->unit_status_of_complex;
                $sub_location_id = $getProjectData[0]->sub_location_id;
                $location_id = $getProjectData[0]->location_id;
                $company_id = $getProjectData[0]->company_id;
                $city_id = $getProjectData[0]->city_id;
                $state_id = $getProjectData[0]->state_id;
                $complex_rating = $getProjectData[0]->complex_rating;
                $complex_description = $getProjectData[0]->complex_description;
                $complex_land_parcel = $getProjectData[0]->complex_land_parcel;
                $company_name = $getProjectData[0]->company_name;
                $complex_open_space = $getProjectData[0]->complex_open_space;
                $sales_office_address =$getProjectData[0]->sales_office_address;
                $e_brochure = $getProjectData[0]->e_brochure;
                $video_links = $getProjectData[0]->video_links;
                $gst_no = $getProjectData[0]->gst_no;
                $gst_certificate = $getProjectData[0]->gst_certificate;
                $comments =$getProjectData[0]->comments;

                $getProjectAmenitiesData = DB::select("SELECT opa.*,pa.amenities FROM op_project_amenities as opa 
                left join dd_project_amenities as pa on pa.project_amenities_id=opa.amenities_id
                WHERE opa.project_id=".$project_id." ");                
                if(!empty($getProjectAmenitiesData)){
                    $getProjectAmenitiesData = $getProjectAmenitiesData;
                }else{
                    $getProjectAmenitiesData = array();
                }

                $getProjectPhotoData = DB::select("SELECT * FROM op_project_photo where project_id=".$project_id." ");
                if(!empty($getProjectPhotoData)){
                    $getProjectPhotoData = $getProjectPhotoData;
                }else{
                    $getProjectPhotoData = array();
                }

                $getProjecthirarchyData = DB::select("SELECT * FROM op_project_hirarchy where project_id=".$project_id." ");
                if(!empty($getProjecthirarchyData)){
                    $getProjecthirarchyData = $getProjecthirarchyData;
                }else{
                    $getProjecthirarchyData = array();
                }

                $company_hirarchy =  DB::select("select * from op_project_hirarchy where project_id=".$project_id."  ");

                
            }
        
        }else{
           $project_id = $group_id = '';
            $project_complex_name = $category_id = $unit_status_of_complex =  $sub_location_id = $location_id = '';
                $company_id = $city_id = $state_id = $complex_rating = $complex_description = $complex_land_parcel = '';
                $complex_open_space = $sales_office_address = $e_brochure = $video_links = $gst_no = $gst_certificate = '';
                $comments = '';
                $getProjectAmenitiesData = array();
                $getProjectPhotoData  = array();
                $getProjecthirarchyData = array();      
                $company_hirarchy =array();
            
        }
        // print_r($group_id);exit();
        $location = $this->getAllData('d','location' );
        $sub_location = $this->getAllData('d','sub_location');
        $city = $this->getAllData('d','city');
        $state = $this->getAllData('d','state');
        $category = $this->getAllData('d','category');
        $category_type = $this->getAllData('d','category_type');
        $construction_technology = $this->getAllData('d','construction_technology');
        $unit = $this->getAllData('d','unit');
        $select = array('company_id','group_name ');
        $group_name = $this->getAllData('o','company',$select);
        $project_amenities = $this->getAllData('d','project_amenities');
        $rating = $this->getAllData('d','rating');
        $unit_status = $this->getAllData('d','unit_status');
        $initials = $this->getAllData('d','initials');
        $country_code = $this->getAllData('d','country');
        $designation = $this->getAllData('d','designation');
        
        // echo "<pre>"; print_r($getProjectAmenitiesData);
        // echo "<pre>"; print_r($company_hirarchy); exit();

        $dataArray = array('project_id'=>$project_id,'group_id'=>$group_id,'project_complex_name'=>$project_complex_name,'category_id'=>$category_id,
                'unit_status_of_complex'=>$unit_status_of_complex,'sub_location_id'=>$sub_location_id,
                'location_id'=>$location_id,'company_id'=>$company_id,'city_id'=>$city_id,
                'state_id'=>$state_id,'complex_rating'=>$complex_rating,'complex_description'=>$complex_description,
                'complex_land_parcel'=>$complex_land_parcel,'complex_open_space'=>$complex_open_space,
                'sales_office_address'=>$sales_office_address,'e_brochure'=>$e_brochure,
                'video_links'=>$video_links,'gst_no'=>$gst_no,'gst_certificate'=>$gst_certificate,
                'comments'=>$comments,
                'location'=>$location,'sub_location'=>$sub_location,'city'=>$city,'state'=>$state,'category'=>$category,'category_type'=>$category_type,'construction_technology'=>$construction_technology,'group_name'=>$group_name,'unit'=>$unit,'project_amenities'=>$project_amenities,'rating'=>$rating,'unit_status'=>$unit_status,'initials'=>$initials,'country_code'=>$country_code,'designation'=>$designation,'company_hirarchy'=>$company_hirarchy,'company_hirarchy1'=>$company_hirarchy,
                'getProjectAmenitiesData'=>$getProjectAmenitiesData,'getProjectPhotoData'=>$getProjectPhotoData,'getProjectPhotoData1'=>$getProjectPhotoData,
                'getProjecthirarchyData'=>$getProjecthirarchyData
            );

        return view('project_master/project_master',$dataArray);
    }

    public function add_project_master(Request $request)
    {
    //    echo "<pre>"; print_r($request->all()); //exit();

        $validator = Validator::make($request->all(), [
            'group_name' => 'required',
            'complex_name' => 'required',
            'location' => 'required',
            'sub_location' => 'required',
            'city' => 'required',
            'state' => 'required',
            'unit_status_of_complex' => 'required',
            'sales_office_address' => 'required',
            'pros_of_complex' => 'required',
            'cons_of_complex' => 'required',
            'company_name' => 'required',
            'complex_land_parcel' => 'required',
            // 'complex_open_space' => 'required',   
            'gst_no' => 'required',            
            
        ]
     );
        
        if ($validator->passes()) {

            $data = array();            
            $data['group_id'] = $request->group_name;
            $data['project_complex_name'] = $request->complex_name;
            $data['location_id'] = $request->location;
            $data['sub_location_id'] = $request->sub_location;
            $data['city_id'] = $request->city;
            $data['state_id'] = $request->state;
            $data['category_id'] = $request->category;
            $data['complex_description'] = $request->complex_description;
            $data['unit_status_of_complex'] = $request->unit_status_of_complex;
            $data['complex_land_parcel'] = $request->project_land_parcel;
            $data['sales_office_address'] = $request->sales_office_address;
            $data['complex_rating'] = $request->project_rating;
            $data['video_links'] = $request->video_link;    

            $data1['pros'] = $request->pros_of_complex;
            $data1['cons'] = $request->cons_of_complex;

            $data['complex_land_parcel'] = $request->complex_land_parcel;
            $data['complex_open_space'] = $request->complex_open_space;
            $data['gst_no'] = $request->gst_no;
            $data['comments'] = $request->comments;

            $join ="";      
            // $uploadPhoto = array();
           

            if( $request->hasfile('e_brochure') )
            { 

                $array_e_brochure = array();
                foreach($request->file('e_brochure') as $file)
                {
                    $rand1 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $rand1);
                    array_push($array_e_brochure,$rand1);
                    
                }
                $data['e_brochure'] = json_encode($array_e_brochure);

                // $rand1 = mt_rand(99,9999999).".".$request->e_brochure->extension();
                // // $img = Image::make($builder_invoice_format->path());       
                // // $img->resize(600, 600);        
                // // // $resource = $img->stream()->detach();
                // // $img->save(public_path('images'));
                // // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                // $request->e_brochure->move(public_path('complex'), $rand1);
                // $data['e_brochure'] = $rand1;
            }

            

            if( $request->hasfile('gst_certificate') )
            { 
                $rand1 = mt_rand(99,9999999).".".$request->gst_certificate->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->gst_certificate->move(public_path('complex'), $rand1);
                $data['gst_certificate'] = $rand1;
            }
            DB::table('op_project')->insert($data); 
            $last_id =DB::getPDO()->lastInsertId();

            $is_available = $request->is_available;
            $description_ame = $request->description_ame;
            $cnt = count($is_available);
            // echo "<pre>"; print_r($is_available); 
            for ($i=0; $i<$cnt ; $i++) { 
                $is_available1 = $is_available[$i];
                 $description_ame1 = $description_ame[$i];
                $array = array();
    
                if(!empty($is_available1)){
                    // $is_available1 = $is_available[$i]; 
                    $is_available2 = $is_available[$i] == 0 ? 'N': 'Y';
                        $a = array('project_id'=>$last_id,'is_available'=>$is_available2,'amenities_id'=>$is_available1,'description'=>$description_ame1) ;
                        $array=   array_merge($array,$a);                       
                    // }
                    // echo "<pre>"; print_r($array); 
                    DB::table('op_project_amenities')->insert($array); 
                }
                
                
            }
              
           
            if( $request->hasfile('upload_video') )
            { 

                foreach($request->file('upload_video') as $file)
                {
                    $randV = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $randV);
                    $videoData = array('project_id'=>$last_id,'type'=>'video','url'=>$randV);
                    DB::table('op_project_photo')->insert($videoData);            
                }      
               
            }

            if($request->hasfile('upload_photos'))
            {
                $array = array();
                foreach($request->file('upload_photos') as $file)
                {
                    $randP = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('complex'), $randP);
                    $photoData = array('project_id'=>$last_id,'type'=>'photo','url'=>$randP);
                    DB::table('op_project_photo')->insert($photoData);                     
                }
               
            }
            $data1['project_id'] = $last_id;
            DB::table('project_pro_cons')->insert($data1); 

            $name_initial = $request->name_initial;
            $name_h = $request->name_h;
            $country_code = $request->country_code;
            $mobile_number_h = $request->mobile_number_h;
            $country_code_w = $request->country_code_w;
            $wap_number = $request->wap_number;
            $primary_email_h = $request->primary_email_h;
            $designation_h = $request->designation_h;
            $reporting_to_h = $request->reporting_to_h;
            $comments_h = $request->comments_h;
        
            if($request->hasfile('photo_h'))
            {
                $arrayH = array();
                foreach($request->file('photo_h') as $file)
                {
                    $rand_h = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('images'), $rand_h);
                        array_push($arrayH,$rand_h);
                    
                }
                // $data2['awards_reco_image'] = ($array);
            }
        
            $cnt = count($name_initial);
            // echo "<pre>"; print_r($designation_h); //exit();
            for ($i=0; $i<$cnt ; $i++) {  
                $initial = $name_initial[$i];
                $name= $name_h[$i];
                $country_code_id = $country_code[$i];
                $mobile_number = $mobile_number_h[$i];
                // $country_code_w = $country_code_w[$i];
                $whatsapp_number = $wap_number[$i];
                $email_id = $primary_email_h[$i];
                $designation_id = $designation_h[$i];
                // $reports_to = $reporting_to_h[$i];
                $comments = $comments_h[$i];
                $array = array('project_id'=>$last_id);
        
                if(!empty($name[$i])){
                    $name = $name_h[$i]; 
                    $a = array('name'=>$name) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($country_code_id[$i])){
                    $country_code_id = $country_code[$i]; 
                    $a = array('country_code_id'=>$country_code_id) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($mobile_number[$i])){
                    $mobile_number = $mobile_number_h[$i]; 
                    $a = array('mobile_number'=>$mobile_number) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($whatsapp_number[$i])){
                    $whatsapp_number = $wap_number[$i]; 
                    $a = array('whatsapp_number'=>$whatsapp_number) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($email_id[$i])){
                    $email_id = $primary_email_h[$i]; 
                    $a = array('email_id'=>$email_id) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($designation_id[$i])){
                    $designation_id = $designation_h[$i]; 
                    $a = array('designation_id'=>$designation_id) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($reports_to[$i])){
                    $reports_to = $reporting_to_h[$i]; 
                    $a = array('reports_to'=>$reports_to) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($comments[$i])){
                    $comments = $comments_h[$i]; 
                    $a = array('comments'=>$comments) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($arrayH[$i])){
                    $photo = $arrayH[$i]; 
                    $aH = array('photo'=>$photo) ;
                    $array=   array_merge($array,$aH);                   
                }else{
                    $aH = array('photo'=>'') ;
                    $array=   array_merge($array,$aH); 
                }
                // echo "<pre>"; print_r($array);
                DB::table('op_project_hirarchy')->insert($array);    
            }

           

            return response()->json(['success'=>'done','lastID'=>$last_id]);

         }
         return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function update_project_master(Request $request)
    {
        echo "<pre>"; print_r($request->all());
    }

}
