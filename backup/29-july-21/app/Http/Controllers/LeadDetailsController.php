<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;


class LeadDetailsController extends Controller
{
   
    
    
    public function lead_details(){
      

        // exit();




        $basic_amount = 600000; // sts  end 70l   interval 1lakh
        $OCA =   500000; // own contribution amount

        $SDR = (600000 * 4) /100;  //stamp duty and registration  4 %

        $TF = 13100;   //transfer fee
        $LF = 11000;   // legal charges
        $consultancyFee = (600000 * 1) /100;  // 1%

        $downPayment = $OCA - ($SDR+$TF+$LF+$consultancyFee);

        $loanAmount = $basic_amount - $downPayment;

        $packageAmount = $loanAmount + $SDR + $downPayment + $LF + $TF - $consultancyFee;
       
        $completePackage = $packageAmount + $consultancyFee;

        // loan == basic - downpayment


        

        // $cal = round($amount/$ratio);

        // print_r(round($cal));

        // echo "<br>";
        // $price = array($amount);
        // array_push('2424');
        // print_r( );
        // unset($price[0]);
        //  print_r( ($price) );
        // echo "<br>";
        // exit();

        // for($i=1; $i <= $cal; $i ++)
        // {
        //     $amt = $price[0];
        //     $final = $amt - $ratio; 
            
        //     // array_push($final);
        //     // echo ;
        //     // echo "<br>";
        //     array_push($price,$final);
        //     // unset($price[0]);
            
        // }

        // echo "<pre>";print_r($price);    exit();

//         $lead_stage = DB::connection($this->db2())->table("lead_stage")->get();
// echo "<pre>";
// print_r($this->getAllData('view','lead_stage'));
// exit();

        
        return view('lead_details3');
    }

    function addPcCompanyName(Request $request){

        $validator = Validator::make($request->all(), [
            'add_pc_company_name' => 'required',
            'add_pc_owner_name' => 'required',
            'add_pc_owner_mobile' => 'required',
            'add_pc_owner_whatsapp_no' => 'required',
            'add_pc_owner_email' => 'required',
            'add_pc_employee_name' => 'required',
            'add_pc_employee_mobile_no' => 'required',
            'add_pc_employee_whatsapp' => 'required',
            'add_pc_employee_email' => 'required',
            'branch_location'=>'required'
        ],
        [
            'add_pc_company_name.required' => 'PC company name is required',
            'add_pc_owner_mobile.required' => 'PC Owner mobile no. is required',
            'add_pc_owner_name.required' => 'PC Owner name is required',
            'add_pc_owner_whatsapp_no.required' => 'PC owner whatsapp no. is required',
            'add_pc_owner_email.required' => 'PC owner email is required',
            'add_pc_employee_name.required' => 'PC employee name is required',
            'add_pc_employee_mobile_no.required' => 'PC employee mobile no. is required',
            'add_pc_employee_whatsapp.required' => 'PC employee whatsapp no. is required',
            'add_pc_employee_email.required' => 'PC employee email is required',
            'branch_location.required' => 'Branch location is required'
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

  

    function addPcExcecutiveName(Request $request){

        $validator = Validator::make($request->all(), [            
            'add_exe_pc_employee_name' => 'required',
            'add_exe_pc_owner_mobile' => 'required',
            'add_exe_pc_whatsapp_no'=> 'required',
            'add_exe_pc_employee_email'=> 'required'
        ],
        [           
            'add_exe_pc_employee_name.required' => 'PC Employee name is required',
            'add_exe_pc_owner_mobile.required' => 'PC Owner mobile number is required',
            'add_exe_pc_whatsapp_no.required' => 'PC Whatsapp number is required',
            'add_exe_pc_employee_email.required' => 'PC Employee Email is required'
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new record.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addUnitInformation(Request $request){

        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'building' => 'required',
            'complex' => 'required',
            'add_sub_location' => 'required',
            'add_location' => 'required',
            'add_city' => 'required',
            'state_name' => 'required'            
        ],
        [
            'building.required' => 'Building name is required',
            'complex.required' => 'Complex name is required',
            'add_sub_location.required' => 'Sub loaction  is required',
            'add_location.required' => 'Location  is required',
            'add_city.required' => 'City is required',
            'state_name.required' => 'State is required'
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }


    function addUnitAmenity(Request $request){

        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'unit_amenity' => 'required'                    
        ],
        [
            'unit_amenity.required' => 'Amenity name is required',
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addBuildingAmenity(Request $request){

        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'building_amenity' => 'required'                    
        ],
        [
            'building_amenity.required' => 'Amenity name is required',
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addLocation(Request $request){
        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'add_state' => 'required',
            'add_city' => 'required',
            'add_location' => 'required',
            'add_sub_location' => 'required',           
        ],
        [
            'add_state.required' => 'State is required',
            'add_city.required' => 'City is required',
            'add_location.required' => 'Location is required',
            'add_sub_location.required' => 'Sub Location is required',
            
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new record.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }
  

    public function calculate_budget_break_up(Request $request)
    {
        // echo "<pre>"; print_r($request->all());
        $minimum_budget = $request->minimum_budget;
        $maximum_budget = $request->maximum_budget;
        $own_contribution_amount = $request->own_contribution_amount;
        $min = $minimum_budget;
        $max = $maximum_budget;
        $OCA = $own_contribution_amount;

        $TF = 13100;   //transfer fee
        $LF = 11000;   // legal charges

        $minPercent = ($min * 5 )/100;
        // echo "<pre>"; print_r($minPercent    );
        // if($minPercent <= $OCA){
        //     echo "valid";
        // }else{
        //     echo "Your OCA must be 5% of basic amount"; //exit();
        // }

        

        

        $loop = $max-$min  ;
        $strl = strlen($loop) - 2;
        $value = '1E'.($strl);
        $divide_and_multiply = (int)$value;
        // print_r($divide_and_multiply);
        // exit();


        $finalLoop = ($loop/$divide_and_multiply) + 1;



        // 
        $plus = array('000000');
        $val = array();

        for($i=0; $i <= $finalLoop ; $i++){
            array_push($val,$min + $plus[$i]);
            array_push($plus,$divide_and_multiply*$i);
        }

        $tableGeneratedFrom = array_unique($val);

        return view('requirement/budget_breakup_table',['tableGeneratedFrom'=>$tableGeneratedFrom,'OCA'=>$OCA,'TF'=>$TF,'LF'=>$LF,'minimum_budget'=>$minimum_budget,'maximum_budget'=>$maximum_budget,'own_contribution_amount'=>$own_contribution_amount]);

        // foreach($tableGeneratedFrom as $tblgen)
        // {
                
        //     $basic_amount = $tblgen; // sts  end 70l   interval 1lakh
        //     echo "<pre>"; echo "Basic Amount: "; print_r($basic_amount);
        //     $OCA =   $OCA; // own contribution amount
        //     echo "<pre>"; echo "OCA: "; print_r($OCA);
        // //     echo "<pre>"; echo "OCA in per "; print_r(($OCA*5)/100);

        //     $SDR = ($basic_amount * 4) /100;  //stamp duty and registration  4 %
        //     echo "<pre>"; echo "SDR: "; print_r($SDR);

        //     $TF = $TF;   //transfer fee
        //     echo "<pre>"; echo "TF: "; print_r($TF);
        //     $LF = $LF;   // legal charges
        //     echo "<pre>"; echo "LF: "; print_r($LF);
        //     $consultancyFee = ($basic_amount * 1) /100;  // 1%  brakerage fee
        //     echo "<pre>"; echo "consultancyFee: "; print_r($consultancyFee);
            
        //     if($OCA >     3000000){
        //         $registrationFee = 30000;
        //     }else{
        //         $registrationFee = $OCA / 100;
        //     }
        //     echo "<pre>"; echo "registrationFee: "; print_r($registrationFee);

        //     $otherChanges = $SDR + $TF + $LF + $consultancyFee + $registrationFee;
        //     echo "<pre>"; echo "otherChanges: "; print_r($otherChanges);

        //     $downPayment = $OCA - $otherChanges;
        //     echo "<pre>"; echo "downPayment: "; print_r($downPayment);

        // //    echo "<pre>"; echo "downPayment in per: "; print_r(($downPayment*5)/100);

        //     $loanAmount = $basic_amount - $downPayment;
        //     echo "<pre>"; echo "loanAmount: "; print_r($loanAmount);

        //     $packageAmount = ($loanAmount +  $downPayment + $otherChanges ) - $consultancyFee;
        //     echo "<pre>"; echo "packageAmount: "; print_r($packageAmount);
        
        //     $completePackage = $loanAmount +  $downPayment + $otherChanges;
        //     echo "<pre>"; echo "completePackage: "; print_r($completePackage);
        //     echo "<pre>";
        //     echo "-----------------------------";


        // }     

    }

    public function download_budget_break_up(Request $request)
    {
        $minimum_budget = $request->minimum_budget;
        $maximum_budget = $request->maximum_budget;
        $own_contribution_amount = $request->own_contribution_amount;
        $min = $minimum_budget;
        $max = $maximum_budget;
        $OCA = $own_contribution_amount;

        $TF = 13100;   //transfer fee
        $LF = 11000;   // legal charges

        $minPercent = ($min * 5 )/100;
        // echo "<pre>"; print_r($minPercent    );
        // if($minPercent <= $OCA){
        //     echo "valid";
        // }else{
        //     echo "Your OCA must be 5% of basic amount"; //exit();
        // }

        

        

        $loop = $max-$min  ;
        $strl = strlen($loop) - 2;
        $value = '1E'.($strl);
        $divide_and_multiply = (int)$value;
        // print_r($divide_and_multiply);
        // exit();


        $finalLoop = ($loop/$divide_and_multiply) + 1;



        // 
        $plus = array('000000');
        $val = array();

        for($i=0; $i <= $finalLoop ; $i++){
            array_push($val,$min + $plus[$i]);
            array_push($plus,$divide_and_multiply*$i);
        }

        $tableGeneratedFrom = array_unique($val);

        // print_r($data); exit();

            $pdf = PDF::loadView('requirement/download_budget_breakup',['data'=>$tableGeneratedFrom,'OCA'=>$OCA,'TF'=>$TF,'LF'=>$LF]);
      	    return $pdf->download('Budget-break-up.pdf');
    }



    function addResCity(Request $request){     

        $validator = Validator::make($request->all(), [
            'add_res_city' => 'required'                    
        ],
        [
            'add_res_city.required' => 'City is required',
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }


    function addCompanyCity(Request $request){     

        $validator = Validator::make($request->all(), [
            'add_company_city' => 'required'                    
        ],
        [
            'add_company_city.required' => 'City is required',
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addDesignation(Request $request){     

        $validator = Validator::make($request->all(), [
            'add_designation' => 'required'                    
        ],
        [
            'add_designation.required' => 'Designation is required',
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    function addIndustry(Request $request){     

        $validator = Validator::make($request->all(), [
            'add_industry_name' => 'required'                    
        ],
        [
            'add_industry_name.required' => 'Industry Name is required',
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }
}
