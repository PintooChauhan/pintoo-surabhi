<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class WingMasterController extends Controller
{
    public function index(Request $request)
    {
        
        if(isset( $request->society_id) && !empty( $request->society_id)){
            $db = env('operationDB');
            $buildingData =   DB::connection($db)->select("select building_name,society_id from op_society  where society_id  = ".$request->society_id." "); 
            // $project_amenities = $this->getAllData('o','project_amenities',null,$where);
            $Finalbuilding_name = $buildingData[0]->building_name;
            $Finalsociety_id = $buildingData[0]->society_id;

            $wingData =   DB::connection($db)->select("select * from op_wing  where society_id  = ".$Finalsociety_id." "); 
           // print_r($wingData);
           
        }else{            
            $Finalbuilding_name = '';
            $Finalsociety_id = '';
            $wingData = array();
        }
        // $wingData = array();
        return view('wing_master/wing_master',['Finalbuilding_name'=>$Finalbuilding_name,'Finalsociety_id'=>$Finalsociety_id,'wingData'=>$wingData,'society_id'=>$request->society_id]);
    }

    public function add_wing_details(Request $request)
    {

        // $getData =   DB::select("select u.unit_id, u.unit_code, u.wing_id from op_unit as u where u.wing_id=52"); 
        // echo "<table>";
        // $unitPerFloor = 8;
        // $i = 1;
        // foreach($getData as $getData){
        // //    for ($i=0; $i < 8 ; $i++) { 
        //        # code...
        //        echo "<td>";
        //     echo "<tr>";
        //    echo "<td>";
        //       //echo"<pre>"; print_r($getData);
        //    echo " ";    print_r($getData->unit_code);
        // //    print("</td>");		
        // //    if ($i == 8) {
        // //        echo "  break from here";
        // //        print("</td><td>");
        // //     //    break;
        // //     $i=0;
        // // }else{
        // //     echo "</td>";
        // // }
            
        //     // echo "\n";
        //     echo "</tr>";
        //     echo "</td>";
        //     $i++;
        // // } 
           
        // }
        // echo "</table>";
        // exit();
            
        if(isset( $request->wing_id) && !empty( $request->wing_id)){
            $wing_id = $request->wing_id;
            $db = env('operationDB');
            $wingData =   DB::connection($db)->select("select wing_id,society_id,wing_name,total_floors,total_units,units_per_floor from op_wing  where wing_id  = ".$wing_id." "); 
            $society_id = $wingData[0]->society_id;
            $wing_id = $wingData[0]->wing_id;
            $wing_name = $wingData[0]->wing_name;
            $total_floors = $wingData[0]->total_floors;
            $total_units = $wingData[0]->total_units;
            $units_per_floor = $wingData[0]->units_per_floor;
            
        }else{
            $wing_id = $wing_name = $total_floors =  $total_units =  $units_per_floor ='';
        }   
        $unit = $this->getAllData('d','unit');
        $configuration = $this->getAllData('d','configuration'); 
        return view('wing_master/wing_master1',['unit'=>$unit,'configuration'=>$configuration, 'society_id'=>$society_id,'wing_id'=>$wing_id,'wing_name'=>$wing_name,'total_floors'=>$total_floors,'total_units'=>$total_units,'units_per_floor'=>$units_per_floor]);
    }

    public function add_wing_master(Request $request)
    {
        // $error = array();
        // if( $request->wing_name=='' ){
        //     $msg = 'Wing name is required';
        //     array_push($error,$msg);
        //     return 0;
            
        // }else{
            
        //     print_r($request->wing_name);
        //     return 1;
        // }
        // echo "<pre>"; print_r($request->all()); exit();

        $validator = Validator::make($request->all(), [
            'wing_name.*' => 'required',
            'total_floor.*' => 'required',
            'haitable_floor.*' => 'required',
            'lifts.*'=>'required',
            
        ]
        ,
        [
            'wing_name.*.required' => 'Wing name is required',
            'total_floor.*.required' => 'Total floor is required',
            'haitable_floor.*.required' => 'Haitable floor is required' ,
            'lifts.*.required' => 'Lifts is required'         
        ]
    );

        if ($validator->passes()) {
            $data = array();    
            $society_id = $request->society_id;
            $wing_name = $request->wing_name;
            $total_floors = $request->total_floor;
            $habitable_floors = $request->haitable_floor;
            $units_per_floor = $request->unit_per_floor;            
            $total_units = $request->total_unit;
            $refugee_units = $request->refugee_unit;
            $no_of_lifts = $request->lifts;
            $comments = $request->comments;


            if($request->hasfile('floor_plan'))
            {
                $array = array();
                foreach($request->file('floor_plan') as $file)
                {
                    $rand1 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('floor_plan'), $rand1);
                   array_push($array,$rand1);
                    
                }
                // $data['floor_plan'] = json_encode($array);
            } 

            if($request->hasfile('name_board'))
            {
                $array1 = array();
                foreach($request->file('name_board') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('name_board'), $rand2);
                   array_push($array1,$rand2);
                    
                }
                // $data['name_board'] = json_encode($array1);

            }
            $cnt = count($wing_name);

            // print_r($cnt);
            $lastWingIds = array();   
            $society_id1 = $society_id;

            for ($i=0; $i<$cnt ; $i++) {  
               
                $wing_name1 = $wing_name[$i];
                $total_floors1 = $total_floors[$i];
                $habitable_floors1 = $habitable_floors[$i];
                $units_per_floor1 = $units_per_floor[$i]; 
                
                // $total_units1 = $total_units[$i];
                $refugee_units1 = $refugee_units[$i];
                $no_of_lifts1 = $no_of_lifts[$i];
                $comments1 = $comments[$i];
                $array = array('society_id'=>$society_id1,'wing_name'=>$wing_name1,'total_floors'=>$total_floors1,'habitable_floors'=>$habitable_floors1,'no_of_lifts'=>$no_of_lifts1);
             
                if(!empty($total_units[$i])){
                    $total_units1 = $total_units[$i]; 
                    $a = array('total_units'=>$total_units1) ;
                    $array=   array_merge($array,$a); 
                }

                if(!empty($units_per_floor[$i])){
                    $units_per_floor1 = $units_per_floor[$i]; 
                    $a = array('units_per_floor'=>$units_per_floor1) ;
                    $array=   array_merge($array,$a);                   
                }
                if(!empty($refugee_units[$i])){
                    $refugee_units1 = $refugee_units[$i]; 
                    $a = array('refugee_units'=>$refugee_units1) ;
                    $array=   array_merge($array,$a);                   
                }
                if(!empty($comments[$i])){
                    $comments1 = $comments[$i]; 
                    $a = array('comments'=>$comments1) ;
                    $array=   array_merge($array,$a);                   
                }         

                //  echo "<pre>"; print_r($last_id);  
               
                DB::table('op_wing')->insert($array); 
                $last_id = DB::getPDO()->lastInsertId();
                array_push($lastWingIds,$last_id);
                   

                  
                  
                        // exit();
                //  $cn = count($total_units);                
                   
                // break;
               
                
            }   
            
            // $cct = count($lastWingIds);
            // print_r($cct);
            foreach( $lastWingIds as $val ){
            for ($i=0; $i<$cnt ; $i++) {  
                $total_floors1 = $total_floors[$i];
                $units_per_floor1 = $units_per_floor[$i]; 
                for ($i=0; $i <= $total_floors1 ; $i++) { 
                    for ($ii=1; $ii <= $units_per_floor1 ; $ii++) { 
                           $un = 100 * $ii +$i;                       
                            $arr = array('unit_code'=>$un,'wing_id'=>$val,'floor_no'=>$i);
                            DB::table('op_unit')->insert($arr);               
                    }
                }
            }           
            }

            // exit();       
            return response()->json(['success'=>'done','lastID'=>$society_id1]);

        }    
        return response()->json(['error'=>$validator->errors()->all()]);
   }

   public function add_unit_info(Request $request)
   {
    //    echo "pr";
       //echo "<pre>"; print_r($request->all());//exit();


       $validator = Validator::make($request->all(), [
        'rera_carpet_area' => 'required',
        'unit_type' => 'required',
        'parking' => 'required',
        'parking_info' => 'required',
        'configuration' => 'required',
        'no_of_bathrooms' => 'required',
        ],
        [
            'rera_carpet_area.required' => 'Rera carpet area is required',
            'unit_type.required' => 'Unit type is required',
            'parking.required' => 'Parking is required',
            'parking_info.required' => 'Parking info is required',
            'configuration.required' => 'Configuration is required',
            'no_of_bathrooms.required' => 'No of bathrooms is required'           
        ]);

        if ($validator->passes()) {

            $floor_no = $request->floor_no;
            // [unit_available] => yes
            // [refugee_unit] => no
            // [property_no] => 1243
            // [floor_no] => 101
            // [rera_carpet_area] => 124
            // [rera_carpet_area_unit] => sq.ft
            // [mofa_carpet_area] => 124
            // [mofa_carpet_area_unit] => sq.ft
            // [door_facing_direction] => wedwqe
            // [configuration] => asd
            // [unit_type] => yes
            // [build_up_area] => dasd
            // [no_of_bedrooms] => 132
            // [no_of_bathrooms] => 123
            // [hall_size] => 213
            // [kitchen_size] => 1232
            // [parking] => 3123
            // [parking_info] => 123
            // [unit_view] => 123
            // [comment] => 123

       $wing_id = $request->wing_id;
       $society_id   = $request->society_id;
       $unit_available = $request->unit_available;
       $refugee_unit = $request->refugee_unit;     
       $property_no = $request->property_no;
       $floor_no = $request->floor_no;
       $rera_carpet_area = $request->rera_carpet_area;
       $mofa_carpet_area = $request->mofa_carpet_area;
       $door_facing_direction = $request->door_facing_direction;
       $configuration = $request->configuration;
       $unit_type = $request->unit_type;
       $build_up_area = $request->build_up_area;
       $no_of_bedrooms = $request->no_of_bedrooms;
       $no_of_bathrooms = $request->no_of_bathrooms;
       $hall_size = $request->hall_size;
       $kitchen_size = $request->kitchen_size;
       $parking = $request->parking;
       $parking_info = $request->parking_info;
       $unit_view = $request->unit_view;
       $comment = $request->comment;
            // 'hall_balcony'=>  ,'dryarea_bathroom','dryarea_kitchen'
       $data = array('refugeeflat'=>$refugee_unit,'reracarpet_area'=>$rera_carpet_area,'mofa_carpet_area'=>$mofa_carpet_area,'dfd'=>$door_facing_direction,'unit_type_id'=>0,
            'hallsize'=>$hall_size,'parking_id'=>$parking,'floor_no'=>$floor_no,'configuration_id'=>$configuration,'built_up_area'=>$build_up_area,
            'no_of_bathrooms'=>$no_of_bathrooms,'kitchen_size'=>$kitchen_size,'parking_info'=>$parking_info, 'unit_available'=>1,'unit_view'=>$unit_view
            );

            DB::table('op_unit')
              ->where('wing_id', $wing_id)
            //   ->where('society_id', $society_id)
              ->where('unit_code',$floor_no)
              ->update($data);

              exit();

            return response()->json(['success'=>'done']);

        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function add_unit_master_data(Request $request)
    {
        // echo "1";
        // echo "<pre>"; print_r($request->all());

        $html = '<div id="modal4" class="modal" style="width:84% !important">
        <div class="modal-content">
            <h5 style="text-align: center"><b>UNIT INFORMATION</b></h5>
            <hr>
        
            <form method="post" id="add_unit_info_form">@csrf
                <input type="text" value="" name="society_id">
                <input type="text" value="" name="wing_id">
            <div class="row" style="margin-right: 0rem !important">

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default" id="unit_available"  data-placeholder="Select" name="unit_available">
                        <option value="" disabled selected>Select</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <label for="property_seen" class="active">Unit Available ?</label>
                </div>

                <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default"  id="refugee_unit" onChange="hide_all_info()"  data-placeholder="Select" name="refugee_unit">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <label for="property_seen1" class="active">Refugee Unit</label>
                </div>
            </div>
            <div id="not_refugee">
                <div class="row" style="margin-right: 0rem !important" >  
            
                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Property No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="property_no" id="property_no"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Floor No.: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="floor_no" id="floor_no"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                            

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="numeric" class="input_select_size" name="rera_carpet_area" id="rera_carpet_area" required placeholder="Enter" >
                                <label>RERA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="rera_carpet_area_unit" name="rera_carpet_area_unit" class="select2 browser-default ">
                               
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="input-field col l3 m4 s12" id="InputsWrapper">
                        <div class="row">
                            <div class="input-field col m8 s8" style="padding: 0 10px;">
                            <input type="numeric" class="input_select_size" name="mofa_carpet_area" id="mofa_carpet_area" required placeholder="Enter"  >
                                <label>MOFA Carpet Area
                            </div>
                            <div class="input-field col m4 s4 mobile_view" style="padding: 0 10px;">
                                <select  id="mofa_carpet_area_unit" name="mofa_carpet_area_unit" class="select2 browser-default ">
                                
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Door Facing Direction: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="door_facing_direction" id="door_facing_direction"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Configuration: <span class="red-text">*</span></label>
                
                    <select  name="configuration" class="validate select2 browser-default">
                                  
                                </select>
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                
                    <div class="input-field col l3 m4 s12 ">
                    <select class="select2  browser-default"  id="re"  data-placeholder="Select" name="unit_type">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Residencial</option>
                        <option value="no">Commercial</option>
                    </select>
                    <label for="property_seen" class="active">Unit Type</label>
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Build Up Area: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="build_up_area" id="build_up_area"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">No.of Bedrooms with Sizes: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="no_of_bedrooms" id="no_of_bedrooms"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>


                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">No.of Bathrooms: <span class="red-text">*</span></label>
                    <input type="numeric" class="validate" name="no_of_bathrooms" id="no_of_bathrooms"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
            
                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Hall Size: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="hall_size" id="hall_size"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Kitchen Size: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="kitchen_size" id="kitchen_size"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
                </div>
                <div class="row" style="margin-right: 0rem !important">

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Parking : <span class="red-text">*</span></label>
                    <input type="numeric" class="validate" name="parking" id="parking"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Parking Info: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="parking_info" id="parking_info"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>
            

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">    Unit View: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="unit_view" id="unit_view"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                    <div class="input-field col l3 m4 s12 display_search">
                    <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                    <input type="text" class="validate" name="comment" id="comment"   placeholder="Enter">
                    <span class="add_pc_company_name_err"></span>                    
                    </div>

                </div>             
            </div>           
            <div class="alert alert-danger print-error-msg_add_unit_info" style="display:none">
            <ul style="color:red"></ul>
            </div>
            </div>
            
            <div class="modal-footer">
            <span class="errors" style="color:red"></span>
            <div class="row">
                <div class="input-field col l3 m3 s6 display_search">
                <button class="btn-small  waves-effect waves-light"  type="submit" name="action">Submit</button>                        
                </div>    
                
                <div class="input-field col l3 m3 s6 display_search">
                <button class=" modal-close waves-effect waves-green btn-small"  style="background-color: red;" type="button">Close</button>                        
                </div>    
            </div>    
            
            
                
            </div>
      
    </div> 
         ';
         return $html;
    }
    
}
