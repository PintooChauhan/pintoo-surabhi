<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;


class SellerController extends Controller
{
    public function index()
    {
        return view('seller');
    }

    public function addBuildingName(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'add_building' => 'required',            
            'add_complex' => 'required',
            'add_sub_location' => 'required',
            'add_location' => 'required',
            'add_city'=>'required',
            'add_state'=>'required',
        ],
        [
            'add_building.required' => 'Building Name is required',
            'add_complex.required' => 'Complex Name is required',
            'add_sub_location.required' => 'Sub Location is required',
            'add_location.required' => 'Location is required',
            'add_city.required' => 'City is required',
            'add_state.required' => 'State is required'
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    public function addWingName(Request $request)
    {
        $validator = Validator::make($request->all(), [
          
            'add_wing_name' => 'required',
          
        ],
        [
          
            'add_wing_name.required' => 'Wing is required',
          
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    public function addSuffixName(Request $request)
    {
        $validator = Validator::make($request->all(), [
          
            'add_suffix' => 'required',
          
        ],
        [
          
            'add_suffix.required' => 'Suffix Name is required',
          
            
        ]);        

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    
}
