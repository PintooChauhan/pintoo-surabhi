<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use PDF;


class UserMasterController extends Controller
{
    
    public function index(Request $request)
    {
        return view('user_master/new_user_list');
    }

    public function access_group()
    {
        return view('user_master/access_group');
    }

    public function new_user()
    {
        return view('user_master/new_user');
    }

}
