<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class SocietyMasterController extends Controller
{
    public function index(Request $request)
    {   
        if(isset( $request->pid) && !empty( $request->pid)){
            $where = 'project_id = '.$request->pid;
            $db = env('operationDB');
            $project_amenities =   DB::connection($db)->select("select pm.*,am.amenities  from op_project_amenities as pm join dd_project_amenities as am on am.project_amenities_id = pm.amenities_id  where pm.project_id = ".$request->pid." "); 
            // $project_amenities = $this->getAllData('o','project_amenities',null,$where);
            $project_id = $request->pid;
        }else{
            $project_amenities = array();
            $project_id = '';
        }
        // echo "<pre>"; print_r($project_amenities); exit();

        $location = $this->getAllData('d','location' );
        $sub_location = $this->getAllData('d','sub_location');
        $city = $this->getAllData('d','city');
        $state = $this->getAllData('d','state');
        $unit = $this->getAllData('d','unit');
        $unit_status = $this->getAllData('d','unit_status');
        $category = $this->getAllData('d','category');
        $parking = $this->getAllData('d','parking');   
        $rating = $this->getAllData('d','rating');      
        $configuration = $this->getAllData('d','configuration');  
        $configuration_size = $this->getAllData('d','configuration_size');  
        $construction_technology = $this->getAllData('d','construction_technology');
        $preferred_tenant = $this->getAllData('d','preferred_tenant');
        $building_type = $this->getAllData('d','building_type');
        $payment_type = $this->getAllData('d','payment_type');

        $initials = $this->getAllData('d','initials');
        $country_code = $this->getAllData('d','country');
        $designation = $this->getAllData('d','designation');
        $employee = $this->getAllData('o','employee');
        $company_hirarchy =  array();
        
        
        $dataArray = array('project_id'=>$project_id,'location'=>$location,'sub_location'=>$sub_location,'city'=>$city,'state'=>$state,'unit'=>$unit,'project_amenities'=>$project_amenities,'unit_status'=>$unit_status,'category'=>$category,'parking'=>$parking,'rating'=>$rating,'configuration'=>$configuration,'configuration_size'=>$configuration_size,'construction_technology'=>$construction_technology,'preferred_tenant'=>$preferred_tenant,'building_type'=>$building_type,'building_type1'=>$building_type,'payment_type'=>$payment_type,'initials'=>$initials,'country_code'=>$country_code,'designation'=>$designation,'company_hirarchy'=>$company_hirarchy,'employee'=>$employee);
        return view('society_master/society_master',$dataArray);
    }

    public function add_society_master(Request $request)
    {
 
    //    echo "<pre>"; print_r($request->all());exit();
        
        $validator = Validator::make($request->all(), [
            'building_name' => 'required',
            'unit_status' => 'required',
            'building_status' => 'required',
            'building_category' => 'required',
            'society_geo_location_lat' => 'required',
            'society_geo_location_long' => 'required',
            'location' => 'required',
            'sub_location' => 'required',
            'city' => 'required',
            'state' => 'required',
            'parking' => 'required',
                       
            
        ]
     );
     if ($validator->passes()) {
        $data = array(); 
        $data['project_id'] = 17;
        $data['building_owner'] = $request->building_owner;
        $data['society_status_id'] = 1;// $request->building_status;
        $data['unit_status'] = $request->unit_status;
        $data['building_description'] = $request->building_description;
        $data['building_name'] = $request->building_name;
        $data['building_code'] = $request->building_code;
        $data['category_id'] = $request->building_category;
        $data['building_land_parcel_area'] = $request->building_land_parcel_area;
        //building_land_parcel_area_unit] => sq.ft
        $data['building_construction_area'] = $request->building_constructed_area;
        //building_constructed_area_unit] => sq.ft
        $data['building_open_space'] = $request->building_open_space;
        //building_open_space_unit] => sq.ft
        $data['building_type_id'] = $request->building_type;
        $data['parking'] =json_encode( $request->parking);

        $data['location_id'] = $request->location;
        $data['sub_location_id'] = $request->sub_location;
        $data['city_id'] = $request->city;
        $data['state_id'] = $request->state;
        $data['construction_technology'] = $request->construction_technology;
        $data['lattitde'] = $request->society_geo_location_lat;
        $data['longitude'] = $request->society_geo_location_long;
        $data['maintenance_amount'] = $request->maintenance_amount;
        $data['building_rating'] = $request->building_rating;
        $data['pros'] = $request->pros_of_building;
        $data['cons'] = $request->cons_of_building;

        
        $data['payment_type'] = json_encode($request->payment_plan);
        $data['brokarage_comments'] = $request->brokarage_comments;
        $data['funded_by'] = $request->funded_by;
        $data['construction_stage'] = $request->building_construction_stage;
        $data['construction_stage_date'] = $request->building_construction_stage_date;
        $data['building_launch_date'] = $request->buulding_lauch_date;
        $data['builder_completion_year'] = $request->builder_completion_year;
        $data['rera_completion_year'] = $request->rera_completion_year;
        $data['comment'] = $request->comment;
        $data['building_video_links'] = $request->building_video_links;
        $data['building_config_details'] = $request->building_config_details;

         // for single image upload
        if( $request->hasfile('oc_certificate') )
        { 
            $rand1 = mt_rand(99,9999999).".".$request->oc_certificate->extension();         
            $request->oc_certificate->move(public_path('images'), $rand1);
            $data['oc_certificate'] = $rand1;
        }

        if( $request->hasfile('registration_certificate') )
        { 
            $rand2 = mt_rand(99,9999999).".".$request->registration_certificate->extension();         
            $request->registration_certificate->move(public_path('images'), $rand2);
            $data['registration_certificate'] = $rand2;
        }

        if( $request->hasfile('conveyance_deed') )
        { 
            $rand3 = mt_rand(99,9999999).".".$request->conveyance_deed->extension();         
            $request->conveyance_deed->move(public_path('images'), $rand3);
            $data['conveyance_deed'] = $rand3;
        }
        
        if( $request->hasfile('rera_certificate') )
        { 
            $rand4 = mt_rand(99,9999999).".".$request->rera_certificate->extension();         
            $request->rera_certificate->move(public_path('images'), $rand4);
            $data['rera_certificate'] = $rand4;
        }




        //reg 
        // oc_certificate
        // registration_certificate
        // conveyance_deed  

        // uc
        // rera_certificate

        // mul
        // construction_stage_images
        // flat_images
        // building_images
        //building_video


        DB::table('op_society')->insert($data); 
        $last_id =DB::getPDO()->lastInsertId();
        // print_r( $last_id );

                // for multiple image upload
                if($request->hasfile('construction_stage_images'))
                {
                    foreach($request->file('construction_stage_images') as $file)
                    {
                        $rand5 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('images'), $rand5);
                        $photoData5 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand5,'meta_key'=>'construction_stage_images');
                        DB::table('op_society_photos')->insert($photoData5);      
                    }
                }

                if($request->hasfile('flat_images'))
                {
                    foreach($request->file('flat_images') as $file)
                    {
                        $rand6 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('images'), $rand6);
                        $photoData6 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand6,'meta_key'=>'flat_images');
                        DB::table('op_society_photos')->insert($photoData6);      
                    }
                }

                if($request->hasfile('building_images'))
                {
                    foreach($request->file('building_images') as $file)
                    {
                        $rand7 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('images'), $rand7);
                        $photoData7 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand7,'meta_key'=>'building_images');
                        DB::table('op_society_photos')->insert($photoData7);      
                    }
                }

                if($request->hasfile('building_video'))
                {
                    foreach($request->file('building_video') as $file)
                    {
                        $rand8 = mt_rand(99,9999999).".".$file->extension();
                        $file->move(public_path('images'), $rand8);
                        $photoData8 = array('society_id'=>$last_id,'type'=>'photo','url'=>$rand8,'meta_key'=>'building_video');
                        DB::table('op_society_photos')->insert($photoData8);      
                    }
                }
                //stoed all imgaes in sepaarte table with name

        // amenities 

        $is_available = $request->is_available;
        $description_ame = $request->description_ame;
        $cnt = count($is_available);
        // echo "<pre>"; print_r($is_available); 
        for ($i=0; $i<$cnt ; $i++) { 
            $is_available1 = $is_available[$i];
             $description_ame1 = $description_ame[$i];
            $array = array();

            if(!empty($is_available1)){
                // $is_available1 = $is_available[$i]; 
                $is_available2 = $is_available[$i] == 0 ? 'N': 'Y';
                 //   $a = array('society_id'=>$last_id,'is_available'=>$is_available2,'amenities_id'=>$is_available1,'description'=>$description_ame1) ;
                   // $array=   array_merge($array,$a);                       
                // }
                // echo "<pre>"; print_r($array); 
                //DB::table('op_society_amnities')->insert($array); 
            }     
        }

       
        $floor_slab_to = $request->floor_slab_to;
        $floor_slab_from = $request->floor_slab_from;
        $configuration1 = $request->configuration1;
        $rs_per_sq_ft = $request->rs_per_sq_ft;
        $box_price = $request->box_price;
        
        $cnt = count($floor_slab_to);
        for ($i=0; $i<$cnt ; $i++) {  
            $array = array('society_id'=>$last_id);

            $floor_slab_to1 = $floor_slab_to[$i];
            $floor_slab_from1 = $floor_slab_from[$i];
            $configuration11 = $configuration1[$i];
            $rs_per_sq_ft1 = $rs_per_sq_ft[$i];
            $box_price1 = $box_price[$i];

            if(!empty($floor_slab_to[$i])){
                $floor_slab_to1 = $floor_slab_to[$i]; 
                $a = array('floor_slab_to'=>$floor_slab_to1) ;
                $array=   array_merge($array,$a); 
            }

            if(!empty($floor_slab_from[$i])){
                $floor_slab_from1 = $floor_slab_from[$i]; 
                $a = array('floor_slab_from'=>$floor_slab_from1) ;
                $array=   array_merge($array,$a); 
            }

            if(!empty($configuration1[$i])){
                $configuration11 = $configuration1[$i]; 
                $a = array('configuration'=>$configuration11) ;
                $array=   array_merge($array,$a); 
            }

            if(!empty($rs_per_sq_ft[$i])){
                $rs_per_sq_ft1 = $rs_per_sq_ft[$i]; 
                $a = array('rs_per_sq_ft'=>$rs_per_sq_ft1) ;
                $array=   array_merge($array,$a); 
            }

            if(!empty($box_price[$i])){
                $box_price1 = $box_price[$i]; 
                $a = array('box_price'=>$box_price1) ;
                $array=   array_merge($array,$a); 
            }
            DB::table('op_society_floor_rise')->insert($array);
        }

            $zero_to_two = $request->zero_to_two;
            $three_to_five = $request->three_to_five;
            $six_to_ten = $request->six_to_ten;
            $above_eleven = $request->above_eleven;
            $brokerageData = array('society_id'=>$last_id,'zero_to_two'=>$zero_to_two,'three_to_five'=>$three_to_five,'six_to_ten'=>$six_to_ten,'above_eleven'=>$above_eleven);
            // DB::table('op_society_brokerage_fee')->insert($brokerageData); 
            $sample_flat_available = $request->sample_flat_available;
            $configuration = $request->configuration;
            $configuration_size = $request->configuration_size;
            $area = $request->area;
            $total_falt_available = $request->total_falt_available;
            $flat_available = $request->flat_available;

            $cnt1 = count($sample_flat_available);
            for ($ii=0; $ii<$cnt1 ; $ii++) {  
                $sample_flat_available1 = $sample_flat_available[$ii];
                $configuration1 = $configuration[$ii];
                $configuration_size1 = $configuration_size[$ii];
                $area1 = $area[$ii];
                $total_falt_available1 = $total_falt_available[$ii];
                $flat_available1 = $flat_available[$ii];
                $array1 = array('society_id'=>$last_id);

                if(!empty($sample_flat_available[$i])){
                    $sample_flat_available1 = $sample_flat_available[$i]; 
                    $a1 = array('sample_flat_available'=>$sample_flat_available1) ;
                    $array1 = array_merge($array1,$a1);                   
                }

                if(!empty($configuration[$i])){
                    $configuration1 = $configuration[$i]; 
                    $a1 = array('configuration_id'=>$configuration1) ;
                    $array1 = array_merge($array1,$a1);                   
                }

                if(!empty($configuration_size[$i])){
                    $configuration_size1 = $configuration_size[$i]; 
                    $a1 = array('suffix'=>$configuration_size1) ;
                    $array1 = array_merge($array1,$a1);                   
                }

                if(!empty($area[$i])){
                    $area1 = $area[$i]; 
                    $a1 = array('area'=>$area1) ;
                    $array1 = array_merge($array1,$a1);                   
                }
                
                if(!empty($total_falt_available[$i])){
                    $total_falt_available1 = $total_falt_available[$i]; 
                    $a1 = array('total_falt_available'=>$total_falt_available1) ;
                    $array1 = array_merge($array1,$a1);                   
                }

                if(!empty($flat_available[$i])){
                    $flat_available1 = $flat_available[$i]; 
                    $a1 = array('flat_available'=>$flat_available1) ;
                    $array1 = array_merge($array1,$a1);                   
                }
                DB::table('op_society_configuration')->insert($array);    

            }



            $name_initial = $request->name_initial;
            $name_h = $request->name_h;
            $country_code = $request->country_code;
            $mobile_number_h = $request->mobile_number_h;
            $country_code_w = $request->country_code_w;
            // $wap_number = $request->wap_number;
            $primary_email_h = $request->primary_email_h;
            $designation_h = $request->designation_h;
            // $reporting_to_h = $request->reporting_to_h;
            // $comments_h = $request->comments_h;
        
            // if($request->hasfile('photo_h'))
            // {
            //     $arrayH = array();
            //     foreach($request->file('photo_h') as $file)
            //     {
            //         $rand_h = mt_rand(99,9999999).".".$file->extension();
            //         $file->move(public_path('images'), $rand_h);
            //             array_push($arrayH,$rand_h);
                    
            //     }
            //     // $data2['awards_reco_image'] = ($array);
            // }
        
            $cnt = count($name_initial);
            // echo "<pre>"; print_r($designation_h); //exit();
            for ($i=0; $i<$cnt ; $i++) {  
                $initial = $name_initial[$i];
                $name= $name_h[$i];
                $country_code_id = $country_code[$i];
                $mobile_number = $mobile_number_h[$i];
                // $country_code_w = $country_code_w[$i];
                // $whatsapp_number = $wap_number[$i];
                $email_id = $primary_email_h[$i];
                $designation_id = $designation_h[$i];
                // $reports_to = $reporting_to_h[$i];
                // $comments = $comments_h[$i];
                $array = array('society_id'=>$last_id);
        
                if(!empty($name[$i])){
                    $name = $name_h[$i]; 
                    $a = array('name'=>$name) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($country_code_id[$i])){
                    $country_code_id = $country_code[$i]; 
                    $a = array('country_code_id'=>$country_code_id) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($mobile_number[$i])){
                    $mobile_number = $mobile_number_h[$i]; 
                    $a = array('mobile_number'=>$mobile_number) ;
                    $array=   array_merge($array,$a);                   
                }
        
                // if(!empty($whatsapp_number[$i])){
                //     $whatsapp_number = $wap_number[$i]; 
                //     $a = array('whatsapp_number'=>$whatsapp_number) ;
                //     $array=   array_merge($array,$a);                   
                // }
        
                if(!empty($email_id[$i])){
                    $email_id = $primary_email_h[$i]; 
                    $a = array('email_id'=>$email_id) ;
                    $array=   array_merge($array,$a);                   
                }
        
                if(!empty($designation_id[$i])){
                    $designation_id = $designation_h[$i]; 
                    $a = array('designation_id'=>$designation_id) ;
                    $array=   array_merge($array,$a);                   
                }        
       
                DB::table('op_society_keymembers')->insert($array);    

                $dataAM = array('society_id'=>$last_id,'employee_id'=>$request->account_manager);
                DB::table('op_society_account_manager')->insert($dataAM);                

            }
            return response()->json(['success'=>'done','lastID'=>$last_id]);
       
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function append_society_hierarchy_form(Request $request)
    {
        // echo "<pre>"; print_r($request->all());
        $cnt  = $request->aa;
        $initials = $this->getAllData('d','initials');
        $designation = $this->getAllData('d','designation');
        $country_code = $this->getAllData('d','country');
        $db = env('operationDB');
        $company_hirarchy =   DB::connection($db)->select("select company_hirarchy_id,name  from op_company_hirarchy");
        $html = '<div class="row"><div class="col l11"><div class="row"><div class="input-field col l3 m4 s12" id="InputsWrapper2"><label for="cus_name active" class="dopr_down_holder_label active">Name <span class="red-text">*</span></label><div  class="sub_val no-search"><select  id="name_initial_'.$cnt.'" name="name_initial[]" class="select2 browser-default">
        ';
            foreach($initials as $ini){
                $html .= '<option value="'.$ini->initials_id.'">'.ucfirst($ini->initials_name).'</option>';
            }
        // <option value="1">Mr.</option><option value="2">Miss.</option><option value="3">Mrs.</option>
        $html .='</select></div><input type="text" class="validate mobile_number" name="name_h[]" id="name_h"  placeholder="Enter"  ></div><div class="input-field col l3 m4 s12" id="InputsWrapper"><label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label><div  class="sub_val no-search"><select  id="country_code_'.$cnt.'" name="country_code[]" class="select2 browser-default">';
            foreach($country_code as $cou){
                $html .= '<option value="'.$cou->country_code_id.'">'.ucfirst($cou->country_code).'</option>';
            }
        // <option value="1">+91</option><option value="2">+1</option>
        $html .= '</select></div><input type="text" class="validate mobile_number" name="mobile_number_h[]" id="mobile_number"  placeholder="Enter" ></div>';
        // $html .='<div class="input-field col l3 m4 s12" id="InputsWrapper2"><div  class="sub_val no-search"><select  id="country_code_w_'.$cnt.'" name="country_code_w[]" class="select2 browser-default">';
        //         foreach($country_code as $cou){
        //             $html .= '<option value="'.$cou->country_code_id.'">'.ucfirst($cou->country_code).'</option>';
        //         }
        // $html .= '</select></div><label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label> <input type="text" class="validate mobile_number" name="wap_number[]" id="wap_number"  placeholder="Enter" ><div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " ><input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto"></div></div>';
        $html .= '<div class="input-field col l3 m4 s12" id="InputsWrapper3"><label for="primary_email active" class="active">Email Address: <span class="red-text">*</span></label><input type="email" class="validate" name="primary_email_h[]" id="primary_email"  placeholder="Enter" ></div>';
        $html .=' <div class="input-field col l3 m4 s12 display_search"><select class="select2  browser-default designation_h"  data-placeholder="Select" id="designation_'.$cnt.'" name="designation_h[]">';
            foreach($designation as $des){
                $html .= '<option value="'.$des->designation_id.'">'.ucfirst($des->designation_name).'</option>';
            }
        $html .='</select><label for="designation" class="active">Designation </label><div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" ><a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a></div></div></div></div>';    
        // $html .='<div class="input-field col l3 m4 s12 display_search"><select class="select2  browser-default"  data-placeholder="Select" id="reporting_to_'.$cnt.'" name="reporting_to_h[]"><option value=""></option>';
        //     foreach($company_hirarchy as $comp){
        //         $html .= '<option value="'.$comp->company_hirarchy_id.'">'.ucfirst($comp->name).'</option>';
        //     }
        // $html .='</select><label for="reporting_to" class="active">Reporting to </label></div><div class="input-field col l3 m4 s12" id="InputsWrapper2"><label  class="active">Comments: <span class="red-text">*</span></label>
        // <input type="text" class="validate" name="comments_h[]" id="comments_h[]"  placeholder="Enter" ></div><div class="input-field col l3 m4 s12 display_search"><label  class="active">Upload Photo : </label><input type="file" name="photo_h[]"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;"></div></div></div>';    
        $html .= ' <div class="col l1"><a href="#" style="color: red !important;font-size: 23px" class="removeclassCompany waves-effect waves-light">-</a><a href="#" id="add_location" class="modal-trigger" title="Action"  style="color: grey !important;font-size: 23px;"> <i class="material-icons  dp48">group</i></a><a href="#modal10" id="add_location" title="View Flow Chart" class="modal-trigger" style="color: grey !important;font-size: 23px;"> <i class="material-icons  dp48">remove_red_eye</i></a></div>';
        return $html;
    }


    public function append_config_sample(Request $request)
    {
        // print_r();
        $increment = $request->aa;
        $configuration = $this->getAllData('d','configuration');  
        $configuration_size = $this->getAllData('d','configuration_size');

        $html = '<tr>
        <td style="width: 21%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
               <select  id="sample_flat_available_'.$increment.'" name="sample_flat_available[]" class="validate select2 browser-default">
                    <option value="1" >Yes</option>                                    
                    <option value="0" >No</option>
                </select>
            <div>
        </td>
        <td style="width: 12.2%;">  
             <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
               <select  name="configuration[]" id="configuration_'.$increment.'" class="validate select2 browser-default">
                   ';
                  foreach($configuration as $configuration){

                    $html .='<option value="'.$configuration->configuration_id.'"> '. ucwords($configuration->configuration_name).'</option>';                                    
                  }
                  
            $html .='</select>
            <div>
        </td>                        
        <td style="width: 9%;">
                <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
                    <select   name="configuration_size[]" id="configuration_size'.$increment.'" class="validate select2 browser-default">
                        ';
                        foreach($configuration_size as $configuration_size){
                        $html .='<option value="'.$configuration_size->configuration_size_id.'">'. ucwords($configuration_size->configuration_size).'</option>';
                        }
                $html .='</select>
               <div>
        </td>
        <td style="width: 12.7%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                <input type="text" class="validation" name="area[]"   placeholder="Enter" >
            </div>
        </td>
        <td style="width: 12.7%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                <input type="text" class="validation" name="total_falt_available[]"   placeholder="Enter" >
            </div>
        </td>  
        <td style="width: 12.7%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                <input type="text" class="validation" name="flat_available[]"   placeholder="Enter" >
            </div>
        </td>                        
        <td></td>
        <td style="width: 9%;">
        
        <a href="#" style="color: red !important;font-size: 23px" class="removeclassConfig waves-effect waves-light">-</a>
                        <!-- <a href="javascript:void(0)" id="hap" class="clonable-button-add" title="Add" style="color: red !important"> <i class="material-icons  dp48">add</i></a> 
                        <a href="javascript:void(0)" class="clonable-button-close" title="Remove" style="color: red !important; display: none;"> <i class="material-icons  dp48">remove</i></a>  -->
        </td>
        </tr>';
        echo $html;
    }
}
