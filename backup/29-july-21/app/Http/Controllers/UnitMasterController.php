<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UnitMasterController extends Controller
{
    public function index(Request $request)
    {


        
        if(isset( $request->society_id) && !empty( $request->society_id)){    
            // $db = env('operationDB');
            $getData =   DB::select("select s.building_name , w.* from op_wing as w join op_society as s on s.society_id=w.society_id where w.society_id  = ".$request->society_id." "); 
        }else{
            $getData = array();
        }
        // print_r($getData);
        $unit = $this->getAllData('d','unit');
        $room = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');
        return view('unit_master/unit_master',['unit'=>$unit,'getData'=>$getData,'room'=>$room,'direction'=>$direction]);
    }

    public function add_unit_master(Request $request)
    {
        // echo 1;
        // echo "<pre>"; print_r($request->all());

        $validator = Validator::make($request->all(), [
            'rera_carpet_area' => 'required',
            'mofa_carpet_area' => 'required',
            'direction_id' => 'required',
            'configuration' => 'required',
            'unit_types' => 'required',
            'build_up_area' => 'required',
            'no_of_bedrooms_with_sizes' => 'required',
            'no_of_bathrooms' => 'required',
            'hall_size' => 'required',
            'kitchen_size' => 'required',
            'parking' => 'required',
            'parking_info' => 'required',
            'comment' => 'required'
        ]
     );
     if ($validator->passes()) {

            $data['rera_carpet_area'] = $request->rera_carpet_area;
            $data['mofa_carpet_area'] = $request->mofa_carpet_area;
            $data['direction_id'] = $request->direction_id;
            $data['configuration_id'] = $request->configuration;
            $data['unit_type'] = $request->unit_types;
            $data['build_up_area'] = $request->build_up_area;
            $data['no_of_bedrooms_with_sizes'] = $request->no_of_bedrooms_with_sizes;
            $data['no_of_bathrooms'] = $request->no_of_bathrooms;
            $data['hall_size'] = $request->hall_size;
            $data['kitchen_size'] = $request->kitchen_size;
            $data['parking_id'] = $request->parking;
            $data['parking_info'] = $request->parking_info;
            $data['unit_view'] = $request->unit_view;
            $data['comment'] = $request->comment;
            $data['refugee_unit'] = $request->refugee_unit;
            $data['unit_available'] = $request->unit_available;

            $property_no = $request->property_no;
            $floor_no = $request->floor_no;

            DB::table('op_unit')->where('unit_code',$property_no)->where('floor_no',$floor_no)->update($data);

        

        return response()->json(['success'=>'done']);

    }
    return response()->json(['error'=>$validator->errors()->all()]);

    }
    
    public function append_unit_structure1(Request $request)
    {
            //  echo 1;
        if(isset( $request->wingId) && !empty( $request->wingId)){
            $wing_id = $request->wingId;
            $db = env('operationDB');
            $wingData =   DB::connection($db)->select("select wing_id,society_id,wing_name,total_floors,total_units,units_per_floor from op_wing  where wing_id  = ".$wing_id." "); 
            $society_id = $wingData[0]->society_id;
            $wing_id = $wingData[0]->wing_id;
            $wing_name = $wingData[0]->wing_name;
            $total_floors = $wingData[0]->total_floors;
            $total_units = $wingData[0]->total_units;
            $units_per_floor = $wingData[0]->units_per_floor;
            
        }else{
            $wing_id = $wing_name = $total_floors =  $total_units =  $units_per_floor ='';
        }
    
        $html = '<div class="row">
        <div class="col l9 m9">
        <table class="bordered" style="overflow-x:auto;">
            <thead>
            <tr style="color: white;background-color: #ffa500d4;">       
                <!-- <th >Floor No</th> 
                <th>No of Flats</th> -->
            </tr>
            </thead>
            <tbody>
            <tr>
          
                <td>Floor 12</td>                        
                <td>10</td>  
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1201)">1201</td>
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1202)">1202</td> 
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1203)">1203</td>  
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1204)">1204</td>
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1205)">1205</td>  
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1206)">1206</td>
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1207)">1207</td>  
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1208)">1208</td>
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1209)">1209</td>  
                <td href="#modal4" class="modal-trigger" onClick="setModalValues(1210)">1210</td>            
            </tr>
            
        


           <!-- <br> <tr style="border-top: solid;">
          
                <td>Total</td>                        
                <td>32</td>  
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>
                <td><a href="#modal4" class="modal-trigger" onClick="setModalValues()" >Edit All Floor</a></td>

            
            </tr> --!>


            
            </tbody>
        </table>
        </div>
        <div class="col l3 m3">
            <div class="row" style="margin-right: 0rem !important">

                <div class="input-field col l6 m6 s12 ">
                    <select class="select2  browser-default" onChange="" id="property_seen1"  data-placeholder="Select" name="property_seen">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <label for="property_seen" class="active">Unit Available ?</label>
                </div>

                <div class="input-field col l6 m6 s12 ">
                    <select class="select2  browser-default" onChange="" id="r1e"  data-placeholder="Select" name="property_seen">
                        <option value="" disabled selected>Select</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <label for="property_seen1" class="active">Refugee Unit</label>
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Property No.: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Floor No.: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="floor_no" id="floor_no"   placeholder="Enter" >
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">RERA Carpet Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">MOFA Carpet Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Door Facing Direction: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Configuration: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 ">
                <select class="select2  browser-default" onChange="" id="re"  data-placeholder="Select" name="property_seen">
                    <option value="" disabled selected>Select</option>
                    <option value="yes">Residencial</option>
                    <option value="no">Commercial</option>
                </select>
                <label for="property_seen" class="active">Unit Type</label>
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Build Up Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">No.of Bedrooms with Sizes: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>


                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">No.of Bathrooms: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Hall Size: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Kitchen Size: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Parking : <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Parking Info: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">    Unit View: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_amenity" id="unit_amenity"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

            </div>
        </div>
    </div>';

        return $html;
    }

    public function append_unit_structure(Request $request)
    {   

        if(isset( $request->wingId) && !empty($request->wingId)){
            // echo $request->wingId;
            $wing_id = $request->wingId;
            $db = env('operationDB');
            $wingData =   DB::connection($db)->select("select wing_id,society_id,wing_name,total_floors,total_units,units_per_floor from op_wing  where wing_id  = ".$wing_id." "); 
            $society_id = $wingData[0]->society_id;
            $wing_id = $wingData[0]->wing_id;
            $wing_name = $wingData[0]->wing_name;
            $total_floors = $wingData[0]->total_floors;
            $total_units = $wingData[0]->total_units;
            $units_per_floor = $wingData[0]->units_per_floor;
            
        }else{
            $wing_id = $wing_name = $total_floors =  $total_units =  $units_per_floor ='';
        }
        $html = array();
        
        for($ii=1; $ii<=$total_floors; $ii++ ){
        $html = '
        <div class="row">
                <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                    <input type="text"   value="Floor '.$ii.'"  style="text-align: center;border: solid 1px;color: orange;font-weight: 700;"   >
                </div>';
                for($i=1; $i<=$units_per_floor; $i++ ){ 
                 $un = 100 * $ii +$i;
                //  $ar = json_encode($un.'#'.$wing_id);
               $html .='<div class="input-field col l1 m4 s12" id="InputsWrapper2">
                    <a href="/unit_master_update_info/?property_no='.$un.'&wing_id='.$wing_id.' "  target="_blank" >
                        <input type="text"   value="'.$un.'"  style="text-align: center;"   >
                    </a>    
                </div>';
                }
                $html .='
                <div class="input-field col l1 m4 s12" id="InputsWrapper2">
                    <input type="text"   value="Total Unit : '.$units_per_floor.'"  style="text-align: center;"   >
                </div> 

        </div>';
        echo  $html;
        }
       
    }

    public function append_unit_structure_side_view(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); exit();
        $param1 = $request->params1; // wing _id  // floor
        $param2 = $request->params2;
        $room = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');
        $configuration = $this->getAllData('d','configuration');
        $html = '<input type="hidden" value="'.$param1.'"> <input type="hidden" value="'.$param2.'">
        <div class="row" style="margin-right: 0rem !important">
            <div class="input-field col l6 m6 s12 ">
                <select class="validate select2  browser-default"  id="unit_available"  data-placeholder="Select" name="unit_available">
                    <option value="" disabled selected>Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <label for="property_seen" class="active">Unit Available ?</label>
            </div>

            <div class="input-field col l6 m6 s12 ">
                <select class=" validate select2  browser-default"  id="refugee_unit"  data-placeholder="Select" name="refugee_unit" onchange="hide_all_info()">
                    <option value="" disabled selected>Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <label for="property_seen1" class="active">Refugee Unit</label>
            </div>
        </div>
        
        <div id="not_refugee">
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Property No.: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="property_no" id="property_no"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Floor No.: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="floor_no" id="floor_no" value="'.$param1.'" readonly placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important">
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">RERA Carpet Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="rera_carpet_area" id="rera_carpet_area"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">MOFA Carpet Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="mofa_carpet_area" id="mofa_carpet_area"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important">     
                <div class="input-field col l6 m6 s12 display_search">
                        <select class="validate select2 browser-default" id="direction" data-placeholder="Company name, Branch name" name="direction">
                        <option value="option 1" >Select</option>';
                            foreach($direction as $direction){
                            $html .=' <option value="'.$direction->direction_id .'">'. ucfirst($direction->direction) .'</option>';
                            }
                    $html .='</select>
                    <label class="active">Door Facing Direction</label>
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                    <select class="validate select2 browser-default" id="configuration" data-placeholder="Company name, Branch name" name="configuration">
                        <option value="option 1" >Select</option>';
                            foreach($configuration as $configuration){
                            $html .=' <option value="'.$configuration->configuration_id .'">'. ucfirst($configuration->configuration_name) .'</option>';
                            }
                    $html .='</select>
                    <label class="active">Configuration</label>               
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important">                
                <div class="input-field col l6 m6 s12 ">
                <select class="select2  browser-default" onChange="" id="unit_type"  data-placeholder="Select" name="unit_types">
                    <option value="" disabled selected>Select</option>
                    <option value="yes">Residencial</option>
                    <option value="no">Commercial</option>
                </select>
                <label for="property_seen" class="active">Unit Type</label>
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Build Up Area: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="build_up_area" id="build_up_area"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important">                
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">No.of Bedrooms with Sizes: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="no_of_bedrooms_with_sizes" id="no_of_bedrooms_with_sizes"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>


                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">No.of Bathrooms: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="no_of_bathrooms" id="no_of_bathrooms"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important"> 
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Hall Size: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="hall_size" id="hall_size"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Kitchen Size: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="kitchen_size" id="kitchen_size"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important"> 
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Parking : <span class="red-text">*</span></label>
                <input type="text" class="validate" name="parking" id="parking"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Parking Info: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="parking_info" id="parking_info"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>
            <div class="row" style="margin-right: 0rem !important"> 
                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">    Unit View: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="unit_view" id="unit_view"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>

                <div class="input-field col l6 m6 s12 display_search">
                <label for="lead_assign" class="active">Comment: <span class="red-text">*</span></label>
                <input type="text" class="validate" name="comment" id="comment"   placeholder="Enter">
                <span class="add_pc_company_name_err"></span>                    
                </div>
            </div>    
        </div>

            </div>
            <div id="not_refugee1">
            <br>
            <span>ROOM | DIMENTION | DIRECTION</span> <a href="javascript:void(0)" onClick="panel_opening()" > V</a> 
            <br>
            </div>
            ';

    // $html .=' <div class="row" id="panel_table" style="display:none" >
    //             <div class="col l4 s4 m4"></div>
    //             <div class="col l8 s8 m8">
    //                 <table class="bordered  striped centered" id="example123"  style=" background-color: white;">
    //                 <thead style=" background-color: lightblue;">
    //                 <tr>
    //                 <th width="10%">Room &nbsp; &nbsp;</th>                                
    //                 <th width="8%">View</th>
    //                 <th width="6%">Dimension</th>
    //                 <th width="10%">Direction</th>
    //                 <th width="2%">Action</th>
                    
    //                 </tr>
    //                 </thead> 
    //                 <tbody>
    //             </tbody>
    //             </table>    
    //                 <table class="bordered">   
    //                 <tbody> 
    //                     <tr>
    //                         <td style="width: 24%;">
    //                             <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
    //                                 <select class="validate select2 browser-default" id="room" data-placeholder="Company name, Branch name" name="room[]">
    //                                         <option value="option 1" >Select</option>';
    //                                         foreach($room as $room){
    //                                            $html .='<option value="'. $room->room_id .' "> '. ucfirst($room->room_name) .'</option>';
    //                                         }
    //                                    $html .='</select>
    //                             <div>        
    //                         </td>
    //                         <td style="width: 20%;">
    //                             <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
    //                                 <input type="text" class="validate" name="view[]" id="view" style="margin-left: -10px;">
    //                             </div>
    //                         </td>
    //                         <td style="width: 20%;">
    //                             <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
    //                                 <input type="text" class="validate" name="dimention[]" id="dimention" style="margin-left: -10px;">
    //                             </div>
    //                         </td>
    //                         <td  style="width: 25%;">
    //                             <div class="input-field col l12 m4 s12" style="padding-left: 0px;">    
    //                                 <select class="validate select2 browser-default" id="direction" data-placeholder="Company name, Branch name" name="direction[]">
    //                                     <option value="option 1" >Select</option>';
    //                                         foreach($direction as $direction){
    //                                            $html .=' <option value="'.$direction->direction_id .'">'. ucfirst($direction->direction) .'</option>';
    //                                         }
    //                                 $html .='</select>
    //                             </div>
    //                         </td>

    //                         <td>
    //                                 <span id="AddMoreFileIdChallenge_panel" class="addbtn" style="position: unset">
    //                                 <a href="#" id="AddMoreFileBoxChallenge_Panel" class="" style="color: red !important">+</a> 
    //                                 </span>
    //                         </td>      

    //                     </tr>               
    //                 </tbody>
    //                 <tbody id="display_inputs_Challenge_panel"></tbody> 
    //             </table>

    //         </div>  ';
        echo $html;
    }

    public function viewDientionDirection(Request $request)
    {   
        $increment = $request->aa;
        $room = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');
        $html = ' <tr>
        <td style="width: 24%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;">
                <select class="validate select2 browser-default" id="room_'.$increment.'" data-placeholder="Company name, Branch name" name="room[]">
                        <option value="option 1" >Select</option>';
                        foreach($room as $room){
                           $html .='<option value="'. $room->room_id .'"> '. ucfirst($room->room_name) .' </option>';
                        }
                   $html .='</select>
            <div>        
        </td>
         <td style="width: 20%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                <input type="text" class="validate" name="view[]" id="view" style="margin-left: -10px;">
            </div>
        </td>
        <td style="width: 20%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;"> 
                <input type="text" class="validate" name="dimention[]" id="dimention" style="margin-left: -10px;">
            </div>
        </td>
        <td  style="width: 25%;">
            <div class="input-field col l12 m4 s12" style="padding-left: 0px;">    
                <select class="validate select2 browser-default" id="direction_'.$increment.'" data-placeholder="Company name, Branch name" name="direction">
                    <option value="option 1" >Select</option>';
                        foreach($direction as $direction){
                            $html .='<option value=" '.$direction->direction_id .'">'. ucfirst($direction->direction) .'</option>';
                        }
               $html .='</select>
            </div>
        </td>

        <td>
        <a href="#" style="color: red !important;font-size: 23px" class="removeclassViewDirection waves-effect waves-light">-</a>
        </td>      

    </tr>';
        echo $html;
    }

    public function unit_master_update_info(Request $request)
    {
        $property_no = $request->property_no; // property_no  // floor
        $wing_id = $request->wing_id;

        $CheckData = DB::select("select * from op_unit  where unit_code=".$property_no." and wing_id=".$wing_id." ");
        if(!empty($CheckData)){
            $floor_no = $CheckData[0]->floor_no;
            $rera_carpet_area = $CheckData[0]->rera_carpet_area;
            $mofa_carpet_area = $CheckData[0]->mofa_carpet_area;
            $direction_id = $CheckData[0]->direction_id;
            $configuration_id = $CheckData[0]->configuration_id;
            $unit_types = $CheckData[0]->unit_type;
            $build_up_area = $CheckData[0]->build_up_area;
            $no_of_bedrooms_with_sizes = $CheckData[0]->no_of_bedrooms_with_sizes;
            $no_of_bathrooms = $CheckData[0]->no_of_bathrooms;
            $hall_size = $CheckData[0]->hall_size;
            $kitchen_size = $CheckData[0]->kitchen_size;
            $parking_id = $CheckData[0]->parking_id;
            $parking_info = $CheckData[0]->parking_info;
            $unit_view = $CheckData[0]->unit_view;
            $comment = $CheckData[0]->comment;
        }else{
            $floor_no = $rera_carpet_area = $mofa_carpet_area = $direction_id =  $configuration_id ='';
            $unit_types = $build_up_area = $no_of_bedrooms_with_sizes = $no_of_bathrooms = $hall_size ='';            
            $kitchen_size = $parking_id = $parking_info = $unit_view =  $comment = '';            
            
        }
        // echo "<pre>"; print_r($CheckData); exit();

        $room = $this->getAllData('d','room');
        $direction = $this->getAllData('d','direction');
        // $direction1 = $this->getAllData('d','direction');
        // echo "<pre>";print_r($CheckData);
        $configuration = $this->getAllData('d','configuration');
        $parking = $this->getAllData('d','parking');
        
        $data = array('property_no'=>$property_no,'wing_id'=>$wing_id,'room'=>$room,'direction'=>$direction,'direction1'=>$direction,'configuration'=>$configuration,'parking'=>$parking, 'floor_no'=>$floor_no ,'rera_carpet_area'=>$rera_carpet_area, 'mofa_carpet_area'=>$mofa_carpet_area, 'direction_id'=>$direction_id, 'configuration_id'=>$configuration_id,'unit_types'=>$unit_types, 'build_up_area'=>$build_up_area, 'no_of_bedrooms_with_sizes'=>$no_of_bedrooms_with_sizes, 'no_of_bathrooms'=>$no_of_bathrooms,'hall_size'=>$hall_size,'kitchen_size'=>$kitchen_size, 'parking_id'=>$parking_id ,'parking_info'=>$parking_info,  'unit_view'=>$unit_view, 'comment'=>$comment);
        return view('unit_master/unit_master_update_info',$data);
    }
}
