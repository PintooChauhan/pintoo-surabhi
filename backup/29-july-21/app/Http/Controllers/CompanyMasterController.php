<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CompanyMasterController extends Controller
{
    public function index()
    {
        $data = DB::select("select c.company_id,p.project_id,c.group_name,p.project_complex_name,us.unit_status,l.location_name,sl.sub_location_name,c.web_site_url,c.tat_period
        from op_company as c
        left join op_project as p on .p.group_id=c.company_id
        left join dd_unit_status as us on us.unit_status_id = p.unit_status_of_complex
        left join dd_sub_location as sl on sl.sub_location_id=p.sub_location_id
        left join dd_location as l on l.location_id=p.location_id
         ");
        return view('company_master/company_master',['data'=>$data]);
    }

    public function add_company_master_form(Request $request)
    {
        // $user = Auth::user();
        // if($user == '' ){
        //     Auth::logout();
        //     return redirect('/login');
        // }

        $initials = $this->getAllData('d','initials');
        $designation = $this->getAllData('d','designation');
        $country_code = $this->getAllData('d','country');
        $db = env('operationDB');
        $company_hirarchy =   DB::connection($db)->select("select company_hirarchy_id,name  from op_company_hirarchy "); //$this->getAllData('o','company_hirarchy');        
        // echo "<pre>";print_r( $company_hirarchy);
        return view('company_master/add_company_master',['initials'=>$initials,'designation'=>$designation,'country_code'=>$country_code,'company_hirarchy'=>$company_hirarchy]);
    }

    public function add_company_master(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); exit();
        // $user = Auth::user();
        // if($user == '' ){
        //     Auth::logout();
        //     return redirect('/login');
        // } 
        
        $validator = Validator::make($request->all(), [
            'group_name' => 'required',
            'reg_office_address' => 'required',
            'tat_period' => 'required',
             'awards_reco_name.*' => 'required',
             'awards_reco_image.*' => 'required',
             
        ],
        [
            'group_name.required' => 'Group name is required',
            'reg_office_address.required' => 'Reg. office address is required',
            'tat_period.required' => 'TAT period is required',
            'awards_reco_name.*.required' => 'Awards recognition is required',
            'awards_reco_image.*.required' => 'Awards recognition photo is required'    

        ]);

          if ($validator->passes()) {
            $data = array();
            $data['group_name'] = $request->group_name;
            $data['regd_office_address'] = $request->reg_office_address;
            $data['office_phone_extension'] = $request->office_phone_extension;
            $data['office_phone'] = $request->office_phone;
            $data['office_email_id'] = $request->office_email_id;
            $data['web_site_url'] = $request->web_site_url;
            $data['company_rating'] = $request->company_rating;
            
            // $data['recognition'] = $request->recognition;
            $data['cp_code'] = $request->cp_code;
            $data['tat_period'] = $request->tat_period;

            $data2['awards_reco_name'] = $request->awards_reco_name;

            

            $data['key_member'] = $request->key_member;
            $data['help_desk_email'] = $request->help_desk_email;
            $data['comments'] = $request->comments;

            $data1['company_pros'] = $request->company_pros;
            $data1['company_cons'] = $request->company_cons;

            $join ="";
            
            if($request->hasfile('images'))
            {
                $array = array();
                foreach($request->file('images') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('images'), $rand2);
                   array_push($array,$rand2);
                    
                }
                $data['images'] = json_encode($array);
            }            
           
            if( $request->hasfile('builder_invoice_format') )
            { 
                $rand1 = mt_rand(99,9999999).".".$request->builder_invoice_format->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->builder_invoice_format->move(public_path('images'), $rand1);
                $data['builder_invoice_format'] = $rand1;
            }

            if( $request->hasfile('builder_invoice_process') )
            { 
                $rand3 = mt_rand(99,9999999).".".$request->builder_invoice_process->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->builder_invoice_process->move(public_path('images'), $rand3);
                $data['builder_invoice_process'] = $rand3;
            }

            if( $request->hasfile('builder_proforma_format') )
            { 
                $rand4 = mt_rand(99,9999999).".".$request->builder_proforma_format->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->builder_proforma_format->move(public_path('images'), $rand4);
                $data['builder_proforma_format'] = $rand4;
            }

            if( $request->hasfile('builder_payment_process') )
            { 
                $rand5 = mt_rand(99,9999999).".".$request->builder_payment_process->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->builder_payment_process->move(public_path('images'), $rand5);
                $data['builder_payment_process'] = $rand5;
            }

            if( $request->hasfile('cp_empanelment_agreement') )
            { 
                $rand6 = mt_rand(99,9999999).".".$request->cp_empanelment_agreement->extension();
                // $img = Image::make($builder_invoice_format->path());       
                // $img->resize(600, 600);        
                // // $resource = $img->stream()->detach();
                // $img->save(public_path('images'));
                // $imageName = time().'.'.$request->builder_invoice_format->extension();  
                $request->cp_empanelment_agreement->move(public_path('images'), $rand6);
                $data['cp_empanelment_agreement'] = $rand6;
            }

            // if($request->hasfile('photo_h'))
            // {
            //     $array = array();
            //     foreach($request->file('photo_h') as $file)
            //     {
            //         $rand2 = mt_rand(99,9999999).".".$file->extension();
            //         $file->move(public_path('images'), $rand2);
            //        array_push($array,$rand2);
                    
            //     }
            //     // $data['photo_h'] = json_encode($array);
            // } 
          
            DB::table('op_company')->insert($data);            
            $company_id = DB::getPdo()->lastInsertId();

            if($request->hasfile('awards_reco_image'))
            {
                $arrayI = array();
                foreach($request->file('awards_reco_image') as $file)
                {
                    $rand2 = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('images'), $rand2);
                   array_push($arrayI,$rand2);
                    
                }
                // $data2['awards_reco_image'] = ($array);
            }  
            // $data2['company_id'] = $company_id;
            $awards_reco_name = $request->awards_reco_name;
            $cnt2 = count($awards_reco_name);
            
          //  print_r($arrayI);//exit();
            for ($ii=0; $ii < $cnt2; $ii++) {  
                $awards_reco_name1 = $awards_reco_name[$ii];
                // $awards_reco_image1 = $arrayI[$ii];
                $array2 = array('company_id'=>$company_id);

                if(!empty($awards_reco_name1[$ii])){
                     $awards_reco_name2 = $awards_reco_name[$ii]; 
                    $a2 = array('awards_reco_name'=>$awards_reco_name2) ;
                    $array2=   array_merge($array2,$a2);                   
                }

                if(!empty($arrayI[$ii])){
                    $awards_reco_image2 = $arrayI[$ii]; 
                    $a2 = array('awards_reco_image'=>$awards_reco_image2) ;
                    $array2=   array_merge($array2,$a2);                   
                }else{
                    $a2 = array('awards_reco_image'=>'') ;
                    $array2=   array_merge($array2,$a2); 
                }
                
                // echo "<pre>";   print_r($array2);///
                DB::table('op_company_awards_recongnition')->insert($array2); 
            }

            // exit();

           

            $data1['company_id'] = $company_id;
            DB::table('op_company_pro_cons')->insert($data1); 
           
            $name_initial = $request->name_initial;
            $name_h = $request->name_h;
            $country_code = $request->country_code;
            $mobile_number_h = $request->mobile_number_h;
            $country_code_w = $request->country_code_w;
            $wap_number = $request->wap_number;
            $primary_email_h = $request->primary_email_h;
            $designation_h = $request->designation_h;
            $reporting_to_h = $request->reporting_to_h;
            $comments_h = $request->comments_h;

            if($request->hasfile('photo_h'))
            {
                $arrayH = array();
                foreach($request->file('photo_h') as $file)
                {
                    $rand_h = mt_rand(99,9999999).".".$file->extension();
                    $file->move(public_path('images'), $rand_h);
                        array_push($arrayH,$rand_h);
                    
                }
                // $data2['awards_reco_image'] = ($array);
            } 

            $cnt = count($name_initial);
            // echo "<pre>"; print_r($designation_h); //exit();
            for ($i=0; $i<$cnt ; $i++) {  
                // $initial = $name_initial[$i];
                // $name= $name_h[$i];
                // $country_code_id = $country_code[$i];
                // $mobile_number = $mobile_number_h[$i];
                // $country_code_w = $country_code_w[$i];
                // $whatsapp_number = $wap_number[$i];
                // $email_id = $primary_email_h[$i];
                // $designation_id = $designation_h[$i];
                // $reports_to = $reporting_to_h[$i];
                // $comments = $comments_h[$i];
                $array = array('company_id'=>$company_id);

                if(!empty($name[$i])){
                    $name = $name_h[$i]; 
                    $a = array('name'=>$name) ;
                    $array=   array_merge($array,$a);                   
                }

                if(!empty($country_code_id[$i])){
                    $country_code_id = $country_code[$i]; 
                    $a = array('country_code_id'=>$country_code_id) ;
                    $array=   array_merge($array,$a);                   
                }

                if(!empty($mobile_number[$i])){
                    $mobile_number = $mobile_number_h[$i]; 
                    $a = array('mobile_number'=>$mobile_number) ;
                    $array=   array_merge($array,$a);                   
                }

                if(!empty($whatsapp_number[$i])){
                    $whatsapp_number = $wap_number[$i]; 
                    $a = array('whatsapp_number'=>$whatsapp_number) ;
                    $array=   array_merge($array,$a);                   
                }

                if(!empty($email_id[$i])){
                    $email_id = $primary_email_h[$i]; 
                    $a = array('email_id'=>$email_id) ;
                    $array=   array_merge($array,$a);                   
                }

                if(!empty($designation_id[$i])){
                    $designation_id = $designation_h[$i]; 
                    $a = array('designation_id'=>$designation_id) ;
                    $array=   array_merge($array,$a);                   
                }

                if(!empty($reports_to[$i])){
                    $reports_to = $reporting_to_h[$i]; 
                    $a = array('reports_to'=>$reports_to) ;
                    $array=   array_merge($array,$a);                   
                }

                if(!empty($comments[$i])){
                    $comments = $comments_h[$i]; 
                    $a = array('comments'=>$comments) ;
                    $array=   array_merge($array,$a);                   
                }

                if(!empty($arrayH[$i])){
                    $photo = $arrayH[$i]; 
                    $aH = array('photo'=>$photo) ;
                    $array=   array_merge($array,$aH);                   
                }else{
                    $aH = array('photo'=>'') ;
                    $array=   array_merge($array,$aH); 
                }
                // echo "<pre>"; print_r($array);
                DB::table('op_company_hirarchy')->insert($array);    
            }

            // echo "<pre>"; print_r($cnt); exit();


            return response()->json(['success'=>'done','lastID'=>$company_id]);
        } 
        // echo "<pre>"; print_r($validator->errors()->all());
        // exit();
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    function addDesignationCompanyMaster(Request $request){     
        // echo "<pre>"; print_r($request->all()); exit();
        $validator = Validator::make($request->all(), [
            'add_designation' => 'required'                    
        ],
        [
            'add_designation.required' => 'Designation is required',
            
        ]);        

        if ($validator->passes()) {
            $data = array('designation_name'=>$request->add_designation);
            DB::table('dd_designation')->insert($data); 
            $lastInsertId = DB::getPdo()->lastInsertId();  
            $a = array('value'=>$lastInsertId,'option'=>$request->add_designation);

            return response()->json(['success'=>'Added new records.','a'=>$a]);			
        }
        return response()->json(['error'=>$validator->errors()]);
    }

    public function edit_company_master(Request $request)
    {
        return view('company_master/edit_company_master');
    }

    public function append_company_hierarchy_form(Request $request)
    {
        // echo "<pre>"; print_r($request->all());
        $cnt  = $request->aa;
        $initials = $this->getAllData('d','initials');
        $designation = $this->getAllData('d','designation');
        $country_code = $this->getAllData('d','country');
        $db = env('operationDB');
        $company_hirarchy =   DB::connection($db)->select("select company_hirarchy_id,name  from op_company_hirarchy");
        $html = '<div class="row"><div class="col l11"><div class="row"><div class="input-field col l3 m4 s12" id="InputsWrapper2"><label for="cus_name active" class="dopr_down_holder_label active">Name <span class="red-text">*</span></label><div  class="sub_val no-search"><select  id="name_initial_'.$cnt.'" name="name_initial[]" class="select2 browser-default">
        ';
            foreach($initials as $ini){
                $html .= '<option value="'.$ini->initials_id.'">'.ucfirst($ini->initials_name).'</option>';
            }
        // <option value="1">Mr.</option><option value="2">Miss.</option><option value="3">Mrs.</option>
        $html .='</select></div><input type="text" class="validate mobile_number" name="name_h[]" id="name_h"  placeholder="Enter"  ></div><div class="input-field col l3 m4 s12" id="InputsWrapper"><label for="contactNum1" class="dopr_down_holder_label active">Mobile Number: <span class="red-text">*</span></label><div  class="sub_val no-search"><select  id="country_code_'.$cnt.'" name="country_code[]" class="select2 browser-default">';
            foreach($country_code as $cou){
                $html .= '<option value="'.$cou->country_code_id.'">'.ucfirst($cou->country_code).'</option>';
            }
        // <option value="1">+91</option><option value="2">+1</option>
        $html .= '</select></div><input type="text" class="validate mobile_number" name="mobile_number_h[]" id="mobile_number"  placeholder="Enter" ></div><div class="input-field col l3 m4 s12" id="InputsWrapper2"><div  class="sub_val no-search"><select  id="country_code_w_'.$cnt.'" name="country_code_w[]" class="select2 browser-default">';
                foreach($country_code as $cou){
                    $html .= '<option value="'.$cou->country_code_id.'">'.ucfirst($cou->country_code).'</option>';
                }
        $html .= '</select></div><label for="wap_number" class="dopr_down_holder_label active">Whatsapp Number: <span class="red-text">*</span></label> <input type="text" class="validate mobile_number" name="wap_number[]" id="wap_number"  placeholder="Enter" ><div  class="addbtn" style="top: 2px !important; right: 27px !important;     width: 16px !important; " ><input type="checkbox" id="copywhatsapp" data-toggle="tooltip" title="Check if whatsapp number is same" onClick="copyMobileNo()"  style="opacity: 1 !important;pointer-events:auto"></div></div>';
        $html .= '<div class="input-field col l3 m4 s12" id="InputsWrapper3"><label for="primary_email active" class="active">Email Address: <span class="red-text">*</span></label><input type="email" class="validate" name="primary_email_h[]" id="primary_email"  placeholder="Enter" ></div> </div>';
        $html .=' <div class="row"><div class="input-field col l3 m4 s12 display_search"><select class="select2  browser-default designation_h"  data-placeholder="Select" id="designation_'.$cnt.'" name="designation_h[]">';
            foreach($designation as $des){
                $html .= '<option value="'.$des->designation_id.'">'.ucfirst($des->designation_name).'</option>';
            }
        $html .='</select><label for="designation" class="active">Designation </label><div id="AddMoreFileId3" class="addbtn" style="right: -9px !important;" ><a href="#modal9" id="add_com_state" class="waves-effect waves-light  modal-trigger" style="color: red !important"> +</a></div></div>';    
        $html .='<div class="input-field col l3 m4 s12 display_search"><select class="select2  browser-default"  data-placeholder="Select" id="reporting_to_'.$cnt.'" name="reporting_to_h[]"><option value=""></option>';
            foreach($company_hirarchy as $comp){
                $html .= '<option value="'.$comp->company_hirarchy_id.'">'.ucfirst($comp->name).'</option>';
            }
        $html .='</select><label for="reporting_to" class="active">Reporting to </label></div><div class="input-field col l3 m4 s12" id="InputsWrapper2"><label  class="active">Comments: <span class="red-text">*</span></label>
        <input type="text" class="validate" name="comments_h[]" id="comments_h[]"  placeholder="Enter" ></div><div class="input-field col l3 m4 s12 display_search"><label  class="active">Upload Photo : </label><input type="file" name="photo_h[]"  id="input-file-now" class="dropify" data-default-file="" style="padding-top: 14px;"></div></div></div>';    
        $html .= ' <div class="col l1"><a href="#" style="color: red !important;font-size: 23px" class="removeclassCompany waves-effect waves-light">-</a><br><a href="#modal10" id="add_location" title="View Flow Chart" class="modal-trigger" style="color: grey !important;font-size: 23px;"> <i class="material-icons  dp48">remove_red_eye</i></a></div>';
        return $html;
    }

    
}
