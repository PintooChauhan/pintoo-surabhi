<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;
class Requirementr1Layout extends Component
{
    use DatabaseTrait;
    public function render()
    {      
        $unit = $this->getAllData('d','unit');       
        $unit1 = $this->getAllData('d','unit');
        $unit_type_residential = $this->getAllData('d','unit_type_residential');
        $unit_type_commercial = $this->getAllData('d','unit_type_commercial');
        $configuration = $this->getAllData('d','configuration');
        $area_range = $this->getAllData('d','area_range');

        // print_r($unit1); exit();
        return view('requirement.requirementr1',['unit'=>$unit,'unit1'=>$unit1,'unit_type_residential'=>$unit_type_residential,'unit_type_commercial'=>$unit_type_commercial, 'configuration'=>$configuration,'area_range'=>$area_range]);
    }
}
