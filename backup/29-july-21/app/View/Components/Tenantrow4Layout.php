<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;

class Tenantrow4Layout extends Component
{
    use DatabaseTrait;
    public function render()
    {      
        return view('tenant.tenantrow4');
        
    }
}
