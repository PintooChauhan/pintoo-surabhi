<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;

class Leadinfor1Layout extends Component
{
    use DatabaseTrait;
    public function render()
    {
        $lead_by = $this->getAllData('d','lead_by');
        $lead_type = $this->getAllData('d','lead_type');
        $lead_source = $this->getAllData('d','lead_source');
        $lead_campaign = $this->getAllData('d','lead_campaign');
        
        return view('lead_info.leadinfor1',['lead_by'=>$lead_by,'lead_type'=>$lead_type,'lead_source'=>$lead_source,'lead_campaign'=>$lead_campaign]);
    }
}
