<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;

class AllModalsLayout extends Component
{
    use DatabaseTrait;
    public function render()
    {        
        return view('modals.all_modals');
    }
}
