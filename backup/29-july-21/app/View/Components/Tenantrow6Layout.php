<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;

class Tenantrow6Layout extends Component
{
    use DatabaseTrait;
    public function render()
    {      
        return view('tenant.tenantrow6');
        
    }
}
