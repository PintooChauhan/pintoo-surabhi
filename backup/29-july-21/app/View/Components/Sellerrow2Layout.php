<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Traits\DatabaseTrait;

class Sellerrow2Layout extends Component
{
    use DatabaseTrait;
    public function render()
    {       
        $unit = array();// $this->getAllData('view','unit');
        $unit1 = array();// $this->getAllData('view','unit');
        $unit_type_residential = array();// $this->getAllData('view','unit_type_residential');
        $unit_type_commercial = array();// $this->getAllData('view','unit_type_commercial');
        $configuration = array();// $this->getAllData('view','configuration');
        $area_range = array();// $this->getAllData('view','area_range');
        return view('seller.sellerrow2',['unit'=>$unit,'unit1'=>$unit1,'unit_type_residential'=>$unit_type_residential,'unit_type_commercial'=>$unit_type_commercial, 'configuration'=>$configuration,'area_range'=>$area_range]);
        
    }
}
