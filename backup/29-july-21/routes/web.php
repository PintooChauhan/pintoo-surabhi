<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','LeadDetailsController@lead_details');
Route::get('addPcCompanyName','LeadDetailsController@addPcCompanyName');
Route::get('addPcExcecutiveName','LeadDetailsController@addPcExcecutiveName');
Route::get('addUnitInformation','LeadDetailsController@addUnitInformation');
Route::get('addUnitAmenity','LeadDetailsController@addUnitAmenity');
Route::get('addBuildingAmenity','LeadDetailsController@addBuildingAmenity');


Route::get('addLocation','LeadDetailsController@addLocation');

Route::get('addResCity','LeadDetailsController@addResCity');
Route::get('addCompanyCity','LeadDetailsController@addCompanyCity');
Route::get('addDesignation','LeadDetailsController@addDesignation');
Route::get('addIndustry','LeadDetailsController@addIndustry');



Route::get('calculate_budget_break_up','LeadDetailsController@calculate_budget_break_up');
Route::get('download_budget_break_up','LeadDetailsController@download_budget_break_up');

Route::get('users-list','UserMasterController@index');
Route::get('user-form','UserMasterController@new_user');
Route::get('access-group','UserMasterController@access_group');


Route::get('seller','SellerController@index');
Route::get('addBuildingName','SellerController@addBuildingName');
Route::get('addWingName','SellerController@addWingName');
Route::get('addSuffixName','SellerController@addSuffixName');

Route::get('owner','OwnerController@index');

Route::get('tenant','TenantController@index');

// Company Master
Route::get('admin','CompanyMasterController@redirect');
Route::get('company-master','CompanyMasterController@index');
Route::get('add-company-master-form','CompanyMasterController@add_company_master_form');
Route::get('edit-company-master','CompanyMasterController@edit_company_master');
Route::get('addDesignationCompanyMaster','CompanyMasterController@addDesignationCompanyMaster');



Route::post('add-company-master','CompanyMasterController@add_company_master')->name('add-company-master');
Route::get('append_company_hierarchy_form','CompanyMasterController@append_company_hierarchy_form');


// project Master
Route::get('project-master','ProjectMasterController@index');
Route::post('add-project-master','ProjectMasterController@add_project_master')->name('add-project-master');
Route::post('update-project-master','ProjectMasterController@update_project_master')->name('update-project-master');


// /Society Master
Route::get('society-master','SocietyMasterController@index');
Route::post('add-society-master','SocietyMasterController@add_society_master')->name('add-society-master');
Route::get('append_society_hierarchy_form','SocietyMasterController@append_society_hierarchy_form');
Route::get('append_config_sample','SocietyMasterController@append_config_sample');

// Wing Master
Route::get('wing-master','WingMasterController@index');
Route::get('add-wing-details','WingMasterController@add_wing_details');
Route::post('add-wing-master','WingMasterController@add_wing_master')->name('add-wing-master');
Route::post('add_unit_info','WingMasterController@add_unit_info');
Route::get('add_unit_master_data','WingMasterController@add_unit_master_data');



// Unit Master
Route::get('unit-master','UnitMasterController@index');
Route::post('add-unit-master','UnitMasterController@add_unit_master')->name('add-unit-master');
Route::get('append_unit_structure','UnitMasterController@append_unit_structure');
Route::get('append_unit_structure_side_view','UnitMasterController@append_unit_structure_side_view');
Route::get('viewDientionDirection','UnitMasterController@viewDientionDirection');
Route::get('unit_master_update_info','UnitMasterController@unit_master_update_info');




























Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
