<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{UserMasterController, RoleMasterController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/buyer','LeadDetailsController@lead_details');
Route::get('/','CompanyMasterController@index');
Route::get('addPcCompanyName','LeadDetailsController@addPcCompanyName');
Route::get('addPcExcecutiveName','LeadDetailsController@addPcExcecutiveName');
Route::get('addUnitInformation','LeadDetailsController@addUnitInformation');
Route::get('addUnitAmenity','LeadDetailsController@addUnitAmenity');
Route::get('addBuildingAmenity','LeadDetailsController@addBuildingAmenity');
Route::post('saveBuyer','LeadDetailsController@saveBuyer');

Route::post('lookingSince','LeadDetailsController@lookingSince');
Route::post('challenges_and_solutions','LeadDetailsController@challenges_and_solutions');
Route::get('/getMaximumBudget','LeadDetailsController@getMaximumBudget');
Route::get('/getOwnContribution','LeadDetailsController@getOwnContribution');
Route::get('/getUnitTypes','LeadDetailsController@getUnitTypes');
Route::post('getData1','LeadDetailsController@getData1');
Route::post('updateTableRecords','LeadDetailsController@updateTableRecords');
Route::post('deleteTableRecords','LeadDetailsController@deleteTableRecords');

Route::get('buyer-list-view','LeadDetailsController@buyerListView');
Route::get('getMinCarpetArea','LeadDetailsController@getMinCarpetArea');
Route::get('findlocationWithsublocationID','LeadDetailsController@findlocationWithsublocationID');
Route::get('getMaxCarpetArea','LeadDetailsController@getMaxCarpetArea');
Route::get('deleteBuyer','LeadDetailsController@deleteBuyer');
Route::get('deleteTenant','LeadDetailsController@deleteTenant');

// Buyer Filter
// Route::get('buyer-list-view','LeadDetailsController@buyerListView');


Route::get('addLocation','LeadDetailsController@addLocation');

Route::get('addResCity','LeadDetailsController@addResCity');
Route::get('addCompanyCity','LeadDetailsController@addCompanyCity');
Route::get('addDesignation','LeadDetailsController@addDesignation');
Route::get('addIndustry','LeadDetailsController@addIndustry');



Route::get('calculate_budget_break_up','LeadDetailsController@calculate_budget_break_up');
Route::get('download_budget_break_up','LeadDetailsController@download_budget_break_up');

Route::get('users-list','UserMasterController@index');
Route::get('user-form','UserMasterController@new_user');
Route::get('access-group','UserMasterController@access_group');

// User View
Route::get('user_master',[UserMasterController::class,'userView'])->name('user.userView');
Route::get('addUser',[UserMasterController::class,'addUser'])->name('user.addUser');
// Add user
Route::post('saveUser',[UserMasterController::class,'saveUser'])->name('user.saveUser');
// delete Image and resume
Route::post('delUserImage',[UserMasterController::class,'delUserImage']);
// status user
Route::post('statusUser',[UserMasterController::class,'statusUser']);
// delete user
Route::post('deleteUser',[UserMasterController::class,'deleteUser']);

// Change Password
Route::post('updatePasswordUser',[UserMasterController::class,'updatePasswordUser']);

// get Reporting Manger
Route::get('getUserReportingManagerData',[UserMasterController::class,'getUserReportingManagerData']);

Route::get('/register', 'UserMasterController@create');
Route::post('/register', 'UserMasterController@store');
         

// Roles View
Route::get('role_master',[RoleMasterController::class,'role_master'])->name('role.role_master');
// Add/Edit Access Level View
Route::get('add_role_access_level',[RoleMasterController::class,'add_role_access_level'])->name('role.add_role_access_level');
Route::post('saveRoleAccessGroup',[RoleMasterController::class,'saveRoleAccessGroup'])->name('role.saveRoleAccessGroup');

// Seller Controller
Route::get('seller','SellerController@index');
Route::get('addBuildingName','SellerController@addBuildingName');
Route::get('addWingName','SellerController@addWingName');
Route::get('addSuffixName','SellerController@addSuffixName');
Route::post('reasontoSell','SellerController@reason_to_sell');

// Owner Controller
Route::get('owner','OwnerController@index');

// Tenant Controller
Route::get('tenant','TenantController@index');
Route::get('getMaxRent','TenantController@getMaxRent');
Route::get('getMaxDeposit','TenantController@getMaxDeposit');
Route::post('savetenat','TenantController@saveTenat');
Route::get('tenant-list-view','TenantController@tenantListView');

Route::post('lookingSinceTenant','TenantController@lookingSinceTenant');
Route::post('challenges_and_solutions_tenant','TenantController@challenges_and_solutions_tenant');

// Company Master
Route::get('admin','CompanyMasterController@redirect');
Route::get('company-master','CompanyMasterController@index');
Route::any('add-company-master-form','CompanyMasterController@add_company_master_form');
Route::get('edit-company-master','CompanyMasterController@edit_company_master');
Route::get('addDesignationCompanyMaster','CompanyMasterController@addDesignationCompanyMaster');
Route::post('update-company-master','CompanyMasterController@update_company_master_form');
Route::get('searchData','CompanyMasterController@searchData');
Route::post('updateTableFormRecords','CompanyMasterController@updateTableFormRecords');
Route::post('deleteTableFormRecords','CompanyMasterController@deleteTableFormRecords');
Route::post('getData','CompanyMasterController@getData');
Route::get('company-list','CompanyMasterController@company_list');
Route::get('search-Company-List','CompanyMasterController@search_company_list');
Route::get('flow_chart_company','CompanyMasterController@flow_chart_company');
Route::post('delImage','CompanyMasterController@delImage');
Route::post('delImageHei','CompanyMasterController@delImageHei');
Route::post('delImageAward','CompanyMasterController@delImageAward');


Route::post('add-company-master','CompanyMasterController@add_company_master')->name('add-company-master');
Route::get('append_company_hierarchy_form','CompanyMasterController@append_company_hierarchy_form');

Route::get('logout','CompanyMasterController@logout');


// project Master
Route::get('project-master','ProjectMasterController@index');
Route::post('add-project-master','ProjectMasterController@add_project_master')->name('add-project-master');
Route::post('update-project-master','ProjectMasterController@update_project_master')->name('update-project-master');
Route::post('getDataProjet','ProjectMasterController@getDataProjet');
Route::post('updateProjectFormRecords','ProjectMasterController@updateProjectFormRecords');
Route::post('deleteProjectFormRecords','ProjectMasterController@deleteProjectFormRecords');
Route::get('findCityWithStateID','ProjectMasterController@findCityWithStateID');
Route::get('findlocationWithcityID','ProjectMasterController@findlocationWithcityID');
Route::get('findsublocationWithlocationID','ProjectMasterController@findsublocationWithlocationID');
Route::get('project-list','ProjectMasterController@project_list');
Route::get('search-project-List','ProjectMasterController@search_project_list');
Route::post('updateProjectAmenity','ProjectMasterController@updateProjectAmenity');
Route::post('addProjectAmenity','ProjectMasterController@addProjectAmenity');
Route::get('flow_chart_project','ProjectMasterController@flow_chart_project');
Route::post('createEmptyProject','ProjectMasterController@createEmptyProject');
Route::post('delImageP','ProjectMasterController@delImageP');
Route::post('delImageB','ProjectMasterController@delImageB');






// /Society Master
Route::get('society-master','SocietyMasterController@index');
Route::post('add-society-master','SocietyMasterController@add_society_master')->name('add-society-master');
Route::get('append_society_hierarchy_form','SocietyMasterController@append_society_hierarchy_form');
Route::get('append_config_sample','SocietyMasterController@append_config_sample');
Route::post('update-society-master','SocietyMasterController@update_society_master');
Route::post('getDataSociety','SocietyMasterController@getDataSociety');
Route::post('updateSocietyFormRecords','SocietyMasterController@updateSocietyFormRecords');
Route::post('deleteSocietyFormRecords','SocietyMasterController@deleteSocietyFormRecords');
Route::post('updateAmenity','SocietyMasterController@updateAmenity');

Route::get('society-list','SocietyMasterController@society_list');
Route::get('search-society-List','SocietyMasterController@search_society_list');
Route::post('getSublocationId','SocietyMasterController@getSublocationId');
Route::post('createEmptySociety','SocietyMasterController@createEmptySociety');

Route::post('delImageS','SocietyMasterController@delImageS');
Route::post('delImageConst','SocietyMasterController@delImageConst');
Route::post('delImageRS','SocietyMasterController@delImageRS');

Route::post('updateUnitAmenity','SocietyMasterController@updateUnitAmenity');





// Wing Master
Route::get('wing-master','WingMasterController@index');
Route::get('add-wing-details','WingMasterController@add_wing_details');
Route::post('add-wing-master','WingMasterController@add_wing_master')->name('add-wing-master');
Route::post('add_unit_info','WingMasterController@add_unit_info');
Route::get('add_unit_master_data','WingMasterController@add_unit_master_data');
Route::post('update-wing-master','WingMasterController@update_wing_master');
Route::post('removeWing','WingMasterController@removeWing');
Route::post('replicaWing','WingMasterController@replicaWing');
Route::get('getUnitConfigDetails1','WingMasterController@getUnitConfigDetails1');
Route::post('delImageW','WingMasterController@delImageW');
Route::post('uploadImg','WingMasterController@uploadImg');
Route::get('generate_structure','WingMasterController@generate_structure');
Route::any('storeStructure','WingMasterController@storeStructure');








// Unit Master
Route::get('unit-master','UnitMasterController@index');
Route::post('add-unit-master','UnitMasterController@add_unit_master')->name('add-unit-master');
Route::get('append_unit_structure','UnitMasterController@append_unit_structure');
Route::get('append_unit_structure_side_view','UnitMasterController@append_unit_structure_side_view');
Route::get('viewDientionDirection','UnitMasterController@viewDientionDirection');
Route::get('unit_master_update_info','UnitMasterController@unit_master_update_info');
Route::get('updateSeries','UnitMasterController@updateSeries');
Route::post('update_series','UnitMasterController@update_series');
Route::post('save-unit','UnitMasterController@save_unit');
Route::post('getDataUnitData','UnitMasterController@getDataUnitData');
Route::post('updateTableUnitDetails','UnitMasterController@updateTableUnitDetails');
Route::post('deleteUnitFormRecords','UnitMasterController@deleteUnitFormRecords');
Route::get('getUnitConfigDetails','UnitMasterController@getUnitConfigDetails');

Route::post('add_configuration','UnitMasterController@add_configuration');
Route::post('add_room','UnitMasterController@add_room');

/// Property Consultant
Route::get('add-new-pc','PCMasterController@pc_master');
Route::get('pc-master','PCMasterController@pc_master_list');
Route::post('savePc','PCMasterController@savePc');
Route::post('saveData','PCMasterController@saveData');
Route::post('getData2','PCMasterController@getData');
Route::post('updateTableRecords1','PCMasterController@updateTableRecords');
Route::post('seachByMobile','PCMasterController@seach_by_mobile');



Route::get('add-branch-information','PCMasterController@branch_information');
Route::post('saveBranch','PCMasterController@saveBranch');
Route::get('getFocusedSublocation','PCMasterController@getFocusedSublocation');
Route::get('getFocusedComplex','PCMasterController@getFocusedComplex');
Route::get('add-employee-information','PCMasterController@employee_information');
Route::post('saveEmployee','PCMasterController@saveEmployee');
Route::post('saveDataReview','PCMasterController@saveDataReview');
Route::post('getDataReview','PCMasterController@getDataReview');
Route::post('updateTableRecordsReview','PCMasterController@updateTableRecordsReview');

Route::post('deletePartially','PCMasterController@deletePartially');
Route::post('deleteCompletly','PCMasterController@deleteCompletly');



// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::get('dashboard',[RoleMasterController::class,'dashboard'])->middleware(['auth'])->name('dashboard');


Route::get('/clear-cache-all', function() {
    Artisan::call('cache:clear');
  
    dd("Cache Clear All");
});

require __DIR__.'/auth.php';
